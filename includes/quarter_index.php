<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Quaterly index -
        <?php
			$ind = FetchData("curriculum",array(),"where quarter_index!=0 group by quarter_index order by quarter_index desc");
			$index = $ind[0]['quarter_index'];
			?>
        <?php																	
     	$date = FetchData("curriculum",array("min(cdate) as mindate", "max(cdate) as maxdate"),"where quarter_index='".$index."'");
      $y1 = date('Y',strtotime($date[0]['mindate']));
      $y2 = date('Y',strtotime($date[0]['maxdate']));
      if($y1!=$y2){
        echo date('F, Y',strtotime($date[0]['mindate']));
				echo " to ";
				echo date('F, Y',strtotime($date[0]['maxdate']));
			}
      else{
        echo date('F',strtotime($date[0]['mindate']));
				echo " to ";
				echo date('F',strtotime($date[0]['maxdate']));
			}
      ?>
      </h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><h2>SENIOR AND JUNIOR LESSONS FOR QUARTER -
        <?php
				echo strtoupper(date('F d, Y',strtotime($date[0]['mindate'])));
				echo strtoupper(" to ");
				echo strtoupper(date('F d, Y',strtotime($date[0]['maxdate'])));
			?>
      </h2></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" cellpadding="5" cellspacing="0">
        <?php
        $lesson = FetchData("curriculum",array(),"where quarter_index = '".$index."' group by cdate order by cdate");
        for($i=0;$i<count($lesson);$i++){
      ?>
      	<tr>
          <td colspan="2"><hr style="border:1px solid #CCCCCC" /></td>
        </tr>
        <tr>
          <td width="25%" align="left" valign="top"><span style="font-size:14px; font-weight:bold; color:#EF7C27;"><?php echo date("F d, Y",strtotime($lesson[$i]['cdate']));?></span> </td>
          <td width="75%" align="left" valign="top"><table width="100%" cellpadding="0" cellspacing="1" border="0">
              	<?php
			  	$jnr1 = FetchData("curriculum",array(),"where category_id = 3 and quarter_index = '".$index."' and cdate = '".$lesson[$i]['cdate']."' order by lesson_no");
				if(count($jnr1)>0){
					for($d=0;$d<count($jnr1);$d++){		
						$jnr = FetchData("curriculum",array(),"where category_id = 3 and lesson_no = '".$jnr1[$d]['lesson_no']."'");
						if(count($jnr)>0){
				?>
              	<tr>
                <td align="left" valign="top"><table width="100%" cellpadding="5" cellspacing="0">
                    <tr>
                      <td bgcolor="#ebf7fc" style=" font-size:14px; font-weight:bold; color:#045577; font-family:Georgia, "times new roman", times, serif;">LESSON <?php echo GetFieldData("book","lesson_no","where id = ".$jnr[0]['lesson_no']);?> Junior Course</td>
                    </tr>
                    <tr>
                      <td><b><?php echo stripslashes($jnr[0]['title'])?></b><br /><?php echo stripslashes($jnr[0]['short_description'])?></td>
                    </tr>
                    <tr>
                    	<td>                        
                        <b>Memory Verse: </b><?php echo strip_tags(stripslashes($jnr[0]['memory_verse']));?>
                      </td>
                    </tr>
                  </table></td>
                <td width="20%" align="center" valign="top" style="padding:5px;"><a href="index.php?p=curriculum_detail&id=<?php echo $jnr[0]['id']?>">Read More</a>                 	
                  <?php 
									if(stripslashes($jnr[0]['file'])!=''){
										echo "<br /><br><a href='"."uploads/curriculum/download/".stripslashes($jnr[0]['file'])."' class='blue_link'><img src='images/word.gif' alt='' align='absmiddle' /></a>";
									}?>
                  <?php 
									if(stripslashes($jnr[0]['video'])!=''){								
										$fullPath = 'uploads/curriculum/video/'.stripslashes($jnr[0]['video']);
										echo "<br /><br><a href=\"download.php?file=".$fullPath."\" class='blue_link'><img src='images/pdf.gif' alt='' align='absmiddle' /></a>";										
									}?>
                </td>
              </tr>
              	<?php } ?>
               	<?php } ?>
                <?php } ?>
              	<?php
			  	$snr1 = FetchData("curriculum",array(),"where category_id = 4 and quarter_index = '".$index."' and cdate = '".$lesson[$i]['cdate']."' order by lesson_no");
				if(count($snr1)>0){
					for($d=0;$d<count($snr1);$d++){		
						$snr = FetchData("curriculum",array(),"where category_id = 4 and lesson_no = ".$snr1[$d]['lesson_no']);
						if(count($snr)>0){
				?>
              	<tr>
                <td align="left" valign="top"><table width="100%" cellpadding="5" cellspacing="0">
                    <tr>
                      <td bgcolor="#eafbdb" style=" font-size:14px; font-weight:bold; color:#045577; font-family:Georgia, "times new roman", times, serif;"> LESSON <?php echo GetFieldData("book","lesson_no","where id = ".$snr[0]['lesson_no']);?> Senior Course </td>
                    </tr>
                    <tr>
                      <td><b><?php echo stripslashes($snr[0]['title'])?></b><br /><?php echo stripslashes($snr[0]['short_description'])?></td>
                    </tr>
                    <tr>
                    	<td>                        
                        <b>Memory Verse: </b><?php echo strip_tags(stripslashes($snr[0]['memory_verse']));?>
                      </td>
                    </tr>
                  </table></td>
                <td align="center" valign="top" style="padding:5px;"><a href="index.php?p=curriculum_detail&id=<?php echo $snr[0]['id']?>">Read More</a> 
                	<?php 
									if(stripslashes($snr[0]['file'])!=''){
										echo "<br /><br><a href='"."uploads/curriculum/download/".stripslashes($snr[0]['file'])."' class='blue_link'><img src='images/word.gif' alt='' align='absmiddle' /></a>";
									}?>
                  <?php 
									if(stripslashes($snr[0]['video'])!=''){								
										$fullPath = 'uploads/curriculum/video/'.stripslashes($snr[0]['video']);
										echo "<br /><br><a href=\"download.php?file=".$fullPath."\" class='blue_link'><img src='images/pdf.gif' alt='' align='absmiddle' /></a>";										
									}?>
                </td>
              </tr>
              	<?php } ?>
                <?php } ?>
                <?php } ?>
            </table></td>
        </tr>        
        <?php } ?>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
