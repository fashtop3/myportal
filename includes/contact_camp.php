<script type="text/javascript">
$(document).ready(function(){ 
	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content
		$('#Content').val(con); // put it in the textarea
	}); 		
	$("#frmCamp").validate();
});
</script>
<?php 
$user = FetchData("camp_user",array(),"where id = '".$_REQUEST['id']."'");
if($_REQUEST['Process']!=''){

	$q = FetchData("camp_user",array(),"where id='".$_REQUEST['id']."'");
		
	$content = $_REQUEST['Content'];
	
	$Process = loadVariable("Process", "");
		
	// *****************************************************
	// ==================== SEND AN EMAIL ==================
	if(isset($Process) && !empty($Process)){ 
		$Subject = $_REQUEST['Subject'];
		$To = $_REQUEST['to1'];
		$From = $_REQUEST['from1'];		
		$Template="mail_templates/general.html";
		$TemplateVars=array(							
							'Message'=>$content
							);
		
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
		//echo $flag;
		if($flag){
			$_SESSION['SuccessMsg'] = "Mail Sent Successfully";	
			header("Location:index.php?p=online_giving");		
		}else{
			$_SESSION['ErrorMsg'] = 'Error while sending mail. Please try again.';				  
			header("Location:index.php?p=online_giving");
		}
		
	}	
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Contact Camp Team</h3></td>
  </tr>  
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">
    	<form name="frmCamp" id="frmCamp" action="" method="post">
      	<input type="hidden" name="id" value="<?php echo $user[0]['id']?>" id="id" />
        <input type="hidden" name="Process" value="Send" id="id" />
        <input type="hidden" name="to" value="camp@apostolicfaith.org.uk" id="to" />
    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
        <tr>
          <td align="left" valign="top">From:</td>
          <td align="left" valign="top"><input type="text" name="from1" id="from1" value="<?php echo stripslashes($user[0]['email'])?>" disabled="disabled" class="field_bdr" style="height:20px; width:250px;" /></td>
        </tr>
        <tr>
          <td align="left" valign="top">To:</td>
          <td align="left" valign="top"><input type="text" name="to1" id="to1" value="camp@apostolicfaith.org.uk" disabled="disabled" class="field_bdr" style="height:20px; width:250px;" /></td>
        </tr>
        <tr>
          <td align="left" valign="top">Subject:</td>
          <td align="left" valign="top"><input type="text" name="Subject" id="Subject" title=" *" validate="required:true" class="field_bdr" style="height:20px; width:250px;" /></td>
        </tr>
        <tr>
          <td align="left" valign="top">Message:</td>
          <td align="left" valign="top">
          	<textarea class="field_bdr" style="width:450px; height:250px;" title=" *" validate="required:true" type="text" name="Content" id="Content"></textarea>
          </td>
        </tr>
        <tr>
          <td align="left" valign="top"></td>
          <td align="left" valign="top">
          <input value="Back" class="search_bg" style="cursor:pointer;" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=online_giving';">
          <input type="submit" id="submit1" value="Send Mail" style="cursor:pointer;" class="search_bg" /></td>
        </tr>
      </table>
      </form>
    </td>
  </tr>
</table>  