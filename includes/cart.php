<?php
$sql = "SELECT * FROM cart WHERE member_id='".$_SESSION['USER_ID']."'";
$cart = $objDB->select($sql);
?>
<script>
$(document).ready(function(){
	$("#frmAdmin").validate();
});
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Shopping Cart</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top">&nbsp;
            <?php showMessage();?></td>
        </tr>
        <tr>
          <td align="left" valign="top"><table width="100%" cellpadding="5" cellspacing="1">
              <tr>
                <td align="left"><img src="images/back.jpg" align="absmiddle" alt="" /> <a href="index.php?p=offering" style="font-size:12px;" class="orange_link">Continue Shopping</a> </td>
              </tr>
              <tr>
                <td align="left"><h3>Order Details</h3></td>
              </tr>
              <tr>
                <td align="left"><b>Please Review Your Order Details</b></td>
              </tr>
              <tr>
                <td align="left">To change the quantity: type in the new quantity and select [UPDATE CART].<br />
                  To remove an item: click on Delete Icon.</td>
              </tr>
              <tr>
                <td align="left"><form action="manage_cart.php" method="post" name="frmAdmin" id="frmAdmin">
                    <input type="hidden" name="url" value="" />
                    <input type="hidden" name="action" value="update" />
                    <!--input type="hidden" value="<?php echo $_SESSION['PlaceID'.$userplc];?>" name="Place" id="Place" /-->
                    <input type="hidden" value="<?php echo count($cart);?>" name="Total" id="Total" />
                    <table width="100%" cellpadding="5" cellspacing="1" border="0">
                      <tr bgcolor="#CC9966">
                        <td align="left" style="color:#FFFFFF; font-weight:bold;"> Product Name </td>
                        <td align="left" style="color:#FFFFFF; font-weight:bold;"> Quantity </td>
                        <td align="left" style="color:#FFFFFF; font-weight:bold;"> Unit Price </td>
                        <td align="left" style="color:#FFFFFF; font-weight:bold;"> Amount </td>
                        <td align="left" style="color:#FFFFFF; font-weight:bold;"> Delete </td>
                      </tr>
                      <?php if(count($cart)>0){ 
								$total = 0;
								for($i=0;$i<count($cart);$i++){
									$total += stripslashes($cart[$i]['total_price']);
							?>
                      <tr bgcolor="#F1F1F1">
                        <td align="left"><input type="hidden" value="<?php echo $cart[$i]['id']?>" name="ID<?php echo $i?>" />
                          <input type="hidden" class="gray_bdr" title="*" validate="required: true" value="<?php echo number_format(stripslashes($cart[$i]['price']),2)?>" name="Price<?php echo $i?>" />
                          <?php if(strtolower($cart[$i]['product_id'])==''){?>
                          <a href="index.php?p=offering" class="blue_link"><?php echo stripslashes($cart[$i]['product'])?></a>
                          <?php }else{ ?>
                          <a href="index.php?p=cd&id=<?php echo strtolower($cart[$i]['product_id']);?>&type=<?php echo stripslashes($cart[$i]['product']);?>" class="blue_link"><?php echo stripslashes(FetchValue("cd","title","id",$cart[$i]['product_id']));?></a>
                          <?php } ?>
                        </td>
                        <?php if(strtolower($cart[$i]['product_id'])!=''){?>
                        <td align="left"><input type="text" class="gray_bdr" size="3" title="*" validate="required: true" value="<?php echo $cart[$i]['qty']?>" name="Quantity<?php echo $i?>" <?php if(number_format($cart[$i]['price'],2)=='0.00'){ echo "readonly "; echo "style='background-color:#CCCCCC' ";}?> /></td>
                        <td align="left"><?php echo getCurrencySign(CURRENCY_SIGN).number_format(stripslashes($cart[$i]['price']),2);?></td>
                        <?php }else{ ?>
                        <td align="left">&nbsp;</td>
                        <td align="left"><?php echo getCurrencySign(CURRENCY_SIGN).number_format(stripslashes($cart[$i]['price']),2);?></td>
                        <?php } ?>
                        <td align="left"><?php echo getCurrencySign(CURRENCY_SIGN).number_format(stripslashes($cart[$i]['total_price']),2);?></td>
                        <td align="center"><img src="images/trash.gif" align="absmiddle" alt="" onmouseover="this.style.cursor='pointer'" onclick="window.location='manage_cart.php?action=delete&id=<?php echo $cart[$i]['id']?>'" /></td>
                      </tr>
                      <?php } ?>
                      <tr>
                        <td align="left" colspan="5"><br />
                          <b>Subtotal:</b> <font size="3"><b><?php echo getCurrencySign(CURRENCY_SIGN).number_format($total,2);?></b></font> </td>
                      </tr>
                      <tr>
                        <td><strong>Place</strong>: </td>
                        <td><select name="Place" id="Place" class="gray_bdr" style="width:210px" onChange="checkCountyOther();">
                              <option value="">No Preference</option>
                              <?php echo FillCombo1('county','c_name','c_id',$cart[0]['place_id'],'where place_key!="portland_oragon"'); ?>
                              
                           </select>
                        </td>
                     </tr>
                      <tr>
                        <td align="left" colspan="5" valign="top"><div align="left" style="vertical-align:top;"><b>Additional Instructions</b></div>
                          <textarea name="Desc" id="Desc" class="gray_bdr" rows="7" cols="35"><?php echo stripslashes($cart[0]['description'])?></textarea>
                        </td>
                      </tr>
                      <tr>
                        <td align="left" colspan="5"><input type="submit" value="Update Cart" class="search_bg" style="height:22px;" />
                          <span style="float:right">
                          <input type="button" onmouseover="this.style.cursor='pointer'" onclick="window.location='paypal.php?url=&Place=<?php echo $_SESSION['PlaceID'.$userplc];?>'" value="Proceed To Paypal Checkout" style="height:22px;" class="search_bg" />
                          </span>
                          <br><br>
                          <strong><i>Important Note:</i></strong>
                          	<ul> 
                          		<li>After complete your paypal payment, you must have to return to the site from your Paypal payment<br><br></li>                          		
                          		<li>There is a link in Paypal after payment something like <br>"Return to the Apostolic Faith Mission (UK)"<br><br></li>
                          		<li>By click on it will redirect to this site to complete your payment process and view and print your order details</li>
                          	</ul>  
                      	</td>
                      </tr>
                      <?php } else { ?>
                      <tr>
                        <td colspan="5" align="left">Your cart is empty.</td>
                      </tr>
                      <?php } ?>
                    </table>
                  </form></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
