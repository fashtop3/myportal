<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>The Higher Way</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top">
    	<?php 
			/*$hg = file_get_contents("http://apostolicfaith.org/Library/HigherWay.aspx");
			$s1 = substr($hg,strpos($hg,"<!-- Start_Module_501 -->"));
			$s2 = substr($s1,0,strpos($s1,"</table>")+8);
			$s2 = str_replace("h5>","h2>",$s2);
			$s2 = str_replace('src="/','src="http://apostolicfaith.org/',$s2);
			echo $s2;*/
			$content = FetchData("cms",array(),"where id = 24");
			echo str_replace("`",'"',stripslashes($content[0]['description']));
			?>

    	<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <!--<tr>
          <td><h2>April-June Higher Way</h2></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td align="center"><a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_103_2/hw_103_2"><img alt="" src="uploads/higherway/HW_103_2cover.jpg" border="0" width="206" height="267"></a> </td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
        </tr>-->
        <tr>
          <td><!--<table align="center" border="0" cellpadding="3" cellspacing="3" width="100%">
              <tbody>
                <tr>
                  <td><a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=no,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_103_1/hw_103_1"><img alt="" src="uploads/higherway/Web_HW 103-1_cover.jpg" border="0" width="130" height="168"></a></td>
                  <td><a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_102_4/hw_102_4"><img alt="" src="uploads/higherway/Web_HW 102-4_Cover.jpg" border="0" width="130" height="168"></a></td>
                  <td><a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_102_3/hw_102_3"><img alt="" src="uploads/higherway/Web_HW 102-3_cover.jpg" border="0" width="130" height="168"></a></td>
                </tr>
              </tbody>
            </table>-->
            <br />
            <p>Our flagship publication, the <em>Higher Way</em>, is a quarterly magazine designed to inspire Christian believers to a deeper walk with the Lord. Click <a href="index.php?p=higherway_archive">here</a> to view previous issues.</p>
            <p>Subscriptions to the <em>Higher Way</em> are free. You can begin a subscription&nbsp;by doing one of the&nbsp;following:</p>
            <table sizset="47" sizcache="0" border="0" cellpadding="3" cellspacing="1" width="410">
              <tbody sizset="47" sizcache="0">
                <tr sizset="47" sizcache="0">
                  <td sizset="47" sizcache="0" style="text-align: center;" align="center" valign="middle"><img alt="" src="uploads/higherway/rss.gif" border="0" width="16" height="16"></td>
                  <td sizset="48" sizcache="0" height="30">Add this <a href="http://apostolicfaith.org.gravitatehosting.com/Library/HigherWay/tabid/99/moduleid/501/RSS.aspx">feed</a> to your RSS&nbsp;account.</td>
                </tr>
                <tr sizset="49" sizcache="0">
                  <td sizset="49" sizcache="0" style="text-align: center;" align="center" valign="middle"><img alt="" src="uploads/higherway/blue_email_icon.png" border="0" width="20" height="15"></td>
                  <td sizset="50" sizcache="0" height="30"><a href="index.php?p=higherway_email">Email</a> us&nbsp;to receive&nbsp;a 2-year&nbsp;subscription of&nbsp;the printed version.</td>
                </tr>
                <tr sizset="51" sizcache="0">
                  <td sizset="51" sizcache="0" style="text-align: left;" align="center" valign="middle"><img alt="" src="uploads/higherway/order.png" border="0" width="24" height="20"></td>
                  <td sizset="52" sizcache="0" height="34">Order a 2-year subscription of the printed version&nbsp;through&nbsp;our <a href="index.php?p=cd_list&type=HM&id=5"><br>
                    online store</a>.</td>
                </tr>
              </tbody>
            </table></td>
        </tr>
      </table></td>
  </tr>
</table>
