<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	$(".btn-slide").click(function(){
		$("#panel").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
	
	 
});
</script>	
<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel-3.0.2.pack.js"></script>
<script type="text/javascript" src="js/jquery.fancybox-1.3.0.pack.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.0.css" media="screen" />
<script type="text/javascript">
		$(document).ready(function() {
			$(".example2").fancybox({
				'titleShow'     : true,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
			});
	
     });
	</script>
<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
<table width="717" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="28" align="left" valign="top"><h3>Photo Gallery</h3></td>
                    </tr>
                  <tr>
                    <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  <tr>
                    <td align="left" valign="top"><div id="Accordion1" class="Accordion" tabindex="0">
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">2010 Easter Concert Gallery</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="gallery_pic/g_t1.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="gallery_pic/g_t2.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="gallery_pic/g_t3.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="gallery_pic/g_t4.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="gallery_pic/g_t5.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="gallery_pic/g_t6.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="gallery_pic/g_t7.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table>
</div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 1</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b9.jpg" title=""><img src="gallery_pic/g_t9.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b10.jpg" title=""><img src="gallery_pic/g_t10.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b11.jpg" title=""><img src="gallery_pic/g_t11.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b12.jpg" title=""><img src="gallery_pic/g_t12.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b9.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b10.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b11.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b12.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b13.jpg" title=""><img src="gallery_pic/g_t13.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b14.jpg" title=""><img src="gallery_pic/g_t14.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b15.jpg" title=""><img src="gallery_pic/g_t15.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 2</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="gallery_pic/g_t1.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="gallery_pic/g_t2.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="gallery_pic/g_t3.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="gallery_pic/g_t4.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="gallery_pic/g_t5.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="gallery_pic/g_t6.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="gallery_pic/g_t7.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 3</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b9.jpg" title=""><img src="gallery_pic/g_t9.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b10.jpg" title=""><img src="gallery_pic/g_t10.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b11.jpg" title=""><img src="gallery_pic/g_t11.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b12.jpg" title=""><img src="gallery_pic/g_t12.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b9.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b10.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b11.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b12.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b13.jpg" title=""><img src="gallery_pic/g_t13.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b14.jpg" title=""><img src="gallery_pic/g_t14.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b15.jpg" title=""><img src="gallery_pic/g_t15.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 4</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="gallery_pic/g_t1.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="gallery_pic/g_t2.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="gallery_pic/g_t3.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="gallery_pic/g_t4.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="gallery_pic/g_t5.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="gallery_pic/g_t6.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="gallery_pic/g_t7.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 5</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="gallery_pic/g_t1.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="gallery_pic/g_t2.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="gallery_pic/g_t3.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="gallery_pic/g_t4.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="gallery_pic/g_t5.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="gallery_pic/g_t6.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="gallery_pic/g_t7.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                    </div></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">&nbsp;</td>
                  </tr>
                </table><script type="text/javascript">
<!--
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
//-->
</script>
