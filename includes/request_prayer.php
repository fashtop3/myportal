<script type="text/javascript">
function request_prayer(frm){	
}
function checkCountyOther(){
	var p = document.getElementById('Place').value;
	if(p=="Other"){
		document.getElementById('otherPlace').style.display = 'block';
		document.getElementById('otherPlace').innerHTML = '<input type="text" name="Other" id="Other" class="gray_bdr" size="30" />';
	}else if(document.getElementById('otherPlace').style.display=='block'){
		document.getElementById('otherPlace').style.display = 'none';
	}
}
</script>
<script>
  $(document).ready(function(){
 
	$("#frmAdmin").validate({
		rules: {
			Email: {required: true,email:true},	
			Name: "required",
			Type: "required",
			Prayer: {required: true, maxlength:250}
		},
		messages: {
			Email: { required: "Please enter email address",email: "Please enter valid email address"},
			Place: "Please select place",
			Name: "Please enter name",
			Type: "Please select prayer type",
			Prayer: {required: "Please enter prayer", maxlength: "Maximum 250 characters are allowed"}
		}
	});	
  });
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Request Prayer</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top"><form name="frmAdmin" id="frmAdmin" method="post" action="manage_prayer.php">
              <table width="100%" cellpadding="5" cellspacing="0">
                <tr>
                  <td colspan="2" align="" class="blue_12"><?php showMessage(); ?></td>
                </tr>
                <tr>
                  <td class="" align="left" valign="top"><strong>Name</strong>: </td>
                  <td align="left" valign="top"><input type="text" name="Name" id="Name" class="gray_bdr" size="30" />
                    <input type="hidden" name="url" value="" />
                  </td>
                </tr>
                <tr>
                  <td class="" align="left" valign="top"><strong>Email</strong>: </td>
                  <td align="left" valign="top"><input type="text" name="Email" id="Email" class="gray_bdr" size="30" />
                    <input type="hidden" value="<?php echo $userplc;?>" name="Place" id="Place" />
                  </td>
                </tr>                
                <tr>
                  <td class="" align="left" valign="top"><strong>Type</strong>: </td>
                  <td align="left" valign="top">
                  	<select name="Type" id="Type" class="gray_bdr">
                    	<option value="">--Select--</option>
                      <option value="Request">Request</option>
                      <option value="Thanksgiving">Thanksgiving</option>
                    </select>
                  </td>
                </tr>
                <tr>
                  <td class="" align="left" valign="top"><strong>Prayer</strong>: </td>
                  <td align="left" valign="top"><textarea name="Prayer" id="Prayer" cols="50" rows="7" class="gray_bdr"></textarea></td>
                </tr>
                <tr>
                  <td>&nbsp;</td>
                  <td><input type="submit" class="search_bg" name="Submit" value="Submit" /></td>
                </tr>
              </table>
            </form></td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
