<script type="text/javascript">
$(document).ready(function(){  		
	$("#frmCamp").validate();
});
</script>
<?php
unset($_SESSION['Desc']);
unset($_SESSION['Name']);
unset($_SESSION['id1']);
?>
<?php 

if($_REQUEST['RefNo']!=''){
	$refn = FetchData("camp_user",array(),"where status=1 and booking_ref_no='".$_REQUEST['RefNo']."'");
}

if($_REQUEST['Camp']!='')
	$id = $_REQUEST['Camp'];
else
	$id = GetFieldData("donation","id","where status=1 and type='Camp Meeting' order by id");
?>
<?php $rm = $refn[0]['room'];
$rms = @explode(',',$rm);
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Booking and payment for camp meeting</h3></td>
  </tr>
  <tr>
  	<td align="left" valign="top"><form name="frmCamp" id="frmCamp" action="" method="post">Camp Meeting    	
    	<select onchange="form.submit();" name="Camp" id="Camp" class="field_bdr" style="width:150px; height:20px;">
      	<?php echo FillCombo1("donation","title","id",$id,"where type='Camp Meeting' and status=1");?>
    	</select><br /><br />
      Already Registered User for the Current Camp Metting Please Click <a href="javascript:void(0)" onclick="document.getElementById('RefNo').style.display='';">here</a>
      <span id="RefNo" style=" <?php if($_REQUEST['RefNo']=='') echo "display:none;";?>"><br />
      <?php if($_SESSION['USERID']!=''){ ?>
      Select bookling reference number:
      <select name="RefNo" style="width:150px; height:20px;" class="field_bdr" title=" *" validate="required:true">
      	<option value="">--Select--</option>
        <?php echo FillCombo1("camp_user","booking_ref_no","booking_ref_no",$_REQUEST['RefNo'],"where user_id='".$_SESSION['USERID']."'");?>
      </select>
      <?php } else { ?>
      Enter your booking reference number:  
      <input type="text" title=" *" validate="required:true" class="field_bdr" name="RefNo" style="width:150px; height:20px;" />
      <?php } ?>
      <input type="submit" style="cursor:pointer" class="search_bg" value="Go" />
      <?php if(count($refn)>0){ ?>
      <br /><br />
      <table width="100%" cellpadding="5" cellspacing="1" bgcolor="#CCCCCC" border="0">
      	<tr bgcolor="#F1F1F1">
        	<td colspan="2" align="left" valign="top"><b style="color:#1370b0;"><?php echo stripslashes($refn[0]['description']);?></b>
          <span style="float:right;">
          <input type="button" style="cursor:pointer; height:20px;" onclick="window.location='index.php?p=contact_camp&id=<?php echo $refn[0]['id']?>'" class="search_bg" value="Contact Camp Team" />
					<?php if($refn[0]['type']!='Overseas Family'){?>
          <input type="button" style="cursor:pointer; height:20px;" onclick="window.location='index.php?p=edit_camp&id=<?php echo $refn[0]['id']?>'" class="search_bg" value="Update Booking" />
          <?php } ?>
          <img src="images/close.gif" style="cursor:pointer;" width="20" height="20" align="absmiddle" alt="" onclick="window.location='index.php?p=online_giving'" />
          </span>
          </td>
        </tr>
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Booking Ref No: </b></td>
          <td align="left" valign="top"><?php echo $_REQUEST['RefNo']?></td>
        </tr>        
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Name: </b></td>
          <td align="left" valign="top"><?php echo stripslashes($refn[0]['name']);?></td>
        </tr>
        <!--<tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Email: </b></td>
          <td align="left" valign="top"><?php echo $refn[0]['email'];?></td>
        </tr>-->
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Phone: </b></td>
          <td align="left" valign="top"><?php echo stripslashes($refn[0]['phone']);?></td>
        </tr>
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Mobile: </b></td>
          <td align="left" valign="top"><?php echo stripslashes($refn[0]['mobile']);?></td>
        </tr>
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Address: </b></td>
          <td align="left" valign="top"><?php echo stripslashes($refn[0]['address']);?></td>
        </tr>
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Country: </b></td>
          <td align="left" valign="top"><?php echo stripslashes($refn[0]['country']);?></td>
        </tr>
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Postcode: </b></td>
          <td align="left" valign="top"><?php echo stripslashes($refn[0]['postcode']);?></td>
        </tr>
        <?php if($refn[0]['type']=='Overseas Family'){?>
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>List of person who wants to come: </b></td>
          <td align="left" valign="top"><?php $per = @explode(', ',$refn[0]['persons']);
					?>
          <table width="100%" cellpadding="5" cellspacing="1" border="0">
          	<tr>
              <td width="5%" align="left" valign="top"><b>No</b></td>
              <td width="65%" align="left" valign="top"><b>Full Name</b></td>
              <td align="left" valign="top"><b>Room No</b></td>
            </tr>
            <?php for($j=0;$j<count($per);$j++){ ?>
            <tr>
              <td align="left" width="5%" valign="top"><?php echo $j+1?></td>
              <td align="left" width="20%" valign="top"><?php echo $per[$j]?></td>
              <td align="left" valign="top"><b><?php echo $rms[$j]?></b>
              </td>
            </tr>
            <?php } ?>
          </table>
          </td>
        </tr>
        <?php } ?>
        <?php if($refn[0]['type']=='OVERSEAS DELEGATES'){?>
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Payment Detail: </b></td>
          <td align="left" valign="top">
          	<?php $pay = FetchData("camp_payment",array(),"where user_id='".$refn[0]['id']."'"); ?>
            <table width="100%" cellpadding="5" cellspacing="0" border="0">
            	<tr>
              	<td width="30%" align="left" valign="top"><b>Payment type:</b></td>
                <td align="left" valign="top"><?php echo stripslashes($pay[0]['payment_type'])?></td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Total price:</b></td>
                <td align="left" valign="top">&pound;<?php echo stripslashes($pay[0]['total_price'])?></td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Payment done till now:</b></td>
                <td align="left" valign="top">&pound;<?php if(stripslashes($pay[0]['payment_submit'])=='') $done = '0'; else $done = stripslashes($pay[0]['payment_submit']);echo $done;?></td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Pending payment:</b></td>
                <td align="left" valign="top">&pound;<?php echo $pay[0]['pending_payment']?></td>
              </tr>
              <?php 							
								if($pay[0]['pending_payment']!='0' && $pay[0]['pending_payment']!=''){
							?>
              <tr>
              	<td width="30%" align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top"><input type="button" value="Pay Now" class="search_bg" style="cursor:pointer;" onclick="window.location='index.php?p=final_payment&id=<?php echo $refn[0]['id']?>'" /></td>
              </tr>
              <?php 								
							}
							?>
            </table>
          </td>
        </tr>
        <tr bgcolor="#F1F1F1">
        	<td colspan="2" align="left" valign="top"><br />List of person who are coming:<br /><?php if($refn[0]['id']!=''){
						$uid = $refn[0]['id'];
						$prc = GetFieldData("camp_payment","total_price","where user_id=".$refn[0]['id']);
						$user = FetchData("camp_user",array(),"where id=".$uid);
						$person = '<table width="100%" cellpadding="5" cellspacing="0" border="0">';
						$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
						$k=0;
						for($i=0;$i<count($age);$i++){
							$person .= '<tr>';
							$person .= '<td colspan="7" align="left" valign="top"><hr><b>';
							$person .= stripslashes(GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'"));
							$person .= '</b><hr></td>';
							$person .= '</tr>';
							$person .= '<tr>';
							$person .= '<td width="5%" align="left" valign="top"><b>Room No</b></td>';
							$person .= '<td align="left" valign="top"><b>Name</b></td>';
							$person .= '<td align="left" valign="top"><b>Country</b></td>';
							$person .= '<td align="left" valign="top"><b>Required Visa?</b></td>';
							$person .= '<td align="left" valign="top"><b>Coming Date</b></td>';
							$person .= '<td align="left" valign="top"><b>Passport No.</b></td>';
							$person .= '<td align="left" valign="top"><b>Price</b></td>';
							$person .= '</tr>';
							$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
							for($j=0;$j<count($per);$j++){
								$person .= '<tr>';
								$person .= '<td align="left" valign="top"><b>'.$rms[$k].'</b></td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['name']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['station']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['visa']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['coming_date']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['passport_no']).'</td>';
								$person .= '<td align="left" valign="top">&pound;';
								if($age[$i]['price']>0)
									$person .= $age[$i]['price']/$age[$i]['person'];
								else
									$person .= $age[$i]['price'];
								$person .= '</td>';
								$person .= '</tr>';
								$k++;
							}
						}
						$person .= "</table>";
					}
					echo $person;
					?></td>
        </tr>        
        <?php } ?>
        <?php if($refn[0]['type']=='UK DELEGATES'){?>
        <?php $pay = FetchData("camp_payment",array(),"where user_id='".$refn[0]['id']."'"); ?>
        <tr bgcolor="#F1F1F1">
        	<td width="25%" align="left" valign="top"><b>Payment Detail: </b></td>
          <td align="left" valign="top">          	
            <table width="100%" cellpadding="5" cellspacing="0" border="0">
            	<tr>
              	<td width="30%" align="left" valign="top"><b>Payment type:</b></td>
                <td align="left" valign="top"><?php echo stripslashes($pay[0]['payment_type'])?></td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Total price:</b></td>
                <td align="left" valign="top">&pound;<?php echo stripslashes($pay[0]['total_price'])?></td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Payment done till now:</b></td>
                <td align="left" valign="top">&pound;<?php if(stripslashes($pay[0]['payment_submit'])=='') $done = '0'; else $done = stripslashes($pay[0]['payment_submit']);echo $done;?></td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Pending payment:</b></td>
                <td align="left" valign="top">&pound;<?php echo stripslashes($pay[0]['pending_payment'])?></td>
              </tr>
              <?php 
							if($pay[0]['pending_payment']!='0' && $pay[0]['pending_payment']!=''){
							?>
              <tr>
              	<td width="30%" align="left" valign="top">&nbsp;</td>
                <td align="left" valign="top"><input type="button" value="Pay Now" class="search_bg" style="cursor:pointer;" onclick="window.location='index.php?p=final_payment&id=<?php echo $refn[0]['id']?>'" /></td>
              </tr>
              <?php 								
							}
							?>
            </table>
          </td>
        </tr>
        <tr bgcolor="#F1F1F1">
        	<td colspan="2" align="left" valign="top"><br />List of person who are coming:<br /><?php 					
					if($refn[0]['id']!=''){
						$uid = $refn[0]['id'];
						$prc = GetFieldData("camp_payment","total_price","where user_id=".$refn[0]['id']);
						$user = FetchData("camp_user",array(),"where id=".$uid);
						$person = '<table width="100%" cellpadding="5" cellspacing="0" border="0">';
						$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
						$k=0;
						for($i=0;$i<count($age);$i++){
							$person .= '<tr>';
							$person .= '<td colspan="4" align="left" valign="top"><hr><b>';
							$person .= stripslashes(GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'"));
							$person .= '</b><hr></td>';
							$person .= '</tr>';
							$person .= '<tr>';
							$person .= '<td width="5%" align="left" valign="top"><b>Room No</b></td>';
							$person .= '<td align="left" valign="top"><b>Name</b></td>';
							$person .= '<td align="left" valign="top"><b>Station</b></td>';
							$person .= '<td align="left" valign="top"><b>Price</b></td>';
							$person .= '</tr>';
							$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
							for($j=0;$j<count($per);$j++){
								$person .= '<tr>';
								$person .= '<td align="left" valign="top"><b>'.$rms[$k].'</b></td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['name']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['station']).'</td>';
								$person .= '<td align="left" valign="top">&pound;';
								if($age[$i]['price']>0)
									$person .= $age[$i]['price']/$age[$i]['person'];
								else
									$person .= $age[$i]['price'];
								$person .= '</td>';
								$person .= '</tr>';
								$k++;
							}
						}
						$person .= "</table>";
					}
					echo $person;
					
					?></td>
        </tr>  
        <?php } ?>        
        <?php
				if($pay[0]['past_payment']!=''){
				?>
        <tr bgcolor="#F1F1F1">
        	<td colspan="2" align="left" valign="top"><br /><b style="color:#1370b0;">Your payment history for this booking reference number:</b><br /><br />
  					<table bgcolor="#acd2ed" width="100%" cellpadding="5" cellspacing="1" border="0">        
				<?php $past = @explode(',',$pay[0]['past_payment']);
          for($i=0;$i<count($past);$i++){
            echo '<tr bgcolor="#f3f8fc"><td align="left" valign="top">'.($i+1).'</td><td align="left" valign="top">';
            echo $past[$i]."</td></tr>";
          }
        ?>   </table>
	        </td>
        </tr>       
        <?php } ?>        
      </table>      
			<?php }else if($_REQUEST['RefNo']!='' && count($refn)==0){ echo "<br><font color='red'>Invalid Booking Reference Number</font>";}?>  
      </span>
      </form>
    </td>
  </tr>
  <?php if($_REQUEST['RefNo']==''){ ?>
  <tr>
  	<td align="left" valign="top">&nbsp;</td>
  </tr>  
  <tr>
    <td align="left" valign="top"><?php showMessage();?><table width="100%" border="0" align="left" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" align="left" valign="top"><b>CHARGES FOR UK DELEGATES</b></td>
        </tr>
        <?php $uk = FetchData("camp_meeting",array(),"where category='UK DELEGATES' and donation_id='".$id."' order by price desc");?>
        <?php for($i=0;$i<count($uk);$i++){ ?>
        <tr>
          <td style="font-size:13px;" width="30%" align="left" valign="top"><?php echo stripslashes($uk[$i]['title'])?></td>
          <td style="font-size:13px;" align="left" valign="top">£<?php echo stripslashes($uk[$i]['price'])?></td>
        </tr>
        <?php } ?>
        <tr>
        	<td align="left" valign="middle" colspan="2" height="30">
          	<input type="button" name="" value="Register Now" style="cursor:pointer;" onclick="window.location='index.php?p=camp_uk&id=<?php echo $id?>'" class="search_bg" />
          </td>
        </tr>	        
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top"><hr /></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="5" cellspacing="0">
        <tr>
          <td colspan="2" align="left" valign="top"><b>CHARGES FOR OVERSEAS DELEGATES</b></td>
        </tr>        
        <?php $uk = FetchData("camp_meeting",array(),"where category='OVERSEAS DELEGATES' and donation_id='".$id."' order by price desc");?>
        <?php for($i=0;$i<count($uk);$i++){ ?>
        <tr>
          <td style="font-size:13px;" width="30%" align="left" valign="top"><?php echo stripslashes($uk[$i]['title'])?></td>
          <td style="font-size:13px;" align="left" valign="top">£<?php echo stripslashes($uk[$i]['price'])?></td>
        </tr>
        <?php } ?>
        <tr>
        	<td align="left" valign="middle" colspan="2" height="30">
          	<input type="button" name="" value="Register Now" style="cursor:pointer;" onclick="window.location='index.php?p=camp_over&id=<?php echo $id?>'" class="search_bg" />
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top"><hr /></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="5" cellspacing="0">
        <tr>
          <td style="font-size:13px;" colspan="2" align="left" valign="top">Family Discount Available for overseas deligates</td>
        </tr>        
        <tr>
          <td style="font-size:13px;" align="left" colspan="2" valign="top">Please contact the UK Camp office</td>
        </tr>
        <tr>
        	<td align="left" valign="middle" colspan="2" height="30">
          	<input type="button" name="" value="Contact Church" style="cursor:pointer;" onclick="window.location='index.php?p=family_contact&id=<?php echo $id?>'" class="search_bg" />
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top"><hr /></td>
  </tr>
  <?php } ?>
</table>
