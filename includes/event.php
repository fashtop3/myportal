<?php

$startDate = "2016-12-01"; //date("Y-m-01");
$endDate = date("Y-m-t", strtotime($startDate." + 3 months"));

$sqlEvent = " and (event_date between '".$startDate."' and '".$endDate."')";// and event_date >= '".date("Y-m-d")."'";

$sql = "select * from event where place_id = '".$userplc."' ".$sqlEvent." order by event_date";

//$sql = "select * from event where (MONTH(event_date) = ".date('m')." or MONTH(to_date) = ".date('m').") and (event_date >= '".date('Y-m-d')."' or to_date >= '".date('Y-m-d')."') and place_id = ".$userplc;
$event = $objDB->select($sql);
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="28" align="left" valign="top"><h3>Upcoming Events</h3></td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
				<tr>
					<td align="left" valign="top">&nbsp;</td>
				</tr>
				<?php
				$prevMonthYear = '';
				if(count($event) > 0){
					 for($i=0;$i<count($event);$i++) {
						$monthYear = date('F Y', strtotime($event[$i]['event_date']));
				?>
					<tr>
						<td width="75%">
							<img src="images/arrow_001.gif" alt="" />
							<?php echo stripslashes($event[$i]['title']);echo ", ".stripslashes($event[$i]['address']); ?>
						</td>
						<th align="right">
							<?php 			
							echo date('d F Y',strtotime($event[$i]['event_date']));
							if($event[$i]['to_date']!='0000-00-00' && $event[$i]['event_date'] < $event[$i]['to_date']){
								echo " <b>-</b> ".date('d F Y',strtotime($event[$i]['to_date']));
							}
							?>
						</th>
					</tr>
					<tr>
						<td height="20" colspan="2" align="left" valign="top"
							class="dot_line">&nbsp;</td>
					</tr>
				<?php
					}
				 }
				 else
				 {
				 	echo '<tr>
					<td align="left" valign="top">No upcoming events available.</td>
				</tr><tr>
					<td align="left" valign="top">&nbsp;</td>
				</tr>';
				 }
				 ?>
				<tr>
					<td style="padding: 5px;">Click on the <a
						href="index.php?p=event_archive" class="blue_link">Archive Events</a>
						to see past events.
					</td>
				</tr>
			</table></td>
	</tr>
	<tr>
		<td align="left" valign="top">&nbsp;</td>
	</tr>
</table>
