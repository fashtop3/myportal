<style type="text/css">
p {
	padding:3px;
}
h5 {
	font-size:14px;
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Devotional</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top"><?php
					$arc = FetchData('devotional_archive',array(),'where YEAR(ddate) = "'.date('Y').'" group by ddate');
					$total = count($arc);
					$arc = FetchData('devotional_archive',array(),'group by ddate order by ddate desc');
				?>
            <table width="100%" cellpadding="5" cellspacing="1">
              <tr>
                <td colspan="2" valign="middle"><b>Devotional for <?php echo date("d/m/Y",strtotime($arc[0]['ddate']))?></b>                  
                </td>
              </tr>
              <tr>
                <td colspan="2" style="font-size:20px; color:#024770; font-family:'Times New Roman', Times, serif; "><i> <?php echo str_replace("`","'",stripslashes($arc[0]['title']));?> </i></td>
              </tr>
              <tr>
                <td colspan="2" align="left"><div class="divo_box">
                    <div class="divo_c1">
                      <div class="divo_c2" style=" padding-top:35px; font-size: 20px; font-family: 'Times New Roman',Times,serif; color: #2485BD;"><?php echo str_replace('`','"',str_replace('<h3','<h2',str_replace('</h3>',"</h2>",str_replace("<br />",'"<br />',stripslashes($arc[0]['description'])))));?> </div>
                    </div>
                  </div></td>
              </tr>
              <tr>
                <td colspan="2">                
								<?php 
								$str = str_replace("`","'",str_replace('<h3','<h2',str_replace('</h3>',"</h2>",str_replace("`","'",stripslashes($arc[0]['full_description'])))));
								$str = str_replace("<div>","",$str);
								$str = str_replace("</div>","",$str);
								$str = strip_tags($str,"<p><h2><img><a><strong><em><b><span><table><tr><td><tbody><th>");
								echo $str;
								?></td>
              </tr>
              <tr>
                <td colspan="2"><h2>Devotional Archive</h2>
                  <br />
                  <a class="blue_link" href="index.php?p=devotional_archive"><?php echo date('Y')." (".$total.")";?></a> </td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
