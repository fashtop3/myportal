<style type="text/css">
.sort_bg {
	background: url('images/bg.gif') repeat-x scroll center bottom #D2D2D2;
	padding-left: 5px;
	vertical-align:middle;
	height:28px;
	font-family:Georgia, "Times New Roman", Times, serif;
	font-size:12px;
	font-weight:bold;
	color:#666666;
	white-space:normal;
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Order -
        <?php if($_REQUEST['type']=='CD') echo "CDs &amp; MP3s"; else if($_REQUEST['type']=='DVD') echo "DVDs"; else if($_REQUEST['type']=='HM') echo "Higher Way Magazine"; else if($_REQUEST['type']=='MT') echo "Music &amp; Tracks"; ?>
      </h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top" style="padding:10px;"><a href="index.php?p=cd_list&type=<?php echo $_REQUEST['type']?>" class="blue_link">
            <?php if($_REQUEST['type']=='CD') echo "CDs &amp; MP3s"; else if($_REQUEST['type']=='DVD') echo "DVDs"; else if($_REQUEST['type']=='HM') echo "Higher Way Magazine"; else if($_REQUEST['type']=='MT') echo "Music &amp; Tracks"; ?>
            </a>
            <?php if($_REQUEST['id']!='') echo " &nbsp; <img src='images/arrow_006.gif' alt='Arrow' />&nbsp; ".stripslashes(FetchValue('cd_category','title','id',$_REQUEST['id']));?></td>
        </tr>
        <tr>
          <td align="left" valign="top"><table width="97%" align="center" border="0" cellpadding="5" cellspacing="1">
              <tr>
                <td class="sort_bg" width="100%"><form name="frmSort" id="frmSort" action="" method="post">
                    Sort&nbsp; By:&nbsp;&nbsp;&nbsp;
                    <select style="border:1px solid #666666; width:150px; font-size:10px;" name="Sort" id="Sort" class="InputBox">
                      <option <?php if($_REQUEST['Sort']=='order by title asc') echo "selected";?> value="order by title asc">Name: A to Z</option>
                      <option <?php if($_REQUEST['Sort']=='order by title desc') echo "selected";?> value="order by title desc">Name: Z to A</option>
                      <option <?php if($_REQUEST['Sort']=='order by price asc') echo "selected";?> value="order by price asc">Price: Low to High</option>
                      <option <?php if($_REQUEST['Sort']=='order by price desc') echo "selected";?> value="order by price desc">Price: High to Low</option>
                      <option <?php if($_REQUEST['Sort']=='order by item_no asc') echo "selected";?> value="order by item_no asc">Item Code: A to Z</option>
                      <option <?php if($_REQUEST['Sort']=='order by item_no desc') echo "selected";?> value="order by item_no desc">Item Code: Z to A</option>
                      <option <?php if($_REQUEST['Sort']=='order by author asc') echo "selected";?> value="order by author asc">Autohr: A to Z</option>
                      <option <?php if($_REQUEST['Sort']=='order by author desc') echo "selected";?> value="order by author desc">Author: Z to A</option>
                      <option <?php if($_REQUEST['Sort']=='order by publisher asc') echo "selected";?> value="order by publisher asc">Publisher: A to Z</option>
                      <option <?php if($_REQUEST['Sort']=='order by publisher desc') echo "selected";?> value="order by publisher desc">Publisher: Z to A</option>
                      <option <?php if($_REQUEST['Sort']=='order by created asc') echo "selected";?> value="order by created asc">Date Created ASC</option>
                      <option <?php if($_REQUEST['Sort']=='order by created desc') echo "selected";?> value="order by created desc">Date Created DESC</option>
                    </select>
                    <input type="image" src="images/vedio_ic.gif" align="absmiddle" />
                  </form></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="right" style="padding:5px;"><?php
				if($_REQUEST['id']!=''){
					$where = "where type='".$_REQUEST['type']."' and category_id = ".$_REQUEST['id'];
				}
				else{
					$where = "where type='".$_REQUEST['type']."'";
				}
				if($_REQUEST['Sort']!='')
					$ord = $_REQUEST['Sort'];
				else
					$ord = "order by title asc";
				$sql = "select * from cd ".$where." ".$ord;
				$objPaging = new paging($sql, "id",3);
				$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
				$cd = $objDB->select($objPaging->get_query());				
			?>
          </td>
        </tr>
        <tr>
          <td><table width="98%" align="center" border="0" cellpadding="4" cellspacing="0">
              <tr>
                <td align="right" colspan="2">Page:
                  <?php $objPaging->show_paging(); ?></td>
              </tr>
              <tr>
                <td height="20" colspan="2" align="left" valign="top" class="dot_line">&nbsp;</td>
              </tr>
              <?php for($i=0;$i<count($cd);$i++){ ?>
              <tr><td align="left" valign="top"><table width="100%">
              <tr>
                <td><font size="3"><i> <a style="font-size:14px;color:#003366;" href="index.php?p=cd&id=<?php echo $cd[$i]['id']?>&type=<?php echo $_REQUEST['type']?>">"<?php echo strip_tags(stripslashes($cd[$i]['title']))?>"</a></i></font> </td>
              </tr>
              <tr>
                <td>Item Number: <?php echo stripslashes($cd[$i]['item_no'])?></td>
              </tr>
              <tr>
                <td>Author: <?php echo stripslashes($cd[$i]['author'])?></td>
              </tr>
              <tr>
                <td>Publisher: <?php echo stripslashes($cd[$i]['publisher'])?></td>
              </tr>
              <tr>
                <td>Price: <strong style="color:#000000"><?php echo getCurrencySign(CURRENCY_SIGN).stripslashes($cd[$i]['price'])?></strong></td>
              </tr>
              <tr>
                <td><script>
						$(document).ready(function(){
							$("#frmSort<?php echo $i?>").validate();
						});
						</script>
                  <form action="manage_cd_cart.php" method="post" name="frmSort<?php echo $i?>" id="frmSort<?php echo $i?>">
                    <input type="hidden" name="Product" value="<?php echo $_REQUEST['type']?>" />
                    <input type="hidden" name="id<?php echo $i ?>" value="<?php echo $cd[$i]['id']?>" />
                    <input type="hidden" name="action" value="add" />
                    <input type="hidden" name="total" value="<?php echo count($cd);?>" />
                    <input type="hidden" name="price<?php echo $i ?>" value="<?php echo stripslashes($cd[$i]['price']);?>" />
                    <input type="hidden" value="<?php echo $_SESSION['PlaceID'.$userplc];?>" name="Place" id="Place" />
                    Qty:
                    <input type="text" name="Qty<?php echo $i?>" size="3" class="gray_bdr" title=" *" value="1" validate="required:true,number:true" />
                    <br />
                    <br />
                    <input type="submit" value="Add To Cart" class="search_bg" style="height:25px;" onmouseover="this.style.cursor='pointer'" />
                  </form></td>
              </tr>
              </table></td>
              <?php if($_REQUEST['type']=='HM'){ ?><td align="right" valign="top">
                <img src="uploads/contact/big/<?php echo stripslashes($cd[$i]['image']);?>" width="125" height="155" /></td>
              <?php } ?>
              <tr>
                <td height="20" align="left" valign="top" colspan="2" class="dot_line">&nbsp;</td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="2" align="right">Page:
                  <?php $objPaging->show_paging(); ?></td>
              </tr>
            </table></td>            
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
