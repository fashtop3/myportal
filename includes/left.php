<?php
$menu = FetchData("menu",array(),"");
?>

<table width="220" border="0" align="right" cellpadding="0" cellspacing="0">
    <tr>
        <td align="left" valign="top"><div id="nav">
                <div id="mainnav">
                    <div id="home" class="careers"> <a href="index.php"><?php echo stripslashes($menu[0]['title'])?></a> </div>
                    <div id="aboutus" class="public"> <a href="index.php?p=cms&id=about_us"><?php echo stripslashes($menu[1]['title'])?></a> </div>
                    <!--<div id="camp" class="campaign"> <a href="index.php?p=camp_meeting"><?php echo stripslashes($menu[2]['title'])?></a> </div>-->
                    <div id="resource" class="complaints"> <a href="index.php?p=school"><?php echo stripslashes($menu[3]['title'])?></a>
                        <ul id="resource_menu">
                            <li> <a href="index.php?p=school" id="school" class="sub_menu_link">Sunday School</a> </li>
                            <li> <a href="index.php?p=curriculum_list" id="curriculum" class="sub_menu_link">Curriculum</a> </li>
                            <li class="open"> <a href="index.php?p=sermon_list" id="sermon" class="sub_menu_link">Sermons</a>
                                <ul style="padding-left:5px;" id="sermon_submenu">
                                    <li><a href="index.php?p=sermon_list" id="sermonlist" class="sub_menu_link1">&raquo; General sermons</a></li>
                                    <li><a href="index.php?p=past_sermon_list" id="pastsermon" class="sub_menu_link1">&raquo; Echoes from the past</a></li>
                                </ul>
                            </li>
                            <li> <a href="index.php?p=tract_list" id="tract" class="sub_menu_link">Featured Tract</a> </li>
                            <li> <a href="index.php?p=devotional" id="devotional" class="sub_menu_link">Devotional</a> </li>
                            <li> <a href="index.php?p=higherway" id="higherway" class="sub_menu_link">Higher Way</a> </li>
                            <li> <a href="index.php?p=offering" id="give" class="sub_menu_link">Online Payment</a> </li>
                            <?php $cat = FetchData("cd_category",array(),"group by type"); ?>
                            <li class="open"> <a class="sub_menu_link" id="multimedia" href="index.php?p=cd_list&type=<?php echo stripslashes($cat[0]['type'])?>">Multimedia</a>
                                <ul style="padding-left:5px;" id="multimedia_submenu">
                                    <?php
										for($i=0;$i<count($cat);$i++){ 
											$sub = FetchData("cd_category",array(),"where type = '".stripslashes($cat[$i]['type'])."'");
									?>
                                    <li class="open"><a href="index.php?p=cd_list&type=<?php echo stripslashes($cat[$i]['type'])?>" id="<?php echo $cat[$i]['type']?>" class="sub_menu_link1">&raquo; <?php echo str_replace('MT','Music &amp; Tracks',str_replace('HM','Higher Way Magazine',stripslashes($cat[$i]['type'])))?></a>
                                        <?php if(count($sub)>0){ ?>
                                        <ul style="padding-left:17px;" id="library_submenu">
                                            <?php for($j=0;$j<count($sub);$j++){ ?>
                                            <li><a id="<?php echo $sub[$j]['id']?>" href="index.php?p=cd_list&type=<?php echo stripslashes($cat[$i]['type'])?>&id=<?php echo $sub[$j]['id']?>" class="sub_menu_link2"><?php echo stripslashes($sub[$j]['title'])?></a></li>
                                            <?php } ?>
                                        </ul>
                                        <?php } ?>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="whatwecover" id="news"> <a href="index.php?p=news"><?php echo stripslashes($menu[4]['title'])?></a>
                        <ul id="news_menu">
                            <li><a href="index.php?p=news" id="newslink" class="sub_menu_link">News</a></li>
                            <li><a id="calendars" href="index.php?p=calendar" class="sub_menu_link">Calendar</a></li>
                            <li><a href="index.php?p=event" id="event" class="sub_menu_link">Events</a></li>
                            <li><a href="index.php?p=gallery" id="gallery" class="sub_menu_link">Photo Gallery</a></li>
                        </ul>
                    </div>
                    <div class="media" id="outreach"> <a href="index.php?p=outreach"><?php echo stripslashes($menu[5]['title'])?></a> </div>
                    <div class="organisations"> <a href="<?php echo YOUTH_LINK?>" target="_blank" ><?php echo stripslashes($menu[6]['title'])?></a> </div>
                    <div class="tools" id="contact"> <a href="index.php?p=contact"><?php echo stripslashes($menu[7]['title'])?></a>
                        <ul id="contact_menu">
                            <li><a href="index.php?p=request_prayer" id="prayer" class="sub_menu_link">Request Prayer</a></li>
                            <?php if($_SESSION['USERID']==''){ ?>
                            <?php /*?><li><a href="member/index.php?p=register" id="register" class="sub_menu_link">Register</a></li>
                            <li><a href="member/index.php" id="login" class="sub_menu_link">Login</a></li><?php */?>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div></td>
    </tr>
    <tr>
        <td height="10" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
    </tr>
    <?php /*?><tr>
        <td align="left" valign="top">
        	<style type="text/css">
				.devotional_table{
					margin-top:0px;
				}
			</style>
        	<table width="96%" class="devotional_table" border="0" align="right" cellpadding="0" cellspacing="0">
                <tr>
                    <td align="left" valign="top"><span class="times_italic" style="padding-left:10px;">                        						
						<?php											
						$arc = FetchData('devotional_archive',array(),'order by ddate desc');
						echo str_replace('`',"'",strip_tags(stripslashes($arc[0]['description']),'<div><p>'));//str_replace("- Jeremiah","<div align='right'>- Jeremiah",htmlentities(strip_tags(str_replace("<br />",'"',$arc[0]['description']))));echo "</div>";
							?>
                        </span></td>
                </tr>
                <tr>
                    <td align="right" valign="top"><a href="index.php?p=devotional" class="times_blue_link">Devotional</a></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td height="10" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
    </tr><?php */?>
    <!--<tr>
    	<td align="left" valign="top"><a href="memory_verse.php" onclick="return hs.htmlExpand(this, { objectType: 'ajax'} )"><img src="images/get_verse.jpg" alt="" width="220" height="51"></a></td>
  	</tr>
  	<tr>
    	<td align="left" valign="top">&nbsp;</td>
  	</tr>-->
    <tr>
        <td align="left" valign="top"><?php
			$sql = "select * from news where status='approved' and place_id = ".$userplc." and YEAR(created)=".date('Y') ." order by id desc limit 3";
			$news = $objDB->select($sql);			
			?>
            <table width="97%" align="right" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" class="btm_gray_bdr"><span class="gray_bg">News</span></td>
                </tr>
                <tr>
                    <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                </tr>
                <?php if(count($news)>0){ ?>
                <tr>
                    <td align="left" valign="top">
                    	<script type="text/javascript" src="js/jquery.cycle.all.2.72.js"></script>
                        <script type="text/javascript">
						$(function() {    		
								$('#slideshow').cycle({
										fx: "scrollUp", //'fade','curtainX','curtainY','toss','scrollUp','turnDown','scrollDown','scrollVert'
											//'scrollLeft','scrollHorz','scrollRight','shuffle','zoom','fadeZoom','wipe'
											// 'blindX', 'blindY', 'blindZ','growX','growY','cover','uncover','all'
								speed:       1000, //'fast'		
										//pager:  '#nav', 		
								timeout:     4000
							});			
						});
						</script>
                        <div id="slideshow">        
						<?php for($i=0;$i<count($news);$i++){ ?>
                        <div class="news_bullet"><span class="blue_12"><strong><a href="index.php?p=news_detail&id=<?php echo $news[$i]['id'];?>" class="blue_12" title="<?php echo stripslashes($news[$i]['title']);?>"><?php echo stripslashes($news[$i]['title']);?></a></strong></span><br />
                            <span class="orange_light"><?php echo date('d/m/Y',strtotime($news[$i]['created']))?></span><br>
                            <span class="news_content"><?php echo substr(strip_tags(stripslashes($news[$i]['short_description'])),0,125);?></span><br />
                            <span class="blue_12" style="text-decoration:none;">&raquo; <a href="index.php?p=news_detail&id=<?php echo $news[$i]['id'];?>" title="Read More">Read More</a></span> <br />
                            <br />
                        </div>
                        <?php } ?>     
				    	</div>
						<?php /*?><link rel="stylesheet" type="text/css" href="css/contentslider.css" />
                        <script type="text/javascript" src="js/contentslider.js"></script>
                        <div id="slider1" class="sliderwrapper">
                            <?php for($i=0;$i<count($news);$i++){ ?>
                            <div class="contentdiv"><span class="blue_12"><strong><a href="index.php?p=news_detail&id=<?php echo $news[$i]['id'];?>" class="blue_12" title="<?php echo stripslashes($news[$i]['title']);?>"><?php echo stripslashes($news[$i]['title']);?></a></strong></span><br />
                                <span class="orange_light"><?php echo date('d/m/Y',strtotime($news[$i]['created']))?></span><br>
                                <span class="news_content"><?php echo substr(strip_tags(stripslashes($news[$i]['short_description'])),0,125);?></span><br />
                                <span class="blue_12" style="text-decoration:none;">&raquo; <a href="index.php?p=news_detail&id=<?php echo $news[$i]['id'];?>" title="Read More">Read More</a></span> <br />
                                <br />
                            </div>
                            <?php } ?>
                        </div>
                        <div id="paginate-slider1" class="pagination"></div>
                        <script type="text/javascript">

						featuredcontentslider.init({
							id: "slider1",  //id of main slider DIV
							contentsource: ["inline", ""],  //Valid values: ["inline", ""] or ["ajax", "path_to_file"]
							toc: "#increment",  //Valid values: "#increment", "markup", ["label1", "label2", etc]
							nextprev: ["", ""],  //labels for "prev" and "next" links. Set to "" to hide.
							enablefade: [true, 0.1],  //[true/false, fadedegree]
							autorotate: [true, 5000],  //[true/false, pausetime]
							onChange: function(previndex, curindex){  //event handler fired whenever script changes slide
								//previndex holds index of last slide viewed b4 current (1=1st slide, 2nd=2nd etc)
								//curindex holds index of currently shown slide (1=1st slide, 2nd=2nd etc)
							}
						})
						
						</script><?php */?>
                        <?php /*?><marquee scrolldelay="10" scrollamount="2" direction="up" behavior="scroll" height="115" onmouseover="this.stop();" onmouseout="this.start();">
						<?php for($i=0;$i<count($news);$i++){ ?>
						<span class="blue_12"><strong><a href="index.php?p=news_detail&id=<?php echo $news[$i]['id'];?>"><?php echo $news[$i]['title']?></a></strong></span><br>
						<span class="orange_light"><?php echo date('d/m/Y',strtotime($news[0]['created']))?></span><br>
						<?php echo $news[$i]['short_description']?><br />
						<?php } ?> 
						</marquee><?php */?>
                    </td>
                </tr>
                <tr>
                    <td height="20" align="right" valign="bottom"><a href="index.php?p=news" class="orange_12_link">Read More &raquo;</a></td>
                </tr>
                <?php } else { ?>
                <tr>
                    <td height="20" align="left" valign="top">No News Available.</td>
                </tr>
                <tr>
                    <td height="20" align="left" valign="top">&nbsp;</td>
                </tr>
                <?php } ?>
            </table></td>
    </tr>
    <tr>
        <td align="left" valign="top"><?php
			$mon = $m = date('m')+0;
			/*echo strpos($mon,'0');
			if(strpos($mon,'0')!=false && strpos($mon,'0')==0)
				$m = substr(date('m'),1,1);
			else
				$m = $mon;*/
			//$sql = "select * from event where MONTH(event_date) >= '".$m."' and MONTH(event_date) <= '".(date('m')+1)."' and event_date >= '".date('Y-m-d')."' and place_id = '".$userplc."' order by event_date limit 3";

            $startDate = /*"2016-12-01";*/ date("Y-m-01");
            $endDate = date("Y-m-t", strtotime($startDate." + 3 months"));
			
			$sqlEvent = " and (event_date between '".$startDate."' and '".$endDate."') and event_date >= '".date("Y-m-d")."'";
//            $sqlEvent = " and (event_date between '".$startDate."' and '".$endDate."') "; //Todo: return this // and event_date >= '".date("Y-m-d")."'";

			$sql = "select * from event where place_id = '".$userplc."' ".$sqlEvent." order by event_date limit 3";
			$event = $objDB->select($sql);
			?>
            <table width="97%" align="right" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td align="left" valign="top" class="btm_gray_bdr"><span class="gray_bg">Events</span></td>
                </tr>
                <tr>
                    <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                </tr>
                <tr>
                    <td align="left" valign="top" class="black_11">View our upcoming events below or view our entire Calendar of events <a href="index.php?p=calendar" class="blue_11_link">here</a>.</td>
                </tr>
                <tr>
                    <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                </tr>
                <?php if(count($event)>0){ 
					for($i=0;$i<count($event);$i++){
				?>
                <tr>
                    <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="53" align="left" valign="middle" class="date_bg"><?php echo date('d',strtotime($event[$i]['event_date']));?><br>
                                    <?php echo date('M',strtotime($event[$i]['event_date']));?></td>
                                <td width="167" align="left" valign="top"><table width="97%" border="0" align="right" cellpadding="0" cellspacing="2">
                                        <tr>
                                            <td align="left" valign="top" class="orange_11"><?php echo date('d/m/Y',strtotime($event[$i]['event_date']));?>
                                                <?php if($event[$i]['to_date']!='0000-00-00' && $event[$i]['to_date']!=$event[$i]['event_date']){?>
                                                - <?php echo date('d/m/Y',strtotime($event[$i]['to_date'])); } ?></td>
                                        </tr>
                                        <tr>
                                            <td align="left" valign="top"><a href="index.php?p=event_detail&id=<?php echo $event[$i]['id']?>" class="blue_11_link"><strong><?php echo stripslashes($event[$i]['title'])?></strong></a></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table></td>
                </tr>
                <tr>
                    <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
                </tr>
                <?php }
				}else{ 
				?>
                <tr>
                    <td height="20" align="left" valign="top">No Event Available</td>
                </tr>
                <tr>
                    <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
                </tr>
                <?php } ?>
                <tr>
                    <td height="20" align="right" valign="bottom"><a href="index.php?p=event" class="orange_12_link">View More &raquo;</a></td>
                </tr>
            </table></td>
    </tr>
    <tr>
        <td align="right" valign="top"><br />
            <a href="index.php?p=contact"> <img src="images/our_church.jpg" width="213" height="42" alt="" /> </a> </td>
    </tr>
    <!--<tr>
  	<td align="left" valign="top"><br />
    	<a href="forum/index.php" target="_blank">
    	<img src="images/forum_btn.jpg" alt="" />
      </a>
    </td>
  </tr>-->
    <tr>
        <td align="right" valign="top"><br />
            <a href="feedback.php" onclick="return hs.htmlExpand(this, { objectType: 'ajax'} )"> <img src="images/feed_back.jpg" width="213" height="42" alt="" /> </a> </td>
    </tr>
</table>
