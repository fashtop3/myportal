<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/functions.php");
		CheckPermission(); 
		exit();
	}

	$Tbl="member_product";
	$FieldsArr=array();
	// -------------- REQUEST A CALL -----------------
	//$SQL = "select count(member_id) as product, sum(price) as price, sum(qty) as qty, member_id from member_product group by member_id order by id desc";
	$SQL = FetchQuery($Tbl,$FieldsArr," where member_id= '".$_REQUEST['id']."' order by id desc","","");
	$objPaging = new paging($SQL, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$prod = $objDB->select($objPaging->get_query());
	if($prod)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	
?>
<?php 
$prod1 = FetchData("member_product",array()," where member_id= '".$_REQUEST['id']."'");
$total1 = 0;
for($i=0;$i<count($prod1);$i++){
	$total1 += $prod1[$i]['total_price'];
}
?>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/payment.jpg" width="48" height="48" /></td>
		<td class="tbl_head" height="24">MEMBER PAYMENT/DONATION HISTORY <span style="float:right">Grand Total: &nbsp;<?php echo htmlentities(CURRENCY_SIGN).number_format($total1,2)?></span> </td>
	</tr>
	<tr>
		<td colspan="2" align="left" class="tbl_head"> Member:
			<?php $mem = FetchData("member",array(),"where id = '".$_REQUEST['id']."'");
									if(count($mem)>0)
										echo stripslashes($mem[0]['firstname']." ".$mem[0]['lastname']);
									else
										echo "Unregistered Member (".$_REQUEST['id'].")";
						?>
			<span style="float:right;" class="column_head">Total No of Results:&nbsp;<?php echo count($cms)?></span></td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="content" width="100%" cellpadding="5" cellspacing="1">
				<tr class="SmallBlackHeading">
					<td align="left"><b>ID</b></td>
					<td align="left"><b>Product</b></td>
					<td align="left"><b>Quantity</b></td>
					<td align="left"><b>Price</b></td>
					<td align="left"><b>Total Amount</b></td>
					<td align="left"><b>Date</b></td>
				</tr>
				<?php $total = 0;		
								
								for($i=0;$i<count($prod);$i++){
									$total += $prod[$i]['price'];
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
							?>
				<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
					<td align="left"><?php echo $prod[$i]['id']?></td>
					<td align="left">
						<?php if(strtolower($prod[$i]['product'])=='tithes' || strtolower($prod[$i]['product'])=='offering'){?>
						<?php echo stripslashes($prod[$i]['product'])?>
						<?php }else{ ?>
						<?php echo stripslashes(FetchValue("cd","title","id",$prod[$i]['product_id']));?>
						<?php } ?>
					</td>
					<td align="left">
						<?php if($prod[$i]['qty']!='' && $prod[$i]['qty']!='0') echo $prod[$i]['qty']; else echo "1";?>
					</td>
					<td align="left"><?php echo htmlentities(CURRENCY_SIGN).number_format($prod[$i]['price'],2);?></td>
					<td align="left"><?php echo htmlentities(CURRENCY_SIGN).number_format($prod[$i]['total_price'],2);?></td>
					<td align="left"><?php echo $prod[$i]['created']?></td>
				</tr>
				<?php } ?>
				<tr height="25">
					<td colspan="4" align="right" class="TopBorder"><b>Sub Total: </b></td>
					<td align="left"><b><?php echo htmlentities(CURRENCY_SIGN).number_format($total,2);?></b></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr height="25">
		<td colspan="2" align="right" class="TopBorder"><?php echo $objPaging->show_paging()?></td>
	</tr>
	<tr>
		<td colspan="2" align="center">
			<input type="button" value="BACK" class="Btn" onclick="window.location='index.php?p=payment_history&pg_no=<?php echo $_REQUEST['pg_no']?>'" />
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
