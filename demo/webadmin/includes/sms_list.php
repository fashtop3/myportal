<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/functions.php");
		CheckPermission(); 
		exit();
	}

	$PANEL_HEADING = "SMS SUBSCRIBERS";
	$Tbl="sms";
	$FieldsArr=array();
	if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!=''){
			$Where=' where place_id='.$_REQUEST['plc'];
			$path1 = '&plc='.$_REQUEST['plc'];
		}
		else{
			$Where=' where 1=1';	
			$path1 = '';
		}
	}
	else
		$Where=' WHERE place_id='.$_SESSION['PlaceID'];

	if($_REQUEST['Search']!='')
		$Where.= " and sms_mobile_no like '%".$_REQUEST['Search']."%'";

	if($_REQUEST['admins']!='')
		$Where .= ' and createdby = '.$_REQUEST['admins'];
	if($_REQUEST['view']=='my_view'){	
		$Where .= ' and createdby = '.$_SESSION['AdminID'];
		$view = strtoupper("All SMS subscribers");
		$viewpath = 'index.php?p=sms_list'.$path1;
		$path2 = '&view=my_view';
	}
	else{
		$view = strtoupper("My Added SMS subscribers");	
		$viewpath = 'index.php?p=sms_list&view=my_view'.$path1;
		$path2 = '';
	}
	
	$Sort="order by sms_id desc";
	$Limit="";
	
	// -------------- REQUEST A CALL -----------------
	$SQL = FetchQuery($Tbl,$FieldsArr,$Where,$Sort,$Limit);
	$objPaging = new paging($SQL, "sms_id");
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$RecordSet = $objDB->select($objPaging->get_query());
	if(!$RecordSet)
			$_SESSION['SuccessMsg'] = 'No Record Found';	
?>
<script language="JavaScript">
	function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_sms.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_sms.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($RecordSet)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
	
</script>


<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/sms.jpg" width="48" height="48" /> <?php echo $PANEL_HEADING?>
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          	<form name="FrmSMS" method="post" action="">
	<input type="hidden" name="ID" id="ID" value="">
    <input type="hidden" name="pg_no" id="pg_no" value="">
	<input type="hidden" name="Process" id="Process" value="">
	
				<?php showMessage();	?>
			<div class="form-group col-md-12">
					<div class="col-md-9">
						<?php if($_SESSION['AdminID']==1){ ?>
						<div class="col-md-3">
							<select name="PlaceValue" class="form-control" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=sms_list&plc='+this.value+'<?php echo $path2; ?>'; else window.location='index.php?p=sms_list'+'<?php echo $path2; ?>';">
					<option value="">All Places</option>
					<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
				</select>
						</div>
						<div class="col-md-3">
							<select name="admins" class="form-control" id="admins" onchange="if(this.value!='') window.location='index.php?p=sms_list&admins='+this.value; else window.location='index.php?p=sms_list';">
					<option value="">--Select Administrator--</option>
					<?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
				</select>
						</div>
						<?php } ?>
						<div class="col-md-2">
							Search by mobile number:
						</div>
						<div class="col-md-3">
								<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="form-control" />
						</div>
						<div class="col-md-1">
							<input type="submit" value="Search" class="btn btn-default" />
						</div>
					</div>
					<div class="col-md-3" style="text-align: right;float: right;">
						<span class="column_head" style="float:right; padding-top:2px;">Total No of Results:&nbsp;<?php echo count($RecordSet)?></span>
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="col-md-9">
							<input type="Button" name="ADDNEW" value="ADD NEW" class="btn btn-default" onClick="window.location.href='index.php?p=sms_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
						
							<input type="Button" name="sermons" value="<?php echo $view?>" class="btn btn-default" onClick="window.location.href='<?php echo $viewpath; ?>'"/>
					
							<input type="Button" name="SENDBULK" value="SEND BULK" class="btn btn-default" onClick="window.location='index.php?p=send_bulk_sms';"/>
						
							<input type="button" value="SHOW ALL" class="btn btn-default" onclick="window.location='index.php?p=sms_list'" />
						
					</div>
					<div class="col-md-3" style="text-align: right;float: right;">
						<?php echo $objPaging->show_paging()?>
					</div>
				</div>
				<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
		<tr height="25" class="SmallBlackHeading">
			<td width="4%" align="center" class="BottomBorder">
				<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
			</td>
			<th width="4%" align="center" class="BottomBorder">#</th>
			<th width="33%" align="left" class="BottomBorder">Name</th>
			<th width="23%" align="left" class="BottomBorder">Mobile No</th>
			<?php if($_SESSION['AdminID']==1){ ?>
			<th width="25%" align="center" class="BottomBorder">Place</th>
			<?php } ?>
			<th width="12%" align="center" class="BottomBorder">Options</th>
		</tr></thead>
<tbody>
		<?php
			$i=0;
			$r=$start_limit+1;
			if(!empty($RecordSet))
			{
				for($i=0;$i<count($RecordSet);$i++)
				{ 
					if(($i+1)%2==0)
						$bg=BGCOLOR_ODD_ROW;
					else
						$bg=BGCOLOR_EVEN_ROW;
		?>
		<tr>
			<td align="center">
				<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $RecordSet[$i]['sms_id']?>">
			</td>
			<td align="center" class="Numbers"><?php echo $i+1?></td>
			<td align="left"><?php echo ucwords(stripslashes($RecordSet[$i]['sms_name'])); ?></td>
			<td align="left"><?php echo ucwords(stripslashes($RecordSet[$i]['sms_mobile_no'])); ?></td>
			<?php if($_SESSION['AdminID']==1){ ?>
			<td align="center"><?php echo stripslashes(FetchValue('county','c_name','c_id',$RecordSet[$i]['place_id']));?> </td>
			<?php } ?>
			<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=sms_addedit&ID=<?php echo $RecordSet[$i]['sms_id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $RecordSet[$i]['sms_id']?>', 'DELETE');" /></td>
		</tr>
		<?php
				$r++;
				}
			}
	?></tbody>
	</table>
</div>
<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;">
						<?php if(count($RecordSet)) print $page_link; else print "&nbsp;"; ?>
					</div>
				</div>


				<div class="form-group col-md-12">
					<div class="col-md-6">
						<input type="submit" name="delete" value="DELETE" class="btn btn-default" onClick="return ValidateSelection(this.form);">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;">
						<?php echo $objPaging->show_paging()?>
					</div>
				</div>

		
</form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>