<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATEVIDEO";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"sermon"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$path = "../uploads/sermon/video/".stripslashes($Info[0]['video']);	
}else
	$MODE="ADDVIDEO";
?>
<?php

	require("include/path.ini");		/*set path access*/				

	require("include/security.inc");	/*check path security*/

	require("include/mime.inc");		/*load the mime type file*/

	

	define("SHOW_EDIT",1);

	define("HIDE_EDIT",0);

	

	if(isset($_GET['dir']))

		$dir=$_GET["dir"];

	else

		$dir="";

	

	$allowbrowse = $AllowSelfBrowse || !strstr($dir,basename(dirname($_SERVER['SCRIPT_NAME'])));

	

	function GetFileInfo($f) 

	{

		$s = array (	'perm'  => '',

				'humanperm' => '',	//human readable permissions: rwxrwxrwx

				'ux'    => false,	//is this file is user executable ?

				'islnk' => false,	//is this file is a link ?

				'owner'  => '',

				'group'  => '',

				'size'   => 0,

				'mtime'  => ''

				);

		$mystat = stat($f);

		

		$s["perm"]=$mystat[2] & 511;

		if(($s["perm"] & 320) == 320) $s['ux']=true;

		$fullperms=256; 			// rwxrwxrwx

		$s["humanperm"]="";

		$let = array('r','x','w'); 	//no, it's not rwx, but rxw, because browsed backwards

		

		for($i=9;$i>0;$i--) 

		{ 

			($s["perm"] & $fullperms)? $s["humanperm"].=$let[$i % 3]:$s["humanperm"].="-";

			$fullperms = $fullperms >> 1;

		}

		

		//last modification date

		$s["mtime"]=date('d M Y H:s',$mystat[10]);

		

		return($s);

	}

?>
<script>
  $(document).ready(function(){
  	
    // validate signup form on keyup and submit
	
	$("#frmAdmin").validate({
		rules: {			
			 <?php if($MODE=='ADDVIDEO'){?>CVideo: "required",<?php } ?>
			 sermon: "required"
		},
		messages: {
			 <?php if($MODE=='ADDVIDEO'){?>CVideo: "Please enter video",<?php } ?>
			 sermon: "Please select sermon"
		}
	});	
	
  });
</script>
<link href="include/fonts.css" rel="stylesheet" type="text/css">
<script language=JavaScript src="include/codes.js"></script>
<section class="content-header">
  <h1>
    SERMON AUDIO/VIDEO
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addvideo.jpg" /></td>
		<td colspan=5 class="tbl_head" height="24">SERMON AUDIO/VIDEO</td>
	</tr>
	<tr>
		<td width="" colspan="6">
			<form method="post" action="manage_sermon.php" id="frmAdmin" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
			<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
			<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
			<tbody>
				<tr>
					<td>
						<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
							<tbody>
								<tr>
									<td class="fieldlabel" width="5%">Sermon</td>
									<td class="fieldarea">
										<select name="sermon" id="sermon" style="width:300px;">
											<option value="">--Select Sermon--</option>
											<?php echo FillCombo1('sermon','title','id',$Info[0]['id'],"");?>
										</select>
									</td>
								</tr>
								<tr>
									<td class="fieldlabel">Video</td>
									<td class="fieldarea" valign="top">
										<input name="Video" id="Video" value="<?php echo $Info[0]['video']?>" class="input" style="width: 300px;" type="text" readonly="readonly" />
									</td>
								</tr>
								<tr>
									<td class="fieldlabel">Flash</td>
									<td class="fieldarea" valign="top">
										<input name="Flash" id="Flash" value="<?php echo $Info[0]['flash']?>" class="input" style="width: 300px;" type="text" readonly="readonly" />
									</td>
								</tr>
								<tr>
									<td class="fieldlabel">Audio</td>
									<td class="fieldarea" valign="top">
										<input name="Audio" id="Audio" value="<?php echo $Info[0]['audio']?>" class="input" style="width: 300px;" type="text" readonly="readonly" />
									</td>
								</tr>
							</tbody>
						</table>
						<br />
						<table width="500" border="0" cellspacing="0" cellpadding="0">
			<form name="myform" action="delete.php" method="post" enctype="multipart/form-data">
				<tr>
						<td width="10">&nbsp;</td>
						<td width="490">&nbsp;</td>
					</tr>
				<tr>
						<td width="10">&nbsp;</td>
						<td height="25" valign="top"><img src="images/explorer.gif" width="16" height="16" border=0>&nbsp;<span class="title">File 
							
							Explorer</span></td>
					</tr>
				<tr>
						<td width="10">&nbsp;</td>
						<td>
							<table width="490" border="0" cellspacing="0" cellpadding="0">
								<tr align="center">
									<td width="25"><a class='text2' href="javascript:shwMoreOpt('upload.php?dir=<?php echo $_GET["dir"]; ?>',410,300,0)"><img src="images/upload.gif" alt="Upload Files" width="16" height="16" border="0"></a></td>
									<td width="25">
										<input name="delete" type="image" value="delete" src="images/delete.gif" alt="Delete Files" width="16" height="16">
									</td>
									<td width="25"><a class='text2' href="javascript:shwMoreOpt('directory_create.php?dir=<?php echo $_GET["dir"]; ?>&option=folder',410,150,0)"><img src="images/createfolder.gif" alt="Create Folder" width="16" height="16" border="0"></a></td>
									<td width="25"><a class='text2' href="javascript:shwMoreOpt('directory_remove.php?dir=<?php echo $_GET["dir"]; ?>&option=folder',410,150,0)"><img src="images/deletefolder.gif" alt="Delete Entire Folder" width="16" height="16" border="0"></a></td>
									<td width="25"><a class='text2' href="javascript:shwMoreOpt('help.php',410,370,0)"><img src="images/help.gif" alt="Help" width="16" height="16" border="0"></a></td>
									<td width="365">&nbsp;</td>
								</tr>
							</table>
						</td>
					</tr>
				<tr>
						<td width="10">&nbsp;</td>
						<td>
							<hr size="1">
						</td>
					</tr>
				<tr>
						<td width="10">&nbsp;</td>
						<td>
							<table width="490" border="0" cellspacing="1" cellpadding="1">
								<tr>
									<td height="20" colspan="6" class="text1">
										<?php

		  		/*if path exists, open it!*/

				$hDir = opendir($ROOTPATH.$dir);

				/*output the current path*/

				if($allowbrowse) 

				{

					print "Contents of ".$dir;

					if(strcmp($dir,'/')) 

						print "/";

				}

		  	?>
									</td>
								</tr>
								<?php

			/*browse dir*/

			$j=0;

			$frmid=0;



			//browse all the dirs + files of the current folder prepare the exceptions (file to hide)

			if(strlen($Exceptions)>0) 

				$except=explode(",",$Exceptions);



			while ($file=readdir($hDir)) 

			{

				$res=true;

				if(strlen($Exceptions)>0) 

				{

					reset($except);

					for($i=0;$i<count($except);$i++) 

					{

						if(!strcmp($file,$except[$i])) 

						{

							$res=false;

							break;

						}

					}

				}

				//is the file to be displayed

				if($res)

					$files[$j++]=$file;

			} /*end while*/



			/*updir link*/

			if($allowbrowse) 

			{

				print " <tr class='header'>\n"; 

          		print "		<td height='20' colspan='6'>\n";

				if(strcmp($dir,"/")) 	

				{

					$updir = dirname($dir);

					$updir = str_replace("\\","/",$updir);

					if ($updir == "")

						$updir = "/";

					print "<a href='explorer.php?dir=$updir'><img src='images/updir.gif' border=0 alt='updir' width=16 height=16></a>\n";

				}

				

				print "		</td>\n";

        		print "	</tr>\n";

        		print "	<tr class='header'>\n"; 

          		print "		<td width='217' height='20' bgcolor='#4C68BD'>&nbsp;Name</td>\n";

          		print "		<td width='75' height='20' align='center' bgcolor='#4C68BD'>Size</td>\n";

          		print "		<td width='150' height='20' align='center' bgcolor='#4C68BD'>Last Modified</td>\n";

          		print "		<td width='16' height='20'>&nbsp;</td>\n";

          		print "		<td width='16' height='20'>&nbsp;</td>\n";

          		print "		<td width='16' height='20'>&nbsp;</td>\n";

        		print "	</tr>\n";

        		


		  		if(count($files)) 

				{

					sort($files);

					reset($files);

			

					/*display info*/

					$i=0;

					while (list($key,$name) = each($files)) 

					{

						$filename=$ROOTPATH."$dir$sp$name";

						$rc=chkfile($name,&$icon);

	

						//is the current file is a directory ?

						if(is_dir($filename)) 

						{

							$filestats=GetFileInfo($filename);

							/*if the dir has not the u+x permission or the dir is explorer, notice it*/

							if(!$filestats['ux'] || !$AllowSelfBrowse && !strcmp($name,basename(dirname($_SERVER['SCRIPT_NAME']))) ) 

							{

								// display name

								print "	<tr class='text1'>\n";

								print "		<td height='20'>\n";

								print "		\t<img border=0 src='images/icon_nodir.gif' alt='private dir' width=16 height=16>&nbsp;&nbsp;".$name."\n</td>\n";

								print "		<td height='20'>&nbsp;</td>\n";

								print "		<td height='20'>&nbsp;</td>\n";

								print "		<td height='20'>&nbsp;</td>\n";

								print "		<td height='20'>&nbsp;</td>\n";

								print "		<td height='20'>&nbsp;</td>\n";

								print "	</tr>\n";

							}

							else 

							{

								// display name

								print "	<tr class='text1'>\n";

								print "		<td height='20' class='hyperlink1'>\n";

								if ($dir == "/")

									$dir = "";

								$href = "explorer.php?dir=$dir$sp$name";

								print "		\t<a class='hyperlink1' href='".$href."'><img border='0' width='16' height='16'";

								if($filestats['islnk'])

									print "src='images/icon_dirlnk.gif' alt='dir link'></a>";

								else

									print "src='images/icon_dir.gif' alt='dir'></a>";

								print "		&nbsp;<a class='hyperlink1' href='".$href."'>".$name."</a>\n";

								print "		<td height='20'>&nbsp;</td>\n";

								print "		<td height='20' align='center'>".$filestats["mtime"]."</td>\n";

								print "		<td height='20'><img border='0' src='images/spacer.gif' width='16' height='16'></td>\n";

								print "		<td height='20'><img border='0' src='images/spacer.gif' width='16' height='16'></td>\n";

								print "		<td height='20'><img border='0' src='images/spacer.gif' width='16' height='16'></td>\n";

								print "	</tr>\n";

								$i++;

							}

						}

					} //end of while loop

		  			

					reset($files);

					while (list($key,$name) = each($files)) 

					{

						$rc=chkfile($name,&$icon);

						$filename=$ROOTPATH."$dir$sp$name";

						$href=$ROOTURL.$dir.$sp.$name;

				

						if(!is_dir($filename)) 

						{

							print "	<tr class='text1'>\n";

							print "		<td height='20'>\n";

							print "		<a href='".$href."'><img border='0' src='".$icon."' alt='file' width='16' height='16'></a>";

							print "		&nbsp;<a class='text2' href=\"".$href."\">$name</a>\n";

							print "    	</td>\n";

				

							//display file size

							print "    	<td class='text1' height='20' align='right'>";

							$s=filesize($filename);

							$unit="b";

							if($s >= 1024) 

							{ 

								$s=floor(round($s/1024));

								$unit="KB";

							}

							$s = number_format($s,"","","thousands_sep");

							print $s." ".$unit;

							print "</td>\n";

					

							//display last modification date

							print "		<td class='text1' height='20' align='center'>";

							$modifytime = date('d M Y H:s',filemtime($filename)); 

							print $modifytime."</td>\n";

																					

							/*Allow browser for deletion*/

							if($IsShowDelete) 

							{

								print "		<td class='text1' height='20'>";

								print "<input type='checkbox' name='removeitems[]' value='".$name."'></td>\n";

							}

							else 

								print " <td class='text1' height='20'>&nbsp;</td>\n";

							

							/*if the file is a known text file, allow edit and delete (based on explorer.cfg)*/

							if($rc) 

							{

								print "		<td class='text1' height='20'>";

								print "<a href='edit.php?dir=$dir&name=$name'><img src='images/edit.gif' border=0 alt='Edit File' width=16 height=16></a></td>\n";

							}	

							else

								print " <td class='text1' height='20'>&nbsp;</td>\n";

												

							/*Allow zip*/

							if (strpos($name,".zip")|| strpos($name,".rar")) 

							{

								print "    	<td class='text1' height='20'>";

								print "		<a class='text2' href=javascript:shwMoreOpt('unzip.php?dir=$dir$sp&filename=$name',410,200,0)><img src='images/icon_zip.gif' alt='Unzip File' border=0 width=16 height=16></a></td>\n";

								print " </tr>\n";

							}

							else

							{

								print "    	<td class='text1' height='20'>";

								print "		<img src='images/spacer.gif' alt='' border=0 width=16 height=16></td>\n";

								print " </tr>\n";

							}

							$i++;

						}

					} //end of while

				} //end of if(count($files))

				else

				{

					print "	<tr class='text1'>\n"; 

          			print "		<td colspan='6' class='text1'>[Empty Folder...]</td>\n";

          			print "	</tr>\n";

				}

			} //endif allowbrowse

			else 

			{

				if(! $pass++)

				{

					print "<td colspan='6' class='text1'>\n";

					print "browsing under the explorer itself is forbidden (see your administrator)\n";

					print "</td>\n";

				}

			}

		?>
								<tr>
									<td colspan="6" class="text1">
										<hr size="1">
									</td>
								</tr>
								<tr>
									<td height="25" colspan="6" valign="top" class="text1">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td width="79%" class="text1">
													<?php

		  	//print number of items in this directory

			$msg=$i." item";

			if($i>1) 

				$msg.="s";

			echo "Total Files and Folders in this Directory: ".$msg;

		  ?>
												</td>
												<td width="21%" align="right" class="text1">
													<input name="allbox" type="checkbox" value="Check All" onClick="CheckAll();">
													<B>Select All</B>&nbsp;</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td colspan="6" class="text2">
										<table width="480" border="0" cellspacing="0" cellpadding="0">
											<tr align="center">
												<td width="25"><a class='text2' href="javascript:shwMoreOpt('upload.php?dir=<?php echo $_GET["dir"]; ?>',410,300,0)"><img src="images/upload.gif" alt="Upload Files" width="16" height="16" border="0"></a></td>
												<td width="25">
													<input name="delete" type="image" value="delete" src="images/delete.gif" alt="Delete Files" width="16" height="16">
												</td>
												<td width="25"><a class='text2' href="javascript:shwMoreOpt('directory_create.php?dir=<?php echo $_GET["dir"]; ?>&option=folder',410,150,0)"><img src="images/createfolder.gif" alt="Create Folder" width="16" height="16" border="0"></a></td>
												<td width="25"><a class='text2' href="javascript:shwMoreOpt('directory_remove.php?dir=<?php echo $_GET["dir"]; ?>&option=folder',410,150,0)"><img src="images/deletefolder.gif" alt="Delete Entire Folder" width="16" height="16" border="0"></a></td>
												<td width="25"><a class='text2' href="javascript:shwMoreOpt('help.php',410,370,0)"><img src="images/help.gif" alt="Help" width="16" height="16" border="0"></a></td>
												<td width="355">
													<input type="hidden" name="delete" value="delete">
													<input name="present_directory" type="hidden" id="present_directory" value="<?php echo $_GET['dir']; ?>">
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				<tr>
						<td width="10">&nbsp;</td>
						<td>
							<hr size="1">
						</td>
					</tr>
				<tr>
						<td width="10">&nbsp;</td>
						<td class="text1">&nbsp;</td>
					</tr>
			</form>
</table>
</td>
</tr>
<tr>
	<td><br>
		<input type="hidden" value="<?php echo $MODE?>" name="action" />
		<br>
		<div align="center">
			<input value="SUBMIT" class="Btn" type="submit" name="submit1" id="submit1">
			<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=video_list';">
		</div>
	</td>
</tr>
</tbody>
</table>
</form>
</td>
</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
