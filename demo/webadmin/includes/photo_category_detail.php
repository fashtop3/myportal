<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"photo_gallery_category"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder" ></script>
<script type="text/javascript" src="js/lightbox.js" ></script>
<link href="css/lightbox.css" rel="stylesheet" type="text/css">
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"> <img src="<?php echo ADMIN_IMAGE_PATH;?>/news_detail.png" width="48" height="48" /> </td>
		<td class="tbl_head" height="24">PHOTO GALLERY CATEGORY DETAIL</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" class="form">
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Title : <?php echo stripslashes($Info[0]['title'])?> </b> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<?php if($Info[0]['image']){ ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"><a rel="lightbox0" href="../uploads/photo_gallery/big/<?php echo stripslashes($Info[0]['image']); ?>"> <img style="border:none" src="../uploads/photo_gallery/big/<?php echo stripslashes($Info[0]['image']); ?>" width="50" height="50" title="Click hre to view"/> </a> <br />
						Click on the Image to enlarge it. </td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;"> <?php echo stripslashes($Info[0]['description'])?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2">
						<center>
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=photo_category_list';">
						</center>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
			</table>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
