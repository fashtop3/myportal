<?php
if($_REQUEST['Process']=='Update'){
	$sql = "update curriculum set quarter_index = 0 where quarter_index = '".$_REQUEST['QIndex']."'";
	$objDB->sql_query($sql);
	
	$sql = "update curriculum set quarter_index = '".$_REQUEST['QIndex']."' where cdate between '".$_REQUEST['SDate']."' and '".$_REQUEST['EDate']."'";
	$objDB->sql_query($sql);
	
	$_SESSION['SuccessMsg'] = "Quarter Index Schedule (Sunday School) updated successfully!";
	header("Location:index.php?p=quarter_index");
	exit;
}
$index = FetchData("curriculum",array(),"order by quarter_index desc");
$quarter = FetchData("curriculum",array(),"where quarter_index='".$index[0]['quarter_index']."' order by cdate");
?>
<script type="text/javascript">
	$(function() {
		$('#SDate').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1910:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
		$('#EDate').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1910:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});
	$("#frmAdmin").validate();
</script>
<section class="content-header">
  <h1>
    <img src="images/sermon.jpg" width="30" height="30" /> QUARTER INDEX SCHEDULE
    <small>(SUNDAY SCHOOL)</small>
  </h1>
  <!-- <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol> -->
  
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
						<?php showMessage();	?>
						<form class="form-horizontal" name="FrmSMS" method="post" action="" id="FrmSMS">
							<input type="hidden" name="Process" id="Process" value="Update" />
							<input type="hidden" name="QIndex" id="QIndex" value="<?php echo $index[0]['quarter_index']?>" />
							<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Start Date:</label>
                  <div class="col-sm-5">
                    	<input type="text" name="SDate" id="SDate" value="<?php echo $quarter[0]['cdate']?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">End Date:</label>
                  <div class="col-sm-5">
                    	<input type="text" name="EDate" id="EDate" value="<?php echo $quarter[count($quarter)-1]['cdate']?>" class="form-control" />
                  </div>
                </div>
									 <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">

                            <input type="submit" value="UPDATE" class="btn btn-success" />
										<input type="reset" value="RESET" class="btn btn-default" />
                          </div>
                        </div>
						</form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>

