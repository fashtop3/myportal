<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"event"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/viewevent.jpg" width="30" height="30" /> EVENT DETAIL
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
         
			<table width="100%" class="form">
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Title : </b> <?php echo stripslashes($Info[0]['title'])?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Date : </b> <?php echo date('d F Y',strtotime($Info[0]['event_date']));?>
						<?php if($Info[0]['to_date']!='0000-00-00' && $Info[0]['to_date']!=$Info[0]['event_date']){echo " - ".date('d F Y',strtotime($Info[0]['to_date']));}?>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Place : </b> <?php echo stripslashes($Info[0]['address'])?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><img src="../uploads/event/big/<?php echo stripslashes($Info[0]['image'])?>" width="100" height="100" /> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;"> <?php echo stripslashes($Info[0]['description'])?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2">
						
							<input value="BACK" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=event_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
			</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
