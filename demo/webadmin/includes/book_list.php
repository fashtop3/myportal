<?php

	$sql = "select * from book where 1";	

	if($_REQUEST['Search']!='')
		$sql .= " and book_no = ".$_REQUEST['Search'];
	
	$sql .= " order by book_no desc,lesson_no desc,lesson_date";
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$schedule = $objDB->select($objPaging->get_query());

		if($schedule)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>

<script type="text/javascript" language="javascript">

function confirmDel(frm)
{
	if(confirm("Do you really want to delete?"))
	{		
		frm.Process.value='DELETEMULTIPLE';
		frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
		frm.action='manage_book.php';	
		frm.submit();
	}	
}
function singleDel(ID,Process)
{
	if(confirm("Do you really want to delete?"))
	{		
		document.FrmSMS.ID.value = ID;
		document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
		document.FrmSMS.Process.value = Process;
		document.FrmSMS.action = 'manage_book.php';
		document.FrmSMS.submit();
	}	
}

function check_all()
{
	dml=document.FrmSMS;
	len = dml.elements.length;
	var i=0;
	if(dml.delall.checked==true)
	{
		for( i=0 ; i<len ; i++) 
		{
			dml.elements[i].checked=true;
		}
	}
	else
	{
		for( i=0 ; i<len ; i++) 
		{
			dml.elements[i].checked=false;
		}
	}
}
function ValidateSelection(frm)
{		
	var x=true;
	for(var i=0;i<<?php echo count($schedule)?>;i++)
	{
		if(document.getElementById('del'+i).checked)
		{
			x=false;
			confirmDel(frm);
			break;
		}
	}
	if(x)
		alert('Please select Atleast One Record');
	return false;	
}
</script>
<script type="text/javascript">
var months = new Array(12);
months[0] = "January";
months[1] = "February";
months[2] = "March";
months[3] = "April";
months[4] = "May";
months[5] = "June";
months[6] = "July";
months[7] = "August";
months[8] = "September";
months[9] = "October";
months[10] = "November";
months[11] = "December";

function showEdit(id,value){		
	hideAll();
	document.getElementById(id+'_0').style.display = 'none';
	document.getElementById(id).style.display = '';
	/*str = '<input type="text" value="'+value+'" name="SDate" id="SDate" />';
	str += '<input type="button" onclick="save()" value="Go" class="btn btn-default" />';
	document.getElementById(id).innerHTML = str;*/
	$(function() {	
		$('#SDate'+id).datepicker({
			beforeShowDay: nonWorkingDates,
			changeMonth: true,
			changeYear: true,
			yearRange: '1975:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});
	
	document.getElementById('SDate'+id).focus();
}

function hideEdit(id, value){	
	var now = new Date(value);
	//alert(now);
	day = now.getDate();//toString('dd mmmm yyyy');
	month = now.getMonth();
	year = now.getFullYear();
	
	str = day+" "+months[month]+" "+year;
	document.getElementById(id).innerHTML = str;
}

function hideAll(){	
	for(id=0;id<<?php echo count($schedule)?>;id++){		
		document.getElementById(id+'_0').style.display = '';
		document.getElementById(id).style.display = 'none';
	}
}

function save(i,id){
	value = document.getElementById("SDate"+i).value;
	//alert(value);
	$.post("save_date.php",{ id:id, dt:value } ,function(data)
	{
		if(data=='No'){
			alert("Not a Sunday Date");
		}
		else{
			document.getElementById(i+'_0').innerHTML = data;
			document.getElementById(i+'_0').style.display = '';
			document.getElementById(i).style.display = 'none';
		}
	});	
}
</script>
<section class="content-header">
  <h1>
   <img src="<?php echo ADMIN_IMAGE_PATH;?>/book.gif" width="30" height="30" /> BOOK MANAGER
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<?php showMessage();	?>
			<form  class="form-horizontal" name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<div class="form-group col-md-12">
					<div class="col-md-9">
						<div class="col-md-3">Search by Book no:
						</div>
						<div class="col-md-3"><input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="form-control" />
						</div>
						<div class="col-md-2"><input type="submit" value="Search" class="btn btn-default" />
						</div>
						<div class="col-md-3">
						</div>
						<div class="col-md-1">
						</div>
					</div>
					<div class="col-md-3" style="text-align: right;float: right;">
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="col-md-7">
						
							<input type="Button" name="ADDNEW" value="ADD NEW" class="btn btn-default" onClick="window.location.href='index.php?p=book_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
						
							<input type="button" value="SHOW ALL" class="btn btn-default" onclick="window.location='index.php?p=book_list'" />
						
							<input type="button" value="Update Multiple Lesson Date of a Book" class="btn btn-default" onclick="window.location='index.php?p=book_multiple_date'" />
						
					</div>
					<div class="col-md-6" style="text-align: right;float: right;">
						<?php echo $objPaging->show_paging()?>
					</div>
				</div>
				<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
					<tr height="25" class="SmallBlackHeading">
						<th width="4%" align="center" class="BottomBorder">
							<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
						</th>
						<th width="5%" align="center" class="BottomBorder">#</th>
						<th width="25%" align="left" class="BottomBorder">Book No</th>
						<th width="25%" align="center" class="BottomBorder">Lesson No</th>
						<th width="25%" align="center" class="BottomBorder">Lesson Date (click on date to edit)</th>
						<th align="center" class="BottomBorder">Options</th>
					</tr></thead>
<tbody>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($schedule))
							{
								for($i=0;$i<count($schedule);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr>
						<td align="center">
							<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $schedule[$i]['id']?>">
						</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo ucwords(stripslashes($schedule[$i]['book_no'])); ?></td>
						<td align="center"><?php echo stripslashes($schedule[$i]['lesson_no']);?> </td>
						<td align="center">
							<span id="<?php echo $i; ?>_0" onclick="showEdit('<?php echo $i; ?>','<?php echo $schedule[$i]['lesson_date']?>')">
							<?php echo date("d F Y",strtotime($schedule[$i]['lesson_date']));?>
							</span>
							<span id="<?php echo $i; ?>" style="display:none">
								<input type="text" value="<?php echo $schedule[$i]['lesson_date']?>" name="SDate<?php echo $i?>" id="SDate<?php echo $i?>" />
								<input type="button" onclick="save('<?php echo $i?>','<?php echo $schedule[$i]['id']?>')" value="Go" class="btn btn-default" />
							</span>
						</td>
						<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=book_addedit&id=<?php echo $schedule[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $schedule[$i]['id']?>', 'DELETE');" /></td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?></tbody>
	</table>
</div>
	<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php if(count($schedule)) print $page_link; else print "&nbsp;"; ?>
					</div>
				</div>

				<div class="form-group col-md-12">
					<div class="col-md-6"><input type="submit" name="delete" value="DELETE" class="btn btn-default" onClick="return ValidateSelection(this.form);">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php echo $objPaging->show_paging()?>
					</div>
				</div>
					
			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
