<?php
	$sql = "select * from cms";
	if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!='')
			$sql.=' where place_id='.$_REQUEST['plc'];
		else
			$sql.=' where 1=1';	
	}
	else
		$sql.=' WHERE place_id='.$_SESSION['PlaceID'];
	if($_REQUEST['admins']!='')
		$sql .= ' and modifiedby = '.$_REQUEST['admins'];
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmMail(frm)
	{
		if(confirm("Do you really want to delete?"))
		{	
			
			frm.Process.value='DELETE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_cms.php';	
			frm.submit();
		}	
	}
function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmMail(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
		
  $(document).ready(function(){   
    $(".tabbox").css("display","none");
var selectedTab;
$(".tab").click(function(){
    var elid = $(this).attr("id");
    $(".tab").removeClass("tabselected");
    $("#"+elid).addClass("tabselected");
    $(".tabbox").slideUp();
    if (elid != selectedTab) {
        selectedTab = elid;
        $("#"+elid+"box").slideDown();
    } else {
        selectedTab = null;
        $(".tab").removeClass("tabselected");
    }
    $("#tab").val(elid.substr(3));
});

  });


</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/cms.jpg" width="30" height="30" /> CMS CONTENT MANAGER
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<?php showMessage();	?>
			<form class="form-horizontal"  name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<div class="form-group col-md-12">
					<div class="col-md-9">
						<?php if($_SESSION['AdminID']==1){ ?>
						<div class="col-md-3">
							<select name="PlaceValue" class="form-control" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=cms_list&plc='+this.value; else window.location='index.php?p=cms_list';">
								<option value="">All Places</option>
								<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
							</select>
						</div>
						<div class="col-md-3">
							<select name="admins" class="form-control" id="admins" onchange="if(this.value!='') window.location='index.php?p=cms_list&admins='+this.value; else window.location='index.php?p=cms_list';">
								<option value="">--Select Administrator--</option>
								<?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
							</select>
						</div>
						<?php } ?>
					</div>
					<div class="col-md-3" style="text-align: right;float: right;"><span class="column_head">Total No of Results:&nbsp;<?php echo count($cms)?></span> 
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php echo $objPaging->show_paging()?>
					</div>
				</div>			
							
							
							
						


<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
					<tr height="25" class="SmallBlackHeading">
						<th width="5%" align="center" class="BottomBorder">&nbsp;
							<!--input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);"-->
						</th>
						<th width="5%" align="center" class="BottomBorder">#</th>
						<th width="60%" align="left" class="BottomBorder">Title</th>
						<?php if($_SESSION['AdminID']==1){ ?>
						<th width="20%" align="center" class="BottomBorder">Place</th>
						<?php } ?>
						<th align="center" class="BottomBorder">Options</th>
					</tr></thead>
<tbody>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr>
						<td align="center">
							<!--input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i]['id']?>"-->
						</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo ucwords(stripslashes($cms[$i]['title'])); ?></td>
						<?php if($_SESSION['AdminID']==1){ ?>
						<td align="center"><?php echo stripslashes(FetchValue('county','c_name','c_id',$cms[$i]['place_id']));?> </td>
						<?php } ?>
						<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=cms_addedit&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp;
							<!--img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp;-->
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="Detail" title="Detail" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onClick="window.location.href='index.php?p=cms_detail&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" /> </td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?></tbody>
	</table>


<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
					</div>
				</div>


<div class="form-group col-md-12">
					<div class="col-md-6">	<!--input type="submit" name="delete" value="DELETE" class="btn btn-default" onClick="return ValidateSelection(this.form);"-->
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
					</div>
				</div>
			</form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
