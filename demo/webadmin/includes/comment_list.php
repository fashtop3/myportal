<?php

	$sql = "select * from comment where 1";
	
	if($_REQUEST['Keyword']!=''){
		$sql .= " and name like '%".$_REQUEST['Keyword']."%'";
	}		
		
	$sql .= " order by id desc";
	
	//=================================================
	
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());
	if($cms)
		$_SESSION['FoundMsg'] = '';
	else
		$_SESSION['SuccessMsg'] = 'No Record Found';	
	
?>
<script type="text/javascript" language="javascript">

function confirmMail(frm)
{
		if(confirm("Do you really want to Mail?"))
		{		
			frm.Process.value='MULTIPLEDELETE';
			frm.QueryString.value='<?php echo $query_var?>';
			frm.action='manage_comment.php';	
			frm.submit();
		}	
}
function ValidateSelection(frm)
{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('testi'+i).checked)
			{
				x=false;
				confirmMail(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
}
function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_comment.php';
			document.FrmSMS.submit();
		}	
	}
	$(document).ready(function(){   
		$(".tabbox").css("display","none");
		var selectedTab;
		$(".tab").click(function(){
			var elid = $(this).attr("id");
			$(".tab").removeClass("tabselected");
			$("#"+elid).addClass("tabselected");
			$(".tabbox").slideUp();
			if (elid != selectedTab) {
				selectedTab = elid;
				$("#"+elid+"box").slideDown();
			} else {
				selectedTab = null;
				$(".tab").removeClass("tabselected");
			}
			$("#tab").val(elid.substr(3));
		});
	});
	
</script>
<section class="content-header">
  <h1>
     <img src="<?php echo ADMIN_IMAGE_PATH;?>/testimonial.jpg" width="30" height="30" /> COMMENTS / SUGGESTIONS MANAGER
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<?php showMessage();	?>
			<form name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<div class="form-group col-md-12">
					<div class="col-md-9">
						<div class="col-md-3">Search by name:
						</div>
						<div class="col-md-3">
							<input type="text" name="Keyword" id="Keyword" value="<?php echo $_REQUEST['Keyword']?>" class="form-control" size="19" />
						</div>
						<div class="col-md-2"><input type="submit" value="Search" class="btn btn-default" />
						</div>
						
					</div>
					<div class="col-md-3" style="text-align: right;float: right;">
						<span class="column_head" style="float:right; padding-top:2px;">Total No of Results:&nbsp;<?php echo count($cms)?></span>
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="col-md-6">
							<input type="button" value="SHOW ALL" class="btn btn-default" onclick="window.location='index.php?p=comment_list'" />
						
					</div>
					<div class="col-md-6" style="text-align: right;float: right;">
						<?php echo $objPaging->show_paging()?>
					</div>
				</div>

<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
					<tr height="25" class="SmallBlackHeading">
						<th width="4%" align="center" class="BottomBorder">&nbsp;</th>
						<th width="4%" align="center" class="BottomBorder">#</th>
						<th width="" align="left" class="BottomBorder">Name</th>
						<th width="" align="left" class="BottomBorder">Email</th>
						<th width="" align="left" class="BottomBorder">Video</th>
                        <th width="15%" align="left" class="BottomBorder">Date</th>
						<th align="center" width="15%" class="BottomBorder">Options</th>
					</tr></thead>
<tbody>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr>
						<td align="center">&nbsp;</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo stripslashes($cms[$i]['name']);?></td>
						<td align="left"><?php echo stripslashes($cms[$i]['email']);?></td>
						<td align="left">
							<?php 
							if($cms[$i]['sermon_id']=='0') echo "Live Broadcast Video";
							else{
								$sermon = FetchData("sermon",array(),"where id = '".$cms[$i]['sermon_id']."'");
								echo stripslashes($sermon[0]['title']);
							}
							?>
						</td>
                        <td align="left"><?php echo date("d/m/Y H:i:s",strtotime($cms[$i]['created']));?></td>
						<td align="center" class="BlackMediumNormal"> <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=comment_detail&id=<?php echo $cms[$i]['id']; ?>';" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp; </td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?></tbody>
	</table>
</div>
<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
					</div>
				</div>

				<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php echo $objPaging->show_paging()?>
					</div>
				</div>

			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
