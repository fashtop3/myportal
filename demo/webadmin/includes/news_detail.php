<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"news"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchData("news",array(),"WHERE id =".$_REQUEST['id']);
		
?>
<section class="content-header">
  <h1>
   <img src="<?php echo ADMIN_IMAGE_PATH;?>/news_detail.png" width="30" height="30" /> NEWS DETAIL
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
         
			<table width="100%" class="form">
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Title : <?php echo stripslashes($Info[0]['title'])?> </b> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Date : <?php echo $Info[0]['created']?> </b> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Short Description : </b> <?php echo stripslashes($Info[0]['short_description'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;"><br />
						<?php 
												if($Info[0]['video']!=''){
													echo "<b>News Video:</b> <br>";
													$u = $_SERVER['REQUEST_URI'];	
																	if(strpos($u,'webadmin')>0)
																		$uri = substr($u,0,strpos($u,'webadmin'));
																	else
																		$uri = $u;
														//			$uri1 = $uri.'index.php?p=event_detail&id='.$_REQUEST['id'];
																	$filepath = $uri."uploads/video/".stripslashes($Info[0]['video']);
											  ?>
						<div id="container"></div>
						<script type="text/javascript" src="../Scripts/swfobject.js"></script>
						<script type="text/javascript">
															var s1 = new SWFObject("../images/player.swf","ply","350","320","9","#FFFFFF");
															s1.addParam("allowfullscreen","true");
															s1.addParam("allowscriptaccess","always");
															s1.addParam("flashvars","file=<?php echo $filepath;?>");
															//s1.write('preview');
															s1.write("container");
															</script>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Full Description:</b> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;"><?php echo stripslashes($Info[0]['full_description'])?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						
							<?php if($Info[0]['status']=='pending'){ ?>
							<input value="APPROVE" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=news_list&action=ApproveStatus&id=<?php echo $Info[0]['id']?>&pg_no=<?php echo $_REQUEST['pg_no']?>';" />
							<?php } ?>
							<input value="BACK" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=news_list&pg_no=<?php echo $_REQUEST['pg_no']?>';" />
						
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
