<?php
	include('../../utils/config.php');
	include('../../utils/dbclass.php');
	include('../../utils/functions.php');
	$objDB = new MySQLCN;

	$PANEL_HEADING="Prayer Detail";
	$Tbl="prayer";
	
	// -------------- REQUEST A CALL -----------------
	$SQL = FetchQuery("county ",array('c_name, c_email'),'where c_id='.$_REQUEST['id'],'','');
	$RecordSet = $objDB->select($SQL);		
	
	if(isset($_REQUEST['process']) && !empty($_REQUEST['process']) && $_REQUEST['process']=="SendReply"){
		$Subject = "Request a Prayer";
		$To = stripslashes($RecordSet[0]['c_email']);
		$From = "Apostolic Faith Mission UK";
		
		$Template="../../mail_templates/send_prayer.html";
		$TemplateVars=array(
							'Desc'=>$_REQUEST['desc']);
			
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
		if($flag){
			echo '<script type="text/javascript">';
			echo 'alert("Mail Sent Successfully!");';
			echo 'window.close();';
			echo '</script>';
		}else{
			echo '<script type="text/javascript">';
			echo 'alert("Error! while sending Mail.");';
			echo 'window.close();';
			echo '</script>';
		}
	}
?>
<link href="../css/style.css" type="text/css" rel="stylesheet" /> 
<body bgcolor="#E6E3BD">
<form name="FrmSendPrayer" method="post" action="">
<input type="hidden" name="process" value="SendReply">
<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" >
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          	<table width="100%" border="0" bgcolor="#F2EFE6" cellspacing="0" cellpadding="5" class="tbl_border">
	<tr>
		<td height="25" colspan="2" align="center" class="tbl_head"><?php echo $PANEL_HEADING?></td>			
	</tr>
	<tr>
      <th align="right" class="SmallBlackMedium" scope="row">Place</th>
	  <td class="SmallBlackNormal"><?php echo stripslashes($RecordSet[0]['c_name']); ?></td>
    </tr>
	<tr>
		<th width="18%" align="right" valign="top" class="SmallBlackMedium" scope="row">Email</th>
		<td width="82%" class="SmallBlackNormal"><?php echo stripslashes($RecordSet[0]['c_email'])?></td>
	</tr>
    <tr>
      <th align="right" valign="top" class="SmallBlackMedium" scope="row">Description</th>
	  <td class="SmallBlackNormal"><textarea name="desc" rows="5" cols="40" ></textarea></td>
    </tr>
	<tr>
		<th scope="row">&nbsp;</th>
		<td><input type="submit" value="Reply" class="Btn" /></td>
	</tr>
</table>
</form>
</body>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
