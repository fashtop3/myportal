<?php
/*	include('../../utils/config.php');
	include('../../utils/dbclass.php');
	include('../../utils/functions.php');
	$objDB = new MySQLCN;*/

	$PANEL_HEADING="SEND SMS";
	$Tbl="prayer";
	
/*	// -------------- REQUEST A CALL -----------------
	$SQL = FetchQuery("county ",array('c_name, c_email'),'where c_id='.$_REQUEST['id'],'','');
	$RecordSet = $objDB->select($SQL);		
*/	
if($_REQUEST['User']!=''){
	if(isset($_REQUEST['process']) && !empty($_REQUEST['process']) && $_REQUEST['process']=="SendSMS"){
	//print_r($_REQUEST);exit;
	$_SESSION['SuccessMsg'] = "Currently API is not integrated . So message can not be sent";	
	header("Location:index.php?p=sms_list");
	exit;
		/*$Subject = "Request a Prayer";
		$To = $RecordSet[0]['c_email'];
		$From = "Apostolic Faith Mission UK";
		
		$Template="../../mail_templates/send_prayer.html";
		$TemplateVars=array(
							'Desc'=>$_REQUEST['desc']);
			
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
		if($flag){
			echo '<script type="text/javascript">';
			echo 'alert("Mail Sent Successfully!");';
			echo 'window.close();';
			echo '</script>';
		}else{
			echo '<script type="text/javascript">';
			echo 'alert("Error! while sending Mail.");';
			echo 'window.close();';
			echo '</script>';
		}*/
	}
}else if ($_REQUEST['process']=="SendSMS")
	$fl = 1;


?>
<style type="text/css">
label {
	width: 10em;
	float: left;
}
label.error {
	float: none;
	color: red;
	padding-left: .5em;
	vertical-align: top;
}
.submit {
	margin-left: 12em;
}
em {
	font-weight: bold;
	padding-right: 1em;
	vertical-align: top;
}
</style>
<!-- <script language="javascript" type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jqueryui.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.validate.js"></script> -->
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#FrmSendPrayer").validate({
		rules: {
			User: "required",
			Message: "required"			
		},
		messages: {
			User: "Please select user",
			Message: "Please enter message"
		}
	});
  });
</script>
<section class="content-header">
  <h1>
    <?php echo $PANEL_HEADING?>
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <form class="form-horizontal" name="FrmSendPrayer" method="post" action="" id="FrmSendPrayer">
	<input type="hidden" name="process" value="SendSMS">
	<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" >
	<?php if($fl==1) echo "Please Select User";?>
	
		<?php if($_SESSION['AdminID']==1){ ?>
		<div class="form-group col-md-12">
	      <label for="inputEmail" class="col-sm-2 control-label">Place</label>
	      <div class="col-sm-5">
			<select name="PlaceValue" class="form-control" id="PlaceValue" onChange="if(this.value!='') window.location='index.php?p=send_bulk_sms&plc='+this.value; else window.location='send_bulk_sms.php';">
					<option value="">All Places</option>
					<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
				</select>
				
	      </div>
	    </div><?php } ?>
		<div class="form-group col-md-12">
	      <label for="inputEmail" class="col-sm-2 control-label">User</label>
	      <div class="col-sm-5">
			<select name="User[]" id="User" multiple class="form-control" style="height:85px;">
					<?php
						$sql = "select * from sms";
						if($_SESSION['AdminID']==1){
							if($_REQUEST['plc']!='')
								$sql.=' where place_id='.$_REQUEST['plc'];
							else
								$sql.='';	
						}
						else
							$sql.=' WHERE place_id='.$_SESSION['PlaceID'];
						$sms = $objDB->select($sql);
						for($i=0;$i<count($sms);$i++)
						{
							echo "<option value='".$sms[$i]['sms_mobile_no']."'>".stripslashes($sms[$i]['sms_name'])."&nbsp;&nbsp;&nbsp;(".stripslashes($sms[$i]['sms_mobile_no']).")</option>";
						}
					?>
				</select>
	      </div>
	    </div>
	    <h5>Select users you want to send SMS. (Ctrl+Click to select more than one user)</h5>
	    <div class="form-group col-md-12">
	      <label for="inputEmail" class="col-sm-2 control-label">Message</label>
	      <div class="col-sm-5">
			<textarea name="Message" rows="5" cols="30" id="Message" class="form-control"><?php echo $_REQUEST['Message'];?></textarea>
	      </div>
	    </div>
		<div class="form-group">

                <div class="col-sm-offset-2 col-sm-10">
                  
<input type="submit" value="Send" class="btn btn-success" />
				<input type="button" value="Back" class="btn btn-default btn btn-default-default" onClick="window.location='index.php?p=sms_list'" />
                </div>

              </div>
</form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
<!-- <link href="../css/style.css" type="text/css" rel="stylesheet" />
<body bgcolor="#E6E3BD">

</body>
 -->