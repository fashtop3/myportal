<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"sermon"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<section class="content-header">
  <h1>
    SERMON VIDEO DETAIL
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/video.jpg" width="48" height="48" /> </td>
		<td class="tbl_head" height="24">SERMON VIDEO DETAIL</td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="form" width="100%">
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Title : </b><?php echo stripslashes($Info[0]['title'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>By : </b>
						<?php	echo $Info[0]['sermonby'];		
			//echo stripslashes(FetchValue1('member','firstname,lastname','id',$Info[0]['createdby']));
		?>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>
						<?php
			
			echo stripslashes(FetchValue1('occation','title','id',$by[0]['occation']));
		?>
						</b> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Video Date : </b><?php echo $Info[0]['sdate']?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;">
						<?php if($Info[0]['video']!=''){?>
						MP4
						<div id="container"></div>
						<script type="text/javascript" src="../Scripts/swfobject.js"></script>
						<script type="text/javascript">
							var s1 = new SWFObject("../images/player.swf","ply","350","320","9","#FFFFFF");
							s1.addParam("allowfullscreen","true");
							s1.addParam("allowscriptaccess","always");
							s1.addParam("flashvars","file=.<?php echo stripslashes($Info[0]['video'])?>");
							s1.write("container");
	          </script>
						<br />
						<?php } ?>
						<?php if($Info[0]['flash']!=''){?>
						FLV
						<div id="container1"></div>
						<script type="text/javascript" src="../Scripts/swfobject.js"></script>
						<script type="text/javascript">
							var s1 = new SWFObject("../images/player.swf","ply","350","320","9","#FFFFFF");
							s1.addParam("allowfullscreen","true");
							s1.addParam("allowscriptaccess","always");
							s1.addParam("flashvars","file=.<?php echo stripslashes($Info[0]['flash'])?>");
							s1.write("container1");
	          </script>
						<br />
						<?php } ?>
						<?php if($Info[0]['audio']!=''){?>
						WMV<br />
						<object ID="MediaPlayer" WIDTH="192" HEIGHT="190" CLASSID="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95" STANDBY="Loading Windows Media Player components..." TYPE="application/x-oleobject">
							<param NAME="FileName" VALUE=".<?php echo stripslashes($Info[0]['audio'])?>">
							<param name="ShowControls" VALUE="true">
							<param name="ShowStatusBar" value="false">
							<param name="ShowDisplay" VALUE="false">
							<param name="autostart" VALUE="false">
							<embed TYPE="application/x-mplayer2" SRC=".<?php echo stripslashes($Info[0]['audio'])?>" NAME="MediaPlayer" WIDTH="350" HEIGHT="320" ShowControls="1" ShowStatusBar="0" ShowDisplay="0" autostart="0"> </embed>
						</object>
						<?php } ?>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;"><?php echo stripslashes($Info[0]['description'])?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<center>
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=video_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						</center>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
