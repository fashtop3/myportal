<?php
ob_start();
session_start();
require_once("../../utils/config.php");
require_once("../../utils/functions.php");
require_once("../../utils/dbclass.php");
$objDB = new MySQLCN;
?>
<script type="text/javascript">
$(function() {	
	$('#FromDate').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: '1975:<?php echo date('Y')+15?>',
		dateFormat: 'yy-mm-dd'
	});			
}); 
</script>
<link href="css/ajaxfileupload.css" type="text/css" rel="stylesheet">
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
<script type="text/javascript">
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function ltrim(stringToTrim) {
	return stringToTrim.replace(/^\s+/,"");
}
function rtrim(stringToTrim) {
	return stringToTrim.replace(/\s+$/,"");
}	

function ajaxFileUpload()
{	
	var frm = document.frmPage;
	var CID = "";
	Process = $('input[name=Process]').val();
	Category = $('input[name=Category]').val();		
	
	var x = document.frmPage.Position.selectedIndex;
	Position = document.frmPage.Position.options[x].value;
	if(Position.length==0){
		alert("Please select Position");
		return false;
	}
	Username = $('input[name=Username]').val();
	if(Username.length>0){
		Username = ltrim(Username);
		Username = rtrim(Username); 
	} 	
	if(Username.length==0){
		alert("Please enter Username");
		return false;
	}
	Password = $('input[name=Password]').val();
	if(Password.length>0){
		Password = ltrim(Password);
		Password = rtrim(Password);  
	}
	if(Password.length==0){
		alert("Please enter Password");
		return false;
	}
	FirstName = $('input[name=FirstName]').val();
	if(FirstName.length>0){
		FirstName = ltrim(FirstName);
		FirstName = rtrim(FirstName);  
	}
	if(FirstName.length==0){
		alert("Please enter FirstName");
		return false;
	}
	LastName = $('input[name=LastName]').val();
	if(LastName.length>0){
		LastName = ltrim(LastName);
		LastName = rtrim(LastName);  
	}
	if(LastName.length==0){
		alert("Please enter LastName");
		return false;
	}
	Email = $('input[name=Email]').val();
	if(Email.length>0){
		Email = ltrim(Email);
		Email = rtrim(Email);  
	}
	if(Email.length==0){
		alert("Please enter Email");
		return false;
	}
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;	
	if(reg.test(Email) == false) {
		alert('Invalid Email Address');
		return false;
	}

	Phone = $('input[name=Phone]').val();
	if(Phone.length>0){
		Phone = ltrim(Phone);
		Phone = rtrim(Phone);  
	}
	if(Phone.length==0){
		alert("Please enter Phone number");
		return false;
	}
	File1 = $('input[name=fileToUpload]').val();
	if(File1.length>0){
		File1 = ltrim(File1);
		File1 = rtrim(File1);
	}
 		
	Address = $('input[name=Address]').val();	
	Address2 = $('input[name=Address2]').val();
	City = $('input[name=City]').val();
	State = $('input[name=State]').val();
	Country = $('input[name=Country]').val();
	Zipcode = $('input[name=Zipcode]').val();
	Place = $('input[name=Place]').val();
		
	var data = '?Actions=FileUpload';
	data += "&Username="+Username;
	data += "&Password="+Password;
	data += "&File1="+File1;
	data += "&FirstName="+FirstName;
	data += "&LastName="+LastName;	 
	data += "&Address="+Address;
	data += "&Address2="+Address2;
	data += "&City="+City;	
	data += "&State="+State;	  
	data += "&Country="+Country;	
	data += "&Zipcode="+Zipcode;	
	data += "&Position="+Position;	
	data += "&Email="+Email;	
	data += "&Phone="+Phone;
	data += "&Place="+Place;	
	$("#loading")
	.ajaxStart(function(){
		$(this).show();
	})
	.ajaxComplete(function(){
		$(this).hide();
	});	
	
	$.ajaxFileUpload
	(
		{
			url:'manage_member.php'+data,
			secureuri:false,
			fileElementId:'fileToUpload',
			dataType: 'json',
			success: function (data, status)
			{
				if(typeof(data.error) != 'undefined')
				{
					if(data.error != '')
					{
						alert(data.error);
					}else
					{
						//$("#Datesheet").html(data.msg);
						$.post("member_list.php",{ id:data.msg } ,function(data1)
						{
							if(data1=='User'){
								$('.user').fadeIn('slow');	
							}else if(data1=='email'){
								$('.Email').fadeIn('slow');	
							}else if(data1=='useremail'){
								$('.userEmail').fadeIn('slow');	
							}else{
								$("#Member").html(data1);
								$('.form1').fadeOut('slow');					
								$('.done').fadeIn('slow');			
							}
						});													
					}
				}
			},
			error: function (data, status, e)
			{
				alert(e);
			}
		}
	)
	
	return false;

}
</script>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="100%" cellpadding="5" cellspacing="5" border="0">
	<tr>
		<td align="left" valign="top">
			<h1 style="font-size:14px">Add New Member</h1>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">
			<div class="done" id="complete">
				<table width="98%" cellpadding="1" cellspacing="1">
					<tr>
						<td align="center"> Member added successfully! </td>
					</tr>
				</table>
			</div>
			<div class="user" id="complete">
				<table width="98%" cellpadding="1" cellspacing="1">
					<tr>
						<td align="center"> Username already exists! </td>
					</tr>
				</table>
			</div>
			<div class="Email" id="complete">
				<table width="98%" cellpadding="1" cellspacing="1">
					<tr>
						<td align="center"> Email Address already exists! </td>
					</tr>
				</table>
			</div>
			<div class="userEmail" id="complete">
				<table width="98%" cellpadding="1" cellspacing="1">
					<tr>
						<td align="center"> Username and Email Address already exists! </td>
					</tr>
				</table>
			</div>
			<div id="PlanForm" class="form1"> <img id="loading" src="images/busy.gif" style="display:none;">
				<form action="" method="post" name="frmPage" id="frmPage" enctype="multipart/form-data">
					<input type="hidden" name="Process" id="Process" value="DatesheetAdd" />
					<input type="hidden" name="Category" id="Category" value="1" />
					<input type="hidden" name="Type" id="Type" value="<?php echo $_REQUEST['type']?>" />
					<input type="hidden" name="FieldName" id="FieldName" value="<?php echo $_REQUEST['fieldname']?>" />
					<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
						<tbody>
							<tr>
								<td>
									<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
										<tbody>
											<?php if($_SESSION['AdminID']==1){ 
											
										?>
											<tr>
												<td class="fieldlabel" width="5%">Place</td>
												<td class="fieldarea">
													<select name="Place" class="InputBox" id="Place" style="width:300px;">
														<option value="">No Place</option>
														<?php echo FillCombo1('county','c_name','c_id',$pl,'');?>
													</select>
												</td>
											</tr>
											<?php }else{ ?>
											<tr>
												<td colspan="2">
													<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
												</td>
											</tr>
											<?php } ?>
											<tr>
												<td class="fieldlabel" width="5%">
													<label for="Place">Position</label>
												</td>
												<td class="fieldarea">
													<select name="Position" class="InputBox" id="Position" style="width:300px;">
														<option value="">--Select Position--</option>
														<?php
														echo FillCombo1('member_position','title','id',$Info[0]['position'],'');
													?>
													</select>
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">User Name</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="Username" id="Username" value="<?php echo $Info[0]['username']?>" style="width:300px;" />
													<span id="msgbox" style="display:none"></span> </td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">Password</td>
												<td class="fieldarea">
													<input class="InputBox" type="password" name="Password" id="Password" value="<?php echo $Info[0]['password']?>" style="width:300px;" />
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">First Name</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="FirstName" id="FirstName" value="<?php echo stripslashes($Info[0]['firstname'])?>" style="width:300px;" />
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">Last Name</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="LastName" id="LastName" value="<?php echo stripslashes($Info[0]['lastname'])?>" style="width:300px;" />
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">Address</td>
												<td class="fieldarea">
													<textarea class="InputBox" name="Address" id="Address" style="width:300px;"><?php echo stripslashes($Info[0]['address'])?></textarea>
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">Address 2</td>
												<td class="fieldarea">
													<textarea class="InputBox" name="Address2" id="Address2" style="width:300px;"><?php echo stripslashes($Info[0]['address2'])?></textarea>
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">City</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="City" id="City" value="<?php echo stripslashes($Info[0]['city'])?>" style="width:300px;" />
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">State</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="State" id="State" value="<?php echo stripslashes($Info[0]['state'])?>" style="width:300px;" />
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">Country</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="Country" id="Country" value="<?php echo stripslashes($Info[0]['country'])?>" style="width:300px;" />
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">Postal/Zip Code</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="Zipcode" id="Zipcode" value="<?php echo stripslashes($Info[0]['zipcode'])?>" style="width:300px;" />
												</td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">Email</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="Email" id="Email" value="<?php echo stripslashes($Info[0]['email'])?>" style="width:300px;" />
													<span id="msgbox1" style="display:none"></span> </td>
											</tr>
											<tr>
												<td class="fieldlabel" width="5%">Phone</td>
												<td class="fieldarea">
													<input class="InputBox" type="text" name="Phone" id="Phone" value="<?php echo stripslashes($Info[0]['phone'])?>" style="width:300px;" />
												</td>
											</tr>
											<tr>
												<td class="fieldlabel">Photo/Image</td>
												<td class="fieldarea" valign="top">
													<input name="fileToUpload" id="fileToUpload" class="InputBox" accept="JPG|GIF|JPEG|PNG" type="file" />
												</td>
											</tr>
										</tbody>
									</table>
								</td>
							</tr>
							<tr>
								<td>
									<input type="hidden" value="<?php echo $MODE?>" name="action" />
									<input value="Add Member" class="Btn" type="button" name="submit1" id="RegisterSubmit" onclick="return ajaxFileUpload();" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</td>
	</tr>
	<tr>
		<td align="left" valign="top">&nbsp;</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
