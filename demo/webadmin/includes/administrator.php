<?php
	$sql = "select * from admin";
	$sql.=' WHERE UserID!=1';

	$sql .= " order by UserID desc";
	$objPaging = new paging($sql, "UserID",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_administrator.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_administrator.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
</script>

<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/member.jpg" width="30" height="30" /> ADMINISTRATOR MANAGER
    
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<?php showMessage();	?>
			<form class="form-horizontal" name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<div class="form-group col-md-12">
					<div class="col-md-6">
						
					</div>
					<div class="col-md-6" style="text-align: right;float: right;">
						<span class="column_head">Total No of Results:&nbsp;<?php echo count($cms)?></span>
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="col-md-6">
						<input type="Button" name="ADDNEW" value="ADD NEW" class="btn btn-default" onClick="window.location.href='index.php?p=administrator_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
					</div>
					<div class="col-md-6" style="text-align: right;float: right;">
						<span class="column_head"><?php echo $objPaging->show_paging()?></span>
					</div>
				</div>
				<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
					<tr height="25" class="SmallBlackHeading">
						<th width="4%" align="center" class="BottomBorder">
							<!--input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);"-->
						</th>
						<th width="5%" align="center" class="BottomBorder">#</th>
						<th width="" align="left" class="BottomBorder">Name</th>
						<th width="" align="left" class="BottomBorder">Place</th>
						<th align="center" class="BottomBorder">Options</th>
					</tr></thead>
<tbody>

					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr>
						<td align="center">
							<!--input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i]['UserID']?>"-->
						</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo ucwords(stripslashes($cms[$i]['FirstName']." ".$cms[$i]['LastName'])); ?></td>
						<td align="left"><?php echo stripslashes(FetchValue1('county','c_name','c_id',$cms[$i]['place_id'])); ?></td>
						<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=administrator_addedit&id=<?php echo $cms[$i]['UserID']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['UserID']?>', 'DELETE');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=administrator_detail&id=<?php echo $cms[$i]['UserID']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>';" />&nbsp; </td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?></tbody>
	</table>
</div>
<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
					</div>
				</div>

				<div class="form-group col-md-12">
					<div class="col-md-6"><!--input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);"-->
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php echo $objPaging->show_paging()?>
					</div>
				</div>
					
			</form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>