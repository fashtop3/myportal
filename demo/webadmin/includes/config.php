<?php
$sql = "select * from configuration";
$c = $objDB->select($sql);
$i=0;
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#configfrm").validate();
  });
</script>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
$(".tabbox").css("display","none");
var selectedTab;
$(".tab").click(function(){
    var elid = $(this).attr("id");
    $(".tab").removeClass("tabselected");
    $("#"+elid).addClass("tabselected");
    if (elid != selectedTab) {
        $(".tabbox").slideUp();
        $("#"+elid+"box").slideDown();
        selectedTab = elid;
    }
    $("#tab").val(elid.substr(3));
});
selectedTab = "tab0";
$("#tab0").addClass("tabselected");
$("#tab0box").css("display","");
  });
</script>
<style type="text/css">

td.fieldarea {
    padding-bottom: 15px;
}</style>
<!-- <link href="css/style1.css" rel="stylesheet" type="text/css"> -->
<section class="content-header">
  <h1>
   <img src="<?php echo ADMIN_IMAGE_PATH;?>/config.jpg" width="30" height="30" /> GENERAL SETTINGS
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
				<?php
					if(isset($_REQUEST['done']))	showMessage();			
				?>
				<form method="post" action="manage_config.php" name="configfrm" id="configfrm">
          <input name="action" value="UPDATE" type="hidden">
<div class="nav-tabs-custom">
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab_1" data-toggle="tab">General</a></li>
    <li><a href="#tab_2" data-toggle="tab">Mail</a></li>
    <li><a href="#tab_3" data-toggle="tab">Uploads</a></li>
    <li><a href="#tab_4" data-toggle="tab">Security</a></li>
    <li><a href="#tab_5" data-toggle="tab">Others</a></li>
    
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab_1">
        <div id="">
              <table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
                <tbody>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" title="*" validate="required:true" size="35" type="text" class="form-control">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true,email:true" class="form-control" type="text" id="ConfigValue<?php echo $i?>">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true,digits:true,min:1" class="form-control" type="text" id="ConfigValue<?php echo $i?>">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
    </div><!-- /.tab-pane -->
    <div class="tab-pane" id="tab_2">
      <div id="">
              <table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
                <tbody>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <textarea class="form-control" name="ConfigValue<?php echo $i?>" rows="4" title="*" validate="required:true" cols="60"><?php echo htmlspecialchars(stripslashes($c[$i]['config_value']))?></textarea>
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo $c[$i]['config_key']?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo $c[$i]['config_title']?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo $c[$i]['config_title']?> </td>
                    <td class="fieldarea">
                      <input class="form-control" name="ConfigValue<?php echo $i?>" title="*" validate="required:true" size="60" value="<?php echo htmlspecialchars($c[$i]['config_value'])?>" />
                      <?php echo $c[$i]['config_description']?>
                      <input type="hidden" value="<?php echo $c[$i]['config_key']?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true,email:true" class="form-control" type="text" id="ConfigValue<?php echo $i?>">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
    </div><!-- /.tab-pane -->
    <div class="tab-pane" id="tab_3">
      <div id="">
              <table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
                <tbody>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true" class="form-control" type="text" id="ConfigValue<?php echo $i?>">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true" class="form-control" type="text" id="ConfigValue<?php echo $i?>">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true" class="form-control" type="text" id="ConfigValue<?php echo $i?>">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
    </div><!-- /.tab-pane -->
    <div class="tab-pane" id="tab_4">
        <div id="">
              <table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
                <tbody>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true,min:6" class="form-control" type="text" id="ConfigValue<?php echo $i?>">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
    </div><!-- /.tab-pane -->
    <div class="tab-pane" id="tab_5">
        <div id="">
              <table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
                <tbody>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <textarea rows="5" cols="35" title="*" validate="required:true" class="form-control" id="ConfigValue<?php echo $i?>" name="ConfigValue<?php echo $i?>"><?php echo stripslashes($c[$i]['config_value'])?></textarea>
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <table cellpadding="0" cellspacing="0">
                        <tbody>
                          <tr>
                            <td>
                              <select style="width:229px;" name="ConfigValue<?php echo $i?>" title="*" validate="required:true" class="form-control">
                                <?php echo FillCombo1('county','c_name','c_id',$c[$i]['config_value'],'');?>
                              </select>
                            </td>
                            <td>&nbsp;<?php echo stripslashes($c[$i]['config_description'])?>
                              <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" title="*" validate="required:true" value="<?php echo stripslashes($c[$i]['config_value'])?>" class="form-control" size="35" type="text">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" title="*" validate="required:true" value="<?php echo stripslashes($c[$i]['config_value'])?>" class="form-control" size="35" type="text">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" title="*" validate="required:true,email:true" value="<?php echo stripslashes($c[$i]['config_value'])?>" class="form-control" size="35" type="text">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                  <tr>
                    <td class="fieldlabel">
                      <input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
                      <?php echo stripslashes($c[$i]['config_title'])?> </td>
                    <td class="fieldarea">
                      <input name="ConfigValue<?php echo $i?>" title="*" validate="required:true" value="<?php echo stripslashes($c[$i]['config_value'])?>" class="form-control" size="35" type="text">
                      <?php echo stripslashes($c[$i]['config_description'])?>
                      <input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
    </div><!-- /.tab-pane -->
  </div><!-- /.tab-content -->
</div><!-- nav-tabs-custom -->
  <input value="Save Changes" class="btn btn-success" class="button" type="submit" />
          </p>
          <input name="tab" id="tab" value="" type="hidden">
        </form>
				
					

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
