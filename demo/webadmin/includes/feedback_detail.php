<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"feedback"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>

<section class="content-header">
  <h1>
  <img src="<?php echo ADMIN_IMAGE_PATH;?>/testimonial.jpg" width="30" height="30" /> FEEDBACK DETAIL
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          
			<table class="form" width="100%">
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Name : </b><?php echo stripslashes($Info[0]['name'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Email : </b> <?php echo stripslashes($Info[0]['email']);?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Date : </b><?php echo $Info[0]['created']?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Comment : </b> <?php echo stripslashes($Info[0]['comment']);?> </td>
				</tr>
				<tr>
					<td colspan="2">
							<input value="BACK" class="btn bn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=feedback_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>