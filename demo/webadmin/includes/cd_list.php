<?php
	$sql = "select * from cd";
	if($_SESSION['AdminID']==1){		
		$sql.=' where 1=1';	
		$path1 = '';
	}	else{
		$sql.=' where createdby = '.$_SESSION['AdminID'];	
	}
	if($_REQUEST['plc']!=''){
			$sql.=' and type="'.$_REQUEST['plc'].'"';
			$path1 = '&plc='.$_REQUEST['plc'];
		}
		else{
			$sql.=' ';	
			$path1 = '';
		}
	if($_REQUEST['admins']!='')
		$sql .= ' and createdby = '.$_REQUEST['admins'];
		
	if($_REQUEST['Search']!='')
		$sql .= " and title like '%".$_REQUEST['Search']."%' or item_no like '%".$_REQUEST['Search']."%'";
	
	if($_REQUEST['view']=='my_view'){	
		$sql .= ' and createdby = '.$_SESSION['AdminID'];
		$view = strtoupper("All Music &amp; Tracks");
		$viewpath = 'index.php?p=cd_list'.$path1;
		$path2 = '&view=my_view';
	}
	else{
		$view = strtoupper("My Added Music &amp; Tracks");	
		$viewpath = 'index.php?p=cd_list&view=my_view'.$path1;
		$path2 = '';
	}
	
	$sql .= " order by id desc";
	
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_cd.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_cd.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/contactcategory.jpg" width="30" height="30" /> CD/DVD/Higherway Magazine/MUSIC &amp; Tracks
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
         
			<?php showMessage();?>
			<form name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="" />
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="" />
				<div class="form-group col-md-12">
					<div class="col-md-8">
						<?php if($_SESSION['AdminID']==1){ ?>
						<div class="col-md-3">
							<select name="admins" class="form-control" id="admins" onchange="if(this.value!='') window.location='index.php?p=cd_list&admins='+this.value; else window.location='index.php?p=cd_list';">
								<option value="">--Select Administrator--</option>
								<?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
							</select>
						</div>
						<div class="col-md-3">
							<select name="PlaceValue" class="form-control" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=cd_list&plc='+this.value+'<?php echo $path2; ?>'; else window.location='index.php?p=cd_list'+'<?php echo $path2; ?>';">
								<option value="">All Types</option>
								<option <?php if($_REQUEST['plc']=='CD') echo "selected";?> value="CD">CDs</option>
								<option <?php if($_REQUEST['plc']=='DVD') echo "selected";?> value="DVD">DVDs</option>
								<option <?php if($_REQUEST['plc']=='HM') echo "selected";?> value="HM">Higherway Magazine</option>
								<option <?php if($_REQUEST['plc']=='MT') echo "selected";?> value="MT">Music &amp; Track</option>
							</select>
						</div>
						<?php } ?>
						<div class="col-md-3">
							Search (Title, Item No):
						</div>
						<div class="col-md-3">
							<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="form-control" />
						</div>
						<div class="col-md-1">
							<input type="submit" value="Search" class="btn btn-default" />
						</div>
					</div>
					<div class="col-md-4" style="text-align: right;float: right;">
						<span class="column_head" style="float:right; padding-top:5px;">Total No of Results:&nbsp;<?php echo count($cms)?></span>
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="col-md-8">
						
						<input type="Button" name="ADDNEW" value="ADD NEW" class="btn btn-default" onClick="window.location.href='index.php?p=cd_new&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
						
							<input type="Button" name="sermons" value="<?php echo $view?>" class="btn btn-default" onClick="window.location.href='<?php echo $viewpath; ?>'"  style="width:200px;"/>
						
							<input type="button" value="SHOW ALL" class="btn btn-default" onclick="window.location='index.php?p=cd_list'" />
						
					</div>
					<div class="col-md-4" style="text-align: right;float: right;">
						<?php echo $objPaging->show_paging()?>
					</div>
				</div>
				<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
					<tr height="25" class="SmallBlackHeading">
						<th width="4%" align="center" class="BottomBorder">
							<!--input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);"-->
						</th>
						<th width="5%" align="center" class="BottomBorder">#</th>
						<th width="" align="left" class="BottomBorder">Title</th>
						<th width="" align="left" class="BottomBorder">Item Number</th>
						<th width="" align="left" class="BottomBorder">Category</th>
						<th align="center" class="BottomBorder">Options</th>
					</tr>
					</thead>
					<tbody>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
						<!-- bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';" -->
					<tr>
						<td align="center"></td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo ucwords(stripslashes($cms[$i]['title'])); ?></td>
						<td align="left"><?php echo stripslashes($cms[$i]['item_no']); ?></td>
						<td align="left"><?php echo stripslashes(FetchValue("cd_category","title","id",$cms[$i]['category_id'])); ?> (<?php echo str_replace("MT","Music &amp; Tracks",str_replace("HM","Higherway Magazine",$cms[$i]['type'])); ?>)</td>
						<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=cd_new&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp; 
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
			</tbody>
	</table>
	<div class="form-group col-md-12">
					<div class="col-md-6"><?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
					</div>
					<div class="col-md-6" style="text-align:right:float:right">
						<?php echo $objPaging->show_paging()?>
					</div>
				</div>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
