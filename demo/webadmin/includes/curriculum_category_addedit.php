<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"curriculum_category"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$Photo = "<img src=../uploads/curriculum/image/big/".stripslashes($Info[0]['image'])." width=75 height=75>";	
}else
	$MODE="ADD";

if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $Info[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}
?>
<script>
  $(document).ready(function(){
  	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#Content').val(con); // put it in the textarea
	});
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required"
			
		},
		messages: {
			Title: "Please enter title"
		}
	});	
  });
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "Content",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
/*            return false;			
			var fileBrowserWindow = new Array();
			fileBrowserWindow["file"] = ajaxfilemanagerurl;
			fileBrowserWindow["title"] = "Ajax File Manager";
			fileBrowserWindow["width"] = "782";
			fileBrowserWindow["height"] = "440";
			fileBrowserWindow["close_previous"] = "no";
			tinyMCE.openWindow(fileBrowserWindow, {
			  window : win,
			  input : field_name,
			  resizable : "yes",
			  inline : "yes",
			  editor_id : tinyMCE.getWindowArg("editor_id")
			});
			
			return false;*/
		}
</script>
<section class="content-header">
  <h1>
   <img src="<?php echo ADMIN_IMAGE_PATH;?>/addcategory.jpg" width="30" height="30" /> <?php echo $MODE;?> CURRICULUM CATEGORY
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<form   class="form-horizontal" method="post" action="manage_curriculum_category.php" id="frmAdmin" enctype="multipart/form-data">
			<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
            <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
			<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
			
			<?php if($_SESSION['AdminID']==1){ ?>
			<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Place</label>
                  <div class="col-sm-5">
                    	<select name="Place" class="form-control" id="Place" >
						<?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
					</select>
                  </div>
                </div>
			<?php }else{ ?>
			<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
			<?php } ?>
			<div class="form-group">
              <label for="inputEmail" class="col-sm-2 control-label">Title</label>
              <div class="col-sm-5">
                	<input type="text" name="Title" id="Title" class="form-control" value="<?php echo stripslashes($Info[0]['title'])?>" />
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-2 control-label">Age Limit</label>
              <div class="col-sm-5">
                	<input type="text" name="AgeLimit" class="form-control" id="AgeLimit" value="<?php echo stripslashes($Info[0]['age_limit'])?>" />
              </div>
            </div>
            <div class="form-group">
              <label for="inputEmail" class="col-sm-2 control-label">Image</label>
              <div class="col-sm-5">
                	<input name="CImage" class="" id="CImage"  type="file" />
					<input type="hidden" id="OldImage" name="OldImage" value="<?php echo stripslashes($Info[0]['image'])?>" >
              </div>
            </div>
		
			<?php if($Info[0]['image']!=''){?>
			<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label"></label>
                  <div class="col-sm-5">
                    	<?php echo $Photo;?>
                  </div>
                </div>
			<?php } ?>
			<div class="form-group">
	          <label for="inputEmail" class="col-sm-2 control-label">Description</label>
	          <div class="col-sm-5">
	            	<textarea name="Content" cols="65" rows="15" id="Content" style="width: 500px;" ><?php echo (stripslashes($Info[0]['description']))?></textarea>
	          </div>
	        </div>
			<input type="hidden" value="<?php echo $MODE?>" name="action" />
			  <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">

                            <input value="<?php echo $MODE?> CURRICULUM CATEGORY" class="btn btn-success" type="submit" name="submit1" id="submit1">
				<input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=curriculum_category_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
                            
                          </div>
                        </div>
			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
