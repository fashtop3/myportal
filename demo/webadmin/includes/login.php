<script language="javascript">
function auto_fillup_password (username, password){
	if(username != '' && username == document.login.UserName.value)
		document.login.Password.value = password;	
	else	
		document.login.Password.value = '';	
}
</script>
<?php
if(isset($_REQUEST['Login']))
{
	$user_query="select * from admin where UserName='".($_REQUEST['UserName'])."' and Password='".$_REQUEST['Password']."'";
	$user_res=$objDB->select($user_query);

	if(count($user_res)!=0)
	{
		if(isset($_REQUEST['rpassword']) && $_REQUEST['rpassword'] != ''){
			setcookie('tailor_webadmin_username', ($_REQUEST['UserName']), time() + 3600*24*365);
			setcookie('tailor_webadmin_password', $_REQUEST['Password'], time() + 3600*24*365);
		}
		if(!isset($_SESSION['AdminID']))
		{
			$_SESSION['AdminID']=$user_res[0]['UserID'];
			$_SESSION['AdminName']=stripslashes($user_res[0]['FirstName']." ".$user_res[0]['LastName']);
			$_SESSION['PlaceID'] = $user_res[0]['place_id'];
			$_SESSION['RecordPerPage'] = 10;
		}
		header("location: index.php");
		exit();
	}
	else if(count($user_res)==0)
	{
		header("location: index.php?done=f");
		exit();	
	}
}
?>
 <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <img src="<?php echo ADMIN_IMAGE_PATH;?>/logo.gif" width="114" height="90" alt="Apostolic Faith Mission UK" title="Apostolic Faith Mission UK" border="0" />
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">User Authentication</p>
        <?php if(isset($_REQUEST['done']) && $_REQUEST['done']=="f"){?>
        	<div class="alert alert-danger">
			  <strong>Error!</strong> Invalid User Name Or Password.
			</div>
        <?php } ?>
        <form name="login" action="index.php" method="post">
          <div class="form-group has-feedback">
            <input id="UserName" name="UserName" value="<?php echo (isset($_COOKIE['tailor_webadmin_username'])) ? $_COOKIE['tailor_webadmin_username'] : ''; ?>" class="form-control" placeholder="User Name">
            <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password"  id="Password" name="Password" onfocus="javascript: auto_fillup_password('<?php echo $_COOKIE['tailor_webadmin_username']?>','<?php echo $_COOKIE['tailor_webadmin_password']?>');">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">
              <div class="checkbox icheck">
                <label>
                  <input type="checkbox" name="rpassword" id="rpassword"> Remember Me
                </label>
              </div>
            </div>
            <div class="col-xs-4">
              <input type="submit" name="Login" value="LOGIN" class="btn btn-primary btn-block btn-flat" />
            </div><!-- /.col -->
          </div>
        </form>
        <a href="index.php?p=forgot_password">I forgot my password</a><br>

      </div>
    </div>