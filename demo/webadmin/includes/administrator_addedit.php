<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"admin"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE UserID =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		$Photo = "<img src='../uploads/member/big/".stripslashes($Info[0]['image'])."' width=75 height=75>";			
}else
	$MODE="ADD";
?>
<script>
  $(document).ready(function(){
  	<?php if($MODE=='ADD'){ ?>
  	$("#UserName").blur(function()
		{
			
			//remove all the class add the messagebox classes and start fading
			

		
			//check the username exists or not from ajax
			$.post("name_chk.php",{ username:$(this).val() } ,function(data)
			{
				if(data=='yes') //if username not avaiable
				{
					$("#msgbox").removeClass().addClass('messagebox').text('Checking....').fadeIn("slow");
					$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						$(this).html('Username already exists').addClass('messageboxerror').fadeTo(900,1);
							if(document.frmAdmin.RegisterSubmit.disabled==false)
								document.frmAdmin.RegisterSubmit.disabled=true; 
						
					});
						
				}				
				else if(data=='no') 
				{
					$("#msgbox").removeClass().addClass('messagebox').text('').fadeIn("slow");					
					$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						//$(this).html('Email address is available').addClass('messageboxok').fadeTo(900,1);	
						if(document.frmAdmin.RegisterSubmit.disabled==true){
							document.frmAdmin.RegisterSubmit.disabled=false; 
						}
					});
				}
			});
	});
	
	$("#Email").blur(function()
		{
			
			//remove all the class add the messagebox classes and start fading
			

		
			//check the username exists or not from ajax
			$.post("email_chk.php",{ email:$(this).val() } ,function(data)
			{
				if(data=='yes') //if username not avaiable
				{					
					$("#msgbox1").removeClass().addClass('messagebox').text('Checking....').fadeIn("slow");
					$("#msgbox1").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						$(this).html('Email already exists').addClass('messageboxerror').fadeTo(900,1);
							if(document.frmAdmin.RegisterSubmit.disabled==false)
								document.frmAdmin.RegisterSubmit.disabled=true; 
						
					});				;		
				}				
						
				else if(data=='no') 
				{					
					$("#msgbox1").removeClass().addClass('messagebox').text('').fadeIn("slow");					
					$("#msgbox1").fadeTo(200,0.1,function()  //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						//$(this).html('Email address is available').addClass('messageboxok').fadeTo(900,1);	
						if(document.frmAdmin.RegisterSubmit.disabled==true){
							document.frmAdmin.RegisterSubmit.disabled=false; 
						}
					});
				}
			});
	});
	<?php } ?>
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {			
			UserName: {
				required: true
			},
			Password: {
				required: true,
				minlength: 6,
				maxlength: <?php echo PASSWORD_LIMIT?>
				},
			Email: {
				required: true,
				email: true
			},
			FirstName: "required",
			LastName: "required"			
		},
		messages: {
			UserName: "Please enter username",
			Password: {required: "Please enter password",
				minlength: "Please enter at least 6 characters",
				maxlength: "Password should not be more than <?php echo PASSWORD_LIMIT?> characters"},
			Email: {
				required: "Please enter email address",
				email: "Please enter valid email address"
			},
			FirstName: "Please enter firstname",
			LastName: "Please enter lastname"
		}
	});	
  });
</script>
<style type="text/css">
.top {
	margin-bottom: 15px;
}
.messagebox {
	position:absolute;
	width:100px;
	margin-left:30px;
}
.messageboxok {
	position:absolute;
	width:auto;
	margin-left:5px;
	color:#008000;
}
.messageboxerror {
	position:absolute;
	width:auto;
	margin-left:5px;
	color:#CC0000;
}
</style>
<section class="content-header">
  <h1>
   <img src="<?php echo ADMIN_IMAGE_PATH;?>/addmember.jpg" width="48" height="48" /> <?php echo $MODE;?> ADMINISTRATOR
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <?php
							showMessage();			
						?>
            <form class="form-horizontal" method="post" id="frmAdmin" action="manage_administrator.php" name="frmAdmin" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
                <input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
               <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">User Name</label>
                  <div class="col-sm-5">
                    	<input class="form-control" type="text" name="UserName" id="UserName" value="<?php echo $Info[0]['UserName']?>" />
                                                <span id="msgbox" style="display:none"></span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Password</label>
                  <div class="col-sm-5">
                    	<input class="form-control" type="password" name="Password" id="Password" value="<?php echo $Info[0]['Password']?>" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Place</label>
                  <div class="col-sm-5">
                    	<select class="form-control" name="Place" id="Place">
                                                    <option value="">--Select Place--</option>
                                                    <?php echo FillCombo1('county','c_name','c_id',$Info[0]['place_id'],'where site!=0');?>
                                                </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">First Name</label>
                  <div class="col-sm-5">
               <input class="form-control" type="text" name="FirstName" id="FirstName" value="<?php echo stripslashes($Info[0]['FirstName'])?>" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Last Name</label>
                  <div class="col-sm-5">
                    	<input class="form-control" type="text" name="LastName" id="LastName" value="<?php echo stripslashes($Info[0]['LastName'])?>" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Address</label>
                  <div class="col-sm-5">
                    	<textarea class="form-control" name="Address" id="Address"><?php echo stripslashes($Info[0]['address'])?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Phone/Mobile</label>
                  <div class="col-sm-5">
                    	<input class="form-control" name="Phone" id="Phone" value="<?php echo stripslashes($Info[0]['Phone'])?>" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-5">
                    	<input class="form-control" type="text" name="Email" id="Email" value="<?php echo stripslashes($Info[0]['Email'])?>" />
                                                <span id="msgbox1" style="display:none"></span>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Photo/Image</label>
                  <div class="col-sm-5">
                    	<input class="" name="CImage" id="CImage"  type="file">
                                                <input type="hidden" id="OldImage" name="OldImage" value="<?php echo stripslashes($Info[0]['image'])?>" >
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label"></label>
                  <div class="col-sm-5">
                    	<?php echo $Photo;?> 
                  </div>
                </div>
                
                                <input type="hidden" value="<?php echo $MODE?>" name="action" />
                                 <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">

                            <input value="<?php echo $MODE?> ADMINISTRATOR" class="btn btn-success" type="submit" name="submit1" id="RegisterSubmit">
                                    <input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=administrator&pg_no=<?php echo $_REQUEST['pg_no']?>';">

                          </div>
                        </div>
                                
            </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>

