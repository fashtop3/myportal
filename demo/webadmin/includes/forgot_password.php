<?php
	if(isset($_REQUEST['go']) && !empty($_REQUEST['go']))
	{
		$user_res=$objDB->select("select * from admin where Email='".$_REQUEST['emailid']."'");
		
		if(count($user_res)!=0)
		{
			$Headers = "From: <".EMAIL_ADDRESS.">\r\n" .
				'X-Mailer: PHP/' . phpversion() . "\r\n" .
				"MIME-Version: 1.0\r\n" .
				"Content-Type: text/html; charset=utf-8\r\n" .
				"Content-Transfer-Encoding: 8bit\r\n\r\n";
			$SQL = "Select * from admin where UserID=".$user_res[0]['UserID'];
			$RecordTmp = $objDB->select($SQL);
			$Subject = "Password Reminder";
			$To = stripslashes($RecordTmp[0]["Email"]);
			$HTMLBody = "Dear ".$RecordTmp[0]["UserName"].",<BR><br>";
			$HTMLBody = $HTMLBody."This is an automatically generated mail from ".SERVICE_EMAIL." in response to your request for password reminder.<BR>";
			$HTMLBody = $HTMLBody."Your Login information at ".SERVER_ROOT." is as follows<BR><BR>";
			$HTMLBody = $HTMLBody."User Name : ".$RecordTmp[0]["UserName"]."<BR>";
			$HTMLBody = $HTMLBody."Password  : ".$RecordTmp[0]["Password"]."<BR><BR>";
		
			$flag=  @mail($To, $Subject, $HTMLBody, $Headers); 

			//$flag=PasswordReminder($user_res[0]['UserID']);
			
			if($flag)
			{
				header("Location: index.php?p=forgot_password&done=s");
				exit();
			}
			else
			{
				header("Location: index.php?p=forgot_password&done=f2");
				exit();
			}
		}
		else if(count($user_res)==0)
		{
			header("Location: index.php?p=forgot_password&done=f1");
			exit();	
		}
	}
	if(isset($_REQUEST['done']) && $_REQUEST['done']=="f1")
	{
		$MsgImg="<img src='".ADMIN_IMAGE_PATH."/error6.jpg' width='16' height='16'>";
		$Msg="<span class='MsgErrorTxt' align='center'>
			Entered Email Id might be wrong</span>";
	}
	else if(isset($_REQUEST['done']) && $_REQUEST['done']=="f2")
	{
		$MsgImg="<img src='".ADMIN_IMAGE_PATH."/error6.jpg' width='16' height='16'>";
		$Msg="<span class='MsgErrorTxt' align='left'>Error while sending mail!</span>";
	}
	else if(isset($_REQUEST['done']) && $_REQUEST['done']=="s")
	{		  
		$MsgImg="<img src='".ADMIN_IMAGE_PATH."/success.gif'>";
		$Msg="<span class='MsgTxt' align='center'>Your Account Information Mailed to you Successfully</span>";
	}
?>
<script>
  $(document).ready(function(){
  	
    // validate signup form on keyup and submit
	$("#FrmForgotPassword").validate({
		rules: {
			emailid: {required: true, email:true}
		},
		messages: {
			emailid: {required: "Please enter email address",email:"Please enter valid email address"}
		}
	});	
  });
</script>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="973" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="300" align="center" valign="middle">
			<table border="0" width="45%" cellpadding="0" cellspacing="0" class="tbl_border"  bgcolor="#FFFFFF" style="border:1px solid #000033;">
				<tr>
					<td width="64" bgcolor="#ffffff" class="BottomBorder" style="border-bottom:1px solid #800001;"><img src="<?php echo ADMIN_IMAGE_PATH;?>/fpass2.jpg" width="38" height="37" /></td>
					<td width="374" colspan="2" class="td_login_head">Forgot Password </td>
				</tr>
				<?php
		if(isset($MsgImg) && isset($Msg))
		{
		?>
				<tr>
					<td height="23" colspan="3" align="center" class="mahead" bgcolor="#FFFFFF">
						<table width="100%" border="0" cellspacing="0" cellpadding="2">
							<tr>
								<td width="15%" align="right"><?php echo $MsgImg;?></td>
								<td width="85%" ><?php echo $Msg;?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<?php
		}
		else
		{
		?>
				<tr>
					<td height="23" colspan="3" align="center" style="padding:5px;">
						<table width="95%" border="0" cellspacing="0" cellpadding="4" bgcolor="#F6F6F6" class="tbl_border">
							<tr>
								<td class="backtxt" align="center"><img src="<?php echo ADMIN_IMAGE_PATH;?>/note.gif" width="24" height="24" /></td>
								<td class="backtxt" align="center"> Kindly Enter Your Email Address For Your Login 
									Details to be Mailed to You</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<?php
		}
		?>
					<td height="10" align="left" class="mahead"></td>
					<td height="5" colspan="2" class="small"></td>
				</tr>
				<tr>
					<td height="30" colspan="3" align="center">
						<table width="85%" border="0" cellspacing="0" cellpadding="2">
							<form name="FrmForgotPassword" id="FrmForgotPassword" action="index.php?p=forgot_password" method="post">
								<tr>
									<td width="3%">&nbsp;</td>
									<td width="20%" class="SmallBlackNormal">Email 
										ID</td>
									<td width="3%">:</td>
									<td width="50%">
										<input type="text" name="emailid" class="InputBox" id="emailid" size="35">
										</span></td>
									<td width="15%"><span class="td_margin_lr15_tb3">
										<input type="submit" name="go" value="GO" class="Btn">
										</span></td>
									<td width="3%">&nbsp;</td>
								</tr>
							</form>
						</table>
					</td>
				</tr>
				<tr>
					<td height="5" align="left" class="mahead"></td>
					<td height="5" colspan="2" class="small"></td>
				</tr>
				<tr>
					<td height="10" align="left" ></td>
					<td height="5" colspan="2" ></td>
				</tr>
				<tr>
					<td colspan="3" align="center" valign="middle" bgcolor="#F6F6F6" class="TopBorder" style="padding:4px"> <a href="index.php" class="backtxt">[Back]</a></td>
				</tr>
			</table>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
