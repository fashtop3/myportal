<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATEVIDEO";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"sermon"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$path = stripslashes($Info[0]['video']);	
}else
	$MODE="ADDVIDEO";
?>
<?php

	require("include/path.ini");		/*set path access*/				
	$ROOTPATHMP4 = "../uploads/sermon/video";
//	$ROOTPATHMP4 = "../../uploads/sermons/mp4";
	$ROOTPATHFLV = "../uploads/sermon/video";
//	$ROOTPATHFLV = "../../uploads/sermons/flv";
	$ROOTPATHMP3 = "../uploads/sermon/video";
//	$ROOTPATHMP3 = "../../uploads/sermons/mp3";
	require("include/security.inc");	/*check path security*/

	require("include/mime.inc");		/*load the mime type file*/

	define("SHOW_EDIT",1);
	define("HIDE_EDIT",0);
	if(isset($_GET['dir']))
		$dir=$_GET["dir"];
	else
		$dir="";
	$allowbrowse = $AllowSelfBrowse || !strstr($dir,basename(dirname($_SERVER['SCRIPT_NAME'])));
	function GetFileInfo($f) 
	{
		$s = array (	'perm'  => '',
				'humanperm' => '',	//human readable permissions: rwxrwxrwx
				'ux'    => false,	//is this file is user executable ?
				'islnk' => false,	//is this file is a link ?
				'owner'  => '',
				'group'  => '',
				'size'   => 0,
				'mtime'  => ''
				);
		$mystat = stat($f);
		$s["perm"]=$mystat[2] & 511;
		if(($s["perm"] & 320) == 320) $s['ux']=true;
		$fullperms=256; 			// rwxrwxrwx
		$s["humanperm"]="";
		$let = array('r','x','w'); 	//no, it's not rwx, but rxw, because browsed backwards
		for($i=9;$i>0;$i--) 
		{ 
			($s["perm"] & $fullperms)? $s["humanperm"].=$let[$i % 3]:$s["humanperm"].="-";
			$fullperms = $fullperms >> 1;
		}
		//last modification date
		$s["mtime"]=date('d M Y H:s',$mystat[10]);
		return($s);
	}
?>
<script>
  $(document).ready(function(){
  	
    // validate signup form on keyup and submit
	
	$("#frmAdmin").validate({
		rules: {			
			 <?php if($MODE=='ADDVIDEO'){?>CVideo: "required",<?php } ?>
			 sermon: "required"
		},
		messages: {
			 <?php if($MODE=='ADDVIDEO'){?>CVideo: "Please enter video",<?php } ?>
			 sermon: "Please select sermon"
		}
	});	
	
  });
</script>
<link href="include/fonts.css" rel="stylesheet" type="text/css">
<script language=JavaScript src="include/codes.js"></script>
<section class="content-header">
  <h1>
    SERMON VIDEO
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addvideo.jpg" /></td>
		<td colspan=5 class="tbl_head" height="24">SERMON VIDEO</td>
	</tr>
	<tr>
		<td width="" colspan="6">
			<form method="post" action="manage_sermon.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="5%" valign="top">Sermon</td>
											<td colspan="1" class="fieldarea">
												<select name="sermon" id="sermon" style="width:300px;">
													<option value="">--Select Sermon--</option>
													<?php echo FillCombo1('sermon','title','id',$Info[0]['id'],"where type='general'");?>
												</select>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<hr />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" valign="top">Video (mp4 file)</td>
											<td class="fieldarea" width="25%" valign="top">
												<input name="Video" id="Video" value="<?php if($Info[0]['video']!='') echo ".";echo stripslashes($Info[0]['video']);?>" class="input" style="width: 300px;" type="text" readonly="readonly" />
											</td>
										</tr>
										<tr>
											<td colspan="2" class="fieldarea" valign="top" align="left">
												<?php
												$hDir = opendir($ROOTPATHMP4.$dir);
												echo "Click on image to see Contents of MP4 Folder";
												?>
												<img src="images/plus.gif" align="absmiddle" id="plus" style="cursor:pointer;" onclick="document.getElementById('mp4').style.display='';document.getElementById('plus').style.display='none';document.getElementById('minus').style.display='';" /> <img src="images/minus.gif" align="absmiddle" id="minus" style="display:none;cursor:pointer;" onclick="document.getElementById('mp4').style.display='none';document.getElementById('minus').style.display='none';document.getElementById('plus').style.display='';" /> <br />
												<table id="mp4" style="display:none;" class="normaltext" width="100%" border="0" cellspacing="1" cellpadding="5">
													<?php
														/*browse dir*/
														$j=0;
														$frmid=0;
														if(strlen($Exceptions)>0) 
															$except=explode(",",$Exceptions);
														while ($file=readdir($hDir)) 
														{
															$res=true;
															if(strlen($Exceptions)>0) 
															{
																reset($except);
																for($i=0;$i<count($except);$i++) 
																{
																	if(!strcmp($file,$except[$i])) 
																	{
																		$res=false;
																		break;
																	}
																}
															}
															if($res)
																$files[$j++]=$file;
														} /*end while*/
														if($allowbrowse) 
														{
															echo " <tr>"; 
															echo "		<td height='20' colspan='6'>";
															if(strcmp($dir,"/")) 	
															{
																$updir = dirname($dir);
																$updir = str_replace("\\","/",$updir);
																if ($updir == "")
																	$updir = "/";
															}
															echo "		</td>";
															echo "	</tr>";
															echo "	<tr bgcolor='#CCCCCC'>"; 
															echo "		<td class='' width='16' height='20'>&nbsp;</td>";
															echo "		<td class='' width='217' height='20'>&nbsp;<b>Name</b></td>";
															echo "		<td class='' width='75' height='20' align='center'><b>Size</b></td>";
															echo "		<td class='' width='150' height='20' align='center'><b>Last Modified</b></td>";
															
															echo "	</tr>";
															if(count($files)) 
															{
																sort($files);
																reset($files);
																while (list($key,$name) = each($files)) 
																{
																	$rc=chkfile($name,&$icon);
																	$filename=$ROOTPATHMP4."$sp$name";
																	$href=$ROOTPATHMP4."$sp$name";
																	if(!is_dir($filename)) 
																	{																		
																		if(strpos(strtolower($name),".mp4")>0){
																			echo "	<tr>";
																			if($IsShowDelete) 
																			{
																				echo "		<td class='fieldarea' height='20'>";
																				echo "<input type='radio' name='removeitems' onclick='document.getElementById(\"Video\").value=this.value;' value=\"".$ROOTPATHMP4."/".$name."\"/></td>";
																			}
																			else 
																				echo " <td class='fieldarea' height='20'>&nbsp;</td>";
																			echo "		<td height='20'>";
																			echo "		<a class='fieldarea' href=\"".$href."\">$name</a>";
																			echo "    	</td>";
																			echo "    	<td class='fieldarea' height='20' align='right'>";
																			$s=filesize($filename);
																			$unit="b";
																			if($s >= 1024) 
																			{ 
																				$s=floor(round($s/1024));
																				$unit="KB";
																			}
																			$s = number_format($s,"","",",");
																			echo $s." ".$unit;
																			echo "</td>";
																			echo "		<td class='fieldarea' height='20' align='center'>";
																			$modifytime = date('d M Y H:s',filemtime($filename)); 
																			echo $modifytime."</td>";		
																		}																
																		$i++;
																	}
																} //end of while
															} //end of if(count($files))
															else
															{
																echo "	<tr class='text1'>"; 
																echo "		<td colspan='6' class='fieldarea'>[Empty Folder...]</td>";
																echo "	</tr>";
															}
														} //endif allowbrowse
														else 
														{
															if(! $pass++)
															{
																echo "<td colspan='6' class='fieldarea'>";
																echo "browsing under the explorer itself is forbidden (see your administrator)";
																echo "</td>";
															}
														}
													?>
													<tr>
														<td colspan="6" class="fieldarea">
															<hr size="1">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<hr />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" valign="top">Flash (flv file)</td>
											<td class="fieldarea" valign="top">
												<input name="Flash" id="Flash" value="<?php if($Info[0]['flash']!='') echo ".";echo stripslashes($Info[0]['flash']);?>" class="input" style="width: 300px;" type="text" readonly="readonly" />
											</td>
										</tr>
										<tr>
											<td colspan="2" class="fieldarea" valign="top">
												<?php
												$hDir = opendir($ROOTPATHFLV.$dir);
												echo "Click on image to see Contents of FLV Folder";
												?>
												<img src="images/plus.gif" align="absmiddle" id="plus1" style="cursor:pointer;" onclick="document.getElementById('flv').style.display='';document.getElementById('plus1').style.display='none';document.getElementById('minus1').style.display='';" /> <img src="images/minus.gif" align="absmiddle" id="minus1" style="display:none;cursor:pointer;" onclick="document.getElementById('flv').style.display='none';document.getElementById('minus1').style.display='none';document.getElementById('plus1').style.display='';" /> <br />
												<table id="flv" style="display:none;" class="normaltext" width="100%" border="0" cellspacing="1" cellpadding="5">
													<?php
														/*browse dir*/
														$j=0;
														$frmid=0;
														if(strlen($Exceptions)>0) 
															$except=explode(",",$Exceptions);
														while ($file=readdir($hDir)) 
														{
															$res=true;
															if(strlen($Exceptions)>0) 
															{
																reset($except);
																for($i=0;$i<count($except);$i++) 
																{
																	if(!strcmp($file,$except[$i])) 
																	{
																		$res=false;
																		break;
																	}
																}
															}
															if($res)
																$files[$j++]=$file;
														} /*end while*/
														if($allowbrowse) 
														{
															echo " <tr>"; 
															echo "		<td height='20' colspan='6'>";
															if(strcmp($dir,"/")) 	
															{
																$updir = dirname($dir);
																$updir = str_replace("\\","/",$updir);
																if ($updir == "")
																	$updir = "/";
															}
															echo "		</td>";
															echo "	</tr>";
															echo "	<tr bgcolor='#CCCCCC'>"; 
															echo "		<td class='' width='16' height='20'>&nbsp;</td>";
															echo "		<td class='' width='217' height='20'>&nbsp;<b>Name</b></td>";
															echo "		<td class='' width='75' height='20' align='center'><b>Size</b></td>";
															echo "		<td class='' width='150' height='20' align='center'><b>Last Modified</b></td>";
															
															echo "	</tr>";
															if(count($files)) 
															{
																sort($files);
																reset($files);
																while (list($key,$name) = each($files)) 
																{
																	$rc=chkfile($name,&$icon);
																	$filename=$ROOTPATHFLV."$sp$name";
																	$href=$ROOTPATHFLV."$sp$name";
																	
																	if(!is_dir($filename)) 
																	{																																	
																		if(strpos(strtolower($name),".flv")>0){
																			print "	<tr>";
																			if($IsShowDelete) 
																			{
																				echo "		<td width='16' class='fieldarea' height='20'>";
																				echo "<input type='radio' name='removeitems1' onclick='document.getElementById(\"Flash\").value=this.value;' value=\"".$ROOTPATHFLV."/".$name."\"/></td>";
																			}
																			else 
																				echo " <td class='fieldarea' height='20'>&nbsp;</td>";
																				
																			echo "		<td height='20'>";
																			echo "		<a class='fieldarea' href=\"".$href."\">$name</a>";
																			echo "    	</td>";
																			echo "    	<td class='fieldarea' height='20' align='right'>";
																			$s=filesize($filename);
																			$unit="b";
																			if($s >= 1024) 
																			{ 
																				$s=floor(round($s/1024));
																				$unit="KB";
																			}
																			$s = number_format($s,"","",",");
																			echo $s." ".$unit;
																			echo "</td>";
																			echo "		<td class='fieldarea' height='20' align='center'>";
																			$modifytime = date('d M Y H:s',filemtime($filename)); 
																			echo $modifytime."</td>";			
																		}															
																		$i++;
																	}
																} //end of while
															} //end of if(count($files))
															else
															{
																echo "	<tr class='text1'>"; 
																echo "		<td colspan='6' class='fieldarea'>[Empty Folder...]</td>";
																echo "	</tr>";
															}
														} //endif allowbrowse
														else 
														{
															if(! $pass++)
															{
																echo "<td colspan='6' class='fieldarea'>";
																echo "browsing under the explorer itself is forbidden (see your administrator)";
																echo "</td>";
															}
														}
													?>
													<tr>
														<td colspan="6" class="fieldarea">
															<hr size="1">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<hr />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" valign="top">Window Media File (wmv file)</td>
											<td class="fieldarea" valign="top">
												<input name="Audio" id="Audio" value="<?php if($Info[0]['audio']!='') echo ".";echo stripslashes($Info[0]['audio']);?>" class="input" style="width: 300px;" type="text" readonly="readonly" />
											</td>
										</tr>
										<tr>
											<td colspan="2" class="fieldarea" valign="top">
												<?php $hDir = opendir($ROOTPATHMP3.$dir);
												echo "Click on image to see Contents of WMV Folder";
												?>
												<img src="images/plus.gif" align="absmiddle" id="plus2" style="cursor:pointer;" onclick="document.getElementById('mp3').style.display='';document.getElementById('plus2').style.display='none';document.getElementById('minus2').style.display='';" /> <img src="images/minus.gif" align="absmiddle" id="minus2" style="display:none;cursor:pointer;" onclick="document.getElementById('mp3').style.display='none';document.getElementById('minus2').style.display='none';document.getElementById('plus2').style.display='';" /> <br />
												<table id="mp3" style="display:none;" class="normaltext" width="100%" border="0" cellspacing="1" cellpadding="5">
													<?php
														/*browse dir*/
														$j=0;
														$frmid=0;
														if(strlen($Exceptions)>0) 
															$except=explode(",",$Exceptions);
														while ($file=readdir($hDir)) 
														{
															$res=true;
															if(strlen($Exceptions)>0) 
															{
																reset($except);
																for($i=0;$i<count($except);$i++) 
																{
																	if(!strcmp($file,$except[$i])) 
																	{
																		$res=false;
																		break;
																	}
																}
															}
															if($res)
																$files[$j++]=$file;
														} /*end while*/
														if($allowbrowse) 
														{
															echo " <tr>"; 
															echo "		<td height='20' colspan='6'>";
															if(strcmp($dir,"/")) 	
															{
																$updir = dirname($dir);
																$updir = str_replace("\\","/",$updir);
																if ($updir == "")
																	$updir = "/";
															}
															echo "		</td>";
															echo "	</tr>";
															echo "	<tr bgcolor='#CCCCCC'>"; 
															echo "		<td class='' width='16' height='20'>&nbsp;</td>";
															echo "		<td class='' width='217' height='20'>&nbsp;<b>Name</b></td>";
															echo "		<td class='' width='75' height='20' align='center'><b>Size</b></td>";
															echo "		<td class='' width='150' height='20' align='center'><b>Last Modified</b></td>";
															
															echo "	</tr>";
															if(count($files)) 
															{
																sort($files);
																reset($files);
																while (list($key,$name) = each($files)) 
																{
																	$rc=chkfile($name,&$icon);
																	$filename=$ROOTPATHMP3."$sp$name";
																	$href=$ROOTPATHMP3."$sp$name";
																	if(!is_dir($filename)) 
																	{																		
																		if(strpos(strtolower($name),".wmv")>0){
																			echo "	<tr>";
																			if($IsShowDelete) 
																			{
																				echo "		<td width='16' class='fieldarea' height='20'>";
																				echo "<input type='radio' name='removeitems2' onclick='document.getElementById(\"Audio\").value=this.value;' value=\"".$ROOTPATHMP3."/".$name."\"></td>";
																			}
																			else 
																				echo " <td class='fieldarea' height='20'>&nbsp;</td>";
																			echo "		<td height='20'>";
																			echo "		<a class='fieldarea' href=\"".$href."\">$name</a>";
																			echo "    	</td>";
																			echo "    	<td class='fieldarea' height='20' align='right'>";
																			$s=filesize($filename);
																			$unit="b";
																			if($s >= 1024) 
																			{ 
																				$s=floor(round($s/1024));
																				$unit="KB";
																			}
																			$s = number_format($s,"","",",");
																			echo $s." ".$unit;
																			echo "</td>";
																			echo "		<td class='fieldarea' height='20' align='center'>";
																			$modifytime = date('d M Y H:s',filemtime($filename)); 
																			echo $modifytime."</td>";	
																		}																	
																		$i++;
																	}
																} //end of while
															} //end of if(count($files))
															else
															{
																echo "	<tr class='text1'>"; 
																echo "		<td colspan='6' class='fieldarea'>[Empty Folder...]</td>";
																echo "	</tr>";
															}
														} //endif allowbrowse
														else 
														{
															if(! $pass++)
															{
																echo "<td colspan='6' class='fieldarea'>";
																echo "browsing under the explorer itself is forbidden (see your administrator)";
																echo "</td>";
															}
														}
													?>
													<tr>
														<td colspan="6" class="fieldarea">
															<hr size="1">
														</td>
													</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td colspan="2">
												<hr />
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<div align="center">
									<input value="SUBMIT" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=video_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
