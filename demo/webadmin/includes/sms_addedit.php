<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/common_functions.php");
		CheckPermission(); 
		exit();
	}

	$CategoryName = LoadVariable("CategoryName","");

	$MODE="ADD";
	$PANEL_HEADING="SMS SUBSCRIBER";

	if(isset($_REQUEST['ID']))
	{
		$MODE="UPDATE";

		$TblFieldsArr = array
		(
			//table name=>feilds name
			"sms"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE sms_id =".$_REQUEST['ID'];
		$Sort="";
		$Limit="";

		$RecordSet=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
		$Name = $RecordSet[0]['sms_name'];
		$MobileNo = $RecordSet[0]['sms_mobile_no'];
		$CategoryID = $RecordSet[0]['sms_id'];
		
		if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $RecordSet[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}
}
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#FrmCategoryAddEdit").validate({
		rules: {
			Name: "required",
			MobileNo: {required: true,maxlength:10,minlength:10}
		},
		messages: {
			Name: "Please enter name",
			MobileNo: {required:"Please enter mobile number",maxlength:"Please enter 10 digit mobile number",minlength:"Please enter 10 digit mobile number"}
		}
	});
  });
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/sms.jpg" width="30" height="30" /> <?php echo $MODE?>&nbsp;<?php echo $PANEL_HEADING?>
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          	<form class="form-horizontal"  action="manage_sms.php" method="post" enctype="multipart/form-data" name="FrmCategoryAddEdit" id="FrmCategoryAddEdit" >
				<input type="hidden" name="Process" value="<?php echo $MODE?>">
			    <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input type="hidden" name="ID" value="<?php echo $_REQUEST['ID']?>">
				<?php ShowMessage(); ?>
		<div class="form-group col-md-12">
	      <label for="inputEmail" class="col-sm-2 control-label">Place</label>
	      <div class="col-sm-5">
			<select name="PlaceValue" class="form-control" id="PlaceValue" onChange="if(this.value!='') window.location='index.php?p=send_bulk_sms&plc='+this.value; else window.location='send_bulk_sms.php';">
					<option value="">All Places</option>
					<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
				</select>
				
	      </div>
	    </div>
		<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Name <span class="RedStar">*</span></label>
                  <div class="col-sm-5">
                    	<input type="text" name="Name" id="Name" class="form-control" value="<?php echo stripslashes($Name)?>" maxlength="255">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Mobile No <span class="RedStar">*</span></label>
                  <div class="col-sm-5">
                    	<input type="text" name="MobileNo" id="MobileNo" class="form-control" value="<?php echo stripslashes($MobileNo)?>" maxlength="255" />
                  </div>
                </div>
		 <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">

                            <input type="submit" name="<?php echo $MODE?>" id="<?php echo $MODE?>" class="btn btn-success" value="<?php echo $MODE?>">
							<input type="button" name="cancel" id="cancel" class="btn btn-default" value="CANCEL" onclick="window.location='index.php?p=sms_list&pg_no=<?php echo $_REQUEST['pg_no']?>'">
                          </div>
                        </div>
</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
