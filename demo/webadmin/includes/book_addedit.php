<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"book"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);		
}else
	$MODE="ADD";
?>
<script>
  $(document).ready(function(){  	
    // validate signup form on keyup and submit
		$("#frmAdmin").validate({
			rules: {
				No: "required",
				Lesson:"required",
				LDate:"required"
			},
			messages: {
				No: "Please enter book number",
				Lesson:"Please enter lesson number",
				LDate: "Please enter lesson date"
			}
		});	
  });
	$(function() {
		$('#LDate').datepicker({
			beforeShowDay: nonWorkingDates,
			changeMonth: true,
			changeYear: true,
			yearRange: '1975:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});
</script>
<section class="content-header">
  <h1>
   <img src="<?php echo ADMIN_IMAGE_PATH;?>/book.gif" width="30" height="30" /> <?php echo $MODE;?> BOOK
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
         
			<form  class="form-horizontal" method="post" action="manage_book.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Book No.</label>
                  <div class="col-sm-5">
                    	<input name="No" id="No" class="form-control" value="<?php echo stripslashes($Info[0]['book_no'])?>" />
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Book Name</label>
                  <div class="col-sm-5">
                    	<input name="Name" id="Name" style="width: 250px;" class="InputBox" value="<?php echo $Info[0]['book_name']?>" />
                  </div>
                </div> -->
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Lesson No.</label>
                  <div class="col-sm-5">
                    	<input name="Lesson" id="Lesson"  class="form-control" value="<?php echo stripslashes($Info[0]['lesson_no'])?>" />
                  </div>
                </div>

                 <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Lesson Date</label>
                  <div class="col-sm-5">
                    	<input name="LDate" id="LDate" class="form-control" value="<?php echo $Info[0]['lesson_date']?>" />
						(Enter only sunday date)
                  </div>
                </div>
				
				<input type="hidden" value="<?php echo $MODE?>" name="action" />
		 <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                          	<input value="<?php echo $MODE?> BOOK" class="btn btn-success" type="submit" name="submit1" id="submit1">
			<input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=book_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">

                            
                          </div>
                        </div>
		
			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
