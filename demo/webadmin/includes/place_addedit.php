<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/common_functions.php");
		CheckPermission(); 
		exit();
	}

	$CategoryName = LoadVariable("CategoryName","");

	$MODE="ADD";
	$PANEL_HEADING="COUNTY";

	if(isset($_REQUEST['ID']))
	{
		$MODE="UPDATE";

		$TblFieldsArr = array
		(
			//table name=>feilds name
			"county"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE c_id =".$_REQUEST['ID'];
		$Sort="";
		$Limit="";

		$RecordSet=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
		$Name = stripslashes($RecordSet[0]['c_name']);
		$MobileNo = stripslashes($RecordSet[0]['c_email']);
		$CategoryID = $RecordSet[0]['c_id'];
}
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#FrmCategoryAddEdit").validate({
		rules: {
			Name: "required",
			Email: "required"			
		},
		messages: {
			Name: "Please enter name",
			Email: "Please enter email address"
		}
	});
  });
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/globe.gif" width="30" height="30" /> <?php echo $MODE?>&nbsp;<?php echo $PANEL_HEADING?>
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <form  class="form-horizontal" action="manage_place.php" method="post" enctype="multipart/form-data" id="FrmCategoryAddEdit" name="FrmCategoryAddEdit">
			<input type="hidden" name="Process" value="<?php echo $MODE?>">
			<input type="hidden" name="OldImage" value="<?php echo stripslashes($RecordSet[0]['image'])?>" >
			<input type="hidden" name="ID" value="<?php echo $_REQUEST['ID']?>">
		    <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
		    <div class="form-group col-md-12">
					<div class="col-md-8"><input type="button" name="BACK" id="BACK" class="btn btn-default" value="BACK" onclick="window.location.href='index.php?p=place_list'">
					</div>
					<div class="col-md-4" style="text-align: right;float: right;">
					</div>
				</div>
				<?php ShowMessage(); ?>
				<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Name</label>
                  <div class="col-sm-5">
                    	<input type="text" name="Name" id="Name" class="form-control" value="<?php echo stripslashes($Name)?>" maxlength="255" style="width:300px;">
                  </div>
                </div>
				<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Church Image (Only Flash File like .swf)</label>
                  <div class="col-sm-5">
                    	<input class="" name="CImage" accept="SWF" id="CImage" style="width: 400px;" type="file">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-5">
                    <textarea name="Email" class="form-control" cols="" id="Email" style="width:300px; height:50px;" rows="" ><?php echo stripslashes($MobileNo)?></textarea>
                    (Enter Email ids with comma separate)
                  </div>
                </div>					
				<?php if($_REQUEST['ID']!=DEFAULT_PLACE){ ?>

				 <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                  <div class="col-sm-5">
                   <input type="checkbox" value="1" <?php if($RecordSet[0]['site']) echo "checked='checked'"; ?> name="Site" id="Site" />
							Do you want to create site for this place?
                  </div>
                </div>		
				<?php } ?>	
					 <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">

                            <input type="submit" name="<?php echo $MODE?>" id="<?php echo $MODE?>" class="btn btn-success" value="<?php echo $MODE?>">
							<input type="button" name="cancel" id="cancel" class="btn btn-default" value="CANCEL" onclick="window.location='index.php?p=place_list&pg_no=<?php echo $_REQUEST['pg_no']?>'">
                          </div>
                        </div>
</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
