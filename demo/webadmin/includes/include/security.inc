<?php
/* get the variable $localname & $dir & name*/
/* if(isset($_POST['dir'])) {
	$dir=$_POST["dir"];
}
else
	{
	$dir=$_GET["dir"];
}
*/
/*filter for security*/
if($dir=="" || $dir=="/") {
	$dir="/";
	//if $dir is /, do not prepend / to the dirname
}
else {
	$sp="/";
	while(strstr($dir,"..") || strstr($dir,"//")) {
		$dir=str_replace("..","",$dir);
		$dir=str_replace("//","/",$dir);
	}
	/*check path*/
	if(substr($dir,0,1) != "/")
		$dir="/".$dir;

	/*remove last char if it's "/" */
	if(substr($dir,strlen($dir)-1,1)=="/") $dir=substr($dir,0,strlen($dir)-1);

	if($dir=="") $dir="/";
	
	/*physically control the path*/
	if(!is_dir($ROOTPATH.$dir)) {
		echo ("<script language='javascript'>\n");
		echo ("location.href = 'unreadf.php?dir=".$dir."';\n");
		echo ("</script>\n");
	}
	
}
?>