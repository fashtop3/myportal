<?php
	require_once("../utils/dbclass.php");
	require_once("../utils/functions.php");	
	require_once("../utils/config.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;

	$Tbl = "county";


//========================  ADD FUNCTION  ========================
if(isset($_REQUEST["Process"]) && $_REQUEST["Process"] == 'ADD')
{
	$sql = "select * from county where place_key = '".$plc1."'";
	$pl = $objDB->select($sql);
	if($pl[0]['c_id']!='')
	{
		$_SESSION['ErrorMsg'] = "Place already exists!";
		header("location: index.php?p=place_list&pg_no=".$_REQUEST['pg_no']);
		exit();
	}
	$plc = $_REQUEST["Name"];
	if($plc!='')
	{	
		$plc1 = str_replace(' , ','_',$plc);	
		$plc1 = str_replace(', ','_',$plc1);		
		$plc1 = str_replace(' ,','_',$plc1);		
		$plc1 = str_replace('  ','_',$plc1);		
		$plc1 = str_replace(' ','_',$plc1);
		$plc1 = str_replace(',','_',$plc1);
		$plc1 = strtolower($plc1);
		
		if($_REQUEST['Site']==1){
			mkdir('../'.$plc1);
			
			function recurse_copy($src,$dst) { 
			$dir = opendir($src);		
			 @mkdir($dst);
			 while(false !== ( $file = readdir($dir)) ) {
					if (( $file != '.' ) && ( $file != '..' )) {
						if ( is_dir($src . '/' . $file) ) {
							 recurse_copy($src . '/' . $file,$dst . '/' . $file);
						}
						else {
							 copy($src . '/' . $file,$dst . '/' . $file);
						}
					}
			 }
			 closedir($dir); 
			}
			$src = '../main';
			$dst = '../'.$plc1;
			recurse_copy($src,$dst);		
				
			$sql = "select * from cms where place_id=".DEFAULT_PLACE;
			$cms = $objDB->select($sql);
			
			$newplace = FetchValue('county','c_id','place_key',$plc1);
			for($i=0;$i<count($cms);$i++)
			{
				$sql = "insert cms set place_id = ".$newplace.",";
				$sql .= "title='".($cms[$i]['title'])."',";
				$sql .= "cms_key='".($cms[$i]['cms_key'])."',";
				$sql .= 'description="'.($cms[$i]['description']).'",';
				$sql .= "created='".date('Y-m-d H:i:s')."',";
				$sql .= "createdby='".$_SESSION['AdminID']."'";
				$objDB->sql_query($sql);
			}
		}
	}
	
	if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper('.swf,.jpg,.gif');
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = 'SWF';
					$flag = true;
					$FileName = $_FILES['CImage']['name'];
					$Lastpos = strrpos($FileName, '.');
					$Image = substr($FileName, 0 , $Lastpos);
					$FileExtension = substr($FileName, $Lastpos + 1 , strlen($FileName));
					
					if(strtoupper($img_extension1[count($img_extension1)-1])=='SWF')
						$ext = '';
					else
						$ext = '.'.$FileExtension;
									
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('place_'.substr(md5($upload_file.strtotime("now")),-5));						
					
					$image = new SimpleImage();
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save("../uploads/place/".$CImage.".".$FileExtension);
					
					
					
									
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=place_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = 'notavailable';
			//$ext = '.jpg';
		}
	$SQL = "INSERT county SET ";
	$SQL .= "c_name = '".addslashes($_REQUEST["Name"])."',";
	$SQL .= "place_key = '".$plc1."',";
	$SQL .= "image = '".$CImage.$ext."',";
	$SQL .= "site = '".addslashes($_REQUEST['Site'])."',";	
	$SQL .= "c_email = '".addslashes($_REQUEST["Email"])."'";	
	$objDB->sql_query($SQL);
	
	$_SESSION['SuccessMsg'] = "Place Info. Saved Successfully!";
	header("location: index.php?p=place_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}

//========================  UPDATE FUNCTION  ========================
if(isset($_REQUEST["Process"]) && $_REQUEST["Process"] == 'UPDATE')
{
	if($_REQUEST['ID']!=DEFAULT_PLACE){
		$plc = $_REQUEST["Name"];
		if($plc!='')
		{	
			$plc1 = str_replace(' , ','_',$plc);	
			$plc1 = str_replace(', ','_',$plc1);		
			$plc1 = str_replace(' ,','_',$plc1);		
			$plc1 = str_replace('  ','_',$plc1);		
			$plc1 = str_replace(' ','_',$plc1);
			$plc1 = str_replace(',','_',$plc1);
			$plc1 = strtolower($plc1);
			
			if($_REQUEST['Site']==1){
				
				/* Delete */
				$plc1 = FetchValue('county','place_key','c_id',$_REQUEST['ID']);
		
				function recurse_del($src) { 
				$dir = opendir($src);		
				 
				 while(false !== ( $file = readdir($dir)) ) {
						if (( $file != '.' ) && ( $file != '..' )) {
							if ( is_dir($src . '/' . $file) ) {
								 recurse_del($src . '/' . $file);
								 @unlink($src . '/' . $file);
							}
							else {
								 @unlink($src . '/' . $file);
							}
						}
				 }
				 closedir($dir); 
				}
				$src1 = '../'.$plc1;
				recurse_del($src1);	
				rmdir($src1."/includes");
				rmdir($src1."/member/includes");
				rmdir($src1."/css");
				rmdir($src1."/js");
				rmdir($src1."/utils");
				rmdir($src1."/Scripts");
				rmdir($src1."/mail_templates");
				rmdir($src1."/member");
				rmdir($src1);
				
				$sql = "delete from cms where place_id = ".$_REQUEST['ID'];
				$objDB->sql_query($sql);
			/* */
			
			/* Create */
				mkdir('../'.$plc1);
				
				function recurse_copy($src,$dst) { 
				$dir = opendir($src);		
				 @mkdir($dst);
				 while(false !== ( $file = readdir($dir)) ) {
						if (( $file != '.' ) && ( $file != '..' )) {
							if ( is_dir($src . '/' . $file) ) {
								 recurse_copy($src . '/' . $file,$dst . '/' . $file);
							}
							else {
								 copy($src . '/' . $file,$dst . '/' . $file);
							}
						}
				 }
				 closedir($dir); 
				}
				$src = '../main';
				$dst = '../'.$plc1;
				recurse_copy($src,$dst);		
					
				$sql = "select * from cms where place_id=".DEFAULT_PLACE;
				$cms = $objDB->select($sql);
				
				$newplace = FetchValue('county','c_id','place_key',$plc1);
				for($i=0;$i<count($cms);$i++)
				{
					$sql = "insert cms set place_id = ".$newplace.",";
					$sql .= "title='".($cms[$i]['title'])."',";
					$sql .= "cms_key='".$cms[$i]['cms_key']."',";
					$sql .= 'description="'.($cms[$i]['description']).'",';
					$sql .= "created='".date('Y-m-d H:i:s')."',";
					$sql .= "createdby='".$_SESSION['AdminID']."'";
					$objDB->sql_query($sql);
				}
			}
			/* */
		}
	}
	if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper('.swf,.jpg,.gif');
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = 'SWF';
					$flag = true;
					$FileName = $_FILES['CImage']['name'];
					$Lastpos = strrpos($FileName, '.');
					$Image = substr($FileName, 0 , $Lastpos);
					$FileExtension = substr($FileName, $Lastpos + 1 , strlen($FileName));
					
					if(strtoupper($img_extension1[count($img_extension1)-1])=='SWF')
						$ext = '';
					else
						$ext = '.'.$FileExtension;
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('place_'.substr(md5($upload_file.strtotime("now")),-5));	
					//$PImage = $FileName;	
					
					copy($_FILES['CImage']['tmp_name'], "../uploads/place/".$CImage.".".$FileExtension);
										
					
					$SQL = "SELECT * FROM county ";
					$SQL .= "WHERE c_id = '".$_REQUEST['ID']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['image']!='notavailable'){
						unlink('../uploads/place/'.$img[0]['image'].".".$FileExtension);					
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=place_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = $_REQUEST['OldImage'];
		}
	$sql = "select * from county where c_name = '".addslashes($_REQUEST['Name'])."' and c_id != ".$_REQUEST['ID'];
	$pl = $objDB->select($sql);
	if($pl[0]['c_id']!='')
	{
		$_SESSION['ErrorMsg'] = "Place already exists!";
		header("location: index.php?p=place_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	if($_REQUEST['ID']==DEFAULT_PLACE)
		$site = '1';
	else
		$site = $_REQUEST['Site'];
	$SQL = "UPDATE county SET ";
	$SQL .= "c_name = '".addslashes($_REQUEST["Name"])."',";
	$SQL .= "site = '".addslashes($site)."',";	
	$SQL .= "image = '".$CImage.$ext."',";	
	$SQL .= "c_email = '".addslashes($_REQUEST["Email"])."'";	
	$SQL .= " WHERE c_id = '".$_REQUEST["ID"]."'";	
	
	//echo $SQL;exit;
	$objDB->sql_query($SQL);
	
	$_SESSION['SuccessMsg'] = "Place Info. Updated Successfully!";
	header("location: index.php?p=place_list&pg_no=".$_REQUEST['pg_no']);
	exit();
}


//==================================  SINGLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	
	$plc1 = FetchValue('county','place_key','c_id',$_REQUEST['ID']);
	
		function recurse_del($src) { 
		$dir = opendir($src);		
		 
		 while(false !== ( $file = readdir($dir)) ) {
			  if (( $file != '.' ) && ( $file != '..' )) {
					if ( is_dir($src . '/' . $file) ) {
						 recurse_del($src . '/' . $file);
						 @unlink($src . '/' . $file);
					}
					else {
						 @unlink($src . '/' . $file);
					}
			  }
		 }
		 closedir($dir); 
		}
		$src1 = '../'.$plc1;
		recurse_del($src1);	
		rmdir($src1."/includes");
		rmdir($src1."/member/includes");
		rmdir($src1."/css");
		rmdir($src1."/js");
		rmdir($src1."/utils");
		rmdir($src1."/Scripts");
		rmdir($src1."/mail_templates");
		rmdir($src1."/member");
		rmdir($src1);
		
		$sql = "delete from cms where place_id = ".$_REQUEST['ID'];
		$objDB->sql_query($sql);
		
		$SQL = "SELECT * FROM county ";
		$SQL .= "WHERE c_id = '".$_REQUEST['ID']."'";
		$img = $objDB->sql_query($SQL);
		if($img[0]['image']!='notavailable'){
			unlink('../uploads/place/'.$img[0]['image'].".swf");
			unlink('../uploads/place/'.$img[0]['image']);
		}					
		DeleteData($Tbl,"c_id","=",$_REQUEST['ID']);
		$_SESSION['SuccessMsg'] = "Place Info. Deleted Successfully";
		header("Location: index.php?p=place_list&pg_no=".$_REQUEST['pg_no']);
}	
exit();
?>
