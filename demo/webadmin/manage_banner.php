<?php
	require_once("../utils/config.php");
	require_once("../utils/functions.php");	
	require_once("../utils/dbclass.php");
	$objDB = new MySQLCN;

$Tbl = "slider";
//==================================  Add  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "ADD")
{
	if(empty($_REQUEST['cms_id']))
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					
					$FileName = $_FILES['CImage']['name'];
					$Lastpos = strrpos($FileName, '.');
					$Image = substr($FileName, 0 , $Lastpos);
					$FileExtension = substr($FileName, $Lastpos + 1 , strlen($FileName));
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$EventImage = 'page_'.substr(md5($upload_file.strtotime("now")),-5).".".$FileExtension;	
										
					copy($_FILES['CImage']['tmp_name'],"../uploads/banner/".$EventImage);							
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Picture Not Uploaded";
				header("Location: index.php?p=banner_new&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Picture Image Not Exists";
			header("Location: index.php?p=banner_new&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		
		if(!empty($_FILES['PImage']['name']))
		{  
			if($_FILES['PImage']['error'] == UPLOAD_ERROR_OK )
			{
					
					$FileName = $_FILES['PImage']['name'];
					$Lastpos = strrpos($FileName, '.');
					$PImage = substr($FileName, 0 , $Lastpos);
					$FileExtension = substr($FileName, $Lastpos + 1 , strlen($FileName));
										
					$upload_file = $_FILES['PImage']['tmp_name'];
					$PEventImage = 'page_'.substr(md5($upload_file.strtotime("now")),-5).".".$FileExtension;	
										
					copy($_FILES['PImage']['tmp_name'],"../uploads/banner/".$PEventImage);							
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Picture Not Uploaded";
				header("Location: index.php?p=banner_new&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$PEventImage = "";
			
			/*$_SESSION['ErrorMsg'] = "Picture Image Not Exists";
			header("Location: index.php?p=banner_new&pg_no=".$_REQUEST['pg_no']);
			exit;*/
		}
		
		/*if(!empty($_FILES['TImage']['name']))
		{  
			if($_FILES['TImage']['error'] == UPLOAD_ERROR_OK )
			{
					
					$FileName = $_FILES['TImage']['name'];
					$Lastpos = strrpos($FileName, '.');
					$Image = substr($FileName, 0 , $Lastpos);
					$FileExtension = substr($FileName, $Lastpos + 1 , strlen($FileName));
										
					$upload_file = $_FILES['TImage']['tmp_name'];
					$TEventImage = 'page_'.substr(md5($upload_file.strtotime("now")),-5).".".$FileExtension;	
										
					copy($_FILES['TImage']['tmp_name'],"../uploads/banner/".$TEventImage);							
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Picture Not Uploaded";
				header("Location: index.php?p=banner_new&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Picture Image Not Exists";
			header("Location: index.php?p=banner_new&pg_no=".$_REQUEST['pg_no']);
			exit;
		}*/
		
		$SQL = "INSERT $Tbl SET 
				title ='".addslashes($_REQUEST['Title'])."',		
				url ='".addslashes($_REQUEST['Links'])."',				
				image ='".addslashes($EventImage)."',	
				popup_image ='".addslashes($PEventImage)."',		
				thumb_icon ='".addslashes($TEventImage)."',		
				from_date ='".$_REQUEST['FromDate']."',
				to_date ='".$_REQUEST['ToDate']."',		
				description ='".addslashes(trim($_REQUEST['ContentDesc']))."'";
	}
	//echo $SQL;exit;
	$objDB->sql_query($SQL);

	$_SESSION['SuccessMsg'] = "Slider Added Successfully!";
	header("Location: index.php?p=banners&pg_no=".$_REQUEST['pg_no']);exit();
}

//==================================  Update  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "UPDATE")
{
	if(!empty($_FILES['CImage']['name']))
	{  
		if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
		{
				
				$FileName = $_FILES['CImage']['name'];
				$Lastpos = strrpos($FileName, '.');
				$Image = substr($FileName, 0 , $Lastpos);
				$FileExtension = substr($FileName, $Lastpos + 1 , strlen($FileName));
									
				$upload_file = $_FILES['CImage']['tmp_name'];
				$EventImage = 'page_'.substr(md5($upload_file.strtotime("now")),-5).".".$FileExtension;	
				@unlink("../uploads/banner/".GetFieldData("$Tbl","image","where id=".$_REQUEST['cms_id']));
				copy($_FILES['CImage']['tmp_name'],"../uploads/banner/".$EventImage);													
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Picture Not Uploaded";
			header("Location: index.php?p=banner_new&cms_id=".$_REQUEST['cms_id']."&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
	}
	else
	{
		$EventImage = $_REQUEST['OldImage'];
	}
	
	if(!empty($_FILES['PImage']['name']))
		{  
			if($_FILES['PImage']['error'] == UPLOAD_ERROR_OK )
			{
					
					$FileName1 = $_FILES['PImage']['name'];
					$Lastpos = strrpos($FileName1, '.');
					$Image = substr($FileName1, 0 , $Lastpos);
					$FileExtension = substr($FileName1, $Lastpos + 1 , strlen($FileName1));
										
					$upload_file = $_FILES['PImage']['tmp_name'];
					$PEventImage = 'page_'.substr(md5($upload_file.strtotime("now")),-5).".".$FileExtension;	
					@unlink("../uploads/banner/".GetFieldData("$Tbl","popup_image","where id=".$_REQUEST['cms_id']));
					copy($_FILES['PImage']['tmp_name'],"../uploads/banner/".$PEventImage);							
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Picture Not Uploaded";
				header("Location: index.php?p=banner_new&cms_id=".$_REQUEST['cms_id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$PEventImage = $_REQUEST['OldPImage'];
		}
	
	/*if(!empty($_FILES['TImage']['name']))
		{  
			if($_FILES['TImage']['error'] == UPLOAD_ERROR_OK )
			{
					
					$FileName1 = $_FILES['TImage']['name'];
					$Lastpos = strrpos($FileName1, '.');
					$Image = substr($FileName1, 0 , $Lastpos);
					$FileExtension = substr($FileName1, $Lastpos + 1 , strlen($FileName1));
										
					$upload_file = $_FILES['TImage']['tmp_name'];
					$TEventImage = 'page_'.substr(md5($upload_file.strtotime("now")),-5).".".$FileExtension;	
					@unlink("../uploads/banner/".GetFieldData("$Tbl","thumb_icon","where id=".$_REQUEST['cms_id']));
					copy($_FILES['TImage']['tmp_name'],"../uploads/banner/".$TEventImage);							
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Picture Not Uploaded";
				header("Location: index.php?p=banner_new&cms_id=".$_REQUEST['cms_id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$TEventImage = $_REQUEST['TOldImage'];
		}
	*/
	$SQL = "UPDATE $Tbl SET 
			title ='".addslashes($_REQUEST['Title'])."',
			url ='".addslashes($_REQUEST['Links'])."',							
			image ='".addslashes($EventImage)."',
			popup_image ='".addslashes($PEventImage)."',
			thumb_icon ='".addslashes($TEventImage)."',		
			from_date ='".$_REQUEST['FromDate']."',
			to_date ='".$_REQUEST['ToDate']."',
			description ='".addslashes($_REQUEST['ContentDesc'])."'
			WHERE id='".$_REQUEST['cms_id']."'";
			
	//echo $SQL;exit;	
	$objDB->sql_query($SQL);

	$_SESSION['SuccessMsg'] = "Slider Updated Successfully!";
	header("Location: index.php?p=banners&pg_no=".$_REQUEST['pg_no']);exit();
}
//==================================  SINGLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	//echo $_REQUEST['cms_id'];exit;
		@unlink("../uploads/banner/".GetFieldData("$Tbl","image","where id=".$_REQUEST['ID']));
		@unlink("../uploads/banner/".GetFieldData("$Tbl","thumb_icon","where id=".$_REQUEST['ID']));
		DeleteData($Tbl,"id","=",$_REQUEST['ID']);
		$_SESSION['SuccessMsg'] = "Slider Deleted Successfully";
		header("Location: index.php?p=banners&pg_no=".$_REQUEST['pg_no']);exit();
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
		//print_r($_REQUEST['del']);exit;
		for($i=0;$i<count($_REQUEST['del']);$i++)
		{
			DeleteData($Tbl,'id','=',$_REQUEST['del'][$i]);
		}
		$_SESSION['SuccessMsg'] = "Slider Deleted Successfully";
		header("Location: index.php?p=banners&pg_no=".$_REQUEST['pg_no']);
		exit();
}
if($_REQUEST['Process']=='changestatus'){
	$SQL = "update $Tbl set status='".$_REQUEST['st']."' WHERE id=".$_REQUEST['id'];
		//echo $SQL;exit;	
	$objDB->sql_query($SQL);

	$_SESSION['SuccessMsg'] = "Status Changed Successfully";
		header("Location: index.php?p=banners&pg_no=".$_REQUEST['pg_no']);
		exit();
}
exit();
?>
