<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$sql = "select * from occation where title='".addslashes($_REQUEST['Title'])."' and id != ".$_REQUEST['id']." and place_id = ".$_REQUEST['Place'];;
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Occasion Already Exists!';
			header("Location: index.php?p=occation_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$SQL = "UPDATE occation SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Occasion Updated Successfully!';
		header("Location: index.php?p=occation_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$sql = "select * from occation where title='".addslashes($_REQUEST['Title'])."' and place_id = ".$_REQUEST['Place'];
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Occasion Already Exists!';
			header("Location: index.php?p=occation_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$SQL = "INSERT occation SET ";		
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		
		$_SESSION['SuccessMsg'] = 'Occasion Added Successfully!';
		header("Location: index.php?p=occation_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$sql = "select * from sermon where occation = ".$_REQUEST['ID'];
	$cu = $objDB->select($sql);
	if(count($cu)>0){
		$_SESSION['ErrorMsg'] = 'You can not delete this Occasion. It is already assigned to few Resource Sermons.';
		header("Location: index.php?p=occation_list&pg_no=".$_REQUEST['pg_no']);	
		exit;
	}
	$SQL = "DELETE FROM occation ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Occation Deleted Successfully!';
	header("Location: index.php?p=occation_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$sql = "select * from sermon where occation = ".$_REQUEST['del'][$i];
		$cu = $objDB->select($sql);
		if(count($cu)>0){
			$_SESSION['ErrorMsg'] = 'You can not delete this Occasion. It is already assigned to few Resource Sermons.';
			header("Location: index.php?p=occation_list&pg_no=".$_REQUEST['pg_no']);	
			exit;
		}
		$SQL = "DELETE FROM occation ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Occation Deleted Successfully!';
	
	header("Location: index.php?p=occation_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
