<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{		
		
		$SQL = "UPDATE contact SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "category_id='".addslashes($_REQUEST['CategoryId'])."',";
		$SQL .= 'address="'.addslashes($_REQUEST['Address']).'",';
		$SQL .= "city='".addslashes($_REQUEST['City'])."',";
		$SQL .= "state='".addslashes($_REQUEST['State'])."',";
		$SQL .= "country='".addslashes($_REQUEST['Country'])."',";
		$SQL .= "zipcode='".addslashes($_REQUEST['Zipcode'])."',";
		$SQL .= "main_phone='".addslashes($_REQUEST['MainPhone'])."',";
		$SQL .= "personage_phone='".addslashes($_REQUEST['PersonagePhone'])."',";
		$SQL .= "email='".addslashes($_REQUEST['Email'])."',";
		$SQL .= "website='".addslashes($_REQUEST['Website'])."',";
		$SQL .= "mobile='".addslashes($_REQUEST['Mobile'])."',";
		$SQL .= "pastor_name='".addslashes($_REQUEST['PastorName'])."',";
		$SQL .= "pastor_email='".addslashes($_REQUEST['PastorEmail'])."',";
		$SQL .= "daytime_desc='".addslashes($_REQUEST['Daytime'])."',";
		$SQL .= "meeting='".addslashes($_REQUEST['Meeting'])."',";
		$SQL .= "description='".addslashes($_REQUEST['Content'])."',";
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Contact Updated Successfully!';
		header("Location: index.php?p=contact_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{		
		$SQL = "INSERT contact SET ";		
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "category_id='".addslashes($_REQUEST['CategoryId'])."',";
		$SQL .= 'address="'.addslashes($_REQUEST['Address']).'",';
		$SQL .= "city='".addslashes($_REQUEST['City'])."',";
		$SQL .= "state='".addslashes($_REQUEST['State'])."',";
		$SQL .= "country='".addslashes($_REQUEST['Country'])."',";
		$SQL .= "zipcode='".addslashes($_REQUEST['Zipcode'])."',";
		$SQL .= "main_phone='".addslashes($_REQUEST['MainPhone'])."',";
		$SQL .= "personage_phone='".addslashes($_REQUEST['PersonagePhone'])."',";
		$SQL .= "email='".addslashes($_REQUEST['Email'])."',";
		$SQL .= "website='".addslashes($_REQUEST['Website'])."',";
		$SQL .= "mobile='".addslashes($_REQUEST['Mobile'])."',";
		$SQL .= "pastor_name='".addslashes($_REQUEST['PastorName'])."',";
		$SQL .= "pastor_email='".addslashes($_REQUEST['PastorEmail'])."',";
		$SQL .= "daytime_desc='".addslashes($_REQUEST['Daytime'])."',";
		$SQL .= "meeting='".addslashes($_REQUEST['Meeting'])."',";
		$SQL .= "description='".addslashes($_REQUEST['Content'])."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Contact Added Successfully!';
		header("Location: index.php?p=contact_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
//	unlink()
	
	$SQL = "DELETE FROM contact ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Contact Deleted Successfully!';
	header("Location: index.php?p=contact_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		
		$SQL = "DELETE FROM contact ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Contacts Deleted Successfully!';
	
	header("Location: index.php?p=contact_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
