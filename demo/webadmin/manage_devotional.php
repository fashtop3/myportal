<?php  
ob_start();
session_start();
require_once("../utils/config.php");
require_once("../utils/functions.php");
require_once("../utils/dbClass.php");
require_once("../utils/SimpleImage.php");
$objDB = new MySQLCN;

//======================== Update ========================
if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
{
	
	$SQL = "UPDATE devotional_archive SET ";
	$SQL .= 'title="'.addslashes($_REQUEST['ContentHeading']).'",';
	$SQL .= 'ddate="'.($_REQUEST['CDate']).'",';
	$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';		
	$SQL .= 'full_description="'.addslashes($_REQUEST['FullContent']).'",';		
	$SQL .= "modified='".date('Y-m-d H:i:s')."',";
	$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
	$SQL .= " WHERE id=".$_REQUEST['id'];
   
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Devotional Content Updated Successfully!';
	header("Location: index.php?p=devotional_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}
else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
{		
	
	$SQL = "INSERT devotional_archive SET ";		
	$SQL .= 'title="'.addslashes($_REQUEST['ContentHeading']).'",';
	$SQL .= 'ddate="'.($_REQUEST['CDate']).'",';
	$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';		
	$SQL .= 'full_description="'.addslashes($_REQUEST['FullContent']).'",';		
	$SQL .= "created='".date('Y-m-d H:i:s')."',";
	$SQL .= "createdby='".$_SESSION['AdminID']."'";	
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Devotional Content Added Successfully!';
	header("Location: index.php?p=devotional_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}
	
//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{	
	$SQL = "DELETE FROM devotional_archive ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Devotional Content Deleted Successfully!';
	header("Location: index.php?p=devotional_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{		
		$SQL = "DELETE FROM devotional_archive ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
	}
	$_SESSION['SuccessMsg'] = 'Devotional Content Deleted Successfully!';
	
	header("Location: index.php?p=devotional_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
