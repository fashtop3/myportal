<?php
session_start();
require_once("../utils/config.php");
require_once("../utils/color_config.php");
require_once("../utils/functions.php");
require_once("../utils/dbclass.php");
require_once("../utils/paging.php");
$objDB = new MySQLCN;

$id = $_REQUEST['id'];
$read = $_REQUEST['reading'];
if($_REQUEST['SDate']=='')
	$date = $_REQUEST['d'];
else
	$date = $_REQUEST['SDate'];

if($_REQUEST['p']=='show')
	$date = '';
$sql = "UPDATE prayer SET p_read = ".$read." where p_id = ".$id;
$objDB->sql_query($sql);

?>
<?php
if($_SESSION['AdminID']!=1){
	$plc = FetchValue("admin","place_id","UserID",$_SESSION['AdminID']);
	$where = "where place_id = 0 or place_id = ".$plc;
}else{
	$where = "where 1";
	$plc = 0;
}
?>
<?php if($_REQUEST['pg_no']!=''){
?>
<html>
<head>
<title><?php echo COMPANY_NAME?>- Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
<?php } ?>
<script type="text/javascript" src="../js/jquery-1.4.1.js"></script>
<script src="../js/jquery.ui.core.js" type="text/javascript"></script>
<script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>

<?php //if($_REQUEST['pg_no']!=''){?>
<script type="text/javascript" src="../js/jquery.mousewheel-3.0.2.pack.js"></script>
<script type="text/javascript" src="../js/jquery.fancybox-1.3.0.pack.js"></script>
<link rel="stylesheet" type="text/css" href="../css/jquery.fancybox-1.3.0.css" media="screen" />
<?php //} ?>

<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">

<?php if($_REQUEST['pg_no']!=''){?>
</head>
<body id="showPrayer">
<?php } ?>
<script type="text/javascript">
	$(function() {
		$('#SDate').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1970:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});
	$(document).ready(function() {
		$(".example2").fancybox({
			'titleShow'     : true,
			'transitionIn'	: 'elastic',
			'transitionOut'	: 'elastic'
		});
	});

function Read(value,d){
	//alert(value);
	$.post("read.php",{ id:value, reading:1,d:d ,p:'<?php echo $_REQUEST['p']?>'} ,function(data)
	{
		$("#showPrayer").html(data);
	});
}
function Unread(value,d){
	//alert(value);
	$.post("read.php",{ id:value, reading:0,d:d,p:'<?php echo $_REQUEST['p']?>' } ,function(data)
	{
		$("#showPrayer").html(data);
	});
}
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><span style="font:Arial, Helvetica, sans-serif; font-size:24px; color:#009999">Prayers</span> <span style="font-size:16px; color:#006666; font-weight:bold; padding-left:5px;">(<?php echo date('d/m/Y',strtotime($date));?>)</span> </td>
  </tr>
  <tr style="font:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
    <td align="right" valign="middle">    	
    	<span style="float:right">      
      	<img src="images/export1.gif" alt="" onClick="window.location='exportunread.php?SDate=<?php echo $date?>'" onMouseOver="this.style.cursor='pointer'" />
      	<img src="images/export2.gif" alt="" onClick="window.location='exportread.php?SDate=<?php echo $date?>'" onMouseOver="this.style.cursor='pointer'" />
      </span>
      <form action="" method="post" style="padding-top:5px;">
    		Enter Date: <input name="SDate" title="*" validate="required:true" type="text" style="border:1px solid #CCCCCC" id="SDate" value="<?php echo $RecordSet[0]["start_date"] ?>" readonly="readonly" />
      	<input type="submit" value="Go" style="background-color:#009999; border:1px solid #006666; color:#FFFFFF; padding:1px; cursor:pointer" />
        <input type="button" value="Today's Prayers" onClick="window.location='prayers.php'" style="background-color:#009999; border:1px solid #006666; color:#FFFFFF; padding:1px; cursor:pointer" />
        <input type="button" value="Show All" onClick="window.location='prayers.php?p=show'" style="background-color:#009999; border:1px solid #006666; color:#FFFFFF; padding:1px; cursor:pointer" />&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;
    	</form>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top"><div id="Accordion1" class="Accordion" tabindex="0">        
        <div class="AccordionPanel">
        	<?php 		
						if($_REQUEST['p']=='show')
							$sql = "select * from prayer ".$where." and p_read=0 order by p_id";				
						else
							$sql = "select * from prayer ".$where." and p_read=0 and DATE(created) = '".$date."' order by p_id";								
						$objPaging = new paging($sql, "p_id",RECORD_PER_PAGE);
						$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
						$img = $objDB->select($objPaging->get_query());									
					?>
          <div class="AccordionPanelTab"><img src="../images/orange_arrow.gif" alt="" hspace="5">Unread Prayers<span style="float:right;"> Page: <?php echo $objPaging->show_paging() ?></span></div>
          <div class="AccordionPanelContent" style="height:400px;"><span style="font-size:14px; padding-bottom:5px; color:#FF0000">Click on chechbox to mark as READ.</span>
            <form name="frmCms" id="frmCms" action="" method="post">
              <table width="100%" border="0" cellspacing="1" cellpadding="5">
                <tr bgcolor="#993300">
                  <td align="left" width="5%" valign="top" style="color:#FFFFFF; font-weight:bold;">Read</td>
                  <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Name</td>
                  <td align="left" valign="top" style="color:#FFFFFF; font-weight:bold;">Prayer</td>
                  <?php if($_SESSION['AdminID']==1){ ?>
                  <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Place</td>
                  <?php } ?>
                </tr>
                <?php if(count($img)>0){
								for($i=0;$i<count($img);$i++){
							?>
                <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><input type="checkbox" value="<?php echo $img[$i]['p_id'];?>" onClick="Read(this.value,'<?php echo $date?>')" name="Read<?php echo $i?>" id="Read<?php echo $i?>" /></td>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo stripslashes($img[$i]['p_name'])?> <?php if($img[$i]['type']!='') echo "(".stripslashes($img[$i]['type']).")"; ?></td>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo substr(wordwrap(stripslashes($img[$i]['p_prayer']),60,"\n",1),0,250); if(strlen(stripslashes($img[$i]['p_prayer']))>=251){?><br><a href="#" onClick="window.open('prayer_detail.php?id=<?php echo $img[$i]['p_id']?>','','width=600,height=500,scrollbars=1')">Read More</a><?php } ?></td>
                  <?php if($_SESSION['AdminID']==1){ ?>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php if($img[$i]['place_id']!=0) echo stripslashes(FetchValue("county","c_name","c_id",$img[$i]['place_id'])); else{
									 $pl = FetchData("county",array(),""); 
										$pl1=array(); 
										for($p=0;$p<count($pl);$p++) 
											$pl1[] = str_replace(","," -",stripslashes($pl[$p]['c_name'])); 
										echo @implode(', ',$pl1);
										}?></td>
                  <?php } ?>
                </tr>
                <tr>
                  <td height="3"  colspan="4"></td>
                </tr>
                <?php } ?>
                <?php }else{ ?>
                <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                  <td align="left" valign="top" colspan="4">No record found&nbsp;</td>
                </tr>
                <?php } ?>
              </table>
            </form>
          </div>
        </div>        
        <div class="AccordionPanel">
        	<?php 
					if($_REQUEST['p']=='show')
						$sql = "select * from prayer ".$where." and p_read=1 order by p_id";								
					else
						$sql = "select * from prayer ".$where." and p_read=1 and DATE(created) = '".$date."' order by p_id";								
					$objPaging = new paging($sql, "p_id",RECORD_PER_PAGE);
					$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
					$img1 = $objDB->select($objPaging->get_query());
				?>
          <div class="AccordionPanelTab"><img src="../images/orange_arrow.gif" alt="" hspace="5">Readed Prayers<span style="float:right"> Page: <?php echo $objPaging->show_paging() ?></span></div>
          <div class="AccordionPanelContent" style="height:400px;"><span style="font-size:14px; padding-bottom:5px; color:#FF0000">Click on chechbox to mark as UNREAD.</span>
            <form name="frmCms" id="frmCms" action="" method="post">
              <table width="100%" border="0" cellspacing="1" cellpadding="5">
                <tr bgcolor="#669900">
                  <td align="left" width="5%" valign="top" style="color:#FFFFFF; font-weight:bold;">Unread</td>
                  <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Name</td>
                  <td align="left" valign="top" style="color:#FFFFFF; font-weight:bold;">Prayer</td>
                  <?php if($_SESSION['AdminID']==1){ ?>
                  <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Place</td>
                  <?php } ?>
                </tr>
                <?php if(count($img1)>0){
								for($i=0;$i<count($img1);$i++){
							?>
                <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><input type="checkbox" value="<?php echo $img1[$i]['p_id'];?>" onClick="Unread(this.value,'<?php echo $date?>')" name="Read<?php echo $i?>" id="Read<?php echo $i?>" /></td>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo stripslashes($img1[$i]['p_name'])?></td>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo substr(wordwrap(stripslashes($img1[$i]['p_prayer']),60,"\n",1),0,250); if(strlen(stripslashes($img1[$i]['p_prayer']))>=251){?><br><a href="#" onClick="window.open('prayer_detail.php?id=<?php echo $img1[$i]['p_id']?>','','width=600,height=500,scrollbars=1')">Read More</a><?php } ?></td>
                  <?php if($_SESSION['AdminID']==1){ ?>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php if($img1[$i]['place_id']!=0) echo stripslashes(FetchValue("county","c_name","c_id",$img1[$i]['place_id'])); else{
									 $pl = FetchData("county",array(),""); 
										$pl1=array(); 
										for($p=0;$p<count($pl);$p++) 
											$pl1[] = str_replace(","," -",stripslashes($pl[$p]['c_name'])); 
										echo @implode(', ',$pl1);
										}?></td>
                  <?php } ?>
                </tr>
                <tr>
                  <td height="3"  colspan="4"></td>
                </tr>
                <?php } ?>
                <?php }else{ ?>
                <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                  <td align="left" valign="top" colspan="4">No record found&nbsp;</td>
                </tr>
                <?php } ?>
              </table>
            </form>
          </div>
        </div>
      </div></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<script type="text/javascript">
<!--
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
//-->
</script>
<?php if($_REQUEST['pg_no']!=''){?>
</body>
</html>
<?php } ?>