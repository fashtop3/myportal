<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	$objDB = new MySQLCN;
	

	// ================= Add Testimonial =================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		
		$FileName = addslashes(str_replace("&","",$_FILES['NonImage']['name']));
		if($FileName!=''){										
			$img_extension1=@explode(".",$_FILES['NonImage']['name']);	
			$upload_file = $_FILES['NonImage']['tmp_name'];
			$unit_pattern = date('ymdHis').".".$img_extension1[count($img_extension1)-1];	
			$path = "../uploads/member/big/";//.$_REQUEST['GalleryId'];
			copy($_FILES['NonImage']['tmp_name'], $path.$unit_pattern);	
			$path = "../uploads/member/small/";//.$_REQUEST['GalleryId'];
			copy($_FILES['NonImage']['tmp_name'], $path.$unit_pattern);	
		}else{
			$unit_pattern = '';
		}
		$desc = addslashes($_REQUEST["message"]);
		$SQL = "INSERT testimonial SET ";
		$SQL .= 'title = "'.addslashes($_REQUEST['Title']).'",';
		if($_REQUEST['Member']=='' && $_REQUEST['NonMember']==''){
			$SQL .= "createdby = '0',";			
			$SQL .= "admin_modifiedby='".$_SESSION['AdminID']."',";
			$SQL .= "name = '',";
		}else if($_REQUEST['Member']!='0' && $_REQUEST['Member']!=''){
			$SQL .= "createdby = '".$_REQUEST['Member']."',";
			$SQL .= "name = '',";
		}else if($_REQUEST['NonMember']!=''){
			$SQL .= "createdby = '',";
			$SQL .= "name = '".$_REQUEST['NonMember']."',";
		}
		$SQL .= 'image="'.$unit_pattern.'",';
		$SQL .= 'position="4",';
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= 'description = "'.$desc.'",';
		$SQL .= "created = '".date('Y-m-d H:i:s')."'";
		
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Testimonial Added Successfully!';
		header("Location: index.php?p=testi_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{		
		$FileName = addslashes(str_replace("&","",$_FILES['NonImage']['name']));
		if($FileName!=''){			
			$img_extension1=@explode(".",$_FILES['NonImage']['name']);								
			$upload_file = $_FILES['NonImage']['tmp_name'];
			$unit_pattern = date('ymdHis').".".$img_extension1[count($img_extension1)-1];	
			$path = "../uploads/member/big/";//.$_REQUEST['GalleryId'];
			copy($_FILES['NonImage']['tmp_name'], $path.$unit_pattern);	
			$path = "../uploads/member/small/";//.$_REQUEST['GalleryId'];
			copy($_FILES['NonImage']['tmp_name'], $path.$unit_pattern);	
		}else{
			$unit_pattern = $_REQUEST['OldNonImage'];
		}
		$path = "../uploads/member/";
		$SQL = "SELECT * FROM testimonial ";
		$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
		$img = $objDB->sql_query($SQL);
		if($img[0]['image']!='notavailable.jpg'){
			unlink($path."/big/".$img[0]['image']);
			unlink($path."/small/".$img[0]['image']);
		}			
		$desc = addslashes($_REQUEST["message"]);
		$SQL = "UPDATE testimonial SET ";
		$SQL .= 'title = "'.addslashes($_REQUEST['Title']).'",';
		if($_REQUEST['Member']=='' && $_REQUEST['NonMember']==''){
			$SQL .= "createdby = '0',";			
			$SQL .= "modifiedby = '0',";
			$SQL .= "admin_modifiedby='".$_SESSION['AdminID']."',";
			$SQL .= "name = '',";
		}else if($_REQUEST['Member']!='0' && $_REQUEST['Member']!=''){
			$SQL .= "createdby = '".$_REQUEST['Member']."',";
			$SQL .= "name = '',";
		}else if($_REQUEST['NonMember']!=''){
			$SQL .= "createdby = '',";
			$SQL .= "name = '".$_REQUEST['NonMember']."',";
		}		
		$SQL .= 'image="'.$unit_pattern.'",';
		$SQL .= 'position="4",';
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= 'description = "'.$desc.'",';
		$SQL .= "modified = '".date('Y-m-d H:i:s')."'";
		$SQL .= " WHERE id=".$_REQUEST['ID'];
	    	
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Testimonial Updated Successfully!';
		header("Location: index.php?p=testi_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	
	
//==================================  SINGLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
		DeleteData('testimonial',"id","=",$_REQUEST['ID']);
		$_SESSION['SuccessMsg'] = "Testimonial Deleted Successfully";
		header("Location: index.php?p=testi_list&pg_no=".$_REQUEST['pg_no']);
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
		//print_r($_REQUEST['del']);exit;
		for($i=0;$i<count($_REQUEST['del']);$i++)
		{
			DeleteData('testimonial','id','=',$_REQUEST['del'][$i]);
		}
		$_SESSION['SuccessMsg'] = "Testimonials Deleted Successfully";
		header("Location: index.php?p=testi_list&pg_no=".$_REQUEST['pg_no']);
}
?>