<?php
ob_start();
session_start();
require_once("../../utils/config.php");
require_once("../../utils/functions.php");
require_once("../../utils/dbClass.php");
//require_once("../../utils/SimpleImage.php");
$objDB = new MySQLCN;
include("JSON.php");
$result = array();
 
if (isset($_FILES['photoupload']) )
{
	$file = $_FILES['photoupload']['tmp_name'];
	$error = false;
	$size = false;
	
	/*if (!$error && !($size = @getimagesize($file) ) )
	{
		$error = 'Please upload only images, no other files are supported.';
	}
	if (!$error && !in_array($size[2], array(1, 2, 3, 7, 8) ) )
	{
		$error = 'Please upload only images of type JPEG.';
	}
	if (!$error && ($size[0] < 25) || ($size[1] < 25))
	{
		$error = 'Please upload an image bigger than 25px.';
	}*/
	if (!is_uploaded_file($file) || ($_FILES['photoupload']['size'] > 20 * 1024 * 1024) )
	{
		$error = 'Please upload only files smaller than 20Mb!';
	}else {
		$FileName = $_FILES['photoupload']['name'];
		$img_extension1=@explode(".",$_FILES['photoupload']['name']);		
		$CImage = $_REQUEST['GalleryId'].'_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1];
		$filename = $_REQUEST['GalleryId'].'_'.date("ymdhis")."_".$_FILES['photoupload']['name'];
		//move_uploaded_file($_FILES['photoupload']['tmp_name'], "./uploadedfiles/".$filename);
		$path = "../../uploads/news";
		/*$image = new SimpleImage();
		$image->load($_FILES['photoupload']['tmp_name']);*/
		//$image->scale(100);
		//$image->save($path."/big/".$CImage);
		copy($_FILES['photoupload']['tmp_name'], $path."/big/".$filename);
		//copy($_FILES['photoupload']['tmp_name'], $path."/small/".$CImage);
		/*$image->scale(45);
		$image->save($path."/small/".$CImage);		*/	
		//chmod("./uploadedfiles/".$filename, 0777);
 		
		$SQL = "INSERT news_gallery_images SET ";		
		$SQL .= 'title="'.str_replace('"',"'",$_REQUEST['Title']).'",';
		$SQL .= "image='".$filename."',";
		$SQL .= "news_id='".$_REQUEST['NewsId']."',";
		$SQL .= "gallery_id='".$_REQUEST['GalleryId']."',";
		$SQL .= 'description="'.str_replace('"',"'",$_REQUEST['Content']).'",';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';	
		$SQL .= "admin_createdby='".$_SESSION['AdminID']."',";
		$SQL .= "createdby='0'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
	}
		
	$addr = gethostbyaddr($_SERVER['REMOTE_ADDR']);
 
	$log = fopen('script.log', 'a');
	fputs($log, ($error ? 'FAILED' : 'SUCCESS') . ' - ' . preg_replace('/^[^.]+/', '***', $addr) . ": {$filename} - {$_FILES['photoupload']['size']} byte\n" );
	fclose($log);
 
	if ($error)
	{
		$result['result'] = 'failed';
		$result['error'] = $error;
	}
	else
	{
		$result['result'] = 'success';
		$result['size'] = "Image uploaded";//"Uploaded an image ({$size['mime']}) with  {$size[0]}px/{$size[1]}px.";
	}
 
}
else
{
	$result['result'] = 'error';
	$result['error'] = 'Missing file or internal error!';
}
 
if (!headers_sent() )
{
	header('Content-type: application/json');
}
 
echo json_encode($result);
 
?>
