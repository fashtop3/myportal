<?php require_once "phpuploader/include_phpuploader.php" ?>
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="images/addgallery.jpg" width="48" height="48" /></td>
		<td colspan=5 class="tbl_head" height="24">GALLERY MULTIPLE IMAGE UPLOAD</td>
	</tr>
	<tr>
		<td width="" colspan="6">
			<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
				<tbody>
					<tr>
						<td align="left" valign="top" class="fieldlabel">
							<?php
								$uploader=new PhpUploader();
						
								$uploader->MultipleFilesUpload=true;
								$uploader->InsertText="Select multiple files (Max 10M)";
								
								$uploader->MaxSizeKB=10240;
								$uploader->AllowedFileExtensions="*.jpg,*.png,*.gif,*.jpeg";
								
								$uploader->UploadUrl="demo2_upload.php?Title=".addslashes($_REQUEST['Title1'])."&GalleryId=".$_REQUEST['GalleryId1']."&Place=".$_REQUEST['Place1']."&Content=".addslashes($_REQUEST['Content1']);
								
								$uploader->Render();
							?>
						
                            <br/><br/>
                
                           	<ol id="filelist">
							</ol>
                            <br/><br/>
                           </td>
					</tr>
				</tbody>
			</table>
		</td>
	</tr>
</table>
