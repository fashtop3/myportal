<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{		
		if($_REQUEST['apath']!=''){
			$apath = $_REQUEST['apath']."&pg_no=".$_REQUEST['pg_no'];
			$bpath = $apath;
		}
		else{
			$apath = "administrator_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no'];
			$bpath = "administrator&pg_no=".$_REQUEST['pg_no'];
		}
		$sql = "select * from admin where UserID != ".$_REQUEST['id']." and UserName = '".$_REQUEST['UserName']."'";
		$users = $objDB->select($sql);
		if($users[0]['UserID']!=''){
			$_SESSION['ErrorMsg'] = "Username already exists";
			header("Location: index.php?p=".$apath);
			exit;
		}
		$sql = "select * from admin where UserID != ".$_REQUEST['id']." and Email = '".$_REQUEST['Email']."'";
		$users = $objDB->select($sql);
		if($users[0]['UserID']!=''){
			$_SESSION['ErrorMsg'] = "Email Address already exists";
			header("Location: index.php?p=".$apath);
			exit;
		}
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=".$apath);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('admin_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/member";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);
					
					$SQL = "SELECT * FROM member ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['image']!=''){
						unlink($path."/big/".stripslashes($img[0]['image']));
						unlink($path."/small/".stripslashes($img[0]['image']));
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=".$apath);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		if($_REQUEST['attend'] == 'on')
			$attend = 'yes';
		else
			$attend = 'no';
		$SQL = "UPDATE admin SET ";
		$SQL .= "FirstName='".addslashes($_REQUEST['FirstName'])."',";
		$SQL .= "place_id='".addslashes($_REQUEST['Place'])."',";
		$SQL .= "LastName='".addslashes($_REQUEST['LastName'])."',";
		$SQL .= "UserName='".addslashes($_REQUEST['UserName'])."',";
		$SQL .= "Password='".$_REQUEST['Password']."',";
		$SQL .= "address='".addslashes($_REQUEST['Address'])."',";		
		$SQL .= "Email='".addslashes($_REQUEST['Email'])."',";		
		$SQL .= "phone='".addslashes($_REQUEST['Phone'])."',";
		$SQL .= "image='".$CImage."',";		
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby=".$_SESSION['AdminID'];
		$SQL .= " WHERE UserID=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Administrator Updated Successfully!';
		header("Location: index.php?p=".$bpath);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{	
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=administrator_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('admin_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/member";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);
									
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=administrator_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = "notavailable.jpg";
		}	
		if($_REQUEST['attend'] == 'on')
			$attend = 'yes';
		else
			$attend = 'no';
		$SQL = "INSERT admin SET ";
		$SQL .= "FirstName='".addslashes($_REQUEST['FirstName'])."',";
		$SQL .= "place_id='".addslashes($_REQUEST['Place'])."',";
		$SQL .= "LastName='".addslashes($_REQUEST['LastName'])."',";
		$SQL .= "UserName='".addslashes($_REQUEST['UserName'])."',";
		$SQL .= "Password='".$_REQUEST['Password']."',";
		$SQL .= "address='".addslashes($_REQUEST['Address'])."',";		
		$SQL .= "Email='".addslashes($_REQUEST['Email'])."',";		
		$SQL .= "phone='".addslashes($_REQUEST['Phone'])."',";
		$SQL .= "image='".$CImage."',";		
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='1'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Administrator Added Successfully!';
		header("Location: index.php?p=administrator&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	/* Delete User Photo*/
	$sql = "select * from admin where id = '".$_REQUEST['ID']."'";
	$user = $objDB->select($sql);
	if($user[0]['image']!='notavailable.jpg')
	{
		@unlink('../uploads/member/big/'.$user[0]['image']);
		@unlink('../uploads/member/small/'.$user[0]['image']);
	}
	
	/* Delete User Created Sermons*/
	$sql = "select * from sermon where admin_createdby = '".$_REQUEST['ID']."'";
	$ser = $objDB->select($sql);
	for($i=0;$i<count($ser);$i++)
	{
		if($ser[$i]['image']!='')
		{
			@unlink('../uploads/sermon/image/big/'.stripslashes($ser[$i]['image']));
			@unlink('../uploads/sermon/image/small/'.stripslashes($ser[$i]['image']));
		}
		if($ser[$i]['file']!='')
		{
			@unlink('../uploads/sermon/download/'.stripslashes($ser[$i]['file']));
		}
		if($ser[$i]['video']!='')
		{
			@unlink('../uploads/sermon/video/'.stripslashes($ser[$i]['video']));
		}
	}
	$objDB->sql_query("delete from sermon where admin_createdby = '".$_REQUEST['ID']."'");
	
	/* Delete User Created Tracts*/
	$sql = "select * from tract where admin_createdby = '".$_REQUEST['ID']."'";
	$ser = $objDB->select($sql);
	for($i=0;$i<count($ser);$i++)
	{
		if($ser[$i]['image']!='')
		{
			@unlink('../uploads/tract/image/big/'.stripslashes($ser[$i]['image']));
			@unlink('../uploads/tract/image/small/'.stripslashes($ser[$i]['image']));
		}
		if($ser[$i]['file']!='')
		{
			@unlink('../uploads/tract/download/'.stripslashes($ser[$i]['file']));
		}
		if($ser[$i]['video']!='')
		{
			@unlink('../uploads/tract/video/'.stripslashes($ser[$i]['video']));
		}
	}
	$objDB->sql_query("delete from tract where admin_createdby = '".$_REQUEST['ID']."'");

	/* Delete User Created Reflection Pages*/	
	$objDB->sql_query("delete from reflection where admin_createdby = '".$_REQUEST['ID']."'");
		
	/* Delete User Created News*/	
	$sql = "select * from news where admin_createdby = '".$_REQUEST['ID']."'";
	$ser = $objDB->select($sql);
	for($i=0;$i<count($ser);$i++){
		$sql = "select * from news_gallery_category where news_id = '".$ser[$i]['id']."'";
		$cu = $objDB->select($sql);
		for($j=0;$j<count($cu);$j++){
			$sql = "select * from news_gallery_images where gallery_id = '".$cu[$j]['id']."'";
			$img = $objDB->select($sql);
			for($k=0;$k<count($img);$k++){
				@unlink('../uploads/news/big/'.stripslashes($img[$k]['image']));
				@unlink('../uploads/news/small/'.stripslashes($img[$k]['image']));
			}
			//rmdir('../uploads/news/news_gallery_'.$cu[$j]['id'].'/big');
			//rmdir('../uploads/news/news_gallery_'.$cu[$j]['id'].'/small');
			//rmdir('../uploads/news/news_gallery_'.$cu[$j]['id']);
			$objDB->sql_query("delete from news_gallery_images where gallery_id = '".$cu[$j]['id']."'");
		}
		$objDB->sql_query("delete from news_gallery_category where news_id = '".$ser[$i]['id']."'");
	}
	
	$objDB->sql_query("delete from news where admin_createdby = '".$_REQUEST['ID']."'");
	
	/* Delete User Created Events*/
	$sql = "select * from event where admin_createdby = '".$_REQUEST['ID']."'";
	$ser = $objDB->select($sql);
	for($i=0;$i<count($ser);$i++)
	{
		if($ser[$i]['image']!='notavailable.jpg')
		{
			@unlink('../uploads/event/big/'.stripslashes($ser[$i]['image']));
			@unlink('../uploads/event/small/'.stripslashes($ser[$i]['image']));
		}		
	}
	$objDB->sql_query("delete from event where admin_createdby = '".$_REQUEST['ID']."'");
	
	/* Delete User Created Testimonials*/	
	$objDB->sql_query("delete from testimonial where admin_createdby = '".$_REQUEST['ID']."'");
	
	/* Delete User Created Photo Gallery*/	
	$sql = "select * from news_gallery_category where admin_createdby = '".$_REQUEST['ID']."'";
	$cu = $objDB->select($sql);	
	for($i=0;$i<count($cu);$i++){
		$sql = "select * from news_gallery_images where gallery_id = '".$cu[$i]['gallery_id']."'";
		$img = $objDB->select($sql);
		for($j=0;$j<count($img);$j++){
			@unlink('../uploads/news/big/'.stripslashes($img[$j]['image']));
			@unlink('../uploads/news/small/'.stripslashes($img[$j]['image']));
		}
		$objDB->sql_query("delete from news_gallery_images where gallery_id = '".$cu[$i]['gallery_id']."'");
		//$name = 'news_gallery_'.$cu[$i]['id'];	
		//rmdir('../uploads/news/'.$name.'/big');
		//rmdir('../uploads/news/'.$name.'/small');
		//rmdir('../uploads/news/'.$name);
	}
	$objDB->sql_query("delete from news_gallery_category where admin_createdby = '".$_REQUEST['ID']."'");
	
	/* Delete User Created Photo Gallery Images*/		
	$sql = "select * from news_gallery_images where admin_createdby = '".$_REQUEST['ID']."'";
	$img = $objDB->select($sql);
	for($j=0;$j<count($img);$j++){
		@unlink('../uploads/news/big/'.stripslashes($img[$j]['image']));
		@unlink('../uploads/news/small/'.stripslashes($img[$j]['image']));
	}		
	$objDB->sql_query("delete from news_gallery_images where admin_createdby = '".$_REQUEST['ID']."'");
	
	$SQL = "DELETE FROM admin ";
	$SQL .= "WHERE UserID = '".$_REQUEST['ID']."'";

	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Administrator Deleted Successfully!';
	header("Location: index.php?p=administrator&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	


?>
