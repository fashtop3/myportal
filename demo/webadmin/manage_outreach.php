<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$SQL = "UPDATE outreach SET ";
		$SQL .= "title='".addslashes($_REQUEST['ContentHeading'])."',";
		$SQL .= "description='".addslashes($_REQUEST['Content'])."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Outreach Content Updated Successfully!';
		header("Location: index.php?p=outreach_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$SQL = "INSERT outreach SET ";		
		$SQL .= "title='".addslashes($_REQUEST['ContentHeading'])."',";
		$SQL .= "description='".addslashes($_REQUEST['Content'])."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Outreach Content Added Successfully!';
		header("Location: index.php?p=outreach_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	
	
//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$SQL = "DELETE FROM outreach ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Outreach Content Deleted Successfully!';
	header("Location: index.php?p=outreach_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "DELETE FROM outreach ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Outreach Content Deleted Successfully!';
	
	header("Location: index.php?p=outreach_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
