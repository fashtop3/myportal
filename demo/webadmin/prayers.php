<?php
ob_start();
session_start();
require_once("../utils/config.php");
require_once("../utils/color_config.php");
require_once("../utils/functions.php");
require_once("../utils/dbclass.php");
require_once("../utils/paging.php");
$objDB = new MySQLCN;
if($_REQUEST['SDate']=='')
  $date = date('Y-m-d');
else
  $date = $_REQUEST['SDate'];
if($_REQUEST['p']=='show')
  $date = '';
?>
<html>
<head>
<title><?php echo COMPANY_NAME?>- Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<?php /* ?>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<script language="javascript" type="text/javascript" src="js/adminmenu.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqueryui.js"></script>
<script type="text/javascript" src="js/jquery.metadata.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.validate.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="js/calender.js"></script>
<?php */ ?>
<link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/favicon.ico">
<link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/favicon.ico">
<link rel="apple-touch-icon-precomposed" href="../images/favicon.ico">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<script type="text/javascript" src="js/jqueryui.js"></script>
<script type="text/javascript" src="js/jquery.metadata.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.validate.js"></script>
<link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">

<link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">
<link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="assets/plugins/morris/morris.css">
<link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
<link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css">
<link rel="stylesheet" href="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
<link rel="stylesheet" href="assets/plugins/datatables/dataTables.bootstrap.css">
<script src="assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script type="text/javascript" src="../js/highslide-with-html.js"></script>
<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script>

    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../js/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="../js/jquery.mousewheel-3.0.2.pack.js"></script>
<script type="text/javascript" src="../js/jquery.fancybox-1.3.0.pack.js"></script>

<script src="../js/jquery.ui.core.js" type="text/javascript"></script>


<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">
    <script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="assets/plugins/knob/jquery.knob.js"></script>
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="assets/plugins/fastclick/fastclick.min.js"></script>
    <script src="assets/dist/js/app.min.js"></script>
    <script src="assets/dist/js/demo.js"></script>
     <script src="assets/plugins/iCheck/icheck.min.js"></script>
    <link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/jquery.fancybox-1.3.0.css" media="screen" />
<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="../css/jquery.fancybox-1.3.0.css" media="screen" />

<link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
  $(function() {
    $('#SDate').datepicker({
      changeMonth: true,
      changeYear: true,
      yearRange: '1970:<?php echo date('Y')+20?>',
      dateFormat: 'yy-mm-dd'
    });
  });
  $(document).ready(function() {
    $(".example2").fancybox({
      'titleShow'     : true,
      'transitionIn'  : 'elastic',
      'transitionOut' : 'elastic'
    });
  });

function Read(value,d){
  //alert(value);
  $.post("read.php",{ id:value, reading:1,d:d,p:'<?php echo $_REQUEST['p']?>' } ,function(data)
  {
    $("#showPrayer").html(data);
  });
}
function Unread(value,d){
  //alert(value);
  $.post("read.php",{ id:value, reading:0,d:d,p:'<?php echo $_REQUEST['p']?>' } ,function(data)
  {
    $("#showPrayer").html(data);
  });
}
</script>
<link href="../SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
</head>

   <body class="skin-blue sidebar-mini">
    <div class="wrapper">
      <header class="main-header">
        <a href="index.php" class="logo">
          <!-- <img src="<?php echo ADMIN_IMAGE_PATH;?>/logo.gif" width="114" height="90" alt="Apostolic Faith Mission UK" title="Apostolic Faith Mission UK" border="0" /> -->
          <span class="logo-mini"><b>A</b></span>
          <span class="logo-lg"><b><?php echo COMPANY_NAME?></b></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="<?php echo ADMIN_IMAGE_PATH;?>/home.jpg" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo $_SESSION['AdminName']?></span>
                </a>
                <ul class="dropdown-menu">
                  <li class="user-header">
                    <img src="<?php echo ADMIN_IMAGE_PATH;?>/home.jpg" class="img-circle" alt="User Image">
                    <p>
                      <?php echo $_SESSION['AdminName']?> <?php if($_SESSION['PlaceID']==0) echo "(Super Administrator)"; else{ echo "(Administrator) (".stripslashes(FetchValue('county','c_name','c_id',$_SESSION['PlaceID']))." Church)";}?>
                     <!--  <small>Member since Nov. 2016</small> -->
                    </p>
                  </li>
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="index.php?p=admin" class="btn btn-default btn-flat">Profile</a>
                    </div>
                    <div class="pull-left">
                      <a href="index.php?p=logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <aside class="main-sidebar">
        <section class="sidebar">
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview"><a href="index.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-gear"></i>
                <span>Setup</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">               
                  <li><a href="index.php"><i class="fa fa-circle-o"></i> Admin Home</a></li>
                  <li><a href="index.php?p=admin"><i class="fa fa-circle-o"></i> My Account</a></li>
                  <?php if($_SESSION['PlaceID']==0) { ?>
                  <li><a href="index.php?p=config"><i class="fa fa-circle-o"></i> General Configuration</a></li>
                  <li><a href="index.php?p=menu_list"><i class="fa fa-circle-o"></i> Front Menu Config</a></li>
                  <?php } ?>
                  <li><a href="index.php?p=playlist"><i class="fa fa-circle-o"></i> Playlist Songs</a></li>
                  <li><a href="index.php?p=clock"><i class="fa fa-circle-o"></i> Live Clock Setting</a></li>
                  <li><a href="index.php?p=logout"><i class="fa fa-circle-o"></i> Logout</a></li>
              </ul>
            </li>            
            
            <li class="treeview">
              <a href="#">
                <i class="fa fa-music"></i>
                <span>Music</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=music_category"><i class="fa fa-circle-o"></i> Music Category</a></li>
                <li><a href="index.php?p=music_category_new"><i class="fa fa-circle-o"></i> Add Music Category</a></li>
                <li><a href="index.php?p=cd_list"><i class="fa fa-circle-o"></i> CDs/DVDs/Higherway  <br>Magazine/Music &amp; Tracks</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Master</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <?php if($_SESSION['PlaceID']==0) { ?>  
                <li><a href="index.php?p=place_list"><i class="fa fa-circle-o"></i> Lists All Places/County</a></li>
                <li><a href="index.php?p=place_addedit"><i class="fa fa-circle-o"></i> Add New Place/County</a></li>
                <?php } ?>
                <li><a href="index.php?p=schedule_list"><i class="fa fa-circle-o"></i> List Schedules</a></li>
                <li><a href="index.php?p=schedule_addedit"><i class="fa fa-circle-o"></i> Add New Schedule</a></li>
                <?php if($_SESSION['PlaceID']==0) { ?>
                <li><a href="index.php?p=administrator"><i class="fa fa-circle-o"></i> List All Administrators</a></li>
                <li><a href="index.php?p=administrator_addedit"><i class="fa fa-circle-o"></i> Add New Administrator</a></li>
                <?php } ?>
                <?php if($_SESSION['PlaceID']==0) { ?>
                <li><a href="index.php?p=shop"><i class="fa fa-circle-o"></i> List Online Payment Category</a></li>
                <li><a href="index.php?p=shop_addedit"><i class="fa fa-circle-o"></i> Add New Online Payment Category</a></li>
                <li><a href="index.php?p=camp_setting"><i class="fa fa-circle-o"></i> Camp Meeting</a></li>
                <?php } ?>
                <li><a href="index.php?p=banners"><i class="fa fa-circle-o"></i> List Banners</a></li>
                <li><a href="index.php?p=banner_new"><i class="fa fa-circle-o"></i> Add New Banner</a></li>
                <!-- <li><a href="index.php?p=curriculum_category_list"><i class="fa fa-circle-o"></i> List Curriculum Category</a></li>
                <li><a href="index.php?p=curriculum_category_addedit"><i class="fa fa-circle-o"></i> Add Curriculum Category</a></li>
                <li><a href="index.php?p=tract_category_list"><i class="fa fa-circle-o"></i> List Tract Category</a></li>
                <li><a href="index.php?p=tract_category_addedit"><i class="fa fa-circle-o"></i> Add Tract Category</a></li>
                <li><a href="index.php?p=occation_list"><i class="fa fa-circle-o"></i> List Occasions</a></li>
                <li><a href="index.php?p=occation_addedit"><i class="fa fa-circle-o"></i> Add Occasion</a></li> -->
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Content</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=banners"><i class="fa fa-circle-o"></i> List CMS Pages</a></li>
                <li><a href="index.php?p=banners"><i class="fa fa-circle-o"></i> List Outreach Pages</a></li>
                <li><a href="index.php?p=banners"><i class="fa fa-circle-o"></i> Add New Outreach Page</a></li>
                <li><a href="index.php?p=banners"><i class="fa fa-circle-o"></i> List Reflection Pages</a></li>
                <li><a href="index.php?p=banners"><i class="fa fa-circle-o"></i> Add New Reflection Page</a></li>
                <li><a href="index.php?p=banners"><i class="fa fa-circle-o"></i> List Doctrines  <br>(Seeking Christ) Pages</a></li>
                <li><a href="index.php?p=doctrines_addedit"><i class="fa fa-circle-o"></i> Add New Doctrines  <br>(Seeking Christ) Page</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>News/Event</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=news_list"><i class="fa fa-circle-o"></i> List News</a></li>
                <li><a href="index.php?p=news_addedit"><i class="fa fa-circle-o"></i> Add New News</a></li>
                <li><a href="index.php?p=event_list"><i class="fa fa-circle-o"></i> List Events</a></li>
                <li><a href="index.php?p=event_addedit"><i class="fa fa-circle-o"></i> Add New Event</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Contacts</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=contact_category_list"><i class="fa fa-circle-o"></i> List Contact Category</a></li>
                <li><a href="index.php?p=contact_category_addedit"><i class="fa fa-circle-o"></i> Add New Contact Category</a></li>
                <li><a href="index.php?p=contact_list"><i class="fa fa-circle-o"></i> List Contacts</a></li>
                <li><a href="index.php?p=contact_addedit"><i class="fa fa-circle-o"></i> Add New Contact</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Resources</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=book_list"><i class="fa fa-circle-o"></i> List Books</a></li>
                <li><a href="index.php?p=book_addedit"><i class="fa fa-circle-o"></i> Add Book</a></li>
                <li><a href="index.php?p=curriculum_category_list"><i class="fa fa-circle-o"></i> List Curriculum Category</a></li>
                <li><a href="index.php?p=curriculum_category_addedit"><i class="fa fa-circle-o"></i> Add Curriculum Category</a></li>
                <li><a href="index.php?p=curriculum_list"><i class="fa fa-circle-o"></i> Curriculums (Junior and  <br> Senior)</a></li>
                <li><a href="index.php?p=curriculum_primary"><i class="fa fa-circle-o"></i> Curriculums (Primary Pal  <br> and Answer)</a></li>
                <li><a href="index.php?p=tract_category_list"><i class="fa fa-circle-o"></i> List Tract Category</a></li>
                <li><a href="index.php?p=tract_category_addedit"><i class="fa fa-circle-o"></i> Add Tract Category</a></li>
                <li><a href="index.php?p=tract_list"><i class="fa fa-circle-o"></i> List Tracts</a></li>
                <li><a href="index.php?p=tract_addedit"><i class="fa fa-circle-o"></i> Add Tract</a></li>
                <li><a href="index.php?p=occation_list"><i class="fa fa-circle-o"></i> List Occasions</a></li>
                <li><a href="index.php?p=occation_addedit"><i class="fa fa-circle-o"></i> Add Occasion</a></li>
                <li><a href="index.php?p=sermon_list"><i class="fa fa-circle-o"></i> List Sermons</a></li>
                <li><a href="index.php?p=sermon_addedit"><i class="fa fa-circle-o"></i> Add Sermon</a></li>
                <!-- <li><a href="index.php?p=video_list"><i class="fa fa-circle-o"></i> List Sermon Video</a></li>
                <li><a href="index.php?p=video_addedit"><i class="fa fa-circle-o"></i> Add New Video</a></li>
                <li><a href="index.php?p=audio_list"><i class="fa fa-circle-o"></i> List Sermon Audios</a></li>
                <li><a href="index.php?p=audio_addedit"><i class="fa fa-circle-o"></i> Add New Audio</a></li>
                <li><a href="index.php?p=photo_category_list"><i class="fa fa-circle-o"></i> List Photo Gallery Category</a></li>
                <li><a href="index.php?p=photo_category_addedit"><i class="fa fa-circle-o"></i> Add Photo category</a></li>
                <li><a href="index.php?p=photo_list"><i class="fa fa-circle-o"></i> List Photo Gallery</a></li>
                <li><a href="index.php?p=photo_addedit"><i class="fa fa-circle-o"></i> Add Photo</a></li> -->
              </ul>
            </li>
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Members</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=member_list"><i class="fa fa-circle-o"></i> List Members</a></li>
                <li><a href="index.php?p=member_addedit"><i class="fa fa-circle-o"></i> Add New Members</a></li>
                <?php if($_SESSION['PlaceID']==0) { ?>
                <li><a href="index.php?p=position_list"><i class="fa fa-circle-o"></i> List Member Postions</a></li>
                <li><a href="index.php?p=position_addedit"><i class="fa fa-circle-o"></i> Add New Position</a></li>
                <?php } ?>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Videos</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=video_list"><i class="fa fa-circle-o"></i> List Webcast Videos</a></li>
                <li><a href="index.php?p=video_addedit"><i class="fa fa-circle-o"></i> Add New Video</a></li>
              </ul>
            </li> -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Gallery</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=news_gallery_list"><i class="fa fa-circle-o"></i> List Image Gallery</a></li>
                <li><a href="index.php?p=news_gallery_addedit"><i class="fa fa-circle-o"></i> Add New Image Gallery</a></li>
                <li><a href="index.php?p=gallery_images_list"><i class="fa fa-circle-o"></i> List Gallery Images</a></li>
                <li><a href="index.php?p=gallery_images_addedit"><i class="fa fa-circle-o"></i> Add Gallery Image</a></li>                
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Others</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=prayer_list"><i class="fa fa-circle-o"></i> Prayer Requests</a></li>
                <li><a href="index.php?p=testi_list"><i class="fa fa-circle-o"></i> Testimonials</a></li>
                <li><a href="index.php?p=feedback_list"><i class="fa fa-circle-o"></i> Feedback</a></li>
                <li><a href="index.php?p=comment_list"><i class="fa fa-circle-o"></i> Comments / Suggestions <br> (Live Video)</a></li>
                <li><a href="index.php?p=sms_list"><i class="fa fa-circle-o"></i> List SMS Subscribers</a></li>
                <li><a href="index.php?p=sms_addedit"><i class="fa fa-circle-o"></i> Add New SMS Subscribers</a></li>
                <li><a href="index.php?p=send_bulk_sms"><i class="fa fa-circle-o"></i> Send Bulk SMS</a></li>
              </ul>
            </li>
            <li><a href="index.php?p=database"><i class="fa fa-database"></i> <span>DB Backup</span> </a></li>
          </ul>
        </section>
      </aside>
       <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <section class="content-header">
            <h1>
             Prayers
            </h1>
          <!--   <ol class="breadcrumb">
              <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
              <li class="active">Here</li>
            </ol>
             -->
          </section>
          <section class="content">
            <div class="row">
              <div class="col-xs-12">
                <div class="box">
                  <div class="box-header"></div>
                  <div class="box-body">
                    <?php
if($_SESSION['AdminID']!=1){
  $plc = FetchValue("admin","place_id","UserID",$_SESSION['AdminID']);
  $where = "where place_id = 0 or place_id = ".$plc;
}else{
  $where = "where 1";
  $plc = 0;
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><span style="font:Arial, Helvetica, sans-serif; font-size:24px; color:#009999">Prayers</span> <span style="font-size:16px; color:#006666; font-weight:bold; padding-left:5px;">(<?php echo date('d/m/Y',strtotime($date));?>)</span> </td>
  </tr>
  <tr style="font:Arial, Helvetica, sans-serif; font-size:14px; color:#333333">
    <td align="right" valign="middle"><span style="float:right"> <img src="images/export1.gif" alt="" onClick="window.location='exportunread.php?SDate=<?php echo $date?>'" onMouseOver="this.style.cursor='pointer'" /> <img src="images/export2.gif" alt="" onClick="window.location='exportread.php?SDate=<?php echo $date?>'" onMouseOver="this.style.cursor='pointer'" /> </span>
      <form action="" method="post" style="padding-top:5px;">
        Enter Date:
        <input name="SDate" title="*" validate="required:true" type="text" style="border:1px solid #CCCCCC" id="SDate" value="<?php echo $RecordSet[0]["start_date"] ?>" readonly="readonly" />
        <input type="submit" value="Go" style="background-color:#009999; border:1px solid #006666; color:#FFFFFF; padding:1px; cursor:pointer" />
        <input type="button" value="Today's Prayers" onClick="window.location='prayers.php'" style="background-color:#009999; border:1px solid #006666; color:#FFFFFF; padding:1px; cursor:pointer" />
        <input type="button" value="Show All" onClick="window.location='prayers.php?p=show'" style="background-color:#009999; border:1px solid #006666; color:#FFFFFF; padding:1px; cursor:pointer" />
        &nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp;
      </form></td>
  </tr>
  <tr>
    <td align="left" valign="top"><div id="Accordion1" class="Accordion" tabindex="0">
        <div class="AccordionPanel">
          <?php 
            if($_REQUEST['p']=='show')
              $sql = "select * from prayer ".$where." and p_read=0 order by p_id";        
            else
              $sql = "select * from prayer ".$where." and p_read=0 and DATE(created) = '".$date."' order by p_id";              
            $objPaging = new paging($sql, "p_id",RECORD_PER_PAGE);
            $objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
            $img = $objDB->select($objPaging->get_query());             
          ?>
          <div class="AccordionPanelTab"><img src="../images/orange_arrow.gif" alt="" hspace="5">Unread Prayers<span style="float:right;"> Page: <?php echo $objPaging->show_paging() ?></span></div>
          <div class="AccordionPanelContent" style="height:400px;"><span style="font-size:14px; padding-bottom:5px; color:#FF0000">Click on chechbox to mark as READ.</span>
            <form name="frmCms" id="frmCms" action="" method="post">
              <table width="100%" border="0" cellspacing="1" cellpadding="5">
                <tr bgcolor="#993300">
                  <td align="left" width="5%" valign="top" style="color:#FFFFFF; font-weight:bold;">Read</td>
                  <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Name</td>
                  <td align="left" valign="top" style="color:#FFFFFF; font-weight:bold;">Prayer</td>
                  <?php if($_SESSION['AdminID']==1){ ?>
                  <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Place</td>
                  <?php } ?>
                </tr>
                <?php if(count($img)>0){
                for($i=0;$i<count($img);$i++){
              ?>
                <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><input type="checkbox" value="<?php echo $img[$i]['p_id'];?>" onClick="Read(this.value,'<?php echo $date?>')" name="Read<?php echo $i?>" id="Read<?php echo $i?>" /></td>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo stripslashes($img[$i]['p_name'])?> <?php if($img[$i]['type']!='') echo "(".stripslashes($img[$i]['type']).")"; ?></td>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo substr(wordwrap(stripslashes($img[$i]['p_prayer']),60,"\n",1),0,250); if(strlen(stripslashes($img[$i]['p_prayer']))>=251){?><br>
                    <a href="#" onClick="window.open('prayer_detail.php?id=<?php echo $img[$i]['p_id']?>','','width=600,height=500,scrollbars=1')">Read More</a>
                    <?php } ?></td>
                  <?php if($_SESSION['AdminID']==1){ ?>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php if($img[$i]['place_id']!=0) echo stripslashes(FetchValue("county","c_name","c_id",$img[$i]['place_id'])); else{ 
                    $pl = FetchData("county",array(),""); 
                    $pl1=array(); 
                    for($p=0;$p<count($pl);$p++) 
                      $pl1[] = str_replace(","," -",stripslashes($pl[$p]['c_name'])); 
                    echo @implode(', ',$pl1);
                    }?></td>
                  <?php } ?>
                </tr>
                <tr>
                  <td height="3"  colspan="4"></td>
                </tr>
                <?php } ?>
                <?php }else{ ?>
                <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                  <td align="left" valign="top" colspan="4">No record found&nbsp;</td>
                </tr>
                <?php } ?>
              </table>
            </form>
          </div>
        </div>
        <div class="AccordionPanel">
          <?php 
          if($_REQUEST['p']=='show')
            $sql = "select * from prayer ".$where." and p_read=1 order by p_id";                
          else
            $sql = "select * from prayer ".$where." and p_read=1 and DATE(created) = '".$date."' order by p_id";  
          $objPaging = new paging($sql, "p_id",RECORD_PER_PAGE);
          $objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
          $img1 = $objDB->select($objPaging->get_query());

        ?>
          <div class="AccordionPanelTab"><img src="../images/orange_arrow.gif" alt="" hspace="5">Readed Prayers<span style="float:right"> Page: <?php echo $objPaging->show_paging() ?></span></div>
          <div class="AccordionPanelContent" style="height:400px;"><span style="font-size:14px; padding-bottom:5px; color:#FF0000">Click on chechbox to mark as UNREAD.</span>
            <form name="frmCms" id="frmCms" action="" method="post">
              <table width="100%" border="0" cellspacing="1" cellpadding="5">
                <tr bgcolor="#669900">
                  <td align="left" width="5%" valign="top" style="color:#FFFFFF; font-weight:bold;">Unread</td>
                  <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Name</td>
                  <td align="left" valign="top" style="color:#FFFFFF; font-weight:bold;">Prayer</td>
                  <?php if($_SESSION['AdminID']==1){ ?>
                  <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Place</td>
                  <?php } ?>
                </tr>
                <?php if(count($img1)>0){
                for($i=0;$i<count($img1);$i++){
              ?>
                <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><input type="checkbox" value="<?php echo $img1[$i]['p_id'];?>" onClick="Unread(this.value,'<?php echo $date?>')" name="Read<?php echo $i?>" id="Read<?php echo $i?>" /></td>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo stripslashes($img1[$i]['p_name'])?></td>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo substr(wordwrap(stripslashes($img1[$i]['p_prayer']),60,"\n",1),0,250); if(strlen(stripslashes($img1[$i]['p_prayer']))>=251){?><br>
                    <a href="#" onClick="window.open('prayer_detail.php?id=<?php echo $img1[$i]['p_id']?>','','width=600,height=500,scrollbars=1')">Read More</a>
                    <?php } ?></td>
                  <?php if($_SESSION['AdminID']==1){ ?>
                  <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php if($img1[$i]['place_id']!=0) echo stripslashes(FetchValue("county","c_name","c_id",$img1[$i]['place_id'])); else{
                   $pl = FetchData("county",array(),""); 
                    $pl1=array(); 
                    for($p=0;$p<count($pl);$p++) 
                      $pl1[] = str_replace(","," -",stripslashes($pl[$p]['c_name'])); 
                    echo @implode(', ',$pl1);
                    }?></td>
                  <?php } ?>
                </tr>
                <tr>
                  <td height="3"  colspan="4"></td>
                </tr>
                <?php } ?>
                <?php }else{ ?>
                <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
                  <td align="left" valign="top" colspan="4">No record found&nbsp;</td>
                </tr>
                <?php } ?>
              </table>
            </form>
          </div>
        </div>
      </div></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
<script type="text/javascript">
<!--
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
//-->
</script>
                  </div><!-- /.box-body -->
                </div><!-- /.box -->
              </div><!-- /.col -->
            </div><!-- /.row -->
          </section>
      </div><!-- /.content-wrapper -->

<?php if(isset($_SESSION['AdminID'])) {  ?>
<footer class="main-footer">
    <div class="pull-left hidden-xs">
      <b>Admin</b>
    </div>
    <strong>Copyright &copy; <?php echo date('Y'); ?> <!-- <a href="<?php echo base_url(); ?>">Almsaeed Studio</a> -->.</strong> All rights reserved.
  </footer>
    
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->  
    <?php } ?>
    
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
    <script type="text/javascript">
    $.metadata.setType("attr", "validate");

    function nonWorkingDates(date){
      var day = date.getDay(), Sunday = 0, Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6;  
      var closedDays = [[Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday]];
      for (var i = 0; i < closedDays.length; i++) {
        if (day == closedDays[i][0]) {
          return [false];
        }

      }
      return [true];
    } 
    </script>
    <script>
    function MM_showHideLayers() { //v9.0
      var i,p,v,obj,args=MM_showHideLayers.arguments;
      for (i=0; i<(args.length-2); i+=3) 
      with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
        if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
        obj.visibility=v; }
    }
    function MM_swapImgRestore() { //v3.0
      var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
    }
    </script>
    
  </body>
</html>

<?php
$objDB->close();
?>
