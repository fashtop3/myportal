<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$sql = "select * from tract_category where title='".addslashes($_REQUEST['Title'])."' and id != ".$_REQUEST['id']." and place_id = ".$_REQUEST['Place'];
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Tract Category Already Exists!';
			header("Location: index.php?p=tract_category_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$SQL = "UPDATE tract_category SET ";
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";		
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Tract Category Updated Successfully!';
		header("Location: index.php?p=tract_category_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$sql = "select * from tract_category where title='".addslashes($_REQUEST['Title'])."' and place_id = ".$_REQUEST['Place'];
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Tract Category Already Exists!';
			header("Location: index.php?p=tract_category_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$SQL = "INSERT tract_category SET ";		
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";	
		$SQL .= 'place_id='.$_REQUEST['Place'].',';	
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Tract Category Added Successfully!';
		header("Location: index.php?p=tract_category_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$sql = "select * from tract where category_id = ".$_REQUEST['ID'];
	$cu = $objDB->select($sql);
	if(count($cu)>0){
		$_SESSION['ErrorMsg'] = 'You can not delete this Tract Category. It is already assigned to few Resource Tracts.';
		header("Location: index.php?p=tract_category_list&pg_no=".$_REQUEST['pg_no']);	
		exit;
	}
	$SQL = "DELETE FROM tract_category ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Tract Category Deleted Successfully!';
	header("Location: index.php?p=tract_category_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$sql = "select * from tract where category_id = ".$_REQUEST['del'][$i];
		$cu = $objDB->select($sql);
		if(count($cu)>0){
			$_SESSION['ErrorMsg'] = 'You can not delete this Tract Category. It is already assigned to few Resource Tracts.';
			header("Location: index.php?p=tract_category_list&pg_no=".$_REQUEST['pg_no']);	
			exit;
		}
		$SQL = "DELETE FROM tract_category ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Tract Category Deleted Successfully!';
	
	header("Location: index.php?p=tract_category_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
