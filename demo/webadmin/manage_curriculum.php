<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;

	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		if($_REQUEST['CategoryId']>=3){
			if(!empty($_FILES['CImage']['name']))
			{  
				if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
				{
						$img_extension1=explode(".",$_FILES['CImage']['name']);			
						
						$image_type = strtoupper(IMAGE_TYPE);
						$type = explode('.',$image_type);
						$type= implode('',$type);
						$type = explode(',',$type);
						$flag = true;
						for($i=0;$i<count($type);$i++)
						{
							if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
								$flag= true;
								break;
							}
							else
								$flag=false;					
						}
						if($flag==false)
						{
							$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
							header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
							exit;
						}
						
						$FileName = $_FILES['CImage']['name'];					
											
						$upload_file = $_FILES['CImage']['tmp_name'];
						$CImage = addslashes('curriculum_'.$_REQUEST['CategoryId']."_".date('ymdHis').".".$img_extension1[count($img_extension1)-1]);//.'_'.$FileName;	
						$CImage = $CImage;
						$path = "../uploads/curriculum/image";//.$_REQUEST['GalleryId'];
						$image = new SimpleImage();
						$image->load($_FILES['CImage']['tmp_name']);
						$image->scale(100);
						$image->save($path."/big/".$CImage);
						$image->scale(45);
						$image->save($path."/small/".$CImage);
						
						$SQL = "SELECT * FROM curriculum ";
						$SQL .= "WHERE id = '".$_REQUEST['id']."'";
						$img = $objDB->sql_query($SQL);
						if($img[0]['image']!=''){
							unlink($path."/big/".stripslashes($img[0]['image']));
							unlink($path."/small/".stripslashes($img[0]['image']));
						}					
				}
				else
				{
					$_SESSION['ErrorMsg'] = "Image Not Uploaded";
					header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
					exit;
				}
			}
			else
			{		
				$CImage = addslashes($_REQUEST['OldImage']);
			}
			
			if(!empty($_FILES['CDown']['name']))
			{  
				if($_FILES['CDown']['error'] == UPLOAD_ERROR_OK )
				{
						$img_extension1=explode(".",$_FILES['CDown']['name']);			
						
						$image_type = strtoupper(FILE_TYPE);
						$type = explode('.',$image_type);
						$type= implode('',$type);
						$type = explode(',',$type);
						$flag = true;
						for($i=0;$i<count($type);$i++)
						{
							if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
								$flag= true;
								break;
							}
							else
								$flag=false;					
						}
						if($flag==false)
						{
							$_SESSION['ErrorMsg'] = "File can not be uploaded...!";
							header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
							exit;
						}
						
						$FileName = $_FILES['CDown']['name'];					
											
						$upload_file = $_FILES['CDown']['tmp_name'];
						$CDown = 'curriculum_'.$_REQUEST['CategoryId']."_".date('ymdHis').".".$img_extension1[count($img_extension1)-1];//.'_'.$FileName;	
						$CDown = addslashes($CDown);
						$path = "../uploads/curriculum/download/";//.$_REQUEST['GalleryId'];
						copy($_FILES['CDown']['tmp_name'], $path.$CDown);
						
						$SQL = "SELECT * FROM curriculum ";
						$SQL .= "WHERE id = '".$_REQUEST['id']."'";
						$img = $objDB->sql_query($SQL);
						if($img[0]['file']!=''){
							unlink($path.stripslashes($img[0]['file']));
						}					
				}
				else
				{
					$_SESSION['ErrorMsg'] = "File Not Uploaded";
					header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
					exit;
				}
			}
			else
			{		
				$CDown = addslashes($_REQUEST['OldDown']);
			}
			
			if(!empty($_FILES['CVideo']['name']))
			{  
				if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
				{
						$img_extension1=explode(".",$_FILES['CVideo']['name']);			
						$flag = true;
							if(strtoupper($img_extension1[count($img_extension1)-1])=='PDF')		{
								$flag= true;
							}
							else
								$flag=false;					
						if($flag==false)
						{
							$_SESSION['ErrorMsg'] = "PDF File can not be uploaded...!";
							header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
							exit;
						}
						
						$FileName = $_FILES['CVideo']['name'];					
											
						$upload_file = $_FILES['CVideo']['tmp_name'];
						$CVideo = 'curriculum_'.$_REQUEST['CategoryId']."_".date('ymdHis').".".$img_extension1[count($img_extension1)-1];//.'_'.$FileName;	
						$CVideo = addslashes($CVideo);
						$path = "../uploads/curriculum/video/";//.$_REQUEST['GalleryId'];
						copy($_FILES['CVideo']['tmp_name'], $path.$CVideo);
						$SQL = "SELECT * FROM curriculum ";
						$SQL .= "WHERE id = '".$_REQUEST['id']."'";
						$img = $objDB->sql_query($SQL);
						if($img[0]['video']!=''){
							unlink($path.stripslashes($img[0]['video']));
						}					
				}
				else
				{
					$_SESSION['ErrorMsg'] = "PDF File Not Uploaded";
					header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
					exit;
				}
			}
			else
			{
				$CVideo = addslashes($_REQUEST['OldVideo']);
			}
			
			$SQL = "UPDATE curriculum SET ";
			$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
			$SQL .= 'bible_author="'.addslashes($_REQUEST['Author']).'",';
			$SQL .= 'image="'.$CImage.'",';
			$SQL .= 'file="'.$CDown.'",';
			$SQL .= 'video="'.$CVideo.'",';
			$SQL .= "lesson_no='".addslashes($_REQUEST['LessonNo'])."',";
			$SQL .= "book_id='".addslashes($_REQUEST['BookNo'])."',";
			$SQL .= "cdate='".addslashes($_REQUEST['CDate'])."',";
			$SQL .= 'memory_verse="'.addslashes($_REQUEST['MemoryVerse']).'",';
			$SQL .= "category_id='".$_REQUEST['CategoryId']."',";
			$SQL .= 'short_description="'.addslashes($_REQUEST['FullTitle']).'",';
			$SQL .= 'full_description="'.addslashes($_REQUEST['Content']).'",';
			$SQL .= 'bible_description="'.addslashes($_REQUEST['Description']).'",';
			$SQL .= "modified='".date('Y-m-d H:i:s')."',";
			$SQL .= 'place_id='.$_REQUEST['Place'].',';
			$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
			$SQL .= " WHERE id=".$_REQUEST['id'];
			$objDB->sql_query($SQL);
			$_SESSION['SuccessMsg'] = 'Curriculum Updated Successfully!';
			header("Location: index.php?p=curriculum_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}else{
			$SQL = "UPDATE curriculum SET ";
			$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';			
			$SQL .= "cdate='".addslashes($_REQUEST['CDate'])."',";
			$SQL .= "category_id='".$_REQUEST['CategoryId']."',";
			$SQL .= 'full_description="'.addslashes($_REQUEST['Content']).'",';
			$SQL .= "modified='".date('Y-m-d H:i:s')."',";
			$SQL .= 'place_id='.$_REQUEST['Place'].',';
			$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
			$SQL .= " WHERE id=".$_REQUEST['id'];
			$objDB->sql_query($SQL);
			$_SESSION['SuccessMsg'] = 'Curriculum Updated Successfully!';
			header("Location: index.php?p=curriculum_primary&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		
		if($_REQUEST['CategoryId']>=3){
			if(!empty($_FILES['CImage']['name']))
			{  
				if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
				{
						$img_extension1=explode(".",$_FILES['CImage']['name']);			
						
						$image_type = strtoupper(IMAGE_TYPE);
						$type = explode('.',$image_type);
						$type= implode('',$type);
						$type = explode(',',$type);
						$flag = true;
						for($i=0;$i<count($type);$i++)
						{
							if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
								$flag= true;
								break;
							}
							else
								$flag=false;					
						}
						if($flag==false)
						{
							$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
							header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
							exit;
						}
						
						$FileName = $_FILES['CImage']['name'];					
											
						$upload_file = $_FILES['CImage']['tmp_name'];
						$CImage = 'curriculum_'.$_REQUEST['CategoryId']."_".date('ymdHis').".".$img_extension1[count($img_extension1)-1];//.'_'.$FileName;
						$CImage = addslashes($CImage);						
						$path = "../uploads/curriculum/image";//.$_REQUEST['GalleryId'];
						$image = new SimpleImage();
						$image->load($_FILES['CImage']['tmp_name']);
						$image->scale(100);
						$image->save($path."/big/".$CImage);
						$image->scale(45);
						$image->save($path."/small/".$CImage);									
				}
				else
				{
					$_SESSION['ErrorMsg'] = "Image Not Uploaded";
					header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
					exit;
				}
			}
			else
			{
				$CImage = "";
			}
			
			if(!empty($_FILES['CDown']['name']))
			{  
				if($_FILES['CDown']['error'] == UPLOAD_ERROR_OK )
				{
						$img_extension1=explode(".",$_FILES['CDown']['name']);			
						
						$image_type = strtoupper(FILE_TYPE);
						$type = explode('.',$image_type);
						$type= implode('',$type);
						$type = explode(',',$type);
						$flag = true;
						for($i=0;$i<count($type);$i++)
						{
							if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
								$flag= true;
								break;
							}
							else
								$flag=false;					
						}
						if($flag==false)
						{
							$_SESSION['ErrorMsg'] = "File can not be uploaded...!";
							header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
							exit;
						}
						
						$FileName = $_FILES['CDown']['name'];					
											
						$upload_file = $_FILES['CDown']['tmp_name'];
						$CDown = 'curriculum_'.$_REQUEST['CategoryId']."_".date('ymdHis').".".$img_extension1[count($img_extension1)-1];//.'_'.$FileName;	
						$CDown = addslashes($CDown);	
						$path = "../uploads/curriculum/download/";//.$_REQUEST['GalleryId'];
						copy($_FILES['CDown']['tmp_name'], $path.$CDown);															
				}
				else
				{
					$_SESSION['ErrorMsg'] = "File Not Uploaded";
					header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
					exit;
				}
			}
			else
			{
				$CDown = "";
			}
			
			if(!empty($_FILES['CVideo']['name']))
			{  
				if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
				{
						$img_extension1=explode(".",$_FILES['CVideo']['name']);			
						$flag = true;
							if(strtoupper($img_extension1[count($img_extension1)-1])=='PDF')		{
								$flag= true;
							}
							else
								$flag=false;					
						if($flag==false)
						{
							$_SESSION['ErrorMsg'] = "PDF File can not be uploaded...!";
							header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
							exit;
						}
						
						$FileName = $_FILES['CVideo']['name'];					
											
						$upload_file = $_FILES['CVideo']['tmp_name'];
						$CVideo = 'curriculum_'.$_REQUEST['CategoryId']."_".date('ymdHis').".".$img_extension1[count($img_extension1)-1];//.'_'.$FileName;	
						$CVideo = addslashes($CVideo);	
						$path = "../uploads/curriculum/video/";//.$_REQUEST['GalleryId'];
						copy($_FILES['CVideo']['tmp_name'], $path.$CVideo);									
				}
				else
				{
					$_SESSION['ErrorMsg'] = "PDF File Not Uploaded";
					header("Location: index.php?p=curriculum_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
					exit;
				}
			}
			else
			{
				$CVideo = "";
			}
			
			$SQL = "INSERT curriculum SET ";
			$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
			$SQL .= 'image="'.$CImage.'",';
			$SQL .= 'file="'.$CDown.'",';
			$SQL .= 'video="'.$CVideo.'",';
			$SQL .= "lesson_no='".addslashes($_REQUEST['LessonNo'])."',";
			$SQL .= "book_id='".addslashes($_REQUEST['BookNo'])."',";
			$SQL .= "cdate='".addslashes($_REQUEST['CDate'])."',";
			$SQL .= 'memory_verse="'.addslashes($_REQUEST['MemoryVerse']).'",';
			$SQL .= "category_id='".$_REQUEST['CategoryId']."',";
			$SQL .= 'short_description="'.addslashes($_REQUEST['FullTitle']).'",';
			$SQL .= 'full_description="'.addslashes($_REQUEST['Content']).'",';
			$SQL .= 'bible_description="'.addslashes($_REQUEST['Description']).'",';
			$SQL .= 'bible_author="'.addslashes($_REQUEST['Author']).'",';
			$SQL .= 'place_id='.$_REQUEST['Place'].',';
			$SQL .= "created='".date('Y-m-d H:i:s')."',";
			$SQL .= "createdby='".$_SESSION['AdminID']."'";
	
			//echo $SQL;exit;
			$objDB->sql_query($SQL);
			$_SESSION['SuccessMsg'] = 'Curriculum Added Successfully!';
			header("Location: index.php?p=curriculum_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}else{
			$SQL = "INSERT curriculum SET ";
			$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';			
			$SQL .= "cdate='".addslashes($_REQUEST['CDate'])."',";
			$SQL .= "category_id='".$_REQUEST['CategoryId']."',";
			$SQL .= 'full_description="'.addslashes($_REQUEST['Content']).'",';			
			$SQL .= 'place_id='.$_REQUEST['Place'].',';
			$SQL .= "created='".date('Y-m-d H:i:s')."',";
			$SQL .= "createdby='".$_SESSION['AdminID']."'";
			$objDB->sql_query($SQL);
			$_SESSION['SuccessMsg'] = 'Curriculum Added Successfully!';
			header("Location: index.php?p=curriculum_primary&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
//	unlink()
	$SQL = "SELECT * FROM curriculum ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);
	$path = '../uploads/curriculum';
	
	if($img[0]['category_id']>=3){
		if($img[0]['image']!=''){
			unlink($path."/image/big/".stripslashes($img[0]['image']));
			unlink($path."/image/small/".stripslashes($img[0]['image']));
		}
		if($img[0]['file']!=''){
			unlink($path."/download/".stripslashes($img[0]['file']));
		}
		if($img[0]['video']!=''){
			unlink($path."/video/".stripslashes($img[0]['video']));
		}
		$SQL = "DELETE FROM curriculum ";
		$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Curriculum Deleted Successfully!';
		header("Location: index.php?p=curriculum_list&pg_no=".$_REQUEST['pg_no']);	
		exit;
	}else{
		$SQL = "DELETE FROM curriculum ";
		$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Curriculum Deleted Successfully!';
		header("Location: index.php?p=curriculum_primary&pg_no=".$_REQUEST['pg_no']);	
		exit;
	}
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	$flag = 0;
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "SELECT * FROM curriculum ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$img = $objDB->sql_query($SQL);
		$path = '../uploads/curriculum';
		if($img[0]['category_id']>=3){
			$flag = 1;
		}				
		if($img[0]['image']!=''){
			unlink($path."/image/big/".stripslashes($img[0]['image']));
			unlink($path."/image/small/".stripslashes($img[0]['image']));
		}
		if($img[0]['file']!=''){
			unlink($path."/download/".stripslashes($img[0]['file']));
		}
		if($img[0]['video']!=''){
			unlink($path."/video/".stripslashes($img[0]['video']));
		}
		$SQL = "DELETE FROM curriculum ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Curriculum Deleted Successfully!';
	
	if($flag==1)
		header("Location: index.php?p=curriculum_list&pg_no=".$_REQUEST['pg_no']);
	else
		header("Location: index.php?p=curriculum_primary&pg_no=".$_REQUEST['pg_no']);
	exit;
}
?>
