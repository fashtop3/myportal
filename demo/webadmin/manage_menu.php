<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$SQL = "UPDATE menu SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= 'title_link="'.addslashes($_REQUEST['Links']).'",';
		$SQL .= "parent_id='".addslashes($_REQUEST['Parent'])."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	  
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Menu Updated Successfully!';
		header("Location: index.php?p=menu_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$SQL = "INSERT menu SET ";		
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= 'title_link="'.addslashes($_REQUEST['Links']).'",';
		$SQL .= "parent_id='".addslashes($_REQUEST['Parent'])."'";

		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Menu Added Successfully!';
		header("Location: index.php?p=menu_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
?>