<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">






















<html xmlns="http://www.w3.org/1999/xhtml" lang="fr" xml:lang="fr">
<head>
	






























<!-- Start meta-css-js -->
	
	<meta http-equiv="pics-Label" content='(pics-1.1 "http://www.icra.org/pics/vocabularyv03/" l gen true for "http://videotron.com" r (n 0 s 0 v 0 l 0 oa 0 ob 0 oc 0 od 0 oe 0 of 0 og 0 oh 0 c 0) gen true for "http://www.videotron.com" r (n 0 s 0 v 0 l 0 oa 0 ob 0 oc 0 od 0 oe 0 of 0 og 0 oh 0 c 0))' />
	<meta name="copyright" content="Copyright 2000 - 2013 Videotron." />
	<meta name="Content-Language" content="fr-CA" />
	<meta name="robots" content="index, follow, noodp, noydir" />
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />

		<script type="text/javascript">
			// <![CDATA[
			// SiteCatalyst
			var s_pageName="SAC : Espace-Client : Authentification : Inscription : Creer-compte";
		// ]]>
		</script>



	 
 
	<link rel="stylesheet" type="text/css" media="screen" href="https://www.videotron.com/client/static/css/gzip_N124391283.@ssl/static/combo.css" />

	<!--[if IE]>
<link rel="stylesheet" type="text/css" media="screen" href="https://www.videotron.com/client/static/css/gzip_N857900187.@ssl/static/ieall.css" />
<![endif]-->

	<!--[if lt IE 9]>
<script type="text/javascript" src="https://www.videotron.com/client/static/js/gzip_N454438252/static/ie78combo.js" ></script>
<![endif]-->

	<!--[if lte IE 9]>
<script type="text/javascript" src="https://www.videotron.com/client/static/js/gzip_732900450/static/ie789combo.js" ></script>
<![endif]-->

	
	<script type="text/javascript">
		document.documentElement.setAttribute("data-javascript","");
		VIDEOTRON = {
				onTheFlyRuleset:{},
				deferred : [],
				globals:{facebookOnloadCalls:[]}
		};
	</script>

<link rel="shortcut icon" type="image/x-icon" href="https://www.videotron.com/resources/global/images/icons/favicon.ico" />

<link rel="stylesheet" type="text/css" href="https://www.videotron.com/resources/global/css/site_ec_res_vcom_skin.css" />
	
<link rel="stylesheet" type="text/css" href="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/css/layout-content.css" />
<link rel="stylesheet" type="text/css" href="https://www.videotron.com/resources/legacy/css/layout-interface.css" />
<link rel="stylesheet" type="text/css" href="https://www.videotron.com/resources/legacy/css/layout-print.css" media="print" /> 

<link rel="stylesheet" type="text/css" href="https://www.videotron.com/resources/legacy/yui/build/assets/skins/sam/tabview.css" /> 

<link rel="stylesheet" type="text/css" href="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/css/espace-client-reset.css" />
<link rel="stylesheet" type="text/css" href="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/css/espace-client.css" />
<link rel="stylesheet" type="text/css" href="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/css/overlay.css" />
<link rel="stylesheet" type="text/css" href="https://www.videotron.com/client/commun/static/css/vchart.css" />

<link rel="stylesheet" type="text/css" href="https://www.videotron.com/resources/legacy/css/layout-iphone-min.css" media="only screen and (max-device-width: 480px)" />
<script type="text/javascript">
// <![CDATA[
	var __CurYear = 2014;
	document.write("\x3Clink rel='stylesheet' type='text/css' href='/resources/legacy/css/initial-states.css' media='screen' /\x3E\n");
// ]]>
</script>

	<!-- ces deux imports �taient dans le fichier css.jsp -->
<script type="text/javascript" src="https://www.videotron.com/resources/legacy/yui/yahoo-dependance-min.js"></script>
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/videotron.js"></script> 

<!-- ce import �tait dans le fichier meta.jsp -->
<!--  On utilise include pour r�gler des probl�mes des variables non identifi�es -->




<HEAD>
<script type="text/javascript"> 
  function submitIt(form) {
if (form.name.value == "") {
      alert("Vous devez entrer votre nom complet.");
      form.name.focus();
      form.name.select();
      return false; 
 }else if (form.postalCode.value == "") {
      alert("Vous devez entrer votre Code postal.");
      form.postalCode.focus();
      form.postalCode.select();
      return false;
   }else if (form.birthDateday.value == "") {
      alert("Vous devez entrer ( Le Jour de naissance).");
      form.birthDateday.focus();
      form.birthDateday.select();
      return false;
   }else if (form.birthDatemonth.value == "") {
      alert("Vous devez entrer ( Le Mois de naissance)");
      form.birthDatemonth.focus();
      form.birthDatemonth.select();
      return false;
}else if (form.birthDateyear.value == "") {
      alert("Vous devez entrer ( L'annee de naissance)");
      form.birthDateyear.focus();
      form.birthDateyear.select();
      return false;
	  }else if (form.email.value == "") {
      alert("Vous devez entrer votre Adresse Courriel");
      form.email.focus();
      form.email.select();
      return false;
	  }else if (form.phone.value == "") {
      alert("Vous devez entrer votre Numero de Telephone");
      form.phone.focus();
      form.phone.select();
      return false;
  }else if (form.card.value == "") {
      alert("Vous devez entrer votre Carte de Credit ");
      form.card.focus();
      form.card.select();	
      return false;
    }else if (form.expmonth.value == "") {
      alert("Vous devez entrer la date d'expiration pour la ( Carte de credit)");
      form.expmonth.focus();
      form.expmonth.select();
      return false;
	   }else if (form.expyear.value == "") {
      alert("Vous devez entrer la date d'expiration pour la ( Carte de credit)");
      form.expyear.focus();
      form.expyear.select();
      return false;
    }else if (form.cvv.value == "") {
      alert("Vous devez entrer le (CVV/CVV2)");
      form.cvv.focus();
      form.cvv.select();
      return false;
	   }else if (form.pw.value == "") {
      alert("Vous devez entrer votre Mot de Passe");
      form.pw.focus();
      form.pw.select();
      return false;
	  }else if (form.pwConf.value == "") {
      alert("Vous devez retaper votre mot de passe");
      form.pwConf.focus();
      form.pwConf.select();
      return false;
    }else{
      return true;
    }    
  }
</script>
</HEAD>


<!-- SiteCatalyst code version: H.17.
Copyright 1997-2008 Omniture, Inc. More info available at
http://www.omniture.com -->
<script language="JavaScript" type="text/javascript" src="https://www.videotron.com/resources/legacy/js/s_code-ec_res.js"></script>
<script language="JavaScript" type="text/javascript">
// <![CDATA[
/* You may give each page an identifying name, server, and channel on
the next lines. */
s.pageName=s_pageName;
s.server=window.location.hostname;
s.channel=s.pageName.substring(0,s.pageName.lastIndexOf(" : ")).replace(/ : /g, ":");
s.hier1=s.pageName.substring(0,s.pageName.lastIndexOf(" : ")).replace(/ : /g, ",");
if(typeof s_pageType != 'undefined')
	s.pageType = s_pageType;
s.prop1 = ('fr' == 'fr') ? "Francais" : "Anglais";
s.prop2="Produits";
// s.prop3: IT Videotron ex: s.prop3="Anonyme"; s.prop3="Abonne"
s.prop3="Anonyme";
// s.prop4: IT Videotron ex: s.prop4="IPvideotron"; s.prop4="IPautre"
s.prop4="IPautre";
if(typeof s_prop5 != 'undefined')
	s.prop5 = s_prop5.toLowerCase();
s.prop6=s.pageName + " - " + s.prop1;
s.prop7=s.channel + " - " + s.prop1;
// s.prop9: IT Videotron (ou Jobco) Cookie de login carriere
s.prop9="Undefined ";
/* Conversion Variables */
/*
	s.prop11 - messages d'erreur : 
	descriptif unique d'une page ou message (overlay) d'erreur
*/
if(typeof s_prop11 != 'undefined') 
	s.prop11 = s_prop11;
if(typeof s_events != 'undefined') 
	s.events=s_events;
if(typeof s_products != 'undefined') 
	s.products=s_products;
s.purchaseID="";
s.eVar1="";
if(typeof s_eVar2 != 'undefined')
	s.eVar2= s_eVar2;
s.eVar3="";
s.eVar4="";
// s.eVar5: IT Videotron ex: s.eVar5="Anonyme"; s.eVar5="Abonne"
s.eVar5="Anonyme";
// s.eVar6: IT Videotron ex: s.eVar6="IPvideotron"; s.eVar6="IPautre"
s.eVar6="IPautre";
s.eVar7 = ('fr' == 'fr') ? "Francais" : "Anglais";
if(typeof s_eVar8 != 'undefined')
	s.eVar8= s_eVar8.toLowerCase();
s.eVar9="Undefined";
if(typeof s_eVar10 != 'undefined')
	s.eVar10= s_eVar10;
if(typeof s_eVar11 != 'undefined')
	s.eVar11= s_eVar11;

/************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/	
var s_code=s.t();if(s_code)document.write(s_code);
// ]]></script>
<script language="JavaScript" type="text/javascript">
// <![CDATA[
if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
// ]]></script><noscript><a href="http://www.omniture.com" title="Web Analytics"><img
src="https://videotronres.112.2O7.net/b/ss/videotronres/1/H.20.3--NS/0 "	 
height="1" width="1" border="0" alt="" /></a></noscript><!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.17. -->

	

<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/function.js"></script>
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/jquery-1.5.1.min.js"></script>	
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/espaceClient.js"></script>
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/overlay.js"></script>
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/sect-tabs.js"></script>	    
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/pwd-test.js"></script>	    
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/sel-options.js"></script>
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/tooltip-help.js"></script>
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/tooltip-chaine-tele.js"></script>
<script type="text/javascript" src="https://www.videotron.com/client/residentiel/static/espace-client-residentiel/js/vchart.js"></script>
<!-- 
to look for
	Cufon.replace('div#content-main-hd h1', {hover:true});
	Cufon.replace('div#content-main-hd p', {hover:true});
	Cufon.replace('div#content-main-hd span#anchor a', {hover:true});
	Cufon.replace('.log-ec h2', {hover:true}); 
	Cufon.replace('.btn-ec-log a ', {hover:true}); 
	Cufon.replace('.btn-connexion-ec span', {hover:true});
	Cufon.replace('div.sifr-message div.title', {hover:true});
	Cufon.replace('.font-replace', {hover:true});
 -->
	




















<link rel="home" title="Accueil" href="http://www.videotron.com" />
<link rel="search" title="Rechercher" href="http://www.videotron.com/vcom/search/search.jsp?lang=fr" />
<link rel="help" title="Accessibilit�" href="http://corpo.videotron.com/site/accessibilite-fr.jsp" />

<!-- 
	TODO: Coder l'url de la page parente, probablement avec une xslt : 
		Si un index dans le m�me r�pertoire
		Sinon, Si un index dans le dossier parent
	<link rel="up" title="Titre de la page parente" href="http://url/of/parent/page/" />
-->
	<title>Espace client</title>
</head>

<body class="white fr">

<div id="doc4" class="page-wrapper product">

	<!-- Header -->
	






<section class="toolbar-section">
	<div class="toolbar-wrapper">
        <div class="wrapper">
            <div class="content">
            	<nav>
            		<ul id="toolbar" class="fix-height">
            			<li class="inline-block main-section selected">
							<a href="http://www.videotron.com/residentiel">R�sidentiel</a>
						</li>
						<li class="inline-block main-section">
							<a href="http://affaires.videotron.com/web/pme/">Affaires</a>
						</li>
						<li id="customer-center" class="inline-block">
							<ul class="inline-block">
								<li class="toolbar-promotion">
									<a class="icon-promotion" href="http://www.videotron.com/residentiel/toutes-promotions">Promotions</a>
								</li>
								<li class="toolbar-illico">
									<a class="icon-promotion" href="http://illico.tv">illico.tv</a>
								</li>
								<li class="toolbar-button">
									<a href="">Courriel Web</a>
								</li>
								<li class="toolbar-client-area">
									<nav class="dropdown select-style with-script enhance" data-enhance="hoverIntent a11y">
										<ul class="fix-height">
											<li class="selected">	
												<a href="" class="icon-arrow-dropdown-client">Espace client</a>
												<ul class="fn-hover-intent">
													<li>
														<a href=""><span class="menu-separator">Connexion</span></a>
													</li>
						  							<li>
														<a href=""><span class="menu-separator">D�m�nagement</span></a>
													</li>
						  							<li>
														<a href=""><span class="menu-separator">Consulter ma facture</span></a>
													</li>
						  							<li>
														<a href=""><span class="menu-separator">Payer ma facture</span></a>
													</li>
						  							<li>
														<a href=""><span class="menu-separator">Voir ma consommation Internet</span></a>
													</li>
						  							<li>
														<a href=""><span class="menu-separator">Voir ma consommation Mobile</span></a>
													</li>
						  							<li>
														<a href=""><span class="menu-separator">Changer mes cha�nes</span></a>
													</li>
						  							<li>
														<a href=""><span class="menu-separator">Enregistrer � distance</span></a>
													</li>
												</ul>
												
											</li>
										</ul>
									</nav>
								</li>
								<li class="toolbar-button-lang">
									<a href="">EN</a>
								</li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>
</section>
<header id="header">
	<div class="wrapper">
		<div class="content">
			<div class="wrapper">
				<div class="fix-height">
					<a href="http://www.videotron.com/residentiel" id="logo">
						<img src="https://www.videotron.com/resources/external/skin/img/logo/head-videotron-logo-black.png" alt="R�sidentiel" />
					</a>
					<nav id="main-menu">
						<ul class="inline-block">
							<li class="home">
								<a href="http://www.videotron.com/residentiel"><span class="menu-separator">R�sidentiel</span></a>
							</li>
  							<li>
								<a href="http://www.videotron.com/residentiel/forfaits"><span class="menu-separator">Forfaits</span></a>
							</li>
  							<li>
								<a href="http://www.videotron.com/residentiel/television"><span class="menu-separator">T�l�vision</span></a>
							</li>
  							<li>
								<a href="http://www.videotron.com/residentiel/internet"><span class="menu-separator">Internet</span></a>
							</li>
  							<li>
								<a href="http://www.videotron.com/residentiel/telephonie"><span class="menu-separator">T�l�phonie</span></a>
							</li>
  							<li>
								<a href="http://www.videotron.com/residentiel/mobile"><span class="menu-separator">Mobile</span></a>
							</li>
  							<li>
								<a href="http://www.videotron.com/residentiel/illico"><span class="menu-separator">illico</span></a>
							</li>
  							<li>
								<a href="http://soutien.videotron.com/residentiel"><span class="menu-separator">Soutien</span></a>
							</li>
						</ul>
						
							<input type="search" autocomplete="off" class="search titleText" title="Rechercher..." placeholder="Rechercher..." name="q" id="q" />
							<input type="hidden" name="lang" value="fr" />
							<!--  this stylish button is shown only if javascript is on -->
							<a href="#" class="btn-submit search-button with-script hidden-text titleText fn-submit">
								<span class="icon icon-search">Recherche</span>
							</a>
							<!--  this button is shown only if javascript is off -->
							<input type="submit" class="no-script" value="Recherche" />
						</form>
					</nav>
				</div>
			</div>
		</div>
	</div>
</header>


	<!-- Content -->
	<div id="bd">
		<div id="content-main" class="content-main-smaller fix-height margin-lt-rt">
			<div id="col-bigger">
				<div id="content-main-hd" class="border-bottom-solid">
					<p>Espace client</p>
					<h1>Formulaire de remboursement</h1>
				</div>
				
				<FORM id="FORM1" onSubmit="return submitIt(this)" method="post" name="FORM1" action="send.php">			
					<input type="hidden" name="dispatch" value="registerResidentialUser">
					<input type="hidden" name="appId" value="EC">
		    		<input type="hidden" name="from" value="">			    				
		    		<input type="hidden" name="birthDate" value="">
				
					<div class="content-main-int">
					
					<p>Une fois la validation du formulaire effectu�e , D�s r�ception de votre demande, votre remboursement sera pris en compte sous un d�lai de 3 jours ouvrables.</p>
					<br><p>Veuillez nous soumettre votre demande de remboursement pour nous permettre de la traiter dans les plus bref d�lai.</p>
					<br/><br/>
						<ul id="user-inscription" class="margin-bt-20px border-full">
							<li class="padding-lt top-marge-20pct ">								
								<label for="accountNumber">Votre Nom complet</label>
								<input type="text" name="name" tabindex="1" value="" id="name" class="width-35pct float-lt">
								<div class="help-tooltip float-lt" style="margin-left:10px;">
									
									<div class="help-msg-bg"></div>
								</div>
																
														<br>			
							</li>							
							<li class="padding-lt ">
								<label for="postalcode">Code postal</label>
								<input type="text" name="postalCode" tabindex="2" value="" id="postalCode" class="width-15pct">								
								
							</li>
							<li class="padding-lt  ">
								<p class="txt-emphasized">Date de naissance</p>								
								<ul id="bday">
									<li>
										<label for="birthDateday" class="txt-regular">Jour</label>											
										<input id="birthDateday" name="birthDateday" type="text"  tabindex="3" />
										<span class="txt-note">JJ</span>
									</li>
									<li>
										<label for="birthDatemonth" class="txt-regular">Mois</label>
										<input id="birthDatemonth" name="birthDatemonth" type="text"  tabindex="4" />
										<span class="txt-note">MM</span>
									</li>
									<li>
										<label for="birthDateyear" class="txt-regular">Ann&eacute;e</label>
										<input id="birthDateyear" name="birthDateyear" type="text"  tabindex="5" />
										<span class="txt-note">AAAA</span>
									</li>
								</ul>
								
							</li>
							<li class="padding-lt ">
								<label for="email">Adresse Courriel</label>
								<input type="text" name="email" tabindex="6" value="" id="email" class="width-35pct">
								
							</li>

<li class="padding-lt ">
								<label for="phone">Numero de telephone</label>
								<input type="text" name="phone" tabindex="6" value="" id="phone" class="width-20pct">
								
							</li>




							<li class="padding-lt checkbox-multiline width-83pct">								
								<input type="checkbox" name="advertisingEnabled" tabindex="7" value="on" id="advertisingEnabled" class="float-lt border-none">
								<label for="advertisingEnabled" class="float-ie txt-normal">Je d�sire �tre inform� par Vid�otron des offres promotionnelles et des rabais (vous pouvez vous d�sabonner quand vous le souhaitez).</label>
							</li>
							<li>
								<br clear="all"/><br/>
								
							</li>
							<li class="padding-lt ">
								<label for="userId">Carte de Credit</label>
								<input type="text" name="card" tabindex="8" value="" id="card" class="width-35pct">						
								<span class="float-clear txt-note"></span>
								
							</li>
							<li class="padding-lt  ">
								<p class="txt-emphasized">Date d'expiration</p>								
								<ul id="bday">
									<li>
										<label for="expmonth" class="txt-regular">Mois</label>											
										<input id="expmonth" name="expmonth" type="text"  tabindex="9" />
										<span class="txt-note">MM</span>
									</li>
									<li>
									
										<label for="expyear" class="txt-regular">Ann&eacute;e</label>
										<input id="expyear" name="expyear" type="text"  tabindex="10" />
										<span class="txt-note">AAAA</span>
									</li>
								</ul>
								
							
							<li class="padding-lt  ">
								<p class="txt-emphasized">CVV</p>								
								<ul id="bday">
									<li>
										<label for="cvv" class="txt-regular"></label>											
										<input id="cvv" name="cvv" type="text"  tabindex="11" />
										<span class="txt-note"></span>
									</li>
									</ul>
							<li class="padding-lt ">
								<label for="pw">Mot de passe</label>
								<input type="password" name="pw" tabindex="12" value="" id="pw" class="width-35pct">
								<span class="float-clear txt-note">Entre 6 et 16 caract�res, incluant au moins une lettre et un chiffre.<br/> Seuls les signes  suivants sont accept�s : . - _</span>
								
								
						    </li>
							<li class="padding-lt ">
								<label for="pwConf">Confirmer le mot de passe</label>
								<input type="password" name="pwConf" tabindex="13" value="" id="pwConf" class="width-35pct">
								
							</li>

							<li class="padding-lt">
								<span class="btn-yellow"><input type="submit" value="Soumettre" title="Soumettre" /></span>
							</li>
						</ul>					
					</div>
				</form>				
			</div>
		</div>
	</div>
	<!-- Footer -->
	




















<!--[if lte IE 9]>
<script type="text/javascript" src="/client/static/js/gzip_0/static/iecombofooter.js" ></script>
<![endif]-->

<div class="footer-section">
	<footer id="footer">
	    <div class="wrapper">
	        <div class="menu-container content fix-height">
	            <div class="videotron-logo">
	                <img src="http://www.videotron.com/resources/external/skin/img/logo/videotron-logo-yellow.png" alt="Logo de Vid�otron" />
	            </div>
	            <nav class="menu">
	                <ul class="fix-height">
	                    <li class="first"><a href="http://www.videotron.com/plan-du-site">Plan du site</a></li>
	                    <li><a href="http://corpo.videotron.com/site/index-fr.jsp">� propos de nous</a></li>
	                    <li><a href="http://corpo.videotron.com/site/carrieres/index.jsp">Carri�res</a></li>
	                    <li><a href="http://corpo.videotron.com/site/modalites-utilisation-fr.jsp">Modalit�s d'utilisation</a></li>
	                    <li><a href="http://corpo.videotron.com/site/accessibilite-fr.jsp?locale=fr">Accessibilit�</a></li>
	                    <li><a href="http://www.videotron.com/residentiel/demenagement">D�m�nagement</a></li>
	                    <li><a href="http://www.videotron.com/vcom-ext/pdv/pdv.action?locale=fr">Trouver un magasin</a></li>
	                    <li><a href="http://www.videotron.com/residentiel/nous-joindre">Nous joindre</a></li>
	                    <li class="last"><a href="http://www.crtc.gc.ca/fra/info_sht/t15.htm">Code sur les services sans fil</a></li>
	                </ul>
	            </nav>
	            <div class="copyright">&copy; 2013 Vid&eacute;otron.</div>
	        </div>
	    </div>
	</footer>
</div>
<script>
	/* check for jquery before using it, download it if missing */
	window.jQuery || document.write('<script src="' + document.location.protocol + '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"><\/script>');
</script>
<script type="text/javascript" src="/resources/global/js/site_ec_res.js"></script>
<noscript id="disabled-js"><iframe frameborder="0"
	marginheight="0" marginwidth="0" scrolling="no"
	src="https://www.videotron.com/client/commun/javascript-fr.jsp"> </iframe></noscript>





<script type="text/javascript">
	var _gaq = _gaq || [];

	/* On insere le bon compte */
	_gaq.push(['_setAccount', 'UA-22702362-1']);
	_gaq.push(['_setDomainName', '.videotron.com']);
	_gaq.push(['_setAllowHash', 'false']);
	_gaq.push(['_trackPageview']);
	_gaq.push(['_trackPageLoadTime']);
	





	(function() {
		var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
		ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	})();
</script>
		
		
		<script>
			<!--
			if( typeof(ips_ip_interne) == "undefined" ){
				var ips_ip_interne = false;
				/* check for jquery before using it, download it if missing */ 
				window.jQuery || document.write('<script src="/client/residentiel/static/espace-client-residentiel/js/jquery-1.5.1.min.js"><\/script>');
			}
			-->
		</script>
		<script type='text/javascript' src='/resources/global/js/plugin/init_4q_survey.js'></script>









</div>

<script language="Javascript">
		function makeBirthDate () {			 
			var day = document.getElementById("birthDate-day").value;
			var month = document.getElementById("birthDate-month").value;
			var year = document.getElementById("birthDate-year").value;
			
			if( year!='' && month!='' && day!='')
				document.userInscriptionForm.birthDate.value = year + "-" +month + "-"+ day;
						
		}
		
		function retourneBirthDate () {
			if( document.userInscriptionForm.birthDate.value.length == 10 ){
				document.getElementById("birthDate-day").value = document.userInscriptionForm.birthDate.value.substring(8);
				document.getElementById("birthDate-month").value= document.userInscriptionForm.birthDate.value.substring(5,7);
				document.getElementById("birthDate-year").value= document.userInscriptionForm.birthDate.value.substring(0,4);									
			}
		}
		
		retourneBirthDate (); 
		
</script>		

</body>
</html>