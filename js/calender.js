var scwDateNow=new Date(Date.parse(new Date().toDateString()));var scwBaseYear=scwDateNow.getFullYear()-100;var scwDropDownYears=100;var scwToday='Today:',scwDrag='<img src="images/close.jpg" width="16" height="16" onclick="scwHide()" alt="Close" title="Close" style="cursor:pointer; text-align:right">',scwInvalidDateMsg='The entered date is invalid.\n',scwOutOfRangeMsg='The entered date is out of range.',scwDoesNotExistMsg='The entered date does not exist.',scwInvalidAlert=['Invalid date (',') ignored.'],scwDateDisablingError=['Error ',' is not a Date object.'],scwRangeDisablingError=['Error ',' should consist of two elements.'],scwArrMonthNames=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],scwArrWeekInits=['S','M','T','W','T','F','S'];var scwWeekStart=1;var scwWeekNumberDisplay=false;var scwWeekNumberBaseDay=4;var scwArrDelimiters=['/','-','.',',',' '];var scwDateDisplayFormat='YYYY-MM-DD';var scwDateOutputFormat='YYYY-MM-DD';var scwDateInputSequence='DMY';var scwZindex=1;var scwBlnStrict=false;var scwEnabledDay=[true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true,true];var scwDisabledDates=new Array();var scwActiveToday=true;var scwOutOfRangeDisable=true;var scwAllowDrag=true;var scwClickToHide=true;var scwBackground='#F9F2F5';var scwHeadText='#000000';var scwTodayText='#000000',scwTodayHighlight='#47B3CD';var scwHighlightText='#000000',scwHighlightBackground='#47B3CD';var scwDragText='#CCCCFF',scwDragBackground='#9999CC';var scwWeekNumberText='#CCCCCC',scwWeekNumberBackground='#776677';var scwWeekendText='#CC6666',scwWeekendBackground='#CCCCCC';var scwExMonthText='#CCCCCC',scwExMonthBackground='#CCCCCC';var scwCellText='#000000',scwCellBackground='#CCCCCC';var scwInDateText='#FF0000',scwInDateBackground='#FFCCCC';var scwDisabledWeekendText='#CC6666',scwDisabledWeekendBackground='#999999';var scwDisabledExMonthText='#666666',scwDisabledExMonthBackground='#999999';var scwDisabledCellText='#000000',scwDisabledCellBackground='#999999';var scwDisabledInDateText='#FF0000',scwDisabledInDateBackground='#CC9999';document.writeln("<style>");document.writeln('.scw       {padding:1px;vertical-align:middle;}');document.writeln('iframe.scw {position:absolute;z-index:'+scwZindex+';top:0px;left:0px;visibility:hidden;'+'width:1px;height:1px;}');document.writeln('table.scw  {padding:0px;visibility:hidden;'+'position:absolute;width:200px;'+'top:0px;left:0px;z-index:'+(scwZindex+1)+';text-align:center;cursor:default;'+'padding:1px;vertical-align:middle;'+'background-color:'+scwBackground+';border:ridge 2px;font-size:10pt;'+'font-family:Arial,Helvetica,Sans-Serif;'+'font-weight:bold;}');document.writeln('td.scwDrag {text-align:right;font-size:8pt;'+'background-color:'+scwDragBackground+';padding:0px 0px;color:'+scwDragText+"}");document.writeln('td.scwHead {padding:0px 0px;text-align:center;}');document.writeln('select.scwHead {margin:3px 1px;}');document.writeln('input.scwHead  {height:22px;width:22px;'+'vertical-align:middle;'+'text-align:center;margin:2px 1px;'+'font-size:10pt;font-family:fixedSys;'+'font-weight:bold;}');document.writeln('td.scwWeekNumberHead '+'{text-align:center;font-weight:bold;'+'padding:0px;color:'+scwBackground+';}');document.writeln('td.scwWeek     {text-align:center;font-weight:bold;'+'padding:0px;color:'+scwHeadText+';}');document.writeln('table.scwCells {text-align:right;font-size:8pt;'+'width:96%;font-family:'+'Arial,Helvetica,Sans-Serif;}');document.writeln('td.scwCells    {padding:3px;vertical-align:middle;'+'width:16px;height:16px;'+'font-weight:bold;color:'+scwCellText+';background-color:'+scwCellBackground+'}');document.writeln('td.scwWeekNo   {padding:3px;vertical-align:middle;'+'width:16px;height:16px;'+'font-weight:bold;color:'+scwWeekNumberText+';background-color:'+scwWeekNumberBackground+'}');document.writeln('td.scwWeeks {padding:3px;vertical-align:middle;'+'width:16px;height:16px;'+'font-weight:bold;color:'+scwCellText+';background-color:'+scwCellBackground+'}');document.writeln('td.scwFoot  {padding:0px;text-align:center;'+'font-weight:normal;color:'+scwTodayText+';}');document.writeln("</style>");var scwTargetEle,scwTriggerEle,scwMonthSum=0,scwBlnFullInputDate=false,scwPassEnabledDay=new Array(),scwSeedDate=new Date(),scwParmActiveToday=true,scwWeekStart=scwWeekStart%7;Date.prototype.scwFormat=function(scwFormat)
{var charCount=0,codeChar='',result='';for(var i=0;i<=scwFormat.length;i++)
{if(i<scwFormat.length&&scwFormat.charAt(i)==codeChar)
{charCount++;}
else{switch(codeChar)
{case'y':case'Y':result+=(this.getFullYear()%Math.pow(10,charCount)).toString().scwPadLeft(charCount);break;case'm':case'M':result+=(charCount<3)?(this.getMonth()+1).toString().scwPadLeft(charCount):scwArrMonthNames[this.getMonth()];break;case'd':case'D':result+=this.getDate().toString().scwPadLeft(charCount);break;default:while(charCount-->0){result+=codeChar;}}
if(i<scwFormat.length)
{codeChar=scwFormat.charAt(i);charCount=1;}}}
return result;}
String.prototype.scwPadLeft=function(padToLength)
{var result='';for(var i=0;i<(padToLength-this.length);i++){result+='0';}
return(result+this);}
Function.prototype.runsAfterSCW=function(){var func=this,args=new Array(arguments.length);for(var i=0;i<args.length;++i)
{args[i]=arguments[i];}
return function()
{for(var i=0;i<arguments.length;++i)
{args[args.length]=arguments[i];}
return(args.shift()==scwTriggerEle)?func.apply(this,args):null;}};var scwNextActionReturn,scwNextAction;function showCal(scwEle,scwSourceEle){scwShow(scwEle,scwSourceEle);}
function scwShow(scwEle,scwSourceEle)
{scwTriggerEle=scwSourceEle;scwParmActiveToday=true;for(var i=0;i<7;i++)
{scwPassEnabledDay[(i+7-scwWeekStart)%7]=true;for(var j=2;j<arguments.length;j++)
{if(arguments[j]==i)
{scwPassEnabledDay[(i+7-scwWeekStart)%7]=false;if(scwDateNow.getDay()==i)scwParmActiveToday=false;}}}
scwSeedDate=scwDateNow;scwEle.value=scwEle.value.replace(/^\s+/,'').replace(/\s+$/,'');if(scwEle.value.length==0)
{scwBlnFullInputDate=false;if((new Date(scwBaseYear+scwDropDownYears-1,11,31))<scwSeedDate||(new Date(scwBaseYear,0,1))>scwSeedDate)
{scwSeedDate=new Date(scwBaseYear+Math.floor(scwDropDownYears/2),5,1);}}
else
{function scwInputFormat(scwEleValue)
{var scwArrSeed=new Array(),scwArrInput=scwEle.value.split(new RegExp('[\\'+scwArrDelimiters.join('\\')+']+','g'));if(scwArrInput[0].length==0)scwArrInput.splice(0,1);if(scwArrInput[scwArrInput.length-1].length==0)
scwArrInput.splice(scwArrInput.length-1,1);scwBlnFullInputDate=false;switch(scwArrInput.length)
{case 1:{scwArrSeed[0]=parseInt(scwArrInput[0],10);scwArrSeed[1]='6';scwArrSeed[2]=1;break;}
case 2:{scwArrSeed[0]=parseInt(scwArrInput[scwDateInputSequence.replace(/D/i,'').search(/Y/i)],10);scwArrSeed[1]=scwArrInput[scwDateInputSequence.replace(/D/i,'').search(/M/i)];scwArrSeed[2]=1;break;}
case 3:{scwArrSeed[0]=parseInt(scwArrInput[scwDateInputSequence.search(/Y/i)],10);scwArrSeed[1]=scwArrInput[scwDateInputSequence.search(/M/i)];scwArrSeed[2]=parseInt(scwArrInput[scwDateInputSequence.search(/D/i)],10);scwBlnFullInputDate=true;break;}
default:{scwArrSeed[0]=0;scwArrSeed[1]=0;scwArrSeed[2]=0;}}
var scwExpValDay=/^(0?[1-9]|[1-2]\d|3[0-1])$/,scwExpValMonth=new RegExp("^(0?[1-9]|1[0-2]|"+scwArrMonthNames.join("|")+")$","i"),scwExpValYear=/^(\d{1,2}|\d{4})$/;if(scwExpValYear.exec(scwArrSeed[0])==null||scwExpValMonth.exec(scwArrSeed[1])==null||scwExpValDay.exec(scwArrSeed[2])==null)
{scwBlnFullInputDate=false;scwArrSeed[0]=scwBaseYear+Math.floor(scwDropDownYears/2);scwArrSeed[1]='6';scwArrSeed[2]=1;}
return scwArrSeed;}
scwArrSeedDate=scwInputFormat(scwEle.value);if(scwArrSeedDate[0]<100)
scwArrSeedDate[0]+=(scwArrSeedDate[0]>50)?1900:2000;if(scwArrSeedDate[1].search(/\d+/)!=0)
{month=scwArrMonthNames.join('|').toUpperCase().search(scwArrSeedDate[1].substr(0,3).toUpperCase());scwArrSeedDate[1]=Math.floor(month/4)+1;}
scwSeedDate=new Date(scwArrSeedDate[0],scwArrSeedDate[1]-1,scwArrSeedDate[2]);}
if(isNaN(scwSeedDate))
{alert(scwInvalidDateMsg+scwInvalidAlert[0]+scwEle.value+scwInvalidAlert[1]);scwSeedDate=new Date(scwBaseYear+Math.floor(scwDropDownYears/2),5,1);scwBlnFullInputDate=false;}
else
{if((new Date(scwBaseYear,0,1))>scwSeedDate)
{if(scwBlnStrict)alert(scwOutOfRangeMsg);scwSeedDate=new Date(scwBaseYear,0,1);scwBlnFullInputDate=false;}
else
{if((new Date(scwBaseYear+scwDropDownYears-1,11,31))<scwSeedDate)
{if(scwBlnStrict)alert(scwOutOfRangeMsg);scwSeedDate=new Date(scwBaseYear+Math.floor(scwDropDownYears)-1,11,1);scwBlnFullInputDate=false;}
else
{if(scwBlnStrict&&scwBlnFullInputDate&&(scwSeedDate.getDate()!=scwArrSeedDate[2]||(scwSeedDate.getMonth()+1)!=scwArrSeedDate[1]||scwSeedDate.getFullYear()!=scwArrSeedDate[0]))
{alert(scwDoesNotExistMsg);scwSeedDate=new Date(scwSeedDate.getFullYear(),scwSeedDate.getMonth()-1,1);scwBlnFullInputDate=false;}}}}
for(var i=0;i<scwDisabledDates.length;i++)
{if(!((typeof scwDisabledDates[i]=='object')&&(scwDisabledDates[i].constructor==Date)))
{if((typeof scwDisabledDates[i]=='object')&&(scwDisabledDates[i].constructor==Array))
{var scwPass=true;if(scwDisabledDates[i].length!=2)
{alert(scwRangeDisablingError[0]+scwDisabledDates[i]+scwRangeDisablingError[1]);scwPass=false;}
else
{for(var j=0;j<scwDisabledDates[i].length;j++)
{if(!((typeof scwDisabledDates[i][j]=='object')&&(scwDisabledDates[i][j].constructor==Date)))
{alert(scwDateDisablingError[0]+scwDisabledDates[i][j]+scwDateDisablingError[1]);scwPass=false;}}}
if(scwPass&&(scwDisabledDates[i][0]>scwDisabledDates[i][1]))
{scwDisabledDates[i].reverse();}}
else
{alert(scwDateDisablingError[0]+scwDisabledDates[i]+scwDateDisablingError[1]);}}}
scwMonthSum=12*(scwSeedDate.getFullYear()-scwBaseYear)+scwSeedDate.getMonth();document.getElementById('scwYears').options.selectedIndex=Math.floor(scwMonthSum/12);document.getElementById('scwMonths').options.selectedIndex=(scwMonthSum%12);var offsetTop=parseInt(scwEle.offsetTop,10)+parseInt(scwEle.offsetHeight,10),offsetLeft=parseInt(scwEle.offsetLeft,10);scwTargetEle=scwEle;do{scwEle=scwEle.offsetParent;offsetTop+=parseInt(scwEle.offsetTop,10);offsetLeft+=parseInt(scwEle.offsetLeft,10);}
while(scwEle.tagName!='BODY');document.getElementById('scw').style.top=offsetTop+'px';document.getElementById('scw').style.left=offsetLeft+'px';if(document.getElementById('scwIframe'))
{document.getElementById('scwIframe').style.top=offsetTop+'px';document.getElementById('scwIframe').style.left=offsetLeft+'px';document.getElementById('scwIframe').style.width=(document.getElementById('scw').offsetWidth-2)+'px';document.getElementById('scwIframe').style.height=(document.getElementById('scw').offsetHeight-2)+'px';document.getElementById('scwIframe').style.visibility='visible';}
document.getElementById('scwDrag').style.display=(scwAllowDrag)?((document.getElementById('scwIFrame'))?'block':'table-row'):'none';scwShowMonth(0);document.getElementById('scw').style.visibility='visible';if(typeof event=='undefined')
{scwSourceEle.parentNode.addEventListener("click",scwStopPropagation,false);}
else{event.cancelBubble=true;}}
function scwHide()
{document.getElementById('scw').style.visibility='hidden';if(document.getElementById('scwIframe'))
{document.getElementById('scwIframe').style.visibility='hidden';}
if(typeof scwNextAction!='undefined'&&scwNextAction!=null)
{scwNextActionReturn=scwNextAction();scwNextAction=null;}}
function scwCancel(scwEvt)
{if(scwClickToHide)scwHide();scwStopPropagation(scwEvt);}
function scwStopPropagation(scwEvt)
{if(scwEvt.stopPropagation)
scwEvt.stopPropagation();else scwEvt.cancelBubble=true;}
function scwBeginDrag(event)
{var elementToDrag=document.getElementById('scw');var deltaX=event.clientX,deltaY=event.clientY,offsetEle=elementToDrag;do{deltaX-=parseInt(offsetEle.offsetLeft,10);deltaY-=parseInt(offsetEle.offsetTop,10);offsetEle=offsetEle.offsetParent;}
while(offsetEle.tagName!='BODY'&&offsetEle.tagName!='HTML');if(document.addEventListener)
{document.addEventListener('mousemove',moveHandler,true);document.addEventListener('mouseup',upHandler,true);}
else{elementToDrag.attachEvent('onmousemove',moveHandler);elementToDrag.attachEvent('onmouseup',upHandler);elementToDrag.setCapture();}
scwStopPropagation(event);function moveHandler(e)
{if(!e)e=window.event;elementToDrag.style.left=(e.clientX-deltaX)+'px';elementToDrag.style.top=(e.clientY-deltaY)+'px';if(document.getElementById('scwIframe'))
{document.getElementById('scwIframe').style.left=(e.clientX-deltaX)+'px';document.getElementById('scwIframe').style.top=(e.clientY-deltaY)+'px';}
scwStopPropagation(e);}
function upHandler(e)
{if(!e)e=window.event;if(document.removeEventListener)
{document.removeEventListener('mousemove',moveHandler,true);document.removeEventListener('mouseup',upHandler,true);}
else{elementToDrag.detachEvent('onmouseup',upHandler);elementToDrag.detachEvent('onmousemove',moveHandler);elementToDrag.releaseCapture();}
scwStopPropagation(e);}}
function scwShowMonth(scwBias)
{var scwShowDate=new Date(Date.parse(new Date().toDateString())),scwStartDate=new Date(),scwSaveBackground,scwSaveText;scwSelYears=document.getElementById('scwYears');scwSelMonths=document.getElementById('scwMonths');if(scwSelYears.options.selectedIndex>-1)
{scwMonthSum=12*(scwSelYears.options.selectedIndex)+scwBias;if(scwSelMonths.options.selectedIndex>-1)
{scwMonthSum+=scwSelMonths.options.selectedIndex;}}
else
{if(scwSelMonths.options.selectedIndex>-1)
{scwMonthSum+=scwSelMonths.options.selectedIndex;}}
scwShowDate.setFullYear(scwBaseYear+Math.floor(scwMonthSum/12),(scwMonthSum%12),1);document.getElementById("scwWeek_").style.display=(scwWeekNumberDisplay)?'block':'none';if((12*parseInt((scwShowDate.getFullYear()-scwBaseYear),10))+parseInt(scwShowDate.getMonth(),10)<(12*scwDropDownYears)&&(12*parseInt((scwShowDate.getFullYear()-scwBaseYear),10))+parseInt(scwShowDate.getMonth(),10)>-1)
{scwSelYears.options.selectedIndex=Math.floor(scwMonthSum/12);scwSelMonths.options.selectedIndex=(scwMonthSum%12);scwCurMonth=scwShowDate.getMonth();scwShowDate.setDate((((scwShowDate.getDay()-scwWeekStart)<0)?-6:1)+scwWeekStart-scwShowDate.getDay());scwStartDate=new Date(scwShowDate);var scwFoot=document.getElementById('scwFoot');function scwFootOutput(){scwSetOutput(scwDateNow);}
function scwFootOver()
{document.getElementById('scwFoot').style.color=scwTodayHighlight;document.getElementById('scwFoot').style.fontWeight='bold';}
function scwFootOut()
{document.getElementById('scwFoot').style.color=scwTodayText;document.getElementById('scwFoot').style.fontWeight='normal';}
if(scwDisabledDates.length==0)
{if(scwActiveToday&&scwParmActiveToday)
{scwFoot.onclick=scwFootOutput;scwFoot.onmouseover=scwFootOver;scwFoot.onmouseout=scwFootOut;scwFoot.style.cursor=(document.getElementById('scwIframe'))?'hand;':'pointer;';}
else
{scwFoot.onclick=null;if(document.addEventListener)
{scwFoot.addEventListener('click',scwStopPropagation,false);}
else{scwFoot.attachEvent('onclick',scwStopPropagation);}
scwFoot.onmouseover=null;scwFoot.onmouseout=null;scwFoot.style.cursor='default';}}
else
{for(var k=0;k<scwDisabledDates.length;k++)
{if(!scwActiveToday||!scwParmActiveToday||((typeof scwDisabledDates[k]=='object')&&(((scwDisabledDates[k].constructor==Date)&&scwDateNow.valueOf()==scwDisabledDates[k].valueOf())||((scwDisabledDates[k].constructor==Array)&&scwDateNow.valueOf()>=scwDisabledDates[k][0].valueOf()&&scwDateNow.valueOf()<=scwDisabledDates[k][1].valueOf()))))
{scwFoot.onclick=null;if(document.addEventListener)
{scwFoot.addEventListener('click',scwStopPropagation,false);}
else{scwFoot.attachEvent('onclick',scwStopPropagation);}
scwFoot.onmouseover=null;scwFoot.onmouseout=null;scwFoot.style.cursor='default';break;}
else
{scwFoot.onclick=scwFootOutput;scwFoot.onmouseover=scwFootOver;scwFoot.onmouseout=scwFootOut;scwFoot.style.cursor=(document.getElementById('scwIframe'))?'hand;':'pointer;';}}}
function scwSetOutput(scwOutputDate)
{scwTargetEle.value=scwOutputDate.scwFormat(scwDateOutputFormat);scwHide();}
function scwCellOutput(scwEvt)
{var scwEle=scwEventTrigger(scwEvt),scwOutputDate=new Date(scwStartDate);if(scwEle.nodeType==3)scwEle=scwEle.parentNode;scwOutputDate.setDate(scwStartDate.getDate()+parseInt(scwEle.id.substr(8),10));scwSetOutput(scwOutputDate);}
function scwHighlight(e)
{var scwEle=scwEventTrigger(e);if(scwEle.nodeType==3)scwEle=scwEle.parentNode;scwSaveText=scwEle.style.color;scwSaveBackground=scwEle.style.backgroundColor;scwEle.style.color=scwHighlightText;scwEle.style.backgroundColor=scwHighlightBackground;return true;}
function scwUnhighlight(e)
{var scwEle=scwEventTrigger(e);if(scwEle.nodeType==3)scwEle=scwEle.parentNode;scwEle.style.backgroundColor=scwSaveBackground;scwEle.style.color=scwSaveText;return true;}
function scwEventTrigger(e)
{if(!e)e=event;return e.target||e.srcElement;}
function scwWeekNumber(scwInDate)
{var scwInDateWeekBase=new Date(scwInDate);scwInDateWeekBase.setDate(scwInDateWeekBase.getDate()-scwInDateWeekBase.getDay()+scwWeekNumberBaseDay+((scwInDate.getDay()>scwWeekNumberBaseDay)?7:0));var scwFirstBaseDay=new Date(scwInDateWeekBase.getFullYear(),0,1)
scwFirstBaseDay.setDate(scwFirstBaseDay.getDate()-scwFirstBaseDay.getDay()+scwWeekNumberBaseDay);if(scwFirstBaseDay<new Date(scwInDateWeekBase.getFullYear(),0,1))
{scwFirstBaseDay.setDate(scwFirstBaseDay.getDate()+7);}
var scwStartWeekOne=new Date(scwFirstBaseDay-scwWeekNumberBaseDay+scwInDate.getDay());if(scwStartWeekOne>scwFirstBaseDay)
{scwStartWeekOne.setDate(scwStartWeekOne.getDate()-7);}
var scwWeekNo="0"+(Math.round((scwInDateWeekBase-scwFirstBaseDay)/604800000,0)+1);return scwWeekNo.substring(scwWeekNo.length-2,scwWeekNo.length);}
var scwCells=document.getElementById('scwCells');for(i=0;i<scwCells.childNodes.length;i++)
{var scwRows=scwCells.childNodes[i];if(scwRows.nodeType==1&&scwRows.tagName=='TR')
{if(scwWeekNumberDisplay)
{scwRows.childNodes[0].innerHTML=scwWeekNumber(scwShowDate);scwRows.childNodes[0].style.display='block';}
else
{scwRows.childNodes[0].style.display='none';}
for(j=1;j<scwRows.childNodes.length;j++)
{var scwCols=scwRows.childNodes[j];if(scwCols.nodeType==1&&scwCols.tagName=='TD')
{scwRows.childNodes[j].innerHTML=scwShowDate.getDate();var scwCellStyle=scwRows.childNodes[j].style,scwDisabled=(scwOutOfRangeDisable&&(scwShowDate<(new Date(scwBaseYear,0,1))||scwShowDate>(new Date(scwBaseYear+scwDropDownYears-1,11,31))))?true:false;for(var k=0;k<scwDisabledDates.length;k++)
{if((typeof scwDisabledDates[k]=='object')&&(scwDisabledDates[k].constructor==Date)&&scwShowDate.valueOf()==scwDisabledDates[k].valueOf())
{scwDisabled=false;}
else
{if((typeof scwDisabledDates[k]=='object')&&(scwDisabledDates[k].constructor==Array)&&scwShowDate.valueOf()>=scwDisabledDates[k][0].valueOf()&&scwShowDate.valueOf()<=scwDisabledDates[k][1].valueOf())
{scwDisabled=false;}}}
if(scwDisabled||!scwEnabledDay[j-1+(7*((i*scwCells.childNodes.length)/6))]||!scwPassEnabledDay[(j-1+(7*(i*scwCells.childNodes.length/6)))%7])
{scwRows.childNodes[j].onclick=null;scwRows.childNodes[j].onmouseover=null;scwRows.childNodes[j].onmouseout=null;scwRows.childNodes[j].style.cursor='default;';if(scwShowDate.getMonth()!=scwCurMonth)
{scwCellStyle.color=scwDisabledExMonthText;scwCellStyle.backgroundColor=scwDisabledExMonthBackground;}
else if(scwBlnFullInputDate&&scwShowDate.toDateString()==scwSeedDate.toDateString())
{scwCellStyle.color=scwDisabledInDateText;scwCellStyle.backgroundColor=scwDisabledInDateBackground;}
else if(scwShowDate.getDay()%6==0)
{scwCellStyle.color=scwDisabledWeekendText;scwCellStyle.backgroundColor=scwDisabledWeekendBackground;}
else
{scwCellStyle.color=scwDisabledCellText;scwCellStyle.backgroundColor=scwDisabledCellBackground;}}
else
{scwRows.childNodes[j].onclick=scwCellOutput;scwRows.childNodes[j].onmouseover=scwHighlight;scwRows.childNodes[j].onmouseout=scwUnhighlight;scwRows.childNodes[j].style.cursor=(document.getElementById('scwIframe'))?'hand;':'pointer;';if(scwShowDate.getMonth()!=scwCurMonth)
{scwCellStyle.color=scwExMonthText;scwCellStyle.backgroundColor=scwExMonthBackground;}
else if(scwBlnFullInputDate&&scwShowDate.toDateString()==scwSeedDate.toDateString())
{scwCellStyle.color=scwInDateText;scwCellStyle.backgroundColor=scwInDateBackground;}
else if(scwShowDate.getDay()%6==0)
{scwCellStyle.color=scwWeekendText;scwCellStyle.backgroundColor=scwWeekendBackground;}
else
{scwCellStyle.color=scwCellText;scwCellStyle.backgroundColor=scwCellBackground;}}
scwShowDate.setDate(scwShowDate.getDate()+1);}}}}}}
document.write("<!--[if IE]>"+"<iframe class='scw' src='scwblank.html' "+"id='scwIframe' name='scwIframe' "+"frameborder='0'>"+"</iframe>"+"<![endif]-->"+"<table id='scw' class='scw' onclick='scwCancel(event);'>"+"<tr class='scw'>"+"<td class='scw'>"+"<table class='scwHead' id='scwHead' width='100%' "+"onClick='scwStopPropagation(event);' "+"cellspacing='0' cellpadding='0'>"+"<tr id='scwDrag' style='display:none;'>"+"<td colspan='4' class='scwDrag' "+"onmousedown='scwBeginDrag(event);'>"+scwDrag+"</td>"+"</tr>"+"<tr class='scwHead'>"+"<td class='scwHead'>"+"<input class='scwHead' type='button' value='<' "+"onclick='scwShowMonth(-1);'  /></td>"+"<td class='scwHead'>"+"<select id='scwMonths' class='scwHead' "+"onChange='scwShowMonth(0);'>");for(i=0;i<scwArrMonthNames.length;i++)
document.write("<option>"+scwArrMonthNames[i]+"</option>");document.write("   </select>"+"</td>"+"<td class='scwHead'>"+"<select id='scwYears' class='scwHead' "+"onChange='scwShowMonth(0);'>");for(i=0;i<scwDropDownYears;i++)
document.write("<option>"+(scwBaseYear+i)+"</option>");document.write("</select>"+"</td>"+"<td class='scwHead'>"+"<input class='scwHead' type='button' value='>' "+"onclick='scwShowMonth(1);' /></td>"+"</tr>"+"</table>"+"</td>"+"</tr>"+"<tr class='scw'>"+"<td class='scw'>"+"<table class='scwCells' align='center'>"+"<thead>"+"<tr><td class='scwWeekNumberHead' id='scwWeek_' ></td>");for(i=0;i<scwArrWeekInits.length;i++)
document.write("<td class='scwWeek' id='scwWeekInit"+i+"'>"+scwArrWeekInits[(i+scwWeekStart)%scwArrWeekInits.length]+"</td>");document.write("</tr>"+"</thead>"+"<tbody id='scwCells' "+"onClick='scwStopPropagation(event);'>");for(i=0;i<6;i++)
{document.write("<tr>"+"<td class='scwWeekNo' id='scwWeek_"+i+"'></td>");for(j=0;j<7;j++)
{document.write("<td class='scwCells' id='scwCell_"+(j+(i*7))+"'></td>");}
document.write("</tr>");}
document.write("</tbody>");if((new Date(scwBaseYear+scwDropDownYears,11,32))>scwDateNow&&(new Date(scwBaseYear,0,0))<scwDateNow)
{document.write("<tfoot class='scwFoot'>"+"<tr class='scwFoot'>"+"<td class='scwFoot' id='scwFoot' colspan='8'>"+scwToday+" "+scwDateNow.scwFormat(scwDateDisplayFormat)+"</td>"+"</tr>"+"</tfoot>");}
document.write("</table>"+"</td>"+"</tr>"+"</table>");if(document.addEventListener)
{document.addEventListener('click',scwHide,false);}
else{document.attachEvent('onclick',scwHide);}