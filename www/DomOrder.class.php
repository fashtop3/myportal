<?php
	include_once("nusoap.php");
	include_once("apiutil.php");

	class DomOrder
	{
		var $serviceObj;
		var $wsdlFileName;
		function DomOrder($wsdlFileName="wsdl/DomOrder.wsdl")
		{
			$this->wsdlFileName = $wsdlFileName;
			$this->serviceObj = new soapclient_nusoap($this->wsdlFileName,"wsdl");
		}
		function getDetails(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId, $option)
		{
			$return = $this->serviceObj->call("getDetails",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId, $option));
			debugfunction($this->serviceObj);
			return $return;
		}
		function checkAvailabilityMultiple($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainNames, $tlds, $suggestAlternative)
		{
			$return = $this->serviceObj->call("checkAvailabilityMultiple",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainNames, $tlds, $suggestAlternative));

			debugfunction($this->serviceObj);
			return $return;
		}
	}
?>