<?php
	include_once("nusoap.php");
	include_once("apiutil.php");

	class MailFwdOrder
	{
		var $serviceObj;
		var $wsdlFileName;
		function MailFwdOrder($wsdlFileName="wsdl/MailFwdOrder.wsdl")
		{
			$this->wsdlFileName = $wsdlFileName;
			$this->serviceObj = new soapclient_nusoap($this->wsdlFileName,"wsdl");
		}
		function getDetails(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId, $option)
		{
			$return = $this->serviceObj->call("getDetails",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId, $option));
			debugfunction($this->serviceObj);
			return $return;
		}
		function del(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId)
		{
			$return = $this->serviceObj->call("del",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId));
			debugfunction($this->serviceObj);
			return $return;
		}
		function getDetailsByDomain(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainName, $option, $productkey)
		{
			$return = $this->serviceObj->call("getDetailsByDomain",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainName, $option, $productkey));
			debugfunction($this->serviceObj);
			return $return;
		}
		function getOrderIdByDomain(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainName, $productkey)
		{
			$return = $this->serviceObj->call("getOrderIdByDomain",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainName, $productkey));
			debugfunction($this->serviceObj);
			return $return;
		}
		function renew(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainHash, $invoiceOption)
		{
			$return = $this->serviceObj->call("renew",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainHash, $invoiceOption));
			debugfunction($this->serviceObj);
			return $return;
		}
		function addForward(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $prefix, $forwardTo)
		{
			$return = $this->serviceObj->call("addForward",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $prefix, $forwardTo));
			debugfunction($this->serviceObj);
			return $return;
		}
		function modForward(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $prefix, $forwardTo)
		{
			$return = $this->serviceObj->call("modForward",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $prefix, $forwardTo));
			debugfunction($this->serviceObj);
			return $return;
		}
		function delForward(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $prefix)
		{
			$return = $this->serviceObj->call("delForward",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $prefix));
			debugfunction($this->serviceObj);
			return $return;
		}
		function activateMailForwardService(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId)
		{
			$return = $this->serviceObj->call("activateMailForwardService",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId));
			debugfunction($this->serviceObj);
			return $return;
		}
		function add(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainHash, $customerId, $invoiceOption)
		{
			$return = $this->serviceObj->call("add",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainHash, $customerId, $invoiceOption));
			debugfunction($this->serviceObj);
			return $return;
		}
		function listOrder(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId, $resellerId, $customerId, $showChildOrders, $currentStatus, $description, $isBounceBack, $source, $destination, $creationDTRangStart, $creationDTRangEnd, $endTimeRangStart, $endTimeRangEnd, $numOfRecordPerPage, $pageNum, $orderBy)
		{
			$return = $this->serviceObj->call("list",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId, $resellerId, $customerId, $showChildOrders, $currentStatus, $description, $isBounceBack, $source, $destination, $creationDTRangStart, $creationDTRangEnd, $endTimeRangStart, $endTimeRangEnd, $numOfRecordPerPage, $pageNum, $orderBy));
			debugfunction($this->serviceObj);
			return $return;
		}
		function mod(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $status, $defaultForward)
		{
			$return = $this->serviceObj->call("mod",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $status, $defaultForward));
			debugfunction($this->serviceObj);
			return $return;
		}
	}
?>