<?php
	include_once("nusoap.php");
	include_once("apiutil.php");

	class DotTel
	{
		var $serviceObj;
		var $wsdlFileName;
		function DotTel($wsdlFileName="wsdl/DotTel.wsdl")
		{
			$this->wsdlFileName = $wsdlFileName;
			$this->serviceObj = new soapclient_nusoap($this->wsdlFileName,"wsdl");
		}
		function modifyWhoisPreference(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId, $whoisType, $publish)
		{
			$return = $this->serviceObj->call("modifyWhoisPreference",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId, $whoisType, $publish));
			debugfunction($this->serviceObj);
			return $return;
		}
		function getCTHLoginDetails(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId)
		{
			$return = $this->serviceObj->call("getCTHLoginDetails",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $orderId));
			debugfunction($this->serviceObj);
			return $return;
		}
	}
?>