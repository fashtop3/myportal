<?php
	include_once("nusoap.php");
	include_once("apiutil.php");

	class ThirdLevelDotUk
	{
		var $serviceObj;
		var $wsdlFileName;
		function ThirdLevelDotUk($wsdlFileName="wsdl/ThirdLevelDotUk.wsdl")
		{
			$this->wsdlFileName = $wsdlFileName;
			$this->serviceObj = new soapclient_nusoap($this->wsdlFileName,"wsdl");
		}
		function activateTransferredDomain(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainName, $securityKey, $customerId)
		{
			$return = $this->serviceObj->call("activateTransferredDomain",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $domainName, $securityKey, $customerId));
			debugfunction($this->serviceObj);
			return $return;
		}
		function releaseDomain(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $newTag)
		{
			$return = $this->serviceObj->call("releaseDomain",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $entityId, $newTag));
			debugfunction($this->serviceObj);
			return $return;
		}
	}
?>