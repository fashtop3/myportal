<?php
	include_once("nusoap.php");
	include_once("apiutil.php");

	class DotTravel
	{
		var $serviceObj;
		var $wsdlFileName;
		function DotTravel($wsdlFileName="wsdl/DotTravel.wsdl")
		{
			$this->wsdlFileName = $wsdlFileName;
			$this->serviceObj = new soapclient_nusoap($this->wsdlFileName,"wsdl");
		}
		function getEligibleDomains(
			$SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $uin)
		{
			$return = $this->serviceObj->call("getEligibleDomains",array($SERVICE_USERNAME, $SERVICE_PASSWORD, $SERVICE_ROLE, $SERVICE_LANGPREF, $SERVICE_PARENTID, $uin));
			debugfunction($this->serviceObj);
			return $return;
		}
	}
?>