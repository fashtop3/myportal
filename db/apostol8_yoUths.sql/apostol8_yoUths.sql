-- phpMyAdmin SQL Dump
-- version 3.4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2011 at 01:17 AM
-- Server version: 5.1.58
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `apostol8_yoUths`
--

-- --------------------------------------------------------

--
-- Table structure for table `jos_banner`
--

DROP TABLE IF EXISTS `jos_banner`;
CREATE TABLE IF NOT EXISTS `jos_banner` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) NOT NULL DEFAULT 'banner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(100) NOT NULL DEFAULT '',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `date` datetime DEFAULT NULL,
  `showBanner` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `viewbanner` (`showBanner`),
  KEY `idx_banner_catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `jos_banner`
--

INSERT INTO `jos_banner` (`bid`, `cid`, `type`, `name`, `alias`, `imptotal`, `impmade`, `clicks`, `imageurl`, `clickurl`, `date`, `showBanner`, `checked_out`, `checked_out_time`, `editor`, `custombannercode`, `catid`, `description`, `sticky`, `ordering`, `publish_up`, `publish_down`, `tags`, `params`) VALUES
(1, 1, 'banner', 'OSM 1', 'osm-1', 0, 187, 1, 'osmbanner1.png', 'http://www.opensourcematters.org', '2004-07-07 15:31:29', 1, 0, '0000-00-00 00:00:00', '', '', 13, '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(2, 1, 'banner', 'OSM 2', 'osm-2', 0, 49, 0, 'osmbanner2.png', 'http://www.opensourcematters.org', '2004-07-07 15:31:29', 1, 0, '0000-00-00 00:00:00', '', '', 13, '', 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(3, 1, '', 'Joomla!', 'joomla', 0, 17, 0, '', 'http://www.joomla.org', '2006-05-29 14:21:28', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomla! The most popular and widely used Open Source CMS Project in the world.', 14, '', 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 1, '', 'JoomlaCode', 'joomlacode', 0, 17, 0, '', 'http://joomlacode.org', '2006-05-29 14:19:26', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomlaCode, development and distribution made easy.', 14, '', 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 1, '', 'Joomla! Extensions', 'joomla-extensions', 0, 12, 0, '', 'http://extensions.joomla.org', '2006-05-29 14:23:21', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nJoomla! Components, Modules, Plugins and Languages by the bucket load.', 14, '', 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 1, '', 'Joomla! Shop', 'joomla-shop', 0, 12, 0, '', 'http://shop.joomla.org', '2006-05-29 14:23:21', 1, 0, '0000-00-00 00:00:00', '', '<a href="{CLICKURL}" target="_blank">{NAME}</a>\r\n<br/>\r\nFor all your Joomla! merchandise.', 14, '', 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(7, 1, '', 'Joomla! Promo Shop', 'joomla-promo-shop', 0, 8, 1, 'shop-ad.jpg', 'http://shop.joomla.org', '2007-09-19 17:26:24', 1, 0, '0000-00-00 00:00:00', '', '', 33, '', 0, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(8, 1, '', 'Joomla! Promo Books', 'joomla-promo-books', 0, 9, 0, 'shop-ad-books.jpg', 'http://shop.joomla.org/amazoncom-bookstores.html', '2007-09-19 17:28:01', 1, 0, '0000-00-00 00:00:00', '', '', 33, '', 0, 4, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_bannerclient`
--

DROP TABLE IF EXISTS `jos_bannerclient`;
CREATE TABLE IF NOT EXISTS `jos_bannerclient` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` time DEFAULT NULL,
  `editor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_bannerclient`
--

INSERT INTO `jos_bannerclient` (`cid`, `name`, `contact`, `email`, `extrainfo`, `checked_out`, `checked_out_time`, `editor`) VALUES
(1, 'Open Source Matters', 'Administrator', 'admin@opensourcematters.org', '', 0, '00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jos_bannertrack`
--

DROP TABLE IF EXISTS `jos_bannertrack`;
CREATE TABLE IF NOT EXISTS `jos_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_categories`
--

DROP TABLE IF EXISTS `jos_categories`;
CREATE TABLE IF NOT EXISTS `jos_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `jos_categories`
--

INSERT INTO `jos_categories` (`id`, `parent_id`, `title`, `name`, `alias`, `image`, `section`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `editor`, `ordering`, `access`, `count`, `params`) VALUES
(1, 0, 'Event Reports', '', 'eventreports', 'taking_notes.jpg', '5', 'left', '<p>Reports from recent events</p>', 1, 0, '0000-00-00 00:00:00', '', 1, 0, 1, ''),
(2, 0, 'Joomla! Specific Links', '', 'joomla-specific-links', 'clock.jpg', 'com_weblinks', 'left', 'A selection of links that are all related to the Joomla! Project.', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(3, 0, 'Devotionals', '', 'devotionals', '', '5', 'left', '', 1, 0, '0000-00-00 00:00:00', '', 2, 0, 0, ''),
(4, 0, 'Joomla!', '', 'joomla', '', 'com_newsfeeds', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(5, 0, 'Free and Open Source Software', '', 'free-and-open-source-software', '', 'com_newsfeeds', 'left', 'Read the latest news about free and open source software from some of its leading advocates.', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(6, 0, 'Related Projects', '', 'related-projects', '', 'com_newsfeeds', 'left', 'Joomla builds on and collaborates with many other free and open source projects. Keep up with the latest news from some of them.', 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(12, 0, 'Contacts', '', 'contacts', '', 'com_contact_details', 'left', 'Contact Details for this Web site', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(13, 0, 'Joomla', '', 'joomla', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(14, 0, 'Text Ads', '', 'text-ads', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 0, 0, 0, ''),
(15, 0, 'Features', '', 'features', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 6, 0, 0, ''),
(17, 0, 'Benefits', '', 'benefits', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(18, 0, 'Platforms', '', 'platforms', '', 'com_content', 'left', '', 0, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(19, 0, 'Other Resources', '', 'other-resources', '', 'com_weblinks', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(29, 0, 'Legals', '', 'legalcontent', '', '4', 'left', '<p>Legal content such as privacy policies, terms of use, accessibility statements and other go in here.</p>', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(25, 0, 'About AFM Youths', '', 'aboutus', '', '4', 'left', '<p>All Content about the youth budy goes in here</p>', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(27, 0, 'Reflections', '', 'reflections', '', '5', 'left', '<p>Reflections from the past</p>', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(30, 0, 'Multimedia', '', 'multimedia', '', '5', 'left', '<p>Multimedia content including videos and images from events and other occasions.</p>', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(31, 0, 'Get Inspired', '', 'getinspired', '', '5', 'left', '<p>Inspirational articles go in here</p>', 0, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(32, 0, 'Testimonies', '', 'testimonies', '', '5', 'left', '<p>Testimonies from young people</p>', 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(33, 0, 'Joomla! Promo', '', 'joomla-promo', '', 'com_banner', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(34, 0, 'Basic', '', 'basic', '', 'com_rokcandy', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(35, 0, 'Typography', '', 'typography', '', 'com_rokcandy', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(38, 0, 'FrontPage Highlights', '', 'highlights', '', '5', 'left', '<p>Highlights for the front page go in here</p>', 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(41, 0, 'Statements and Memos', '', 'statements', '', '1', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 7, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_chronoforms_contactform`
--

DROP TABLE IF EXISTS `jos_chronoforms_contactform`;
CREATE TABLE IF NOT EXISTS `jos_chronoforms_contactform` (
  `cf_id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recordtime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cf_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_chronoforms_prayerrequest`
--

DROP TABLE IF EXISTS `jos_chronoforms_prayerrequest`;
CREATE TABLE IF NOT EXISTS `jos_chronoforms_prayerrequest` (
  `cf_id` int(11) NOT NULL,
  `uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `recordtime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ipaddress` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cf_user_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`cf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jos_chrono_contact`
--

DROP TABLE IF EXISTS `jos_chrono_contact`;
CREATE TABLE IF NOT EXISTS `jos_chrono_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `html` longtext NOT NULL,
  `scriptcode` longtext NOT NULL,
  `stylecode` longtext NOT NULL,
  `redirecturl` text NOT NULL,
  `emailresults` tinyint(1) NOT NULL,
  `fieldsnames` text NOT NULL,
  `fieldstypes` text NOT NULL,
  `onsubmitcode` longtext NOT NULL,
  `onsubmitcodeb4` longtext NOT NULL,
  `server_validation` longtext NOT NULL,
  `attformtag` longtext NOT NULL,
  `submiturl` text NOT NULL,
  `emailtemplate` longtext NOT NULL,
  `useremailtemplate` longtext NOT NULL,
  `paramsall` longtext NOT NULL,
  `titlesall` longtext NOT NULL,
  `extravalrules` longtext NOT NULL,
  `dbclasses` longtext NOT NULL,
  `autogenerated` longtext NOT NULL,
  `chronocode` longtext NOT NULL,
  `theme` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `extra1` longtext NOT NULL,
  `extra2` longtext NOT NULL,
  `extra3` longtext NOT NULL,
  `extra4` longtext NOT NULL,
  `extra5` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jos_chrono_contact`
--

INSERT INTO `jos_chrono_contact` (`id`, `name`, `html`, `scriptcode`, `stylecode`, `redirecturl`, `emailresults`, `fieldsnames`, `fieldstypes`, `onsubmitcode`, `onsubmitcodeb4`, `server_validation`, `attformtag`, `submiturl`, `emailtemplate`, `useremailtemplate`, `paramsall`, `titlesall`, `extravalrules`, `dbclasses`, `autogenerated`, `chronocode`, `theme`, `published`, `extra1`, `extra2`, `extra3`, `extra4`, `extra5`) VALUES
(1, 'prayerrequest', '<div class="form_item">\r\n  <div class="form_element cf_text"> <span class="cf_text">Required fields are marked with an asterisk (*)</span> </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Name</label>\r\n    <input class="cf_inputbox" maxlength="150" size="30" title="" id="text_2" name="name" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Email Address *</label>\r\n    <input class="cf_inputbox required validate-email" maxlength="150" size="30" title="Please enter a valid email address" id="text_3" name="email" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Phone Number</label>\r\n    <input class="cf_inputbox" maxlength="150" size="30" title="" id="text_6" name="telephone" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textarea">\r\n    <label class="cf_label" style="width: 150px;">Prayer Request *</label>\r\n    <textarea class="cf_inputbox required" rows="3" id="text_4" title="Enter email address" cols="30" name="request"></textarea>\r\n    \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_button">\r\n    <input value="Submit" name="button_5" type="submit" />\r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n', '', '', '', 2, 'name,email,telephone,request', 'text,text,text,textarea', '<p>Thank you for sending your prayer request. We will join you in prayer.</p>', '', '', '', '', '', '', 'formmethod=post\nLoadFiles=Yes\nsubmissions_limit=\nsubmissions_limit_error=Sorry but you can not submit the form again very soon like this!\nhandlepostedarrays=Yes\ndebug=0\ncheckToken=1\nmysql_type=1\nenmambots=No\ncaptcha_dataload=0\ncaptcha_dataload_skip=\nuseCurrent=\ndatefieldformat=d/m/Y\ndatefieldsnames=\ndatefieldextras=classes: [''dashboard'']\ndbconnection=No\nsavedataorder=after_email\ndvfields=\ndvrecord=Record #n\nuploads=No\nuploadfields=\nuploadpath=/home/favor09/public_html/afmyouth/components/com_chronocontact/uploads/prayerrequest/\nfilename_format=$filename = date(''YmdHis'').''_''.$chronofile[''name''];\nupload_exceedslimit=Sorry, Your uploaded file size exceeds the allowed limit.\nupload_lesslimit=Sorry, Your uploaded file size is less than the allowed limit\nupload_notallowed=Sorry, Your uploaded file type is not allowed\nimagever=No\nimtype=0\nimgver_error_msg=You have entered an incorrect verification code at the bottom of the form.\nvalidate=No\nvalidatetype=mootools\nvalidate_onlyOnBlur=1\nvalidate_wait=\nvalidate_onlyOnSubmit=0\nvalidation_type=default\nval_required=\nval_validate_number=\nval_validate_digits=\nval_validate_alpha=\nval_validate_alphanum=\nval_validate_date=\nval_validate_email=\nval_validate_url=\nval_validate_date_au=\nval_validate_currency_dollar=\nval_validate_selection=\nval_validate_one_required=\nval_validate_confirmation=\nservervalidate=No\nautogenerated_order=3\nonsubmitcode_order=2\nplugins_order=1\nplugins=\nmplugins_order=\ntablenames=jos_chronoforms_prayerrequest', '', '', '<?php\nif (!class_exists(''Tablechronoforms_prayerrequest'')) {\nclass Tablechronoforms_prayerrequest extends JTable {\nvar $cf_id = null;\nvar $uid = null;\nvar $recordtime = null;\nvar $ipaddress = null;\nvar $cf_user_id = null;\nfunction __construct( &$database ) {\nparent::__construct( ''jos_chronoforms_prayerrequest'', ''cf_id'', $database );\n}\n}\n}\n?>\n', '<?php\n		$MyForm =& CFChronoForm::getInstance("prayerrequest");\n		if($MyForm->formparams("dbconnection") == "Yes"){\n			$user = JFactory::getUser();			\n			$row =& JTable::getInstance("chronoforms_prayerrequest", "Table");\n			srand((double)microtime()*10000);\n			$inum	=	"I" . substr(base64_encode(md5(rand())), 0, 16).md5(uniqid(mt_rand(), true));\n			JRequest::setVar( "recordtime", JRequest::getVar( "recordtime", date("Y-m-d")." - ".date("H:i:s"), "post", "string", "" ));\n			JRequest::setVar( "ipaddress", JRequest::getVar( "ipaddress", $_SERVER["REMOTE_ADDR"], "post", "string", "" ));\n			JRequest::setVar( "uid", JRequest::getVar( "uid", $inum, "post", "string", "" ));\n			JRequest::setVar( "cf_user_id", JRequest::getVar( "cf_user_id", $user->id, "post", "int", "" ));\n			$post = JRequest::get( "post" , JREQUEST_ALLOWRAW );			\n			if (!$row->bind( $post )) {\n				JError::raiseWarning(100, $row->getError());\n			}				\n			if (!$row->store()) {\n				JError::raiseWarning(100, $row->getError());\n			}\n			$MyForm->tablerow["jos_chronoforms_prayerrequest"] = $row;\n		}\n		?>\n		', '[type="cf_text"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLclass = cf_textCHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLtag = spanCHRONO_CONSTANT_EOLlabelwidth = 0CHRONO_CONSTANT_EOLlabeltext = Required fields are marked with an asterisk (*)CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_2CHRONO_CONSTANT_EOLname = nameCHRONO_CONSTANT_EOLclass = cf_inputboxCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = NameCHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_3CHRONO_CONSTANT_EOLname = emailCHRONO_CONSTANT_EOLclass = cf_inputbox required validate-emailCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = Please enter a valid email addressCHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Email Address *CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_6CHRONO_CONSTANT_EOLname = telephoneCHRONO_CONSTANT_EOLclass = cf_inputboxCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Phone NumberCHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textarea"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_4CHRONO_CONSTANT_EOLname = requestCHRONO_CONSTANT_EOLclass = cf_inputbox requiredCHRONO_CONSTANT_EOLrows = 3CHRONO_CONSTANT_EOLcols = 30CHRONO_CONSTANT_EOLtitle = Enter email addressCHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Prayer Request *CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_button"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLvalue = SubmitCHRONO_CONSTANT_EOLname = button_5CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLreset = 0CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 0CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL', 'default', 1, '', '', '', '', ''),
(2, 'contactform', '<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">First Name *</label>\r\n    <input class="cf_inputbox required" maxlength="150" size="30" title="Please provide your name" id="text_0" name="fname" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Last Name</label>\r\n    <input class="cf_inputbox" maxlength="150" size="30" title="" id="text_1" name="lname" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Email *</label>\r\n    <input class="cf_inputbox required validate-email" maxlength="150" size="30" title="please enter valid email" id="text_2" name="email" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Phone Number</label>\r\n    <input class="cf_inputbox" maxlength="150" size="30" title="" id="text_3" name="phone" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_heading">\r\n    <h4 class="cf_text">Reason for contact</h4>\r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_radiobutton">\r\n    <label class="cf_label" style="width: 150px;">Please select one *</label>\r\n    <div class="float_left">\r\n      <input value="General Enquiries" title="" class="radio validate-one-required" id="radio00" name="radio0" type="radio" />\r\n      <label for="radio00" class="radio_label">General Enquiries</label>\r\n      <br />\r\n      \r\n<input value="Suggestions/Feedback" title="" class="radio validate-one-required" id="radio01" name="radio0" type="radio" />\r\n      <label for="radio01" class="radio_label">Suggestions/Feedback</label>\r\n      <br />\r\n      \r\n<input value="Prayer Requests" title="" class="radio validate-one-required" id="radio02" name="radio0" type="radio" />\r\n      <label for="radio02" class="radio_label">Prayer Requests</label>\r\n      <br />\r\n      \r\n\r\n    </div>\r\n    \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textarea">\r\n    <label class="cf_label" style="width: 150px;">Comments *</label>\r\n    <textarea class="cf_inputbox required" rows="3" id="text_6" title="" cols="30" name="comments"></textarea>\r\n    \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_button">\r\n    <input value="Submit" name="button_7" type="submit" />\r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n', '', '', '', 2, 'fname,lname,email,phone,radio0,comments', 'text,text,text,text,radio,textarea', '<p>Hi {fname},</p>\r\n<p>Thank you for contacting us. We will try to respond within the next 48 hours, usually less.</p>', '', '', '', '', '', '', 'formmethod=post\nLoadFiles=Yes\nsubmissions_limit=\nsubmissions_limit_error=Sorry but you can not submit the form again very soon like this!\nhandlepostedarrays=Yes\ndebug=0\ncheckToken=1\nmysql_type=1\nenmambots=No\ncaptcha_dataload=0\ncaptcha_dataload_skip=\nuseCurrent=\ndatefieldformat=d/m/Y\ndatefieldsnames=\ndatefieldextras=classes: [''dashboard'']\ndbconnection=Yes\nsavedataorder=after_email\ndvfields=\ndvrecord=Record #n\nuploads=No\nuploadfields=\nuploadpath=/home/favor09/public_html/afmyouth/components/com_chronocontact/uploads/contactform/\nfilename_format=$filename = date(''YmdHis'').''_''.$chronofile[''name''];\nupload_exceedslimit=Sorry, Your uploaded file size exceeds the allowed limit.\nupload_lesslimit=Sorry, Your uploaded file size is less than the allowed limit\nupload_notallowed=Sorry, Your uploaded file type is not allowed\nimagever=No\nimtype=0\nimgver_error_msg=You have entered an incorrect verification code at the bottom of the form.\nvalidate=No\nvalidatetype=mootools\nvalidate_onlyOnBlur=1\nvalidate_wait=\nvalidate_onlyOnSubmit=0\nvalidation_type=default\nval_required=\nval_validate_number=\nval_validate_digits=\nval_validate_alpha=\nval_validate_alphanum=\nval_validate_date=\nval_validate_email=\nval_validate_url=\nval_validate_date_au=\nval_validate_currency_dollar=\nval_validate_selection=\nval_validate_one_required=\nval_validate_confirmation=\nservervalidate=No\nautogenerated_order=3\nonsubmitcode_order=2\nplugins_order=1\nplugins=\nmplugins_order=\ntablenames=jos_chronoforms_contactform', '', '', '<?php\nif (!class_exists(''Tablechronoforms_contactform'')) {\nclass Tablechronoforms_contactform extends JTable {\nvar $cf_id = null;\nvar $uid = null;\nvar $recordtime = null;\nvar $ipaddress = null;\nvar $cf_user_id = null;\nfunction __construct( &$database ) {\nparent::__construct( ''jos_chronoforms_contactform'', ''cf_id'', $database );\n}\n}\n}\n?>\n', '<?php\n		$MyForm =& CFChronoForm::getInstance("contactform");\n		if($MyForm->formparams("dbconnection") == "Yes"){\n			$user = JFactory::getUser();			\n			$row =& JTable::getInstance("chronoforms_contactform", "Table");\n			srand((double)microtime()*10000);\n			$inum	=	"I" . substr(base64_encode(md5(rand())), 0, 16).md5(uniqid(mt_rand(), true));\n			JRequest::setVar( "recordtime", JRequest::getVar( "recordtime", date("Y-m-d")." - ".date("H:i:s"), "post", "string", "" ));\n			JRequest::setVar( "ipaddress", JRequest::getVar( "ipaddress", $_SERVER["REMOTE_ADDR"], "post", "string", "" ));\n			JRequest::setVar( "uid", JRequest::getVar( "uid", $inum, "post", "string", "" ));\n			JRequest::setVar( "cf_user_id", JRequest::getVar( "cf_user_id", $user->id, "post", "int", "" ));\n			$post = JRequest::get( "post" , JREQUEST_ALLOWRAW );			\n			if (!$row->bind( $post )) {\n				JError::raiseWarning(100, $row->getError());\n			}				\n			if (!$row->store()) {\n				JError::raiseWarning(100, $row->getError());\n			}\n			$MyForm->tablerow["jos_chronoforms_contactform"] = $row;\n		}\n		?>\n		', '[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_0CHRONO_CONSTANT_EOLname = fnameCHRONO_CONSTANT_EOLclass = cf_inputbox requiredCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = Please provide your nameCHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = First Name *CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_1CHRONO_CONSTANT_EOLname = lnameCHRONO_CONSTANT_EOLclass = cf_inputboxCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Last NameCHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_2CHRONO_CONSTANT_EOLname = emailCHRONO_CONSTANT_EOLclass = cf_inputbox required validate-emailCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = please enter valid emailCHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Email *CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_3CHRONO_CONSTANT_EOLname = phoneCHRONO_CONSTANT_EOLclass = cf_inputboxCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Phone NumberCHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_heading"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLclass = cf_textCHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLtag = h4CHRONO_CONSTANT_EOLlabelwidth = 0CHRONO_CONSTANT_EOLlabeltext = Reason for contactCHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_radiobutton"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLclass = radio validate-one-requiredCHRONO_CONSTANT_EOLname = radio0CHRONO_CONSTANT_EOLtheoptions = General Enquiries*,*Suggestions/Feedback*,*Prayer RequestsCHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Please select one *CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textarea"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_6CHRONO_CONSTANT_EOLname = commentsCHRONO_CONSTANT_EOLclass = cf_inputbox requiredCHRONO_CONSTANT_EOLrows = 3CHRONO_CONSTANT_EOLcols = 30CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Comments *CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_button"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLvalue = SubmitCHRONO_CONSTANT_EOLname = button_7CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLreset = 0CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 0CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL', 'default', 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_chrono_contact_elements`
--

DROP TABLE IF EXISTS `jos_chrono_contact_elements`;
CREATE TABLE IF NOT EXISTS `jos_chrono_contact_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `placeholder` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `code` longtext NOT NULL,
  `params` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_chrono_contact_emails`
--

DROP TABLE IF EXISTS `jos_chrono_contact_emails`;
CREATE TABLE IF NOT EXISTS `jos_chrono_contact_emails` (
  `emailid` int(11) NOT NULL AUTO_INCREMENT,
  `formid` int(11) NOT NULL,
  `to` text NOT NULL,
  `dto` text NOT NULL,
  `subject` text NOT NULL,
  `dsubject` text NOT NULL,
  `cc` text NOT NULL,
  `dcc` text NOT NULL,
  `bcc` text NOT NULL,
  `dbcc` text NOT NULL,
  `fromname` text NOT NULL,
  `dfromname` text NOT NULL,
  `fromemail` text NOT NULL,
  `dfromemail` text NOT NULL,
  `replytoname` text NOT NULL,
  `dreplytoname` text NOT NULL,
  `replytoemail` text NOT NULL,
  `dreplytoemail` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `params` longtext NOT NULL,
  `template` longtext NOT NULL,
  PRIMARY KEY (`emailid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `jos_chrono_contact_emails`
--

INSERT INTO `jos_chrono_contact_emails` (`emailid`, `formid`, `to`, `dto`, `subject`, `dsubject`, `cc`, `dcc`, `bcc`, `dbcc`, `fromname`, `dfromname`, `fromemail`, `dfromemail`, `replytoname`, `dreplytoname`, `replytoemail`, `dreplytoemail`, `enabled`, `params`, `template`) VALUES
(3, 1, 'peter.inyang@gmail.com', '', 'Prayer Request from website', '', '', '', '', '', '', 'name', '', 'email', '', '', '', '', 1, 'recordip=1\nemailtype=html\nenabled=1\neditor=1\nenable_attachments=1', '<p>Hello,</p>\r\n<p>This is a notification email for a newly added prayer request from the youth website.</p>\r\n<p>Details</p>\r\n<p>Name: {name}</p>\r\n<p>Email: {email}</p>\r\n<p>Please follow up immediately.</p>\r\n<p>System Administrator.</p>\r\n<p>***</p>\r\n<p>This email has been auto-generated from the youth website. If you believe you have recieved this email in error, please send an email immediately to youth@apostolicfaith.org.uk</p>'),
(4, 2, 'peter.inyang@gmail.com', '', 'Contact from youth website', '', '', '', '', '', '', 'fname', '', 'email', '', '', '', '', 1, 'recordip=1\nemailtype=html\nenabled=1\neditor=1\nenable_attachments=1', '<p><strong>First Name *</strong> <br /> {fname}<br /> <strong>Last Name</strong> <br /> {lname}<br /> <strong>Email *</strong> <br /> {email}<br /> <strong>Phone Number</strong> <br /> {phone}<br /> <strong>Reason for contact</strong> <br /> <strong>Please select one *</strong> <br /> {radio0}<br /> <strong>Comments *</strong> <br /> {comments}</p>');

-- --------------------------------------------------------

--
-- Table structure for table `jos_chrono_contact_plugins`
--

DROP TABLE IF EXISTS `jos_chrono_contact_plugins`;
CREATE TABLE IF NOT EXISTS `jos_chrono_contact_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `event` varchar(255) NOT NULL,
  `params` longtext NOT NULL,
  `extra1` longtext NOT NULL,
  `extra2` longtext NOT NULL,
  `extra3` longtext NOT NULL,
  `extra4` longtext NOT NULL,
  `extra5` longtext NOT NULL,
  `extra6` longtext NOT NULL,
  `extra7` longtext NOT NULL,
  `extra8` longtext NOT NULL,
  `extra9` longtext NOT NULL,
  `extra10` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_components`
--

DROP TABLE IF EXISTS `jos_components`;
CREATE TABLE IF NOT EXISTS `jos_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_menu_link` varchar(255) NOT NULL DEFAULT '',
  `admin_menu_alt` varchar(255) NOT NULL DEFAULT '',
  `option` varchar(50) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `admin_menu_img` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_option` (`parent`,`option`(32))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=87 ;

--
-- Dumping data for table `jos_components`
--

INSERT INTO `jos_components` (`id`, `name`, `link`, `menuid`, `parent`, `admin_menu_link`, `admin_menu_alt`, `option`, `ordering`, `admin_menu_img`, `iscore`, `params`, `enabled`) VALUES
(1, 'Banners', '', 0, 0, '', 'Banner Management', 'com_banners', 0, 'js/ThemeOffice/component.png', 0, 'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n', 1),
(2, 'Banners', '', 0, 1, 'option=com_banners', 'Active Banners', 'com_banners', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(3, 'Clients', '', 0, 1, 'option=com_banners&c=client', 'Manage Clients', 'com_banners', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(4, 'Web Links', 'option=com_weblinks', 0, 0, '', 'Manage Weblinks', 'com_weblinks', 0, 'js/ThemeOffice/component.png', 0, 'show_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 1),
(5, 'Links', '', 0, 4, 'option=com_weblinks', 'View existing weblinks', 'com_weblinks', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(6, 'Categories', '', 0, 4, 'option=com_categories&section=com_weblinks', 'Manage weblink categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(7, 'Contacts', 'option=com_contact', 0, 0, '', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/component.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(8, 'Contacts', '', 0, 7, 'option=com_contact', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/edit.png', 1, '', 1),
(9, 'Categories', '', 0, 7, 'option=com_categories&section=com_contact_details', 'Manage contact categories', '', 2, 'js/ThemeOffice/categories.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(10, 'Polls', 'option=com_poll', 0, 0, 'option=com_poll', 'Manage Polls', 'com_poll', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(11, 'News Feeds', 'option=com_newsfeeds', 0, 0, '', 'News Feeds Management', 'com_newsfeeds', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(12, 'Feeds', '', 0, 11, 'option=com_newsfeeds', 'Manage News Feeds', 'com_newsfeeds', 1, 'js/ThemeOffice/edit.png', 0, 'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 1),
(13, 'Categories', '', 0, 11, 'option=com_categories&section=com_newsfeeds', 'Manage Categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(14, 'User', 'option=com_user', 0, 0, '', '', 'com_user', 0, '', 1, '', 1),
(15, 'Search', 'option=com_search', 0, 0, 'option=com_search', 'Search Statistics', 'com_search', 0, 'js/ThemeOffice/component.png', 1, 'enabled=0\n\n', 1),
(16, 'Categories', '', 0, 1, 'option=com_categories&section=com_banner', 'Categories', '', 3, '', 1, '', 1),
(17, 'Wrapper', 'option=com_wrapper', 0, 0, '', 'Wrapper', 'com_wrapper', 0, '', 1, '', 1),
(18, 'Mail To', '', 0, 0, '', '', 'com_mailto', 0, '', 1, '', 1),
(19, 'Media Manager', '', 0, 0, 'option=com_media', 'Media Manager', 'com_media', 0, '', 1, 'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\nimage_path=images/stories\nrestrict_uploads=1\nallowed_media_usergroup=3\ncheck_mime=1\nimage_extensions=bmp,gif,jpg,png\nignore_extensions=\nupload_mime=image/jpeg,image/gif,image/png,image/bmp,application/x-shockwave-flash,application/msword,application/excel,application/pdf,application/powerpoint,text/plain,application/x-zip\nupload_mime_illegal=text/html\nenable_flash=0\n\n', 1),
(20, 'Articles', 'option=com_content', 0, 0, '', '', 'com_content', 0, '', 1, 'show_noauth=1\nshow_title=1\nlink_titles=1\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nshow_hits=1\nfeed_summary=0\nfilter_tags=\nfilter_attritbutes=\n\n', 1),
(21, 'Configuration Manager', '', 0, 0, '', 'Configuration', 'com_config', 0, '', 1, '', 1),
(22, 'Installation Manager', '', 0, 0, '', 'Installer', 'com_installer', 0, '', 1, '', 1),
(23, 'Language Manager', '', 0, 0, '', 'Languages', 'com_languages', 0, '', 1, '', 1),
(24, 'Mass mail', '', 0, 0, '', 'Mass Mail', 'com_massmail', 0, '', 1, 'mailSubjectPrefix=\nmailBodySuffix=\n\n', 1),
(25, 'Menu Editor', '', 0, 0, '', 'Menu Editor', 'com_menus', 0, '', 1, '', 1),
(27, 'Messaging', '', 0, 0, '', 'Messages', 'com_messages', 0, '', 1, '', 1),
(28, 'Modules Manager', '', 0, 0, '', 'Modules', 'com_modules', 0, '', 1, '', 1),
(29, 'Plugin Manager', '', 0, 0, '', 'Plugins', 'com_plugins', 0, '', 1, '', 1),
(30, 'Template Manager', '', 0, 0, '', 'Templates', 'com_templates', 0, '', 1, '', 1),
(31, 'User Manager', '', 0, 0, '', 'Users', 'com_users', 0, '', 1, 'allowUserRegistration=0\nnew_usertype=Registered\nuseractivation=1\nfrontend_userparams=1\n\n', 1),
(32, 'Cache Manager', '', 0, 0, '', 'Cache', 'com_cache', 0, '', 1, '', 1),
(33, 'Control Panel', '', 0, 0, '', 'Control Panel', 'com_cpanel', 0, '', 1, '', 1),
(34, 'RokCandyBundle', '', 0, 0, '', 'RokCandyBundle', 'com_rokcandybundle', 0, '', 0, '', 0),
(35, 'RokCandy', '', 0, 0, 'option=com_rokcandy', 'RokCandy', 'com_rokcandy', 0, 'components/com_rokcandy/assets/rokcandy-icon-16.png', 1, '', 1),
(36, 'Macros', '', 0, 35, 'option=com_rokcandy', 'Macros', 'com_rokcandy', 0, 'images/blank.png', 0, '', 1),
(37, 'Categories', '', 0, 35, 'option=com_categories&section=com_rokcandy', 'Categories', 'com_rokcandy', 1, 'images/blank.png', 0, '', 1),
(38, 'RokModule', '', 0, 0, '', 'RokModule', 'com_rokmodule', 0, '', 0, '', 1),
(39, 'RokNavMenuBundle', '', 0, 0, '', 'RokNavMenuBundle', 'com_roknavmenubundle', 0, '', 0, '', 1),
(42, 'Gantry', '', 0, 0, '', 'Gantry', 'com_gantry', 0, '', 0, '', 1),
(43, 'K2', 'option=com_k2', 0, 0, 'option=com_k2', 'K2', 'com_k2', 0, 'components/com_k2/images/system/k2-icon.png', 0, 'enable_css=1\nimagesQuality=100\nitemImageXS=100\nitemImageS=200\nitemImageM=400\nitemImageL=600\nitemImageXL=900\nitemImageGeneric=300\ncatImageWidth=100\ncatImageDefault=1\nuserImageWidth=100\nuserImageDefault=1\ncommenterImgWidth=48\nuserName=1\nuserImage=1\nuserDescription=1\nuserURL=1\nuserEmail=1\nuserFeed=1\nuserItemCount=10\nuserItemTitle=1\nuserItemTitleLinked=1\nuserItemDateCreated=1\nuserItemImage=1\nuserItemIntroText=1\nuserItemCategory=1\nuserItemTags=1\nuserItemCommentsAnchor=1\nuserItemReadMore=1\nuserItemK2Plugins=1\ngenericItemCount=10\ngenericItemTitle=1\ngenericItemTitleLinked=1\ngenericItemDateCreated=1\ngenericItemImage=1\ngenericItemIntroText=1\ngenericItemCategory=1\ngenericItemReadMore=1\ncomments=1\ncommentsOrdering=DESC\ncommentsLimit=10\ncommentsFormPosition=below\ncommentsPublishing=1\ngravatar=1\ntinyURL=1\nfeedLimit=10\nfeedItemImage=1\nfeedImgSize=S\nfeedItemIntroText=1\nfeedItemFullText=1\nlinkPopupWidth=900\nlinkPopupHeight=600\nfrontendEditing=1\nshowImageTab=1\nshowImageGalleryTab=1\nshowVideoTab=1\nshowExtraFieldsTab=1\nshowAttachmentsTab=1\nshowK2Plugins=1\nmergeEditors=1\nsideBarDisplay=1\ntaggingSystem=1\ngoogleSearchContainer=k2Container\nK2UserProfile=1\nadminSearch=simple\nshowItemsCounterAdmin=1\nshowChildCatItems=1\nsh404SefLabelUser=blog\n', 1),
(44, 'MorfeoShow', 'option=com_morfeoshow', 0, 0, 'option=com_morfeoshow', 'MorfeoShow', 'com_morfeoshow', 0, 'components/com_morfeoshow/images/tool.png', 0, '', 1),
(45, 'Galleries', '', 0, 44, 'option=com_morfeoshow', 'Galleries', 'com_morfeoshow', 0, 'components/com_morfeoshow/images/gall.png', 0, '', 1),
(46, 'Configuration', '', 0, 44, 'option=com_morfeoshow&task=showSettings', 'Configuration', 'com_morfeoshow', 1, 'components/com_morfeoshow/images/conf.png', 0, '', 1),
(47, 'Info', '', 0, 44, 'option=com_morfeoshow&task=info', 'Info', 'com_morfeoshow', 2, 'components/com_morfeoshow/images/infor.png', 0, '', 1),
(49, 'Chrono Forms', 'option=com_chronocontact', 0, 0, 'option=com_chronocontact', 'Chrono Forms', 'com_chronocontact', 0, 'components/com_chronocontact/CF.png', 0, '', 1),
(50, 'Forms Management', '', 0, 49, 'option=com_chronocontact&act=all', 'Forms Management', 'com_chronocontact', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(51, 'Form Wizard', '', 0, 49, 'option=com_chronocontact&task=form_wizard', 'Form Wizard', 'com_chronocontact', 1, 'js/ThemeOffice/component.png', 0, '', 1),
(52, 'Wizard Custom Elements', '', 0, 49, 'option=com_chronocontact&task=wizard_elements', 'Wizard Custom Elements', 'com_chronocontact', 2, 'js/ThemeOffice/component.png', 0, '', 1),
(53, 'Menu Creator', '', 0, 49, 'option=com_chronocontact&task=menu_creator', 'Menu Creator', 'com_chronocontact', 3, 'js/ThemeOffice/component.png', 0, '', 1),
(54, 'Menu Remover', '', 0, 49, 'option=com_chronocontact&task=menu_remover', 'Menu Remover', 'com_chronocontact', 4, 'js/ThemeOffice/component.png', 0, '', 1),
(55, 'Upgrade SQL and Load Demo Form', '', 0, 49, 'option=com_chronocontact&task=doupgrade', 'Upgrade SQL and Load Demo Form', 'com_chronocontact', 5, 'js/ThemeOffice/component.png', 0, '', 1),
(56, 'Validate Installation', '', 0, 49, 'option=com_chronocontact&task=validatelicense', 'Validate Installation', 'com_chronocontact', 6, 'js/ThemeOffice/component.png', 0, '', 1),
(57, 'JCE', 'option=com_jce', 0, 0, 'option=com_jce', 'JCE', 'com_jce', 0, 'components/com_jce/img/logo.png', 0, '', 1),
(58, 'JCE MENU CPANEL', '', 0, 57, 'option=com_jce', 'JCE MENU CPANEL', 'com_jce', 0, 'templates/khepri/images/menu/icon-16-cpanel.png', 0, '', 1),
(59, 'JCE MENU CONFIG', '', 0, 57, 'option=com_jce&type=config', 'JCE MENU CONFIG', 'com_jce', 1, 'templates/khepri/images/menu/icon-16-config.png', 0, '', 1),
(60, 'JCE MENU GROUPS', '', 0, 57, 'option=com_jce&type=group', 'JCE MENU GROUPS', 'com_jce', 2, 'templates/khepri/images/menu/icon-16-user.png', 0, '', 1),
(61, 'JCE MENU PLUGINS', '', 0, 57, 'option=com_jce&type=plugin', 'JCE MENU PLUGINS', 'com_jce', 3, 'templates/khepri/images/menu/icon-16-plugin.png', 0, '', 1),
(62, 'JCE MENU INSTALL', '', 0, 57, 'option=com_jce&type=install', 'JCE MENU INSTALL', 'com_jce', 4, 'templates/khepri/images/menu/icon-16-install.png', 0, '', 1),
(63, 'Gavick Tabs Manager GK3', 'option=com_gk3_tabs_manager', 0, 0, 'option=com_gk3_tabs_manager', 'Gavick Tabs Manager GK3', 'com_gk3_tabs_manager', 0, 'components/com_gk3_tabs_manager/interface/images/com_logo_gk3.png', 0, '', 1),
(64, 'uddeIM', 'option=com_uddeim', 0, 0, 'option=com_uddeim', 'uddeIM', 'com_uddeim', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(65, 'EventList', 'option=com_eventlist', 0, 0, 'option=com_eventlist', 'EventList', 'com_eventlist', 0, '../administrator/components/com_eventlist/assets/images/eventlist.png', 0, 'display_num=15\ncat_num=4\nfilter=1\ndisplay=1\nicons=1\nshow_print_icon=1\nshow_email_icon=1\n', 1),
(76, 'Phoca Gallery', 'option=com_phocagallery', 0, 0, 'option=com_phocagallery', 'Phoca Gallery', 'com_phocagallery', 0, 'components/com_phocagallery/assets/images/icon-16-pg-menu.png', 0, 'categories_columns=1\nequal_percentage_width=1\ndisplay_image_categories=1\ncategories_box_width=33%\nimage_categories_size=4\ncategories_image_ordering=9\ncategories_display_avatar=0\ndisplay_subcategories=1\ndisplay_empty_categories=0\nhide_categories=\ndisplay_access_category=0\ndefault_pagination_categories=0\npagination_categories=5;10;15;20;50\nfont_color=#b36b00\nbackground_color=#fcfcfc\nbackground_color_hover=#f5f5f5\nimage_background_color=#f5f5f5\nimage_background_shadow=shadow1\nborder_color=#e8e8e8\nborder_color_hover=#b36b00\nmargin_box=5\npadding_box=5\ndisplay_name=1\ndisplay_icon_detail=1\ndisplay_icon_download=2\ndisplay_icon_folder=0\nfont_size_name=12\nchar_length_name=15\ncategory_box_space=0\ndisplay_categories_sub=0\ndisplay_subcat_page=0\ndisplay_category_icon_image=0\ncategory_image_ordering=9\ndisplay_back_button=1\ndisplay_categories_back_button=1\ndefault_pagination_category=20\npagination_category=5;10;15;20;50\ndisplay_img_desc_box=0\nfont_size_img_desc=10\nimg_desc_box_height=30\nchar_length_img_desc=300\ndisplay_categories_cv=0\ndisplay_subcat_page_cv=0\ndisplay_category_icon_image_cv=0\ncategory_image_ordering_cv=9\ndisplay_back_button_cv=1\ndisplay_categories_back_button_cv=1\ncategories_columns_cv=1\ndisplay_image_categories_cv=1\nimage_categories_size_cv=4\ndetail_window=0\ndetail_window_background_color=#ffffff\nmodal_box_overlay_color=#000000\nmodal_box_overlay_opacity=0.3\nmodal_box_border_color=#6b6b6b\nmodal_box_border_width=2\nsb_slideshow_delay=5\nsb_lang=en\nhighslide_class=rounded-white\nhighslide_opacity=0\nhighslide_outline_type=rounded-white\nhighslide_fullimg=0\nhighslide_close_button=0\nhighslide_slideshow=1\njak_slideshow_delay=5\njak_orientation=none\njak_description=1\njak_description_height=10\ndisplay_description_detail=0\ndisplay_title_description=0\nfont_size_desc=11\nfont_color_desc=#333333\ndescription_detail_height=16\ndescription_lightbox_font_size=12\ndescription_lightbox_font_color=#ffffff\ndescription_lightbox_bg_color=#000000\nslideshow_delay=3000\nslideshow_pause=0\nslideshow_random=0\ndetail_buttons=1\nphocagallery_width=\nphocagallery_center=0\ncategory_ordering=1\nimage_ordering=1\ngallery_metadesc=\ngallery_metakey=\nenable_user_cp=0\nenable_upload_avatar=1\nenable_avatar_approve=0\nenable_usercat_approve=0\nenable_usersubcat_approve=0\nuser_subcat_count=5\nmax_create_cat_char=1000\nenable_userimage_approve=0\nmax_upload_char=1000\nupload_maxsize=3145728\nupload_maxres_width=3072\nupload_maxres_height=2304\nuser_images_max_size=20971520\nenable_java=0\nenable_java_admin=1\njava_resize_width=-1\njava_resize_height=-1\njava_box_width=480\njava_box_height=480\ndisplay_rating=0\ndisplay_rating_img=0\ndisplay_comment=0\ndisplay_comment_img=1\ncomment_width=500\nmax_comment_char=1000\nexternal_comment_system=0\nenable_piclens=0\nstart_piclens=0\npiclens_image=1\nswitch_image=0\nswitch_width=640\nswitch_height=480\nenable_overlib=0\nol_bg_color=#666666\nol_fg_color=#f6f6f6\nol_tf_color=#000000\nol_cf_color=#ffffff\noverlib_overlay_opacity=0.7\noverlib_image_rate=\ncreate_watermark=0\nwatermark_position_x=center\nwatermark_position_y=middle\ndisplay_icon_vm=0\ndisplay_category_statistics=0\ndisplay_main_cat_stat=1\ndisplay_lastadded_cat_stat=1\ncount_lastadded_cat_stat=3\ndisplay_mostviewed_cat_stat=1\ncount_mostviewed_cat_stat=3\ndisplay_camera_info=0\nexif_information=FILE.FileName;FILE.FileDateTime;FILE.FileSize;FILE.MimeType;COMPUTED.Height;COMPUTED.Width;COMPUTED.IsColor;COMPUTED.ApertureFNumber;IFD0.Make;IFD0.Model;IFD0.Orientation;IFD0.XResolution;IFD0.YResolution;IFD0.ResolutionUnit;IFD0.Software;IFD0.DateTime;IFD0.Exif_IFD_Pointer;IFD0.GPS_IFD_Pointer;EXIF.ExposureTime;EXIF.FNumber;EXIF.ExposureProgram;EXIF.ISOSpeedRatings;EXIF.ExifVersion;EXIF.DateTimeOriginal;EXIF.DateTimeDigitized;EXIF.ShutterSpeedValue;EXIF.ApertureValue;EXIF.ExposureBiasValue;EXIF.MaxApertureValue;EXIF.MeteringMode;EXIF.LightSource;EXIF.Flash;EXIF.FocalLength;EXIF.SubSecTimeOriginal;EXIF.SubSecTimeDigitized;EXIF.ColorSpace;EXIF.ExifImageWidth;EXIF.ExifImageLength;EXIF.SensingMethod;EXIF.CustomRendered;EXIF.ExposureMode;EXIF.WhiteBalance;EXIF.DigitalZoomRatio;EXIF.FocalLengthIn35mmFilm;EXIF.SceneCaptureType;EXIF.GainControl;EXIF.Contrast;EXIF.Saturation;EXIF.Sharpness;EXIF.SubjectDistanceRange;GPS.GPSLatitudeRef;GPS.GPSLatitude;GPS.GPSLongitudeRef;GPS.GPSLongitude;GPS.GPSAltitudeRef;GPS.GPSAltitude;GPS.GPSTimeStamp;GPS.GPSStatus;GPS.GPSMapDatum;GPS.GPSDateStamp\ndisplay_categories_geotagging=0\ncategories_lng=\ncategories_lat=\ncategories_zoom=2\ncategories_map_width=\ncategories_map_height=500\ndisplay_icon_geotagging=0\ndisplay_category_geotagging=0\ncategory_map_width=\ncategory_map_height=400\npagination_thumbnail_creation=0\nclean_thumbnails=0\nenable_thumb_creation=1\ncrop_thumbnail=5\njpeg_quality=85\nenable_picasa_loading=1\npicasa_load_pagination=20\nicon_format=gif\nlarge_image_width=640\nlarge_image_height=480\nmedium_image_width=100\nmedium_image_height=100\nsmall_image_width=50\nsmall_image_height=50\nfront_modal_box_width=680\nfront_modal_box_height=560\nadmin_modal_box_width=680\nadmin_modal_box_height=520\nfolder_permissions=0755\njfile_thumbs=1\n\n', 1),
(77, 'Control Panel', '', 0, 76, 'option=com_phocagallery', 'Control Panel', 'com_phocagallery', 0, 'components/com_phocagallery/assets/images/icon-16-pg-control-panel.png', 0, '', 1),
(78, 'Images', '', 0, 76, 'option=com_phocagallery&view=phocagallerys', 'Images', 'com_phocagallery', 1, 'components/com_phocagallery/assets/images/icon-16-pg-menu-gal.png', 0, '', 1),
(79, 'Categories', '', 0, 76, 'option=com_phocagallery&view=phocagallerycs', 'Categories', 'com_phocagallery', 2, 'components/com_phocagallery/assets/images/icon-16-pg-menu-cat.png', 0, '', 1),
(80, 'Themes', '', 0, 76, 'option=com_phocagallery&view=phocagalleryt', 'Themes', 'com_phocagallery', 3, 'components/com_phocagallery/assets/images/icon-16-pg-menu-theme.png', 0, '', 1),
(81, 'Category Rating', '', 0, 76, 'option=com_phocagallery&view=phocagalleryra', 'Category Rating', 'com_phocagallery', 4, 'components/com_phocagallery/assets/images/icon-16-pg-menu-vote.png', 0, '', 1),
(82, 'Image Rating', '', 0, 76, 'option=com_phocagallery&view=phocagalleryraimg', 'Image Rating', 'com_phocagallery', 5, 'components/com_phocagallery/assets/images/icon-16-pg-menu-vote-img.png', 0, '', 1),
(83, 'Category Comments', '', 0, 76, 'option=com_phocagallery&view=phocagallerycos', 'Category Comments', 'com_phocagallery', 6, 'components/com_phocagallery/assets/images/icon-16-pg-menu-comment.png', 0, '', 1),
(84, 'Image Comments', '', 0, 76, 'option=com_phocagallery&view=phocagallerycoimgs', 'Image Comments', 'com_phocagallery', 7, 'components/com_phocagallery/assets/images/icon-16-pg-menu-comment-img.png', 0, '', 1),
(85, 'Users', '', 0, 76, 'option=com_phocagallery&view=phocagalleryusers', 'Users', 'com_phocagallery', 8, 'components/com_phocagallery/assets/images/icon-16-pg-menu-users.png', 0, '', 1),
(86, 'Info', '', 0, 76, 'option=com_phocagallery&view=phocagalleryin', 'Info', 'com_phocagallery', 9, 'components/com_phocagallery/assets/images/icon-16-pg-menu-info.png', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_contact_details`
--

DROP TABLE IF EXISTS `jos_contact_details`;
CREATE TABLE IF NOT EXISTS `jos_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_contact_details`
--

INSERT INTO `jos_contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `imagepos`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`) VALUES
(1, 'Name', 'name', 'Position', 'Street', 'Suburb', 'State', 'Country', 'Zip Code', 'Telephone', 'Fax', 'Miscellanous info', 'powered_by.png', 'top', 'email@email.com', 0, 1, 0, '0000-00-00 00:00:00', 1, 'show_name=1\nshow_position=1\nshow_email=1\nshow_street_address=1\nshow_suburb=1\nshow_state=1\nshow_postcode=1\nshow_country=1\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nshow_webpage=1\nshow_misc=1\nshow_image=1\nallow_vcard=1\ncontact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_mobile=\nicon_fax=\nicon_misc=\nshow_email_form=1\nemail_description=1\nshow_email_copy=1\nbanned_email=\nbanned_subject=\nbanned_text=', 62, 12, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_content`
--

DROP TABLE IF EXISTS `jos_content`;
CREATE TABLE IF NOT EXISTS `jos_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `title_alias` varchar(255) NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(11) unsigned NOT NULL DEFAULT '0',
  `mask` int(11) unsigned NOT NULL DEFAULT '0',
  `catid` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_section` (`sectionid`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `jos_content`
--

INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(10, 'Send in your Prayer Requests', 'send-in-your-requests', '', 'Send us your prayer requests so we can join you in prayers<br />', '', 1, 0, 0, 0, '2008-07-30 14:06:37', 62, '', '2010-03-15 16:29:50', 62, 0, '0000-00-00 00:00:00', '2006-09-29 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 15, '', 'Send in your prayer requests - The Apostolic Faith Church', 0, 11, 'robots=\nauthor='),
(19, 'Contact Us', 'contact-us', '', '<p>Please use the form below to contact us:</p>\r\n<ul>\r\n<li>Call for Advice (Prayer Line)/General Enquiries: +44 (0)207 639 8897</li>\r\n</ul>\r\n<p>If none of the above are applicable, please submit Administrative Queries and Comments below. Please bear in mind that we may not be able to reply to all queries.</p>\r\n<p>{chronocontact}contactform{/chronocontact}</p>', '', 1, 0, 0, 0, '2008-08-09 07:49:20', 62, '', '2010-03-19 17:14:47', 62, 0, '0000-00-00 00:00:00', '2006-10-07 10:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 16, 0, 2, '', 'Contact Us', 0, 205, 'robots=\nauthor='),
(46, 'Get Inspired', 'get-inspired', '', '<h1>Reflections</h1>\r\n<p><img src="images/stories/calendar_content.jpg" border="0" alt="The Youth Programme Calendar" width="124" height="55" style="float: left; border: 1px solid black; margin-left: 5px; margin-right: 5px;" /></p>\r\n<p> </p>\r\n<p> </p>\r\n<p> </p>\r\n<hr />\r\n<h1>Testimonies</h1>\r\n<p><img src="images/stories/youth_camp.jpg" border="0" alt="Youth Camp 2010" width="132" height="58" style="float: left; border: 1px solid black; margin-left: 5px; margin-right: 5px;" />The Youth camp 2010 will be holding this year at YMCA, Isle of Wight. To register, you can send an email directly to youth@apostolicfaith.org.uk or register online.</p>\r\n<p> </p>\r\n<hr />\r\n<h1>Request Prayer</h1>\r\n<p><img src="images/stories/ist1_9577442-happy-new-year-2010.jpg" border="0" alt="Recent Event Reports" width="133" height="72" style="border: 1px solid black; margin-left: 5px; margin-right: 5px; float: left;" />Get an overview of our programmes coming up in the next few months.  Download you own copy of the youth calendar for year 2010 and make a  date with us as we celebrate Jesus Christ together.</p>', '', 1, 0, 0, 0, '2010-02-21 14:06:32', 62, '', '2010-03-15 15:46:17', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:06:32', '0000-00-00 00:00:00', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 29, 0, 26, '', 'What''s New at The Apostolic Faith Church?', 0, 194, 'robots=\nauthor='),
(48, 'Get Connected', 'get-conected', '', '<p>You are invited to the 2010 Youth Camp holding this year at the YMCA, Winchester in the Isle of Wight.</p>', '', 1, 0, 0, 0, '2010-02-21 14:07:13', 62, '', '2010-03-15 15:38:08', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:07:13', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 41, 0, 14, '', '', 0, 136, 'robots=\nauthor='),
(76, 'What''s New?', 'whatsnew', '', '<h1>Latest Events</h1>\r\n<p><img src="images/stories/calendar_content.jpg" border="0" alt="The Youth Programme Calendar" width="124" height="55" style="float: left; border: 1px solid black; margin-left: 5px; margin-right: 5px;" />Get an overview of our programmes coming up in the next few months. Download you own copy of the youth calendar for year 2010 and make a date with us as we celebrate Jesus Christ together.</p>\r\n<p> </p>\r\n<hr />\r\n<h1>Next Event</h1>\r\n<p><img src="images/stories/youth_camp.jpg" border="0" alt="Youth Camp 2010" width="132" height="58" style="float: left; border: 1px solid black; margin-left: 5px; margin-right: 5px;" />The Youth camp 2010 will be holding this year at YMCA, Isle of Wight. To register, you can send an email directly to youth@apostolicfaith.org.uk or register online.</p>\r\n<p> </p>\r\n<hr />\r\n<h1>Reports from recent events</h1>\r\n<p><img src="images/stories/ist1_9577442-happy-new-year-2010.jpg" border="0" alt="Recent Event Reports" width="133" height="72" style="border: 1px solid black; margin-left: 5px; margin-right: 5px; float: left;" />Get an overview of our programmes coming up in the next few months.  Download you own copy of the youth calendar for year 2010 and make a  date with us as we celebrate Jesus Christ together.</p>', '', 1, 0, 0, 0, '2010-02-21 14:06:32', 62, '', '2010-03-15 14:02:09', 62, 62, '2010-07-20 22:46:02', '2010-02-21 14:06:32', '0000-00-00 00:00:00', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 0, 0, 27, '', 'What''s New at The Apostolic Faith Church?', 0, 0, 'robots=\nauthor='),
(68, 'Newsletter', 'newsletter', '', '<div class="demo-overlay">\r\n<div class="demo-top-overlay">\r\n<div class="demo-bottom-overlay"><img src="images/stories/demo/frontpage/tab3.jpg" alt="Mar10 Demo Image" /></div>\r\n</div>\r\n</div>\r\n<p class="rt-desc1">Sign up for automatic updates via email</p>\r\n<p class="rt-desc2">You can sign up right now to recieve updates via email</p>\r\n[readon url="index.php?option=com_content&amp;view=article&amp;id=49&amp;Itemid=56"]Preview Variations[/readon]', '', 1, 0, 0, 0, '2010-02-25 18:00:05', 62, '', '2010-03-16 21:08:57', 62, 0, '0000-00-00 00:00:00', '2010-02-25 18:00:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 16, '', '', 0, 0, 'robots=\nauthor='),
(63, 'Other Featured Stuff Continued', 'other-featured-stuff-continued', '', '<div class="demo-mb"><img src="images/stories/demo/frontpage/mb2.jpg" alt="Mar10 Demo Image" class="demo-fp-img img-left" />\r\n<p>Appraisal of the year to date...</p>\r\n<p>This placeholder holds news and updates for site users.</p>\r\n[readon2 url="index.php?option=com_content&amp;view=article&amp;id=47&amp;Itemid=54"]Learn More[/readon2]</div>', '', 1, 0, 0, 0, '2010-02-25 15:08:25', 62, '', '2010-03-16 20:37:51', 62, 0, '0000-00-00 00:00:00', '2010-02-25 15:08:25', '0000-00-00 00:00:00', '', '', 'show_title=0\nlink_titles=0\nshow_intro=\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_vote=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nlanguage=\nkeyref=\nreadmore=', 10, 0, 8, '', '', 0, 8, 'robots=\nauthor='),
(64, 'Other Featured Stuff Continued...', 'other-featured-stuff-continued', '', '<div class="demo-mb"><img src="images/stories/demo/frontpage/mb3.jpg" alt="Mar10 Demo Image" class="demo-fp-img img-left" />\r\n<p>This placeholder needs content. Other items the frontpage go in here..</p>\r\n[readon2 url="index.php?option=com_content&amp;view=article&amp;id=49&amp;Itemid=56"]More Details[/readon2]</div>', '', 1, 0, 0, 0, '2010-02-25 15:08:25', 62, '', '2010-03-16 20:37:37', 62, 0, '0000-00-00 00:00:00', '2010-02-25 15:08:25', '0000-00-00 00:00:00', '', '', 'show_title=0\nlink_titles=0\nshow_intro=\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_vote=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nlanguage=\nkeyref=\nreadmore=', 9, 0, 9, '', '', 0, 6, 'robots=\nauthor='),
(65, 'Features', 'features', '', '<div class="demo-mb-btm demo-mb-btm-first">\r\n<p><img src="images/stories/demo/frontpage/cb1.jpg" alt="Mar10 Demo Image" class="demo-fp-img" /></p>\r\n<p>Write for us</p>\r\n[readon2 url="index.php?option=com_content&amp;view=article&amp;id=50&amp;Itemid=57"]Find out more[/readon2]</div>\r\n<div class="demo-mb-btm">\r\n<p><img src="images/stories/demo/frontpage/cb2.jpg" alt="Mar10 Demo Image" class="demo-fp-img" /></p>\r\n<p>Send us your testimonies</p>\r\n[readon2 url="index.php?option=com_content&amp;view=article&amp;id=54&amp;Itemid=61"]Get more information[/readon2]</div>\r\n<div class="demo-mb-btm">\r\n<p><img src="images/stories/demo/frontpage/cb3.jpg" alt="Mar10 Demo Image" class="demo-fp-img" /></p>\r\n<p>Send in your suggestions</p>\r\n[readon2 url="index.php?option=com_content&amp;view=article&amp;id=60&amp;Itemid=67"]Find out more[/readon2]</div>\r\n<div class="clear"></div>', '', 1, 0, 0, 0, '2010-02-25 15:08:25', 62, '', '2010-03-16 20:52:05', 62, 0, '0000-00-00 00:00:00', '2010-02-25 15:08:25', '0000-00-00 00:00:00', '', '', 'show_title=0\nlink_titles=0\nshow_intro=\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_vote=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nlanguage=\nkeyref=\nreadmore=', 16, 0, 23, '', '', 0, 11, 'robots=\nauthor='),
(67, 'Welcome', 'welcome', '', '<div class="demo-overlay">\r\n<div class="demo-top-overlay">\r\n<div class="demo-bottom-overlay"><img alt="The Apostolic Faith Church (UK) Youths" src="images/stories/demo/frontpage/tab1.jpg" /></div>\r\n</div>\r\n</div>\r\n<p class="rt-desc1">Welcome to The Apostolic Faith Church, UK.</p>\r\n<p class="rt-desc2">You will find inspiring <strong>personal testimonies</strong>, <strong>devotionals </strong>and <strong>reflections </strong>from young people in all walks of life, who are spreading the gospel daily to win more souls for christ. We welcome and do strongly solicit your feedback. Tell us what you think of the new site.</p>\r\n[readon url="index.php?option=com_content&amp;view=article&amp;id=48&amp;Itemid=55"][/readon]&nbsp;&nbsp; [readon url="index.php?option=com_content&amp;view=article&amp;id=58&amp;Itemid=65"]Admin Controls[/readon] [readon url="index.php?option=com_content&amp;view=article&amp;id=58&amp;Itemid=65"]Send Feedback[/readon]', '', 1, 0, 0, 0, '2010-02-25 18:00:05', 62, '', '2010-03-17 16:02:16', 62, 0, '0000-00-00 00:00:00', '2010-02-25 18:00:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 21, 0, 21, 'Apostolic faith church, apostolic faith', 'Welcome to The Apostolic Faith Church, UK', 0, 1, 'robots=\nauthor='),
(50, 'Multimedia Showcase', 'media-showcase', '', 'Multimedia showcase from images to videos<br />', '', 1, 0, 0, 0, '2010-02-21 14:07:34', 62, '', '2010-03-15 16:39:32', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:07:34', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 25, '', '', 0, 46, 'robots=\nauthor='),
(52, 'Stop', 'stop', '', 'We are living in a fast world where almost everything gets done quickly.&nbsp; We finish our assignments to strict deadlines quickly;&nbsp; get to our workplace quickly;&nbsp; eat lunch quickly;&nbsp; and even gasp in the rush hour quickly.&nbsp; The Internet runs quickly but when it doesn', '', 1, 0, 0, 0, '2010-02-21 14:07:57', 62, '', '2010-03-19 16:46:57', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:07:57', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 13, 'The Apostolic Faith church, apostolic faith church, apostolic faith church uk', 'Reflections - The Apostolic Faith Church', 0, 55, 'robots=\nauthor='),
(54, 'Pray, Believing', 'pray-believing', '', '<p style="text-align: center;"><strong>Not So Tough...</strong></p>\r\n<h3 style="text-align: center;">', '', 1, 0, 0, 0, '2010-02-21 14:08:19', 62, '', '2010-03-16 09:52:47', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:08:19', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 22, 'apostolic faith, devotion', 'Pray Believing, a devotional from The Apostolic Faith Church youths', 0, 71, 'robots=\nauthor='),
(62, 'Other Featured Content', 'other-featured-content', '', '<div class="demo-mb"><img src="images/stories/demo/frontpage/mb1.jpg" alt="Mar10 Demo Image" class="demo-fp-img img-left" />\r\n<p>This placeholder holds other featured content such as written contributions from site users.</p>\r\n</div>', '', 1, 0, 0, 0, '2010-02-25 15:02:15', 62, '', '2010-03-16 20:39:05', 62, 0, '0000-00-00 00:00:00', '2010-02-25 15:02:15', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=0\nlink_section=\nshow_category=0\nlink_category=\nshow_vote=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nlanguage=\nkeyref=\nreadmore=', 19, 0, 7, '', '', 0, 8, 'robots=\nauthor='),
(55, 'Bible Challenge', 'biblechallenge', '', '1. Name the four Gospels.<img style="float: right;" src="images/stories/open_bible_afm.jpg" alt="Open Bible Challenge" /><br />2. What is the longest book in the Bible?<br /><br />3. Who wrote the Acts of the Apostles?<br /><br />4. Which book says, "The wages of sin is death"?<br /><br />5. How many people were on Noah''s Ark?<br /><br />6. What is the Golden Rule?<br /><br />7. Put in chronological order: Daniel, Noah, Paul, Moses and David.<br /><br />8. How many books are there in the Bible?<br /><br />9. Who were the first two people?<br /><br />10. Name the Ten Commandments.<br /><br />11. Angels are: ghosts of dead people, children who died in infancy, genies, or invisible beings created by God?<br /><br />12. Who was the brother of Jacob?<br /><br />13. Who wrote Psalm 23?<br /><br />14. Name three of Jesus'' parables.<br /><br />15. How many epistles were written by John?<br /><br />16. Was Luke: a fisherman, an apostle, a doctor or a shepherd?<br /><br />17. Which tribe did Moses belong to?<br /><br />18. Is Daniel in the Old or the New Testament?<br /><br />19. In what language was the New Testament written?<br /><br />20. Faith is: a leap into the dark, trust in what God says, believing in something you know isn''t true, or a religious feeling?<br /><br />21. Who said, "It is more blessed to give than to receive"?<br /><br />22. True or false: The apostles James and John were brothers.<br /><br />23. Who were the three patriarchs?<br /><br />24. Put in chronological order: Temple, Church, Tower of Babel, Tabernacle.<br /><br />25. What did Jesus say were the two greatest commandments?', '', 1, 0, 0, 0, '2010-02-21 14:08:31', 62, '', '2010-03-16 00:31:06', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:08:31', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 24, '', '', 0, 99, 'robots=\nauthor='),
(56, 'Hello, Im Kemi', 'kemis-testimony', '', '<h2><img style="float: left; margin-top: 0px; margin-right: 5px; border: 2px dashed #073054;" alt="Kemi''s Testimony" src="images/stories/kemi_small.jpg" /></h2>\r\n<h2>Hatred Gave Way for Salvation</h2>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Hatred Gave Way for Salvation</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;"></div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">I grew up in a Christian home, but that did not lead me to becoming a Christian. I decided that I wanted my own life; I would lie to my parents, and seek after the world, I would swear, fornicate, and deal in drugs and fraud. I would also drink to please friends around me and myself. I went too deep into sins because I thought that I was having a good life. I kept on doing things that would offend God and my parents but that did not stop my father from praying for me.</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;"></div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">About 4 or 5 years ago, I was pregnant and I did not know it. I woke up one morning with so much pain in my body but I still prepared to go out. However, I eventually collapsed on the stairs and my housemates had to call the ambulance to come and take me to the hospital. When I got to the hospital, the doctor said that a baby was developing outside my womb. They had to operate on me to take the baby out of me. I can look back now and confidently say that the Lord truly has a plan for my life.</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;"></div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Before the camp meeting, I had prayed everywhere for my salvation but to no avail. I prayed at home, at work and at church because I was desperate for peace in my troubled life. I did not know what was blocking me from praying through. I would pray until I could feel God’s presence but without being saved. One day, in the cabin on the campground, I shared my feelings with a sister and eventually discovered that I was finding it difficult to be saved because of the hatred I had for my mother and some other matters. That very day, I went to the altar and poured out my heart, mind, feelings and all to the Lord and He forgave me for hating my mother.</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;"></div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">The Lord gave me the joy of salvation in my heart. I feel so much better and I thank God for helping me to love my mother. I can boldly say that God is good. I thank the Lord that I was dead in sin but now I am alive to tell people that God is good. I would like to thank the Lord for opening my eyes to see the beauty of the Gospel of Jesus Christ. Salvation is good. Pray for my sanctification and the baptism of the Holy Ghost and fire.</div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;"></div>\r\n<div id="_mcePaste" style="position: absolute; left: -10000px; top: 19px; width: 1px; height: 1px; overflow-x: hidden; overflow-y: hidden;">Kemi is a member of the Apostolic Faith Mission London, UK</div>\r\n<div></div>\r\n<p>Hatred Gave Way for Salvation&nbsp;I grew up in a Christian home, but that did not lead me to becoming a Christian. I decided that I wanted my own life; I would lie to my parents, and seek after the world, I would swear, fornicate, and deal in drugs and fraud. I would also drink to please friends around me and myself.</p>\r\n<p>I went too deep into sins because I thought that I was having a good life. I kept on doing things that would offend God and my parents but that did not stop my father from praying for me.&nbsp;</p>\r\n<p>About 4 or 5 years ago, I was pregnant and I did not know it. I woke up one morning with so much pain in my body but I still prepared to go out. However, I eventually collapsed on the stairs and my housemates had to call the ambulance to come and take me to the hospital. When I got to the hospital, the doctor said that a baby was developing outside my womb. They had to operate on me to take the baby out of me. I can look back now and confidently say that the Lord truly has a plan for my life.&nbsp;</p>\r\n<p><br />Before the camp meeting, I had prayed everywhere for my salvation but to no avail. I prayed at home, at work and at church because I was desperate for peace in my troubled life. I did not know what was blocking me from praying through. I would pray until I could feel God’s presence but without being saved.</p>\r\n<p>One day, in the cabin on the campground, I shared my feelings with a sister and eventually discovered that I was finding it difficult to be saved because of the hatred I had for my mother and some other matters. <br />That very day, I went to the altar and poured out my heart, mind, feelings and all to the Lord and He forgave me for hating my mother.&nbsp;<br />The Lord gave me the joy of salvation in my heart. I feel so much better and I thank God for helping me to love my mother.</p>\r\n<p>I can boldly say that God is good. I thank the Lord that I was dead in sin but now I am alive to tell people that God is good. I would like to thank the Lord for opening my eyes to see the beauty of the Gospel of Jesus Christ.</p>\r\n<p>Salvation is good. Pray for my sanctification and the baptism of the Holy Ghost and fire.</p>\r\n<p>Kemi is a member of the Apostolic Faith Mission London, UK</p>', '', 1, 0, 0, 0, '2010-02-21 14:08:41', 62, '', '2010-03-19 17:31:06', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:08:41', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 11, 0, 11, '', 'Kemi''s Testimony - The Apostolic Faith Church', 0, 39, 'robots=\nauthor='),
(57, 'Word Search', 'wordsearch', '', '<img src="images/stories/000 wordsearch no answers.jpg" /><br />', '', 1, 0, 0, 0, '2010-02-21 14:08:53', 62, '', '2010-03-16 00:36:39', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:08:53', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 5, '', '', 0, 25, 'robots=\nauthor='),
(58, 'Accessibility Statement', 'accessibility-statement', '', '<div class="alert">\r\n<div class="typo-icon"><span style="color: #474747; font-family: Helvetica, Arial, FreeSans, sans-serif; line-height: 20px;">\r\n<p style="margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 12px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; background-position: initial initial; background-repeat: initial initial; padding: 0px; border: 0px initial initial;"><span style="border-collapse: collapse; color: #333333; font-family: arial, sans-serif; font-size: 14px; line-height: 18px;"> </span></p>\r\n<p style="margin-top: 0px; margin-right: 0px; margin-bottom: 13px; margin-left: 0px; border-collapse: collapse; font-family: arial, sans-serif; background-repeat: no-repeat no-repeat; padding: 0px;"><span style="border-collapse: separate; color: #333333; font-family: Frutiger, Arial, sans-serif; line-height: 22px; font-size: medium;">\r\n<p style="margin-top: 10px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">We have tried to make this website as accessible as possible and easy to use for everyone, regardless of circumstance or ability.</p>\r\n<p style="margin-top: 10px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">All our page templates comply with the WCAG Priority 2 checkpoints as a minimum requirement. And we make every effort to ensure that all the content on our site meets this standard too.</p>\r\n<p style="margin-top: 10px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">We conducted user testing for accessibility during the development of the site, with users with diverse access requirements using a range of assistive technologies. And we commissioned an independent accessibility review and acted on the findings.</p>\r\n<p style="margin-top: 10px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">We developed the design and layout of the site so that it can be used by blind or partially sighted users. It is compatible with screen readers and can be navigated without the use of a mouse or keyboard.</p>\r\n<p style="margin-top: 10px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">We are making every effort to ensure that we don''t exclude any users. For example:</p>\r\n<ul style="list-style-image: url(http://share.fco.gov.uk/resources/fco/layouts/xm_supplied/files/images/v2/list_arrow.png); line-height: 1.5em; padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 20px; margin: 0px;">\r\n<li>We try to use clear and simple language.</li>\r\n<li>We use alternative text for all our images </li>\r\n<li>The HTML we produce conforms to the standard: XHTML 1.0 Transitional</li>\r\n<li>We always include a text transcript when we publish videos on our blogs (or vlogs).</li>\r\n<li>We have tested the colours we use in the design for contrast.</li>\r\n</ul>\r\n<p style="margin-top: 10px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">We try to publish all our text content as accessible HTML rather than in other formats such as PDF. Where we do publish PDFs or other formats our policy is to make them as accessible as we can.</p>\r\n<p style="margin-top: 10px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">We decided not to use access keys after they caused confusion in our user testing. (The users who might have benefited from access keys already used keyboard shortcuts, and said that additional, site-specific access keys were not helpful.)</p>\r\n<p style="margin-top: 10px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px;">Please contact us if you have any questions, if you are having difficulties using the site, or would like to know more about what we are doing to make our websites accessible.</p>\r\n</span></p>\r\n</span>\r\n<p> </p>\r\n</div>\r\n</div>\r\n<ul class="bullet-9">\r\n</ul>', '', 1, 0, 0, 0, '2010-02-21 14:09:06', 62, '', '2010-03-13 16:07:53', 62, 62, '2010-07-28 02:30:43', '2010-02-21 14:09:06', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 11, 0, 1, '', '', 0, 66, 'robots=\nauthor='),
(59, 'Hello, I''m Seyi', 'seyis-testimonies', '', '<h2><img src="images/stories/joomla-dev_cycle.png" height="290" width="290" /></h2>\r\n<h2>POWER TO SAVE AND TO DELIVER</h2>\r\nI WANT TO THANK GOD FOR THE POWER TO SAVE AND TO DELIVER . I ALSO WANT TO THANK GOD FOR GIVING ME A CHRISTIAN PARENT,&nbsp; AND A PRAYERFUL ONE INDEED. I WAS BORN INTO A CHRISTIAN HOME AND RAISED IN THE CHRISTIAN WAY, I WAS ACTUALLY BORN IN THE GOSPEL.&nbsp; I WANT TO THANK GOD FOR SAVING&nbsp; MY SOUL, FOR SANCTIFYING ME AND FOR FILLING ME WITH THE HOLY GHOST AND FIRE.<br /><br />I GOT MY THREE CHRISTIAN EXPERIENCES BACK IN 2006 WHEN GOD OPENED MY EYES TO SEE THAT IF I DON''T RETRACE MY STEPS IT COULD LEAD ME TO HELL. I WENT TO OUR PASTOR TO TALK TO HIM, HE THEN ENCOURAGED ME TO PRAY FOR MY CHRISTIAN EXPERINCES. OUR PASTOR PRAYED WITH ME AND THAT SAME DAY GOD SAVED AND SANCTIFIED ME. LATER HE FILLED ME WITH THE HOLY GHOST AND FIRE.<br /><br />IN 2005 I HAD A GHASTLY CAR ACCIDENT. THE CAR I WAS IN RAN INTO THE RAILLINGS MADE FOR PEDESTRAN CROSSING, AND IT CAPSIZED. I STARTED TO CALL THE NAME OF JESUS OVER AND OVER AGAIN UNTIL THE CAR CAME TO AN HALT. I MANAGED TO GET OUT OF THE WRECKED CAR, THE CAR WAS DAMAGED BEYOND REPAIR. I WAS TAKEN TO THE HOSPITAL TO THE ACCIDENT AND EMERGENCY UNIT. THEY SCANNED ME AND I WAS TOLD NOTHING WAS WRONG WITH ME. THE PEOPLE THAT WITNESSED THE ACCIDENT ASKED ME WHO THE VICTIMS WERE, I SAID I WAS ONE OF THEM, THEY COULD NOT BELIEVE IT. I TOLD THEM ITS THE POWER OF GOD THAT DELIVERED MY FRIEND AND I FROM THE CRASH. PLEASE PRAY FOR ME, THAT THIS SAME GOD THAT HAS DONE THIS FOR ME, I WANT TO SEE HIM AND REIGN WITH HIM AT LAST.<br /><br />OLUSEYI OYETUGA', '', 1, 0, 0, 0, '2010-02-21 14:09:19', 62, '', '2010-03-15 16:21:04', 62, 62, '2010-03-19 08:48:40', '2010-02-21 14:09:19', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 12, '', 'Seyi''s Testimony - The Apostolic Faith Church', 0, 102, 'robots=\nauthor='),
(60, 'Terms and Conditions for Website Usage', 'terms-and-conditions', '', '<div class="note">\r\n<h2 class="typo-icon"><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; line-height: normal; font-size: 11px;">\r\n<h3 style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; margin-top: 0px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; padding: 0px;"><span style="font-weight: normal; line-height: 13px;">Welcome to our website. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern The Apostolic Faith Church''s relationship with you in relation to this website.</span></h3>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">The term ''Apostolic Faith Church'' or ''us'' or ''we'' refers to the owner of the website whose registered office is 95 Fenham Road, London; <span style="color: #474747; font-family: Helvetica, Arial, FreeSans, sans-serif; line-height: 20px; font-size: 12px;">SE15 1AE. <span style="color: #333333; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; line-height: 13px; font-size: 11px;">The term ''you'' refers to the user or viewer of our website.</span></span></p>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">The use of this website is subject to the following terms of use:</p>\r\n<ul style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; line-height: 1.3em; padding: 0px;">\r\n<li style="margin-left: 25px;">The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>\r\n<li style="margin-left: 25px;">Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>\r\n<li style="margin-left: 25px;">Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>\r\n<li style="margin-left: 25px;">This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>\r\n<li style="margin-left: 25px;">All trade marks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.</li>\r\n<li style="margin-left: 25px;">Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</li>\r\n<li style="margin-left: 25px;">From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li>\r\n<li style="margin-left: 25px;">You may not create a link to this website from another website or document without prior written consent from The Apostolic Faith Church.</li>\r\n<li style="margin-left: 25px;">Your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Scotland and Wales.</li>\r\n</ul>\r\n</span></h2>\r\n</div>', '', 1, 0, 0, 0, '2010-02-21 14:09:29', 62, '', '2010-03-13 14:47:16', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:09:29', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 3, '', '', 0, 45, 'robots=\nauthor='),
(61, 'Privacy Policy', 'privacy-policy', '', '<div class="media">\r\n<div class="typo-icon"><strong><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight: normal; line-height: normal; font-size: 11px;">\r\n<h3 style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; margin-top: 0px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; padding: 0px;"><span style="font-family: Tahoma, Helvetica, Arial, sans-serif; font-weight: normal; font-size: 12px; line-height: 15px;"><strong><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight: normal; line-height: normal; font-size: 11px;">\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; display: inline !important; padding: 0px;">This privacy policy sets out how The Apostolic Faith uses and protects any information that you give The Apostolic Faith Church, UK when you use this website.</p>\r\n</span></strong></span></h3>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">The Apostolic Faith Church, UK is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.</p>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">The Apostolic Faith Church may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 13/03/2010.</p>\r\n<h3 style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; margin-top: 0px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; padding: 0px;">What we collect</h3>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">We may collect the following information:</p>\r\n<ul style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; line-height: 1.3em; padding: 0px;">\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">name and job title</div>\r\n</li>\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">contact information including email address</div>\r\n</li>\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">demographic information such as postcode, preferences and interests</div>\r\n</li>\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">other information relevant to customer surveys and/or offers</div>\r\n</li>\r\n</ul>\r\n<h3 style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; margin-top: 0px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; padding: 0px;">What we do with the information we gather</h3>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>\r\n<ul style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; line-height: 1.3em; padding: 0px;">\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">Internal record keeping.</div>\r\n</li>\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">We may use the information to improve our products and services.</div>\r\n</li>\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">We may periodically send promotional emails about new products, special offers or other information which we think you may find interesting using the email address which you have provided.</div>\r\n</li>\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">From time to time, we may also use your information to contact you for market research purposes. We may contact you by email, phone, fax or mail. We may use the information to customise the website according to your interests.</div>\r\n</li>\r\n</ul>\r\n<h3 style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; margin-top: 0px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; padding: 0px;">Security</h3>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>\r\n<h3 style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; margin-top: 0px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; padding: 0px;">How we use cookies</h3>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">A cookie is a small file which asks permission to be placed on your computer''s hard drive. Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.</p>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">We use traffic log cookies to identify which pages are being used. This helps us analyse data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.</p>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.</p>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.</p>\r\n<h3 style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; margin-top: 0px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; padding: 0px;">Links to other websites</h3>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.</p>\r\n<h3 style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; margin-top: 0px; margin-right: 0px; margin-bottom: 2px; margin-left: 0px; padding: 0px;">Controlling your personal information</h3>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">You may choose to restrict the collection or use of your personal information in the following ways:</p>\r\n<ul style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 10px; margin-left: 0px; line-height: 1.3em; padding: 0px;">\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">whenever you are asked to fill in a form on the website, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes</div>\r\n</li>\r\n<li style="margin-left: 25px;">\r\n<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em;">if you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us here.</div>\r\n</li>\r\n</ul>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</p>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">You may request details of personal information which we hold about you under the Data Protection Act 1998. A small fee will be payable. If you would like a copy of the information held on you please write to 95 Fenham Road, London; <span style="font-family: Tahoma, Helvetica, Arial, sans-serif; line-height: 15px; font-size: 12px;"><strong><span style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-weight: normal; line-height: normal; font-size: 11px;"><span style="color: #474747; font-family: Helvetica, Arial, FreeSans, sans-serif; font-size: 12px; line-height: 20px;">\r\n<p style="margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; outline-width: 0px; outline-style: initial; outline-color: initial; font-size: 12px; vertical-align: baseline; background-image: initial; background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; display: inline !important; padding: 0px; border: 0px initial initial;">SE15 1AE</p>\r\n</span></span></strong></span></p>\r\n<p style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; margin-top: 0px; margin-right: 0px; margin-bottom: 15px; margin-left: 0px; line-height: 1.27em; padding: 0px;">If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</p>\r\n</span></strong></div>\r\n</div>', '', 1, 0, 0, 0, '2010-02-21 14:09:39', 62, '', '2010-03-13 15:20:28', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:09:39', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 4, '', '', 0, 36, 'robots=\nauthor=');
INSERT INTO `jos_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(71, 'Bible Bowl', 'bible-bowl', '', '<div class="demo-overlay">\r\n<div class="demo-top-overlay">\r\n<div class="demo-bottom-overlay"><img src="images/stories/demo/frontpage/tab2.jpg" alt="Mar10 Demo Image" /></div>\r\n</div>\r\n</div>\r\n<p class="rt-desc1">Join in the Bible Bowl for 2010</p>\r\n<p class="rt-desc2">Bible Bowl content will go in here. Highlights only</p>\r\n[readon url="index.php?option=com_content&amp;view=article&amp;id=54&amp;Itemid=61"]More Details[/readon]', '', 1, 0, 0, 0, '2010-02-25 18:00:05', 62, '', '2010-03-16 21:07:57', 62, 0, '0000-00-00 00:00:00', '2010-02-25 18:00:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 17, '', '', 0, 0, 'robots=\nauthor='),
(72, 'Youth Camp 2010', 'youth-camp-2010', '', '<div class="demo-overlay">\r\n<div class="demo-top-overlay">\r\n<div class="demo-bottom-overlay"><img src="images/stories/demo/frontpage/tab1.jpg" alt="Mar10 Demo Image" /></div>\r\n</div>\r\n</div>\r\n<p class="rt-desc1">"Get connected" at This year''s Youth Camp. Aptly-named, this year'' youth camp is about recharging your spiritual life.</p>\r\n<p class="rt-desc2">Make a date with Jesus this year to recharge your batteries at the 2010 youth camp holding in Winchester, Isle of Man.</p>\r\n[readon url="index.php?option=com_content&amp;view=article&amp;id=47&amp;Itemid=54"]Find out more information[/readon]', '', 1, 0, 0, 0, '2010-02-25 18:00:05', 62, '', '2010-03-16 17:28:28', 62, 0, '0000-00-00 00:00:00', '2010-02-25 18:00:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 8, 0, 18, '', '', 0, 1, 'robots=\nauthor='),
(73, 'Reflections', 'reflections', '', '<div class="demo-overlay">\r\n<div class="demo-top-overlay">\r\n<div class="demo-bottom-overlay"><img src="images/stories/demo/frontpage/tab3.jpg" alt="Mar10 Demo Image" /></div>\r\n</div>\r\n</div>\r\n<p class="rt-desc1">Reflections on our daily lives and our walk with God.</p>\r\n<p class="rt-desc2">We are living in a fast world where almost everything gets done  quickly.&nbsp; We finish our assignments to strict deadlines quickly;&nbsp; get to  our workplace quickly...</p>\r\n[readon url="/index.php?option=com_content&amp;view=category&amp;layout=blog&amp;id=27&amp;Itemid=59"]Read the rest of this entry[/readon]', '', 1, 0, 0, 0, '2010-02-25 18:00:05', 62, '', '2010-03-16 17:19:46', 62, 0, '0000-00-00 00:00:00', '2010-02-25 18:00:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 4, 0, 19, '', '', 0, 0, 'robots=\nauthor='),
(74, 'Word Search Challenge', 'wordsearch', '', '<div class="demo-overlay">\r\n<div class="demo-top-overlay">\r\n<div class="demo-bottom-overlay"><img src="images/stories/demo/frontpage/tab2.jpg" alt="Mar10 Demo Image" /></div>\r\n</div>\r\n</div>\r\n<p class="rt-desc1">Challenge your <span style="color: #ff6600;">knowledge of the Bible</span> with our latest word search brimming with bible names, locations and events.</p>\r\n<p>Download your copy of the word search, fill it in and send it back to us and you just might be our lucky winner this month of April [or replace with appropriate text].</p>\r\n<p>[readon url="index.php?option=com_content&amp;view=article&amp;id=48&amp;Itemid=55"]More Information[/readon]</p>', '', 1, 0, 0, 0, '2010-02-25 18:00:05', 62, '', '2010-03-16 17:15:07', 62, 62, '2010-03-16 17:15:07', '2010-02-25 18:00:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 9, 0, 20, '', '', 0, 0, 'robots=\nauthor='),
(75, 'Word Search II', 'wordsii', '', '<img src="images/stories/000 wordsearch with answers.jpg" height="709" width="645" /><br />', '', 0, 0, 0, 0, '2010-02-28 13:28:39', 62, '', '2010-03-16 00:42:38', 62, 0, '0000-00-00 00:00:00', '2010-02-28 13:28:39', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 13, 0, 6, '', '', 0, 86, 'robots=\nauthor='),
(47, 'Youth Calendar', 'youth-calendar', '', '<p><img src="images/stories/ythactivityschedule2010_web01.jpg" border="0" /></p>', '', 1, 0, 0, 0, '2010-02-21 14:07:02', 62, '', '2010-03-15 14:05:01', 62, 0, '0000-00-00 00:00:00', '2010-02-21 14:07:02', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 10, 0, 10, '', '', 0, 85, 'robots=\nauthor=');

-- --------------------------------------------------------

--
-- Table structure for table `jos_content_frontpage`
--

DROP TABLE IF EXISTS `jos_content_frontpage`;
CREATE TABLE IF NOT EXISTS `jos_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_content_frontpage`
--

INSERT INTO `jos_content_frontpage` (`content_id`, `ordering`) VALUES
(63, 4),
(62, 1),
(64, 3),
(65, 2);

-- --------------------------------------------------------

--
-- Table structure for table `jos_content_rating`
--

DROP TABLE IF EXISTS `jos_content_rating`;
CREATE TABLE IF NOT EXISTS `jos_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro`
--

DROP TABLE IF EXISTS `jos_core_acl_aro`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_aro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_value` varchar(240) NOT NULL DEFAULT '0',
  `value` varchar(240) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_section_value_value_aro` (`section_value`(100),`value`(100)),
  KEY `jos_gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jos_core_acl_aro`
--

INSERT INTO `jos_core_acl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', '62', 0, 'Administrator', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_groups`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_groups`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `jos_gacl_parent_id_aro_groups` (`parent_id`),
  KEY `jos_gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `jos_core_acl_aro_groups`
--

INSERT INTO `jos_core_acl_aro_groups` (`id`, `parent_id`, `name`, `lft`, `rgt`, `value`) VALUES
(17, 0, 'ROOT', 1, 22, 'ROOT'),
(28, 17, 'USERS', 2, 21, 'USERS'),
(29, 28, 'Public Frontend', 3, 12, 'Public Frontend'),
(18, 29, 'Registered', 4, 11, 'Registered'),
(19, 18, 'Author', 5, 10, 'Author'),
(20, 19, 'Editor', 6, 9, 'Editor'),
(21, 20, 'Publisher', 7, 8, 'Publisher'),
(30, 28, 'Public Backend', 13, 20, 'Public Backend'),
(23, 30, 'Manager', 14, 19, 'Manager'),
(24, 23, 'Administrator', 15, 18, 'Administrator'),
(25, 24, 'Super Administrator', 16, 17, 'Super Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_map`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_map`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_map` (
  `acl_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(230) NOT NULL DEFAULT '0',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_aro_sections`
--

DROP TABLE IF EXISTS `jos_core_acl_aro_sections`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_aro_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(230) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(230) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jos_gacl_value_aro_sections` (`value`),
  KEY `jos_gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jos_core_acl_aro_sections`
--

INSERT INTO `jos_core_acl_aro_sections` (`id`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', 1, 'Users', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_acl_groups_aro_map`
--

DROP TABLE IF EXISTS `jos_core_acl_groups_aro_map`;
CREATE TABLE IF NOT EXISTS `jos_core_acl_groups_aro_map` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(240) NOT NULL DEFAULT '',
  `aro_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `group_id_aro_id_groups_aro_map` (`group_id`,`section_value`,`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_core_acl_groups_aro_map`
--

INSERT INTO `jos_core_acl_groups_aro_map` (`group_id`, `section_value`, `aro_id`) VALUES
(25, '', 10);

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_log_items`
--

DROP TABLE IF EXISTS `jos_core_log_items`;
CREATE TABLE IF NOT EXISTS `jos_core_log_items` (
  `time_stamp` date NOT NULL DEFAULT '0000-00-00',
  `item_table` varchar(50) NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_core_log_searches`
--

DROP TABLE IF EXISTS `jos_core_log_searches`;
CREATE TABLE IF NOT EXISTS `jos_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_eventlist_categories`
--

DROP TABLE IF EXISTS `jos_eventlist_categories`;
CREATE TABLE IF NOT EXISTS `jos_eventlist_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `catname` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(100) NOT NULL DEFAULT '',
  `catdescription` mediumtext NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `image` varchar(100) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `groupid` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_eventlist_events`
--

DROP TABLE IF EXISTS `jos_eventlist_events`;
CREATE TABLE IF NOT EXISTS `jos_eventlist_events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `locid` int(11) unsigned NOT NULL DEFAULT '0',
  `catsid` int(11) unsigned NOT NULL DEFAULT '0',
  `dates` date NOT NULL DEFAULT '0000-00-00',
  `enddates` date DEFAULT NULL,
  `times` time DEFAULT NULL,
  `endtimes` time DEFAULT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(100) NOT NULL DEFAULT '',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL,
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `author_ip` varchar(15) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `datdescription` mediumtext NOT NULL,
  `meta_keywords` varchar(200) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `recurrence_number` int(2) NOT NULL DEFAULT '0',
  `recurrence_type` int(2) NOT NULL DEFAULT '0',
  `recurrence_counter` date NOT NULL DEFAULT '0000-00-00',
  `datimage` varchar(100) NOT NULL DEFAULT '',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `registra` tinyint(1) NOT NULL DEFAULT '0',
  `unregistra` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_eventlist_groupmembers`
--

DROP TABLE IF EXISTS `jos_eventlist_groupmembers`;
CREATE TABLE IF NOT EXISTS `jos_eventlist_groupmembers` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `member` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_eventlist_groups`
--

DROP TABLE IF EXISTS `jos_eventlist_groups`;
CREATE TABLE IF NOT EXISTS `jos_eventlist_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_eventlist_register`
--

DROP TABLE IF EXISTS `jos_eventlist_register`;
CREATE TABLE IF NOT EXISTS `jos_eventlist_register` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0',
  `uregdate` varchar(50) NOT NULL DEFAULT '',
  `uip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_eventlist_settings`
--

DROP TABLE IF EXISTS `jos_eventlist_settings`;
CREATE TABLE IF NOT EXISTS `jos_eventlist_settings` (
  `id` int(11) NOT NULL,
  `oldevent` tinyint(4) NOT NULL,
  `minus` tinyint(4) NOT NULL,
  `showtime` tinyint(4) NOT NULL,
  `showtitle` tinyint(4) NOT NULL,
  `showlocate` tinyint(4) NOT NULL,
  `showcity` tinyint(4) NOT NULL,
  `showmapserv` tinyint(4) NOT NULL,
  `map24id` varchar(20) NOT NULL,
  `gmapkey` varchar(255) NOT NULL,
  `tablewidth` varchar(20) NOT NULL,
  `datewidth` varchar(20) NOT NULL,
  `titlewidth` varchar(20) NOT NULL,
  `locationwidth` varchar(20) NOT NULL,
  `citywidth` varchar(20) NOT NULL,
  `datename` varchar(100) NOT NULL,
  `titlename` varchar(100) NOT NULL,
  `locationname` varchar(100) NOT NULL,
  `cityname` varchar(100) NOT NULL,
  `formatdate` varchar(100) NOT NULL,
  `formattime` varchar(100) NOT NULL,
  `timename` varchar(50) NOT NULL,
  `showdetails` tinyint(4) NOT NULL,
  `showtimedetails` tinyint(4) NOT NULL,
  `showevdescription` tinyint(4) NOT NULL,
  `showdetailstitle` tinyint(4) NOT NULL,
  `showdetailsadress` tinyint(4) NOT NULL,
  `showlocdescription` tinyint(4) NOT NULL,
  `showlinkvenue` tinyint(4) NOT NULL,
  `showdetlinkvenue` tinyint(4) NOT NULL,
  `delivereventsyes` tinyint(4) NOT NULL,
  `mailinform` tinyint(4) NOT NULL,
  `mailinformrec` varchar(150) NOT NULL,
  `mailinformuser` tinyint(4) NOT NULL,
  `datdesclimit` varchar(15) NOT NULL,
  `autopubl` tinyint(4) NOT NULL,
  `deliverlocsyes` tinyint(4) NOT NULL,
  `autopublocate` tinyint(4) NOT NULL,
  `showcat` tinyint(4) NOT NULL,
  `catfrowidth` varchar(20) NOT NULL,
  `catfroname` varchar(100) NOT NULL,
  `evdelrec` tinyint(4) NOT NULL,
  `evpubrec` tinyint(4) NOT NULL,
  `locdelrec` tinyint(4) NOT NULL,
  `locpubrec` tinyint(4) NOT NULL,
  `sizelimit` varchar(20) NOT NULL,
  `imagehight` varchar(20) NOT NULL,
  `imagewidth` varchar(20) NOT NULL,
  `gddisabled` tinyint(4) NOT NULL,
  `imageenabled` tinyint(4) NOT NULL,
  `comunsolution` tinyint(4) NOT NULL,
  `comunoption` tinyint(4) NOT NULL,
  `catlinklist` tinyint(4) NOT NULL,
  `showfroregistra` tinyint(4) NOT NULL,
  `showfrounregistra` tinyint(4) NOT NULL,
  `eventedit` tinyint(4) NOT NULL,
  `eventeditrec` tinyint(4) NOT NULL,
  `eventowner` tinyint(4) NOT NULL,
  `venueedit` tinyint(4) NOT NULL,
  `venueeditrec` tinyint(4) NOT NULL,
  `venueowner` tinyint(4) NOT NULL,
  `lightbox` tinyint(4) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `showstate` tinyint(4) NOT NULL,
  `statename` varchar(100) NOT NULL,
  `statewidth` varchar(20) NOT NULL,
  `regname` tinyint(4) NOT NULL,
  `storeip` tinyint(4) NOT NULL,
  `commentsystem` tinyint(4) NOT NULL,
  `lastupdate` varchar(20) NOT NULL DEFAULT '',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_eventlist_settings`
--

INSERT INTO `jos_eventlist_settings` (`id`, `oldevent`, `minus`, `showtime`, `showtitle`, `showlocate`, `showcity`, `showmapserv`, `map24id`, `gmapkey`, `tablewidth`, `datewidth`, `titlewidth`, `locationwidth`, `citywidth`, `datename`, `titlename`, `locationname`, `cityname`, `formatdate`, `formattime`, `timename`, `showdetails`, `showtimedetails`, `showevdescription`, `showdetailstitle`, `showdetailsadress`, `showlocdescription`, `showlinkvenue`, `showdetlinkvenue`, `delivereventsyes`, `mailinform`, `mailinformrec`, `mailinformuser`, `datdesclimit`, `autopubl`, `deliverlocsyes`, `autopublocate`, `showcat`, `catfrowidth`, `catfroname`, `evdelrec`, `evpubrec`, `locdelrec`, `locpubrec`, `sizelimit`, `imagehight`, `imagewidth`, `gddisabled`, `imageenabled`, `comunsolution`, `comunoption`, `catlinklist`, `showfroregistra`, `showfrounregistra`, `eventedit`, `eventeditrec`, `eventowner`, `venueedit`, `venueeditrec`, `venueowner`, `lightbox`, `meta_keywords`, `meta_description`, `showstate`, `statename`, `statewidth`, `regname`, `storeip`, `commentsystem`, `lastupdate`, `checked_out`, `checked_out_time`) VALUES
(1, 0, 1, 0, 1, 1, 1, 0, '', '', '100%', '15%', '25%', '20%', '20%', 'Date', 'Title', 'Venue', 'City', '%d.%m.%Y', '%H.%M', 'h', 1, 0, 1, 1, 1, 1, 1, 2, -2, 0, 'example@example.com', 0, '1000', -2, -2, -2, 1, '20%', 'Type', 1, 1, 1, 1, '100', '100', '100', 0, 1, 0, 0, 1, 2, 2, -2, 1, 0, -2, 1, 0, 0, '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 'State', '0', 0, 1, 0, '1174491851', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_eventlist_venues`
--

DROP TABLE IF EXISTS `jos_eventlist_venues`;
CREATE TABLE IF NOT EXISTS `jos_eventlist_venues` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `venue` varchar(50) NOT NULL DEFAULT '',
  `alias` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(200) NOT NULL DEFAULT '',
  `street` varchar(50) DEFAULT NULL,
  `plz` varchar(20) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `locdescription` mediumtext NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `locimage` varchar(100) NOT NULL DEFAULT '',
  `map` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `author_ip` varchar(15) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_gk3_tabs_manager_groups`
--

DROP TABLE IF EXISTS `jos_gk3_tabs_manager_groups`;
CREATE TABLE IF NOT EXISTS `jos_gk3_tabs_manager_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `desc` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_gk3_tabs_manager_options`
--

DROP TABLE IF EXISTS `jos_gk3_tabs_manager_options`;
CREATE TABLE IF NOT EXISTS `jos_gk3_tabs_manager_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `jos_gk3_tabs_manager_options`
--

INSERT INTO `jos_gk3_tabs_manager_options` (`id`, `name`, `value`) VALUES
(1, 'modal_news', '0'),
(2, 'modal_settings', '1'),
(3, 'group_shortcuts', '1'),
(4, 'tab_shortcuts', '1'),
(5, 'wysiwyg', '1'),
(6, 'gavick_news', '1'),
(7, 'article_id', '0');

-- --------------------------------------------------------

--
-- Table structure for table `jos_gk3_tabs_manager_tabs`
--

DROP TABLE IF EXISTS `jos_gk3_tabs_manager_tabs`;
CREATE TABLE IF NOT EXISTS `jos_gk3_tabs_manager_tabs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `type` varchar(32) NOT NULL,
  `content` text NOT NULL,
  `published` int(2) NOT NULL,
  `access` int(1) NOT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_groups`
--

DROP TABLE IF EXISTS `jos_groups`;
CREATE TABLE IF NOT EXISTS `jos_groups` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_groups`
--

INSERT INTO `jos_groups` (`id`, `name`) VALUES
(0, 'Public'),
(1, 'Registered'),
(2, 'Special');

-- --------------------------------------------------------

--
-- Table structure for table `jos_jce_extensions`
--

DROP TABLE IF EXISTS `jos_jce_extensions`;
CREATE TABLE IF NOT EXISTS `jos_jce_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `published` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_jce_extensions`
--

INSERT INTO `jos_jce_extensions` (`id`, `pid`, `name`, `extension`, `folder`, `published`) VALUES
(1, 54, 'Joomla Links for Advanced Link', 'joomlalinks', 'links', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_jce_groups`
--

DROP TABLE IF EXISTS `jos_jce_groups`;
CREATE TABLE IF NOT EXISTS `jos_jce_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `users` text NOT NULL,
  `types` varchar(255) NOT NULL,
  `components` text NOT NULL,
  `rows` text NOT NULL,
  `plugins` varchar(255) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `checked_out` tinyint(3) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jos_jce_groups`
--

INSERT INTO `jos_jce_groups` (`id`, `name`, `description`, `users`, `types`, `components`, `rows`, `plugins`, `published`, `ordering`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Default', 'Default group for all users with edit access', '', '19,20,21,23,24,25', '', '6,7,8,9,10,11,12,13,14,15,16,17,18,19;20,21,22,23,24,25,26,27,28,30,31,32,33,36;37,38,39,40,41,42,43,44,45,46,47,48;49,50,51,52,53,54,55,57,58', '1,2,3,4,5,6,20,21,37,38,39,40,41,42,49,50,51,52,53,54,55,57,58', 1, 1, 0, '0000-00-00 00:00:00', ''),
(2, 'Front End', 'Sample Group for Authors, Editors, Publishers', '', '19,20,21', '', '6,7,8,9,10,13,14,15,16,17,18,19,27,28;20,21,25,26,30,31,32,36,43,44,45,47,48,50,51;24,33,39,40,42,46,49,52,53,54,55,57,58', '6,20,21,50,51,1,3,5,39,40,42,49,52,53,54,55,57,58', 0, 2, 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_jce_plugins`
--

DROP TABLE IF EXISTS `jos_jce_plugins`;
CREATE TABLE IF NOT EXISTS `jos_jce_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `row` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `editable` tinyint(3) NOT NULL,
  `iscore` tinyint(3) NOT NULL,
  `elements` varchar(255) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `jos_jce_plugins`
--

INSERT INTO `jos_jce_plugins` (`id`, `title`, `name`, `type`, `icon`, `layout`, `row`, `ordering`, `published`, `editable`, `iscore`, `elements`, `checked_out`, `checked_out_time`) VALUES
(1, 'Context Menu', 'contextmenu', 'plugin', '', '', 0, 0, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(2, 'File Browser', 'browser', 'plugin', '', '', 0, 0, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(3, 'Inline Popups', 'inlinepopups', 'plugin', '', '', 0, 0, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(4, 'Media Support', 'media', 'plugin', '', '', 0, 0, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(5, 'Safari Browser Support', 'safari', 'plugin', '', '', 0, 0, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(6, 'Help', 'help', 'plugin', 'help', 'help', 1, 1, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(7, 'New Document', 'newdocument', 'command', 'newdocument', 'newdocument', 1, 2, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(8, 'Bold', 'bold', 'command', 'bold', 'bold', 1, 3, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(9, 'Italic', 'italic', 'command', 'italic', 'italic', 1, 4, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(10, 'Underline', 'underline', 'command', 'underline', 'underline', 1, 5, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(11, 'Font Select', 'fontselect', 'command', 'fontselect', 'fontselect', 1, 6, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(12, 'Font Size Select', 'fontsizeselect', 'command', 'fontsizeselect', 'fontsizeselect', 1, 7, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(13, 'Style Select', 'styleselect', 'command', 'styleselect', 'styleselect', 1, 8, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(14, 'StrikeThrough', 'strikethrough', 'command', 'strikethrough', 'strikethrough', 1, 9, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(15, 'Justify Full', 'full', 'command', 'justifyfull', 'justifyfull', 1, 10, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(16, 'Justify Center', 'center', 'command', 'justifycenter', 'justifycenter', 1, 11, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(17, 'Justify Left', 'left', 'command', 'justifyleft', 'justifyleft', 1, 12, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(18, 'Justify Right', 'right', 'command', 'justifyright', 'justifyright', 1, 13, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(19, 'Format Select', 'formatselect', 'command', 'formatselect', 'formatselect', 1, 14, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(20, 'Paste', 'paste', 'plugin', 'pasteword,pastetext', 'paste', 2, 1, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(21, 'Search Replace', 'searchreplace', 'plugin', 'search,replace', 'searchreplace', 2, 2, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(22, 'Font ForeColour', 'forecolor', 'command', 'forecolor', 'forecolor', 2, 3, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(23, 'Font BackColour', 'backcolor', 'command', 'backcolor', 'backcolor', 2, 4, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(24, 'Unlink', 'unlink', 'command', 'unlink', 'unlink', 2, 5, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(25, 'Indent', 'indent', 'command', 'indent', 'indent', 2, 6, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(26, 'Outdent', 'outdent', 'command', 'outdent', 'outdent', 2, 7, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(27, 'Undo', 'undo', 'command', 'undo', 'undo', 2, 8, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(28, 'Redo', 'redo', 'command', 'redo', 'redo', 2, 9, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(29, 'HTML', 'html', 'command', 'code', 'code', 2, 10, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(30, 'Numbered List', 'numlist', 'command', 'numlist', 'numlist', 2, 11, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(31, 'Bullet List', 'bullist', 'command', 'bullist', 'bullist', 2, 12, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(32, 'Clipboard Actions', 'clipboard', 'command', 'cut,copy,paste', 'clipboard', 2, 13, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(33, 'Anchor', 'anchor', 'command', 'anchor', 'anchor', 2, 14, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(34, 'Image', 'image', 'command', 'image', 'image', 2, 15, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(35, 'Link', 'link', 'command', 'link', 'link', 2, 16, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(36, 'Code Cleanup', 'cleanup', 'command', 'cleanup', 'cleanup', 2, 17, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(37, 'Directionality', 'directionality', 'plugin', 'ltr,rtl', 'directionality', 3, 1, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(38, 'Emotions', 'emotions', 'plugin', 'emotions', 'emotions', 3, 2, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(39, 'Fullscreen', 'fullscreen', 'plugin', 'fullscreen', 'fullscreen', 3, 3, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(40, 'Preview', 'preview', 'plugin', 'preview', 'preview', 3, 4, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(41, 'Tables', 'table', 'plugin', 'tablecontrols', 'buttons', 3, 5, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(42, 'Print', 'print', 'plugin', 'print', 'print', 3, 6, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(43, 'Horizontal Rule', 'hr', 'command', 'hr', 'hr', 3, 7, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(44, 'Subscript', 'sub', 'command', 'sub', 'sub', 3, 8, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(45, 'Superscript', 'sup', 'command', 'sup', 'sup', 3, 9, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(46, 'Visual Aid', 'visualaid', 'command', 'visualaid', 'visualaid', 3, 10, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(47, 'Character Map', 'charmap', 'command', 'charmap', 'charmap', 3, 11, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(48, 'Remove Format', 'removeformat', 'command', 'removeformat', 'removeformat', 3, 12, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(49, 'Styles', 'style', 'plugin', 'styleprops', 'style', 4, 1, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(50, 'Non-Breaking', 'nonbreaking', 'plugin', 'nonbreaking', 'nonbreaking', 4, 2, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(51, 'Visual Characters', 'visualchars', 'plugin', 'visualchars', 'visualchars', 4, 3, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(52, 'XHTML Xtras', 'xhtmlxtras', 'plugin', 'cite,abbr,acronym,del,ins,attribs', 'xhtmlxtras', 4, 4, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(53, 'Image Manager', 'imgmanager', 'plugin', 'imgmanager', 'imgmanager', 4, 5, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(54, 'Advanced Link', 'advlink', 'plugin', 'advlink', 'advlink', 4, 6, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(55, 'Spell Checker', 'spellchecker', 'plugin', 'spellchecker', 'spellchecker', 4, 7, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(56, 'Layers', 'layer', 'plugin', 'insertlayer,moveforward,movebackward,absolute', 'layer', 4, 8, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(57, 'Advanced Code Editor', 'advcode', 'plugin', 'advcode', 'advcode', 4, 9, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(58, 'Article Breaks', 'article', 'plugin', 'readmore,pagebreak', 'article', 4, 10, 1, 0, 1, '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_attachments`
--

DROP TABLE IF EXISTS `jos_k2_attachments`;
CREATE TABLE IF NOT EXISTS `jos_k2_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `titleAttribute` text NOT NULL,
  `hits` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `itemID` (`itemID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_categories`
--

DROP TABLE IF EXISTS `jos_k2_categories`;
CREATE TABLE IF NOT EXISTS `jos_k2_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `parent` int(11) DEFAULT '0',
  `extraFieldsGroup` int(11) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL,
  `params` text NOT NULL,
  `trash` smallint(6) NOT NULL DEFAULT '0',
  `plugins` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category` (`published`,`access`,`trash`),
  KEY `parent` (`parent`),
  KEY `ordering` (`ordering`),
  KEY `published` (`published`),
  KEY `access` (`access`),
  KEY `trash` (`trash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_comments`
--

DROP TABLE IF EXISTS `jos_k2_comments`;
CREATE TABLE IF NOT EXISTS `jos_k2_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `itemID` int(11) NOT NULL,
  `userID` int(11) NOT NULL,
  `userName` varchar(255) NOT NULL,
  `commentDate` datetime NOT NULL,
  `commentText` text NOT NULL,
  `commentEmail` varchar(255) NOT NULL,
  `commentURL` varchar(255) NOT NULL,
  `published` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `itemID` (`itemID`),
  KEY `userID` (`userID`),
  KEY `published` (`published`),
  KEY `latestComments` (`published`,`commentDate`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_extra_fields`
--

DROP TABLE IF EXISTS `jos_k2_extra_fields`;
CREATE TABLE IF NOT EXISTS `jos_k2_extra_fields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `type` varchar(255) NOT NULL,
  `group` int(11) NOT NULL,
  `published` tinyint(4) NOT NULL,
  `ordering` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group` (`group`),
  KEY `published` (`published`),
  KEY `ordering` (`ordering`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_items`
--

DROP TABLE IF EXISTS `jos_k2_items`;
CREATE TABLE IF NOT EXISTS `jos_k2_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `catid` int(11) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  `introtext` text NOT NULL,
  `fulltext` text NOT NULL,
  `video` text,
  `gallery` varchar(255) DEFAULT NULL,
  `extra_fields` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `extra_fields_search` text NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL,
  `checked_out` int(10) unsigned NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL,
  `publish_down` datetime NOT NULL,
  `trash` smallint(6) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `featured` smallint(6) NOT NULL DEFAULT '0',
  `featured_ordering` int(11) NOT NULL DEFAULT '0',
  `image_caption` text NOT NULL,
  `image_credits` varchar(255) NOT NULL,
  `video_caption` text NOT NULL,
  `video_credits` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL,
  `params` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `metakey` text NOT NULL,
  `plugins` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `item` (`published`,`publish_up`,`publish_down`,`trash`,`access`),
  KEY `catid` (`catid`),
  KEY `created_by` (`created_by`),
  KEY `ordering` (`ordering`),
  KEY `featured` (`featured`),
  KEY `featured_ordering` (`featured_ordering`),
  KEY `hits` (`hits`),
  FULLTEXT KEY `search` (`title`,`introtext`,`fulltext`,`extra_fields_search`,`image_caption`,`image_credits`,`video_caption`,`video_credits`,`metadesc`,`metakey`),
  FULLTEXT KEY `title` (`title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_rating`
--

DROP TABLE IF EXISTS `jos_k2_rating`;
CREATE TABLE IF NOT EXISTS `jos_k2_rating` (
  `itemID` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`itemID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_tags`
--

DROP TABLE IF EXISTS `jos_k2_tags`;
CREATE TABLE IF NOT EXISTS `jos_k2_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `published` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  FULLTEXT KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_tags_xref`
--

DROP TABLE IF EXISTS `jos_k2_tags_xref`;
CREATE TABLE IF NOT EXISTS `jos_k2_tags_xref` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tagID` int(11) NOT NULL,
  `itemID` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tagID` (`tagID`),
  KEY `itemID` (`itemID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_users`
--

DROP TABLE IF EXISTS `jos_k2_users`;
CREATE TABLE IF NOT EXISTS `jos_k2_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `userName` varchar(255) DEFAULT NULL,
  `gender` enum('m','f') NOT NULL DEFAULT 'm',
  `description` text NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `group` int(11) NOT NULL DEFAULT '0',
  `plugins` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `userID` (`userID`),
  KEY `group` (`group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_k2_user_groups`
--

DROP TABLE IF EXISTS `jos_k2_user_groups`;
CREATE TABLE IF NOT EXISTS `jos_k2_user_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `permissions` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jos_k2_user_groups`
--

INSERT INTO `jos_k2_user_groups` (`id`, `name`, `permissions`) VALUES
(1, 'Registered', 'frontEdit=0\nadd=0\neditOwn=0\neditAll=0\npublish=0\ncomment=1\ninheritance=0\ncategories=all\n\n'),
(2, 'Site Owner', 'frontEdit=1\nadd=1\neditOwn=1\neditAll=1\npublish=1\ncomment=1\ninheritance=1\ncategories=all\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `jos_menu`
--

DROP TABLE IF EXISTS `jos_menu`;
CREATE TABLE IF NOT EXISTS `jos_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text,
  `type` varchar(50) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `componentid` int(11) unsigned NOT NULL DEFAULT '0',
  `sublevel` int(11) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pollid` int(11) NOT NULL DEFAULT '0',
  `browserNav` tinyint(4) DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `utaccess` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `lft` int(11) unsigned NOT NULL DEFAULT '0',
  `rgt` int(11) unsigned NOT NULL DEFAULT '0',
  `home` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `componentid` (`componentid`,`menutype`,`published`,`access`),
  KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=152 ;

--
-- Dumping data for table `jos_menu`
--

INSERT INTO `jos_menu` (`id`, `menutype`, `name`, `alias`, `link`, `type`, `published`, `parent`, `componentid`, `sublevel`, `ordering`, `checked_out`, `checked_out_time`, `pollid`, `browserNav`, `access`, `utaccess`, `params`, `lft`, `rgt`, `home`) VALUES
(1, 'mainmenu', 'Home', 'home', 'index.php?option=com_content&view=frontpage', 'component', 1, 0, 20, 0, 65, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'num_leading_articles=4\nnum_intro_articles=0\nnum_columns=1\nnum_links=0\norderby_pri=\norderby_sec=order\nmulti_column_order=1\nshow_pagination=0\nshow_pagination_results=0\nshow_feed_link=1\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=-1\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=The Apostolic Faith Church Youths\nshow_page_title=0\npageclass_sfx=mar10-home\nmenu_image=-1\nsecure=0\n\n', 0, 0, 1),
(137, 'keyconcepts', 'Military', 'military', '?presets=military', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(138, 'keyconcepts', 'Bright Day', 'bright-day', '?presets=brightday', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(139, 'keyconcepts', 'Hot Pepper', 'hot-pepper', '?presets=hotpepper', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(134, 'keyconcepts', 'Woody', 'woody', '?presets=woody', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(135, 'keyconcepts', 'Moderna', 'moderna', '?presets=moderna', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(136, 'keyconcepts', 'Elegance', 'elegance', '?presets=elegance', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(11, 'keyconcepts', 'Joomla! Home', 'joomla-home', 'http://www.joomla.org', 'url', -2, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(12, 'keyconcepts', 'Joomla! Forums', 'joomla-forums', 'http://forum.joomla.org', 'url', -2, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(13, 'keyconcepts', 'Joomla! Documentation', 'joomla-documentation', 'http://docs.joomla.org', 'url', -2, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(14, 'keyconcepts', 'Joomla! Community', 'joomla-community', 'http://community.joomla.org', 'url', -2, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(15, 'keyconcepts', 'Joomla! Magazine', 'joomla-community-magazine', 'http://community.joomla.org/magazine.html', 'url', -2, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(16, 'keyconcepts', 'OSM Home', 'osm-home', 'http://www.opensourcematters.org', 'url', -2, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 6, 'menu_image=-1\n\n', 0, 0, 0),
(17, 'keyconcepts', 'Administrator', 'administrator', 'administrator/', 'url', -2, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'menu_image=-1\n\n', 0, 0, 0),
(18, 'topmenu', 'News', 'news', 'index.php?option=com_newsfeeds&view=newsfeed&id=1&feedid=1', 'component', -2, 0, 11, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'show_page_title=1\npage_title=News\npageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_other_cats=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 0, 0, 0),
(20, 'usermenu', 'Your Details', 'your-details', 'index.php?option=com_user&view=user&task=edit', 'component', 1, 0, 14, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 1, 3, '', 0, 0, 0),
(24, 'usermenu', 'Logout', 'logout', 'index.php?option=com_user&view=login', 'component', 1, 0, 14, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 1, 3, '', 0, 0, 0),
(38, 'keyconcepts', 'Content Layouts', 'content-layouts', 'index.php?option=com_content&view=article&id=24', 'component', -2, 0, 20, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(27, 'mainmenu', 'Contact Us', 'contactus', 'index.php?option=com_content&view=article&id=19', 'component', 1, 0, 20, 0, 70, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(28, 'topmenu', 'About Joomla!', 'about-joomla', 'index.php?option=com_content&view=article&id=25', 'component', -2, 0, 20, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(29, 'topmenu', 'Features', 'features', 'index.php?option=com_content&view=article&id=22', 'component', -2, 0, 20, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(30, 'topmenu', 'The Community', 'the-community', 'index.php?option=com_content&view=article&id=27', 'component', -2, 0, 20, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(133, 'keyconcepts', 'Preset Styles', 'preset-styles', 'index.php?option=com_content&view=article&id=75', 'component', -2, 0, 20, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=2\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(40, 'keyconcepts', 'Extensions', 'extensions', 'index.php?option=com_content&view=article&id=26', 'component', -2, 0, 20, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(143, 'keyconcepts', 'Rich Vintage', 'rich-vintage', '?presets=richvintage', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(144, 'keyconcepts', 'Entrapped', 'entrapped', '?presets=entrapped', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(145, 'keyconcepts', 'Butterfly', 'butterfly', '?presets=butterfly', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(43, 'keyconcepts', 'Example Pages', 'example-pages', 'index.php?option=com_content&view=article&id=43', 'component', -2, 0, 20, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'pageclass_sfx=\nmenu_image=-1\nsecure=0\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(44, 'intranetmenu', 'Section Blog', 'section-blog', 'index.php?option=com_content&view=section&layout=blog&id=3', 'component', -2, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Section Blog layout (FAQ section)\nshow_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\nshow_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_pri=\norderby_sec=\nshow_pagination=2\nshow_pagination_results=1\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(45, 'intranetmenu', 'Section Table', 'section-table', 'index.php?option=com_content&view=section&id=3', 'component', -2, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Table Blog layout (FAQ section)\nshow_description=0\nshow_description_image=0\nshow_categories=1\nshow_empty_categories=0\nshow_cat_num_articles=1\nshow_category_description=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby=\nshow_noauth=0\nshow_title=1\nnlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(46, 'intranetmenu', 'Category Blog', 'categoryblog', 'index.php?option=com_content&view=category&layout=blog&id=31', 'component', -2, 0, 20, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Category Blog layout (FAQs/General category)\nshow_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\nshow_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_pri=\norderby_sec=\nshow_pagination=2\nshow_pagination_results=1\nshow_noauth=0\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(47, 'intranetmenu', 'Category Table', 'category-table', 'index.php?option=com_content&view=category&id=32', 'component', -2, 0, 20, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_page_title=1\npage_title=Example of Category Table layout (FAQs/Languages category)\nshow_headings=1\nshow_date=0\ndate_format=\nfilter=1\nfilter_type=title\npageclass_sfx=\nmenu_image=-1\nsecure=0\norderby_sec=\nshow_pagination=1\nshow_pagination_limit=1\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\n\n', 0, 0, 0),
(48, 'mainmenu', 'Web Links', 'web-links', 'index.php?option=com_weblinks&view=category&id=2', 'component', -2, 0, 4, 0, 49, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_feed_link=1\nshow_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\ntarget=\nlink_icons=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=Weblinks\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(140, 'keyconcepts', 'Velvetine', 'velvetine', '?presets=velvetine', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(141, 'keyconcepts', 'Mild Pepper', 'mild-pepper', '?presets=mildpepper', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(142, 'keyconcepts', 'Crystalline', 'crystalline', '?presets=crystalline', 'url', -2, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(50, 'mainmenu', 'Blog Layout', 'blog-layout', 'index.php?option=com_content&view=category&layout=blog&id=1', 'component', -2, 0, 20, 0, 48, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=2\nnum_columns=1\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=0\nshow_title=1\nlink_titles=0\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=1\nshow_create_date=1\nshow_modify_date=1\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=1\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=1\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=The News\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(51, 'usermenu', 'Submit an Article', 'submit-an-article', 'index.php?option=com_content&view=article&layout=form', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 2, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(52, 'usermenu', 'Submit a Web Link', 'submit-a-web-link', 'index.php?option=com_weblinks&view=weblink&layout=form', 'component', 0, 0, 4, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 2, 0, '', 0, 0, 0),
(53, 'mainmenu', 'What''s New', 'whatsnew', 'index.php?option=com_content&view=article&id=46', 'component', 1, 0, 20, 0, 66, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(54, 'keyconcepts', 'Upcoming Events', 'upcomingevents', 'index.php?option=com_content&view=article&id=47', 'component', 1, 0, 20, 0, 12, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(55, 'keyconcepts', 'Next Events', 'nextevents', 'index.php?option=com_content&view=article&id=48', 'component', -2, 0, 20, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(56, 'keyconcepts', 'Recent Event Reports', 'recentreports', 'index.php?option=com_content&view=article&id=49', 'component', 1, 0, 20, 0, 14, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(57, 'mainmenu', 'Module Positions', 'module-positions', 'index.php?option=com_content&view=article&id=50', 'component', -2, 0, 20, 0, 47, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(58, 'mainmenu', 'Get Inspired', 'getinspired', 'index.php?option=com_content&view=article&id=46', 'component', 1, 0, 20, 0, 67, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(59, 'keyconcepts', 'Reflections', 'reflections', 'index.php?option=com_content&view=category&layout=blog&id=27', 'component', 1, 0, 20, 0, 15, 62, '2010-03-15 16:18:36', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=0\nnum_columns=1\nnum_links=0\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(60, 'keyconcepts', 'Testimonies', 'testimonies', 'index.php?option=com_content&view=category&layout=blog&id=32', 'component', 1, 0, 20, 0, 16, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=0\nnum_intro_articles=2\nnum_columns=2\nnum_links=0\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(61, 'mainmenu', 'Multimedia Showcase', 'mediashowcase', 'index.php?option=com_content&view=category&layout=blog&id=30', 'component', 1, 0, 20, 0, 68, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=0\nnum_intro_articles=4\nnum_columns=1\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(62, 'mainmenu', 'Bible Challenge', 'biblechallenge', 'index.php?option=com_content&view=category&layout=blog&id=40', 'component', 1, 0, 20, 0, 69, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=0\nnum_intro_articles=4\nnum_columns=1\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(63, 'keyconcepts', 'Wordsearch', 'wordsearch', 'index.php?option=com_content&view=category&layout=blog&id=39', 'component', 1, 0, 20, 0, 17, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=0\nnum_intro_articles=1\nnum_columns=1\nnum_links=0\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=0\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(64, 'keyconcepts', 'Devotionals', 'devotionals', 'index.php?option=com_content&view=category&layout=blog&id=3', 'component', 1, 0, 20, 0, 18, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=0\nnum_columns=1\nnum_links=0\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(65, 'keyconcepts', 'Bible Names', 'biblenames', 'index.php?option=com_content&view=article&id=58', 'component', 1, 0, 20, 0, 19, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(66, 'mainmenu', 'Menu Options', 'menu-options', 'index.php?option=com_content&view=article&id=59', 'component', -2, 0, 20, 0, 45, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(67, 'mainmenu', 'Logo Editing', 'logo-editing', 'index.php?option=com_content&view=article&id=60', 'component', -2, 0, 20, 0, 44, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(68, 'mainmenu', 'Using Typography', 'using-typography', 'index.php?option=com_content&view=article&id=61', 'component', -2, 0, 20, 0, 43, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(70, 'mainmenu', 'Menu Icons', 'menu-icons', 'index.php?Itemid=66', 'menulink', -2, 0, 0, 0, 42, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_item=66\nfusion_item_subtext=Preview Icons\nfusion_columns=2\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(72, 'mainmenu', 'Child Items', 'child-items', 'index.php?Itemid=66', 'menulink', -2, 0, 0, 0, 41, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_item=66\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(73, 'keyconcepts', 'Request Prayer', 'requestprayer', 'index.php?option=com_content&view=article&id=10', 'component', 1, 0, 20, 0, 20, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(75, 'mainmenu', 'Contact Us', 'contact-us', 'index.php?option=com_contact&view=contact&id=1', 'component', -2, 0, 7, 0, 40, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_contact_list=0\nshow_category_crumb=0\ncontact_icons=\nicon_address=\nicon_email=\nicon_telephone=\nicon_mobile=\nicon_fax=\nicon_misc=\nshow_headings=\nshow_position=\nshow_email=\nshow_telephone=\nshow_mobile=\nshow_fax=\nallow_vcard=\nbanned_email=\nbanned_subject=\nbanned_text=\nvalidate_session=\ncustom_reply=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(76, 'mainmenu', 'Member Access', 'member-access', 'index.php?option=com_user&view=login', 'component', -2, 0, 14, 0, 39, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_login_title=1\nheader_login=Members Only Area\nlogin=\nlogin_message=0\ndescription_login=0\ndescription_login_text=\nimage_login=\nimage_login_align=right\nshow_logout_title=1\nheader_logout=\nlogout=\nlogout_message=1\ndescription_logout=1\ndescription_logout_text=\nimage_logout=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(77, 'mainmenu', 'Wrapper', 'wrapper', 'index.php?option=com_wrapper&view=wrapper', 'component', -2, 0, 17, 0, 38, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'url=http://www.google.com\nscrolling=auto\nwidth=100%\nheight=650\nheight_auto=0\nadd_scheme=1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(78, 'mainmenu', 'Add Icon', 'add-icon', '', 'separator', -2, 0, 0, 0, 37, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-add.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(79, 'mainmenu', 'Arrow Icon', 'arrow-icon', '', 'separator', -2, 0, 0, 0, 50, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-arrow.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(80, 'mainmenu', 'Briefcase Icon', 'briefcase-icon', '', 'separator', -2, 0, 0, 0, 51, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-briefcase.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(81, 'mainmenu', 'Calendar Icon', 'calendar-icon', '', 'separator', -2, 0, 0, 0, 60, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-calendar.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(82, 'mainmenu', 'Check Icon', 'check-icon', '', 'separator', -2, 0, 0, 0, 61, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-check.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(83, 'mainmenu', 'Crank Icon', 'crank-icon', '', 'separator', -2, 0, 0, 0, 62, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-crank.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(84, 'mainmenu', 'Docs Icon', 'docs-icon', '', 'separator', -2, 0, 0, 0, 63, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-docs.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(85, 'mainmenu', 'Email Icon', 'email-icon', '', 'separator', -2, 0, 0, 0, 64, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-email.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(86, 'mainmenu', 'Home Icon', 'home-icon', '', 'separator', -2, 0, 0, 0, 59, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-home.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(87, 'mainmenu', 'Lock Icon', 'lock-icon', '', 'separator', -2, 0, 0, 0, 58, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-key1.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(88, 'mainmenu', 'Minus Icon', 'minus-icon', '', 'separator', -2, 0, 0, 0, 57, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-minus.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(89, 'mainmenu', 'Monitor Icon', 'monitor-icon', '', 'separator', -2, 0, 0, 0, 56, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-monitor.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(90, 'mainmenu', 'Notes Icon', 'notes-icon', '', 'separator', -2, 0, 0, 0, 55, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-notes.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(91, 'mainmenu', 'Post Icon', 'post-icon', '', 'separator', -2, 0, 0, 0, 54, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-post.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(92, 'mainmenu', 'Printer Icon', 'printer-icon', '', 'separator', -2, 0, 0, 0, 53, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-printer.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(93, 'mainmenu', 'RSS Icon', 'rss-icon', '', 'separator', -2, 0, 0, 0, 52, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-rss.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(94, 'mainmenu', 'Warning Icon', 'warning-icon', '', 'separator', -2, 0, 0, 0, 36, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-warning.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(95, 'mainmenu', 'Write Icon', 'write-icon', '', 'separator', -2, 0, 0, 0, 35, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-write.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(96, 'mainmenu', 'Delete Icon', 'delete-icon', '', 'separator', -2, 0, 0, 0, 34, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-delete.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(97, 'mainmenu', 'Key Icon', 'key-icon', '', 'separator', -2, 0, 0, 0, 16, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-key.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(98, 'mainmenu', 'Unlock Icon', 'unlock-icon', '', 'separator', -2, 0, 0, 0, 15, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=icon-key2.png\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(99, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 14, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=Single Column Example\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(100, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 13, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(101, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 12, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(102, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 11, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(103, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 10, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(104, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 9, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(105, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 8, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(106, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(107, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(108, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=Multi-Column Example\nfusion_columns=2\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(109, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(110, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(111, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(112, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 17, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(113, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 18, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(114, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 33, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(115, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 32, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(116, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 31, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(117, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 30, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(118, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 29, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(119, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 28, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(120, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 27, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=Single Column Example\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(121, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 26, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(122, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 46, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(123, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 25, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(124, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 24, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(125, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 23, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(126, 'mainmenu', 'Child Item', 'child-item', '', 'separator', -2, 0, 0, 0, 22, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(127, 'keyconcepts', 'MultiMedia Archives', 'archives', 'index.php?option=com_phocagallery&view=categories', 'component', 0, 0, 76, 0, 21, 62, '2010-03-20 18:32:34', 0, 0, 0, 0, 'image=-1\nimage_align=right\nshow_pagination_categories=0\nshow_pagination_category=1\nshow_pagination_limit_categories=0\nshow_pagination_limit_category=1\ndisplay_cat_name_title=1\ncategories_columns=2\nequal_percentage_width=\ndisplay_image_categories=\ncategories_box_width=200\nimage_categories_size=\ncategories_image_ordering=\ncategories_display_avatar=\ndisplay_subcategories=\ndisplay_empty_categories=\nhide_categories=\ndisplay_access_category=\ndefault_pagination_categories=\npagination_categories=\nfont_color=\nbackground_color=\nbackground_color_hover=\nimage_background_color=\nimage_background_shadow=\nborder_color=\nborder_color_hover=\nmargin_box=\npadding_box=\ndisplay_name=\ndisplay_icon_detail=\ndisplay_icon_download=\ndisplay_icon_folder=\nfont_size_name=\nchar_length_name=\ncategory_box_space=\ndisplay_categories_sub=\ndisplay_subcat_page=\ndisplay_category_icon_image=\ncategory_image_ordering=\ndisplay_back_button=\ndisplay_categories_back_button=\ndefault_pagination_category=\npagination_category=\ndisplay_img_desc_box=\nfont_size_img_desc=\nimg_desc_box_height=\nchar_length_img_desc=\ndisplay_categories_cv=\ndisplay_subcat_page_cv=\ndisplay_category_icon_image_cv=\ncategory_image_ordering_cv=\ndisplay_back_button_cv=\ndisplay_categories_back_button_cv=\ncategories_columns_cv=\ndisplay_image_categories_cv=\nimage_categories_size_cv=\ndetail_window=\ndetail_window_background_color=\nmodal_box_overlay_color=\nmodal_box_overlay_opacity=\nmodal_box_border_color=\nmodal_box_border_width=\nsb_slideshow_delay=\nsb_lang=\nhighslide_class=\nhighslide_opacity=\nhighslide_outline_type=\nhighslide_fullimg=\nhighslide_close_button=\nhighslide_slideshow=\njak_slideshow_delay=\njak_orientation=\njak_description=\njak_description_height=\ndisplay_description_detail=\ndisplay_title_description=\nfont_size_desc=\nfont_color_desc=\ndescription_detail_height=\ndescription_lightbox_font_size=\ndescription_lightbox_font_color=\ndescription_lightbox_bg_color=\nslideshow_delay=\nslideshow_pause=\nslideshow_random=\ndetail_buttons=\nphocagallery_width=\nphocagallery_center=\ncategory_ordering=\nimage_ordering=\ngallery_metadesc=\ngallery_metakey=\nenable_user_cp=\nenable_upload_avatar=\nenable_avatar_approve=\nenable_usercat_approve=\nenable_usersubcat_approve=\nuser_subcat_count=\nmax_create_cat_char=\nenable_userimage_approve=\nmax_upload_char=\nupload_maxsize=\nupload_maxres_width=\nupload_maxres_height=\nuser_images_max_size=\nenable_java=\nenable_java_admin=\njava_resize_width=\njava_resize_height=\njava_box_width=\njava_box_height=\ndisplay_rating=\ndisplay_rating_img=\ndisplay_comment=\ndisplay_comment_img=\ncomment_width=\nmax_comment_char=\nexternal_comment_system=\nenable_piclens=\nstart_piclens=\npiclens_image=\nswitch_image=\nswitch_width=\nswitch_height=\nenable_overlib=\nol_bg_color=\nol_fg_color=\nol_tf_color=\nol_cf_color=\noverlib_overlay_opacity=\noverlib_image_rate=\ncreate_watermark=\nwatermark_position_x=\nwatermark_position_y=\ndisplay_icon_vm=\ndisplay_category_statistics=\ndisplay_main_cat_stat=\ndisplay_lastadded_cat_stat=\ncount_lastadded_cat_stat=\ndisplay_mostviewed_cat_stat=\ncount_mostviewed_cat_stat=\ndisplay_camera_info=\nexif_information=\ndisplay_categories_geotagging=\ncategories_lng=\ncategories_lat=\ncategories_zoom=\ncategories_map_width=\ncategories_map_height=\ndisplay_icon_geotagging=\ndisplay_category_geotagging=\ncategory_map_width=\ncategory_map_height=\npagination_thumbnail_creation=\nclean_thumbnails=\nenable_thumb_creation=\ncrop_thumbnail=\njpeg_quality=\nenable_picasa_loading=\npicasa_load_pagination=\nicon_format=\nlarge_image_width=\nlarge_image_height=\nmedium_image_width=\nmedium_image_height=\nsmall_image_width=\nsmall_image_height=\nfront_modal_box_width=\nfront_modal_box_height=\nadmin_modal_box_width=\nadmin_modal_box_height=\nfolder_permissions=\njfile_thumbs=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=-1\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(128, 'keyconcepts', 'Recent Showcase', 'recentshowcase', '#', 'url', 0, 0, 0, 0, 22, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_image=-1\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=-1\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(129, 'mainmenu', 'RokNavMenu', 'roknavmenu', 'index.php?Itemid=61', 'menulink', -2, 0, 0, 0, 21, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_item=61\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0);
INSERT INTO `jos_menu` (`id`, `menutype`, `name`, `alias`, `link`, `type`, `published`, `parent`, `componentid`, `sublevel`, `ordering`, `checked_out`, `checked_out_time`, `pollid`, `browserNav`, `access`, `utaccess`, `params`, `lft`, `rgt`, `home`) VALUES
(130, 'mainmenu', 'RokBox', 'rokbox', 'index.php?Itemid=61', 'menulink', -2, 0, 0, 0, 20, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_item=61\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(131, 'mainmenu', 'RokGZipper', 'rokgzipper', 'index.php?Itemid=61', 'menulink', -2, 0, 0, 0, 19, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_item=61\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(132, 'mainmenu', 'RokCandy', 'rokcandy', 'index.php?Itemid=61', 'menulink', -2, 0, 0, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'menu_item=61\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\n\n', 0, 0, 0),
(146, 'intranetmenu', 'Latest Updates', 'latestupdates', 'index.php?option=com_content&view=category&layout=blog&id=42', 'component', 1, 0, 20, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(147, 'intranetmenu', 'How to Manage the site', 'management', 'index.php?option=com_content&view=article&id=53', 'component', 1, 0, 20, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(148, 'intranetmenu', 'Internal Msg. System', 'internalmessaging', 'index.php?option=com_uddeim', 'component', 1, 0, 64, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'fusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(149, 'othermenu', 'Privacy Policy', 'privacy', 'index.php?option=com_content&view=article&id=61', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(150, 'othermenu', 'Terms of Use', 'termsandconditions', 'index.php?option=com_content&view=article&id=60', 'component', 1, 0, 20, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(151, 'othermenu', 'Accessibility', 'accessibility-statement', 'index.php?option=com_content&view=article&id=58', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_menu_types`
--

DROP TABLE IF EXISTS `jos_menu_types`;
CREATE TABLE IF NOT EXISTS `jos_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `jos_menu_types`
--

INSERT INTO `jos_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(2, 'usermenu', 'User Menu', 'A Menu for logged in Users'),
(3, 'topmenu', 'Top Menu', 'Top level navigation'),
(4, 'othermenu', 'Resources', 'Additional links'),
(5, 'intranetmenu', 'Intranet Menu', 'Intranet Menu for Youth Leaders'),
(6, 'keyconcepts', 'Key Concepts', 'This describes some critical information for new Users.');

-- --------------------------------------------------------

--
-- Table structure for table `jos_messages`
--

DROP TABLE IF EXISTS `jos_messages`;
CREATE TABLE IF NOT EXISTS `jos_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(11) NOT NULL DEFAULT '0',
  `priority` int(1) unsigned NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_messages_cfg`
--

DROP TABLE IF EXISTS `jos_messages_cfg`;
CREATE TABLE IF NOT EXISTS `jos_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_migration_backlinks`
--

DROP TABLE IF EXISTS `jos_migration_backlinks`;
CREATE TABLE IF NOT EXISTS `jos_migration_backlinks` (
  `itemid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `sefurl` text NOT NULL,
  `newurl` text NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_modules`
--

DROP TABLE IF EXISTS `jos_modules`;
CREATE TABLE IF NOT EXISTS `jos_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) DEFAULT NULL,
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `numnews` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `control` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=156 ;

--
-- Dumping data for table `jos_modules`
--

INSERT INTO `jos_modules` (`id`, `title`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `published`, `module`, `numnews`, `access`, `showtitle`, `params`, `iscore`, `client_id`, `control`) VALUES
(2, 'Login', '', 1, 'login', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, '', 1, 1, ''),
(3, 'Popular', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_popular', 0, 2, 1, '', 0, 1, ''),
(4, 'Recent added Articles', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_latest', 0, 2, 1, 'ordering=c_dsc\nuser_id=0\ncache=0\n\n', 0, 1, ''),
(5, 'Menu Stats', '', 5, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_stats', 0, 2, 1, '', 0, 1, ''),
(6, 'Unread Messages', '', 1, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_unread', 0, 2, 1, '', 1, 1, ''),
(7, 'Online Users', '', 2, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_online', 0, 2, 1, '', 1, 1, ''),
(8, 'Toolbar', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', 1, 'mod_toolbar', 0, 2, 1, '', 1, 1, ''),
(9, 'Quick Icons', '', 1, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_quickicon', 0, 2, 1, '', 1, 1, ''),
(10, 'Logged in Users', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_logged', 0, 2, 1, '', 0, 1, ''),
(11, 'Footer', '', 0, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_footer', 0, 0, 1, '', 1, 1, ''),
(12, 'Admin Menu', '', 1, 'menu', 0, '0000-00-00 00:00:00', 1, 'mod_menu', 0, 2, 1, '', 0, 1, ''),
(13, 'Admin SubMenu', '', 1, 'submenu', 0, '0000-00-00 00:00:00', 1, 'mod_submenu', 0, 2, 1, '', 0, 1, ''),
(14, 'User Status', '', 1, 'status', 0, '0000-00-00 00:00:00', 1, 'mod_status', 0, 2, 1, '', 0, 1, ''),
(15, 'Title', '', 1, 'title', 0, '0000-00-00 00:00:00', 1, 'mod_title', 0, 2, 1, '', 0, 1, ''),
(16, 'Site Poll', '', 7, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_poll', 0, 0, 1, 'id=14\nmoduleclass_sfx=title3\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(17, 'User Menu', '', 1, 'feature-b', 0, '0000-00-00 00:00:00', 1, 'mod_mainmenu', 0, 1, 1, 'menutype=usermenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(18, 'Login Form', '', 9, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, 'cache=0\nmoduleclass_sfx=title5\npretext=\nposttext=\nlogin=\nlogout=\ngreeting=1\nname=0\nusesecure=0\n\n', 1, 0, ''),
(19, 'Latest News', '', 1, 'maintop-b', 0, '0000-00-00 00:00:00', 0, 'mod_latestnews', 0, 0, 1, 'count=5\nordering=c_dsc\nuser_id=0\nshow_front=1\nsecid=\ncatid=1\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 1, 0, ''),
(21, 'Who''s Online', '', 8, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_whosonline', 0, 0, 1, 'cache=0\nshowmode=0\nmoduleclass_sfx=title2\n\n', 0, 0, ''),
(22, 'Popular', '', 1, 'maintop-a', 0, '0000-00-00 00:00:00', 0, 'mod_mostread', 0, 0, 1, 'moduleclass_sfx=\nshow_front=1\ncount=5\ncatid=1\nsecid=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(25, 'Newsflash', '', 1, 'content-bottom-a', 0, '0000-00-00 00:00:00', 0, 'mod_newsflash', 0, 0, 1, 'catid=3\nlayout=default\nimage=0\nlink_titles=\nshowLastSeparator=1\nreadmore=0\nitem_title=0\nitems=\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(35, 'Breadcrumbs', '', 1, 'breadcrumb', 0, '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 0, 0, 1, 'showHome=1\nhomeText=Home\nshowLast=1\nseparator=\nmoduleclass_sfx=\ncache=0\n\n', 1, 0, ''),
(41, 'Welcome to Joomla!', '<div style="padding: 5px">  <p>   Congratulations on choosing Joomla! as your content management system. To   help you get started, check out these excellent resources for securing your   server and pointers to documentation and other helpful resources. </p> <p>   <strong>Security</strong><br /> </p> <p>   On the Internet, security is always a concern. For that reason, you are   encouraged to subscribe to the   <a href="http://feedburner.google.com/fb/a/mailverify?uri=JoomlaSecurityNews" target="_blank">Joomla!   Security Announcements</a> for the latest information on new Joomla! releases,   emailed to you automatically. </p> <p>   If this is one of your first Web sites, security considerations may   seem complicated and intimidating. There are three simple steps that go a long   way towards securing a Web site: (1) regular backups; (2) prompt updates to the   <a href="http://www.joomla.org/download.html" target="_blank">latest Joomla! release;</a> and (3) a <a href="http://docs.joomla.org/Security_Checklist_2_-_Hosting_and_Server_Setup" target="_blank" title="good Web host">good Web host</a>. There are many other important security considerations that you can learn about by reading the <a href="http://docs.joomla.org/Category:Security_Checklist" target="_blank" title="Joomla! Security Checklist">Joomla! Security Checklist</a>. </p> <p>If you believe your Web site was attacked, or you think you have discovered a security issue in Joomla!, please do not post it in the Joomla! forums. Publishing this information could put other Web sites at risk. Instead, report possible security vulnerabilities to the <a href="http://developer.joomla.org/security/contact-the-team.html" target="_blank" title="Joomla! Security Task Force">Joomla! Security Task Force</a>.</p><p><strong>Learning Joomla!</strong> </p> <p>   A good place to start learning Joomla! is the   "<a href="http://docs.joomla.org/beginners" target="_blank">Absolute Beginner''s   Guide to Joomla!.</a>" There, you will find a Quick Start to Joomla!   <a href="http://help.joomla.org/ghop/feb2008/task048/joomla_15_quickstart.pdf" target="_blank">guide</a>   and <a href="http://help.joomla.org/ghop/feb2008/task167/index.html" target="_blank">video</a>,   amongst many other tutorials. The   <a href="http://community.joomla.org/magazine/view-all-issues.html" target="_blank">Joomla!   Community Magazine</a> also has   <a href="http://community.joomla.org/magazine/article/522-introductory-learning-joomla-using-sample-data.html" target="_blank">articles   for new learners</a> and experienced users, alike. A great place to look for   answers is the   <a href="http://docs.joomla.org/Category:FAQ" target="_blank">Frequently Asked   Questions (FAQ)</a>. If you are stuck on a particular screen in the   Administrator (which is where you are now), try clicking the Help toolbar   button to get assistance specific to that page. </p> <p>   If you still have questions, please feel free to use the   <a href="http://forum.joomla.org/" target="_blank">Joomla! Forums.</a> The forums   are an incredibly valuable resource for all levels of Joomla! users. Before   you post a question, though, use the forum search (located at the top of each   forum page) to see if the question has been asked and answered. </p> <p>   <strong>Getting Involved</strong> </p> <p>   <a name="twjs" title="twjs"></a> If you want to help make Joomla! better, consider getting   involved. There are   <a href="http://www.joomla.org/about-joomla/contribute-to-joomla.html" target="_blank">many ways   you can make a positive difference.</a> Have fun using Joomla!.</p></div>', 0, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 2, 1, 'moduleclass_sfx=\n\n', 1, 1, ''),
(42, 'Joomla! Security Newsfeed', '', 6, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_feed', 0, 0, 1, 'cache=1\ncache_time=15\nmoduleclass_sfx=\nrssurl=http://feeds.joomla.org/JoomlaSecurityNews\nrssrtl=0\nrsstitle=1\nrssdesc=0\nrssimage=1\nrssitems=1\nrssitemdesc=1\nword_count=0\n\n', 0, 1, ''),
(155, 'Live Search Box', '', 0, 'header-b', 0, '0000-00-00 00:00:00', 1, 'mod_rokajaxsearch', 0, 0, 0, 'moduleclass_sfx=\nsearch_page=index.php?option=com_search&view=search&tmpl=component\nadv_search_page=index.php?option=com_search&view=search\ninclude_css=1\ntheme=light\nsearchphrase=any\nordering=newest\nlimit=10\nperpage=3\nwebsearch=1\nblogsearch=0\nimagesearch=0\nvideosearch=0\nwebsearch_api=\nshow_pagination=1\nsafesearch=MODERATE\nimage_size=MEDIUM\nshow_estimated=1\nhide_divs=\ninclude_link=1\nshow_description=1\ninclude_category=1\nshow_readmore=1\n\n', 0, 0, ''),
(44, 'Main Menu', '', 1, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_roknavmenu', 0, 0, 1, 'menutype=mainmenu\nlimit_levels=0\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\ntheme=/templates/rt_crystalline_j15/html/mod_roknavmenu/themes/gantry-splitmenu\nenable_js=0\nopacity=1\neffect=slidefade\nhidedelay=500\nmenu_animation=Quad.easeOut\nmenu_duration=400\npill=0\npill_animation=Back.easeOut\npill_duration=400\ncentered-offset=0\ntweakInitial_x=0\ntweakInitial_y=0\ntweakSubsequent_x=0\ntweakSubsequent_y=0\nenable_current_id=0\nroknavmenu_splitmenu_enable_current_id=0\nroknavmenu_fusion_load_css=1\nroknavmenu_fusion_enable_js=0\nroknavmenu_fusion_opacity=1\nroknavmenu_fusion_effect=slidefade\nroknavmenu_fusion_hidedelay=500\nroknavmenu_fusion_menu_animation=Quad.easeOut\nroknavmenu_fusion_menu_duration=400\nroknavmenu_fusion_pill=0\nroknavmenu_fusion_pill_animation=Back.easeOut\nroknavmenu_fusion_pill_duration=400\nroknavmenu_fusion_centeredOffset=0\nroknavmenu_fusion_tweakInitial_x=0\nroknavmenu_fusion_tweakInitial_y=0\nroknavmenu_fusion_tweakSubsequent_x=0\nroknavmenu_fusion_tweakSubsequent_y=0\nroknavmenu_fusion_enable_current_id=0\ncustom_layout=default.php\ncustom_formatter=default.php\nurl_type=relative\ncache=0\nmodule_cache=1\ncache_time=900\ntag_id=\nclass_sfx=\nmoduleclass_sfx=title1\nmaxdepth=10\nmenu_images=0\nmenu_images_link=0\n\n', 1, 0, ''),
(45, 'RokTabs', '', 0, 'showcase-a', 0, '0000-00-00 00:00:00', 1, 'mod_roktabs', 0, 0, 0, 'style=base\ncontent_type=joomla\nsecid=0\ncatid=38\nshow_front=1\nitemsOrdering=order\nwidth=960\ntabs_count=0\nduration=600\ntransition_type=scrolling\ntransition_fx=Quad.easeInOut\nlinksMargins=0\ntabs_position=bottom\ntabs_event=click\ntabs_title=content\ntabs_incremental=Tab\ntabs_hideh6=1\ntabs_showicons=1\ntabs_iconside=left\ntabs_iconpath=images/stories/demo/tabs\ntabs_icon=applications.png,color.png,new.png,lock.png,user.png,movies.png\nautoplay=1\nautoplay_delay=6000\nmoduleclass_sfx=flush\ncache=0\nmodule_cache=1\ncache_time=900\n\n', 0, 0, ''),
(124, 'Content Highlight', '<p><img src="images/stories/demo/frontpage/ft-b.jpg" alt="Mar10 Demo Image" class="demo-fp-img" /></p>\r\n<p>Content highlight goes in here<br />New content will be highlighted in this box</p>\r\n[readon2 url="index.php?option=com_content&amp;view=article&amp;id=54&amp;Itemid=61"]Read More[/readon2]', 2, 'feature-b', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=flushbottom\n\n', 0, 0, ''),
(125, 'Images from Recent Events', '<img alt="Past Youth Events" src="images/stories/youth website 040.jpg" height="69" width="124" /><span class="demo-sep">&nbsp;</span> <img alt="Past Youth Events" src="images/stories/100_1015.jpg" height="69" width="124" /><span class="demo-sep">&nbsp;</span> <img alt="Past Youth Events" src="images/stories/100_4148.jpg" height="69" width="124" />\r\n<div class="demo-horiz-sep"></div>\r\n<img alt="Past Youth Events" src="images/stories/100_4137.jpg" height="69" width="124" /><span class="demo-sep">&nbsp;</span> <img alt="Past Youth Events" src="images/stories/100_2435.jpg" height="69" width="124" /><span class="demo-sep">&nbsp;</span> <img alt="Past Youth Events" src="images/stories/love feast feb 2009 009.jpg" height="69" width="124" />\r\n<div class="demo-horiz-sep"></div>\r\n[readon2 url="index.php?option=com_content&amp;view=article&amp;id=47&amp;Itemid=54"]See Full Picture Gallery[/readon2]', 0, 'feature-c', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=flushbottom\n\n', 0, 0, ''),
(153, 'Site Links', '', 2, 'bottom-d', 0, '0000-00-00 00:00:00', 1, 'mod_roknavmenu', 0, 0, 1, 'menutype=othermenu\nlimit_levels=0\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\ntheme=/templates/rt_crystalline_j15/html/mod_roknavmenu/themes\\gantry-fusion\nenable_js=0\nopacity=1\neffect=slidefade\nhidedelay=500\nmenu_animation=Quad.easeOut\nmenu_duration=400\npill=0\npill_animation=Back.easeOut\npill_duration=400\ncentered-offset=0\ntweakInitial_x=0\ntweakInitial_y=0\ntweakSubsequent_x=0\ntweakSubsequent_y=0\nenable_current_id=0\nroknavmenu_splitmenu_enable_current_id=0\nroknavmenu_fusion_load_css=1\nroknavmenu_fusion_enable_js=0\nroknavmenu_fusion_opacity=1\nroknavmenu_fusion_effect=slidefade\nroknavmenu_fusion_hidedelay=500\nroknavmenu_fusion_menu_animation=Quad.easeOut\nroknavmenu_fusion_menu_duration=400\nroknavmenu_fusion_pill=0\nroknavmenu_fusion_pill_animation=Back.easeOut\nroknavmenu_fusion_pill_duration=400\nroknavmenu_fusion_centeredOffset=0\nroknavmenu_fusion_tweakInitial_x=0\nroknavmenu_fusion_tweakInitial_y=0\nroknavmenu_fusion_tweakSubsequent_x=0\nroknavmenu_fusion_tweakSubsequent_y=0\nroknavmenu_fusion_enable_current_id=0\ncustom_layout=default.php\ncustom_formatter=default.php\nurl_type=relative\ncache=0\nmodule_cache=1\ncache_time=900\ntag_id=\nclass_sfx=\nmoduleclass_sfx=title1\nmaxdepth=10\nmenu_images=0\nmenu_images_link=0\n\n', 0, 0, ''),
(98, 'Copyright Module Position - Default Styling', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum at sem ut ipsum vestibulum euismod. Mauris et massa porta leo facilisis feugiat. Suspendisse id neque a sem facilisis blandit. Aliquam sem leo, commodo ut, rutrum auctor, iaculis nec, eros. Aenean massa. Mauris tincidunt. Vivamus consectetur, tortor sit amet dictum sagittis, urna lectus dapibus metus, ut congue ligula odio sed nunc. Suspendisse potenti.</p>', 1, 'copyright', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(120, 'Latest Features', '<p>Below is a list of the top features of Gantry 2.0:</p>\r\n\r\n<p class="dropcap"><span class="dropcap">1</span><a target="_blank" href="http://www.rockettheme.com/blog/coding/543-gantry-framework-part-5-20-features"><strong>RTL Support</strong></a>: automatically flip the entire modular structure of Gantry when RTL languages are detected.</p>\r\n<p class="dropcap"><span class="dropcap">2</span><a target="_blank" href="http://www.rockettheme.com/blog/coding/543-gantry-framework-part-5-20-features"><strong>Custom Presets</strong></a>: an intuitive and simple user interface for creating and saving your own custom presets.</p>\r\n<p class="dropcap"><span class="dropcap">3</span><a target="_blank" href="http://www.rockettheme.com/blog/coding/543-gantry-framework-part-5-20-features"><strong>Menu Item Control</strong></a>: set the template options on a per menu item basis, such as the layout controls.</p>\r\n<p class="dropcap"><span class="dropcap">4</span><a target="_blank" href="http://www.rockettheme.com/blog/coding/543-gantry-framework-part-5-20-features"><strong>Gantry GZipper</strong></a>: RokGZipper functionality built straight into the Gantry core for its CSS/JS items.</p>', 5, 'sidebar-b', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(121, 'Blog Docs', '<p>We advise reading the blog entries below to gain a greater understanding of Gantry. They go into much greater detail than the overview on this page.</p>\r\n\r\n<ul>\r\n  <li><a href="http://www.rockettheme.com/blog/coding/511-gantry-framework-overview" target="_blank">Part 1: Overview</a></li>\r\n  <li><a href="http://www.rockettheme.com/blog/coding/512-gantry-framework-layouts" target="_blank">Part 2: Layouts</a></li>\r\n  <li><a href="http://www.rockettheme.com/blog/coding/520-gantry-framework-body-features" target="_blank">Part 3: Body + Features</a></li>\r\n  <li><a href="http://www.rockettheme.com/blog/coding/534-gantry-framework-going-gpl" target="_blank">Part 4: v2.0 Going GPL</a></li>\r\n  <li><a href="http://www.rockettheme.com/blog/coding/543-gantry-framework-part-5-20-features" target="_blank">Part 5: 2.0 Features</a></li>\r\n</ul>', 6, 'sidebar-b', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(122, 'RTL Preview', '<p>Below is a preview of the demo frontpage in RTL mode, please click on the image for a larger preview via RokBox:</p>\r\n\r\n<p><a rel="rokbox[715 1687]" title="Preview of the Demo Frontpage in RTL mode" href="images/stories/demo/general/rtl-preview-full.jpg"><img class="rt-image" src="images/stories/demo/general/rtl-preview.jpg" alt="RTL Preview" /></a></p>', 7, 'sidebar-b', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(123, 'Color Chooser', '<p><img src="images/stories/demo/frontpage/ft-a.jpg" alt="Mar10 Demo Image" class="demo-fp-img" /></p>\r\n<p>\r\n	<em class="bold">Customize the theme live with the Color Chooser.</em>\r\n	<br />\r\n	Save your custom presets by logging in as a Super Admin.\r\n</p>\r\n[readon2 url="index.php?option=com_content&amp;view=article&amp;id=48&amp;Itemid=55"]Read More[/readon2]', 2, 'feature-a', 62, '2010-03-17 16:09:46', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=flushbottom\n\n', 0, 0, ''),
(126, 'Recent Testimonies', '<img style="margin-top: 0px; margin-right: 5px; float: left;" alt="Seyi''s Testimony" src="images/stories/seyioyetugasmall.jpg" height="98" width="89" />&nbsp;<span><em class="bold">Power to save and to deliver - Seyi Oyetuga</em> <br /></span>I want to thank God for the power to save and to deliver...<br />\r\n<div class="ct-read">[readon url="index.php?option=com_content&amp;view=article&amp;id=59&amp;Itemid=66"]Read more[/readon]</div>', 0, 'content-top-c', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=flushbottom\n\n', 0, 0, ''),
(127, 'Cross Browser Compatible', '<p><em class="bold">Cross Browser Compatible</em></p>\r\n<p>This theme is compatible with the major and modern browsers, plus limited support for IE6.</p>\r\n[readon url="index.php?option=com_content&amp;view=article&amp;id=46&amp;Itemid=53"]Learn More[/readon]', 6, 'sidebar-a', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(128, 'Recent Testimonies II', '<h3><img style="margin: 0px 5px 0px 0px; float: left;" src="images/stories/kemi_small.jpg" alt="Kemi''s Testimony" height="93" width="96" /></h3>\r\n<strong><em>Hatred gave way for salvation - Kemi </em></strong>\r\n<span>Growing up in a christian home did not make me one...</span><br />[readon2 url="index.php?option=com_content&amp;view=article&amp;id=59&amp;Itemid=66"]Read more[/readon2]', 0, 'content-top-c', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=flushtop\n\n', 0, 0, ''),
(129, 'Latest Upload', '<img src="images/stories/ct-b.jpg" alt="Latest Multimedia upload" />', 0, 'content-top-b', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=flushbottom\n\n', 0, 0, ''),
(130, 'About Us', '<p><img src="images/stories/demo/frontpage/rt1.jpg" alt="Mar10 Demo Image" class="demo-fp-img" /></p>\r\n<p>This is the website fot The Apostolic Faith Church (UK) youths and our aim is simple - winning souls for the kingdom of God.</p>\r\n<p>A bit more content in this area would help. *See youth leaders for frontpage content.</p>\r\n[readon url="index.php?option=com_content&amp;view=article&amp;id=46&amp;Itemid=53"]Read More[/readon]', 4, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=bg3\n\n', 0, 0, ''),
(131, 'Worship with Us @', '<p>Apostolic Faith Church</p>\r\n<p>95 Fenham Road, London</p>\r\n<p>SE12 AB34</p>', 1, 'bottom-a', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(132, 'Year Calendar', '<p>A placeholder for calendar will go in here</p>\r\n<p>[readon url="index.php?option=com_content&amp;view=article&amp;id=55&amp;Itemid=62"]More Details[/readon]</p>', 0, 'bottom-b', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(133, 'Demo Title', '<p>This site needs content</p>\r\n<p>This area holds content items which may+ be of interest to site users and visitors</p>\r\n[readon url="index.php?option=com_content&amp;view=article&amp;id=57&amp;Itemid=64"]Learn More[/readon]', 0, 'bottom-c', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(134, 'Site Links', '<p><em class="bold">Validate Crystalline Live</em></p>\r\n<p>The template is fully compliant with the <strong>XHTML 1.0 Transitional</strong> and <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fdemo.rockettheme.com%2Fmar10&amp;profile=css3&amp;usermedium=all&amp;warning=1&amp;lang=en" target="_blank">CSS3 standards</a>, as set by the World Wide Web Consortium.</p>\r\n[readon url="http://validator.w3.org/check?uri=http://demo.rockettheme.com/mar10/"]Validate (HTML)[/readon]', 0, 'bottom-d', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(140, 'Member Survey', '', 5, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_poll', 0, 0, 1, 'id=15\nmoduleclass_sfx=bg2\ncache=0\ncache_time=900\n\n', 0, 0, ''),
(136, 'Site Navigation', '', 3, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_roknavmenu', 0, 0, 1, 'menutype=mainmenu\nlimit_levels=0\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\ntheme=/templates/rt_crystalline_j15/html/mod_roknavmenu/themes/gantry-splitmenu\nenable_js=0\nopacity=1\neffect=slidefade\nhidedelay=500\nmenu_animation=Quad.easeOut\nmenu_duration=400\npill=0\npill_animation=Back.easeOut\npill_duration=400\ncentered-offset=0\ntweakInitial_x=0\ntweakInitial_y=0\ntweakSubsequent_x=0\ntweakSubsequent_y=0\nenable_current_id=0\nroknavmenu_splitmenu_enable_current_id=0\nroknavmenu_fusion_load_css=1\nroknavmenu_fusion_enable_js=0\nroknavmenu_fusion_opacity=1\nroknavmenu_fusion_effect=slidefade\nroknavmenu_fusion_hidedelay=500\nroknavmenu_fusion_menu_animation=Quad.easeOut\nroknavmenu_fusion_menu_duration=400\nroknavmenu_fusion_pill=0\nroknavmenu_fusion_pill_animation=Back.easeOut\nroknavmenu_fusion_pill_duration=400\nroknavmenu_fusion_centeredOffset=0\nroknavmenu_fusion_tweakInitial_x=0\nroknavmenu_fusion_tweakInitial_y=0\nroknavmenu_fusion_tweakSubsequent_x=0\nroknavmenu_fusion_tweakSubsequent_y=0\nroknavmenu_fusion_enable_current_id=0\ncustom_layout=default.php\ncustom_formatter=default.php\nurl_type=relative\ncache=0\nmodule_cache=1\ncache_time=900\ntag_id=\nclass_sfx=\nmoduleclass_sfx=bg1\nmaxdepth=10\nmenu_images=0\nmenu_images_link=0\n\n', 0, 0, ''),
(142, 'Who''s Online', '', 10, 'sidebar-a', 0, '0000-00-00 00:00:00', 0, 'mod_whosonline', 0, 0, 1, 'cache=0\nshowmode=0\nmoduleclass_sfx=bg5\n\n', 0, 0, ''),
(143, 'Site Login', '', 1, 'popup', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, 'cache=0\nmoduleclass_sfx=\npretext=\nposttext=\nlogin=\nlogout=\ngreeting=1\nname=0\nusesecure=0\n\n', 0, 0, ''),
(144, 'K2 Comments', '', 1, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_k2_comments', 0, 0, 1, 'comments_limit=5\ncomments_word_limit=10\ncommenterName=1\ncommentAvatar=1\ncommentDate=1\ncommentDateFormat=absolute\ncommentLink=1\nitemTitle=1\nitemCategory=1\nfeed=1\ncommenters_limit=5\ncommenterAvatar=1\ncommenterLink=1\ncommenterCommentsCounter=1\ncommenterLatestComment=1\n', 0, 0, ''),
(145, 'K2 Content', '', 2, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_k2_content', 0, 0, 1, 'getTemplate=Default\nsource=filter\nitems_limit=10\nFeaturedItems=1\nitemTitle=1\nitemAuthor=1\nitemAuthorAvatar=1\nitemIntroText=1\nitemImage=1\nitemImgSize=XSmall\nitemVideo=1\nitemVideoCaption=1\nitemVideoCredits=1\nitemAttachments=1\nitemTags=1\nitemCategory=1\nitemDateCreated=1\nitemHits=1\nitemReadMore=1\nitemCommentsCounter=1\nfeed=1\nitemCustomLinkURL=http://\n', 0, 0, ''),
(146, 'K2 Login', '', 3, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_k2_login', 0, 0, 1, 'greeting=1\nname=1\nuserAvatar=1\n', 0, 0, ''),
(147, 'K2 Tools', '', 4, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_k2_tools', 0, 0, 1, 'archiveItemsCounter=1\nauthorItemsCounter=1\nauthorAvatar=1\nauthorLatestItem=1\ncategoriesListItemsCounter=1\nwidth=20\nmin_size=75\nmax_size=300\ncloud_limit=30\n', 0, 0, ''),
(148, 'K2 QuickIcons (admin)', '', 99, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_k2_quickicons', 0, 2, 1, 'modCSSStyling=1\nmodLogo=1\n', 0, 1, ''),
(149, 'Dynamic XML Sitemap', '', 5, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_dynamic_xmlsitemap', 0, 0, 1, 'catsec=1\n', 0, 0, ''),
(150, 'Gavick Tab GK1', '', 6, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_gk_tab', 0, 0, 1, 'module_id=tabmix1\nmoduleHeight=100px\n@spacer= \nnews_content_header_pos=1\nnews_content_image_pos=1\nnews_content_text_pos=1\nnews_content_info_pos=1\nnews_content_readmore_pos=2\nnews_readmore_text=READMORE\nnews_header_link=1\nnews_image_link=1\nnews_author=1\nnews_cats=1\nnews_date=1\nnews_header_order=1\nnews_image_order=2\nnews_text_order=3\nnews_info_order=4\nnews_limit=30\nclean_xhtml=1\ndate_format=D, d M Y\n@spacer= \nactivator=click\nanimationType=1\nanimationSpeed=1000\nanimationInterval=5000\nanimationFun=linear\n@spacer= \nbuttons=1\nstyleCSS=style1\n@spacer= \nfixedHeightValue=200\n', 0, 0, ''),
(151, 'Internal Mailbox', '', 1, 'feature-a', 0, '0000-00-00 00:00:00', 1, 'mod_uddeim_mailbox', 0, 2, 1, 'moduleclass_sfx=\nuddshownew=1\nuddshowinbox=1\nuddshowoutbox=1\nuddshowtrashcan=1\nuddshowarchive=1\nuddshowcontacts=1\nuddshowsettings=1\nuddshowcompose=1\nuddshowicons=1\n\n', 0, 0, ''),
(152, 'uddeIM Statistics', '', 7, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_uddeim_statistics', 0, 0, 1, 'uddshowall=1\nuddshow7=1\nuddshow30=1\nuddshow365=1\n', 0, 0, ''),
(154, 'Banner', '', 0, 'header-b', 0, '0000-00-00 00:00:00', 1, 'mod_banners', 0, 0, 0, 'target=0\ncount=1\ncid=1\ncatid=13\ntag_search=0\nordering=0\nheader_text=\nfooter_text=\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_modules_menu`
--

DROP TABLE IF EXISTS `jos_modules_menu`;
CREATE TABLE IF NOT EXISTS `jos_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_modules_menu`
--

INSERT INTO `jos_modules_menu` (`moduleid`, `menuid`) VALUES
(16, 53),
(16, 55),
(16, 57),
(16, 58),
(16, 59),
(16, 60),
(16, 62),
(16, 63),
(16, 64),
(16, 65),
(16, 66),
(16, 67),
(16, 68),
(16, 133),
(17, 0),
(18, 53),
(18, 55),
(18, 57),
(18, 59),
(18, 60),
(18, 64),
(18, 65),
(18, 66),
(18, 67),
(18, 133),
(19, 27),
(21, 27),
(21, 48),
(21, 50),
(21, 53),
(21, 55),
(21, 57),
(21, 58),
(21, 59),
(21, 60),
(21, 61),
(21, 62),
(21, 63),
(21, 64),
(21, 65),
(21, 66),
(21, 67),
(21, 68),
(21, 75),
(21, 76),
(21, 133),
(22, 27),
(25, 27),
(25, 37),
(25, 48),
(25, 50),
(25, 75),
(25, 76),
(35, 11),
(35, 12),
(35, 13),
(35, 14),
(35, 15),
(35, 16),
(35, 17),
(35, 18),
(35, 20),
(35, 24),
(35, 27),
(35, 28),
(35, 29),
(35, 30),
(35, 37),
(35, 38),
(35, 40),
(35, 43),
(35, 44),
(35, 45),
(35, 46),
(35, 47),
(35, 48),
(35, 50),
(35, 51),
(35, 52),
(35, 53),
(35, 54),
(35, 55),
(35, 56),
(35, 57),
(35, 58),
(35, 59),
(35, 60),
(35, 61),
(35, 62),
(35, 63),
(35, 64),
(35, 65),
(35, 66),
(35, 67),
(35, 68),
(35, 69),
(35, 70),
(35, 71),
(35, 72),
(35, 73),
(35, 75),
(35, 76),
(35, 77),
(35, 78),
(35, 79),
(35, 80),
(35, 81),
(35, 82),
(35, 83),
(35, 84),
(35, 85),
(35, 86),
(35, 87),
(35, 88),
(35, 89),
(35, 90),
(35, 91),
(35, 92),
(35, 93),
(35, 94),
(35, 95),
(35, 96),
(35, 97),
(35, 98),
(35, 99),
(35, 100),
(35, 101),
(35, 102),
(35, 103),
(35, 104),
(35, 105),
(35, 106),
(35, 107),
(35, 108),
(35, 109),
(35, 110),
(35, 111),
(35, 112),
(35, 113),
(35, 114),
(35, 115),
(35, 116),
(35, 117),
(35, 118),
(35, 119),
(35, 120),
(35, 121),
(35, 122),
(35, 123),
(35, 124),
(35, 125),
(35, 126),
(35, 127),
(35, 128),
(35, 129),
(35, 130),
(35, 131),
(35, 132),
(44, 11),
(44, 12),
(44, 13),
(44, 14),
(44, 15),
(44, 16),
(44, 17),
(44, 18),
(44, 20),
(44, 24),
(44, 27),
(44, 28),
(44, 29),
(44, 30),
(44, 38),
(44, 40),
(44, 43),
(44, 44),
(44, 45),
(44, 46),
(44, 47),
(44, 48),
(44, 50),
(44, 51),
(44, 52),
(44, 53),
(44, 55),
(44, 57),
(44, 58),
(44, 59),
(44, 60),
(44, 61),
(44, 62),
(44, 63),
(44, 64),
(44, 65),
(44, 66),
(44, 67),
(44, 68),
(44, 70),
(44, 72),
(44, 73),
(44, 75),
(44, 76),
(44, 78),
(44, 79),
(44, 80),
(44, 81),
(44, 82),
(44, 83),
(44, 84),
(44, 85),
(44, 86),
(44, 87),
(44, 88),
(44, 89),
(44, 90),
(44, 91),
(44, 92),
(44, 93),
(44, 94),
(44, 95),
(44, 96),
(44, 97),
(44, 98),
(44, 99),
(44, 100),
(44, 101),
(44, 102),
(44, 103),
(44, 104),
(44, 105),
(44, 106),
(44, 107),
(44, 108),
(44, 109),
(44, 110),
(44, 111),
(44, 112),
(44, 113),
(44, 114),
(44, 115),
(44, 116),
(44, 117),
(44, 118),
(44, 119),
(44, 120),
(44, 121),
(44, 122),
(44, 123),
(44, 124),
(44, 125),
(44, 126),
(44, 127),
(44, 128),
(44, 129),
(44, 130),
(44, 131),
(44, 132),
(45, 1),
(98, 56),
(120, 54),
(121, 54),
(122, 54),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1),
(130, 1),
(131, 1),
(132, 1),
(133, 1),
(134, 1),
(136, 1),
(140, 1),
(142, 1),
(143, 0),
(144, 0),
(145, 0),
(146, 0),
(147, 0),
(148, 0),
(149, 0),
(150, 0),
(151, 1),
(152, 0),
(153, 1),
(154, 0),
(155, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_morfeoshow`
--

DROP TABLE IF EXISTS `jos_morfeoshow`;
CREATE TABLE IF NOT EXISTS `jos_morfeoshow` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `description1` text NOT NULL,
  `folder` varchar(32) NOT NULL,
  `shortcut_filename` varchar(128) NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `flashgallery` int(2) NOT NULL DEFAULT '0',
  `height` int(4) NOT NULL,
  `width` int(4) NOT NULL,
  `heightsw` int(4) NOT NULL,
  `widthsw` int(4) NOT NULL,
  `heightpc` int(4) NOT NULL,
  `widthpc` int(4) NOT NULL,
  `heightpl` int(4) NOT NULL,
  `widthpl` int(4) NOT NULL,
  `trans` int(2) NOT NULL DEFAULT '0',
  `movimento` tinyint(1) NOT NULL DEFAULT '1',
  `navigation` tinyint(1) NOT NULL DEFAULT '1',
  `tempo` int(4) NOT NULL,
  `bkgnd` varchar(12) NOT NULL,
  `bkgnd1` varchar(12) NOT NULL,
  `bkgnd2` varchar(12) NOT NULL,
  `bkgnd3` varchar(12) NOT NULL,
  `ordering` int(4) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `glat` varchar(20) NOT NULL,
  `glng` varchar(20) NOT NULL,
  `gzoom` int(4) NOT NULL,
  `gmapkey` varchar(100) NOT NULL,
  `luogo` varchar(128) NOT NULL,
  `user_id` varchar(255) NOT NULL,
  `group_id` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL,
  `tags` varchar(255) NOT NULL,
  `set_id` varchar(255) NOT NULL,
  `sort` int(4) NOT NULL,
  `pusername` varchar(180) NOT NULL,
  `pphotosize` int(4) NOT NULL DEFAULT '512',
  `pthumbsize` int(4) NOT NULL DEFAULT '64',
  `palbumcols` int(4) NOT NULL DEFAULT '3',
  `pcols` int(4) NOT NULL DEFAULT '6',
  `pmaxresults` int(4) NOT NULL DEFAULT '24',
  `pmaxalbums` int(4) NOT NULL DEFAULT '4',
  `psingle` varchar(120) NOT NULL,
  `pback` varchar(10) NOT NULL,
  `paltezza` int(4) NOT NULL,
  `plarghezza` int(4) NOT NULL,
  `overstretch` tinyint(1) NOT NULL DEFAULT '0',
  `shuffle` tinyint(1) NOT NULL DEFAULT '0',
  `tclassic` int(4) NOT NULL,
  `tcol` int(4) NOT NULL,
  `orderfront` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_morfeoshow`
--

INSERT INTO `jos_morfeoshow` (`id`, `name`, `description`, `description1`, `folder`, `shortcut_filename`, `published`, `flashgallery`, `height`, `width`, `heightsw`, `widthsw`, `heightpc`, `widthpc`, `heightpl`, `widthpl`, `trans`, `movimento`, `navigation`, `tempo`, `bkgnd`, `bkgnd1`, `bkgnd2`, `bkgnd3`, `ordering`, `metakey`, `metadesc`, `metadata`, `glat`, `glng`, `gzoom`, `gmapkey`, `luogo`, `user_id`, `group_id`, `text`, `tags`, `set_id`, `sort`, `pusername`, `pphotosize`, `pthumbsize`, `palbumcols`, `pcols`, `pmaxresults`, `pmaxalbums`, `psingle`, `pback`, `paltezza`, `plarghezza`, `overstretch`, `shuffle`, `tclassic`, `tcol`, `orderfront`) VALUES
(1, 'Love Feast', 'Love 200x', '', 'love_feast-7580', '', 1, 4, 400, 600, 680, 650, 480, 640, 680, 580, 0, 0, 1, 4, 'FFFFFF', '', '', '', 1, '', '', '', '', '', 0, '', '', '', '', '', 'sea', '72157602230948200', 0, '', 512, 64, 3, 6, 24, 4, '', 'FFFFFF', 420, 500, 0, 0, 0, 4, 'rand()');

-- --------------------------------------------------------

--
-- Table structure for table `jos_morfeoshow_img`
--

DROP TABLE IF EXISTS `jos_morfeoshow_img`;
CREATE TABLE IF NOT EXISTS `jos_morfeoshow_img` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(5) NOT NULL,
  `filename` varchar(128) NOT NULL,
  `title` text NOT NULL,
  `imgdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` text NOT NULL,
  `height` int(4) NOT NULL,
  `width` int(4) NOT NULL,
  `creator` text NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_morfeoshow_rating`
--

DROP TABLE IF EXISTS `jos_morfeoshow_rating`;
CREATE TABLE IF NOT EXISTS `jos_morfeoshow_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_newsfeeds`
--

DROP TABLE IF EXISTS `jos_newsfeeds`;
CREATE TABLE IF NOT EXISTS `jos_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text NOT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(11) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(11) unsigned NOT NULL DEFAULT '3600',
  `checked_out` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `jos_newsfeeds`
--

INSERT INTO `jos_newsfeeds` (`catid`, `id`, `name`, `alias`, `link`, `filename`, `published`, `numarticles`, `cache_time`, `checked_out`, `checked_out_time`, `ordering`, `rtl`) VALUES
(4, 1, 'Joomla! Announcements', 'joomla-official-news', 'http://feeds.joomla.org/JoomlaAnnouncements', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0),
(4, 2, 'Joomla! Core Team Blog', 'joomla-core-team-blog', 'http://feeds.joomla.org/JoomlaCommunityCoreTeamBlog', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0),
(4, 3, 'Joomla! Community Magazine', 'joomla-community-magazine', 'http://feeds.joomla.org/JoomlaMagazine', '', 1, 20, 3600, 0, '0000-00-00 00:00:00', 3, 0),
(4, 4, 'Joomla! Developer News', 'joomla-developer-news', 'http://feeds.joomla.org/JoomlaDeveloper', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0),
(4, 5, 'Joomla! Security News', 'joomla-security-news', 'http://feeds.joomla.org/JoomlaSecurityNews', '', 1, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0),
(5, 6, 'Free Software Foundation Blogs', 'free-software-foundation-blogs', 'http://www.fsf.org/blogs/RSS', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0),
(5, 7, 'Free Software Foundation', 'free-software-foundation', 'http://www.fsf.org/news/RSS', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 3, 0),
(5, 8, 'Software Freedom Law Center Blog', 'software-freedom-law-center-blog', 'http://www.softwarefreedom.org/feeds/blog/', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0),
(5, 9, 'Software Freedom Law Center News', 'software-freedom-law-center', 'http://www.softwarefreedom.org/feeds/news/', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0),
(5, 10, 'Open Source Initiative Blog', 'open-source-initiative-blog', 'http://www.opensource.org/blog/feed', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0),
(6, 11, 'PHP News and Announcements', 'php-news-and-announcements', 'http://www.php.net/feed.atom', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0),
(6, 12, 'Planet MySQL', 'planet-mysql', 'http://www.planetmysql.org/rss20.xml', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0),
(6, 13, 'Linux Foundation Announcements', 'linux-foundation-announcements', 'http://www.linuxfoundation.org/press/rss20.xml', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 3, 0),
(6, 14, 'Mootools Blog', 'mootools-blog', 'http://feeds.feedburner.com/mootools-blog', NULL, 1, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery`
--

DROP TABLE IF EXISTS `jos_phocagallery`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `description` text,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `latitude` varchar(20) NOT NULL DEFAULT '',
  `longitude` varchar(20) NOT NULL DEFAULT '',
  `zoom` int(3) NOT NULL DEFAULT '0',
  `geotitle` varchar(255) NOT NULL DEFAULT '',
  `videocode` text,
  `vmproductid` int(11) NOT NULL DEFAULT '0',
  `imgorigsize` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  `metakey` text,
  `metadesc` text,
  `extlink1` text,
  `extlink2` text,
  `extid` varchar(255) NOT NULL DEFAULT '',
  `extl` varchar(255) NOT NULL DEFAULT '',
  `extm` varchar(255) NOT NULL DEFAULT '',
  `exts` varchar(255) NOT NULL DEFAULT '',
  `exto` varchar(255) NOT NULL DEFAULT '',
  `extw` varchar(255) NOT NULL DEFAULT '',
  `exth` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `jos_phocagallery`
--

INSERT INTO `jos_phocagallery` (`id`, `catid`, `sid`, `title`, `alias`, `filename`, `description`, `date`, `hits`, `latitude`, `longitude`, `zoom`, `geotitle`, `videocode`, `vmproductid`, `imgorigsize`, `published`, `approved`, `checked_out`, `checked_out_time`, `ordering`, `params`, `metakey`, `metadesc`, `extlink1`, `extlink2`, `extid`, `extl`, `extm`, `exts`, `exto`, `extw`, `exth`) VALUES
(1, 2, 0, '100_1005', '1001005', 'Kidscruise/100_1005.jpg', NULL, '2010-03-20 18:23:49', 0, '', '', 0, '', NULL, 0, 92930, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(2, 2, 0, '100_1015', '1001015', 'Kidscruise/100_1015.jpg', NULL, '2010-03-20 18:23:49', 1, '', '', 0, '', NULL, 0, 67389, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(3, 2, 0, '100_1024', '1001024', 'Kidscruise/100_1024.jpg', NULL, '2010-03-20 18:23:49', 0, '', '', 0, '', NULL, 0, 61493, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(4, 2, 0, '100_1031', '1001031', 'Kidscruise/100_1031.jpg', NULL, '2010-03-20 18:23:49', 0, '', '', 0, '', NULL, 0, 68326, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(5, 2, 0, '100_1036', '1001036', 'Kidscruise/100_1036.jpg', NULL, '2010-03-20 18:23:49', 2, '', '', 0, '', NULL, 0, 48146, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(6, 2, 0, '100_1048', '1001048', 'Kidscruise/100_1048.jpg', NULL, '2010-03-20 18:23:49', 1, '', '', 0, '', NULL, 0, 61373, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(7, 3, 0, '1', '1', 'Misc/1.jpg', NULL, '2010-03-20 18:44:02', 1, '', '', 0, '', NULL, 0, 5112, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(8, 3, 0, '100_2432', '1002432', 'Misc/100_2432.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 1289056, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(9, 3, 0, '100_2435', '1002435', 'Misc/100_2435.jpg', NULL, '2010-03-20 18:44:02', 1, '', '', 0, '', NULL, 0, 1318650, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(10, 3, 0, 'dscf0020', 'dscf0020', 'Misc/dscf0020.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 1547251, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(11, 3, 0, 'holland2 026', 'holland2-026', 'Misc/holland2 026.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 152737, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(12, 3, 0, 'youth website 001', 'youth-website-001', 'Misc/youth website 001.jpg', NULL, '2010-03-20 18:44:02', 1, '', '', 0, '', NULL, 0, 54382, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(13, 3, 0, 'youth website 002', 'youth-website-002', 'Misc/youth website 002.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 57820, 1, 1, 0, '0000-00-00 00:00:00', 7, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(14, 3, 0, 'youth website 007', 'youth-website-007', 'Misc/youth website 007.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 34979, 1, 1, 0, '0000-00-00 00:00:00', 8, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(15, 3, 0, 'youth website 012', 'youth-website-012', 'Misc/youth website 012.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 35938, 1, 1, 0, '0000-00-00 00:00:00', 9, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(16, 3, 0, 'youth website 013', 'youth-website-013', 'Misc/youth website 013.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 42500, 1, 1, 0, '0000-00-00 00:00:00', 10, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(17, 3, 0, 'youth website 014', 'youth-website-014', 'Misc/youth website 014.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 42159, 1, 1, 0, '0000-00-00 00:00:00', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(18, 3, 0, 'youth website 019', 'youth-website-019', 'Misc/youth website 019.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 2161173, 1, 1, 0, '0000-00-00 00:00:00', 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(19, 3, 0, 'youth website 0192', 'youth-website-0192', 'Misc/youth website 0192.jpg', NULL, '2010-03-20 18:44:02', 0, '', '', 0, '', NULL, 0, 2161173, 1, 1, 0, '0000-00-00 00:00:00', 13, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(20, 4, 0, '100_4136', '1004136', 'Youthcamp07/100_4136.jpg', NULL, '2010-03-21 01:21:20', 0, '', '', 0, '', NULL, 0, 976994, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(21, 4, 0, '100_4137', '1004137', 'Youthcamp07/100_4137.jpg', NULL, '2010-03-21 01:21:20', 0, '', '', 0, '', NULL, 0, 1199341, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(22, 4, 0, '100_4139', '1004139', 'Youthcamp07/100_4139.jpg', NULL, '2010-03-21 01:21:20', 0, '', '', 0, '', NULL, 0, 1313122, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(23, 4, 0, '100_4145', '1004145', 'Youthcamp07/100_4145.jpg', NULL, '2010-03-21 01:21:20', 0, '', '', 0, '', NULL, 0, 1248214, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(24, 4, 0, '100_4146', '1004146', 'Youthcamp07/100_4146.jpg', NULL, '2010-03-21 01:21:20', 0, '', '', 0, '', NULL, 0, 1355396, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(25, 4, 0, '100_4148', '1004148', 'Youthcamp07/100_4148.jpg', NULL, '2010-03-21 01:21:20', 1, '', '', 0, '', NULL, 0, 1130681, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(26, 4, 0, '100_4149', '1004149', 'Youthcamp07/100_4149.jpg', NULL, '2010-03-21 01:21:20', 2, '', '', 0, '', NULL, 0, 1159193, 1, 1, 0, '0000-00-00 00:00:00', 7, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery_categories`
--

DROP TABLE IF EXISTS `jos_phocagallery_categories`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `accessuserid` text,
  `uploaduserid` text,
  `deleteuserid` text,
  `userfolder` text,
  `latitude` varchar(20) NOT NULL DEFAULT '',
  `longitude` varchar(20) NOT NULL DEFAULT '',
  `zoom` int(3) NOT NULL DEFAULT '0',
  `geotitle` varchar(255) NOT NULL DEFAULT '',
  `extid` varchar(255) NOT NULL DEFAULT '',
  `exta` varchar(255) NOT NULL DEFAULT '',
  `extu` varchar(255) NOT NULL DEFAULT '',
  `params` text,
  `metakey` text,
  `metadesc` text,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `jos_phocagallery_categories`
--

INSERT INTO `jos_phocagallery_categories` (`id`, `parent_id`, `owner_id`, `title`, `name`, `alias`, `image`, `section`, `image_position`, `description`, `date`, `published`, `approved`, `checked_out`, `checked_out_time`, `editor`, `ordering`, `access`, `count`, `hits`, `accessuserid`, `uploaduserid`, `deleteuserid`, `userfolder`, `latitude`, `longitude`, `zoom`, `geotitle`, `extid`, `exta`, `extu`, `params`, `metakey`, `metadesc`) VALUES
(1, 0, 0, 'Love Feast 2007', '', 'love-feast-2007', '', '', 'left', '', '2010-03-20 00:00:00', 1, 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, 0, '0', '-2', '-2', 'Lovefeast2009', '', '', 0, '', '', '', '', NULL, 'Apostolic faith youths, apostolic faith uk', 'Love Feast 2009 organised by The Apostolic Faith Youths'),
(2, 0, 0, 'Kid''s Cruise', '', 'kids-cruise', '', '', 'left', '', '2010-03-20 00:00:00', 1, 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, 1, '0', '-2', '-2', 'Kidscruise', '', '', 0, '', '', '', '', NULL, 'Apostolic faith church, apostolic faith uk, apostolic faith church youths', 'Kid''s Cruise organised by The Apostolic Faith Church Youths, UK'),
(3, 0, 0, 'Miscellaneous', '', 'misc', '', '', 'left', '', '2010-03-20 00:00:00', 1, 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, 1, '0', '-2', '-2', 'Misc', '', '', 0, '', '', '', '', NULL, '', 'Photo Gallery'),
(4, 0, 0, 'Youth Camp 07''', '', 'youthcamp-07', '', '', 'left', '', '2010-03-20 17:11:48', 1, 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, 1, '0', '-2', '-2', 'Youthcamp07', '', '', 0, '', '', '', '', NULL, 'Apostolic faith church youths, apostolic faith youth website, apostolic faith church youth website, apostolic faith church uk, apostolic faith church', 'Youth Camp 2007 Photo Gallery'),
(5, 0, 0, 'Teen Beach Day', '', 'teen-beach-day', '', '', 'left', '', '2010-03-20 17:16:10', 1, 1, 0, '0000-00-00 00:00:00', NULL, 5, 0, 0, 0, '0', '-2', '-2', 'Teenbeachday', '', '', 0, '', '', '', '', NULL, '', 'Teen Beach Day photo gallery');

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery_comments`
--

DROP TABLE IF EXISTS `jos_phocagallery_comments`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL DEFAULT '',
  `comment` text,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery_img_comments`
--

DROP TABLE IF EXISTS `jos_phocagallery_img_comments`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery_img_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL DEFAULT '',
  `comment` text,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery_img_votes`
--

DROP TABLE IF EXISTS `jos_phocagallery_img_votes`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery_img_votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rating` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery_img_votes_statistics`
--

DROP TABLE IF EXISTS `jos_phocagallery_img_votes_statistics`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery_img_votes_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgid` int(11) NOT NULL DEFAULT '0',
  `count` tinyint(11) NOT NULL DEFAULT '0',
  `average` float(8,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery_user`
--

DROP TABLE IF EXISTS `jos_phocagallery_user`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `avatar` varchar(40) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery_votes`
--

DROP TABLE IF EXISTS `jos_phocagallery_votes`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery_votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rating` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_phocagallery_votes_statistics`
--

DROP TABLE IF EXISTS `jos_phocagallery_votes_statistics`;
CREATE TABLE IF NOT EXISTS `jos_phocagallery_votes_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `count` tinyint(11) NOT NULL DEFAULT '0',
  `average` float(8,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_plugins`
--

DROP TABLE IF EXISTS `jos_plugins`;
CREATE TABLE IF NOT EXISTS `jos_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `element` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) NOT NULL DEFAULT '',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder` (`published`,`client_id`,`access`,`folder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=54 ;

--
-- Dumping data for table `jos_plugins`
--

INSERT INTO `jos_plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Authentication - Joomla', 'joomla', 'authentication', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(2, 'Authentication - LDAP', 'ldap', 'authentication', 0, 2, 0, 1, 0, 0, '0000-00-00 00:00:00', 'host=\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),
(3, 'Authentication - GMail', 'gmail', 'authentication', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 'Authentication - OpenID', 'openid', 'authentication', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(5, 'User - Joomla!', 'joomla', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'autoregister=1\n\n'),
(6, 'Search - Content', 'content', 'search', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch_archived=1\n\n'),
(7, 'Search - Contacts', 'contacts', 'search', 0, 3, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(8, 'Search - Categories', 'categories', 'search', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(9, 'Search - Sections', 'sections', 'search', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(10, 'Search - Newsfeeds', 'newsfeeds', 'search', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(11, 'Search - Weblinks', 'weblinks', 'search', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(12, 'Content - Pagebreak', 'pagebreak', 'content', 0, 10000, 1, 1, 0, 0, '0000-00-00 00:00:00', 'enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),
(13, 'Content - Rating', 'vote', 'content', 0, 4, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(14, 'Content - Email Cloaking', 'emailcloak', 'content', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'mode=1\n\n'),
(15, 'Content - Code Hightlighter (GeSHi)', 'geshi', 'content', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(16, 'Content - Load Module', 'loadmodule', 'content', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'enabled=1\nstyle=0\n\n'),
(17, 'Content - Page Navigation', 'pagenavigation', 'content', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'position=1\n\n'),
(18, 'Editor - No Editor', 'none', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(19, 'Editor - TinyMCE', 'tinymce', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 'mode=advanced\nskin=0\ncompressed=0\ncleanup_startup=0\ncleanup_save=2\nentity_encoding=raw\nlang_mode=0\nlang_code=en\ntext_direction=ltr\ncontent_css=1\ncontent_css_custom=\nrelative_urls=1\nnewlines=0\ninvalid_elements=applet\nextended_elements=\ntoolbar=top\ntoolbar_align=left\nhtml_height=550\nhtml_width=750\nelement_path=1\nfonts=1\npaste=1\nsearchreplace=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:%M:%S\ncolors=1\ntable=1\nsmilies=1\nmedia=1\nhr=1\ndirectionality=1\nfullscreen=1\nstyle=1\nlayer=1\nxhtmlxtras=1\nvisualchars=1\nnonbreaking=1\ntemplate=0\nadvimage=1\nadvlink=1\nautosave=1\ncontextmenu=1\ninlinepopups=1\nsafari=1\ncustom_plugin=\ncustom_button=\n\n'),
(20, 'Editor - XStandard Lite 2.0', 'xstandard', 'editors', 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(21, 'Editor Button - Image', 'image', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(22, 'Editor Button - Pagebreak', 'pagebreak', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(23, 'Editor Button - Readmore', 'readmore', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(24, 'XML-RPC - Joomla', 'joomla', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(25, 'XML-RPC - Blogger API', 'blogger', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', 'catid=1\nsectionid=0\n\n'),
(27, 'System - SEF', 'sef', 'system', 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(28, 'System - Debug', 'debug', 'system', 0, 2, 1, 0, 0, 0, '0000-00-00 00:00:00', 'queries=1\nmemory=1\nlangauge=1\n\n'),
(29, 'System - Legacy', 'legacy', 'system', 0, 3, 0, 1, 0, 0, '0000-00-00 00:00:00', 'route=0\n\n'),
(30, 'System - Cache', 'cache', 'system', 0, 4, 0, 1, 0, 0, '0000-00-00 00:00:00', 'browsercache=0\ncachetime=15\n\n'),
(31, 'System - Log', 'log', 'system', 0, 5, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(32, 'System - Remember Me', 'remember', 'system', 0, 6, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(33, 'System - Backlink', 'backlink', 'system', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(34, 'System - RokCandy', 'rokcandy_system', 'system', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(35, 'Button - RokCandy', 'rokcandy_button', 'editors-xtd', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(36, 'RokNavMenu - Boost', 'boost', 'roknavmenu', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(37, 'RokNavMenu - Extended Link', 'extendedlink', 'roknavmenu', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(38, 'Content - RokBox', 'rokbox', 'content', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'thumb_ext=_thumb\nthumb_class=album\nthumb_dir=cache\nthumb_width=150\nthumb_height=100\nthumb_quality=90\n'),
(39, 'Editor - RokPad', 'rokpad', 'editors', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', '@spacer=<div id="parser-type"   style="font-weight:normal;font-size:12px;color:#fff;padding:4px;margin:0;background:#666;">Parser Type</div>\nrokpad-parser=xhtmlmixed\nrokpad-tidylevel=XHTML 1.0 Transitional\nrokpad-show-formatter=1\n@spacer=<div id="editor-parameters"   style="font-weight:normal;font-size:12px;color:#fff;padding:4px;margin:0;background:#666;">Editor Parameters</div>\nrokpad-height=350\nrokpad-passdelay=200\nrokpad-passtime=50\nrokpad-linenumberdelay=200\nrokpad-linenumbertime=50\nrokpad-continuous=500\nrokpad-matchparens=1\nrokpad-history=50\nrokpad-history-delay=800\nrokpad-lineHandler=1\nrokpad-textwrapperHandler=1\nrokpad-indentunit=2\nrokpad-tabmode=indent\nrokpad-loadindent=1\n'),
(40, 'System - RokBox', 'rokbox', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'theme=light\ncustom-theme=sample\n'),
(41, 'System - RokGantry Cache', 'rokgantrycache', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(42, 'System - RokGZipper', 'rokgzipper', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'cache_time=900\nexpires_header_time=1440\nstrip_css=1\n'),
(43, 'Search - K2', 'k2', 'search', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n'),
(44, 'System - K2', 'k2', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(45, 'User - K2', 'k2', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(47, 'chronocontact', 'chronocontact', 'content', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(48, 'Editor - JCE 1.5.6', 'jce', 'editors', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(49, 'uddeIM PMS Content Link', 'uddeim_pms_contentlink', 'content', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(50, 'uddeIM Searchbot', 'uddeim.searchbot', 'search', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n'),
(52, 'System - contentstatic', 'contentstatic', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(53, 'System - CssJsCompress', 'CssJsCompress', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'css=1\njavascript=1\njqueryNoConflict=1\njquery=jquery.js\ncustomOrder=mootools.js,jquery.js,jquery.innerfade.js\n');

-- --------------------------------------------------------

--
-- Table structure for table `jos_polls`
--

DROP TABLE IF EXISTS `jos_polls`;
CREATE TABLE IF NOT EXISTS `jos_polls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `voters` int(9) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `lag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `jos_polls`
--

INSERT INTO `jos_polls` (`id`, `title`, `alias`, `voters`, `checked_out`, `checked_out_time`, `published`, `access`, `lag`) VALUES
(14, 'What do you think of this site?', 'site-poll', 12, 0, '0000-00-00 00:00:00', 1, 0, 86400),
(15, 'This new website is...', 'new-website-poll', 2, 0, '0000-00-00 00:00:00', 1, 0, 86400);

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_data`
--

DROP TABLE IF EXISTS `jos_poll_data`;
CREATE TABLE IF NOT EXISTS `jos_poll_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollid` int(11) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pollid` (`pollid`,`text`(1))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `jos_poll_data`
--

INSERT INTO `jos_poll_data` (`id`, `pollid`, `text`, `hits`) VALUES
(1, 14, 'Brilliant! I love it', 2),
(2, 14, 'I like it', 3),
(3, 14, 'Not impressed', 1),
(4, 14, 'I dont like it', 0),
(5, 14, 'I dont know', 0),
(6, 14, '', 2),
(7, 14, '', 4),
(8, 14, '', 0),
(9, 14, '', 0),
(10, 14, '', 0),
(11, 14, '', 0),
(12, 14, '', 0),
(13, 15, 'Brilliant!!', 0),
(14, 15, 'Good', 1),
(15, 15, 'Not bad', 0),
(16, 15, 'Terrible', 1),
(17, 15, 'No comment', 0),
(18, 15, '', 0),
(19, 15, '', 0),
(20, 15, '', 0),
(21, 15, '', 0),
(22, 15, '', 0),
(23, 15, '', 0),
(24, 15, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_date`
--

DROP TABLE IF EXISTS `jos_poll_date`;
CREATE TABLE IF NOT EXISTS `jos_poll_date` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vote_id` int(11) NOT NULL DEFAULT '0',
  `poll_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `jos_poll_date`
--

INSERT INTO `jos_poll_date` (`id`, `date`, `vote_id`, `poll_id`) VALUES
(1, '2006-10-09 13:01:58', 1, 14),
(2, '2006-10-10 15:19:43', 7, 14),
(3, '2006-10-11 11:08:16', 7, 14),
(4, '2006-10-11 15:02:26', 2, 14),
(5, '2006-10-11 15:43:03', 7, 14),
(6, '2006-10-11 15:43:38', 7, 14),
(7, '2006-10-12 00:51:13', 2, 14),
(8, '2007-05-10 19:12:29', 3, 14),
(9, '2007-05-14 14:18:00', 6, 14),
(10, '2007-06-10 15:20:29', 6, 14),
(11, '2007-07-03 12:37:53', 2, 14),
(12, '2010-02-27 07:44:26', 14, 15),
(13, '2010-02-28 14:04:02', 16, 15),
(14, '2010-03-01 08:46:38', 7, 14);

-- --------------------------------------------------------

--
-- Table structure for table `jos_poll_menu`
--

DROP TABLE IF EXISTS `jos_poll_menu`;
CREATE TABLE IF NOT EXISTS `jos_poll_menu` (
  `pollid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pollid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_rokcandy`
--

DROP TABLE IF EXISTS `jos_rokcandy`;
CREATE TABLE IF NOT EXISTS `jos_rokcandy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL,
  `macro` text NOT NULL,
  `html` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `ordering` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `jos_rokcandy`
--

INSERT INTO `jos_rokcandy` (`id`, `catid`, `macro`, `html`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`) VALUES
(20, 34, '[code]{text}[/code]', '<code>{text}</code>', 1, 0, '0000-00-00 00:00:00', 7, ''),
(21, 34, '[i]{text}[/i]', '<em>{text}</em>', 1, 0, '0000-00-00 00:00:00', 6, ''),
(22, 34, '[b]{text}[/b]', '<strong>{text}</strong>', 1, 0, '0000-00-00 00:00:00', 5, ''),
(23, 34, '[h4]{text}[/h4]', '<h4>{text}</h4>', 1, 0, '0000-00-00 00:00:00', 4, ''),
(24, 34, '[h1]{text}[/h1]', '<h1>{text}</h1>', 1, 0, '0000-00-00 00:00:00', 1, ''),
(25, 34, '[h2]{text}[/h2]', '<h2>{text}</h2>', 1, 0, '0000-00-00 00:00:00', 2, ''),
(26, 34, '[h3]{text}[/h3]', '<h3>{text}</h3>', 1, 0, '0000-00-00 00:00:00', 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_sections`
--

DROP TABLE IF EXISTS `jos_sections`;
CREATE TABLE IF NOT EXISTS `jos_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` text NOT NULL,
  `scope` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_scope` (`scope`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `jos_sections`
--

INSERT INTO `jos_sections` (`id`, `title`, `name`, `alias`, `image`, `scope`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `ordering`, `access`, `count`, `params`) VALUES
(1, 'Intranet', '', 'intranet', 'articles.jpg', 'content', 'right', '<p>Content in this section is reserved for only site administrators and youths leaders</p>', 1, 0, '0000-00-00 00:00:00', 2, 0, 5, ''),
(4, 'General Information', '', 'general-information', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 1, 0, 18, ''),
(5, 'Articles and News (Content)', '', 'content', '', 'content', 'left', '', 1, 0, '0000-00-00 00:00:00', 4, 0, 15, '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_session`
--

DROP TABLE IF EXISTS `jos_session`;
CREATE TABLE IF NOT EXISTS `jos_session` (
  `username` varchar(150) DEFAULT '',
  `time` varchar(14) DEFAULT '',
  `session_id` varchar(200) NOT NULL DEFAULT '0',
  `guest` tinyint(4) DEFAULT '1',
  `userid` int(11) DEFAULT '0',
  `usertype` varchar(50) DEFAULT '',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext,
  PRIMARY KEY (`session_id`(64)),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_session`
--

INSERT INTO `jos_session` (`username`, `time`, `session_id`, `guest`, `userid`, `usertype`, `gid`, `client_id`, `data`) VALUES
('', '1314947789', 'ncg9g3skreajpi1inl7sh3gmh4', 1, 0, '', 0, 0, '__default|a:8:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1314947789;s:18:"session.timer.last";i:1314947789;s:17:"session.timer.now";i:1314947789;s:22:"session.client.browser";s:84:"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.2) Gecko/20100115 Firefox/3.6";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:1:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:74:"/home1/apostol8/public_html/youths/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:13:"session.token";s:32:"323b1889721fb5bed29949fb534ab5c0";}');

-- --------------------------------------------------------

--
-- Table structure for table `jos_stats_agents`
--

DROP TABLE IF EXISTS `jos_stats_agents`;
CREATE TABLE IF NOT EXISTS `jos_stats_agents` (
  `agent` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_templates_menu`
--

DROP TABLE IF EXISTS `jos_templates_menu`;
CREATE TABLE IF NOT EXISTS `jos_templates_menu` (
  `template` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menuid`,`client_id`,`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jos_templates_menu`
--

INSERT INTO `jos_templates_menu` (`template`, `menuid`, `client_id`) VALUES
('rt_crystalline_j15', 0, 0),
('khepri', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jos_uddeim`
--

DROP TABLE IF EXISTS `jos_uddeim`;
CREATE TABLE IF NOT EXISTS `jos_uddeim` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `replyid` int(11) NOT NULL DEFAULT '0',
  `fromid` int(11) NOT NULL DEFAULT '0',
  `toid` int(11) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  `datum` int(11) DEFAULT NULL,
  `toread` int(1) NOT NULL DEFAULT '0',
  `totrash` int(1) NOT NULL DEFAULT '0',
  `totrashdate` int(11) DEFAULT NULL,
  `totrashoutbox` int(1) NOT NULL DEFAULT '0',
  `totrashdateoutbox` int(11) DEFAULT NULL,
  `expires` int(11) NOT NULL DEFAULT '0',
  `disablereply` int(1) NOT NULL DEFAULT '0',
  `systemmessage` varchar(60) DEFAULT NULL,
  `archived` int(1) NOT NULL DEFAULT '0',
  `cryptmode` int(1) NOT NULL DEFAULT '0',
  `flagged` int(1) NOT NULL DEFAULT '0',
  `crypthash` varchar(32) DEFAULT NULL,
  `publicname` text,
  `publicemail` text,
  PRIMARY KEY (`id`),
  KEY `toid_toread` (`toid`,`toread`),
  KEY `fromid` (`fromid`),
  KEY `replyid` (`replyid`),
  KEY `datum` (`datum`),
  KEY `totrashdate` (`totrashdate`),
  KEY `totrashdateoutbox_datum` (`totrashdateoutbox`,`datum`),
  KEY `toread_totrash_datum` (`toread`,`totrash`,`datum`),
  KEY `totrash_totrashdate` (`totrash`,`totrashdate`),
  KEY `archived_totrash_toid_datum` (`archived`,`totrash`,`toid`,`datum`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_uddeim`
--

INSERT INTO `jos_uddeim` (`id`, `replyid`, `fromid`, `toid`, `message`, `datum`, `toread`, `totrash`, `totrashdate`, `totrashoutbox`, `totrashdateoutbox`, `expires`, `disablereply`, `systemmessage`, `archived`, `cryptmode`, `flagged`, `crypthash`, `publicname`, `publicemail`) VALUES
(1, 0, 62, 62, 'Welcome to uddeIM!\n\nYou have succesfully installed uddeIM.\n\nTry viewing this message with different templates. You can set them in the administration backend of uddeIM.\n\nuddeIM is a project in development. If you find bugs or weaknesses, please write them to me so that we can make uddeIM better together.\n\nGood luck and have fun!', 1268735485, 0, 0, NULL, 1, 1268735485, 0, 1, 'uddeIM', 0, 0, 0, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jos_uddeim_attachments`
--

DROP TABLE IF EXISTS `jos_uddeim_attachments`;
CREATE TABLE IF NOT EXISTS `jos_uddeim_attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(1) NOT NULL DEFAULT '0',
  `tempname` text NOT NULL,
  `filename` text NOT NULL,
  `fileid` varchar(32) NOT NULL,
  `size` int(1) NOT NULL DEFAULT '0',
  `datum` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`),
  KEY `fileid` (`fileid`),
  KEY `datum` (`datum`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_uddeim_blocks`
--

DROP TABLE IF EXISTS `jos_uddeim_blocks`;
CREATE TABLE IF NOT EXISTS `jos_uddeim_blocks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blocker` int(11) NOT NULL DEFAULT '0',
  `blocked` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_uddeim_config`
--

DROP TABLE IF EXISTS `jos_uddeim_config`;
CREATE TABLE IF NOT EXISTS `jos_uddeim_config` (
  `varname` tinytext NOT NULL,
  `value` tinytext NOT NULL,
  PRIMARY KEY (`varname`(30))
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jos_uddeim_emn`
--

DROP TABLE IF EXISTS `jos_uddeim_emn`;
CREATE TABLE IF NOT EXISTS `jos_uddeim_emn` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `popup` int(1) NOT NULL DEFAULT '0',
  `public` int(1) NOT NULL DEFAULT '0',
  `remindersent` int(11) NOT NULL DEFAULT '0',
  `lastsent` int(11) NOT NULL DEFAULT '0',
  `autoresponder` int(1) NOT NULL DEFAULT '0',
  `autorespondertext` text NOT NULL,
  `autoforward` int(1) NOT NULL DEFAULT '0',
  `autoforwardid` int(1) NOT NULL DEFAULT '0',
  `locked` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jos_uddeim_emn`
--

INSERT INTO `jos_uddeim_emn` (`id`, `userid`, `status`, `popup`, `public`, `remindersent`, `lastsent`, `autoresponder`, `autorespondertext`, `autoforward`, `autoforwardid`, `locked`) VALUES
(1, 62, 2, 1, 0, 1268742145, 0, 0, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jos_uddeim_spam`
--

DROP TABLE IF EXISTS `jos_uddeim_spam`;
CREATE TABLE IF NOT EXISTS `jos_uddeim_spam` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mid` int(11) NOT NULL DEFAULT '0',
  `datum` int(11) DEFAULT NULL,
  `reported` int(11) DEFAULT NULL,
  `fromid` int(1) NOT NULL DEFAULT '0',
  `toid` int(1) NOT NULL DEFAULT '0',
  `message` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mid` (`mid`),
  KEY `fromid` (`fromid`),
  KEY `toid` (`toid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_uddeim_userlists`
--

DROP TABLE IF EXISTS `jos_uddeim_userlists`;
CREATE TABLE IF NOT EXISTS `jos_uddeim_userlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `name` varchar(40) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `userids` text NOT NULL,
  `global` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`),
  KEY `global` (`global`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jos_users`
--

DROP TABLE IF EXISTS `jos_users`;
CREATE TABLE IF NOT EXISTS `jos_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=63 ;

--
-- Dumping data for table `jos_users`
--

INSERT INTO `jos_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `gid`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES
(62, 'Administrator', 'admin', 'peter.inyang@gmail.com', 'f636f7a6ac479e2832d063cd55c195d6', 'Super Administrator', 0, 1, 25, '2010-03-11 00:39:34', '2010-07-28 10:51:49', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jos_weblinks`
--

DROP TABLE IF EXISTS `jos_weblinks`;
CREATE TABLE IF NOT EXISTS `jos_weblinks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`,`archived`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `jos_weblinks`
--

INSERT INTO `jos_weblinks` (`id`, `catid`, `sid`, `title`, `alias`, `url`, `description`, `date`, `hits`, `published`, `checked_out`, `checked_out_time`, `ordering`, `archived`, `approved`, `params`) VALUES
(1, 2, 0, 'Joomla!', 'joomla', 'http://www.joomla.org', 'Home of Joomla!', '2005-02-14 15:19:02', 5, 1, 0, '0000-00-00 00:00:00', 1, 0, 1, 'target=0'),
(2, 2, 0, 'php.net', 'php', 'http://www.php.net', 'The language that Joomla! is developed in', '2004-07-07 11:33:24', 8, 1, 0, '0000-00-00 00:00:00', 3, 0, 1, ''),
(3, 2, 0, 'MySQL', 'mysql', 'http://www.mysql.com', 'The database that Joomla! uses', '2004-07-07 10:18:31', 3, 1, 0, '0000-00-00 00:00:00', 5, 0, 1, ''),
(4, 2, 0, 'OpenSourceMatters', 'opensourcematters', 'http://www.opensourcematters.org', 'Home of OSM', '2005-02-14 15:19:02', 13, 1, 0, '0000-00-00 00:00:00', 2, 0, 1, 'target=0'),
(5, 2, 0, 'Joomla! - Forums', 'joomla-forums', 'http://forum.joomla.org', 'Joomla! Forums', '2005-02-14 15:19:02', 6, 1, 0, '0000-00-00 00:00:00', 4, 0, 1, 'target=0'),
(6, 2, 0, 'Ohloh Tracking of Joomla!', 'ohloh-tracking-of-joomla', 'http://www.ohloh.net/projects/20', 'Objective reports from Ohloh about Joomla''s development activity. Joomla! has some star developers with serious kudos.', '2007-07-19 09:28:31', 3, 1, 0, '0000-00-00 00:00:00', 6, 0, 1, 'target=0\n\n');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
