-- phpMyAdmin SQL Dump
-- version 3.4.3.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 02, 2011 at 01:16 AM
-- Server version: 5.1.58
-- PHP Version: 5.2.9

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `apostol8_jml1`
--

-- --------------------------------------------------------

--
-- Table structure for table `jml_acajoom_lists`
--

DROP TABLE IF EXISTS `jml_acajoom_lists`;
CREATE TABLE IF NOT EXISTS `jml_acajoom_lists` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `list_name` varchar(101) NOT NULL DEFAULT '',
  `list_desc` text NOT NULL,
  `list_type` tinyint(2) NOT NULL DEFAULT '0',
  `sendername` varchar(64) NOT NULL DEFAULT '',
  `senderemail` varchar(64) NOT NULL DEFAULT '',
  `bounceadres` varchar(64) NOT NULL DEFAULT '',
  `layout` text NOT NULL,
  `template` int(9) NOT NULL DEFAULT '0',
  `subscribemessage` text NOT NULL,
  `unsubscribemessage` text NOT NULL,
  `unsubscribesend` tinyint(1) NOT NULL DEFAULT '1',
  `auto_add` tinyint(1) NOT NULL DEFAULT '0',
  `user_choose` tinyint(1) NOT NULL DEFAULT '0',
  `cat_id` varchar(250) NOT NULL DEFAULT '',
  `delay_min` int(2) NOT NULL DEFAULT '0',
  `delay_max` int(2) NOT NULL DEFAULT '7',
  `follow_up` int(10) NOT NULL DEFAULT '0',
  `html` tinyint(1) NOT NULL DEFAULT '1',
  `hidden` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `createdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `acc_level` int(2) NOT NULL DEFAULT '0',
  `acc_id` int(11) NOT NULL DEFAULT '29',
  `notification` tinyint(1) NOT NULL DEFAULT '0',
  `owner` int(11) NOT NULL DEFAULT '0',
  `footer` tinyint(1) NOT NULL DEFAULT '1',
  `notify_id` int(10) NOT NULL DEFAULT '0',
  `next_date` int(11) NOT NULL DEFAULT '0',
  `start_date` date NOT NULL,
  `params` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `list_name` (`list_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jml_acajoom_lists`
--

INSERT INTO `jml_acajoom_lists` (`id`, `list_name`, `list_desc`, `list_type`, `sendername`, `senderemail`, `bounceadres`, `layout`, `template`, `subscribemessage`, `unsubscribemessage`, `unsubscribesend`, `auto_add`, `user_choose`, `cat_id`, `delay_min`, `delay_max`, `follow_up`, `html`, `hidden`, `published`, `createdate`, `acc_level`, `acc_id`, `notification`, `owner`, `footer`, `notify_id`, `next_date`, `start_date`, `params`) VALUES
(1, 'Youth Updates', '', 1, 'Olos Irenopa', 'bright_shiningstar@hotmail.co.uk', 'bright_shainingstar@hotmail.co.uk', '<table border="0" cellspacing="0" cellpadding="0" width="100%" bgcolor="#f1f1f1">\r\n<tbody>\r\n<tr>\r\n<td align="center" valign="top">\r\n<table border="0" cellspacing="0" cellpadding="0" width="530" bgcolor="#f1f1f1">\r\n<tbody>\r\n<tr>\r\n<td class="hbnr" colspan="3" bgcolor="#ffffff"><br /></td>\r\n</tr>\r\n<tr>\r\n<td colspan="3" bgcolor="#ffffff"><br /></td>\r\n</tr>\r\n<tr>\r\n<!-- /// gutter \\\\\\ -->\r\n<td width="15" valign="top" bgcolor="#ffffff"><img src="components/com_acajoom/templates/default/tpl0_spacer.gif" border="0" alt="1" width="15" height="1" /></td>\r\n<!-- \\\\\\ gutter /// --> <!-- /// content cell \\\\\\ -->\r\n<td width="500" valign="top" bgcolor="#ffffff"><br />\r\n<p> </p>\r\n<p>Your Subscription:<br /> [SUBSCRIPTIONS]</p>\r\n<p> </p>\r\n</td>\r\n<!-- \\\\\\ content cell /// --> <!-- /// gutter \\\\\\ -->\r\n<td width="15" valign="top" bgcolor="#ffffff"><img src="components/com_acajoom/templates/default/tpl0_spacer.gif" border="0" alt="1" width="15" height="1" /></td>\r\n<!-- \\\\\\ gutter /// -->\r\n</tr>\r\n<!-- /// footer area with contact info and opt-out link \\\\\\ --> \r\n<tr>\r\n<td colspan="3" bgcolor="#ffffff"><br /></td>\r\n</tr>\r\n<tr>\r\n<td style="border-top: 1px solid #aeaeae;" colspan="3" height="60" align="center" valign="middle" bgcolor="#cacaca">\r\n<p class="footerText"> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<!-- \\\\\\ footer area with contact info and opt-out link /// --></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<!-- \\\\\\ Newsletter Powered by Acajoom!  /// -->\r\n<p> </p>', 0, '', '<p>This is a confirmation email that you have been unsubscribed from our list.  We are sorry that you decided to unsubscribe should you decide to re-subscribe you can always do so on our site.  Should you have any question please contact our webmaster.</p>', 1, 2, 0, '0:0', 0, 0, 0, 1, 0, 1, '2010-09-23 08:11:57', 25, 29, 0, 62, 1, 0, 1285229340, '2010-09-23', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jml_acajoom_mailings`
--

DROP TABLE IF EXISTS `jml_acajoom_mailings`;
CREATE TABLE IF NOT EXISTS `jml_acajoom_mailings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `list_id` int(10) NOT NULL DEFAULT '0',
  `list_type` tinyint(2) NOT NULL DEFAULT '0',
  `issue_nb` int(10) NOT NULL DEFAULT '0',
  `subject` varchar(120) NOT NULL DEFAULT '',
  `fromname` varchar(64) NOT NULL DEFAULT '',
  `fromemail` varchar(64) NOT NULL DEFAULT '',
  `frombounce` varchar(64) NOT NULL DEFAULT '',
  `htmlcontent` longtext NOT NULL,
  `textonly` longtext NOT NULL,
  `attachments` text NOT NULL,
  `images` text NOT NULL,
  `send_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `delay` int(10) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  `html` tinyint(1) NOT NULL DEFAULT '1',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `createdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `acc_level` int(2) NOT NULL DEFAULT '0',
  `author_id` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jml_acajoom_mailings`
--

INSERT INTO `jml_acajoom_mailings` (`id`, `list_id`, `list_type`, `issue_nb`, `subject`, `fromname`, `fromemail`, `frombounce`, `htmlcontent`, `textonly`, `attachments`, `images`, `send_date`, `delay`, `visible`, `html`, `published`, `createdate`, `acc_level`, `author_id`, `params`) VALUES
(1, 1, 1, 1, 'Testing the Newsletter', 'Olos Irenopa', 'bright_shiningstar@hotmail.co.uk', 'bright_shainingstar@hotmail.co.uk', '<table bgcolor="#f1f1f1" cellpadding="0" cellspacing="0" border="0" style="width: 100%;">\r\n<tbody>\r\n<tr>\r\n<td align="center" valign="top">\r\n<table bgcolor="#f1f1f1" cellpadding="0" cellspacing="0" border="0" style="width: 530px;">\r\n<tbody>\r\n<tr>\r\n<td colspan="3" bgcolor="#ffffff" class="hbnr"><br /><strong><span style="font-family: arial black,avant garde;"><span style="font-size: 18pt;">Apostolic Faith Youth Website</span></span></strong></td>\r\n</tr>\r\n<tr>\r\n<td colspan="3" bgcolor="#ffffff"><br />Site relauanched finally by the young man, Peter</td>\r\n</tr>\r\n<tr>\r\n<!-- /// gutter  -->\r\n<td width="15" valign="top" bgcolor="#ffffff"><img height="1" width="15" src="components/com_acajoom/templates/default/tpl0_spacer.gif" alt="1" border="0" /></td>\r\n<!--  gutter /// --><!-- /// content cell  -->\r\n<td width="500" valign="top" bgcolor="#ffffff"><br />\r\n<p>Hello this is to inform you that our wed designer managed to complete the website at long last.</p>\r\n<p>Hooray.</p>\r\n<p>By the way, this email is from Peter.</p>\r\n<p>God bless you all.&nbsp;</p>\r\n<p>Your Subscription:<br />[SUBSCRIPTIONS]</p>\r\n<p> </p>\r\n</td>\r\n<!--  content cell /// --><!-- /// gutter  -->\r\n<td width="15" valign="top" bgcolor="#ffffff"><img height="1" width="15" src="components/com_acajoom/templates/default/tpl0_spacer.gif" alt="1" border="0" /></td>\r\n<!--  gutter /// -->\r\n</tr>\r\n<!-- /// footer area with contact info and opt-out link  -->\r\n<tr>\r\n<td colspan="3" bgcolor="#ffffff"><br /></td>\r\n</tr>\r\n<tr>\r\n<td align="center" colspan="3" valign="middle" height="60" bgcolor="#cacaca" style="border-top: 1px solid #aeaeae;">\r\n<p class="footerText"> </p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<!--  footer area with contact info and opt-out link /// --></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<!--  Newsletter Powered by Acajoom!  /// -->\r\n<p> </p>', '', '', '', '2010-09-24 20:55:46', 1440, 1, 1, 0, '2010-09-24 20:58:26', 29, 62, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jml_acajoom_queue`
--

DROP TABLE IF EXISTS `jml_acajoom_queue`;
CREATE TABLE IF NOT EXISTS `jml_acajoom_queue` (
  `qid` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(2) NOT NULL DEFAULT '0',
  `subscriber_id` int(11) NOT NULL DEFAULT '0',
  `list_id` int(10) NOT NULL DEFAULT '0',
  `mailing_id` int(11) NOT NULL DEFAULT '0',
  `issue_nb` int(10) NOT NULL DEFAULT '0',
  `send_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `suspend` tinyint(1) NOT NULL DEFAULT '0',
  `delay` int(10) NOT NULL DEFAULT '0',
  `acc_level` int(2) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`qid`),
  UNIQUE KEY `subscriber_id` (`subscriber_id`,`list_id`,`mailing_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `jml_acajoom_queue`
--

INSERT INTO `jml_acajoom_queue` (`qid`, `type`, `subscriber_id`, `list_id`, `mailing_id`, `issue_nb`, `send_date`, `suspend`, `delay`, `acc_level`, `published`, `params`) VALUES
(1, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(2, 1, 2, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(3, 1, 3, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(4, 1, 4, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(5, 1, 5, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(6, 1, 6, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(7, 1, 7, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(8, 1, 8, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(9, 1, 9, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(10, 1, 10, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(11, 1, 11, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(12, 1, 12, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(13, 1, 13, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(14, 1, 14, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(15, 1, 15, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(16, 1, 16, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(17, 1, 17, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(18, 1, 18, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(19, 1, 19, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(20, 1, 20, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(21, 1, 21, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(22, 1, 22, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(23, 1, 23, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(24, 1, 24, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(25, 1, 25, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(26, 1, 26, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(27, 1, 27, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(28, 1, 28, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(29, 1, 29, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(30, 1, 30, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(31, 1, 31, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(32, 1, 32, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(33, 1, 33, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(34, 1, 34, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(35, 1, 35, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(36, 1, 36, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(37, 1, 37, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(38, 1, 38, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, ''),
(39, 1, 39, 1, 0, 0, '0000-00-00 00:00:00', 0, 0, 29, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_acajoom_stats_details`
--

DROP TABLE IF EXISTS `jml_acajoom_stats_details`;
CREATE TABLE IF NOT EXISTS `jml_acajoom_stats_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mailing_id` int(11) NOT NULL DEFAULT '0',
  `subscriber_id` int(11) NOT NULL DEFAULT '0',
  `sentdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html` tinyint(1) NOT NULL DEFAULT '0',
  `read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sub_mail` (`mailing_id`,`subscriber_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_acajoom_stats_global`
--

DROP TABLE IF EXISTS `jml_acajoom_stats_global`;
CREATE TABLE IF NOT EXISTS `jml_acajoom_stats_global` (
  `mailing_id` int(11) NOT NULL DEFAULT '0',
  `sentdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `html_sent` int(11) NOT NULL DEFAULT '0',
  `text_sent` int(11) NOT NULL DEFAULT '0',
  `html_read` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`mailing_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jml_acajoom_stats_global`
--

INSERT INTO `jml_acajoom_stats_global` (`mailing_id`, `sentdate`, `html_sent`, `text_sent`, `html_read`) VALUES
(1, '2010-09-24 20:58:27', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jml_acajoom_subscribers`
--

DROP TABLE IF EXISTS `jml_acajoom_subscribers`;
CREATE TABLE IF NOT EXISTS `jml_acajoom_subscribers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `receive_html` tinyint(1) NOT NULL DEFAULT '1',
  `confirmed` tinyint(1) NOT NULL DEFAULT '0',
  `blacklist` tinyint(1) NOT NULL DEFAULT '0',
  `timezone` time NOT NULL DEFAULT '00:00:00',
  `language_iso` varchar(10) NOT NULL DEFAULT 'eng',
  `subscribe_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `date` (`subscribe_date`),
  KEY `joomlauserid` (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `jml_acajoom_subscribers`
--

INSERT INTO `jml_acajoom_subscribers` (`id`, `user_id`, `name`, `email`, `receive_html`, `confirmed`, `blacklist`, `timezone`, `language_iso`, `subscribe_date`, `params`) VALUES
(1, 62, 'Administrator', 'ade@microtek.uk.com', 1, 1, 0, '00:00:00', 'eng', '2010-07-27 18:50:45', NULL),
(2, 63, 'Alex Ogunsemi', 'alexogunsemi@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-09-06 09:10:44', NULL),
(3, 64, 'Olos Irenoa', 'bright_shiningstar@msn.com', 1, 1, 0, '00:00:00', 'eng', '2010-09-06 09:13:19', NULL),
(4, 65, 'Mbak Ukpe', 'mbakukpe@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-09-06 09:13:55', NULL),
(5, 66, 'Timothy Okusanya', 'timokusanya@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-09-06 09:14:42', NULL),
(6, 67, 'Esther Itang', 'estheritang@gmail.com', 1, 1, 0, '00:00:00', 'eng', '2010-09-06 09:15:25', NULL),
(7, 0, 'Peter', 'peteredohoeket@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-09-24 20:54:44', ''),
(8, 0, 'Adaramola Oluwaseyi', 'adaramolaoluwaseyi@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-09-29 08:42:13', ''),
(9, 0, 'OLATINKAN COMFORT EBUNOLUWA', 'tinkan4krist@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-10-04 21:01:54', ''),
(10, 0, 'Joseph', 'engrjossy@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-10-08 11:06:34', ''),
(11, 0, 'mxolisi', 'mxolisithabo@gmail.com', 1, 1, 0, '00:00:00', 'eng', '2010-10-20 13:49:37', ''),
(12, 0, 'Kukoyi Olurotimi', 'kukoyio@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-10-21 06:46:52', ''),
(13, 0, 'Olajide Yesufu', 'olajideyesufu@gmail.com', 1, 1, 0, '00:00:00', 'eng', '2010-12-15 07:47:37', ''),
(14, 0, 'aminu ige michael', 'michaelige40@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2010-12-28 14:22:35', ''),
(15, 0, 'Tinuke Onifade', 'tinumaria4treasure@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-01-10 17:38:20', ''),
(16, 0, 'Kela.Uwadinjo', 'kela.uwadibad@gmail.com', 1, 1, 0, '00:00:00', 'eng', '2011-01-13 13:00:18', ''),
(17, 0, 'AJAYI PAUL MAYOWA', 'realliberty@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-01-14 11:44:35', ''),
(18, 0, 'ola philemon', 'esthevine@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-01-31 20:09:01', ''),
(19, 0, 'omar farrell', 'juleesha5@hotmail.com', 1, 1, 0, '00:00:00', 'eng', '2011-02-06 12:06:05', ''),
(20, 0, 'Abraham Babatunde', 'Abrahambabatunde@ijewere.com', 1, 1, 0, '00:00:00', 'eng', '2011-02-09 10:06:33', ''),
(21, 0, 'felex mwandura', 'felex@cool.com', 1, 1, 0, '00:00:00', 'eng', '2011-02-15 16:36:07', ''),
(22, 0, 'clement', 'clementjames78@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-02-21 07:47:06', ''),
(23, 0, 'Akintoye silas Ayodele', 'asilasayodele@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-02-23 15:55:57', ''),
(24, 0, 'Prosper Mateza', 'matazpro@yahoo.co.uk', 1, 1, 0, '00:00:00', 'eng', '2011-02-25 18:54:41', ''),
(25, 0, 'Noloyiso Popo', 'noloyisopopo@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-03-10 10:51:22', ''),
(26, 0, 'daniel tetteh ajesiwor', 'ajesiwor@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-03-26 21:11:26', ''),
(27, 0, 'Oluwatobi', 'tobsowo@gmail.com', 1, 1, 0, '00:00:00', 'eng', '2011-04-23 15:51:33', ''),
(28, 0, 'dzikamai magazine', 'mutambamagaz@gmail.com', 1, 1, 0, '00:00:00', 'eng', '2011-05-14 10:30:47', ''),
(29, 0, 'sovi', 'sovipeter@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-05-28 16:50:11', ''),
(30, 0, 'darlynton', 'ebynasco@hotmail.co.uk', 1, 1, 0, '00:00:00', 'eng', '2011-06-08 10:58:00', ''),
(31, 0, 'OYESOLA&amp;#039; Ruth', 'rutoye@rocketmail.com', 1, 1, 0, '00:00:00', 'eng', '2011-06-08 15:17:26', ''),
(32, 0, 'roxana becerra', 'roxanaebg@gmail.com', 1, 1, 0, '00:00:00', 'eng', '2011-06-10 18:20:19', ''),
(33, 0, 'newa Leswadula', 'nleswadula@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-06-14 12:51:13', ''),
(34, 0, 'david', 'danjowa2008@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-06-25 12:57:29', ''),
(35, 0, 'Tiamiyu  Gbenga', 'oluwagbengatiamiyu@yahoo.co.uk', 1, 1, 0, '00:00:00', 'eng', '2011-07-05 14:38:01', ''),
(36, 0, 'Akinboye Timothy Olumide', 'tim_boye@yahoo.co.uk', 1, 1, 0, '00:00:00', 'eng', '2011-07-19 15:44:54', ''),
(37, 0, 'Dada Oluwaseyi', 'seyisunday75@yahoo.com', 1, 1, 0, '00:00:00', 'eng', '2011-08-06 15:57:39', ''),
(38, 0, 'Bamidele Olaoye', 'bamideleolaoye@gmail.com', 1, 1, 0, '00:00:00', 'eng', '2011-08-10 17:59:59', ''),
(39, 0, 'HECTOR JULIO GUILAMO P.', 'hguilamo@hotmail.com', 1, 1, 0, '00:00:00', 'eng', '2011-08-18 02:15:36', '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_acajoom_xonfig`
--

DROP TABLE IF EXISTS `jml_acajoom_xonfig`;
CREATE TABLE IF NOT EXISTS `jml_acajoom_xonfig` (
  `akey` varchar(32) NOT NULL DEFAULT '',
  `text` varchar(254) NOT NULL DEFAULT '',
  `value` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`akey`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jml_acajoom_xonfig`
--

INSERT INTO `jml_acajoom_xonfig` (`akey`, `text`, `value`) VALUES
('component', 'Acajoom', 0),
('type', 'PRO', 0),
('version', '4.1.5', 0),
('level', '3', 0),
('emailmethod', 'mail', 0),
('sendmail_from', 'ade@microtek.uk.com', 0),
('sendmail_name', 'Apostolic Faith Mission (UK) Youths', 0),
('sendmail_path', '/usr/sbin/sendmail', 0),
('smtp_host', 'localhost', 0),
('smtp_auth_required', '0', 0),
('smtp_username', '', 0),
('smtp_password', '', 0),
('embed_images', '0', 0),
('confirm_return', 'ade@microtek.uk.com', 0),
('upload_url', '/components/com_acajoom/upload', 0),
('enable_statistics', '1', 0),
('statistics_per_subscriber', '1', 0),
('send_data', '1', 0),
('allow_unregistered', '1', 0),
('require_confirmation', '0', 0),
('redirectconfirm', '', 0),
('show_login', '1', 0),
('show_logout', '1', 0),
('send_unsubcribe', '1', 0),
('confirm_fromname', 'Apostolic Faith Mission (UK) Youths', 0),
('confirm_fromemail', 'ade@microtek.uk.com', 0),
('confirm_html', '1', 0),
('time_zone', '0', 0),
('show_archive', '1', 0),
('pause_time', '20', 0),
('emails_between_pauses', '65', 0),
('wait_for_user', '0', 0),
('script_timeout', '0', 0),
('display_trace', '1', 0),
('send_log', '1', 0),
('send_auto_log', '0', 0),
('send_log_simple', '0', 0),
('send_log_closed', '1', 0),
('save_log', '0', 0),
('send_email', '1', 0),
('save_log_simple', '0', 0),
('save_log_file', '/administrator/components/com_acajoom/com_acajoom.log', 0),
('send_log_address', '@ijoobi.com', 0),
('option', 'com_sdonkey', 0),
('send_log_name', 'Acajoom Mailing Report', 0),
('homesite', 'http://www.ijoobi.com', 0),
('report_site', 'http://www.ijoobi.com', 0),
('integration', '0', 0),
('cb_plugin', '1', 0),
('cb_listIds', '0', 0),
('cb_intro', '', 0),
('cb_showname', '1', 0),
('cb_checkLists', '1', 0),
('cb_showHTML', '1', 0),
('cb_defaultHTML', '1', 0),
('cb_integration', '0', 0),
('cb_pluginInstalled', '0', 0),
('cron_max_freq', '10', 0),
('cron_max_emails', '60', 0),
('last_cron', '', 0),
('last_sub_update', '1285361696', 0),
('next_autonews', '', 0),
('show_footer', '1', 0),
('show_signature', '1', 0),
('update_url', 'http://www.ijoobi.com/update/', 0),
('date_update', '2010-09-23 08:06:52', 0),
('update_message', '', 0),
('show_guide', '1', 0),
('news1', '0', 0),
('news2', '0', 0),
('news3', '0', 0),
('cron_setup', '0', 0),
('queuedate', '', 0),
('update_avail', '0', 0),
('show_tips', '1', 0),
('update_notification', '1', 0),
('show_lists', '1', 0),
('use_sef', '0', 0),
('listHTMLeditor', '1', 0),
('mod_pub', '1', 0),
('firstmailing', '1', 0),
('nblist', '9', 0),
('license', '', 0),
('token', '', 0),
('maintenance', '', 0),
('admin_debug', '0', 0),
('send_error', '1', 0),
('report_error', '1', 0),
('fullcheck', '0', 0),
('frequency', '0', 0),
('nb_days', '7', 0),
('date_type', '1', 0),
('arv_cat', '', 0),
('arv_sec', '', 0),
('maintenance_clear', '24', 0),
('clean_stats', '90', 0),
('maintenance_date', '', 0),
('mail_format', '1', 0),
('mail_encoding', '0', 0),
('showtag', '0', 0),
('show_author', '0', 0),
('addEmailRedLink', '0', 0),
('itemidAca', '999', 0),
('show_jcalpro', '0', 0),
('show_jlinks', '0', 0),
('disabletooltip', '0', 0),
('minisendmail', '0', 0),
('word_wrap', '0', 0),
('listname0', '', 0),
('listnames0', 'All mailings', 0),
('listype0', '1', 0),
('listshow0', '1', 0),
('classes0', '', 0),
('listlogo0', 'addedit.png', 0),
('totallist0', '', 1),
('act_totallist0', '', 1),
('totalmailing0', '', 1),
('totalmailingsent0', '', 0),
('act_totalmailing0', '', 1),
('totalsubcribers0', '', 33),
('act_totalsubcribers0', '', 33),
('listname1', '_ACA_NEWSLETTER', 0),
('listnames1', '_ACA_MENU_NEWSLETTERS', 0),
('listype1', '1', 0),
('listshow1', '1', 0),
('classes1', 'newsletter', 0),
('listlogo1', 'inbox.png', 0),
('totallist1', '', 1),
('act_totallist1', '', 1),
('totalmailing1', '', 1),
('totalmailingsent1', '', 0),
('act_totalmailing1', '', 1),
('totalsubcribers1', '', 0),
('act_totalsubcribers1', '', 0),
('listname2', '_ACA_AUTORESP', 0),
('listnames2', '_ACA_MENU_AUTOS', 0),
('listype2', '1', 0),
('listshow2', '1', 0),
('classes2', 'autoresponder', 0),
('listlogo2', 'message_config.png', 0),
('totallist2', '', 0),
('act_totallist2', '', 0),
('totalmailing2', '', 0),
('totalmailingsent2', '', 0),
('act_totalmailing2', '', 0),
('totalsubcribers2', '', 0),
('act_totalsubcribers2', '', 0),
('listname3', '', 0),
('listnames3', '', 0),
('listype3', '', 0),
('listshow3', '', 0),
('classes3', '', 0),
('listlogo3', '', 0),
('totallist3', '', 0),
('act_totallist3', '', 0),
('totalmailing3', '', 0),
('totalmailingsent3', '', 0),
('act_totalmailing3', '', 0),
('totalsubcribers3', '', 0),
('act_totalsubcribers3', '', 0),
('listname4', '', 0),
('listnames4', '', 0),
('listype4', '', 0),
('listshow4', '', 0),
('classes4', '', 0),
('listlogo4', '', 0),
('totallist4', '', 0),
('act_totallist4', '', 0),
('totalmailing4', '', 0),
('totalmailingsent4', '', 0),
('act_totalmailing4', '', 0),
('totalsubcribers4', '', 0),
('act_totalsubcribers4', '', 0),
('listname5', '', 0),
('listnames5', '', 0),
('listype5', '', 0),
('listshow5', '', 0),
('classes5', '', 0),
('listlogo5', '', 0),
('totallist5', '', 0),
('act_totallist5', '', 0),
('totalmailing5', '', 0),
('totalmailingsent5', '', 0),
('act_totalmailing5', '', 0),
('totalsubcribers5', '', 0),
('act_totalsubcribers5', '', 0),
('listname6', '', 0),
('listnames6', '', 0),
('listype6', '', 0),
('listshow6', '', 0),
('classes6', '', 0),
('listlogo6', '', 0),
('totallist6', '', 0),
('act_totallist6', '', 0),
('totalmailing6', '', 0),
('totalmailingsent6', '', 0),
('act_totalmailing6', '', 0),
('totalsubcribers6', '', 0),
('act_totalsubcribers6', '', 0),
('listname7', '_ACA_AUTONEWS', 0),
('listnames7', '_ACA_MENU_AUTONEWS', 0),
('listype7', '1', 0),
('listshow7', '1', 0),
('classes7', 'autonews', 0),
('listlogo7', 'inbox.png', 0),
('totallist7', '', 0),
('act_totallist7', '', 0),
('totalmailing7', '', 0),
('totalmailingsent7', '', 0),
('act_totalmailing7', '', 0),
('totalsubcribers7', '', 0),
('act_totalsubcribers7', '', 0),
('listname8', '', 0),
('listnames8', '', 0),
('listype8', '', 0),
('listshow8', '', 0),
('classes8', '', 0),
('listlogo8', '', 0),
('totallist8', '', 0),
('act_totallist8', '', 0),
('totalmailing8', '', 0),
('totalmailingsent8', '', 0),
('act_totalmailing8', '', 0),
('totalsubcribers8', '', 0),
('act_totalsubcribers8', '', 0),
('activelist', '1,2,7', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jml_advancedmodules`
--

DROP TABLE IF EXISTS `jml_advancedmodules`;
CREATE TABLE IF NOT EXISTS `jml_advancedmodules` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`,`moduleid`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `jml_advancedmodules`
--

INSERT INTO `jml_advancedmodules` (`id`, `moduleid`, `params`) VALUES
(1, 29, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n'),
(2, 31, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n'),
(3, 38, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n'),
(4, 34, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n'),
(5, 37, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n'),
(6, 30, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n'),
(7, 32, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n'),
(8, 39, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n'),
(9, 41, 'hideempty=0\nlimit_seccats=0\nlimit_articles=0\nlimit_articles_ids=\nlimit_components=0\nlimit_components_ids=x\nlimit_date=0\npublish_up=\npublish_down=\nlimit_usergrouplevels=0\nlimit_usergrouplevels_ids=0\nlimit_languages=0\nlimit_templates=0\n');

-- --------------------------------------------------------

--
-- Table structure for table `jml_advancedmodules_menu`
--

DROP TABLE IF EXISTS `jml_advancedmodules_menu`;
CREATE TABLE IF NOT EXISTS `jml_advancedmodules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jml_advancedmodules_menu`
--

INSERT INTO `jml_advancedmodules_menu` (`moduleid`, `menuid`) VALUES
(38, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jml_banner`
--

DROP TABLE IF EXISTS `jml_banner`;
CREATE TABLE IF NOT EXISTS `jml_banner` (
  `bid` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` varchar(30) NOT NULL DEFAULT 'banner',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `imageurl` varchar(100) NOT NULL DEFAULT '',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `date` datetime DEFAULT NULL,
  `showBanner` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `custombannercode` text,
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`bid`),
  KEY `viewbanner` (`showBanner`),
  KEY `idx_banner_catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_bannerclient`
--

DROP TABLE IF EXISTS `jml_bannerclient`;
CREATE TABLE IF NOT EXISTS `jml_bannerclient` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `checked_out` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out_time` time DEFAULT NULL,
  `editor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_bannertrack`
--

DROP TABLE IF EXISTS `jml_bannertrack`;
CREATE TABLE IF NOT EXISTS `jml_bannertrack` (
  `track_date` date NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_categories`
--

DROP TABLE IF EXISTS `jml_categories`;
CREATE TABLE IF NOT EXISTS `jml_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jml_categories`
--

INSERT INTO `jml_categories` (`id`, `parent_id`, `title`, `name`, `alias`, `image`, `section`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `editor`, `ordering`, `access`, `count`, `params`) VALUES
(1, 0, 'Event Report', '', '', '', '1', '', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(2, 0, 'Testimony', '', '', '', '1', '', '', 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, ''),
(3, 0, 'Events', '', '', '', '1', '', '', 0, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, ''),
(4, 0, 'Devotional', '', 'devotional', '', '1', 'right', '', 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, ''),
(5, 0, 'Contact Us', '', '', '', '1', '', '', 1, 0, '0000-00-00 00:00:00', NULL, 5, 0, 0, ''),
(6, 0, 'Reflections', '', 'reflections', '', '1', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 6, 0, 0, ''),
(7, 0, 'Basic', '', 'basic', '', 'com_rokcandy', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(8, 0, 'Typography', '', 'typography', '', 'com_rokcandy', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(9, 0, 'Frontpage', '', 'frontpage', '', 'com_djimageslider', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, ''),
(10, 0, 'Precious Gems', '', 'precious-gems', '', '1', 'left', '', 1, 0, '0000-00-00 00:00:00', NULL, 7, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_chronoforms_ContactForm`
--

DROP TABLE IF EXISTS `jml_chronoforms_ContactForm`;
CREATE TABLE IF NOT EXISTS `jml_chronoforms_ContactForm` (
  `cf_id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) NOT NULL,
  `recordtime` varchar(255) NOT NULL,
  `ipaddress` varchar(255) NOT NULL,
  `cf_user_id` varchar(255) NOT NULL,
  PRIMARY KEY (`cf_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `jml_chronoforms_ContactForm`
--

INSERT INTO `jml_chronoforms_ContactForm` (`cf_id`, `uid`, `recordtime`, `ipaddress`, `cf_user_id`) VALUES
(1, 'INzkzOWI3ZGQwMjM15c6b3fdb53a537260c5e694d2711dd3c', '2010-10-04 - 04:31:19', '92.15.170.155', '0'),
(2, 'INWQxYjY0MzE4NDhl8b131dec79829c4ec4b13c0ff79a12c0', '2010-10-20 - 07:54:18', '196.214.146.82', '0'),
(3, 'INzU3NjFmMTllZjlkd0c121ea08c471c35ecfd07e9486d574', '2010-10-21 - 11:53:06', '173.2.11.122', '0'),
(4, 'IMWE3MDVkMjEzZGI05af330b7b35fb47530ee8320fee828fc', '2010-11-06 - 12:21:06', '87.194.42.69', '0'),
(5, 'INjEzMmNmNGZmNjAwfe87f54a58a357e9dc760c9cb0cad5e3', '2010-11-06 - 12:26:28', '87.194.42.69', '0'),
(6, 'INjIyMGJiMWU4OWNj14e8427f7e2f90c2cf2008b5aa355ea4', '2010-11-06 - 12:27:54', '87.194.42.69', '0'),
(7, 'IMjJlMjZhNzBjNDQ37efe590e588515a59b0bde2201f7e60a', '2010-12-03 - 07:39:05', '41.78.80.80', '0'),
(8, 'INzA0M2IwN2JhNTgze28bd1054d9b4c6de323fdb858be1afc', '2010-12-03 - 07:39:43', '41.78.80.80', '0'),
(9, 'IYzNlM2RjNzUyZTA0e9729b9c12371c06f3b2fa4fff8f8e36', '2010-12-15 - 01:02:53', '41.220.77.168', '0'),
(10, 'IMTZlN2U0MmQ4M2Fl8b6334f0e4f3b2ed035639f3f86f1f3f', '2011-01-09 - 04:31:35', '41.206.12.2', '0'),
(11, 'IOGU1Zjk3OWI2M2Q3962c82d5377f4dbd5273e4f9a4b29f4a', '2011-01-14 - 04:46:45', '208.122.240.2', '0'),
(12, 'IMTliOTg5NTc4MGQ123c90ffb5c848f41980e52d259b1997c', '2011-01-14 - 04:50:35', '208.122.240.2', '0'),
(13, 'IYmM4ZmU3MzRjZTg31314429381614b42218d2942bbf65383', '2011-01-14 - 04:56:36', '208.122.240.2', '0'),
(14, 'IYzMyMGJmZGY4YWI1f2df049ef4dfe1f234f33be5f1177fe1', '2011-01-14 - 04:59:39', '208.122.240.2', '0'),
(15, 'IMjY1NTM5MjAxOWRlf5f7f887c57abd8734c510e859f42937', '2011-01-18 - 12:09:02', '41.78.77.181', '0'),
(16, 'INjgzMTRkN2U4MmYz5fc0ab5629bd5bd7797b031c28d718a7', '2011-01-21 - 06:02:06', '208.122.240.2', '0'),
(17, 'INDhlNGUxYWU5YmQ10b5afba8a22e8bdc76ca8d724a83a289', '2011-01-22 - 06:53:25', '212.100.68.28', '0'),
(18, 'IM2JlYjY1NjI0Nzkw5bbbd1993b4640959e1de2c4d7566bc6', '2011-02-05 - 18:25:32', '128.243.253.101', '0'),
(19, 'IYWU3YmQyYjA5NjU5abed0a30ed1bb28a4e2a60647eb9c696', '2011-02-06 - 05:10:34', '66.87.7.152', '0'),
(20, 'IZTgwOWFlZGExMzg2dff0c776b85f2dda9c3a768260ffe8ae', '2011-02-15 - 09:40:15', '41.78.76.202', '0'),
(21, 'IODc5ZjE2MTIwNzU4fba2b334bd5ec4b3a826514b5359f9a3', '2011-02-21 - 00:58:16', '41.73.128.242', '0'),
(22, 'IYWM1NDYwYzRmNDEy764a76efbf56ced162ca2f101ece12cb', '2011-02-21 - 05:49:02', '80.3.101.223', '0'),
(23, 'INjllOWQwOWEwYzIw14aacd3192ec64054f096ff654afd0d1', '2011-02-23 - 09:35:00', '82.145.210.91', '0'),
(24, 'INGIzZjVmYTYyNjIy546b93ab3016a1332884a3d57c7e88cf', '2011-03-20 - 07:35:24', '41.204.224.17', '0'),
(25, 'IZGZlODg4YzZlMDM3d6aea4c57e0a282165712f3e68791693', '2011-04-18 - 05:21:27', '86.20.27.189', '0'),
(26, 'IZTMyMTYwYjg0ZTMw20594438e4253781b49198c95a74b720', '2011-05-16 - 07:12:33', '123.108.200.34', '0'),
(27, 'IZWMxYjZkYjA1NjFl3c958679777cef67fd96d24409978c07', '2011-06-16 - 07:39:42', '86.162.51.38', '0'),
(28, 'INDk1MzIxZmJlMWE2957d0f32d5a02a7d99bfa262ce746629', '2011-07-24 - 05:12:22', '223.176.220.40', '0'),
(29, 'IMmFhMmE4YjE0Yzg093e17654498b9071d2ffbb44d8818ee8', '2011-08-13 - 09:17:02', '117.206.34.39', '0');

-- --------------------------------------------------------

--
-- Table structure for table `jml_chrono_contact`
--

DROP TABLE IF EXISTS `jml_chrono_contact`;
CREATE TABLE IF NOT EXISTS `jml_chrono_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `html` longtext NOT NULL,
  `scriptcode` longtext NOT NULL,
  `stylecode` longtext NOT NULL,
  `redirecturl` text NOT NULL,
  `emailresults` tinyint(1) NOT NULL,
  `fieldsnames` text NOT NULL,
  `fieldstypes` text NOT NULL,
  `onsubmitcode` longtext NOT NULL,
  `onsubmitcodeb4` longtext NOT NULL,
  `server_validation` longtext NOT NULL,
  `attformtag` longtext NOT NULL,
  `submiturl` text NOT NULL,
  `emailtemplate` longtext NOT NULL,
  `useremailtemplate` longtext NOT NULL,
  `paramsall` longtext NOT NULL,
  `titlesall` longtext NOT NULL,
  `extravalrules` longtext NOT NULL,
  `dbclasses` longtext NOT NULL,
  `autogenerated` longtext NOT NULL,
  `chronocode` longtext NOT NULL,
  `theme` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `extra1` longtext NOT NULL,
  `extra2` longtext NOT NULL,
  `extra3` longtext NOT NULL,
  `extra4` longtext NOT NULL,
  `extra5` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jml_chrono_contact`
--

INSERT INTO `jml_chrono_contact` (`id`, `name`, `html`, `scriptcode`, `stylecode`, `redirecturl`, `emailresults`, `fieldsnames`, `fieldstypes`, `onsubmitcode`, `onsubmitcodeb4`, `server_validation`, `attformtag`, `submiturl`, `emailtemplate`, `useremailtemplate`, `paramsall`, `titlesall`, `extravalrules`, `dbclasses`, `autogenerated`, `chronocode`, `theme`, `published`, `extra1`, `extra2`, `extra3`, `extra4`, `extra5`) VALUES
(1, 'ContactForm', '\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Name: </label>\r\n    <input class="cf_inputbox" maxlength="150" size="30" title="" id="text_1" name="name" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Email: </label>\r\n    <input class="cf_inputbox validate-email" maxlength="150" size="30" title="" id="text_5" name="email" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textbox">\r\n    <label class="cf_label" style="width: 150px;">Phone: </label>\r\n    <input class="cf_inputbox" maxlength="150" size="30" title="" id="text_6" name="phone" type="text" />\r\n  \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_textarea">\r\n    <label class="cf_label" style="width: 150px;">Message</label>\r\n    <textarea class="cf_inputbox required" rows="3" id="text_2" title="" cols="30" name="message"></textarea>\r\n    \r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n\r\n<div class="form_item">\r\n  <div class="form_element cf_button">\r\n    <input value="Submit" name="button_3" type="submit" />\r\n  </div>\r\n  <div class="cfclear">&nbsp;</div>\r\n</div>\r\n', '', '', '', 2, 'name,email,phone,message', 'text,text,text,textarea', '<h1 style="text-align: center;"><span style="font-family: comic sans ms,sans-serif;"><em>Thank you for contacting us. <br />We will be in touch.</em></span></h1>', '', '', '', '', '', '', 'formmethod=post\nLoadFiles=Yes\nsubmissions_limit=\nsubmissions_limit_error=Sorry but you can not submit the form again very soon like this!\nhandlepostedarrays=Yes\ndebug=0\ncheckToken=1\nmysql_type=1\nenmambots=No\ncaptcha_dataload=0\ncaptcha_dataload_skip=\nuseCurrent=\ndatefieldformat=d/m/Y\ndatefieldsnames=\ndatefieldextras=classes: [''dashboard'']\ndbconnection=Yes\nsavedataorder=after_email\ndvfields=\ndvrecord=Record #n\nuploads=No\nuploadfields=\nuploadpath=/home1/apostol8/public_html/youth/components/com_chronocontact/uploads/ContactForm/\nfilename_format=$filename = date(''YmdHis'').''_''.$chronofile[''name''];\nupload_exceedslimit=Sorry, Your uploaded file size exceeds the allowed limit.\nupload_lesslimit=Sorry, Your uploaded file size is less than the allowed limit\nupload_notallowed=Sorry, Your uploaded file type is not allowed\nimagever=No\nimtype=0\nimgver_error_msg=You have entered an incorrect verification code at the bottom of the form.\nvalidate=Yes\nvalidatetype=mootools\nvalidate_onlyOnBlur=1\nvalidate_wait=\nvalidate_onlyOnSubmit=0\nvalidation_type=default\nval_required=\nval_validate_number=\nval_validate_digits=\nval_validate_alpha=\nval_validate_alphanum=\nval_validate_date=\nval_validate_email=\nval_validate_url=\nval_validate_date_au=\nval_validate_currency_dollar=\nval_validate_selection=\nval_validate_one_required=\nval_validate_confirmation=\nservervalidate=No\nautogenerated_order=3\nonsubmitcode_order=2\nplugins_order=1\nplugins=\nmplugins_order=\ntablenames=jml_chronoforms_ContactForm', '', '', '<?php\nif (!class_exists(''Tablechronoforms_ContactForm'')) {\nclass Tablechronoforms_ContactForm extends JTable {\nvar $cf_id = null;\nvar $uid = null;\nvar $recordtime = null;\nvar $ipaddress = null;\nvar $cf_user_id = null;\nfunction __construct( &$database ) {\nparent::__construct( ''jml_chronoforms_ContactForm'', ''cf_id'', $database );\n}\n}\n}\n?>\n', '<?php\n		$MyForm =& CFChronoForm::getInstance("ContactForm");\n		if($MyForm->formparams("dbconnection") == "Yes"){\n			$user = JFactory::getUser();			\n			$row =& JTable::getInstance("chronoforms_ContactForm", "Table");\n			srand((double)microtime()*10000);\n			$inum	=	"I" . substr(base64_encode(md5(rand())), 0, 16).md5(uniqid(mt_rand(), true));\n			JRequest::setVar( "recordtime", JRequest::getVar( "recordtime", date("Y-m-d")." - ".date("H:i:s"), "post", "string", "" ));\n			JRequest::setVar( "ipaddress", JRequest::getVar( "ipaddress", $_SERVER["REMOTE_ADDR"], "post", "string", "" ));\n			JRequest::setVar( "uid", JRequest::getVar( "uid", $inum, "post", "string", "" ));\n			JRequest::setVar( "cf_user_id", JRequest::getVar( "cf_user_id", $user->id, "post", "int", "" ));\n			$post = JRequest::get( "post" , JREQUEST_ALLOWRAW );			\n			if (!$row->bind( $post )) {\n				JError::raiseWarning(100, $row->getError());\n			}				\n			if (!$row->store()) {\n				JError::raiseWarning(100, $row->getError());\n			}\n			$MyForm->tablerow["jml_chronoforms_ContactForm"] = $row;\n		}\n		?>\n		', '[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_1CHRONO_CONSTANT_EOLname = nameCHRONO_CONSTANT_EOLclass = cf_inputboxCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Name: CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_5CHRONO_CONSTANT_EOLname = emailCHRONO_CONSTANT_EOLclass = cf_inputbox validate-emailCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Email: CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textbox"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_6CHRONO_CONSTANT_EOLname = phoneCHRONO_CONSTANT_EOLclass = cf_inputboxCHRONO_CONSTANT_EOLtype = textCHRONO_CONSTANT_EOLsize = 30CHRONO_CONSTANT_EOLmaxlength = 150CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = Phone: CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_textarea"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLid = text_2CHRONO_CONSTANT_EOLname = messageCHRONO_CONSTANT_EOLclass = cf_inputbox requiredCHRONO_CONSTANT_EOLrows = 3CHRONO_CONSTANT_EOLcols = 30CHRONO_CONSTANT_EOLtitle = CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 150pxCHRONO_CONSTANT_EOLlabeltext = MessageCHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLslabel = CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL[type="cf_button"CHRONO_CONSTANT_EOL{CHRONO_CONSTANT_EOLvalue = SubmitCHRONO_CONSTANT_EOLname = button_3CHRONO_CONSTANT_EOLtooltiptext = CHRONO_CONSTANT_EOLreset = 0CHRONO_CONSTANT_EOLhidelabel = 0CHRONO_CONSTANT_EOLlabelwidth = 0CHRONO_CONSTANT_EOL}]CHRONO_CONSTANT_EOL', 'default', 1, '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_chrono_contact_elements`
--

DROP TABLE IF EXISTS `jml_chrono_contact_elements`;
CREATE TABLE IF NOT EXISTS `jml_chrono_contact_elements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `placeholder` varchar(255) NOT NULL,
  `desc` text NOT NULL,
  `code` longtext NOT NULL,
  `params` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_chrono_contact_emails`
--

DROP TABLE IF EXISTS `jml_chrono_contact_emails`;
CREATE TABLE IF NOT EXISTS `jml_chrono_contact_emails` (
  `emailid` int(11) NOT NULL AUTO_INCREMENT,
  `formid` int(11) NOT NULL,
  `to` text NOT NULL,
  `dto` text NOT NULL,
  `subject` text NOT NULL,
  `dsubject` text NOT NULL,
  `cc` text NOT NULL,
  `dcc` text NOT NULL,
  `bcc` text NOT NULL,
  `dbcc` text NOT NULL,
  `fromname` text NOT NULL,
  `dfromname` text NOT NULL,
  `fromemail` text NOT NULL,
  `dfromemail` text NOT NULL,
  `replytoname` text NOT NULL,
  `dreplytoname` text NOT NULL,
  `replytoemail` text NOT NULL,
  `dreplytoemail` text NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `params` longtext NOT NULL,
  `template` longtext NOT NULL,
  PRIMARY KEY (`emailid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `jml_chrono_contact_emails`
--

INSERT INTO `jml_chrono_contact_emails` (`emailid`, `formid`, `to`, `dto`, `subject`, `dsubject`, `cc`, `dcc`, `bcc`, `dbcc`, `fromname`, `dfromname`, `fromemail`, `dfromemail`, `replytoname`, `dreplytoname`, `replytoemail`, `dreplytoemail`, `enabled`, `params`, `template`) VALUES
(15, 1, 'bright_shiningstar@msn.com', '', '', 'check0', '', '', '', '', '', 'name', '', 'email', '', '', '', '', 1, 'recordip=1\nemailtype=html\nenabled=1\neditor=1\nenable_attachments=1', '<p>Hello,</p>\r\n<p>This email is from the AFM youth website. {name} has just sent an email using the contact form and the details are displayed below.</p>\r\n<p><strong>Name: </strong><br />{name}<br /><strong>Email: </strong><br />{email}<br /><strong>Phone: </strong><br />{phone}<br /><strong>Reason for Contact:</strong> <br />{check0}<br /><strong>Message</strong> <br />{message}</p>\r\n<p> </p>\r\n<p>Please reply as appropriate.</p>\r\n<p>SystemBot.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `jml_chrono_contact_plugins`
--

DROP TABLE IF EXISTS `jml_chrono_contact_plugins`;
CREATE TABLE IF NOT EXISTS `jml_chrono_contact_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `form_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `event` varchar(255) NOT NULL,
  `params` longtext NOT NULL,
  `extra1` longtext NOT NULL,
  `extra2` longtext NOT NULL,
  `extra3` longtext NOT NULL,
  `extra4` longtext NOT NULL,
  `extra5` longtext NOT NULL,
  `extra6` longtext NOT NULL,
  `extra7` longtext NOT NULL,
  `extra8` longtext NOT NULL,
  `extra9` longtext NOT NULL,
  `extra10` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_components`
--

DROP TABLE IF EXISTS `jml_components`;
CREATE TABLE IF NOT EXISTS `jml_components` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `link` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) unsigned NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `admin_menu_link` varchar(255) NOT NULL DEFAULT '',
  `admin_menu_alt` varchar(255) NOT NULL DEFAULT '',
  `option` varchar(50) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `admin_menu_img` varchar(255) NOT NULL DEFAULT '',
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `parent_option` (`parent`,`option`(32))
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=94 ;

--
-- Dumping data for table `jml_components`
--

INSERT INTO `jml_components` (`id`, `name`, `link`, `menuid`, `parent`, `admin_menu_link`, `admin_menu_alt`, `option`, `ordering`, `admin_menu_img`, `iscore`, `params`, `enabled`) VALUES
(1, 'Banners', '', 0, 0, '', 'Banner Management', 'com_banners', 0, 'js/ThemeOffice/component.png', 0, 'track_impressions=0\ntrack_clicks=0\ntag_prefix=\n\n', 1),
(2, 'Banners', '', 0, 1, 'option=com_banners', 'Active Banners', 'com_banners', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(3, 'Clients', '', 0, 1, 'option=com_banners&c=client', 'Manage Clients', 'com_banners', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(4, 'Web Links', 'option=com_weblinks', 0, 0, '', 'Manage Weblinks', 'com_weblinks', 0, 'js/ThemeOffice/component.png', 0, 'show_comp_description=1\ncomp_description=\nshow_link_hits=1\nshow_link_description=1\nshow_other_cats=1\nshow_headings=1\nshow_page_title=1\nlink_target=0\nlink_icons=\n\n', 1),
(5, 'Links', '', 0, 4, 'option=com_weblinks', 'View existing weblinks', 'com_weblinks', 1, 'js/ThemeOffice/edit.png', 0, '', 1),
(6, 'Categories', '', 0, 4, 'option=com_categories&section=com_weblinks', 'Manage weblink categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(7, 'Contacts', 'option=com_contact', 0, 0, '', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/component.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(8, 'Contacts', '', 0, 7, 'option=com_contact', 'Edit contact details', 'com_contact', 0, 'js/ThemeOffice/edit.png', 1, '', 1),
(9, 'Categories', '', 0, 7, 'option=com_categories&section=com_contact_details', 'Manage contact categories', '', 2, 'js/ThemeOffice/categories.png', 1, 'contact_icons=0\nicon_address=\nicon_email=\nicon_telephone=\nicon_fax=\nicon_misc=\nshow_headings=1\nshow_position=1\nshow_email=0\nshow_telephone=1\nshow_mobile=1\nshow_fax=1\nbannedEmail=\nbannedSubject=\nbannedText=\nsession=1\ncustomReply=0\n\n', 1),
(10, 'Polls', 'option=com_poll', 0, 0, 'option=com_poll', 'Manage Polls', 'com_poll', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(11, 'News Feeds', 'option=com_newsfeeds', 0, 0, '', 'News Feeds Management', 'com_newsfeeds', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(12, 'Feeds', '', 0, 11, 'option=com_newsfeeds', 'Manage News Feeds', 'com_newsfeeds', 1, 'js/ThemeOffice/edit.png', 0, 'show_headings=1\nshow_name=1\nshow_articles=1\nshow_link=1\nshow_cat_description=1\nshow_cat_items=1\nshow_feed_image=1\nshow_feed_description=1\nshow_item_description=1\nfeed_word_count=0\n\n', 1),
(13, 'Categories', '', 0, 11, 'option=com_categories&section=com_newsfeeds', 'Manage Categories', '', 2, 'js/ThemeOffice/categories.png', 0, '', 1),
(14, 'User', 'option=com_user', 0, 0, '', '', 'com_user', 0, '', 1, '', 1),
(15, 'Search', 'option=com_search', 0, 0, 'option=com_search', 'Search Statistics', 'com_search', 0, 'js/ThemeOffice/component.png', 1, 'enabled=0\n\n', 1),
(16, 'Categories', '', 0, 1, 'option=com_categories&section=com_banner', 'Categories', '', 3, '', 1, '', 1),
(17, 'Wrapper', 'option=com_wrapper', 0, 0, '', 'Wrapper', 'com_wrapper', 0, '', 1, '', 1),
(18, 'Mail To', '', 0, 0, '', '', 'com_mailto', 0, '', 1, '', 1),
(19, 'Media Manager', '', 0, 0, 'option=com_media', 'Media Manager', 'com_media', 0, '', 1, 'upload_extensions=bmp,csv,doc,epg,gif,ico,jpg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,EPG,GIF,ICO,JPG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS\nupload_maxsize=10000000\nfile_path=images\nimage_path=images/stories\nrestrict_uploads=1\nallowed_media_usergroup=3\ncheck_mime=1\nimage_extensions=bmp,gif,jpg,png\nignore_extensions=\nupload_mime=image/jpeg,image/gif,image/png,image/bmp,application/x-shockwave-flash,application/msword,application/excel,application/pdf,application/powerpoint,text/plain,application/x-zip\nupload_mime_illegal=text/html\nenable_flash=0\n\n', 1),
(20, 'Articles', 'option=com_content', 0, 0, '', '', 'com_content', 0, '', 1, 'show_noauth=0\nshow_title=1\nlink_titles=1\nshow_intro=1\nshow_section=0\nlink_section=0\nshow_category=0\nlink_category=0\nshow_author=0\nshow_create_date=0\nshow_modify_date=0\nshow_item_navigation=0\nshow_readmore=1\nshow_vote=0\nshow_icons=0\nshow_pdf_icon=0\nshow_print_icon=0\nshow_email_icon=0\nshow_hits=0\nfeed_summary=0\nfilter_tags=\nfilter_attritbutes=\n\n', 1),
(21, 'Configuration Manager', '', 0, 0, '', 'Configuration', 'com_config', 0, '', 1, '', 1),
(22, 'Installation Manager', '', 0, 0, '', 'Installer', 'com_installer', 0, '', 1, '', 1),
(23, 'Language Manager', '', 0, 0, '', 'Languages', 'com_languages', 0, '', 1, '', 1),
(24, 'Mass mail', '', 0, 0, '', 'Mass Mail', 'com_massmail', 0, '', 1, 'mailSubjectPrefix=\nmailBodySuffix=\n\n', 1),
(25, 'Menu Editor', '', 0, 0, '', 'Menu Editor', 'com_menus', 0, '', 1, '', 1),
(27, 'Messaging', '', 0, 0, '', 'Messages', 'com_messages', 0, '', 1, '', 1),
(28, 'Modules Manager', '', 0, 0, '', 'Modules', 'com_modules', 0, '', 1, '', 1),
(29, 'Plugin Manager', '', 0, 0, '', 'Plugins', 'com_plugins', 0, '', 1, '', 1),
(30, 'Template Manager', '', 0, 0, '', 'Templates', 'com_templates', 0, '', 1, '', 1),
(31, 'User Manager', '', 0, 0, '', 'Users', 'com_users', 0, '', 1, 'allowUserRegistration=0\nnew_usertype=Registered\nuseractivation=1\nfrontend_userparams=1\n\n', 1),
(32, 'Cache Manager', '', 0, 0, '', 'Cache', 'com_cache', 0, '', 1, '', 1),
(33, 'Control Panel', '', 0, 0, '', 'Control Panel', 'com_cpanel', 0, '', 1, '', 1),
(34, 'Mass content', 'option=com_masscontent', 0, 0, 'option=com_masscontent', 'Mass content', 'com_masscontent', 0, 'js/ThemeOffice/component.png', 0, 'nbMassContent=10\nnbMassCategories=10\nnbMassSections=10\ndisplayAlias=1\ndisplayIntroText=1\ndisplayFullText=1\n', 1),
(35, 'Create mass content', '', 0, 34, 'option=com_masscontent&controller=content', 'Create mass content', 'com_masscontent', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(36, 'Create mass sections', '', 0, 34, 'option=com_masscontent&controller=sections', 'Create mass sections', 'com_masscontent', 1, 'js/ThemeOffice/component.png', 0, '', 1),
(37, 'Create mass categories', '', 0, 34, 'option=com_masscontent&controller=categories', 'Create mass categories', 'com_masscontent', 2, 'js/ThemeOffice/component.png', 0, '', 1),
(38, 'Delete mass content', '', 0, 34, 'option=com_masscontent&controller=delete', 'Delete mass content', 'com_masscontent', 3, 'js/ThemeOffice/component.png', 0, '', 1),
(39, 'Tor Power Calendar', 'option=com_torpowercalendar', 0, 0, 'option=com_torpowercalendar', 'Tor Power Calendar', 'com_torpowercalendar', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(40, 'Control Panel', '', 0, 39, 'option=com_torpowercalendar', 'Control Panel', 'com_torpowercalendar', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(41, 'Info', '', 0, 39, 'option=com_torpowercalendar&view=torpowercalendarinfo', 'Info', 'com_torpowercalendar', 1, 'js/ThemeOffice/component.png', 0, '', 1),
(42, 'Phoca Gallery', 'option=com_phocagallery', 0, 0, 'option=com_phocagallery', 'Phoca Gallery', 'com_phocagallery', 0, 'components/com_phocagallery/assets/images/icon-16-pg-menu.png', 0, 'categories_columns=1\nequal_percentage_width=1\ndisplay_image_categories=1\ncategories_box_width=33%\nimage_categories_size=5\ncategories_image_ordering=9\ncategories_display_avatar=0\ndisplay_subcategories=1\ndisplay_empty_categories=0\nhide_categories=\nshow_categories=\ndisplay_access_category=0\ndefault_pagination_categories=0\npagination_categories=5;10;15;20;50\nfont_color=#b36b00\nbackground_color=#fcfcfc\nbackground_color_hover=#f5f5f5\nimage_background_color=#f5f5f5\nimage_background_shadow=shadow1\nborder_color=#e8e8e8\nborder_color_hover=#b36b00\nmargin_box=5\npadding_box=5\ndisplay_name=1\ndisplay_icon_detail=1\ndisplay_icon_download=2\ndisplay_icon_folder=0\nfont_size_name=12\nchar_length_name=15\ncategory_box_space=0\ndisplay_categories_sub=0\ndisplay_subcat_page=0\ndisplay_category_icon_image=0\ncategory_image_ordering=9\ndisplay_back_button=1\ndisplay_categories_back_button=1\ndefault_pagination_category=20\npagination_category=5;10;15;20;50\ndisplay_img_desc_box=0\nfont_size_img_desc=10\nimg_desc_box_height=30\nchar_length_img_desc=300\ndisplay_categories_cv=0\ndisplay_subcat_page_cv=0\ndisplay_category_icon_image_cv=0\ncategory_image_ordering_cv=9\ndisplay_back_button_cv=1\ndisplay_categories_back_button_cv=1\ncategories_columns_cv=1\ndisplay_image_categories_cv=1\nimage_categories_size_cv=4\ndetail_window=4\ndetail_window_background_color=#ffffff\nmodal_box_overlay_color=#000000\nmodal_box_overlay_opacity=0.3\nmodal_box_border_color=#6b6b6b\nmodal_box_border_width=2\nsb_slideshow_delay=5\nsb_lang=en\nhighslide_class=rounded-white\nhighslide_opacity=0\nhighslide_outline_type=rounded-white\nhighslide_fullimg=0\nhighslide_close_button=0\nhighslide_slideshow=1\njak_slideshow_delay=5\njak_orientation=none\njak_description=1\njak_description_height=10\ndisplay_description_detail=0\ndisplay_title_description=0\nfont_size_desc=11\nfont_color_desc=#333333\ndescription_detail_height=16\ndescription_lightbox_font_size=12\ndescription_lightbox_font_color=#ffffff\ndescription_lightbox_bg_color=#000000\nslideshow_delay=2000\nslideshow_pause=0\nslideshow_random=0\ndetail_buttons=1\nphocagallery_width=\nphocagallery_center=0\ncategory_ordering=1\nimage_ordering=1\ngallery_metadesc=\ngallery_metakey=\nalt_value=1\nenable_user_cp=0\nenable_upload_avatar=1\nenable_avatar_approve=0\nenable_usercat_approve=0\nenable_usersubcat_approve=0\nuser_subcat_count=5\nmax_create_cat_char=1000\nenable_userimage_approve=0\nmax_upload_char=1000\nupload_maxsize=3145728\nupload_maxres_width=3072\nupload_maxres_height=2304\nuser_images_max_size=20971520\nenable_java=0\nenable_java_admin=1\njava_resize_width=-1\njava_resize_height=-1\njava_box_width=480\njava_box_height=480\ndisplay_rating=0\ndisplay_rating_img=0\ndisplay_comment=0\ndisplay_comment_img=0\ncomment_width=500\nmax_comment_char=1000\nexternal_comment_system=0\nenable_piclens=0\nstart_piclens=0\npiclens_image=1\nswitch_image=0\nswitch_width=640\nswitch_height=480\nswitch_fixed_size=0\nenable_overlib=0\nol_bg_color=#666666\nol_fg_color=#f6f6f6\nol_tf_color=#000000\nol_cf_color=#ffffff\noverlib_overlay_opacity=0.7\noverlib_image_rate=\ncreate_watermark=0\nwatermark_position_x=center\nwatermark_position_y=middle\ndisplay_icon_vm=0\ndisplay_category_statistics=0\ndisplay_main_cat_stat=1\ndisplay_lastadded_cat_stat=1\ncount_lastadded_cat_stat=3\ndisplay_mostviewed_cat_stat=1\ncount_mostviewed_cat_stat=3\ndisplay_camera_info=0\nexif_information=FILE.FileName;FILE.FileDateTime;FILE.FileSize;FILE.MimeType;COMPUTED.Height;COMPUTED.Width;COMPUTED.IsColor;COMPUTED.ApertureFNumber;IFD0.Make;IFD0.Model;IFD0.Orientation;IFD0.XResolution;IFD0.YResolution;IFD0.ResolutionUnit;IFD0.Software;IFD0.DateTime;IFD0.Exif_IFD_Pointer;IFD0.GPS_IFD_Pointer;EXIF.ExposureTime;EXIF.FNumber;EXIF.ExposureProgram;EXIF.ISOSpeedRatings;EXIF.ExifVersion;EXIF.DateTimeOriginal;EXIF.DateTimeDigitized;EXIF.ShutterSpeedValue;EXIF.ApertureValue;EXIF.ExposureBiasValue;EXIF.MaxApertureValue;EXIF.MeteringMode;EXIF.LightSource;EXIF.Flash;EXIF.FocalLength;EXIF.SubSecTimeOriginal;EXIF.SubSecTimeDigitized;EXIF.ColorSpace;EXIF.ExifImageWidth;EXIF.ExifImageLength;EXIF.SensingMethod;EXIF.CustomRendered;EXIF.ExposureMode;EXIF.WhiteBalance;EXIF.DigitalZoomRatio;EXIF.FocalLengthIn35mmFilm;EXIF.SceneCaptureType;EXIF.GainControl;EXIF.Contrast;EXIF.Saturation;EXIF.Sharpness;EXIF.SubjectDistanceRange;GPS.GPSLatitudeRef;GPS.GPSLatitude;GPS.GPSLongitudeRef;GPS.GPSLongitude;GPS.GPSAltitudeRef;GPS.GPSAltitude;GPS.GPSTimeStamp;GPS.GPSStatus;GPS.GPSMapDatum;GPS.GPSDateStamp\ndisplay_categories_geotagging=0\ncategories_lng=\ncategories_lat=\ncategories_zoom=2\ncategories_map_width=\ncategories_map_height=500\ndisplay_icon_geotagging=0\ndisplay_category_geotagging=0\ncategory_map_width=\ncategory_map_height=400\npagination_thumbnail_creation=0\nclean_thumbnails=0\nenable_thumb_creation=1\ncrop_thumbnail=5\njpeg_quality=85\nenable_picasa_loading=1\npicasa_load_pagination=20\nicon_format=gif\nlarge_image_width=640\nlarge_image_height=480\nmedium_image_width=100\nmedium_image_height=100\nsmall_image_width=50\nsmall_image_height=50\nfront_modal_box_width=680\nfront_modal_box_height=560\nadmin_modal_box_width=680\nadmin_modal_box_height=520\nfolder_permissions=0755\njfile_thumbs=1\n\n', 1),
(43, 'Control Panel', '', 0, 42, 'option=com_phocagallery', 'Control Panel', 'com_phocagallery', 0, 'components/com_phocagallery/assets/images/icon-16-pg-control-panel.png', 0, '', 1),
(44, 'Images', '', 0, 42, 'option=com_phocagallery&view=phocagallerys', 'Images', 'com_phocagallery', 1, 'components/com_phocagallery/assets/images/icon-16-pg-menu-gal.png', 0, '', 1),
(45, 'Categories', '', 0, 42, 'option=com_phocagallery&view=phocagallerycs', 'Categories', 'com_phocagallery', 2, 'components/com_phocagallery/assets/images/icon-16-pg-menu-cat.png', 0, '', 1),
(46, 'Themes', '', 0, 42, 'option=com_phocagallery&view=phocagalleryt', 'Themes', 'com_phocagallery', 3, 'components/com_phocagallery/assets/images/icon-16-pg-menu-theme.png', 0, '', 1),
(47, 'Category Rating', '', 0, 42, 'option=com_phocagallery&view=phocagalleryra', 'Category Rating', 'com_phocagallery', 4, 'components/com_phocagallery/assets/images/icon-16-pg-menu-vote.png', 0, '', 1),
(48, 'Image Rating', '', 0, 42, 'option=com_phocagallery&view=phocagalleryraimg', 'Image Rating', 'com_phocagallery', 5, 'components/com_phocagallery/assets/images/icon-16-pg-menu-vote-img.png', 0, '', 1),
(49, 'Category Comments', '', 0, 42, 'option=com_phocagallery&view=phocagallerycos', 'Category Comments', 'com_phocagallery', 6, 'components/com_phocagallery/assets/images/icon-16-pg-menu-comment.png', 0, '', 1),
(50, 'Image Comments', '', 0, 42, 'option=com_phocagallery&view=phocagallerycoimgs', 'Image Comments', 'com_phocagallery', 7, 'components/com_phocagallery/assets/images/icon-16-pg-menu-comment-img.png', 0, '', 1),
(51, 'Users', '', 0, 42, 'option=com_phocagallery&view=phocagalleryusers', 'Users', 'com_phocagallery', 8, 'components/com_phocagallery/assets/images/icon-16-pg-menu-users.png', 0, '', 1),
(52, 'Info', '', 0, 42, 'option=com_phocagallery&view=phocagalleryin', 'Info', 'com_phocagallery', 9, 'components/com_phocagallery/assets/images/icon-16-pg-menu-info.png', 0, '', 1),
(53, 'Xmap', 'option=com_xmap', 0, 0, 'option=com_xmap', 'Xmap', 'com_xmap', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(54, 'RokCandyBundle', '', 0, 0, '', 'RokCandyBundle', 'com_rokcandybundle', 0, '', 0, '', 0),
(55, 'RokCandy', '', 0, 0, 'option=com_rokcandy', 'RokCandy', 'com_rokcandy', 0, 'components/com_rokcandy/assets/rokcandy-icon-16.png', 1, '', 1),
(56, 'Macros', '', 0, 55, 'option=com_rokcandy', 'Macros', 'com_rokcandy', 0, 'images/blank.png', 0, '', 1),
(57, 'Categories', '', 0, 55, 'option=com_categories&section=com_rokcandy', 'Categories', 'com_rokcandy', 1, 'images/blank.png', 0, '', 1),
(58, 'RokModule', '', 0, 0, '', 'RokModule', 'com_rokmodule', 0, '', 0, '', 1),
(59, 'RokNavMenuBundle', '', 0, 0, '', 'RokNavMenuBundle', 'com_roknavmenubundle', 0, '', 0, '', 1),
(60, 'Gantry', '', 0, 0, '', 'Gantry', 'com_gantry', 0, '', 0, '', 1),
(61, 'eXtplorer', 'option=com_extplorer', 0, 0, 'option=com_extplorer', 'eXtplorer', 'com_extplorer', 0, '../administrator/components/com_extplorer/images/joomla_x_icon.png', 0, '', 1),
(62, 'EventList', 'option=com_eventlist', 0, 0, 'option=com_eventlist', 'EventList', 'com_eventlist', 0, '../administrator/components/com_eventlist/assets/images/eventlist.png', 0, 'display_num=20\ncat_num=4\nfilter=1\ndisplay=1\nicons=1\nshow_print_icon=1\nshow_email_icon=0\n\n', 1),
(63, 'DJ Image Slider', '', 0, 0, 'option=com_djimageslider', 'DJ Image Slider', 'com_djimageslider', 0, 'components/com_djimageslider/assets/icon-16-dj.png', 0, '', 1),
(64, 'Slides', '', 0, 63, 'option=com_djimageslider&view=items', 'Slides', 'com_djimageslider', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(65, 'Categories', '', 0, 63, 'option=com_categories&section=com_djimageslider', 'Categories', 'com_djimageslider', 1, 'js/ThemeOffice/component.png', 0, '', 1),
(66, 'Acajoom', 'option=com_acajoom', 0, 0, 'option=com_acajoom', 'Acajoom', 'com_acajoom', 0, '../administrator/components/com_acajoom/images/acajoom_icon.png', 0, '', 1),
(67, 'Lists', '', 0, 66, 'option=com_acajoom&act=list', 'Lists', 'com_acajoom', 0, '../includes/js/ThemeOffice/edit.png', 0, '', 1),
(68, 'Subscribers', '', 0, 66, 'option=com_acajoom&act=subscribers', 'Subscribers', 'com_acajoom', 1, '../includes/js/ThemeOffice/users_add.png', 0, '', 1),
(69, 'Newsletters', '', 0, 66, 'option=com_acajoom&act=mailing&listype=1', 'Newsletters', 'com_acajoom', 2, '../includes/js/ThemeOffice/messaging_inbox.png', 0, '', 1),
(70, 'Auto-responders', '', 0, 66, 'option=com_acajoom&act=mailing&listype=2', 'Auto-responders', 'com_acajoom', 3, '../includes/js/ThemeOffice/messaging_config.png', 0, '', 1),
(71, 'Smart-Newsletters', '', 0, 66, 'option=com_acajoom&act=mailing&listype=7', 'Smart-Newsletters', 'com_acajoom', 4, 'js/ThemeOffice/component.png', 0, '', 1),
(72, 'Statistics', '', 0, 66, 'option=com_acajoom&act=statistics', 'Statistics', 'com_acajoom', 5, '../includes/js/ThemeOffice/query.png', 0, '', 1),
(73, 'Configuration', '', 0, 66, 'option=com_acajoom&act=configuration', 'Configuration', 'com_acajoom', 6, '../includes/js/ThemeOffice/menus.png', 0, '', 1),
(74, 'Import', '', 0, 66, 'option=com_acajoom&act=update', 'Import', 'com_acajoom', 7, '../includes/js/ThemeOffice/restore.png', 0, '', 1),
(75, 'About', '', 0, 66, 'option=com_acajoom&act=about', 'About', 'com_acajoom', 8, '../includes/js/ThemeOffice/credits.png', 0, '', 1),
(76, 'JCE', 'option=com_jce', 0, 0, 'option=com_jce', 'JCE', 'com_jce', 0, 'components/com_jce/img/logo.png', 0, '', 1),
(77, 'JCE MENU CPANEL', '', 0, 76, 'option=com_jce', 'JCE MENU CPANEL', 'com_jce', 0, 'templates/khepri/images/menu/icon-16-cpanel.png', 0, '', 1),
(78, 'JCE MENU CONFIG', '', 0, 76, 'option=com_jce&type=config', 'JCE MENU CONFIG', 'com_jce', 1, 'templates/khepri/images/menu/icon-16-config.png', 0, '', 1),
(79, 'JCE MENU GROUPS', '', 0, 76, 'option=com_jce&type=group', 'JCE MENU GROUPS', 'com_jce', 2, 'templates/khepri/images/menu/icon-16-user.png', 0, '', 1),
(80, 'JCE MENU PLUGINS', '', 0, 76, 'option=com_jce&type=plugin', 'JCE MENU PLUGINS', 'com_jce', 3, 'templates/khepri/images/menu/icon-16-plugin.png', 0, '', 1),
(81, 'JCE MENU INSTALL', '', 0, 76, 'option=com_jce&type=install', 'JCE MENU INSTALL', 'com_jce', 4, 'templates/khepri/images/menu/icon-16-install.png', 0, '', 1),
(83, 'Advanced Module Manager', '', 0, 0, '', 'Advanced Module Manager', 'com_advancedmodules', 0, '', 0, '', 1),
(85, 'ReReplacer', '', 0, 0, 'option=com_rereplacer', 'ReReplacer', 'com_rereplacer', 0, 'components/com_rereplacer/images/icon-rereplacer.png', 0, '', 1),
(86, 'Chrono Forms', 'option=com_chronocontact', 0, 0, 'option=com_chronocontact', 'Chrono Forms', 'com_chronocontact', 0, 'components/com_chronocontact/CF.png', 0, '', 1),
(87, 'Forms Management', '', 0, 86, 'option=com_chronocontact&act=all', 'Forms Management', 'com_chronocontact', 0, 'js/ThemeOffice/component.png', 0, '', 1),
(88, 'Form Wizard', '', 0, 86, 'option=com_chronocontact&task=form_wizard', 'Form Wizard', 'com_chronocontact', 1, 'js/ThemeOffice/component.png', 0, '', 1),
(89, 'Wizard Custom Elements', '', 0, 86, 'option=com_chronocontact&task=wizard_elements', 'Wizard Custom Elements', 'com_chronocontact', 2, 'js/ThemeOffice/component.png', 0, '', 1),
(90, 'Menu Creator', '', 0, 86, 'option=com_chronocontact&task=menu_creator', 'Menu Creator', 'com_chronocontact', 3, 'js/ThemeOffice/component.png', 0, '', 1),
(91, 'Menu Remover', '', 0, 86, 'option=com_chronocontact&task=menu_remover', 'Menu Remover', 'com_chronocontact', 4, 'js/ThemeOffice/component.png', 0, '', 1),
(92, 'Upgrade SQL and Load Demo Form', '', 0, 86, 'option=com_chronocontact&task=doupgrade', 'Upgrade SQL and Load Demo Form', 'com_chronocontact', 5, 'js/ThemeOffice/component.png', 0, '', 1),
(93, 'Validate Installation', '', 0, 86, 'option=com_chronocontact&task=validatelicense', 'Validate Installation', 'com_chronocontact', 6, 'js/ThemeOffice/component.png', 0, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jml_contact_details`
--

DROP TABLE IF EXISTS `jml_contact_details`;
CREATE TABLE IF NOT EXISTS `jml_contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `imagepos` varchar(20) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_content`
--

DROP TABLE IF EXISTS `jml_content`;
CREATE TABLE IF NOT EXISTS `jml_content` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `title_alias` varchar(255) NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `sectionid` int(11) unsigned NOT NULL DEFAULT '0',
  `mask` int(11) unsigned NOT NULL DEFAULT '0',
  `catid` int(11) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` text NOT NULL,
  `version` int(11) unsigned NOT NULL DEFAULT '1',
  `parentid` int(11) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_section` (`sectionid`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `jml_content`
--

INSERT INTO `jml_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(1, 'George Yalley''s Testimony', 'george-yalleys-testimony', '', '<p><strong><img height="111" width="94" src="images/stories/george.jpg" alt="George Yalley - The Apostolic Faith Mission (UK)" border="0" style="margin-left: 5px; margin-right: 5px; float: left;" />I am the youngest of nine children and five other siblings. Although my father was a church steward, he was married to three wives.</strong></p>\r\n', '\r\n<p>As the last born, I was always pampered but also disciplined whenever I misbehaved. At the age of 10, my step brother returned home from Nigeria and introduced us to the Apostolic Faith and along with my two other brothers, I followed him to church.</p>\r\n<p>When I was 11 years old, I had a heart disease and I told my mother that I would not take any medication because I believed that God was able to heal me. My mother, who did not attend Apostolic Faith, said she could not afford me to die and banned me from attending Apostolic Faith. I started following her to the church where she worshipped and it was there that I eventually lost interest in going to church. My father died after I completed my O- Levels and I was left alone with my mother.</p>\r\n<p>I furthered my education and made sciences my strength. I had hoped to become a medical doctor some day and worked extremely hard to make this possible. Unfortunately, I didn’t qualify to do medicine. This began a downward trend in my life. I began drinking and going out with different women.</p>\r\n<p>Two months before I began university, my mother died. When my mother died, I felt empty. I realised that there was a great void missing in my life. I decided to study chemistry just for the sake of obtaining a degree. On several occasions, my friends invited me to church but I refused.</p>\r\n<p>I laughed at them because I thought they were wasting their time instead of studying. Little did I know that success comes from God. I was almost withdrawn from university that year for poor academic performance. One night, I was rushed to the hospital and I knew I was going to die. I realised how much of a failure my life had become.</p>\r\n<p>The doctors were unable to help me but I thank God for his mercy. He brought me through and I survived the attack. When I recovered, I heard a voice telling me I needed to go to church, only God can help me. I asked which church I should go to and was redirected to the Apostolic Faith. After wandering and weeping in the city of Kumasi Ghana for difficulties in locating the church, I said to myself “Lord, don’t let me go back to campus without locating the church”.</p>\r\n<p><strong>God heard and answered that desperate plea. </strong></p>\r\n<p>I prayed earnestly that day at the altar but I didn’t get saved. I continued to seek God and he saved my soul. He also healed me of my heart disease. I consecrated further and he sanctified and baptised me with the Holy Ghost and fire. My friends noticed the difference.</p>\r\n<p>It was such a dramatic change. I cannot enumerate the benefits of being a Christian. The Lord has done so much for me. My studies took a different turn. Each time I wanted to study, I read my Bible and sang. God always directed me to read the topics that would come out in the exam.</p>\r\n<p><strong>God gave me a good degree and today, I am a free and happy man in Christ.</strong></p>', 1, 1, 0, 2, '2010-07-28 01:35:05', 62, '', '2011-02-17 08:34:09', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '2011-02-17 23:59:59', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 7, 0, 1, '', '', 0, 233, 'robots=\nauthor='),
(2, 'Seyi Oyetuga''s Testimony', 'seyi-oyetugas-testimony', '', '<p><strong>I WANT TO THANK GOD FOR THE POWER TO SAVE AND TO DELIVER . I ALSO WANT TO THANK GOD FOR GIVING ME A CHRISTIAN PARENT, AND A PRAYERFUL ONE INDEED.</strong> \r\n', '\r\n</p>\r\n<p>I WAS BORN INTO A CHRISTIAN HOME AND RAISED IN THE CHRISTIAN WAY, I WAS ACTUALLY BORN IN THE GOSPEL. I WANT TO THANK GOD FOR SAVING MY SOUL, FOR SANCTIFYING ME AND FOR FILLING ME WITH THE HOLY GHOST AND FIRE.</p>\r\n<p>I GOT MY THREE CHRISTIAN EXPERIENCES BACK IN 2006 WHEN GOD OPENED MY EYES TO SEE THAT IF I DON''T RETRACE MY STEPS IT COULD LEAD ME TO HELL. I WENT TO OUR PASTOR TO TALK TO HIM, HE THEN ENCOURAGED ME TO PRAY FOR MY CHRISTIAN EXPERINCES.</p>\r\n<p>OUR PASTOR PRAYED WITH ME AND THAT SAME DAY GOD SAVED AND SANCTIFIED ME. LATER HE FILLED ME WITH THE HOLY GHOST AND FIRE. IN 2005 I HAD A GHASTLY CAR ACCIDENT. THE CAR I WAS IN RAN INTO THE RAILLINGS MADE FOR PEDESTRAN CROSSING, AND IT CAPSIZED. I STARTED TO CALL THE NAME OF JESUS OVER AND OVER AGAIN UNTIL THE CAR CAME TO AN HALT.</p>\r\n<p>I MANAGED TO GET OUT OF THE WRECKED CAR, THE CAR WAS DAMAGED BEYOND REPAIR. I WAS TAKEN TO THE HOSPITAL TO THE ACCIDENT AND EMERGENCY UNIT. THEY SCANNED ME AND I WAS TOLD NOTHING WAS WRONG WITH ME. THE PEOPLE THAT WITNESSED THE ACCIDENT ASKED ME WHO THE VICTIMS WERE, I SAID I WAS ONE OF THEM, THEY COULD NOT BELIEVE IT.</p>\r\n<p>I TOLD THEM ITS THE POWER OF GOD THAT DELIVERED MY FRIEND AND I FROM THE CRASH. PLEASE PRAY FOR ME, THAT THIS SAME GOD THAT HAS DONE THIS FOR ME, I WANT TO SEE HIM AND REIGN WITH HIM AT LAST.</p>', 1, 1, 0, 2, '2010-07-28 01:35:05', 62, '', '2011-02-17 08:35:08', 62, 0, '0000-00-00 00:00:00', '2011-02-18 00:00:00', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 6, 0, 2, '', '', 0, 309, 'robots=\nauthor='),
(3, 'Victor Akintola''s Testimony', 'victor-akintolas-testimony', '', '<p>To God be the glory, great things He has done for me. I lived a very rough life style. I was engaged in alcohol, night parties, fornicating, fighting and telling lies.</p>\r\n', '\r\n<p>Although I was advised against such practices, I was so trapped that it was practically impossible for me to change. I thought that donating a large sum of money to the church and giving to the poor would help me but to my disappointment they did not. I did not obey my mum who told me to stop drinking, because I thought that I was enjoying myself.</p>\r\n<p><strong>God spoke to me in a dream</strong></p>\r\n<p>In the dream, I was heading to hell. I was on top of a very high building and a scary fire was coming towards me, but I could not turn back and jump because of the height of the building. I shouted in my dream until I woke up to reality. I was terrified; I thought it was real. I was scared whenever people talked about the world coming to an end, but one day, all the fears in my heart vanished as a result of confessing my sin.</p>\r\n<p>The Lord in His mercy saved my soul and sanctified me but I did not pray through to the Baptism of the Holy Ghost. I took things for granted. After some time, sin crept in and I began to do those things I had promised the Lord I would not do. Looking outside of the Gospel landed me in a big trouble.</p>\r\n<p><strong>“Christian seek not yet repose, thou art in the midst of foes, watch and pray</strong>,” says the songwriter, but my ears were closed to that warning. I also tried without success to get out of the consequences of the sins I got into. In the midst of my problems, I could hear the gentle voice of my Saviour saying, “come to me and I will deliver you”. I yielded to that voice. I confessed my sins again to God and He saved me, sanctified me and baptised my soul with the Holy Ghost and Fire. I thank God that my problems turned into blessings. I thank God for transforming my difficulties into opportunities.</p>\r\n<p>My advice to my fellow young people is that you should seek for all your Christian experiences and not to compromise your faith for anything the world has to offer because not all that glitters is gold. You should hold on tightly and firmly with unstoppable prayer in order not to loose what you have.</p>\r\n<p>There is nothing out there in the world; it is absolutely empty. I have suffered the consequences of being careless and I can genuinely tell you that it was a terrible experience. I thank God I was ‘knocked down but not knocked out’; “though he fall, he shall not be utterly cast down: for the LORD upholdeth him with his hand” Psalm 37: 24.</p>\r\n<p>I thank God for using His people at the Apostolic Faith Church to rebuild my hope in Him. I am resolved to serve the LORD till the end of my life.</p>\r\n<p><strong><em>Victor Akintola is a member of the Apostolic Faith Mission UK, London.</em></strong></p>', 1, 1, 0, 2, '2010-07-28 01:35:05', 62, '', '2010-07-28 03:50:10', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 3, '', '', 0, 191, 'robots=\nauthor='),
(4, 'Mbak Ukpe''s Testimony', 'mbak-ukpes-testimony', '', '<p><strong>On the road to salvation I was brought up in a Christian home with parents who never failed to gather us at the end of everyday to teach us the word of God.</strong>\r\n', '\r\n</p>\r\n<p>Early years of childhood came with a very tender heart, and lessons on the second coming of Jesus gripped my heart in absolute fear…oh how I wanted to be a Christian! However, along with my teenage years came a passion for fashion and popularity amongst girls and the opposite sex, very soon that tender heart was replaced with a dark conscience and all thoughts of being saved quickly left a bitter taste. No doubt I would have Sunday conviction from time to time but as the week rolled on the devil would make me see numerous reasons why I shouldn’t be saved and I believed him.</p>\r\n<p>I thought I had life figured out and thought i was enjoying the pleasure of sin. Within years my heart hardened towards anything to do with salvation and nobody could convince me otherwise. I had planned to go abroad for Portland campmeeting with the sole intention of having a laugh, socializing and just releasing some of the tension that had begun to build up in London. I kissed my family and friends goodbye and set out on my journey. Only God knew the journey I was really embarking on.</p>\r\n<p>It only took a few days of socializing with the youths at campmeeting that I realized the majority of them had something I didn’t…they were saved. They didn’t look bored or sad you could tell in the way they testified, the constant glow on their face and the way they thrived on working for God.</p>\r\n<p>For once in my life I felt out of place and awkward, I really began to question myself, I was hardly a teenager anymore and all the years of lying and being dishonest was starting to become a burden. Maybe getting saved wasn’t such a bad idea after all? The problem was how was I going to let go of all the worldly things I clung to so dearly and be interested enough to pursue salvation. One day after a sin filled week I told God that frankly I don’t want to be saved but I know I need to and I would be happy if he could give me an interest in getting saved because otherwise it would be hard for me to pray.</p>\r\n<p>I didn’t expect the answer so soon…. By the second week of campmeeting conviction had settled on my heart in the most unimaginable way, forget the world and everything in it! I needed to get saved! There was a day I overslept during a meeting, when I awoke my first thought was “I wish I was in that meeting, I may have been saved” and I cried and cried. My days and nights were filled with praying. On occasions I would go to an almost empty tabernacle and weep to God, I had never sought for anything like this in my life!</p>\r\n<p>My eyes were bloodshot my knees were in pain but to me that was small a price to pay for so great salvation. I carried this attitude over to the London campmeeting were I just knew I was going to get saved, in fact I started practicing righteousness because I was so certain I would get saved at the London Campmeeting, It was the Monday evening after the opening of London campmeeting I was feeling particularly tired that night but I knew I wanted to pray as long as possible before going to bed, so I found myself a comfortable position.</p>\r\n<p>Before long a lady tapped me on the shoulder and invited me to the altar “if you reach out in faith tonight, this could be your night” she beckoned. Immediately the devil screamed at me that I could pray anywhere and be saved, the spirit of God reasoned with me that I had tried everything to no avail why don’t I take this leap of faith?</p>\r\n<p>I found myself at the altars pleading with God to give me this much needed change in my soul and within five minutes my prayer was answered….I was saved! Wow what a joy, peace and just an overall relief, and few months on I still feel it why did I waste so much time? I can honestly say there is no pleasure that beats being a Christian.</p>', 1, 1, 0, 2, '2010-07-28 01:35:05', 62, '', '2010-07-28 03:51:05', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 4, '', '', 0, 224, 'robots=\nauthor='),
(5, 'Kemi Adekunbi''s Testimony', 'kemi-adekunbis-testimony', '', '<p><strong>Hatred Gave Way for Salvation I grew up in a Christian home, but that did not lead me to becoming a Christian. I decided that I wanted my own life;</strong></p>\r\n', '\r\n<p>I would lie to my parents, and seek after the world, I would swear, fornicate, and deal in drugs and fraud. I would also drink to please friends around me and myself. I went too deep into sins because I thought that I was having a good life. I kept on doing things that would offend God and my parents but that did not stop my father from praying for me.</p>\r\n<p>About 4 or 5 years ago, I was pregnant and I did not know it. I woke up one morning with so much pain in my body but I still prepared to go out. However, I eventually collapsed on the stairs and my housemates had to call the ambulance to come and take me to the hospital. When I got to the hospital, the doctor said that a baby was developing outside my womb.</p>\r\n<p>They had to operate on me to take the baby out of me. I can look back now and confidently say that the Lord truly has a plan for my life. Before the camp meeting, I had prayed everywhere for my salvation but to no avail. I prayed at home, at work and at church because I was desperate for peace in my troubled life. I did not know what was blocking me from praying through.</p>\r\n<p>I would pray until I could feel God’s presence but without being saved. One day, in the cabin on the campground, I shared my feelings with a sister and eventually discovered that I was finding it difficult to be saved because of the hatred I had for my mother and some other matters.</p>\r\n<p>That very day, I went to the altar and poured out my heart, mind, feelings and all to the Lord and He forgave me for hating my mother. The Lord gave me the joy of salvation in my heart. I feel so much better and I thank God for helping me to love my mother.</p>\r\n<p>I can boldly say that God is good. I thank the Lord that I was dead in sin but now I am alive to tell people that God is good. I would like to thank the Lord for opening my eyes to see the beauty of the Gospel of Jesus Christ. Salvation is good. Pray for my sanctification and the baptism of the Holy Ghost and fire.</p>\r\n<p><strong><em>Kemi Adekunbi is a member of the Apostolic Faith Mission London, UK</em></strong></p>', 1, 1, 0, 2, '2010-07-28 01:35:05', 62, '', '2010-07-28 03:52:21', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 5, '', '', 0, 123, 'robots=\nauthor='),
(6, 'Youth Camp 2008', 'youth-camp-2008', '', '<p><strong>“Make It Happen” Pursue, Overtake……and Recover All! </strong></p>\r\n<p><strong>1st Samuel 30:8</strong>\r\n', '\r\nWe are eternally grateful to God for His blessings that He poured out on us this youth camp. We have not enough words to express our gratitude. On the evening of May 2nd, 70+ youths and elders waited for two coaches to arrive and take them to ‘Winchester House’ on the Isle of Wight.</p>\r\n<p>Also a group of about 12 delegates left Birmingham to join the main youth body. The groups were to travel down to Portsmouth and take a ferry to the Isle of Wight. The Master of the Ocean gave us a peaceful journey on the sea. Looking back, God has given us fantastic youth camp themes.</p>\r\n<p><strong>2004 was ‘JAM – Just Ask for More’</strong>: <strong>2005 ‘Tough enough’</strong>, <strong>2006 Soul Chemistry and 2007 ‘Step Up Your Game’</strong>.</p>\r\n<p>This year’s theme was the crowning of the past youth camps. The Youths were given the encouragement to make things happen in there lives. What ever may have been lost/stolen by the enemy of our soul, if we seek God, He has promised us that we ‘pursue, overtake and without fail recover all’.</p>\r\n<p>On arrival, youths were directed to where the baggage should go and then entered the Sanctuary for a short introduction and prayers. Already the Spirit of God could be felt. A peaceful and clam atmosphere pervaded the location. The youths had an introductory word from the Youth Camp Director – <strong>Bro Ola Balogun</strong>.</p>\r\n<p>Saturday Devotionals had been prepared for early Bible studies for mentees and mentors, based on 1st Samuel 30:8. Each day the youth were to look at each instruction God had given to David (‘Pursue, Overtake ……and Recover all’) and apply the message to their lives. The first programme of the day was titled ‘I can make it happen’. The youths were put into groups and were given a jigsaw puzzle to assemble together. They enjoyed it and it helped to build a good rappor amongst themselves. Unknown to the youths, some of the pieces of the jigsaw were missing. At the end of the time allocated, the groups were to reconvene in the sanctuary to display their finished/unfinished work.</p>\r\n<p>The roundup focused on the fact that God has set a stage or a beautiful scene (God’s wonderful plan for our lives) for each and every one of us. We are the missing piece to this scene and the decision is ours to choose if we want the step in that scene and ‘Make it Happen in our lives’. The second part of the morning programme was a group discussion on 4 topics: Topic 1: Blind Bartimæus Made It Happen! Mark 10: 46 - 52 Topic 2: Prodigal Son Made It Happen! Luke 15:11 - 23 Topic 3: The Woman with the issue of Blood made It Happen! Matt 9:20-23, Mark 5:25 – 34 Topic 4: The Syrophoenician woman made it Happen! Mark 7:24 - 30 The youth were to discuss the problems in the lives of the four individuals, as well as considering the solutions and apply there answers to their lives.</p>\r\n<p>Their feedback was noted down and to be presented on the last day of camp. The afternoon time was more relaxed, the guys went to play football, whilst the girls had an active aerobic session. Others went to inhale some scenery by a stroll along the beech waterfront. With fully exhausted muscles the girls and guys looked forward to a relaxing yet spirit filled film show evening to cap off the day. The film was titled ‘The Crossing’ based on how God used the death of a college student to bring his best friend to the knowledge of God’s love.</p>\r\n<p>It was evident how important it was to receive Christ’s love in order to use His rugged yet sufficient cross, to cross over to the side of Freedom. A heart felt round up was given by Bro Banji Alade as he encouraged all young campers to fall at the altar and pray and cry out to God. The spirit of God gripped the youth, and prayers ascended to God like sweet smelling incense from penitent hearts. God didn’t disappoint. Even after the prayer time, some young people went back to there rooms and longed to establish the connection with Jesus until the early hours of the morning. The connection was established as God came down to save a soul. Oh what joy that flowed in the recipient’s heart and others around.</p>\r\n<p>Sunday Sunday morning found mentors and their mentees digging into the second camp devotional. After a refreshing breakfast, once again with a heightened spirit of expectation, campers made their way to the sanctuary for Sunday school. Six classes had been prepared for various age groups. It was refreshing to have the Sunday school in a new and nature-orientated location. The spiritual benefit was of course, unaffected by the change of environment. Morning service began with an orchestral piece: Give thanks.</p>\r\n<p>This was followed by ‘I will call upon the Lord &amp; Lift the saviour up by the choir. John Olaleye gave a beautiful solo, ‘I need you’ which encouraged the youth to understand that in order to make it happen in their lives they need God. The anthem was an exhortation to ‘Bless the Lord’. The special song ‘All of Me’ was rendered by <strong>Ruth Hovertzs</strong>.</p>\r\n<p><strong>Bro. Mothlabani</strong> gave the sermon. He exhorted campers by encouraging them on ‘Pursue, overtake and recover all’. He admonished the youths be encouraged as David encouraged himself in the lord when he was facing a problem. We should pray to God, sing songs to inspire our hearts, search the scriptures and remember testimonies of how God has helped others. He explained the situation o David and how God gave the victory, and how we too can have such a victory in our lives.</p>\r\n<p>Sunday afternoon was an open ended drama, which was to involve the audience. The drama was base on three young people who had got to a cross road in there lives and had to make decision that would shape their future. The drama paused at the mid point and then the audience were split into groups to act out what they thought would be the end result of these three young people. The group feedback was mind-blowing as each group performed various scenarios in engulfing ways.</p>\r\n<p>The true conclusion of the three youths was then displayed, showing no matter where one is, what they’re running from, there isn’t any alternate route than the loving arms of Jesus saying “You can do it my child but only through me.” With a solemn conclusion the call to prayer was given as youth made personal contact with God to their hearts desire. The evening service heralded a mighty victory; the musical preludes began with flute &amp; violin duet by Ruth Alade &amp; Ruth Hovertzs.</p>\r\n<p>It was clear that God’s Holy Spirit was present and what a blessing came down. The choir sang God’s gonna do it again’ with ‘Holy spirit Rain down’ &amp; ‘The Battle belongs to the Lord. Theses songs ensured that God was truly present that night. The special was ‘God is still doing great things’ and the female quartet wrapped up with ‘Lord I come to you’ Heart-touching testimonies were given by youths, who just wanted to Thank God and who were particularly grateful for blessings received at camp.</p>\r\n<p>One particular testimony by a young female told of how she felt stuck not going back or forward. She refused to go back to her old life but had no drive to move and press forward with God. God quickly changed her circumstance around as she jumped for joy the previous Saturday evening amongst those who wanted to establish their connection with God. It was there her love for Christ bubbled up from what seemed to be an enclosed volcano, desperate to erupt. God reminded her that he still loves her and she was happy to tell the world. Bro. Seun Ogunleye gave a hair rising sermon. He posed questions to the youths to think outside of the box and be truthful to themselves at w</p>\r\n<p>here they’re at with God as if they were sat beside Jesus to watch a DVD of their lives. Souls were pricked to action to make good things happen through prayer. All fell on their knees in the sanctuary and others in nearby meeting rooms too pulled Gods hand of mercy down in penitence. The Last Day It appeared as soon as we arrived we had to leave. As campers pulled off their beddings and arranged last minute items into their suitcases, they reminisced on the events of yet another powerful youth camp, recalling what they had received and still waiting to receiving from God.</p>\r\n<p>All gathered for the last time in the sanctuary. Group feed back from Saturday’s activity was extremely profound. They illustrated their understanding of the problems in the lives of Blind Bartimeus, The Prodigal Son, and The Woman with the issue of Blood and The Syrophoenician woman; as well as considering the solutions the four characters used, to MAKE THINGS HAPPEN with Christ. The story of the Prodigal son was brought into our modern day. He had problems of greed, selfishness, disrespect, by taking his inheritance from his father, even before his death, partying all night, drinking, smoking, using cursing words, lusting and taking advantage of women, spending, spending, spending and trusting in friends for his life.</p>\r\n<p>It wasn’t until his money ran out that he discovered that all his friends, the girls and ‘fun’ would run out on him too. In Blind Bartimeus’ case, he suffered discouragement from on-lookers, even his tattered garments held him back which he quickly removed. We recalled the passage in the Bible if your eye is causing you to sin and holding you back, it is better to pluck it out and throw it in the lake of fire that you’re whole soul perishing with it. Blind Bartimeus also fumbled over the rocks and stone on the ground; the feeling of not knowing as he was blind and couldn’t see where he was going, also was a pull back.</p>\r\n<p>But he was certain of whom he wanted to reach and that was Jesus. The youths were encouraged to give practical methods to avoid such extreme ungodly activities and apply them to their lives. Bro Ola gave some final words of encouragement to all and gratitude to God for not disappointing our requests. He recalled how God answers in different ways and how this particular youth camp was unique and special as exemplified by the blanket of prayer that went before us from branches world wide; as well as the honest prayer request by campers and parents before our arrival to the site.</p>\r\n<p>Gods’ blessings were humbly and quietly spread to all, as was his Spirit ministering to saved and unsaved souls in various ways. We departed the Isle of Wight leaving a foot print of the Holy Spirit as we looked forward to another fun, blessed youth camp and ultimately a new relationship with Christ.</p>', 1, 1, 0, 1, '2010-07-28 01:35:05', 62, '', '2010-07-28 03:35:57', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 1, '', 'Youth Camp 2008: Make it Happen!!', 0, 338, 'robots=\nauthor='),
(7, 'Youth Day 2008, Birmingham', 'youth-day-2008-birmingham', '', '<p><strong>"Secret 2 Success" T<br />he fifteenth day of the fourth month of year 2007 marked the birth of youth day programme in the Birmingham branch of the Apostolic Faith Church U.K.</strong>\r\n', '\r\n</p>\r\n<p>In a twinkle of an eye, another year rolled by and there arrived again the youth day we all in Birmingham so much longed for. A day all the youths had waited and prayed for. For the second time running, the Birmingham annual youth day was held at Hutton Community Centre, Birmingham on the 13th of April, 2008 and themed; <strong>‘SECRET TO SUCCESS’.</strong></p>\r\n<p>It was a day packed with various sacred activities sandwiched with inspirational songs. The arrival of the thirteen (13) young people from London marked the beginning of the day.</p>\r\n<p>‘Seun Ogunleye opened the Sunday school programme, at 10.30 am leading the congregational songs and prayers, before the Sunday school lesson which was taken by Bro Stan Nyakuhwa. The Sunday school lesson review was taken by Bro Ola Balogun, before everyone went on their knees to pray. All the youth and the Birmingham congregation had lunch together at the worship center before the main event for the day titled. After the lunch and preparation period for the programme the youths and the Birmingham congregation had a wonderful prayer session at 15.00hrs for about half an hour.</p>\r\n<p>Two main challenges faced on that day shortly before the event which created an atmosphere of fear of low or no turn out by any of our invitees were: one, there was a major football match involving two big teams supported by most of our invitees billed for the same time as the programme, and secondly, we have always enjoyed a large audience from youths of another Christian group who have also fixed a special event for the same time. Following the prayer session we had a few of the invitees shelving the game for the programme.</p>\r\n<p>The programme commenced at 4.00pm with a welcome address given by Seun Ogunleye, which was followed by series of songs by VOH and the congregational songs led by Sister Ruth Hovert before the opening prayers by Bro. Ola Balogun. The features of the programme included a series of songs by VOH, with many of the songs involving the congregation. We had a short drama on the theme of the event featuring two young people who were introduced to the gospel of Christ by their friends while in the University.</p>\r\n<p>One of them accepted the gospel message and the other could not be bothered. They both graduated, and went their different ways. Years later they met up, the one that accepted the gospel, though he graduated with a slightly lower grade than his friend, had been successful, in life, he had a good job, a happy family whereas, his friend’s life is in shambles. He was able to tell him the secret to his success was accepting that gospel message. We had a number of inspiring testimonies from the youth highlighting how God had helped them to be successful in one thing or the other.</p>\r\n<p>The testimonies were sandwiched with some short choruses.</p>\r\n<p>A short soul searching sermon was preached by Bro. Mothlabani on the theme of the youth day <strong>“Secret to Success”.</strong> Taking his text from <strong>2 Chronicles 26:5</strong>, he admonished everyone that God has created in everyone a potential to be successful. He took Joseph as a case study Gen 39:3, 23, he admonished all that for us to be successful we must be determined to follow God through in spite all the hindrances such as discouragement, doubt, fear, laziness and procrastination that we have to deal with.</p>\r\n<p>He rounded off the message in a creative way showing that to be successful in life we must the right thing first, which is doing God’s will. At the end of this sermonette; both members and visitors fell on their knees to pour their heart to God while Bro. Banji Alade gave the closing prayer.</p>\r\n<p>It was indeed a blessing filled day as God blessed each soul that sincerely sought Him. The programme was brought to an end at 5.50pm. The London youth were thanked for their support and as they embarked on their journey back home about 6.10pm while people were still praying at the alter. In all we had a total of 58 people in attendance.</p>', 1, 1, 0, 1, '2010-07-28 01:35:05', 62, '', '2010-07-28 03:37:49', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 2, '', 'Youth Day, 2008', 0, 128, 'robots=\nauthor='),
(8, 'Reflections: STOP', 'reflections-stop', '', '<p>“STOP!” We are living in a fast world where almost everything gets done quickly. We finish our assignments to strict deadlines quickly;\r\n', '\r\n</p>\r\n<p>get to our workplace quickly; eat lunch quickly; and even gasp in the rush hour quickly. The Internet runs quickly but when it doesn’t work to our desired speed... we react in disappointment for a faster service quickly! If care isn’t taken, suddenly all this can get into our spiritual lives and we want to have a fast and quick relationship with God!</p>\r\n<p><strong>Take a deep breath…</strong></p>\r\n<p>God has been known from the beginning never to be in a hurry! 2 Peter 2:8 reads "But, beloved, be not ignorant of this one thing, that one day is with the Lord as a thousand years, and a thousand years as one day." Sometimes God may seem to ‘take ages’ but He is teaching us to be …still …and …patient.</p>\r\n<p>"Be still, and know that I am God: I will be exalted among the heathen, I will be exalted in the earth." Psalm 46:10. As I reflect on this, there was a time my life was really getting out of control until I was meditating one day and God said "STOP!" But there was more to this “STOP!” than what I thought; each of those letters meant something I needed to do.</p>\r\n<p>Here is how it went:</p>\r\n<p><strong>S = SOLITUDE</strong> with God We all need a time in quietness, just you and God and maybe a pen, paper, your Bible and that devotional or bible study book.</p>\r\n<p>We just need that time uninterrupted. God really loves it and our lives would be fresher for it. Most of all we''ll feel His valuable presence again!</p>\r\n<p><strong>T = Get in TUNE</strong> with God Sometimes we get out of step with God and we know it! We just don''t seem to be operating on His frequency. God wants our heartbeat and His to rhyme and that''s no joke, because He loves us! How about that?</p>\r\n<p><strong>O = Get your life ORGANIZED …</strong>Every part. God seldom likes when we sit pretty and console ourselves by saying, "it''s alright, it''s organised mess." No!!</p>\r\n<p>Those documents must be filed, properly and according to date, section and type; those clothes in the wardrobe must be sorted; those letters must be replied; that room must be tidy,… are you with me?</p>\r\n<p><strong>P = PRIORITIZE </strong>Get what is important completed and leave the unimportant till after! We do not need to postpone or delay needlessly things that are important or urgent, things that are right in front of us, always begging for attention but like a giant we avoid them.</p>\r\n<p>My tax return was like that to me but thank God I got it done before the deadline! Lets try and <strong>“STOP!”</strong> …In the midst of chaos, quick and hurried lives we can bask in the glory of God’s peaceful presence.</p>', 1, 1, 0, 6, '2010-07-28 01:35:05', 62, '', '2010-07-28 03:41:42', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 3, '', '', 0, 0, 'robots=\nauthor='),
(9, 'Happily Ever After', 'happily-ever-after', '', '<p><strong>Happily Ever After Outside of our original and continuing decision to serve God, we’ll probably never face a more momentous choice than who we should marry. Kind of scary, huh?</strong>\r\n', '\r\n</p>\r\n<p>If the car we decide to buy turns out to be a lemon, we can always get a different one.If the college course we decide on to fill out our credits is a bust, we can transfer. If the color we chose to paint our bedroom walls is revolting, we can head back to the paint store. But we don’t get a second chance regarding who we marry.</p>\r\n<p><strong>God designed marriage to last until death. </strong></p>\r\n<p>So, how do we make sure we’re connecting our lives with the right person? We start by recognizing that any choice based on our own reasoning, evaluation, or feelings is going to be unreliable. We can’t possibly know all there is to know about anyone. We can’t see the future. There’s really only one infallible source of guidance: God! So we start with prayer: sincere, heartfelt, determined prayer. We let God know that we want His will, and that we will not make a decision until He reveals it.</p>\r\n<p>This prayer can’t be a casual, “I’m sure this must be the one, don’t you agree, Lord?” thrown heavenward.</p>\r\n<p>And we must not pray about something already spelled out in His Word. For example, what would be the point in praying about whether it would be all right to marry a non-Christian? God’s Word clearly says, “Be ye not unequally yoked together with unbelievers” (2 Corinthians 6:14). We have the answer already.</p>\r\n<p>Next, we need to think about the attributes of true love listed in 1 Corinthians 13 and the evidences of a Spirit-filled life given in Galatians 5:22,23. Then it’s time to do some inventorying. The one we consider marrying should live and love according to these standards. Building a marriage on anything less is risky business!</p>\r\n<p>We’ll need to spend some time studying God’s pattern for marriage. Is that man or girl of our dreams ready to step into the role God has defined? The Bible teaches that wives are to submit to their husbands.</p>\r\n<p>In our day of women’s rights, submission is far from being a popular refrain. But it’s Biblical! Husbands are to love their wives as Christ loved the Church, and to be the spiritual leader, protector, and provider. Is he ready to fill that position? The advice of our parents and other spiritual counselors shouldn’t be overlooked.</p>\r\n<p>Love may be blind, but very likely our adult associates still have their eyes open. They can offer a perspective based on years of experience.</p>\r\n<p>Finally, we must take a long look at our spiritual values and goals. Will marriage to this person draw us closer to those goals and to the Lord? Will this relationship deepen our desire to serve God or distract us from that purpose? It’s critical that we don’t rush into making this vital decision.</p>\r\n<p>Yes, it’s hard to wait! We’ve all had the thought cross our minds that while we ponder and pray, all the good ones are marching to the altar. But not so. If we are waiting on God, He will have the right one come along in His time.</p>\r\n<p>Are we willing to wait?</p>', 1, 1, 0, 6, '2010-07-28 01:35:05', 62, '', '2010-07-28 03:42:55', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 4, '', '', 0, 0, 'robots=\nauthor='),
(10, 'Pray, Believing', 'pray-believing', '', '<p><strong>“Ask and it shall be given you” (Matthew 7:7) Looking at today’s society, there seems to be so much that appeals to the eye, which may not necessarily be bad things such as good education, a new car, a new job, a big house, friends, health, wealth and lots more.</strong>\r\n', '\r\n</p>\r\n<p>But these things do gradually become a burden when we solely focus ONLY on them. They block our vision to the extent that they suddenly blind us. We forget to pray, to read our Bible and meditate on it. We even forget that Jesus has already promised us in His word that we will inherit the earth that ALL we need to do is to seek Him first and ALL other things WILL be added unto us.</p>\r\n<p>We forget to thank Him for our daily bread. Thank God for His mercy endures forever – for He is willing to receive us again unto His side. He is willing to forget the neglect and He is waiting with open arms that are filled with fantastic blessings that we were unable to see when we allowed the material things of this world to blind us.</p>\r\n<p>Once we return unto his side as the prodigal sons, we are sure to receive double blessings that we would never have had if our eyes were still on those earthly things. T</p>\r\n<p>hank God for his mercy – for whatsoever, we ask, will be given us.</p>', 1, 1, 0, 6, '2010-07-28 01:35:05', 62, '', '2010-07-28 03:44:27', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:35:05', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 5, '', '', 0, 0, 'robots=\nauthor='),
(11, 'No worries', 'no-worries', '', '<p><strong>“Therefore I say unto you, Take no thought for your life, what ye shall eat, or what ye shall drink; nor yet for your body, what ye shall put on. Is not the life more than meat, and the body than raiment?” (Matthew 6:25)</strong>\r\n', '\r\nThere are several interpretations of what it means to worry. It can be described as an uneasy feeling or concern about something – to be troubled. Worry refers to negative self-talk that often distracts the mind from focusing on the problem at hand.</p>\r\n<p>For example, when students become anxious during examinations, they may be unable to remember relevant materials, they may tell themselves that they are doomed by failure or that they will not get the grades that their parents want them to.</p>\r\n<p>Unfortunately, they do not realise that in thinking this way, the speech areas of the brain that are needed to complete the examinations, for example, are being used for worrying. In other words, that part of the brain is unable to focus on the exam.</p>\r\n<p>But you know what, God Himself wants control of that emotion. After all, foxes have holes and beds have nests in which to lay their heads. God wants to serve you, thereby carrying your burden (worries).</p>\r\n<p>He ALONE wants to take pride in your affairs. You are His child and He wants NOTHING more than to have you focus on Him alone and on nothing else. So - Why worry, when you can pray, Trust Jesus and He will lead the way, Don’t be a doubting Thomas, Just lean upon His promise.</p>\r\n<p>Why worry, worry, worry, worry, When you can pray!!</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:33:03', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 10, '', 'No Worries', 0, 0, 'robots=\nauthor='),
(12, 'Devotional: Slow to ANGER', 'slow-to-anger', '', '<p><strong>“The LORD is slow to anger, and great in power” (Nahum 1:3)</strong> \r\n', '\r\nAnger is a response to a perceived threat to self or others.</p>\r\n<p>It may be expressed through active or passive behaviours. In the case of ‘active’ emotion, the angry person ‘lashes out’ verbally or physically at the intended target. In the case of ‘passive’ emotion, the angry person is characterised by sulking, passive-aggressive behaviour (hostility) and tension.</p>\r\n<p>A lot of people are in rehab, in depression, in prison and some have died because of this illness. Many have booked expensive sessions with shrinks, the nickname for psychiatrist, as many struggle to get their lives back from the damage caused by anger. The Bible tells us that we ought to be slow to anger and sin not.</p>\r\n<p>The reason for this is so that we can overcome this illness that is like a powerful force – once it has entered into one, it is hard to get it out. God wants and needs us to follow in His footsteps and if God, Himself, can do it.</p>\r\n<p>After all, He has made it clear that He is a jealous God and a judge. If He can do it so can we. Have faith in God and draw neigh to Him and He will direct thy parts. He will show you the ropes and give you the strength you need to overcome this daily tormentor in the form of anger.</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:27:33', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 9, '', 'Slow to ANGER', 0, 120, 'robots=\nauthor='),
(13, 'Prayer', 'prayer', '', '<p><strong>“Men ought always to pray, and not to faint” (Luke 18:1)</strong>\r\n', '\r\n<strong>Luke 21:36</strong> says "Watch ye therefore, and pray always, that ye may be accounted worthy to escape all these things that shall come to pass, and to stand before the Son of man".</p>\r\n<p>Prayer is an action or practice of communicating to God for the purpose of worshipping, requesting guidance, confessing sins or even to express one’s thought s and emotions. The words of prayer may therefore take form in a hymn or utterance in a person’s words. Jesus took this seriously and even tried to encourage people by teaching them how to pray in what is known as the Lord’s prayer in Matthew 6: 9–13;</p>\r\n<p><strong>1. Praise Him.</strong> “Our Father which art in heaven, Hallowed be thy name. Thy kingdom come. Thy will be done on earth, as it is in heaven.” He has done so much for us. Come on – He deserves it. Praise Him so much that His head swells – imagine what you will do when you get to be invited before the presence of not only a famous person, but the most powerful person in the world, that you highly regard.</p>\r\n<p>Wouldn’t you commend on the great things that person has done?</p>\r\n<p><strong>2. Confess your sin(s).</strong> “Give us this day our daily bread. And forgive us our debts, as we forgive our debtors.” You have to let Him know that you are indeed sorry for all the sins you have committed. The ones you know or remember and the ones you don’t even know or remember. He is faithful and just to forgive us our sins.</p>\r\n<p><strong>3. Lay your request(s).</strong> Let Him know that although He is all knowing, omniscient, you want Him to still hear from your lips all that you desire. “And lead us not into temptation, but deliver us from evil”.</p>\r\n<p>4. His will be done. Remember, you want God in total control of your life because you trust Him. But let Him know that you care for His opinion and you want His will to be done in your life. “For thine is the kingdom, and the power, and the glory for ever. Amen.”</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:19:12', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 8, '', 'Prayer; a devotional', 0, 0, 'robots=\nauthor=');
INSERT INTO `jml_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(14, 'Make this world a better place', 'make-this-world-a-better-place', '', '<p><strong>“But God commendeth his love toward us, in that, while we were yet sinners, Christ died for us.” (Romans 5:8)</strong>\r\n', '\r\nThere is a song that goes like this; Reach out and touch Somebody’s hand, Make this world a better place If you can. As you are aware, there are many different group activists determined to make this world a better place.</p>\r\n<p>A place where there will be no more wars, no more crime and punishment, no more pollution, no more wastage of recyclable materials, even no more strong division between the rich and the poor and the list goes on. These groups believe in what they are fighting for and feel that the harder they fight, the closer they are to achieving these things – regardless of the amount of hurdles they must overcome to achieve their goal! Jesus also had a goal to safe mankind from sin, which affected both the spiritual and physical lives of man.</p>\r\n<p>This goal was like no other; so divine, unique and powerful to the extent that He left His Father to come down to earth to become man and to live as man. He too like the activists was determined to go through humiliation and strife in order to safe mankind from the pollution and heartache sin has brought into this world. He came to reach out to those who would at least hear Him out and allow Him to help them by teaching God’s law, performing various forms of miracles such as making the blind see, the lame walk and even raising up the dead.</p>\r\n<p>Even to the extent of dying for the whole world simply because of His love for man to make Heaven and have everlasting life.</p>\r\n<p>Are you reaching out to make this world a better place by speaking to people about God, by helping those in need? May God help make us to reach out through His word, to help make this world a better place and by His grace you can.</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:25:26', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 7, '', 'Make this world a better place', 0, 0, 'robots=\nauthor='),
(15, 'Share the Word of God', 'share-the-word-of-god', '', '<p><strong>“Go ye therefore, and teach all nations … to observe all things whatsoever I have commanded you: and, lo, I am with you alway, even unto the end of the world. Amen.” (Matthew 28:19,20)</strong></p>\r\n<p>\r\n', '\r\n</p>\r\n<p>As Christians, we have a divine mission to share the Word of God with the world. The Bible says that we ought to teach ALL nations the Word of God. This means that anywhere we find our selves, whether at school, work, in the gym, on the bus, wherever, God is expecting us to do this.</p>\r\n<p>Of course there are some of us that may be extroverts, which makes it easier to speak openly to almost everyone and anyone. But those of us that are introverts, we can also share the word of God. For example, some of us have family members who may not be Christians, who may be scattered everywhere globally, friends at school and so on.</p>\r\n<p>We can start with them and gradually build ourselves up. But it must be noted that although God says that we should share His Word, we have to always remember to still ask for His leading. Afterall, we want God to take the glory and since this is His doctrine, we MUST let Him lead us to where we ought to go.</p>\r\n<p>That way, we will be able to find the right words to say and do the right things in order to have the attention of the people He wants us to win over to Him! And remember that in doing this, God is lining up crowns that will be added unto your crown in Heaven.</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:24:12', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 6, '', '', 0, 0, 'robots=\nauthor='),
(16, 'What’s The Game?', 'whats-the-game', '', '<p><strong>Jamie sat on the bench with his eyes stinging with hot tears. It so wasn’t fair! How did the other team manage to win?</strong></p>\r\n<p>\r\n', '\r\n</p>\r\n<p>He had been the captain for his school football team for a year now and he had led them to victory every time. The game against St John’s High School was supposed to be easy. So the team hadn’t trained much before hand. They didn’t think they needed to – they were saving their energy for the game itself. And now it was quite clear that that plan had flopped. There would be no trophy to add to the cabinet and there would be no celebration party. <strong>It was over.</strong></p>\r\n<p>We’re all called to play in a ‘game’. The point of the game is to live a Christ-centred life here on earth and then get to Heaven in the end, bringing as many people with us as possible. As young people, we have become complacent like Jamie and his team.</p>\r\n<p>The opponent is winning and if we’re not careful, we may lose the game with no prize of Heaven at the end and ultimately destruction. We need to step up our game! I John 1:9 states that in order to qualify as a player, we must confess our sins <strong>because Christ is “faithful and just to forgive us our sins, and to cleanse us from all unrighteousness”.</strong></p>\r\n<p>We must also confess our sins <strong>because our “adversary the devil, as a roaring lion, walketh about, seeking whom he may devour”</strong> according to <strong>I Peter 5:8</strong></p>\r\n<p>Note also that we get our rules from the word of God which is “light” to our paths. (Psalm 119:105) “Howbeit when He, the Spirit of truth, is come, He will guide you into all truth” (John 16:13) This assures us of our top class coach.</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:22:32', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 5, '', '', 0, 2, 'robots=\nauthor='),
(17, 'Strategy for Winning', 'strategy-for-winning', '', '<p><strong>You’re playing a game of football, you have the ball, you’re tackling, you’re getting closer and closer to the goal, you pass to a team mate, they passes back to you, you’re moving in, closer, closer, closer just a little tap and it’ll be in the goal and PHWEEEEEEEEEEEEEEEEEEEE.</strong></p>\r\n<p>\r\n', '\r\n</p>\r\n<p>The whistle goes and the game is over – you lose 2-1.</p>\r\n<p><strong>How do you feel? </strong></p>\r\n<p>Surely the referee saw that you were going to score – surely, just two more seconds and you would have tied with the opponent team! Why do you think you lost? What was the matter? Let’s compare this scenario with a few characters from the Bible: How about King Saul?</p>\r\n<p>Let’s read <strong>1st Samuel 15:13-29. </strong>He’d been a great King of Israel. He’d started the race very well, in fact he was truly winning the game of life – God was on his side – He was making headway in life but then something happened and he found himself losing!</p>\r\n<p>Let’s now look at the prodigal son in <strong>Luke 15:11-17.</strong> He was out there, he’d left his father’s house and surely all was going well for him. He was winning the game of life – he was enjoying, getting on but eventually the whistle blew and he lost the game. He ran out of money, food and friends.</p>\r\n<p>God’s desire and design is that we win the game of life. If we allow things to get in the way of us winning, we aren’t going to make it to Heaven and we’ll have quite a miserable life on earth.</p>\r\n<p>Let’s be determined to get every obstacle out of the way so we score the first three important goals of the game; <strong>Salvation, Sanctification and the Baptism of the Holy Ghost</strong> and then continue walking with God so that we will be winners when the trumpet sounds!</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:21:20', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 4, '', 'Strategy for Winning; a devotional', 0, 94, 'robots=\nauthor='),
(18, 'Wear the right gear and stay on the winning side!', 'wear-the-right-gear-and-stay-on-the-winning-side', '', '<p><strong>In the game of chess, you know that we have to watch what the opponent is doing. If not, you are finished and you’re out of the game – your opponent has won.</strong></p>\r\n<p>\r\n', '\r\n</p>\r\n<p><strong>What’s the aim of the game? </strong></p>\r\n<p>To win! Well, if we watch and pray as Christians, we wont get ‘caught out’ and lose to the Devil. We’ll be a winner in Christ instead.</p>\r\n<p>Now think of the American football. What happens if you don’t have the right gear on? You are finished! You’ll get tramped on, bruised and hurt very badly!</p>\r\n<p>Well, in all truth, if we don’t have any spiritual gear on, we’re finished spiritually. We’ll be battered and bruised before we know it and then what? We’ve lost the game. We’re out and our adversary, the Devil (I Peter 5:8), is going home with the trophy – another soul in hell!</p>\r\n<p>2 Corinthians 10:4 tells us that “the weapons of our warfare are not carnal, but mighty through God to the pulling down of strongholds”.</p>\r\n<p>We need to be sure that we have on the spiritual gear in order to win the very important game ahead of us.</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:16:17', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 3, '', 'Wear the right gear and stay on the winning side; a devotional', 0, 0, 'robots=\nauthor='),
(19, 'Not So Tough……..', 'not-so-tough', '', '<p><strong>He had God’s calling and everybody knew it. His arrival in to the world was proclaimed by an angel, he grew up to be tall, dark, handsome, an only child , had God-fearing parents, had pure blood-no toxic substances had ever touched his tongue and he would certainly put Arnold Schwarzenegger to shame! </strong></p>\r\n', '\r\n<p>The beasts of the field tried to challenge him but failed. He became the judge for his people for 20 years. The Spirit of God moved him from time to time, and the Lord was his provider, even down to the most basic need of water. The people loved him. The girls loved him, or more to the point, he loved the girls!</p>\r\n<p><strong>And that was his weakness. </strong></p>\r\n<p>Only thing is our young friend didn’t see it as a weakness. How dare anyone call him weak?! And who did his parents think they were by trying to rule his life?! God was obviously cool with him-he still helped him out when he faced his physical challenges. When the love of his life tricked him and was eventually given away to his best mate, he was livid.</p>\r\n<p>A street girl’s lure and perfume soon helped him get over that. And then he met Beauty personified. The crème de la crème of his enemies-imagine that! Who would say God wasn’t on his side now?! At first he played some trick cards, as a guy has to do, but then at last he let down his guard. This girl was heaven-sent, so what was the big deal with sharing heavens secret with an angel?</p>\r\n<p><strong>Then the biggest crash of his life! </strong></p>\r\n<p>Philistine headlines: “Zorah’s hero: blinded and in chains at the mill-for a hefty sum of 1,100 pieces of silver!” The rhythmic grind of the mill mixed with the clang of the chains, echoes of his parents’ warnings and cackling laughter of girls filled his head. Where had he gone wrong?!….. Are you feeling our friend Samson? Can you identify with him?</p>\r\n<p>Think about the problems and challenges you came up with in your P.D.P. So what’s the big deal? Why do we need to be tough? (Because life isn’t easy etc…) When we as young people fall under pressure, we drift away from God. And why do we fall? Because most of the time our spiritual fitness levels are low, or even non-existent.</p>\r\n<p>We’re just not tough enough. Why do we need to be spiritually tough? In order to maintain a close relationship with the Lord We need to be able to be positive examples Our enemy the devil is out to knock us out, using trials, temptations and rough times.</p>\r\n<p><strong>1 Peter 5:8; Ephesians 6:11-13</strong></p>\r\n<p>Ultimately, to make it to heaven we must be tough.</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2010-07-28 03:08:31', 62, 0, '0000-00-00 00:00:00', '2010-07-28 01:53:51', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 2, '', 'Not so tough; a devotional', 0, 0, 'robots=\nauthor='),
(20, 'The Work Out Program', 'the-work-out-program', '', '<p><strong>“Examine………yourselves, whether ye be in faith, prove your own selves”... …2 Corinthians 13:17</strong></p>\r\n<p>\r\n', '\r\nYesterday we figured that we face lots of problems, spiritual hindrances, and temptations that mess up our relationship with God and cause us to despair. Most of the time, we refuse to deal with these problems at their root causes.</p>\r\n<p>We tend to see each defeat in isolation rather than viewing it as the result of the constant neglect of our spiritual health. A spiritual check up will help us find out where we’re at spiritually, and help us to get real with our problems and deal with them. So, what the symptoms of a person who is spiritually unfit?</p>\r\n<p>Do you personally suffer from any of these symptoms? Now we’ve identified the symptoms, how about a work out program? The obvious first step in getting our spiritual fitness levels back up is to have that definite experience of salvation – that radical change the Bible compares to a new birth through “which old things pass away and all things become new” (2 Cor. 5:17).</p>\r\n<p>Taking the bold step to break away from our old relationships, habits and ways require some form of “toughness” which we can only have through the mighty work of salvation. Proper spiritual nourishment is also one of the primary necessities for maintaining spiritual vigour and ensuring Christian growth. We find that nourishment through time spent in reading and meditating on God’s word. Job said, “I have esteemed the words of his mouth more than my necessary food” (Job 23:12) Adequate rest is necessary for physical well-being, and it is just as important in the spiritual sense.</p>\r\n<p>Quiet moments of meditation and communion with God should be a regular part of our lives. They bring inner peace, no matter how troublesome or stressful the outward circumstances may be. Steering clear of unhealthy influences which are hazardous to our spiritual well-being will help us maintain a healthy spiritual condition.</p>\r\n<p>In 1 John 2:15, we are warned to “Love not the world, neither the things of the world.”</p>', 1, 1, 0, 4, '2010-07-28 01:53:51', 62, '', '2011-01-20 22:49:39', 62, 0, '0000-00-00 00:00:00', '2011-01-17 00:00:00', '2011-01-21 23:59:59', '', '', 'show_title=1\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=Read this devotional', 7, 0, 1, '', 'The Work Out Program: a devotional', 0, 13, 'robots=\nauthor='),
(21, 'The Choice is Yours!', 'the-choice-is-yours', '', '<p><strong>It’s decision time. Time to decide what is important in life. What really matters?</strong>\r\n', '\r\nThis is where you look beyond the here and now and think about eternity, which remember is forever!</p>\r\n<p>It’s a matter of choice not force. Yes, we may go through rough times, but the Lord gives us the promise that He will “never leave us nor forsake us” if we choose to follow him. If you are saved, great news! But there is still work to be done. You need to progress further. Now you are saved God requires much more from you.</p>\r\n<p>Using yesterday’s work out program and the passages below, write down a personal fitness action plan. Think about setting priorities and set targets for yourself, but be realistic! .</p>\r\n<p>Reading the Bible a little longer for one or two mornings won''t make a lasting differ¬ence. Spending an extra five minutes in prayer for two days in a row probably won''t bring long-term results. But if the effort is sustained, the results will follow!</p>\r\n<p>Deep commitment to keeping spiritually fit will bring great dividends in your life!</p>\r\n<p><strong>References: Colossians 3:16; 1st Peter 2:1</strong></p>', 1, 1, 0, 6, '2010-07-28 02:02:03', 62, '', '2010-07-28 03:39:33', 62, 0, '0000-00-00 00:00:00', '2010-07-28 02:02:03', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 2, '', 'The Choice is Yours!', 0, 104, 'robots=\nauthor='),
(22, 'Contact Us', 'contact-us', '', '<p>You can reach us by filling out the contact form below or through any of the methods below. We would be thrilled to hear from you, support you in prayer or&nbsp;recieve your testimony.</p>\r\n<p>Main Tel:&nbsp;+44 (0) 207 639 8897<br />Parsonage:&nbsp;+44 (0) 208 316 5290<br />Website:&nbsp;<a href="http://www.apostolicfaith.org.uk/youth">www.apostolicfaith.org.uk/youth</a><br />Youth Leader: Olos Irenoa<br />Mobile:&nbsp;+44 (0) 7908635010</p>\r\n<p>{chronocontact}ContactForm{/chronocontact}</p>', '', 1, 1, 0, 5, '2010-07-28 02:41:07', 62, '', '2010-10-04 10:28:58', 62, 0, '0000-00-00 00:00:00', '2010-07-28 02:41:07', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 5, 0, 3, '', 'Contact us at The Apostolic Faith Mission UK', 0, 501, 'robots=\nauthor='),
(23, 'Privacy Policy', 'privacy-policy', '', '<p><span class="Apple-style-span" style="WIDOWS: 2; TEXT-TRANSFORM: none; TEXT-INDENT: 0px; BORDER-COLLAPSE: separate; FONT: 11px Verdana; WHITE-SPACE: normal; ORPHANS: 2; LETTER-SPACING: normal; COLOR: #333333; WORD-SPACING: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0"></span>\r\n<p> </p>\r\n</p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">The Apostolic Faith Mission is a registered charity organisation in the United Kingdom.</p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">This privacy policy sets out how The Apostolic Faith Mission (UK) uses and protects any information that you give The Apostolic Faith Mission (UK) when you use this website.</p>\r\n<p> </p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">The Apostolic Faith Mission (UK) is committed to ensuring that your privacy is protected. Should we ask you to provide certain information by which you can be identified when using this website, then you can be assured that it will only be used in accordance with this privacy statement.</p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">The Apostolic Faith Mission (UK) may change this policy from time to time by updating this page. You should check this page from time to time to ensure that you are happy with any changes. This policy is effective from 28/Jul/2010.</p>\r\n<p> </p>\r\n<h3 style="PADDING-BOTTOM: 0px; MARGIN: 0px 0px 2px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; FONT-WEIGHT: bold; PADDING-TOP: 0px">What we collect</h3>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">We may collect the following information:</p>\r\n<ul style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.3em; MARGIN: 0px 0px 10px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">\r\n<li style="MARGIN-LEFT: 25px">\r\n<div style="FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em">name</div>\r\n</li>\r\n<li style="MARGIN-LEFT: 25px">\r\n<div style="FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em">contact information including email address</div>\r\n</li>\r\n<li style="MARGIN-LEFT: 25px">\r\n<div style="FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em">other information relevant to your inquiry</div>\r\n</li>\r\n</ul>\r\n<h3 style="PADDING-BOTTOM: 0px; MARGIN: 0px 0px 2px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; FONT-WEIGHT: bold; PADDING-TOP: 0px">What we do with the information we gather</h3>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">We require this information to understand your needs and provide you with a better service, and in particular for the following reasons:</p>\r\n<ul style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.3em; MARGIN: 0px 0px 10px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">\r\n<li style="MARGIN-LEFT: 25px">\r\n<div style="FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em">Internal record keeping.</div>\r\n</li>\r\n<li style="MARGIN-LEFT: 25px">\r\n<div style="FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em">We may use the information to improve our websites.</div>\r\n</li>\r\n<li style="MARGIN-LEFT: 25px">\r\n<div style="FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em">We may periodically send emails about new articles or other information which we think you may find interesting using the email address which you have provided.</div>\r\n</li>\r\n</ul>\r\n<h3 style="PADDING-BOTTOM: 0px; MARGIN: 0px 0px 2px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; FONT-WEIGHT: bold; PADDING-TOP: 0px">Security</h3>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">We are committed to ensuring that your information is secure. In order to prevent unauthorised access or disclosure we have put in place suitable physical, electronic and managerial procedures to safeguard and secure the information we collect online.</p>\r\n<h3 style="PADDING-BOTTOM: 0px; MARGIN: 0px 0px 2px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; FONT-WEIGHT: bold; PADDING-TOP: 0px">How we use cookies</h3>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">A cookie is a small file which asks permission to be placed on your computer''s hard drive. Once you agree, the file is added and the cookie helps analyse web traffic or lets you know when you visit a particular site. Cookies allow web applications to respond to you as an individual. The web application can tailor its operations to your needs, likes and dislikes by gathering and remembering information about your preferences.</p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">We use traffic log cookies to identify which pages are being used. This helps us analyse data about webpage traffic and improve our website in order to tailor it to customer needs. We only use this information for statistical analysis purposes and then the data is removed from the system.</p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">Overall, cookies help us provide you with a better website, by enabling us to monitor which pages you find useful and which you do not. A cookie in no way gives us access to your computer or any information about you, other than the data you choose to share with us.</p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">You can choose to accept or decline cookies. Most web browsers automatically accept cookies, but you can usually modify your browser setting to decline cookies if you prefer. This may prevent you from taking full advantage of the website.</p>\r\n<h3 style="PADDING-BOTTOM: 0px; MARGIN: 0px 0px 2px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; FONT-WEIGHT: bold; PADDING-TOP: 0px">Links to other websites</h3>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">Our website may contain links to other websites of interest. However, once you have used these links to leave our site, you should note that we do not have any control over that other website. Therefore, we cannot be responsible for the protection and privacy of any information which you provide whilst visiting such sites and such sites are not governed by this privacy statement. You should exercise caution and look at the privacy statement applicable to the website in question.</p>\r\n<h3 style="PADDING-BOTTOM: 0px; MARGIN: 0px 0px 2px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; FONT-WEIGHT: bold; PADDING-TOP: 0px">Controlling your personal information</h3>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">You may choose to restrict the collection or use of your personal information in the following ways:</p>\r\n<ul style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.3em; MARGIN: 0px 0px 10px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">\r\n<li style="MARGIN-LEFT: 25px">\r\n<div style="FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em">whenever you are asked to fill in a form on the website, look for the box that you can click to indicate that you do not want the information to be used by anybody for direct marketing purposes</div>\r\n</li>\r\n<li style="MARGIN-LEFT: 25px">\r\n<div style="FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em">if you have previously agreed to us using your personal information for direct marketing purposes, you may change your mind at any time by writing to or emailing us at [email address]</div>\r\n</li>\r\n</ul>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">We will not sell, distribute or lease your personal information to third parties unless we have your permission or are required by law to do so. We may use your personal information to send you promotional information about third parties which we think you may find interesting if you tell us that you wish this to happen.</p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">You may request details of personal information which we hold about you under the Data Protection Act 1998. A small fee will be payable. If you would like a copy of the information held on you please write to:</p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px"><strong>95 Fenham Road, Peckham, London. SE15 1AE</strong></p>\r\n<p style="PADDING-BOTTOM: 0px; LINE-HEIGHT: 1.27em; MARGIN: 0px 0px 15px; PADDING-LEFT: 0px; PADDING-RIGHT: 0px; FONT-FAMILY: Verdana, Geneva, Arial, Helvetica, sans-serif; COLOR: #333333; FONT-SIZE: 1em; PADDING-TOP: 0px">If you believe that any information we are holding on you is incorrect or incomplete, please write to or email us as soon as possible, at the above address. We will promptly correct any information found to be incorrect.</p>', '', 1, 1, 0, 5, '2010-07-28 02:48:40', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2010-07-28 02:48:40', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 2, '', '', 0, 58, 'robots=\nauthor='),
(24, 'Terms of Website Usage', 'terms-of-website-usage', '', '<p><span class="Apple-style-span" style="widows: 2; text-transform: none; text-indent: 0px; border-collapse: separate; font: 11px Verdana; white-space: normal; orphans: 2; letter-spacing: normal; color: #333333; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0;">\r\n<h3 style="margin: 0px 0px 2px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; font-weight: bold; padding: 0px;">Terms and conditions template for website usage</h3>\r\n<p style="line-height: 1.27em; margin: 0px 0px 15px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; padding: 0px;">Welcome to our website. If you continue to browse and use this website you are agreeing to comply with and be bound by the following terms and conditions of use, which together with our privacy policy govern The Apostolic Faith''s relationship with you in relation to this website.</p>\r\n<p style="line-height: 1.27em; margin: 0px 0px 15px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; padding: 0px;">The term ''The Apotolic Faith Mission'' or ''us'' or ''we'' refers to the owner of the website whose registered office is 95 Fenham Road, London; SE15 1AE. The term ''you'' refers to the user or viewer of our website.</p>\r\n<p style="line-height: 1.27em; margin: 0px 0px 15px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; padding: 0px;"><strong>The use of this website is subject to the following terms of use:</strong></p>\r\n<ul style="line-height: 1.3em; margin: 0px 0px 10px; font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; color: #333333; font-size: 1em; padding: 0px;">\r\n<li style="margin-left: 25px;">The content of the pages of this website is for your general information and use only. It is subject to change without notice.</li>\r\n<li style="margin-left: 25px;">Neither we nor any third parties provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and we expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law.</li>\r\n<li style="margin-left: 25px;">Your use of any information or materials on this website is entirely at your own risk, for which we shall not be liable. It shall be your own responsibility to ensure that any products, services or information available through this website meet your specific requirements.</li>\r\n<li style="margin-left: 25px;">This website contains material which is owned by or licensed to us. This material includes, but is not limited to, the design, layout, look, appearance and graphics. Reproduction is prohibited other than in accordance with the copyright notice, which forms part of these terms and conditions.</li>\r\n<li style="margin-left: 25px;">All trade marks reproduced in this website which are not the property of, or licensed to, the operator are acknowledged on the website.</li>\r\n<li style="margin-left: 25px;">Unauthorised use of this website may give rise to a claim for damages and/or be a criminal offence.</li>\r\n<li style="margin-left: 25px;">From time to time this website may also include links to other websites. These links are provided for your convenience to provide further information. They do not signify that we endorse the website(s). We have no responsibility for the content of the linked website(s).</li>\r\n<li style="margin-left: 25px;">You may not create a link to this website from another website or document without our prior written consent.</li>\r\n<li style="margin-left: 25px;">Your use of this website and any dispute arising out of such use of the website is subject to the laws of England, Scotland and Wales.</li>\r\n</ul>\r\n</span></p>', '', 1, 1, 0, 5, '2010-07-28 02:54:01', 62, '', '0000-00-00 00:00:00', 0, 62, '2010-07-28 03:03:13', '2010-07-28 02:54:01', '0000-00-00 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 1, '', '', 0, 71, 'robots=\nauthor='),
(25, 'Poem by Bro. Banijo', 'poem-by-bro-banijo', '', '<a href="index.php?option=com_content&amp;view=article&amp;id=10:pray-believing&amp;catid=6">\r\n<p><img src="images/stories/multimedia.jpg" /></p>\r\n<p>Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium.</p>\r\n', '\r\n<p>&nbsp;Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium.Lorem ipsum del irium.Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium.</p>\r\n<p>Lorem i[sum,</p>\r\n<p> </p>\r\n</a><a></a><a></a><a href="index.php?option=com_content&amp;view=article&amp;id=10:pray-believing&amp;catid=6"></a>', -2, 1, 0, 2, '2010-09-14 00:00:00', 62, '', '2010-09-24 20:36:03', 62, 0, '0000-00-00 00:00:00', '2010-09-23 00:00:00', '2010-09-30 00:00:00', '', '', 'show_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=1\nlanguage=\nkeyref=\nreadmore=Read everything', 8, 0, 0, '', '', 0, 4, 'robots=\nauthor='),
(26, 'God Knows Best', 'god-knows-best', '', '<p>Our Father knows what''s best for us,<br />So why should we complain ...<br />We always want the sunshine,<br />But He knows there must be rain.</p>\r\n<p><img src="http://65.55.75.167/att/GetInline.aspx?messageid=206bce47-da56-48e2-9102-ce97b76e776e&amp;attindex=1&amp;cp=-1&amp;attdepth=1&amp;imgsrc=cid%3a2.2429517737%40web31504.mail.mud.yahoo.com&amp;shared=1&amp;hm__login=bright_shiningstar&amp;hm__domain=msn.com&amp;ip=10.13.200.8&amp;d=d4055&amp;mf=2&amp;hm__ts=Thu%2c%2030%20Sep%202010%2018%3a25%3a28%20GMT&amp;st=bright_shiningstar%402&amp;hm__ha=01_f5089d8d2007e09c381f1e3b77abd87df632154cc25246ec402d36cdce6134d9&amp;oneredir=1" /></p>\r\n<p>We love the sound of laughter <br />And the merriment of cheer;<br />But our hearts would lose their tenderness <br />If we never shed a tear.</p>\r\n<p><img src="http://65.55.75.167/att/GetInline.aspx?messageid=206bce47-da56-48e2-9102-ce97b76e776e&amp;attindex=2&amp;cp=-1&amp;attdepth=2&amp;imgsrc=cid%3a3.2429517737%40web31504.mail.mud.yahoo.com&amp;shared=1&amp;hm__login=bright_shiningstar&amp;hm__domain=msn.com&amp;ip=10.13.200.8&amp;d=d4055&amp;mf=2&amp;hm__ts=Thu%2c%2030%20Sep%202010%2018%3a25%3a28%20GMT&amp;st=bright_shiningstar%402&amp;hm__ha=01_93aeffa4939a40ef5ccfbe514a5c808f74f53de884d1ac9b38a2334738ddfd53&amp;oneredir=1" /></p>\r\n<p>Our Father tests us often <br />With suffering and with sorrow;<br />He tests us, not to punish us,<br />But to help us meet "tomorrow."</p>\r\n<p><img src="http://65.55.75.167/att/GetInline.aspx?messageid=206bce47-da56-48e2-9102-ce97b76e776e&amp;attindex=3&amp;cp=-1&amp;attdepth=3&amp;imgsrc=cid%3a4.2429517737%40web31504.mail.mud.yahoo.com&amp;shared=1&amp;hm__login=bright_shiningstar&amp;hm__domain=msn.com&amp;ip=10.13.200.8&amp;d=d4055&amp;mf=2&amp;hm__ts=Thu%2c%2030%20Sep%202010%2018%3a25%3a28%20GMT&amp;st=bright_shiningstar%402&amp;hm__ha=01_b4d932eb9afb7c73b48ed3fe727b8246b4f88a2ca36f7b1950f477ea388c6d38&amp;oneredir=1" /></p>\r\n<p>For growing trees are strengthened <br />When they withstand the storm;<br />And the sharp cut of the chisel <br />Gives the marble grace and form.</p>\r\n<p><img src="http://65.55.75.167/att/GetInline.aspx?messageid=206bce47-da56-48e2-9102-ce97b76e776e&amp;attindex=4&amp;cp=-1&amp;attdepth=4&amp;imgsrc=cid%3a5.2429517737%40web31504.mail.mud.yahoo.com&amp;shared=1&amp;hm__login=bright_shiningstar&amp;hm__domain=msn.com&amp;ip=10.13.200.8&amp;d=d4055&amp;mf=2&amp;hm__ts=Thu%2c%2030%20Sep%202010%2018%3a25%3a28%20GMT&amp;st=bright_shiningstar%402&amp;hm__ha=01_308e32aefa750c135bf034a47baa03ad3596963e7f6fc4bf2ec6370fa0c9c56e&amp;oneredir=1" /></p>\r\n<p>God never hurts us needlessly,<br />And He never wastes our pain;<br />For every loss He sends to us <br />Is followed by rich gain.</p>\r\n<p><img src="http://65.55.75.167/att/GetInline.aspx?messageid=206bce47-da56-48e2-9102-ce97b76e776e&amp;attindex=5&amp;cp=-1&amp;attdepth=5&amp;imgsrc=cid%3a6.2429517737%40web31504.mail.mud.yahoo.com&amp;shared=1&amp;hm__login=bright_shiningstar&amp;hm__domain=msn.com&amp;ip=10.13.200.8&amp;d=d4055&amp;mf=2&amp;hm__ts=Thu%2c%2030%20Sep%202010%2018%3a25%3a28%20GMT&amp;st=bright_shiningstar%402&amp;hm__ha=01_aff9d5cc5f19e85fd7bbd02bfda19a3ef54911c611f73880893d6b42f5b3ff84&amp;oneredir=1" /></p>\r\n<p>And when we count the blessings <br />That God has so freely sent;<br />We will find no cause for murmuring <br />And no time to lament.</p>\r\n<p><img src="http://65.55.75.167/att/GetInline.aspx?messageid=206bce47-da56-48e2-9102-ce97b76e776e&amp;attindex=6&amp;cp=-1&amp;attdepth=6&amp;imgsrc=cid%3a7.2429517737%40web31504.mail.mud.yahoo.com&amp;shared=1&amp;hm__login=bright_shiningstar&amp;hm__domain=msn.com&amp;ip=10.13.200.8&amp;d=d4055&amp;mf=2&amp;hm__ts=Thu%2c%2030%20Sep%202010%2018%3a25%3a28%20GMT&amp;st=bright_shiningstar%402&amp;hm__ha=01_85878e60c69b53817bd4a3c1aa499577977a38f9c3982f9aaffcfef51ea9d334&amp;oneredir=1" /></p>\r\n<p>For Our Father loves His children,<br />And to Him all things are plain;<br />So He never sends us "pleasure"<br />When the "soul''s deep need is pain."</p>\r\n<p><img src="http://65.55.75.167/att/GetInline.aspx?messageid=206bce47-da56-48e2-9102-ce97b76e776e&amp;attindex=7&amp;cp=-1&amp;attdepth=7&amp;imgsrc=cid%3a8.2429517737%40web31504.mail.mud.yahoo.com&amp;shared=1&amp;hm__login=bright_shiningstar&amp;hm__domain=msn.com&amp;ip=10.13.200.8&amp;d=d4055&amp;mf=2&amp;hm__ts=Thu%2c%2030%20Sep%202010%2018%3a25%3a28%20GMT&amp;st=bright_shiningstar%402&amp;hm__ha=01_d807cf74dbcb3034713c45195b2deb23a32c8352c7214853a9c797e77b322044&amp;oneredir=1" /></p>\r\n<p>So whenever we are troubled,<br />And when everything goes wrong,<br />It is just God working in us <br />To make "our spirits strong."</p>', '', 0, 1, 0, 6, '2010-09-30 00:00:00', 62, 'Unkown', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2010-10-25 00:00:00', '2010-11-05 00:00:00', '', '', 'show_title=1\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=1\nlink_category=1\nshow_vote=0\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=1\nshow_print_icon=1\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 1, '', '', 0, 0, 'robots=\nauthor='),
(40, 'Precious Gems  11', 'precious-gems-11', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="font-family: book antiqua,palatino;"><strong><span style="font-size: 14pt;"><span style="color: #0000ff;">11<sup>th</sup> January 2011</span></span></strong></span></p>\r\n<p><span style="font-family: book antiqua,palatino;"><em><span style="font-size: 12pt;">Order my steps in thy word</span></em></span></p>\r\n<p class="font-family-georgia" style="padding-left: 180px;"><em><span class="font-family-georgia" style="font-family: book antiqua,palatino;"><span class="font-family-georgia" style="font-size: 12pt;">Ps. 119:153</span></span></em></p>\r\n<p> </p>', '', 1, 1, 0, 10, '2011-01-10 23:33:57', 62, '', '2011-01-10 23:40:34', 62, 0, '0000-00-00 00:00:00', '2011-01-11 00:00:00', '2011-01-11 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=en-GB\nkeyref=\nreadmore=', 3, 0, 24, '', '', 0, 1, 'robots=\nauthor='),
(29, 'Precious Gems  1', 'january-2011', '', '<p style="text-align: left;">&nbsp;<img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="color: #0000ff;"><span class="shadow-bl" style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;"><strong>1<sup>st</sup> January 2011</strong></span></span></span></span></p>\r\n<p>&nbsp;<em><span class="font-family-georgia"><span class="font-family-optima" style="font-family: book antiqua, palatino;"><span style="font-size: 12pt;">The LORD, He it is that doth go before thee; </span></span></span></em></p>\r\n<p><em><span class="font-family-georgia"><span style="font-family: book antiqua, palatino;"><span style="font-size: 12pt;">He will be with thee, He will not fail thee...</span></span></span></em></p>\r\n<p style="padding-left: 90px;"><span style="font-family: book antiqua,palatino;"><em><span class="font-family-georgia">&nbsp;</span></em><em><span class="font-family-georgia">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deut. 31:8</span></em></span></p>', '', 1, 1, 0, 10, '2010-12-31 15:52:24', 62, '', '2011-01-02 20:18:51', 62, 0, '0000-00-00 00:00:00', '2011-01-01 00:00:00', '2011-01-01 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=en-GB\nkeyref=\nreadmore=', 24, 0, 34, '', '', 0, 0, 'robots=\nauthor='),
(30, 'Precious Gems  2', 'precious-gems-january-2011', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><strong><span style="color: #0000ff;">2<sup>nd</sup> January 2011</span></strong></span></span></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em>&nbsp;The LORD shall guide thee continually, ..</em></span></span></p>\r\n<p style="padding-left: 90px;"><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="font-family-georgia" style="font-family: book antiqua,palatino;">Isa. 58:11</span></em></p>', '', 1, 1, 0, 10, '2010-12-31 16:32:56', 62, '', '2011-01-02 20:19:12', 62, 0, '0000-00-00 00:00:00', '2011-01-02 00:00:00', '2011-01-02 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=en-GB\nkeyref=\nreadmore=', 10, 0, 33, '', '', 0, 0, 'robots=\nauthor='),
(31, 'Precious Gems 3', 'precious-gems-3', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">3<sup>rd</sup> January 2011</span></span></strong></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em>As thy days, so shall they Strength be.</em></span></span></p>\r\n<p style="padding-left: 90px;"><span style="font-family: book antiqua,palatino;"><span class="font-family-georgia"><em>&nbsp;</em><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Deut. 33.25</em></span></span></p>', '', 1, 1, 0, 10, '2010-12-31 18:57:49', 62, '', '2011-01-02 20:20:01', 62, 0, '0000-00-00 00:00:00', '2011-01-03 00:00:00', '2011-01-03 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 32, '', '', 0, 0, 'robots=\nauthor='),
(32, 'Precious Gems 4', 'precious-gems-4', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="font-family: book antiqua,palatino;"><strong><span style="font-size: 14pt;"><span style="color: #0000ff;">4<sup>th</sup> January 2011</span></span></strong></span></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em>The eyes of the LORD are over the righteous</em></span></span></p>\r\n<p style="padding-left: 210px;"><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em class="font-family-georgia">1 Pet. 3:12</em></span></span></p>', '', 1, 1, 0, 10, '2010-12-31 19:08:56', 62, '', '2011-01-02 20:21:02', 62, 0, '0000-00-00 00:00:00', '2011-01-04 00:00:00', '2011-01-04 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 4, 0, 31, '', '', 0, 1, 'robots=\nauthor=');
INSERT INTO `jml_content` (`id`, `title`, `alias`, `title_alias`, `introtext`, `fulltext`, `state`, `sectionid`, `mask`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `parentid`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`) VALUES
(33, 'Percious gems 5', 'percious-gems-5', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><strong><span style="color: #0000ff;">5<sup>th</sup> January 2011</span></strong></span></span></p>\r\n<p><span style="font-family: book antiqua,palatino;"><em><span style="font-size: 12pt;">Rest in the LORD, and wait patiently for Him</span></em></span></p>\r\n<p style="padding-left: 240px;"><span class="font-family-georgia" style="font-family: book antiqua,palatino;"><em><span style="font-size: 12pt;">Ps. 37:7</span></em></span></p>', '', 1, 1, 0, 10, '2010-12-31 19:14:27', 62, '', '2011-01-05 23:35:32', 62, 0, '0000-00-00 00:00:00', '2011-01-05 00:00:00', '2011-01-05 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 30, '', '', 0, 0, 'robots=\nauthor='),
(34, 'Precious Gems 6', 'precious-gems-6', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">6<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p style="padding-left: 30px;"><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em>With GOD all things are Possible.</em></span></span></p>\r\n<p style="padding-left: 180px;"><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em class="font-family-georgia">Matt. 19:26</em></span></span></p>\r\n<p> </p>', '', 1, 1, 0, 10, '2011-01-05 23:36:08', 62, '', '2011-01-06 00:05:10', 62, 0, '0000-00-00 00:00:00', '2011-01-06 00:00:00', '2011-01-06 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=en-GB\nkeyref=\nreadmore=', 3, 0, 29, '', '', 0, 0, 'robots=\nauthor='),
(35, 'Precious Gems  7', 'precious-gems-7', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="color: #0000ff;"><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">7<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><span style="font-family: book antiqua,palatino;"><em><span style="font-size: 12pt;">The LORD shall open uno thee his good treasure.</span></em></span></p>\r\n<p style="padding-left: 210px;"><em class="font-family-georgia"><span class="font-family-georgia">Deut 28:12</span></em></p>\r\n<p> </p>', '', 1, 1, 0, 10, '2011-01-05 23:42:07', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-07 00:00:00', '2011-01-07 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=en-GB\nkeyref=\nreadmore=', 1, 0, 28, '', '', 0, 0, 'robots=\nauthor='),
(36, 'Precious Gems  8', 'precious-gems-8', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="color: #0000ff;"><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">8<sup>th</sup> January 2011</span></span></strong></span></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">I will rejoice over them to do them good.</span></span></em></p>\r\n<p style="padding-left: 210px;"><em class="font-family-georgia">Jer. 32:41</em></p>', '', 1, 1, 0, 10, '2011-01-05 23:47:05', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-08 00:00:00', '2011-01-08 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=en-GB\nkeyref=\nreadmore=', 1, 0, 27, '', '', 0, 0, 'robots=\nauthor='),
(37, 'Precious Gems  9', 'precious-gems-9', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">9<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">I am thy shield, and thy exceeding great reward.</span></span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 240px;"><em>Gen. 15:1</em></p>', '', 1, 1, 0, 10, '2011-01-05 23:54:01', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-09 00:00:00', '2011-01-09 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 26, '', '', 0, 0, 'robots=\nauthor='),
(41, 'Precious Gems 12', 'precious-gems-12', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">12<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em>The LORD shall preserve thee from all Evil</em></span></span></p>\r\n<p class="font-family-georgia" style="padding-left: 240px;"><em>Ps.121:7</em></p>\r\n<p> </p>\r\n<p> </p>', '', 1, 1, 0, 10, '2011-01-11 21:48:53', 62, '', '2011-01-11 21:53:55', 62, 0, '0000-00-00 00:00:00', '2011-01-12 00:00:00', '2011-01-12 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 23, '', '', 0, 0, 'robots=\nauthor='),
(39, 'Preicous Gems   10', 'preicous-gems-10', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="font-family: book antiqua,palatino;"><strong><span style="font-size: 14pt;"><span style="color: #0000ff;">10<sup>th</sup> January 2011</span></span></strong></span></p>\r\n<p><span style="font-size: 12pt;"><span style="font-family: book antiqua,palatino;"><em>I am the LORD thy GOD which teacheth thee to profit.</em></span></span></p>\r\n<p class="font-family-georgia" style="padding-left: 240px;"><em>Isa. 48:17</em></p>', '', 1, 1, 0, 10, '2011-01-05 23:58:33', 62, '', '2011-01-06 00:09:16', 62, 0, '0000-00-00 00:00:00', '2011-01-10 00:00:00', '2011-01-10 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 3, 0, 25, '', '', 0, 0, 'robots=\nauthor='),
(42, 'Precious Gems  13', 'precious-gems-13', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="font-family: book antiqua,palatino;"><strong><span style="font-size: 14pt;"><span style="color: #0000ff;">13<sup>th</sup> January 2011</span></span></strong></span></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">Cause me to know the way wherein I should walk</span></span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 240px;"><em>Ps. 143.8</em></p>', '', 1, 1, 0, 10, '2011-01-12 23:09:15', 62, '', '2011-01-12 23:21:45', 62, 0, '0000-00-00 00:00:00', '2011-01-13 00:00:00', '2011-01-13 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 22, '', '', 0, 0, 'robots=\nauthor='),
(43, 'Precious Gems  14', 'precious-gems-14', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="color: #0000ff;"><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">14<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">Walk worthy of GOD, who hath called you</span></span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 210px;"><em>1 Thess. 2:12</em></p>\r\n<p> </p>', '', 1, 1, 0, 10, '2011-01-12 23:13:35', 62, '', '2011-01-12 23:21:27', 62, 0, '0000-00-00 00:00:00', '2011-01-14 00:00:00', '2011-01-14 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 21, '', '', 0, 1, 'robots=\nauthor='),
(44, 'Precious Gems 15', 'precious-gems-15', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="color: #0000ff;"><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">15<sup>th</sup> January 2011</span></span></strong></span></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">He is ale to succour them that are temped</span></span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 240px;"><em>Heb. 2:18</em></p>', '', 1, 1, 0, 10, '2011-01-12 23:18:01', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-15 00:00:00', '2011-01-15 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 20, '', '', 0, 0, 'robots=\nauthor='),
(45, 'Precious Gems  16', 'precious-gems-16', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">16<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-size: 12pt;"><span style="font-family: book antiqua,palatino;">The LORD Hath done great things for us.</span></span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 210px;"><em>Ps. 126:3</em></p>', '', 1, 1, 0, 10, '2011-01-15 19:32:32', 62, '', '2011-01-15 19:45:44', 62, 0, '0000-00-00 00:00:00', '2011-01-16 00:00:00', '2011-01-16 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 19, '', '', 0, 2, 'robots=\nauthor='),
(46, 'Precious Gems  17', 'precious-gems-17', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">17<sup>th</sup> January 2011</span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">In the fear of the LORD is strong confidence</span></span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 210px;"><em>Prov. 14:26</em></p>\r\n<p> </p>', '', 1, 1, 0, 10, '2011-01-15 19:40:30', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-17 00:00:00', '2011-01-17 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 18, '', '', 0, 0, 'robots=\nauthor='),
(47, 'Precious Gems  18', 'precious-gems-18', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">18<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">Think upon me, My GOD, for good.</span></span></em></p>\r\n<p style="padding-left: 210px;"><em class="font-family-georgia">Neh. 5:19</em></p>', '', 1, 1, 0, 10, '2011-01-15 19:45:54', 62, '', '2011-01-15 19:56:03', 62, 0, '0000-00-00 00:00:00', '2011-01-18 00:00:00', '2011-01-18 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 17, '', '', 0, 1, 'robots=\nauthor='),
(48, 'Precious Gems  19', 'precious-gems-19', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">19<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><span style="font-size: 12pt;"><em><span style="font-family: book antiqua,palatino;">He shall reward every man according to His works</span></em></span></p>\r\n<p style="padding-left: 210px;"><em class="font-family-georgia">Matt. 16:27</em></p>', '', 1, 1, 0, 10, '2011-01-15 19:50:41', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-19 00:00:00', '2011-01-19 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 16, '', '', 0, 1, 'robots=\nauthor='),
(49, 'Precious Gems  20', 'precious-gems-20', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">20<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">A righteous man regardeth the life of his beast</span></span></em></p>\r\n<p style="padding-left: 210px;"><em class="font-family-georgia">Prov. 12:10</em></p>', '', 1, 1, 0, 10, '2011-01-15 19:56:08', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-20 00:00:00', '2011-01-20 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 15, '', '', 0, 1, 'robots=\nauthor='),
(50, 'Precious Gems 21', 'precious-gems-21', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;"><strong>21<sup>st</sup> January 2011</strong></span></span></span></p>\r\n<p><em><span style="font-family: book antiqua,palatino;">He that cometh to me I will in no wise cast out</span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 210px;"><em>John 6:37</em></p>', '', 1, 1, 0, 10, '2011-01-20 23:12:13', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-21 00:00:00', '2011-01-21 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 14, '', '', 0, 2, 'robots=\nauthor='),
(51, 'Precious Gems 22', 'precious-gems-22', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">22<sup>nd</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;">Knock, and it shall be opened unto you.</span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 210px;"><em>Luke 11:9</em></p>', '', 1, 1, 0, 10, '2011-01-20 23:19:54', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-22 00:00:00', '2011-01-22 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 13, '', '', 0, 1, 'robots=\nauthor='),
(52, 'Precious Gems 23', 'precious-gems-23', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="color: #0000ff;"><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">23<sup>rd</sup> Janaury 2011</span></span></strong></span></p>\r\n<p><em><span style="font-family: book antiqua,palatino;">The&nbsp;blood of JESUS CHRIST ... Cleanseth us from&nbsp;all sin</span></em></p>\r\n<p style="padding-left: 240px;"><em class="font-family-georgia">1 John 1:7</em></p>', '', 1, 1, 0, 10, '2011-01-20 23:24:19', 62, '', '2011-01-20 23:33:52', 62, 0, '0000-00-00 00:00:00', '2011-01-23 00:00:00', '2011-01-23 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 12, '', '', 0, 2, 'robots=\nauthor='),
(53, 'Percious Gems 24', 'percious-gems-24', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">24<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;">Ye shall rejoice in all that ye put your hands unto</span></em></p>\r\n<p style="padding-left: 240px;"><em class="font-family-georgia">Deut. 12:7</em></p>', '', 1, 1, 0, 10, '2011-01-20 23:28:01', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-24 00:00:00', '2011-01-24 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 11, '', '', 0, 0, 'robots=\nauthor='),
(54, 'Precious Gems 25', 'precious-gems-25', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="color: #0000ff;"><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">25<sup>th</sup> January 2011</span></span></strong></span></p>\r\n<p><em><span style="font-family: book antiqua,palatino;">I will be sorry for my sin.</span></em></p>\r\n<p class="font-family-georgia" style="padding-left: 210px;"><em>Ps. 38:18</em></p>', '', 1, 1, 0, 10, '2011-01-20 23:34:01', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-25 00:00:00', '2011-01-25 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 10, '', '', 0, 0, 'robots=\nauthor='),
(55, 'Precious Gems 26', 'precious-gems-26', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">&nbsp;26<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p>&nbsp;<em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">The LORD is merciful and gracious, slow to anger, </span></span></em></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">and plenteous in mercy&nbsp; </span></span></em></p>\r\n<p style="padding-left: 240px;"><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"></span></span></em><em class="font-family-georgia">Ps. 103:8</em></p>', '', 1, 1, 0, 10, '2011-02-17 08:36:00', 62, '', '2011-02-17 08:45:32', 62, 0, '0000-00-00 00:00:00', '2011-01-26 00:00:00', '2011-01-26 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 2, 0, 9, '', '', 0, 0, 'robots=\nauthor='),
(56, 'Precious Gems  27', 'precious-gems-27', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">27<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">The counsel of the LORD standeth for ever, </span></span></em></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">the thoughts of his heart to all generations.<br /></span></span></em><br /><span class="font-family-georgia"><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<strong>&nbsp;&nbsp;&nbsp; Ps. 33:11</strong></em></span></p>', '', 1, 1, 0, 10, '2011-02-17 08:45:58', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-27 00:00:00', '2011-01-27 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 8, '', '', 0, 0, 'robots=\nauthor='),
(57, 'Precious Gems   28', 'precious-gems-28', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="color: #0000ff;"><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><strong>28<sup>th</sup> January 2011</strong></span></span></span></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">And if ye be Christ’s, then are ye Abraham’s seed, </span></span></em></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">and heirs according to the promise.<br /></span></span></em></p>\r\n&nbsp;&nbsp;<em class="font-family-georgia">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Gal. 3:29</em>', '', 1, 1, 0, 10, '2011-02-17 08:49:26', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-28 00:00:00', '2011-01-28 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 7, '', '', 0, 0, 'robots=\nauthor='),
(58, 'Precious Gems  29', 'precious-gems-29', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><span style="color: #0000ff;"><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;">29<sup>th</sup> January 2011</span></span></strong></span></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">For I know the thoughts that I think toward you, </span></span></em></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">saith the LORD, thoughts of peace, and not of evil, </span></span></em></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">to give you an expected end.<strong></strong></span></span></em><strong><br /><br /><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Jer. 29:11</em></strong></p>', '', 1, 1, 0, 10, '2011-02-17 08:53:04', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-29 00:00:00', '2011-01-29 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 6, '', '', 0, 0, 'robots=\nauthor='),
(59, 'Percious Gems 30', 'percious-gems-30', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">30<sup>th</sup> January 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">He maketh me to lie down in green pastures: </span></span></em></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">he leadeth me beside the still waters.<strong></strong></span></span></em><strong><br /><span class="font-family-georgia"><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Ps. 23:2</em></span></strong></p>', '', 1, 1, 0, 10, '2011-02-17 08:55:35', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-30 00:00:00', '2011-01-30 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 5, '', '', 0, 0, 'robots=\nauthor='),
(60, 'Precious Gems  31', 'precious-gems-31', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">31<sup>st</sup> January 2011</span></span></span></strong></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;"><em><span style="color: #000000;"><span style="font-size: 12pt;">The LORD bless thee, and keep thee</span></span></em></span></span></span></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;"><em><span style="color: #000000;"><span style="font-size: 12pt;"><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Num. 6:24</strong></span></span></em></span></span></span></p>', '', 1, 1, 0, 10, '2011-02-17 08:58:01', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-01-31 00:00:00', '2011-01-31 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 4, '', '', 0, 0, 'robots=\nauthor='),
(61, 'Precious Gems  32', 'precious-gems-32', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">1<sup>st</sup> February 2011</span></span></span></strong></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em>And ye shall seek me, and find me, when ye </em></span></span></p>\r\n<p><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;"><em>shall search for me with all your heart.<strong></strong></em></span></span><strong><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <em class="font-family-georgia">Jer. 29:13</em></strong></p>', '', 1, 1, 0, 10, '2011-02-17 09:00:50', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-02-01 00:00:00', '2011-02-01 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 3, '', '', 0, 0, 'robots=\nauthor='),
(62, 'Precious Gems  33', 'precious-gems-33', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">2<sup>nd</sup> February 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;">Casting all your care upon him; for he careth for you.<strong></strong><br /><br /></span></em><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="font-family-georgia"><em>1 Pet. 5:7</em></span></strong></p>', '', 1, 1, 0, 10, '2011-02-17 09:03:18', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-02-02 00:00:00', '2011-02-02 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 2, '', '', 0, 0, 'robots=\nauthor='),
(63, 'Precious Gems  34', 'precious-gems-34', '', '<p><img src="images/stories/preciousgems365 4.jpg" /></p>\r\n<p><strong><span style="font-family: book antiqua,palatino;"><span style="font-size: 14pt;"><span style="color: #0000ff;">3<sup>rd</sup> February 2011</span></span></span></strong></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">For the LORD God is a sun and shield: the LORD will </span></span></em></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">give grace and glory: No good thing will he withhold </span></span></em></p>\r\n<p><em><span style="font-family: book antiqua,palatino;"><span style="font-size: 12pt;">from them that walk uprightly.<strong></strong></span></span></em><strong><br /><br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <span class="font-family-georgia"><em>Ps. 84:11</em></span></strong></p>', '', 1, 1, 0, 10, '2011-02-17 09:11:23', 62, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2011-02-03 00:00:00', '2011-02-03 23:59:59', '', '', 'show_title=0\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_vote=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nlanguage=\nkeyref=\nreadmore=', 1, 0, 1, '', '', 0, 0, 'robots=\nauthor=');

-- --------------------------------------------------------

--
-- Table structure for table `jml_content_frontpage`
--

DROP TABLE IF EXISTS `jml_content_frontpage`;
CREATE TABLE IF NOT EXISTS `jml_content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jml_content_frontpage`
--

INSERT INTO `jml_content_frontpage` (`content_id`, `ordering`) VALUES
(29, 35),
(30, 34),
(31, 33),
(33, 31),
(32, 32),
(34, 30),
(35, 29),
(36, 28),
(37, 27),
(20, 26),
(39, 25),
(40, 24),
(41, 23),
(42, 22),
(43, 21),
(44, 20),
(45, 19),
(46, 18),
(47, 17),
(48, 16),
(49, 15),
(50, 14),
(51, 13),
(52, 12),
(53, 11),
(54, 10),
(55, 9),
(56, 8),
(57, 7),
(58, 6),
(59, 5),
(60, 4),
(61, 3),
(62, 2),
(63, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jml_content_rating`
--

DROP TABLE IF EXISTS `jml_content_rating`;
CREATE TABLE IF NOT EXISTS `jml_content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(11) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(11) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_core_acl_aro`
--

DROP TABLE IF EXISTS `jml_core_acl_aro`;
CREATE TABLE IF NOT EXISTS `jml_core_acl_aro` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_value` varchar(240) NOT NULL DEFAULT '0',
  `value` varchar(240) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jml_section_value_value_aro` (`section_value`(100),`value`(100)),
  KEY `jml_gacl_hidden_aro` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `jml_core_acl_aro`
--

INSERT INTO `jml_core_acl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', '62', 0, 'Administrator', 0),
(12, 'users', '64', 0, 'Olos Irenoa', 0),
(14, 'users', '66', 0, 'Timothy Okusanya', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jml_core_acl_aro_groups`
--

DROP TABLE IF EXISTS `jml_core_acl_aro_groups`;
CREATE TABLE IF NOT EXISTS `jml_core_acl_aro_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `value` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `jml_gacl_parent_id_aro_groups` (`parent_id`),
  KEY `jml_gacl_lft_rgt_aro_groups` (`lft`,`rgt`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `jml_core_acl_aro_groups`
--

INSERT INTO `jml_core_acl_aro_groups` (`id`, `parent_id`, `name`, `lft`, `rgt`, `value`) VALUES
(17, 0, 'ROOT', 1, 22, 'ROOT'),
(28, 17, 'USERS', 2, 21, 'USERS'),
(29, 28, 'Public Frontend', 3, 12, 'Public Frontend'),
(18, 29, 'Registered', 4, 11, 'Registered'),
(19, 18, 'Author', 5, 10, 'Author'),
(20, 19, 'Editor', 6, 9, 'Editor'),
(21, 20, 'Publisher', 7, 8, 'Publisher'),
(30, 28, 'Public Backend', 13, 20, 'Public Backend'),
(23, 30, 'Manager', 14, 19, 'Manager'),
(24, 23, 'Administrator', 15, 18, 'Administrator'),
(25, 24, 'Super Administrator', 16, 17, 'Super Administrator');

-- --------------------------------------------------------

--
-- Table structure for table `jml_core_acl_aro_map`
--

DROP TABLE IF EXISTS `jml_core_acl_aro_map`;
CREATE TABLE IF NOT EXISTS `jml_core_acl_aro_map` (
  `acl_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(230) NOT NULL DEFAULT '0',
  `value` varchar(100) NOT NULL,
  PRIMARY KEY (`acl_id`,`section_value`,`value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_core_acl_aro_sections`
--

DROP TABLE IF EXISTS `jml_core_acl_aro_sections`;
CREATE TABLE IF NOT EXISTS `jml_core_acl_aro_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(230) NOT NULL DEFAULT '',
  `order_value` int(11) NOT NULL DEFAULT '0',
  `name` varchar(230) NOT NULL DEFAULT '',
  `hidden` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `jml_gacl_value_aro_sections` (`value`),
  KEY `jml_gacl_hidden_aro_sections` (`hidden`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `jml_core_acl_aro_sections`
--

INSERT INTO `jml_core_acl_aro_sections` (`id`, `value`, `order_value`, `name`, `hidden`) VALUES
(10, 'users', 1, 'Users', 0);

-- --------------------------------------------------------

--
-- Table structure for table `jml_core_acl_groups_aro_map`
--

DROP TABLE IF EXISTS `jml_core_acl_groups_aro_map`;
CREATE TABLE IF NOT EXISTS `jml_core_acl_groups_aro_map` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `section_value` varchar(240) NOT NULL DEFAULT '',
  `aro_id` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `group_id_aro_id_groups_aro_map` (`group_id`,`section_value`,`aro_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jml_core_acl_groups_aro_map`
--

INSERT INTO `jml_core_acl_groups_aro_map` (`group_id`, `section_value`, `aro_id`) VALUES
(23, '', 14),
(25, '', 10),
(25, '', 12);

-- --------------------------------------------------------

--
-- Table structure for table `jml_core_log_items`
--

DROP TABLE IF EXISTS `jml_core_log_items`;
CREATE TABLE IF NOT EXISTS `jml_core_log_items` (
  `time_stamp` date NOT NULL DEFAULT '0000-00-00',
  `item_table` varchar(50) NOT NULL DEFAULT '',
  `item_id` int(11) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_core_log_searches`
--

DROP TABLE IF EXISTS `jml_core_log_searches`;
CREATE TABLE IF NOT EXISTS `jml_core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(11) unsigned NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_djimageslider`
--

DROP TABLE IF EXISTS `jml_djimageslider`;
CREATE TABLE IF NOT EXISTS `jml_djimageslider` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`,`archived`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jml_djimageslider`
--

INSERT INTO `jml_djimageslider` (`id`, `catid`, `sid`, `title`, `alias`, `image`, `description`, `published`, `checked_out`, `checked_out_time`, `ordering`, `archived`, `approved`, `params`) VALUES
(1, 9, 0, 'Youth Camp Reports', 'youth-camp-reports', 'images/stories/youth_camp.jpg', 'Youth Camp is a time for refreshing and uplifting. Participants have received diverse blessings and outpourings.', 1, 0, '0000-00-00 00:00:00', 1, 0, 1, 'targetswitch=2\ntarget=2\ntargetart=6\ntargeturl=#\n\n'),
(2, 9, 0, 'Testimony', 'testimony', 'images/stories/testimony.jpg', '', 1, 0, '0000-00-00 00:00:00', 2, 0, 1, 'targetswitch=0\ntarget=6\ntargetart=22\ntargeturl=\n\n'),
(3, 9, 0, 'hjhghjghj', 'hjhghjghj', 'images/stories/j0433335.jpg', 'Lorem ipsum del irium. Lorem ipsum del irium.  Lorem ipsum del irium.  Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium. Lorem ipsum del irium. ', 0, 0, '0000-00-00 00:00:00', 3, 0, 1, 'targetswitch=1\ntarget=\ntargetart=22\ntargeturl=#\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `jml_eventlist_categories`
--

DROP TABLE IF EXISTS `jml_eventlist_categories`;
CREATE TABLE IF NOT EXISTS `jml_eventlist_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) unsigned NOT NULL DEFAULT '0',
  `catname` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(100) NOT NULL DEFAULT '',
  `catdescription` mediumtext NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `image` varchar(100) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(11) unsigned NOT NULL DEFAULT '0',
  `groupid` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jml_eventlist_categories`
--

INSERT INTO `jml_eventlist_categories` (`id`, `parent_id`, `catname`, `alias`, `catdescription`, `meta_keywords`, `meta_description`, `image`, `published`, `checked_out`, `checked_out_time`, `access`, `groupid`, `ordering`) VALUES
(1, 0, 'Youth Leaders'' Events', 'youth-leaders-events', '', '', 'Youth Leaders events at the Apostolic Faith Mission UK', '', 1, 0, '0000-00-00 00:00:00', 0, 1, 1),
(2, 0, 'General Youth Events', 'general-youth-events', '', '', 'General Youth Events at the Apostolic Faith Mission UK', '', 1, 0, '0000-00-00 00:00:00', 0, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `jml_eventlist_events`
--

DROP TABLE IF EXISTS `jml_eventlist_events`;
CREATE TABLE IF NOT EXISTS `jml_eventlist_events` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `locid` int(11) unsigned NOT NULL DEFAULT '0',
  `catsid` int(11) unsigned NOT NULL DEFAULT '0',
  `dates` date NOT NULL DEFAULT '0000-00-00',
  `enddates` date DEFAULT NULL,
  `times` time DEFAULT NULL,
  `endtimes` time DEFAULT NULL,
  `title` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(100) NOT NULL DEFAULT '',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `modified` datetime NOT NULL,
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `author_ip` varchar(15) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `datdescription` mediumtext NOT NULL,
  `meta_keywords` varchar(200) NOT NULL DEFAULT '',
  `meta_description` varchar(255) NOT NULL DEFAULT '',
  `recurrence_number` int(2) NOT NULL DEFAULT '0',
  `recurrence_type` int(2) NOT NULL DEFAULT '0',
  `recurrence_counter` date NOT NULL DEFAULT '0000-00-00',
  `datimage` varchar(100) NOT NULL DEFAULT '',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `registra` tinyint(1) NOT NULL DEFAULT '0',
  `unregistra` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `jml_eventlist_events`
--

INSERT INTO `jml_eventlist_events` (`id`, `locid`, `catsid`, `dates`, `enddates`, `times`, `endtimes`, `title`, `alias`, `created_by`, `modified`, `modified_by`, `author_ip`, `created`, `datdescription`, `meta_keywords`, `meta_description`, `recurrence_number`, `recurrence_type`, `recurrence_counter`, `datimage`, `checked_out`, `checked_out_time`, `registra`, `unregistra`, `published`) VALUES
(1, 1, 2, '2010-09-12', NULL, '11:00:00', '13:00:00', 'Youth Service - AM 5', 'youth-service-am-5', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:36:20', '<p>Leavers/Freshers Day organised by the Youths of the Apostolic Faith Mission UK</p>', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(2, 1, 2, '2010-09-12', NULL, '14:30:00', '17:00:00', 'Youth Activities', 'youth-activities', 62, '2010-09-06 09:43:23', 62, '92.15.129.166', '2010-09-06 09:40:57', '<div class="description event_desc">\r\n<p>Leavers/Freshers Day organised by the Youths of the Apostolic  Faith Mission UK</p>\r\n</div>', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(3, 3, 2, '2010-09-19', NULL, '15:00:00', '16:30:00', 'YCC Music Workshop', 'ycc-music-workshop', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:46:09', '<p>All youths are welcome to join the YCC music workshop</p>', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(4, 1, 2, '2010-09-26', NULL, '17:00:00', '18:00:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:47:16', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(5, 3, 2, '2010-10-10', NULL, '15:00:00', '16:30:00', 'YCC Music Workshop', 'ycc-music-workshop', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:48:11', '<p>All youths are welcome to join the YCC music workshop</p>', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(6, 1, 2, '2010-10-16', NULL, '13:00:00', '16:00:00', 'Diversity', 'diversity', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:50:05', '<p>Celebrate with us this october, diversity month</p>', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(7, 1, 2, '2010-10-17', NULL, '15:00:00', '16:30:00', 'YCC Grace', 'ycc-grace', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:50:53', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(8, 3, 2, '2010-10-24', NULL, '15:00:00', '16:30:00', 'YCC Music Workshop', 'ycc-music-workshop', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:53:35', '<div class="description event_desc">\r\n<p>All youths are welcome to join the YCC music workshop</p>\r\n</div>', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(9, 3, 2, '2010-10-31', NULL, '17:00:00', '18:00:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:55:15', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(10, 1, 2, '2010-11-14', NULL, '15:30:00', '16:30:00', 'YCC Prayer/Online', 'ycc-prayeronline', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:57:05', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(11, 1, 2, '2010-11-20', NULL, '13:00:00', '16:00:00', 'Reflection', 'reflection', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:58:18', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(12, 3, 2, '2010-11-21', NULL, '15:00:00', '16:30:00', 'YCC Music Workshop', 'ycc-music-workshop', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 09:59:40', '<div class="description event_desc">\r\n<p>All youths are welcome to join the YCC music workshop</p>\r\n</div>', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(13, 3, 2, '2010-11-28', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(14, 3, 2, '2010-12-05', NULL, '15:00:00', '16:30:00', 'YCC Music Workshop (TBD)', 'ycc-music-workshop', 62, '2010-09-06 10:08:17', 62, '92.15.129.166', '2010-09-06 10:07:14', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(15, 1, 2, '2010-12-12', NULL, '15:30:00', '16:30:00', 'YCC FILM', 'ycc-film', 62, '2010-09-06 10:49:14', 62, '92.15.129.166', '2010-09-06 10:41:59', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(16, 3, 2, '2010-12-19', NULL, '15:00:00', '16:30:00', 'YCC Music Workshop', 'ycc-music-workshop', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:50:30', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(17, 0, 1, '2010-09-29', NULL, NULL, NULL, 'Youth Leader''s Conference', 'youth-leaders-conference', 62, '2010-09-24 21:08:26', 62, '87.194.42.69', '2010-09-24 21:07:18', 'There is a youth leaders conference coming up. Date not announced yet.', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 0, '0000-00-00', '', 0, '0000-00-00 00:00:00', 1, 1, 0),
(18, 3, 2, '2010-12-26', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(19, 3, 2, '2011-01-30', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(20, 3, 2, '2011-02-27', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(21, 3, 2, '2011-03-27', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(22, 3, 2, '2011-04-24', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(23, 3, 2, '2011-05-29', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(24, 3, 2, '2011-06-26', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(25, 3, 2, '2011-07-31', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(26, 3, 2, '2011-08-28', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, -1),
(27, 3, 2, '2011-09-25', NULL, '15:00:00', '16:30:00', 'Youth Service', 'youth-service', 62, '0000-00-00 00:00:00', 0, '92.15.129.166', '2010-09-06 10:01:54', '', '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 5, 10, '0000-00-00', '', 0, '0000-00-00 00:00:00', 0, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jml_eventlist_groupmembers`
--

DROP TABLE IF EXISTS `jml_eventlist_groupmembers`;
CREATE TABLE IF NOT EXISTS `jml_eventlist_groupmembers` (
  `group_id` int(11) NOT NULL DEFAULT '0',
  `member` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jml_eventlist_groupmembers`
--

INSERT INTO `jml_eventlist_groupmembers` (`group_id`, `member`) VALUES
(1, 64),
(2, 64),
(2, 66);

-- --------------------------------------------------------

--
-- Table structure for table `jml_eventlist_groups`
--

DROP TABLE IF EXISTS `jml_eventlist_groups`;
CREATE TABLE IF NOT EXISTS `jml_eventlist_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jml_eventlist_groups`
--

INSERT INTO `jml_eventlist_groups` (`id`, `name`, `description`, `checked_out`, `checked_out_time`) VALUES
(1, 'Youth Leaders', 'This group of youth leaders with Sis. Olos as the sole person who can update and maintain the events linked to this group.', 0, '0000-00-00 00:00:00'),
(2, 'General Youth Events', 'The following people can update this group with events:\r\n\r\nSis. Olos,\r\nBro. Timothy', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jml_eventlist_register`
--

DROP TABLE IF EXISTS `jml_eventlist_register`;
CREATE TABLE IF NOT EXISTS `jml_eventlist_register` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `event` int(11) NOT NULL DEFAULT '0',
  `uid` int(11) NOT NULL DEFAULT '0',
  `uregdate` varchar(50) NOT NULL DEFAULT '',
  `uip` varchar(15) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_eventlist_settings`
--

DROP TABLE IF EXISTS `jml_eventlist_settings`;
CREATE TABLE IF NOT EXISTS `jml_eventlist_settings` (
  `id` int(11) NOT NULL,
  `oldevent` tinyint(4) NOT NULL,
  `minus` tinyint(4) NOT NULL,
  `showtime` tinyint(4) NOT NULL,
  `showtitle` tinyint(4) NOT NULL,
  `showlocate` tinyint(4) NOT NULL,
  `showcity` tinyint(4) NOT NULL,
  `showmapserv` tinyint(4) NOT NULL,
  `map24id` varchar(20) NOT NULL,
  `gmapkey` varchar(255) NOT NULL,
  `tablewidth` varchar(20) NOT NULL,
  `datewidth` varchar(20) NOT NULL,
  `titlewidth` varchar(20) NOT NULL,
  `locationwidth` varchar(20) NOT NULL,
  `citywidth` varchar(20) NOT NULL,
  `datename` varchar(100) NOT NULL,
  `titlename` varchar(100) NOT NULL,
  `locationname` varchar(100) NOT NULL,
  `cityname` varchar(100) NOT NULL,
  `formatdate` varchar(100) NOT NULL,
  `formattime` varchar(100) NOT NULL,
  `timename` varchar(50) NOT NULL,
  `showdetails` tinyint(4) NOT NULL,
  `showtimedetails` tinyint(4) NOT NULL,
  `showevdescription` tinyint(4) NOT NULL,
  `showdetailstitle` tinyint(4) NOT NULL,
  `showdetailsadress` tinyint(4) NOT NULL,
  `showlocdescription` tinyint(4) NOT NULL,
  `showlinkvenue` tinyint(4) NOT NULL,
  `showdetlinkvenue` tinyint(4) NOT NULL,
  `delivereventsyes` tinyint(4) NOT NULL,
  `mailinform` tinyint(4) NOT NULL,
  `mailinformrec` varchar(150) NOT NULL,
  `mailinformuser` tinyint(4) NOT NULL,
  `datdesclimit` varchar(15) NOT NULL,
  `autopubl` tinyint(4) NOT NULL,
  `deliverlocsyes` tinyint(4) NOT NULL,
  `autopublocate` tinyint(4) NOT NULL,
  `showcat` tinyint(4) NOT NULL,
  `catfrowidth` varchar(20) NOT NULL,
  `catfroname` varchar(100) NOT NULL,
  `evdelrec` tinyint(4) NOT NULL,
  `evpubrec` tinyint(4) NOT NULL,
  `locdelrec` tinyint(4) NOT NULL,
  `locpubrec` tinyint(4) NOT NULL,
  `sizelimit` varchar(20) NOT NULL,
  `imagehight` varchar(20) NOT NULL,
  `imagewidth` varchar(20) NOT NULL,
  `gddisabled` tinyint(4) NOT NULL,
  `imageenabled` tinyint(4) NOT NULL,
  `comunsolution` tinyint(4) NOT NULL,
  `comunoption` tinyint(4) NOT NULL,
  `catlinklist` tinyint(4) NOT NULL,
  `showfroregistra` tinyint(4) NOT NULL,
  `showfrounregistra` tinyint(4) NOT NULL,
  `eventedit` tinyint(4) NOT NULL,
  `eventeditrec` tinyint(4) NOT NULL,
  `eventowner` tinyint(4) NOT NULL,
  `venueedit` tinyint(4) NOT NULL,
  `venueeditrec` tinyint(4) NOT NULL,
  `venueowner` tinyint(4) NOT NULL,
  `lightbox` tinyint(4) NOT NULL,
  `meta_keywords` varchar(255) NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `showstate` tinyint(4) NOT NULL,
  `statename` varchar(100) NOT NULL,
  `statewidth` varchar(20) NOT NULL,
  `regname` tinyint(4) NOT NULL,
  `storeip` tinyint(4) NOT NULL,
  `commentsystem` tinyint(4) NOT NULL,
  `lastupdate` varchar(20) NOT NULL DEFAULT '',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jml_eventlist_settings`
--

INSERT INTO `jml_eventlist_settings` (`id`, `oldevent`, `minus`, `showtime`, `showtitle`, `showlocate`, `showcity`, `showmapserv`, `map24id`, `gmapkey`, `tablewidth`, `datewidth`, `titlewidth`, `locationwidth`, `citywidth`, `datename`, `titlename`, `locationname`, `cityname`, `formatdate`, `formattime`, `timename`, `showdetails`, `showtimedetails`, `showevdescription`, `showdetailstitle`, `showdetailsadress`, `showlocdescription`, `showlinkvenue`, `showdetlinkvenue`, `delivereventsyes`, `mailinform`, `mailinformrec`, `mailinformuser`, `datdesclimit`, `autopubl`, `deliverlocsyes`, `autopublocate`, `showcat`, `catfrowidth`, `catfroname`, `evdelrec`, `evpubrec`, `locdelrec`, `locpubrec`, `sizelimit`, `imagehight`, `imagewidth`, `gddisabled`, `imageenabled`, `comunsolution`, `comunoption`, `catlinklist`, `showfroregistra`, `showfrounregistra`, `eventedit`, `eventeditrec`, `eventowner`, `venueedit`, `venueeditrec`, `venueowner`, `lightbox`, `meta_keywords`, `meta_description`, `showstate`, `statename`, `statewidth`, `regname`, `storeip`, `commentsystem`, `lastupdate`, `checked_out`, `checked_out_time`) VALUES
(1, 2, 1, 0, 1, 1, 1, 2, '', 'ABQIAAAApgEJ2mHRnt5idPG0uPRXjBTVwqs1qr60oErxUYSUJK3cvP1f3BSS-qjpS1UccWbgbJfcmm-4TEyT7w', '100%', '15%', '25%', '20%', '20%', 'Date', 'Title', 'Venue', 'City', '%d.%m.%Y', '%H.%M', 'h', 1, 1, 1, 1, 1, 1, 1, 2, 30, 3, 'bright_shiningstar@msn.com', 0, '1000', 24, 23, 24, 1, '20%', 'Type', 1, 1, 1, 1, '100', '100', '100', 0, 1, 0, 0, 1, 2, 2, 24, 1, 0, 24, 1, 0, 0, '[title], [a_name], [catsid], [times]', 'The event titled [title] starts on [dates]!', 0, 'State', '0', 0, 1, 0, '1314813802', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jml_eventlist_venues`
--

DROP TABLE IF EXISTS `jml_eventlist_venues`;
CREATE TABLE IF NOT EXISTS `jml_eventlist_venues` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `venue` varchar(50) NOT NULL DEFAULT '',
  `alias` varchar(100) NOT NULL DEFAULT '',
  `url` varchar(200) NOT NULL DEFAULT '',
  `street` varchar(50) DEFAULT NULL,
  `plz` varchar(20) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state` varchar(50) DEFAULT NULL,
  `country` varchar(2) DEFAULT NULL,
  `locdescription` mediumtext NOT NULL,
  `meta_keywords` text NOT NULL,
  `meta_description` text NOT NULL,
  `locimage` varchar(100) NOT NULL DEFAULT '',
  `map` tinyint(4) NOT NULL DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL DEFAULT '0',
  `author_ip` varchar(15) NOT NULL DEFAULT '',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jml_eventlist_venues`
--

INSERT INTO `jml_eventlist_venues` (`id`, `venue`, `alias`, `url`, `street`, `plz`, `city`, `state`, `country`, `locdescription`, `meta_keywords`, `meta_description`, `locimage`, `map`, `created_by`, `author_ip`, `created`, `modified`, `modified_by`, `published`, `checked_out`, `checked_out_time`, `ordering`) VALUES
(1, 'Apostolic Faith Mission, Peckham, London', 'apostolic-faith-mission-peckham-london', 'http://www.apostolicfaith.org.uk', '95 Fenham Road, Peckham', 'SE15 1AE', 'London', 'London', 'UK', '', 'apostolic faith church uk, apostolic faith church london, af mission london, apostolic faith church peckham, ', 'The Apostolic Faith Mission UK', '', 0, 62, '92.15.129.166', '2010-09-06 09:28:47', '0000-00-00 00:00:00', 0, 1, 0, '0000-00-00 00:00:00', 1),
(2, '19 Penhill Road, Bexley, London', '19-penhill-road-bexley-london', 'http://www.apostolicfaith.org.uk', '19 Penhill Road, Bexley, Kent', 'DA5 3', 'Kent', 'London', 'UK', '', 'apostolic faith church Bexley, Apostolic Faith Mission UK, Apostolic Faith Mission, London', '', '', 0, 62, '92.15.129.166', '2010-09-06 09:33:09', '0000-00-00 00:00:00', 0, 1, 0, '0000-00-00 00:00:00', 2),
(3, 'All branches in London', 'all-branches-in-london', '', '', '', '', '', '', '', '', '', '', 0, 62, '92.15.129.166', '2010-09-06 09:44:43', '0000-00-00 00:00:00', 0, 1, 0, '0000-00-00 00:00:00', 3);

-- --------------------------------------------------------

--
-- Table structure for table `jml_groups`
--

DROP TABLE IF EXISTS `jml_groups`;
CREATE TABLE IF NOT EXISTS `jml_groups` (
  `id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `name` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jml_groups`
--

INSERT INTO `jml_groups` (`id`, `name`) VALUES
(0, 'Public'),
(1, 'Registered'),
(2, 'Special');

-- --------------------------------------------------------

--
-- Table structure for table `jml_jce_extensions`
--

DROP TABLE IF EXISTS `jml_jce_extensions`;
CREATE TABLE IF NOT EXISTS `jml_jce_extensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `folder` varchar(255) NOT NULL,
  `published` tinyint(3) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jml_jce_extensions`
--

INSERT INTO `jml_jce_extensions` (`id`, `pid`, `name`, `extension`, `folder`, `published`) VALUES
(1, 54, 'Joomla Links for Advanced Link', 'joomlalinks', 'links', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jml_jce_groups`
--

DROP TABLE IF EXISTS `jml_jce_groups`;
CREATE TABLE IF NOT EXISTS `jml_jce_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `users` text NOT NULL,
  `types` varchar(255) NOT NULL,
  `components` text NOT NULL,
  `rows` text NOT NULL,
  `plugins` varchar(255) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `ordering` int(11) NOT NULL,
  `checked_out` tinyint(3) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jml_jce_groups`
--

INSERT INTO `jml_jce_groups` (`id`, `name`, `description`, `users`, `types`, `components`, `rows`, `plugins`, `published`, `ordering`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Default', 'Default group for all users with edit access', '', '19,20,21,23,24,25', '', '6,7,8,9,10,11,12,13,14,15,16,17,18,19;20,21,22,23,24,25,26,27,28,30,31,32,33,36;37,38,39,40,41,42,43,44,45,46,47,48;49,50,51,52,53,54,55,57,58', '1,2,3,4,5,6,20,21,37,38,39,40,41,42,49,50,51,52,53,54,55,57,58', 1, 1, 0, '0000-00-00 00:00:00', ''),
(2, 'Front End', 'Sample Group for Authors, Editors, Publishers', '', '19,20,21', '', '6,7,8,9,10,13,14,15,16,17,18,19,27,28;20,21,25,26,30,31,32,36,43,44,45,47,48,50,51;24,33,39,40,42,46,49,52,53,54,55,57,58', '6,20,21,50,51,1,3,5,39,40,42,49,52,53,54,55,57,58', 0, 2, 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_jce_plugins`
--

DROP TABLE IF EXISTS `jml_jce_plugins`;
CREATE TABLE IF NOT EXISTS `jml_jce_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `layout` varchar(255) NOT NULL,
  `row` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `published` tinyint(3) NOT NULL,
  `editable` tinyint(3) NOT NULL,
  `iscore` tinyint(3) NOT NULL,
  `elements` varchar(255) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `plugin` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

--
-- Dumping data for table `jml_jce_plugins`
--

INSERT INTO `jml_jce_plugins` (`id`, `title`, `name`, `type`, `icon`, `layout`, `row`, `ordering`, `published`, `editable`, `iscore`, `elements`, `checked_out`, `checked_out_time`) VALUES
(1, 'Context Menu', 'contextmenu', 'plugin', '', '', 0, 0, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(2, 'File Browser', 'browser', 'plugin', '', '', 0, 0, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(3, 'Inline Popups', 'inlinepopups', 'plugin', '', '', 0, 0, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(4, 'Media Support', 'media', 'plugin', '', '', 0, 0, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(5, 'Safari Browser Support', 'safari', 'plugin', '', '', 0, 0, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(6, 'Help', 'help', 'plugin', 'help', 'help', 1, 1, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(7, 'New Document', 'newdocument', 'command', 'newdocument', 'newdocument', 1, 2, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(8, 'Bold', 'bold', 'command', 'bold', 'bold', 1, 3, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(9, 'Italic', 'italic', 'command', 'italic', 'italic', 1, 4, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(10, 'Underline', 'underline', 'command', 'underline', 'underline', 1, 5, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(11, 'Font Select', 'fontselect', 'command', 'fontselect', 'fontselect', 1, 6, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(12, 'Font Size Select', 'fontsizeselect', 'command', 'fontsizeselect', 'fontsizeselect', 1, 7, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(13, 'Style Select', 'styleselect', 'command', 'styleselect', 'styleselect', 1, 8, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(14, 'StrikeThrough', 'strikethrough', 'command', 'strikethrough', 'strikethrough', 1, 9, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(15, 'Justify Full', 'full', 'command', 'justifyfull', 'justifyfull', 1, 10, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(16, 'Justify Center', 'center', 'command', 'justifycenter', 'justifycenter', 1, 11, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(17, 'Justify Left', 'left', 'command', 'justifyleft', 'justifyleft', 1, 12, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(18, 'Justify Right', 'right', 'command', 'justifyright', 'justifyright', 1, 13, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(19, 'Format Select', 'formatselect', 'command', 'formatselect', 'formatselect', 1, 14, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(20, 'Paste', 'paste', 'plugin', 'pasteword,pastetext', 'paste', 2, 1, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(21, 'Search Replace', 'searchreplace', 'plugin', 'search,replace', 'searchreplace', 2, 2, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(22, 'Font ForeColour', 'forecolor', 'command', 'forecolor', 'forecolor', 2, 3, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(23, 'Font BackColour', 'backcolor', 'command', 'backcolor', 'backcolor', 2, 4, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(24, 'Unlink', 'unlink', 'command', 'unlink', 'unlink', 2, 5, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(25, 'Indent', 'indent', 'command', 'indent', 'indent', 2, 6, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(26, 'Outdent', 'outdent', 'command', 'outdent', 'outdent', 2, 7, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(27, 'Undo', 'undo', 'command', 'undo', 'undo', 2, 8, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(28, 'Redo', 'redo', 'command', 'redo', 'redo', 2, 9, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(29, 'HTML', 'html', 'command', 'code', 'code', 2, 10, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(30, 'Numbered List', 'numlist', 'command', 'numlist', 'numlist', 2, 11, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(31, 'Bullet List', 'bullist', 'command', 'bullist', 'bullist', 2, 12, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(32, 'Clipboard Actions', 'clipboard', 'command', 'cut,copy,paste', 'clipboard', 2, 13, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(33, 'Anchor', 'anchor', 'command', 'anchor', 'anchor', 2, 14, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(34, 'Image', 'image', 'command', 'image', 'image', 2, 15, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(35, 'Link', 'link', 'command', 'link', 'link', 2, 16, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(36, 'Code Cleanup', 'cleanup', 'command', 'cleanup', 'cleanup', 2, 17, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(37, 'Directionality', 'directionality', 'plugin', 'ltr,rtl', 'directionality', 3, 1, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(38, 'Emotions', 'emotions', 'plugin', 'emotions', 'emotions', 3, 2, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(39, 'Fullscreen', 'fullscreen', 'plugin', 'fullscreen', 'fullscreen', 3, 3, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(40, 'Preview', 'preview', 'plugin', 'preview', 'preview', 3, 4, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(41, 'Tables', 'table', 'plugin', 'tablecontrols', 'buttons', 3, 5, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(42, 'Print', 'print', 'plugin', 'print', 'print', 3, 6, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(43, 'Horizontal Rule', 'hr', 'command', 'hr', 'hr', 3, 7, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(44, 'Subscript', 'sub', 'command', 'sub', 'sub', 3, 8, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(45, 'Superscript', 'sup', 'command', 'sup', 'sup', 3, 9, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(46, 'Visual Aid', 'visualaid', 'command', 'visualaid', 'visualaid', 3, 10, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(47, 'Character Map', 'charmap', 'command', 'charmap', 'charmap', 3, 11, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(48, 'Remove Format', 'removeformat', 'command', 'removeformat', 'removeformat', 3, 12, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(49, 'Styles', 'style', 'plugin', 'styleprops', 'style', 4, 1, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(50, 'Non-Breaking', 'nonbreaking', 'plugin', 'nonbreaking', 'nonbreaking', 4, 2, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(51, 'Visual Characters', 'visualchars', 'plugin', 'visualchars', 'visualchars', 4, 3, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(52, 'XHTML Xtras', 'xhtmlxtras', 'plugin', 'cite,abbr,acronym,del,ins,attribs', 'xhtmlxtras', 4, 4, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(53, 'Image Manager', 'imgmanager', 'plugin', 'imgmanager', 'imgmanager', 4, 5, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(54, 'Advanced Link', 'advlink', 'plugin', 'advlink', 'advlink', 4, 6, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(55, 'Spell Checker', 'spellchecker', 'plugin', 'spellchecker', 'spellchecker', 4, 7, 1, 1, 1, '', 0, '0000-00-00 00:00:00'),
(56, 'Layers', 'layer', 'plugin', 'insertlayer,moveforward,movebackward,absolute', 'layer', 4, 8, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(57, 'Advanced Code Editor', 'advcode', 'plugin', 'advcode', 'advcode', 4, 9, 1, 0, 1, '', 0, '0000-00-00 00:00:00'),
(58, 'Article Breaks', 'article', 'plugin', 'readmore,pagebreak', 'article', 4, 10, 1, 0, 1, '', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jml_menu`
--

DROP TABLE IF EXISTS `jml_menu`;
CREATE TABLE IF NOT EXISTS `jml_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text,
  `type` varchar(50) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `parent` int(11) unsigned NOT NULL DEFAULT '0',
  `componentid` int(11) unsigned NOT NULL DEFAULT '0',
  `sublevel` int(11) DEFAULT '0',
  `ordering` int(11) DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pollid` int(11) NOT NULL DEFAULT '0',
  `browserNav` tinyint(4) DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `utaccess` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `lft` int(11) unsigned NOT NULL DEFAULT '0',
  `rgt` int(11) unsigned NOT NULL DEFAULT '0',
  `home` int(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `componentid` (`componentid`,`menutype`,`published`,`access`),
  KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `jml_menu`
--

INSERT INTO `jml_menu` (`id`, `menutype`, `name`, `alias`, `link`, `type`, `published`, `parent`, `componentid`, `sublevel`, `ordering`, `checked_out`, `checked_out_time`, `pollid`, `browserNav`, `access`, `utaccess`, `params`, `lft`, `rgt`, `home`) VALUES
(1, 'mainmenu', 'Frontpage', 'home', 'index.php?option=com_content&view=frontpage', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 3, 'num_leading_articles=0\nnum_intro_articles=4\nnum_columns=1\nnum_links=0\norderby_pri=\norderby_sec=front\nmulti_column_order=1\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 1),
(2, 'mainmenu', 'Reports', 'reports', 'index.php?option=com_content&view=category&layout=blog&id=1', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=1\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(3, 'mainmenu', 'Calendar', 'calendar', 'index.php?option=com_eventlist&view=categoryevents&id=2', 'component', 1, 0, 62, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'display_num=\ncat_num=4\nfilter=\ndisplay=\nicons=\nshow_print_icon=\nshow_email_icon=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=0\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(4, 'mainmenu', 'Testimonies', 'testimonies', 'index.php?option=com_content&view=category&layout=blog&id=2', 'component', 1, 0, 20, 0, 5, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(5, 'mainmenu', 'Contact Us', 'contact-us', 'index.php?option=com_content&view=article&id=22', 'component', 1, 0, 20, 0, 8, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(6, 'bottommenu', 'Contact Us', 'contact-us', 'index.php?option=com_content&view=article&id=22', 'component', 1, 0, 20, 0, 1, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(7, 'bottommenu', 'Privacy Policy', 'privacy-policy', 'index.php?option=com_content&view=article&id=23', 'component', 1, 0, 20, 0, 2, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(8, 'bottommenu', 'Terms of Use', 'terms-of-use', 'index.php?option=com_content&view=article&id=24', 'component', 1, 0, 20, 0, 3, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(9, 'mainmenu', 'Galleries', 'galleries', 'index.php?option=com_phocagallery&view=categories', 'component', 1, 0, 42, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'image=-1\nimage_align=right\nshow_pagination_categories=0\nshow_pagination_category=1\nshow_pagination_limit_categories=0\nshow_pagination_limit_category=1\ndisplay_cat_name_title=1\ncategories_columns=3\nequal_percentage_width=\ndisplay_image_categories=\ncategories_box_width=\nimage_categories_size=\ncategories_image_ordering=\ncategories_display_avatar=\ndisplay_subcategories=\ndisplay_empty_categories=1\nhide_categories=\nshow_categories=\ndisplay_access_category=\ndefault_pagination_categories=\npagination_categories=\nfont_color=\nbackground_color=\nbackground_color_hover=\nimage_background_color=\nimage_background_shadow=\nborder_color=\nborder_color_hover=\nmargin_box=\npadding_box=\ndisplay_name=\ndisplay_icon_detail=\ndisplay_icon_download=\ndisplay_icon_folder=\nfont_size_name=\nchar_length_name=\ncategory_box_space=\ndisplay_categories_sub=\ndisplay_subcat_page=\ndisplay_category_icon_image=\ncategory_image_ordering=\ndisplay_back_button=\ndisplay_categories_back_button=\ndefault_pagination_category=\npagination_category=\ndisplay_img_desc_box=\nfont_size_img_desc=\nimg_desc_box_height=\nchar_length_img_desc=\ndisplay_categories_cv=\ndisplay_subcat_page_cv=\ndisplay_category_icon_image_cv=\ncategory_image_ordering_cv=\ndisplay_back_button_cv=\ndisplay_categories_back_button_cv=\ncategories_columns_cv=\ndisplay_image_categories_cv=\nimage_categories_size_cv=\ndetail_window=\ndetail_window_background_color=\nmodal_box_overlay_color=\nmodal_box_overlay_opacity=\nmodal_box_border_color=\nmodal_box_border_width=\nsb_slideshow_delay=\nsb_lang=\nhighslide_class=\nhighslide_opacity=\nhighslide_outline_type=\nhighslide_fullimg=\nhighslide_close_button=\nhighslide_slideshow=\njak_slideshow_delay=\njak_orientation=\njak_description=\njak_description_height=\ndisplay_description_detail=\ndisplay_title_description=\nfont_size_desc=\nfont_color_desc=\ndescription_detail_height=\ndescription_lightbox_font_size=\ndescription_lightbox_font_color=\ndescription_lightbox_bg_color=\nslideshow_delay=\nslideshow_pause=\nslideshow_random=\ndetail_buttons=\nphocagallery_width=\nphocagallery_center=\ncategory_ordering=\nimage_ordering=\ngallery_metadesc=\ngallery_metakey=\nalt_value=\nenable_user_cp=\nenable_upload_avatar=\nenable_avatar_approve=\nenable_usercat_approve=\nenable_usersubcat_approve=\nuser_subcat_count=\nmax_create_cat_char=\nenable_userimage_approve=\nmax_upload_char=\nupload_maxsize=\nupload_maxres_width=\nupload_maxres_height=\nuser_images_max_size=\nenable_java=\nenable_java_admin=\njava_resize_width=\njava_resize_height=\njava_box_width=\njava_box_height=\ndisplay_rating=\ndisplay_rating_img=\ndisplay_comment=\ndisplay_comment_img=\ncomment_width=\nmax_comment_char=\nexternal_comment_system=\nenable_piclens=\nstart_piclens=\npiclens_image=\nswitch_image=\nswitch_width=\nswitch_height=\nswitch_fixed_size=\nenable_overlib=\nol_bg_color=\nol_fg_color=\nol_tf_color=\nol_cf_color=\noverlib_overlay_opacity=\noverlib_image_rate=\ncreate_watermark=\nwatermark_position_x=\nwatermark_position_y=\ndisplay_icon_vm=\ndisplay_category_statistics=\ndisplay_main_cat_stat=\ndisplay_lastadded_cat_stat=\ncount_lastadded_cat_stat=\ndisplay_mostviewed_cat_stat=\ncount_mostviewed_cat_stat=\ndisplay_camera_info=\nexif_information=\ndisplay_categories_geotagging=\ncategories_lng=\ncategories_lat=\ncategories_zoom=\ncategories_map_width=\ncategories_map_height=\ndisplay_icon_geotagging=\ndisplay_category_geotagging=\ncategory_map_width=\ncategory_map_height=\npagination_thumbnail_creation=\nclean_thumbnails=\nenable_thumb_creation=\ncrop_thumbnail=\njpeg_quality=\nenable_picasa_loading=\npicasa_load_pagination=\nicon_format=\nlarge_image_width=\nlarge_image_height=\nmedium_image_width=\nmedium_image_height=\nsmall_image_width=\nsmall_image_height=\nfront_modal_box_width=\nfront_modal_box_height=\nadmin_modal_box_width=\nadmin_modal_box_height=\nfolder_permissions=\njfile_thumbs=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(10, 'bottommenu', 'Login to send us your ideas', 'login-to-send-us-your-ideas', 'index.php?option=com_user&view=login', 'component', 1, 0, 14, 0, 4, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_login_title=1\nheader_login=\nlogin=\nlogin_message=0\ndescription_login=0\ndescription_login_text=\nimage_login=\nimage_login_align=right\nshow_logout_title=1\nheader_logout=\nlogout=\nlogout_message=1\ndescription_logout=1\ndescription_logout_text=\nimage_logout=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(11, 'mainmenu', 'Reflections', 'reflections', 'index.php?option=com_content&view=category&layout=blog&id=6', 'component', 0, 0, 20, 0, 6, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0),
(12, 'mainmenu', 'Devotionals', 'devotionals', 'index.php?option=com_content&view=category&layout=blog&id=4', 'component', 0, 0, 20, 0, 7, 0, '0000-00-00 00:00:00', 0, 0, 0, 0, 'show_description=0\nshow_description_image=0\nnum_leading_articles=1\nnum_intro_articles=4\nnum_columns=2\nnum_links=4\norderby_pri=\norderby_sec=\nmulti_column_order=0\nshow_pagination=2\nshow_pagination_results=1\nshow_feed_link=1\nshow_noauth=\nshow_title=\nlink_titles=\nshow_intro=\nshow_section=\nlink_section=\nshow_category=\nlink_category=\nshow_author=\nshow_create_date=\nshow_modify_date=\nshow_item_navigation=\nshow_readmore=\nshow_vote=\nshow_icons=\nshow_pdf_icon=\nshow_print_icon=\nshow_email_icon=\nshow_hits=\nfeed_summary=\nfusion_item_subtext=\nfusion_columns=1\nfusion_customimage=\nsplitmenu_item_subtext=\nsuckerfish_item_subtext=\npage_title=\nshow_page_title=1\npageclass_sfx=\nmenu_image=-1\nsecure=0\n\n', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jml_menu_types`
--

DROP TABLE IF EXISTS `jml_menu_types`;
CREATE TABLE IF NOT EXISTS `jml_menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(75) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `menutype` (`menutype`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `jml_menu_types`
--

INSERT INTO `jml_menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(2, 'othermenu', 'Other Menu', 'Other Menu'),
(3, 'bottommenu', 'Bottom Menu', 'Bottom Menu');

-- --------------------------------------------------------

--
-- Table structure for table `jml_messages`
--

DROP TABLE IF EXISTS `jml_messages`;
CREATE TABLE IF NOT EXISTS `jml_messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` int(10) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(11) NOT NULL DEFAULT '0',
  `priority` int(1) unsigned NOT NULL DEFAULT '0',
  `subject` text NOT NULL,
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_messages_cfg`
--

DROP TABLE IF EXISTS `jml_messages_cfg`;
CREATE TABLE IF NOT EXISTS `jml_messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_migration_backlinks`
--

DROP TABLE IF EXISTS `jml_migration_backlinks`;
CREATE TABLE IF NOT EXISTS `jml_migration_backlinks` (
  `itemid` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `url` text NOT NULL,
  `sefurl` text NOT NULL,
  `newurl` text NOT NULL,
  PRIMARY KEY (`itemid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_modules`
--

DROP TABLE IF EXISTS `jml_modules`;
CREATE TABLE IF NOT EXISTS `jml_modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) DEFAULT NULL,
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `numnews` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `iscore` tinyint(4) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `control` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `jml_modules`
--

INSERT INTO `jml_modules` (`id`, `title`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `published`, `module`, `numnews`, `access`, `showtitle`, `params`, `iscore`, `client_id`, `control`) VALUES
(1, 'Main Menu', '', 6, 'sidebar-a', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 1, 'menutype=mainmenu\nmenu_style=list\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\nshow_whitespace=0\ncache=1\ntag_id=\nclass_sfx=\nmoduleclass_sfx=_menu\nmaxdepth=10\nmenu_images=0\nmenu_images_align=0\nmenu_images_link=0\nexpand_menu=0\nactivate_parent=0\nfull_active_id=0\nindent_image=0\nindent_image1=\nindent_image2=\nindent_image3=\nindent_image4=\nindent_image5=\nindent_image6=\nspacer=\nend_spacer=\n\n', 1, 0, ''),
(2, 'Login', '', 1, 'login', 0, '0000-00-00 00:00:00', 1, 'mod_login', 0, 0, 1, '', 1, 1, ''),
(3, 'Popular', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_popular', 0, 2, 1, '', 0, 1, ''),
(4, 'Recent added Articles', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_latest', 0, 2, 1, 'ordering=c_dsc\nuser_id=0\ncache=0\n\n', 0, 1, ''),
(5, 'Menu Stats', '', 5, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_stats', 0, 2, 1, '', 0, 1, ''),
(6, 'Unread Messages', '', 1, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_unread', 0, 2, 1, '', 1, 1, ''),
(7, 'Online Users', '', 2, 'header', 0, '0000-00-00 00:00:00', 1, 'mod_online', 0, 2, 1, '', 1, 1, ''),
(8, 'Toolbar', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', 1, 'mod_toolbar', 0, 2, 1, '', 1, 1, ''),
(9, 'Quick Icons', '', 1, 'icon', 0, '0000-00-00 00:00:00', 1, 'mod_quickicon', 0, 2, 1, '', 1, 1, ''),
(10, 'Logged in Users', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', 1, 'mod_logged', 0, 2, 1, '', 0, 1, ''),
(11, 'Footer', '', 0, 'footer', 0, '0000-00-00 00:00:00', 1, 'mod_footer', 0, 0, 1, '', 1, 1, ''),
(12, 'Admin Menu', '', 1, 'menu', 0, '0000-00-00 00:00:00', 1, 'mod_menu', 0, 2, 1, '', 0, 1, ''),
(13, 'Admin SubMenu', '', 1, 'submenu', 0, '0000-00-00 00:00:00', 1, 'mod_submenu', 0, 2, 1, '', 0, 1, ''),
(14, 'User Status', '', 1, 'status', 0, '0000-00-00 00:00:00', 1, 'mod_status', 0, 2, 1, '', 0, 1, ''),
(15, 'Title', '', 1, 'title', 0, '0000-00-00 00:00:00', 1, 'mod_title', 0, 2, 1, '', 0, 1, ''),
(16, 'Other Menu', '', 1, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_mainmenu', 0, 0, 1, 'menutype=othermenu', 0, 0, ''),
(34, 'Multimedia', '<p><a href="index.php?option=com_phocagallery&amp;view=categories&amp;Itemid=9"><img height="180" width="430" src="images/stories/multimedia.jpg" border="0" /></a></p>', 1, 'content-top-a', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(18, 'JComments Latest', '', 2, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_jcomments', 0, 0, 1, 'object_group=com_content\ncount=5\nlength=50\navatar_size=32\nlimit_object_title=30\nlabel4more=More...\nlabel4author=By\ndateformat=%d.%m.%y %H:%M\nlabel4rss=RSS\n', 0, 0, ''),
(19, 'RokTabs', '', 3, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_roktabs', 0, 0, 1, 'style=light\ncontent_type=joomla\n@spacer=<div id="joomla-label"  style="font-weight:normal;font-size:12px;color:#fff;padding:4px;margin:0;background:#666;">Joomla Core Content</div>\nshow_front=1\n@spacer=<div id="k2-label"  id="k2-label"style="font-weight:normal;font-size:12px;color:#fff;padding:4px;margin:0;background:#666;">K2 Content</div>\nFeaturedItems=1\n@spacer=<div id="content-label"  style="font-weight:normal;font-size:12px;color:#fff;padding:4px;margin:0;background:#666;">Content Parameters</div>\n@spacer=<div style="font-weight:normal;font-size:12px;color:#fff;padding:4px;margin:0;background:#666;">Animation and Styling</div>\nwidth=500\ntabs_count=3\nduration=600\ntransition_type=scrolling\ntransition_fx=Quad.easeInOut\n@spacer=<div style="font-weight:normal;font-size:12px;color:#fff;padding:4px;margin:0;background:#666;">Tabs Details</div>\ntabs_position=top\ntabs_event=click\ntabs_title=content\ntabs_incremental=Tab \ntabs_hideh6=1\ntabs_iconside=left\ntabs_iconpath=__module__/images\ntabs_icon=icon_home.gif, icon_security.gif, icon_comment.gif, icon_world.gif, icon_note.gif\n@spacer=<div style="font-weight:normal;font-size:12px;color:#fff;padding:4px;margin:0;background:#666;">Autoplay Settings</div>\nautoplay_delay=2000\n', 0, 0, ''),
(20, 'RokNavMenu', '', 1, 'sidebar-b', 0, '0000-00-00 00:00:00', 1, 'mod_roknavmenu', 0, 0, 0, 'menutype=mainmenu\nlimit_levels=0\nstartLevel=0\nendLevel=0\nshowAllChildren=0\nwindow_open=\ntheme=/templates/rt_crystalline_j15/html/mod_roknavmenu/themes/gantry-fusion\nenable_js=0\nopacity=1\neffect=slidefade\nhidedelay=500\nmenu_animation=Quad.easeOut\nmenu_duration=400\npill=0\npill_animation=Back.easeOut\npill_duration=400\ncentered-offset=0\ntweakInitial_x=0\ntweakInitial_y=0\ntweakSubsequent_x=0\ntweakSubsequent_y=0\nenable_current_id=1\nroknavmenu_splitmenu_enable_current_id=0\nroknavmenu_fusion_load_css=1\nroknavmenu_fusion_enable_js=0\nroknavmenu_fusion_opacity=1\nroknavmenu_fusion_effect=slidefade\nroknavmenu_fusion_hidedelay=500\nroknavmenu_fusion_menu_animation=Quad.easeOut\nroknavmenu_fusion_menu_duration=400\nroknavmenu_fusion_pill=0\nroknavmenu_fusion_pill_animation=Back.easeOut\nroknavmenu_fusion_pill_duration=400\nroknavmenu_fusion_centeredOffset=0\nroknavmenu_fusion_tweakInitial_x=0\nroknavmenu_fusion_tweakInitial_y=0\nroknavmenu_fusion_tweakSubsequent_x=0\nroknavmenu_fusion_tweakSubsequent_y=0\nroknavmenu_fusion_enable_current_id=0\ncustom_layout=default.php\ncustom_formatter=default.php\nurl_type=relative\ncache=0\nmodule_cache=1\ncache_time=900\ntag_id=\nclass_sfx=\nmoduleclass_sfx=\nmaxdepth=10\nmenu_images=0\nmenu_images_link=0\n\n', 1, 0, ''),
(21, 'Highlights', '', 7, 'sidebar-a', 0, '0000-00-00 00:00:00', 0, 'mod_rokstories', 0, 0, 1, 'moduleclass_sfx=\nload_css=1\nlayout_type=layout1\ncontent_type=joomla\nsecid=1\ncatid=2\nshow_front=1\narticle_count=4\nitemsOrdering=\nstrip_tags=a,i,br\ncontent_position=left\nshow_article_title=1\nshow_created_date=0\nshow_article=1\nshow_article_link=1\nlegacy_readmore=0\nthumb_width=90\nstart_width=auto\nuser_id=0\nstart_element=0\nthumbs_opacity=0.3\nfixed_height=0\nmouse_type=click\nautoplay=0\nautoplay_delay=5000\nshow_label_article_title=0\nshow_arrows=0\narrows_placement=outside\nshow_thumbs=0\nfixed_thumb=1\nlink_titles=0\nlink_labels=0\nlink_images=0\nshow_mask=1\nmask_desc_dir=topdown\nmask_imgs_dir=bottomup\nleft_offset_x=-40\nleft_offset_y=-100\nright_offset_x=-30\nright_offset_y=-100\nleft_f_offset_x=-40\nleft_f_offset_y=-100\nright_f_offset_x=-30\nright_f_offset_y=-100\ncache=0\nmodule_cache=1\ncache_time=900\n\n', 0, 0, ''),
(23, 'Recent Testimonies', '', 2, 'content-top-a', 0, '0000-00-00 00:00:00', 0, 'mod_latestnews', 0, 0, 1, 'count=4\nordering=c_dsc\nuser_id=0\nshow_front=1\nsecid=\ncatid=2\nmoduleclass_sfx=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(24, 'log me in', '', 8, 'sidebar-a', 0, '0000-00-00 00:00:00', 0, 'mod_login', 0, 0, 1, 'cache=0\nmoduleclass_sfx=\npretext=\nposttext=\nlogin=\nlogout=\ngreeting=1\nname=0\nusesecure=0\n\n', 0, 0, ''),
(25, 'Contact Details', '<p>95 Fenham Road<br />Peckham, London<br />SE15 1AE</p>\r\n<p>Tel: +44 (0) 207 639 8897<br />T: (0) 208 316 5290<br />Mobile: +44 (0) 7908635010</p>', 4, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 1, 'moduleclass_sfx=\n\n', 0, 0, ''),
(22, 'Search this Site', '', 5, 'sidebar-a', 62, '2010-09-23 08:28:03', 0, 'mod_search', 0, 0, 1, 'moduleclass_sfx=\nwidth=20\ntext=\nbutton=\nbutton_pos=right\nimagebutton=\nbutton_text=\nset_itemid=\ncache=1\ncache_time=900\n\n', 0, 0, ''),
(36, 'Logo', '<p><a href="index.php?option=com_content&amp;view=frontpage&amp;Itemid=1"><img src="images/stories/aflogo.png" border="0" /></a></p>', 0, 'top-a', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(27, 'Dynamic XML Sitemap', '', 1, 'debug', 0, '0000-00-00 00:00:00', 0, 'mod_dynamic_xmlsitemap', 0, 0, 0, 'catsec=1\naccess=0\nlivesite=\nmanual=\n\n', 0, 0, ''),
(28, 'Logo and Slogan', '<p><img src="images/stories/af_logo.png" border="0" width="645" height="100" /></p>', 0, 'top-b', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(29, 'DJ Image Slider', '', 1, 'feature-a', 0, '0000-00-00 00:00:00', 1, 'mod_djimageslider', 0, 0, 0, 'slider_source=1\nslider_type=2\nimage_folder=images/stories/fruit\nlink=\ncategory=9\nshow_title=1\nshow_desc=1\nshow_readmore=1\nlink_title=1\nlink_desc=0\nlimit_desc=\nimage_width=900\nimage_height=300\nvisible_images=3\nspace_between_images=2\nmax_images=20\nsort_by=1\neffect=Cubic\nautoplay=1\nshow_buttons=1\nshow_arrows=1\ndesc_width=\ndesc_bottom=0\ndesc_horizontal=60\nleft_arrow=\nright_arrow=\nplay_button=\npause_button=\narrows_top=30\narrows_horizontal=5\neffect_type=easeIn\nduration=8000\ndelay=2000\nmoduleclass_sfx=\ncache=0\n\n', 0, 0, ''),
(35, 'Article As Module', '', 4, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_articleasmodule', 0, 0, 1, 'article_id=1\nshow_head=1\nhead_link=1\nhead_pre_css=<div class=''articleheading''>\nhead_post_css=</div>\narticle_pre_css=<div style=''margin:10px''> <b>\narticle_post_css=</b></div>\nread_more=1\nread_more_css=readmore\nread_more_text=More...\n', 0, 0, ''),
(30, 'Email Newsletter', '', 3, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_acajoom', 0, 0, 1, 'listids=1\nred_url=\nlinear=0\nintrotext=\nshowlistname=0\ndefaultchecked=1\nshownamefield=1\ndropdown=0\nselecteddrop=0\nfieldsize=30\nposttext=\nreceivehtmldefault=1\nshowreceivehtml=0\nbutton_text=\nbutton_img=\nbutton_text_change=\nbutton_img_change=\nmoduleclass_sfx=\nmod_align=\ncache=0\n\n', 0, 0, ''),
(31, 'RokAjaxSearch', '', 1, 'sidebar-a', 0, '0000-00-00 00:00:00', 1, 'mod_rokajaxsearch', 0, 0, 0, 'moduleclass_sfx=\nsearch_page=index.php?option=com_search&view=search&tmpl=component\nadv_search_page=index.php?option=com_search&view=search\ninclude_css=1\ntheme=blue\nsearchphrase=any\nordering=newest\nlimit=10\nperpage=3\nwebsearch=0\nblogsearch=0\nimagesearch=0\nvideosearch=0\nwebsearch_api=\nshow_pagination=1\nsafesearch=STRICT\nimage_size=MEDIUM\nshow_estimated=1\nhide_divs=\ninclude_link=1\nshow_description=1\ninclude_category=0\nshow_readmore=1\n\n', 0, 0, ''),
(32, 'Join In...', '<p><a href="index.php?option=com_content&amp;view=article&amp;id=22&amp;Itemid=5"><img src="images/stories/ideas.jpg" border="0" /></a></p>\r\n<p><a href="index.php?option=com_content&amp;view=article&amp;id=22:contact-us&amp;catid=5&amp;Itemid=6"><img src="images/stories/feedback.jpg" border="0" /></a></p>\r\n<p><a href="index.php?option=com_content&amp;view=article&amp;id=22:contact-us&amp;catid=5&amp;Itemid=6"><img src="images/stories/prayerrequsts.jpg" border="0" /></a></p>\r\n<p><a href="index.php?option=com_content&amp;view=article&amp;id=22:contact-us&amp;catid=5&amp;Itemid=6"><img src="images/stories/testimonies.jpg" border="0" /></a></p>', 2, 'sidebar-b', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(37, 'Recent Testimonies', '', 3, 'content-top-a', 0, '0000-00-00 00:00:00', 1, 'mod_jw_ucd', 0, 0, 1, 'moduleclass_sfx=\ndisablecss=0\ncache=0\nucd_displaytype=list\nuniqueid=ucd-instance\nwhere=category\nwhere_id=2\nordering=rdate\ncount=1\nshow_front=0\ndisplay=2\nlinked=1\ndatecreated=0\nshow_section_title=0\nshow_category_title=0\nseperator=>>\nwords=\nchars=\nmore=1\nplugins=1\nhideimages=0\ncleanupimages=0\nstriptags=0\nucd_ajf_width=100%\nucd_ajf_height=300px\nucd_ajf_delay=6000\nucd_ajf_bgcolor=#ffffff\nucd_ajf_bottomfade=0\nucd_ajf_cache=10\nucd_jqf_cheight=300px\nucd_jqf_anim=slide\nucd_jqf_speed=750\nucd_jqf_timeout=6000\nucd_jqf_bgcolor=#ffffff\nucd_jqf_bottomfade=0\n\n', 0, 0, ''),
(33, 'Address at Foot of Page', '<p style="text-align: center;">95 Fenham Road, Peckham, London. SE15 1AE&nbsp;</p>', 0, 'copyright', 0, '0000-00-00 00:00:00', 0, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(38, 'Menu, side of Page', '', 0, 'sidebar-b', 0, '0000-00-00 00:00:00', 1, 'mod_rokajaxsearch', 0, 0, 0, 'moduleclass_sfx=\nsearch_page=index.php?option=com_search&view=search&tmpl=component\nadv_search_page=index.php?option=com_search&view=search\ninclude_css=1\ntheme=blue\nsearchphrase=any\nordering=newest\nlimit=10\nperpage=3\nwebsearch=0\nblogsearch=0\nimagesearch=0\nvideosearch=0\nwebsearch_api=\nshow_pagination=1\nsafesearch=STRICT\nimage_size=MEDIUM\nshow_estimated=1\nhide_divs=\ninclude_link=1\nshow_description=1\ninclude_category=0\nshow_readmore=1\n\n', 0, 0, ''),
(39, 'Slogan', '<img src="images/stories/slogan.png" />', 0, 'top-b', 0, '0000-00-00 00:00:00', 1, 'mod_custom', 0, 0, 0, 'moduleclass_sfx=\n\n', 0, 0, ''),
(40, 'ChronoForms', '', 5, 'left', 0, '0000-00-00 00:00:00', 0, 'mod_chronocontact', 0, 0, 1, '', 0, 0, ''),
(41, 'Precious Gems', '', 0, 'sidebar-c', 0, '0000-00-00 00:00:00', 0, 'mod_newsflash', 0, 0, 0, 'catid=10\nlayout=default\nimage=1\nlink_titles=\nshowLastSeparator=1\nreadmore=0\nitem_title=0\nitems=1\nmoduleclass_sfx=\ncache=0\ncache_time=900\n\n', 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_modules_menu`
--

DROP TABLE IF EXISTS `jml_modules_menu`;
CREATE TABLE IF NOT EXISTS `jml_modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jml_modules_menu`
--

INSERT INTO `jml_modules_menu` (`moduleid`, `menuid`) VALUES
(1, 0),
(16, 0),
(18, 0),
(19, 0),
(20, 0),
(21, 0),
(22, 0),
(23, 1),
(24, 0),
(25, 1),
(27, 0),
(28, 0),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(32, 9),
(33, 0),
(34, 1),
(35, 0),
(36, 0),
(37, 1),
(38, 2),
(38, 3),
(38, 4),
(38, 5),
(38, 6),
(38, 7),
(38, 8),
(38, 9),
(39, 0),
(40, 0),
(41, 0);

-- --------------------------------------------------------

--
-- Table structure for table `jml_newsfeeds`
--

DROP TABLE IF EXISTS `jml_newsfeeds`;
CREATE TABLE IF NOT EXISTS `jml_newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `alias` varchar(255) NOT NULL DEFAULT '',
  `link` text NOT NULL,
  `filename` varchar(200) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(11) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(11) unsigned NOT NULL DEFAULT '3600',
  `checked_out` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `published` (`published`),
  KEY `catid` (`catid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery`
--

DROP TABLE IF EXISTS `jml_phocagallery`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `filename` varchar(250) NOT NULL DEFAULT '',
  `description` text,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `latitude` varchar(20) NOT NULL DEFAULT '',
  `longitude` varchar(20) NOT NULL DEFAULT '',
  `zoom` int(3) NOT NULL DEFAULT '0',
  `geotitle` varchar(255) NOT NULL DEFAULT '',
  `videocode` text,
  `vmproductid` int(11) NOT NULL DEFAULT '0',
  `imgorigsize` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  `metakey` text,
  `metadesc` text,
  `extlink1` text,
  `extlink2` text,
  `extid` varchar(255) NOT NULL DEFAULT '',
  `extl` varchar(255) NOT NULL DEFAULT '',
  `extm` varchar(255) NOT NULL DEFAULT '',
  `exts` varchar(255) NOT NULL DEFAULT '',
  `exto` varchar(255) NOT NULL DEFAULT '',
  `extw` varchar(255) NOT NULL DEFAULT '',
  `exth` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=240 ;

--
-- Dumping data for table `jml_phocagallery`
--

INSERT INTO `jml_phocagallery` (`id`, `catid`, `sid`, `title`, `alias`, `filename`, `description`, `date`, `hits`, `latitude`, `longitude`, `zoom`, `geotitle`, `videocode`, `vmproductid`, `imgorigsize`, `published`, `approved`, `checked_out`, `checked_out_time`, `ordering`, `params`, `metakey`, `metadesc`, `extlink1`, `extlink2`, `extid`, `extl`, `extm`, `exts`, `exto`, `extw`, `exth`) VALUES
(60, 2, 0, 'yc-2008-156', 'yc-2008-156', 'yc-2008-156.png', NULL, '2010-08-07 11:45:06', 54, '', '', 0, '', NULL, 0, 367354, 1, 1, 0, '0000-00-00 00:00:00', 18, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(59, 2, 0, 'yc-2008-18', 'yc-2008-18', 'yc-2008-18.png', NULL, '2010-08-07 11:45:06', 50, '', '', 0, '', NULL, 0, 372817, 1, 1, 0, '0000-00-00 00:00:00', 17, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(58, 2, 0, 'yc-2008-17', 'yc-2008-17', 'yc-2008-17.png', NULL, '2010-08-07 11:45:06', 52, '', '', 0, '', NULL, 0, 315610, 1, 1, 0, '0000-00-00 00:00:00', 16, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(57, 2, 0, 'yc-2008-15', 'yc-2008-15', 'yc-2008-15.png', NULL, '2010-08-07 11:45:06', 67, '', '', 0, '', NULL, 0, 342897, 1, 1, 0, '0000-00-00 00:00:00', 15, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(55, 2, 0, 'yc-2008-13', 'yc-2008-13', 'yc-2008-13.png', NULL, '2010-08-07 11:45:06', 67, '', '', 0, '', NULL, 0, 357428, 1, 1, 0, '0000-00-00 00:00:00', 13, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(56, 2, 0, 'yc-2008-14', 'yc-2008-14', 'yc-2008-14.png', NULL, '2010-08-07 11:45:06', 61, '', '', 0, '', NULL, 0, 302890, 1, 1, 0, '0000-00-00 00:00:00', 14, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(54, 2, 0, 'yc-2008-12', 'yc-2008-12', 'yc-2008-12.png', NULL, '2010-08-07 11:45:06', 55, '', '', 0, '', NULL, 0, 342731, 1, 1, 0, '0000-00-00 00:00:00', 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(53, 2, 0, 'yc-2008-11', 'yc-2008-11', 'yc-2008-11.png', NULL, '2010-08-07 11:45:06', 58, '', '', 0, '', NULL, 0, 340786, 1, 1, 0, '0000-00-00 00:00:00', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(52, 2, 0, 'yc-2008-10', 'yc-2008-10', 'yc-2008-10.png', NULL, '2010-08-07 11:45:06', 60, '', '', 0, '', NULL, 0, 359922, 1, 1, 0, '0000-00-00 00:00:00', 10, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(51, 2, 0, 'yc-2008-9', 'yc-2008-9', 'yc-2008-9.png', NULL, '2010-08-07 11:45:06', 48, '', '', 0, '', NULL, 0, 289628, 1, 1, 0, '0000-00-00 00:00:00', 9, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(50, 2, 0, 'yc-2008-8', 'yc-2008-8', 'yc-2008-8.png', NULL, '2010-08-07 11:45:06', 56, '', '', 0, '', NULL, 0, 365077, 1, 1, 0, '0000-00-00 00:00:00', 8, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(49, 2, 0, 'yc-2008-7', 'yc-2008-7', 'yc-2008-7.png', NULL, '2010-08-07 11:45:06', 59, '', '', 0, '', NULL, 0, 302195, 1, 1, 0, '0000-00-00 00:00:00', 7, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(48, 2, 0, 'yc-2008-6', 'yc-2008-6', 'yc-2008-6.png', NULL, '2010-08-07 11:45:06', 60, '', '', 0, '', NULL, 0, 399464, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(47, 2, 0, 'yc-2008-5', 'yc-2008-5', 'yc-2008-5.png', NULL, '2010-08-07 11:45:06', 66, '', '', 0, '', NULL, 0, 378015, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(46, 2, 0, 'yc-2008-4', 'yc-2008-4', 'yc-2008-4.png', NULL, '2010-08-07 11:45:06', 62, '', '', 0, '', NULL, 0, 359389, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(45, 2, 0, 'yc-2008-3', 'yc-2008-3', 'yc-2008-3.png', NULL, '2010-08-07 11:45:06', 78, '', '', 0, '', NULL, 0, 332459, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(44, 2, 0, 'yc-2008-2', 'yc-2008-2', 'yc-2008-2.png', NULL, '2010-08-07 11:45:06', 76, '', '', 0, '', NULL, 0, 298342, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(43, 2, 0, 'yc-2008-1', 'yc-2008-1', 'yc-2008-1.png', NULL, '2010-08-07 11:45:06', 145, '', '', 0, '', NULL, 0, 345117, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(61, 5, 0, '100_4136', '1004136', 'Images/youthcamp2007/100_4136.jpg', NULL, '2010-08-07 13:36:15', 141, '', '', 0, '', NULL, 0, 976994, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(62, 5, 0, '100_4137', '1004137', 'Images/youthcamp2007/100_4137.jpg', NULL, '2010-08-07 13:36:15', 84, '', '', 0, '', NULL, 0, 1199341, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(63, 5, 0, '100_4139', '1004139', 'Images/youthcamp2007/100_4139.jpg', NULL, '2010-08-07 13:36:15', 98, '', '', 0, '', NULL, 0, 1313122, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(64, 5, 0, '100_4145', '1004145', 'Images/youthcamp2007/100_4145.jpg', NULL, '2010-08-07 13:36:15', 84, '', '', 0, '', NULL, 0, 1248214, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(65, 5, 0, '100_4146', '1004146', 'Images/youthcamp2007/100_4146.jpg', NULL, '2010-08-07 13:36:15', 95, '', '', 0, '', NULL, 0, 1355396, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(66, 5, 0, '100_4148', '1004148', 'Images/youthcamp2007/100_4148.jpg', NULL, '2010-08-07 13:36:15', 95, '', '', 0, '', NULL, 0, 1130681, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(67, 5, 0, '100_4149', '1004149', 'Images/youthcamp2007/100_4149.jpg', NULL, '2010-08-07 13:36:15', 91, '', '', 0, '', NULL, 0, 1159193, 1, 1, 0, '0000-00-00 00:00:00', 7, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(68, 4, 0, '100_1005', '1001005', 'Images/kidscruise/100_1005.jpg', NULL, '2010-08-08 19:48:59', 89, '', '', 0, '', NULL, 0, 92930, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(69, 4, 0, '100_1015', '1001015', 'Images/kidscruise/100_1015.jpg', NULL, '2010-08-08 19:48:59', 57, '', '', 0, '', NULL, 0, 67389, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(70, 4, 0, '100_1024', '1001024', 'Images/kidscruise/100_1024.jpg', NULL, '2010-08-08 19:48:59', 54, '', '', 0, '', NULL, 0, 61493, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(71, 4, 0, '100_1031', '1001031', 'Images/kidscruise/100_1031.jpg', NULL, '2010-08-08 19:48:59', 54, '', '', 0, '', NULL, 0, 68326, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(72, 4, 0, '100_1036', '1001036', 'Images/kidscruise/100_1036.jpg', NULL, '2010-08-08 19:48:59', 53, '', '', 0, '', NULL, 0, 48146, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(73, 4, 0, '100_1048', '1001048', 'Images/kidscruise/100_1048.jpg', NULL, '2010-08-08 19:48:59', 57, '', '', 0, '', NULL, 0, 61373, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(74, 6, 0, 'dscf0103', 'dscf0103', 'Images/teenbeachday/dscf0103.jpg', NULL, '2010-08-09 01:09:45', 125, '', '', 0, '', NULL, 0, 119625, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(75, 6, 0, 'dscf0104', 'dscf0104', 'Images/teenbeachday/dscf0104.jpg', NULL, '2010-08-09 01:09:45', 68, '', '', 0, '', NULL, 0, 117217, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(76, 6, 0, 'dscf01042', 'dscf01042', 'Images/teenbeachday/dscf01042.jpg', NULL, '2010-08-09 01:09:45', 70, '', '', 0, '', NULL, 0, 117217, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(77, 6, 0, 'dscf0105', 'dscf0105', 'Images/teenbeachday/dscf0105.jpg', NULL, '2010-08-09 01:09:45', 87, '', '', 0, '', NULL, 0, 253568, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(78, 6, 0, 'dscf0108', 'dscf0108', 'Images/teenbeachday/dscf0108.jpg', NULL, '2010-08-09 01:09:45', 74, '', '', 0, '', NULL, 0, 116917, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(79, 6, 0, 'dscf0111', 'dscf0111', 'Images/teenbeachday/dscf0111.jpg', NULL, '2010-08-09 01:09:45', 89, '', '', 0, '', NULL, 0, 383791, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(80, 6, 0, 'dscf0112', 'dscf0112', 'Images/teenbeachday/dscf0112.jpg', NULL, '2010-08-09 01:09:45', 86, '', '', 0, '', NULL, 0, 382334, 1, 1, 0, '0000-00-00 00:00:00', 7, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(81, 6, 0, 'dscf0115', 'dscf0115', 'Images/teenbeachday/dscf0115.jpg', NULL, '2010-08-09 01:09:45', 89, '', '', 0, '', NULL, 0, 1041397, 1, 1, 0, '0000-00-00 00:00:00', 8, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(82, 6, 0, 'dscf0131', 'dscf0131', 'Images/teenbeachday/dscf0131.jpg', NULL, '2010-08-09 01:09:45', 83, '', '', 0, '', NULL, 0, 393043, 1, 1, 0, '0000-00-00 00:00:00', 9, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(83, 6, 0, 'dscf0132', 'dscf0132', 'Images/teenbeachday/dscf0132.jpg', NULL, '2010-08-09 01:09:45', 74, '', '', 0, '', NULL, 0, 1004467, 1, 1, 0, '0000-00-00 00:00:00', 10, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(84, 6, 0, 'dscf0133', 'dscf0133', 'Images/teenbeachday/dscf0133.jpg', NULL, '2010-08-09 01:09:45', 68, '', '', 0, '', NULL, 0, 1025002, 1, 1, 0, '0000-00-00 00:00:00', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(85, 6, 0, 'dscf0136', 'dscf0136', 'Images/teenbeachday/dscf0136.jpg', NULL, '2010-08-09 01:09:45', 70, '', '', 0, '', NULL, 0, 998910, 1, 1, 0, '0000-00-00 00:00:00', 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(86, 3, 0, '1', '1', 'Images/msc/1.jpg', NULL, '2010-08-09 01:18:45', 151, '', '', 0, '', NULL, 0, 5112, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(87, 3, 0, '100_2432', '1002432', 'Images/msc/100_2432.jpg', NULL, '2010-08-09 01:18:45', 108, '', '', 0, '', NULL, 0, 1289056, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(88, 3, 0, '100_2435', '1002435', 'Images/msc/100_2435.jpg', NULL, '2010-08-09 01:18:45', 95, '', '', 0, '', NULL, 0, 1318650, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(89, 3, 0, 'dscf0020', 'dscf0020', 'Images/msc/dscf0020.jpg', NULL, '2010-08-09 01:18:45', 67, '', '', 0, '', NULL, 0, 1547251, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(90, 3, 0, 'holland2 026', 'holland2-026', 'Images/msc/holland2 026.jpg', NULL, '2010-08-09 01:18:45', 81, '', '', 0, '', NULL, 0, 152737, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(91, 3, 0, 'youth website 001', 'youth-website-001', 'Images/msc/youth website 001.jpg', NULL, '2010-08-09 01:18:45', 95, '', '', 0, '', NULL, 0, 54382, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(92, 3, 0, 'youth website 002', 'youth-website-002', 'Images/msc/youth website 002.jpg', NULL, '2010-08-09 01:18:45', 91, '', '', 0, '', NULL, 0, 57820, 1, 1, 0, '0000-00-00 00:00:00', 7, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(93, 3, 0, 'youth website 007', 'youth-website-007', 'Images/msc/youth website 007.jpg', NULL, '2010-08-09 01:18:45', 70, '', '', 0, '', NULL, 0, 34979, 1, 1, 0, '0000-00-00 00:00:00', 8, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(94, 3, 0, 'youth website 012', 'youth-website-012', 'Images/msc/youth website 012.jpg', NULL, '2010-08-09 01:18:45', 64, '', '', 0, '', NULL, 0, 35938, 1, 1, 0, '0000-00-00 00:00:00', 9, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(95, 3, 0, 'youth website 013', 'youth-website-013', 'Images/msc/youth website 013.jpg', NULL, '2010-08-09 01:18:45', 62, '', '', 0, '', NULL, 0, 42500, 1, 1, 0, '0000-00-00 00:00:00', 10, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(96, 3, 0, 'youth website 014', 'youth-website-014', 'Images/msc/youth website 014.jpg', NULL, '2010-08-09 01:18:45', 106, '', '', 0, '', NULL, 0, 42159, 1, 1, 0, '0000-00-00 00:00:00', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(97, 3, 0, 'youth website 019', 'youth-website-019', 'Images/msc/youth website 019.jpg', NULL, '2010-08-09 01:18:45', 71, '', '', 0, '', NULL, 0, 2161173, 1, 1, 0, '0000-00-00 00:00:00', 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(98, 3, 0, 'youth website 0192', 'youth-website-0192', 'Images/msc/youth website 0192.jpg', NULL, '2010-08-09 01:18:45', 79, '', '', 0, '', NULL, 0, 2161173, 1, 1, 0, '0000-00-00 00:00:00', 13, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(100, 7, 0, '01052010163', '01052010163', 'Images/2010/01052010163.jpg', NULL, '2010-11-06 19:31:57', 286, '', '', 0, '', NULL, 0, 634554, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(143, 10, 0, 'cimg2292', 'cimg2292', 'Images/youthcamp2009/cimg2292.jpg', NULL, '2010-11-06 20:24:55', 120, '', '', 0, '', NULL, 0, 1917811, 1, 1, 0, '0000-00-00 00:00:00', 1, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(102, 7, 0, 'pict0537', 'pict0537', 'Images/2010/pict0537.jpg', NULL, '2010-11-06 19:31:57', 101, '', '', 0, '', NULL, 0, 3066468, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(103, 7, 0, 'pict0540', 'pict0540', 'Images/2010/pict0540.jpg', NULL, '2010-11-06 19:31:57', 84, '', '', 0, '', NULL, 0, 2918823, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(104, 7, 0, 'pict0552', 'pict0552', 'Images/2010/pict0552.jpg', NULL, '2010-11-06 19:31:57', 85, '', '', 0, '', NULL, 0, 3017193, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(105, 7, 0, 'pict0554', 'pict0554', 'Images/2010/pict0554.jpg', NULL, '2010-11-06 19:31:57', 71, '', '', 0, '', NULL, 0, 2730670, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(106, 7, 0, 'pict0556', 'pict0556', 'Images/2010/pict0556.jpg', NULL, '2010-11-06 19:31:57', 72, '', '', 0, '', NULL, 0, 3122995, 1, 1, 0, '0000-00-00 00:00:00', 7, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(107, 7, 0, 'pict0559', 'pict0559', 'Images/2010/pict0559.jpg', NULL, '2010-11-06 19:31:57', 67, '', '', 0, '', NULL, 0, 2895355, 1, 1, 0, '0000-00-00 00:00:00', 8, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(108, 7, 0, 'pict0563', 'pict0563', 'Images/2010/pict0563.jpg', NULL, '2010-11-06 19:31:57', 72, '', '', 0, '', NULL, 0, 2951383, 1, 1, 0, '0000-00-00 00:00:00', 9, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(109, 7, 0, 'pict0566', 'pict0566', 'Images/2010/pict0566.jpg', NULL, '2010-11-06 19:31:57', 57, '', '', 0, '', NULL, 0, 2735293, 1, 1, 0, '0000-00-00 00:00:00', 10, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(110, 7, 0, 'pict0567', 'pict0567', 'Images/2010/pict0567.jpg', NULL, '2010-11-06 19:31:57', 72, '', '', 0, '', NULL, 0, 1717763, 1, 1, 0, '0000-00-00 00:00:00', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(111, 7, 0, 'pict0589', 'pict0589', 'Images/2010/pict0589.jpg', NULL, '2010-11-06 19:31:57', 74, '', '', 0, '', NULL, 0, 3004220, 1, 1, 0, '0000-00-00 00:00:00', 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(112, 7, 0, 'pict0595', 'pict0595', 'Images/2010/pict0595.jpg', NULL, '2010-11-06 19:31:57', 62, '', '', 0, '', NULL, 0, 3118566, 1, 1, 0, '0000-00-00 00:00:00', 13, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(113, 7, 0, 'pict0613', 'pict0613', 'Images/2010/pict0613.jpg', NULL, '2010-11-06 19:31:57', 62, '', '', 0, '', NULL, 0, 2959455, 1, 1, 0, '0000-00-00 00:00:00', 14, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(114, 7, 0, 'pict0628', 'pict0628', 'Images/2010/pict0628.jpg', NULL, '2010-11-06 19:31:57', 63, '', '', 0, '', NULL, 0, 2581570, 1, 1, 0, '0000-00-00 00:00:00', 15, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(115, 7, 0, 'pict0631', 'pict0631', 'Images/2010/pict0631.jpg', NULL, '2010-11-06 19:31:57', 58, '', '', 0, '', NULL, 0, 3022434, 1, 1, 0, '0000-00-00 00:00:00', 16, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(116, 7, 0, 'pict0633', 'pict0633', 'Images/2010/pict0633.jpg', NULL, '2010-11-06 19:31:57', 61, '', '', 0, '', NULL, 0, 2911647, 1, 1, 0, '0000-00-00 00:00:00', 17, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(117, 7, 0, 'pict0638', 'pict0638', 'Images/2010/pict0638.jpg', NULL, '2010-11-06 19:31:57', 53, '', '', 0, '', NULL, 0, 2857628, 1, 1, 0, '0000-00-00 00:00:00', 18, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(118, 7, 0, 'pict0643', 'pict0643', 'Images/2010/pict0643.jpg', NULL, '2010-11-06 19:31:57', 61, '', '', 0, '', NULL, 0, 2913345, 1, 1, 0, '0000-00-00 00:00:00', 19, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(119, 7, 0, 'pict0645', 'pict0645', 'Images/2010/pict0645.jpg', NULL, '2010-11-06 19:31:57', 77, '', '', 0, '', NULL, 0, 2939874, 1, 1, 0, '0000-00-00 00:00:00', 20, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(120, 7, 0, 'pict0655', 'pict0655', 'Images/2010/pict0655.jpg', NULL, '2010-11-06 19:31:57', 68, '', '', 0, '', NULL, 0, 2839418, 1, 1, 0, '0000-00-00 00:00:00', 21, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(121, 7, 0, 'pict0656', 'pict0656', 'Images/2010/pict0656.jpg', NULL, '2010-11-06 19:31:57', 31, '', '', 0, '', NULL, 0, 3116131, 1, 1, 0, '0000-00-00 00:00:00', 22, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(122, 7, 0, 'pict0683', 'pict0683', 'Images/2010/pict0683.jpg', NULL, '2010-11-06 19:31:57', 30, '', '', 0, '', NULL, 0, 913404, 1, 1, 0, '0000-00-00 00:00:00', 23, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(123, 7, 0, 'pict0686', 'pict0686', 'Images/2010/pict0686.jpg', NULL, '2010-11-06 19:31:57', 35, '', '', 0, '', NULL, 0, 1113254, 1, 1, 0, '0000-00-00 00:00:00', 24, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(124, 7, 0, 'pict0696', 'pict0696', 'Images/2010/pict0696.jpg', NULL, '2010-11-06 19:31:57', 29, '', '', 0, '', NULL, 0, 907051, 1, 1, 0, '0000-00-00 00:00:00', 25, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(125, 7, 0, 'pict0698', 'pict0698', 'Images/2010/pict0698.jpg', NULL, '2010-11-06 19:31:57', 24, '', '', 0, '', NULL, 0, 873961, 1, 1, 0, '0000-00-00 00:00:00', 26, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(126, 7, 0, 'pict0708', 'pict0708', 'Images/2010/pict0708.jpg', NULL, '2010-11-06 19:31:57', 30, '', '', 0, '', NULL, 0, 3121267, 1, 1, 0, '0000-00-00 00:00:00', 27, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(127, 7, 0, 'pict0713', 'pict0713', 'Images/2010/pict0713.jpg', NULL, '2010-11-06 19:31:57', 30, '', '', 0, '', NULL, 0, 2889340, 1, 1, 0, '0000-00-00 00:00:00', 28, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(128, 7, 0, 'pict0716', 'pict0716', 'Images/2010/pict0716.jpg', NULL, '2010-11-06 19:31:57', 32, '', '', 0, '', NULL, 0, 2208961, 1, 1, 0, '0000-00-00 00:00:00', 29, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(129, 7, 0, 'pict0720', 'pict0720', 'Images/2010/pict0720.jpg', NULL, '2010-11-06 19:31:57', 23, '', '', 0, '', NULL, 0, 3088619, 1, 1, 0, '0000-00-00 00:00:00', 30, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(130, 7, 0, 'pict0727', 'pict0727', 'Images/2010/pict0727.jpg', NULL, '2010-11-06 19:31:57', 24, '', '', 0, '', NULL, 0, 1049337, 1, 1, 0, '0000-00-00 00:00:00', 31, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(131, 7, 0, 'pict0728', 'pict0728', 'Images/2010/pict0728.jpg', NULL, '2010-11-06 19:31:57', 26, '', '', 0, '', NULL, 0, 973906, 1, 1, 0, '0000-00-00 00:00:00', 32, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(132, 7, 0, 'pict0736', 'pict0736', 'Images/2010/pict0736.jpg', NULL, '2010-11-06 19:31:57', 29, '', '', 0, '', NULL, 0, 921455, 1, 1, 0, '0000-00-00 00:00:00', 33, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(133, 7, 0, 'pict0744', 'pict0744', 'Images/2010/pict0744.jpg', NULL, '2010-11-06 19:31:57', 29, '', '', 0, '', NULL, 0, 997884, 1, 1, 0, '0000-00-00 00:00:00', 34, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(134, 7, 0, 'pict0745', 'pict0745', 'Images/2010/pict0745.jpg', NULL, '2010-11-06 19:31:57', 32, '', '', 0, '', NULL, 0, 934176, 1, 1, 0, '0000-00-00 00:00:00', 35, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(135, 7, 0, 'pict0748', 'pict0748', 'Images/2010/pict0748.jpg', NULL, '2010-11-06 19:31:57', 31, '', '', 0, '', NULL, 0, 958360, 1, 1, 0, '0000-00-00 00:00:00', 36, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(136, 7, 0, 'pict0763', 'pict0763', 'Images/2010/pict0763.jpg', NULL, '2010-11-06 19:31:57', 25, '', '', 0, '', NULL, 0, 812325, 1, 1, 0, '0000-00-00 00:00:00', 37, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(137, 7, 0, 'pict0767', 'pict0767', 'Images/2010/pict0767.jpg', NULL, '2010-11-06 19:31:57', 38, '', '', 0, '', NULL, 0, 997718, 1, 1, 0, '0000-00-00 00:00:00', 38, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(138, 7, 0, 'pict0772', 'pict0772', 'Images/2010/pict0772.jpg', NULL, '2010-11-06 19:31:57', 35, '', '', 0, '', NULL, 0, 979863, 1, 1, 0, '0000-00-00 00:00:00', 39, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(139, 7, 0, 'pict0777', 'pict0777', 'Images/2010/pict0777.jpg', NULL, '2010-11-06 19:31:57', 42, '', '', 0, '', NULL, 0, 1153912, 1, 1, 0, '0000-00-00 00:00:00', 40, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(140, 7, 0, 'pict0787', 'pict0787', 'Images/2010/pict0787.jpg', NULL, '2010-11-06 19:31:57', 34, '', '', 0, '', NULL, 0, 1194618, 1, 1, 0, '0000-00-00 00:00:00', 41, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(141, 7, 0, 'pict0789', 'pict0789', 'Images/2010/pict0789.jpg', NULL, '2010-11-06 19:31:57', 37, '', '', 0, '', NULL, 0, 1115039, 1, 1, 0, '0000-00-00 00:00:00', 42, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(142, 7, 0, 'pict0796', 'pict0796', 'Images/2010/pict0796.jpg', NULL, '2010-11-06 19:31:57', 28, '', '', 0, '', NULL, 0, 1108746, 1, 1, 0, '0000-00-00 00:00:00', 43, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(144, 10, 0, 'cimg2294', 'cimg2294', 'Images/youthcamp2009/cimg2294.jpg', NULL, '2010-11-06 20:24:55', 55, '', '', 0, '', NULL, 0, 1936594, 1, 1, 0, '0000-00-00 00:00:00', 2, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(145, 10, 0, 'cimg2295', 'cimg2295', 'Images/youthcamp2009/cimg2295.jpg', NULL, '2010-11-06 20:24:56', 46, '', '', 0, '', NULL, 0, 1870438, 1, 1, 0, '0000-00-00 00:00:00', 3, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(146, 10, 0, 'cimg2296', 'cimg2296', 'Images/youthcamp2009/cimg2296.jpg', NULL, '2010-11-06 20:24:56', 50, '', '', 0, '', NULL, 0, 1960749, 1, 1, 0, '0000-00-00 00:00:00', 4, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(147, 10, 0, 'cimg2297', 'cimg2297', 'Images/youthcamp2009/cimg2297.jpg', NULL, '2010-11-06 20:24:56', 62, '', '', 0, '', NULL, 0, 1889206, 1, 1, 0, '0000-00-00 00:00:00', 5, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(148, 10, 0, 'img_1337_2', 'img13372', 'Images/youthcamp2009/img_1337_2.jpg', NULL, '2010-11-06 20:24:56', 61, '', '', 0, '', NULL, 0, 747301, 1, 1, 0, '0000-00-00 00:00:00', 6, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(149, 10, 0, 'pict4555', 'pict4555', 'Images/youthcamp2009/pict4555.jpg', NULL, '2010-11-06 20:24:56', 51, '', '', 0, '', NULL, 0, 458220, 1, 1, 0, '0000-00-00 00:00:00', 7, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(150, 10, 0, 'pict4558', 'pict4558', 'Images/youthcamp2009/pict4558.jpg', NULL, '2010-11-06 20:24:56', 52, '', '', 0, '', NULL, 0, 318646, 1, 1, 0, '0000-00-00 00:00:00', 8, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(151, 10, 0, 'pict4561', 'pict4561', 'Images/youthcamp2009/pict4561.jpg', NULL, '2010-11-06 20:24:56', 54, '', '', 0, '', NULL, 0, 556891, 1, 1, 0, '0000-00-00 00:00:00', 9, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(152, 10, 0, 'pict4562', 'pict4562', 'Images/youthcamp2009/pict4562.jpg', NULL, '2010-11-06 20:24:56', 49, '', '', 0, '', NULL, 0, 449371, 1, 1, 0, '0000-00-00 00:00:00', 10, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(153, 10, 0, 'pict4566', 'pict4566', 'Images/youthcamp2009/pict4566.jpg', NULL, '2010-11-06 20:24:56', 55, '', '', 0, '', NULL, 0, 497278, 1, 1, 0, '0000-00-00 00:00:00', 11, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(154, 10, 0, 'pict4568', 'pict4568', 'Images/youthcamp2009/pict4568.jpg', NULL, '2010-11-06 20:24:56', 56, '', '', 0, '', NULL, 0, 463340, 1, 1, 0, '0000-00-00 00:00:00', 12, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(155, 10, 0, 'pict4571', 'pict4571', 'Images/youthcamp2009/pict4571.jpg', NULL, '2010-11-06 20:24:56', 54, '', '', 0, '', NULL, 0, 482709, 1, 1, 0, '0000-00-00 00:00:00', 13, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(156, 10, 0, 'pict4574', 'pict4574', 'Images/youthcamp2009/pict4574.jpg', NULL, '2010-11-06 20:24:56', 52, '', '', 0, '', NULL, 0, 482854, 1, 1, 0, '0000-00-00 00:00:00', 14, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(157, 10, 0, 'pict4578', 'pict4578', 'Images/youthcamp2009/pict4578.jpg', NULL, '2010-11-06 20:24:56', 51, '', '', 0, '', NULL, 0, 385877, 1, 1, 0, '0000-00-00 00:00:00', 15, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(158, 10, 0, 'pict4581', 'pict4581', 'Images/youthcamp2009/pict4581.jpg', NULL, '2010-11-06 20:24:56', 48, '', '', 0, '', NULL, 0, 465015, 1, 1, 0, '0000-00-00 00:00:00', 16, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(159, 10, 0, 'pict4584', 'pict4584', 'Images/youthcamp2009/pict4584.jpg', NULL, '2010-11-06 20:24:56', 47, '', '', 0, '', NULL, 0, 350855, 1, 1, 0, '0000-00-00 00:00:00', 17, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(160, 10, 0, 'pict4587', 'pict4587', 'Images/youthcamp2009/pict4587.jpg', NULL, '2010-11-06 20:24:56', 48, '', '', 0, '', NULL, 0, 385805, 1, 1, 0, '0000-00-00 00:00:00', 18, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(161, 10, 0, 'pict4589', 'pict4589', 'Images/youthcamp2009/pict4589.jpg', NULL, '2010-11-06 20:24:56', 55, '', '', 0, '', NULL, 0, 461274, 1, 1, 0, '0000-00-00 00:00:00', 19, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(162, 10, 0, 'pict4590', 'pict4590', 'Images/youthcamp2009/pict4590.jpg', NULL, '2010-11-06 20:24:56', 56, '', '', 0, '', NULL, 0, 728921, 1, 1, 0, '0000-00-00 00:00:00', 20, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(163, 10, 0, 'pict4593', 'pict4593', 'Images/youthcamp2009/pict4593.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 520745, 1, 1, 0, '0000-00-00 00:00:00', 21, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(164, 10, 0, 'pict4595', 'pict4595', 'Images/youthcamp2009/pict4595.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 790097, 1, 1, 0, '0000-00-00 00:00:00', 22, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(165, 10, 0, 'pict4602', 'pict4602', 'Images/youthcamp2009/pict4602.jpg', NULL, '2010-11-06 20:24:56', 30, '', '', 0, '', NULL, 0, 657497, 1, 1, 0, '0000-00-00 00:00:00', 23, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(166, 10, 0, 'pict4605', 'pict4605', 'Images/youthcamp2009/pict4605.jpg', NULL, '2010-11-06 20:24:56', 34, '', '', 0, '', NULL, 0, 532720, 1, 1, 0, '0000-00-00 00:00:00', 24, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(167, 10, 0, 'pict4610', 'pict4610', 'Images/youthcamp2009/pict4610.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 585989, 1, 1, 0, '0000-00-00 00:00:00', 25, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(168, 10, 0, 'pict4616', 'pict4616', 'Images/youthcamp2009/pict4616.jpg', NULL, '2010-11-06 20:24:56', 30, '', '', 0, '', NULL, 0, 462038, 1, 1, 0, '0000-00-00 00:00:00', 26, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(169, 10, 0, 'pict4619', 'pict4619', 'Images/youthcamp2009/pict4619.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 418164, 1, 1, 0, '0000-00-00 00:00:00', 27, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(170, 10, 0, 'pict4622', 'pict4622', 'Images/youthcamp2009/pict4622.jpg', NULL, '2010-11-06 20:24:56', 23, '', '', 0, '', NULL, 0, 536663, 1, 1, 0, '0000-00-00 00:00:00', 28, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(171, 10, 0, 'pict4627', 'pict4627', 'Images/youthcamp2009/pict4627.jpg', NULL, '2010-11-06 20:24:56', 28, '', '', 0, '', NULL, 0, 448390, 1, 1, 0, '0000-00-00 00:00:00', 29, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(172, 10, 0, 'pict4630', 'pict4630', 'Images/youthcamp2009/pict4630.jpg', NULL, '2010-11-06 20:24:56', 32, '', '', 0, '', NULL, 0, 473067, 1, 1, 0, '0000-00-00 00:00:00', 30, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(173, 10, 0, 'pict4632', 'pict4632', 'Images/youthcamp2009/pict4632.jpg', NULL, '2010-11-06 20:24:56', 29, '', '', 0, '', NULL, 0, 462114, 1, 1, 0, '0000-00-00 00:00:00', 31, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(174, 10, 0, 'pict4638', 'pict4638', 'Images/youthcamp2009/pict4638.jpg', NULL, '2010-11-06 20:24:56', 26, '', '', 0, '', NULL, 0, 400809, 1, 1, 0, '0000-00-00 00:00:00', 32, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(175, 10, 0, 'pict4640', 'pict4640', 'Images/youthcamp2009/pict4640.jpg', NULL, '2010-11-06 20:24:56', 26, '', '', 0, '', NULL, 0, 803125, 1, 1, 0, '0000-00-00 00:00:00', 33, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(176, 10, 0, 'pict4643', 'pict4643', 'Images/youthcamp2009/pict4643.jpg', NULL, '2010-11-06 20:24:56', 24, '', '', 0, '', NULL, 0, 703115, 1, 1, 0, '0000-00-00 00:00:00', 34, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(177, 10, 0, 'pict4646', 'pict4646', 'Images/youthcamp2009/pict4646.jpg', NULL, '2010-11-06 20:24:56', 32, '', '', 0, '', NULL, 0, 855513, 1, 1, 0, '0000-00-00 00:00:00', 35, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(178, 10, 0, 'pict4654', 'pict4654', 'Images/youthcamp2009/pict4654.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 623145, 1, 1, 0, '0000-00-00 00:00:00', 36, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(179, 10, 0, 'pict4662', 'pict4662', 'Images/youthcamp2009/pict4662.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 825578, 1, 1, 0, '0000-00-00 00:00:00', 37, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(180, 10, 0, 'pict4664', 'pict4664', 'Images/youthcamp2009/pict4664.jpg', NULL, '2010-11-06 20:24:56', 28, '', '', 0, '', NULL, 0, 565175, 1, 1, 0, '0000-00-00 00:00:00', 38, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(181, 10, 0, 'pict4669', 'pict4669', 'Images/youthcamp2009/pict4669.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 497389, 1, 1, 0, '0000-00-00 00:00:00', 39, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(182, 10, 0, 'pict4671', 'pict4671', 'Images/youthcamp2009/pict4671.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 502494, 1, 1, 0, '0000-00-00 00:00:00', 40, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(183, 10, 0, 'pict4677', 'pict4677', 'Images/youthcamp2009/pict4677.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 499035, 1, 1, 0, '0000-00-00 00:00:00', 41, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(184, 10, 0, 'pict4680', 'pict4680', 'Images/youthcamp2009/pict4680.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 430008, 1, 1, 0, '0000-00-00 00:00:00', 42, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(185, 10, 0, 'pict4687', 'pict4687', 'Images/youthcamp2009/pict4687.jpg', NULL, '2010-11-06 20:24:56', 24, '', '', 0, '', NULL, 0, 468135, 1, 1, 0, '0000-00-00 00:00:00', 43, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(186, 10, 0, 'pict4694', 'pict4694', 'Images/youthcamp2009/pict4694.jpg', NULL, '2010-11-06 20:24:56', 26, '', '', 0, '', NULL, 0, 498890, 1, 1, 0, '0000-00-00 00:00:00', 44, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(187, 10, 0, 'pict4700', 'pict4700', 'Images/youthcamp2009/pict4700.jpg', NULL, '2010-11-06 20:24:56', 38, '', '', 0, '', NULL, 0, 425478, 1, 1, 0, '0000-00-00 00:00:00', 45, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(188, 10, 0, 'pict4702', 'pict4702', 'Images/youthcamp2009/pict4702.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 329287, 1, 1, 0, '0000-00-00 00:00:00', 46, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(189, 10, 0, 'pict4704', 'pict4704', 'Images/youthcamp2009/pict4704.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 412465, 1, 1, 0, '0000-00-00 00:00:00', 47, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(190, 10, 0, 'pict4715', 'pict4715', 'Images/youthcamp2009/pict4715.jpg', NULL, '2010-11-06 20:24:56', 28, '', '', 0, '', NULL, 0, 363937, 1, 1, 0, '0000-00-00 00:00:00', 48, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(191, 10, 0, 'pict4719', 'pict4719', 'Images/youthcamp2009/pict4719.jpg', NULL, '2010-11-06 20:24:56', 31, '', '', 0, '', NULL, 0, 1276054, 1, 1, 0, '0000-00-00 00:00:00', 49, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(192, 10, 0, 'pict4721', 'pict4721', 'Images/youthcamp2009/pict4721.jpg', NULL, '2010-11-06 20:24:56', 28, '', '', 0, '', NULL, 0, 533474, 1, 1, 0, '0000-00-00 00:00:00', 50, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(193, 10, 0, 'pict4727', 'pict4727', 'Images/youthcamp2009/pict4727.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 590176, 1, 1, 0, '0000-00-00 00:00:00', 51, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(194, 10, 0, 'pict4733', 'pict4733', 'Images/youthcamp2009/pict4733.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 570123, 1, 1, 0, '0000-00-00 00:00:00', 52, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(195, 10, 0, 'pict4739', 'pict4739', 'Images/youthcamp2009/pict4739.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 400615, 1, 1, 0, '0000-00-00 00:00:00', 53, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(196, 10, 0, 'pict4742', 'pict4742', 'Images/youthcamp2009/pict4742.jpg', NULL, '2010-11-06 20:24:56', 23, '', '', 0, '', NULL, 0, 577948, 1, 1, 0, '0000-00-00 00:00:00', 54, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(197, 10, 0, 'pict4745', 'pict4745', 'Images/youthcamp2009/pict4745.jpg', NULL, '2010-11-06 20:24:56', 20, '', '', 0, '', NULL, 0, 589266, 1, 1, 0, '0000-00-00 00:00:00', 55, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(198, 10, 0, 'pict4749', 'pict4749', 'Images/youthcamp2009/pict4749.jpg', NULL, '2010-11-06 20:24:56', 32, '', '', 0, '', NULL, 0, 511592, 1, 1, 0, '0000-00-00 00:00:00', 56, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(199, 10, 0, 'pict4751', 'pict4751', 'Images/youthcamp2009/pict4751.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 555173, 1, 1, 0, '0000-00-00 00:00:00', 57, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(200, 10, 0, 'pict4756', 'pict4756', 'Images/youthcamp2009/pict4756.jpg', NULL, '2010-11-06 20:24:56', 24, '', '', 0, '', NULL, 0, 488211, 1, 1, 0, '0000-00-00 00:00:00', 58, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(201, 10, 0, 'pict4758', 'pict4758', 'Images/youthcamp2009/pict4758.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 447808, 1, 1, 0, '0000-00-00 00:00:00', 59, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(202, 10, 0, 'pict4760', 'pict4760', 'Images/youthcamp2009/pict4760.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 478447, 1, 1, 0, '0000-00-00 00:00:00', 60, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(203, 10, 0, 'pict4763', 'pict4763', 'Images/youthcamp2009/pict4763.jpg', NULL, '2010-11-06 20:24:56', 23, '', '', 0, '', NULL, 0, 546019, 1, 1, 0, '0000-00-00 00:00:00', 61, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(204, 10, 0, 'pict4766', 'pict4766', 'Images/youthcamp2009/pict4766.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 315745, 1, 1, 0, '0000-00-00 00:00:00', 62, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(205, 10, 0, 'pict4767', 'pict4767', 'Images/youthcamp2009/pict4767.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 481781, 1, 1, 0, '0000-00-00 00:00:00', 63, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(206, 10, 0, 'pict4769', 'pict4769', 'Images/youthcamp2009/pict4769.jpg', NULL, '2010-11-06 20:24:56', 20, '', '', 0, '', NULL, 0, 417559, 1, 1, 0, '0000-00-00 00:00:00', 64, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(207, 10, 0, 'pict4775', 'pict4775', 'Images/youthcamp2009/pict4775.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 502571, 1, 1, 0, '0000-00-00 00:00:00', 65, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(208, 10, 0, 'pict4778', 'pict4778', 'Images/youthcamp2009/pict4778.jpg', NULL, '2010-11-06 20:24:56', 27, '', '', 0, '', NULL, 0, 443721, 1, 1, 0, '0000-00-00 00:00:00', 66, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(209, 10, 0, 'pict4779', 'pict4779', 'Images/youthcamp2009/pict4779.jpg', NULL, '2010-11-06 20:24:56', 24, '', '', 0, '', NULL, 0, 498041, 1, 1, 0, '0000-00-00 00:00:00', 67, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(210, 10, 0, 'pict4781', 'pict4781', 'Images/youthcamp2009/pict4781.jpg', NULL, '2010-11-06 20:24:56', 22, '', '', 0, '', NULL, 0, 529423, 1, 1, 0, '0000-00-00 00:00:00', 68, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(211, 10, 0, 'pict4786', 'pict4786', 'Images/youthcamp2009/pict4786.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 442294, 1, 1, 0, '0000-00-00 00:00:00', 69, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(212, 10, 0, 'pict4788', 'pict4788', 'Images/youthcamp2009/pict4788.jpg', NULL, '2010-11-06 20:24:56', 17, '', '', 0, '', NULL, 0, 484325, 1, 1, 0, '0000-00-00 00:00:00', 70, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(213, 10, 0, 'pict4792', 'pict4792', 'Images/youthcamp2009/pict4792.jpg', NULL, '2010-11-06 20:24:56', 21, '', '', 0, '', NULL, 0, 421534, 1, 1, 0, '0000-00-00 00:00:00', 71, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(214, 10, 0, 'pict4795', 'pict4795', 'Images/youthcamp2009/pict4795.jpg', NULL, '2010-11-06 20:24:56', 21, '', '', 0, '', NULL, 0, 468385, 1, 1, 0, '0000-00-00 00:00:00', 72, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(215, 10, 0, 'pict4797', 'pict4797', 'Images/youthcamp2009/pict4797.jpg', NULL, '2010-11-06 20:24:56', 19, '', '', 0, '', NULL, 0, 383337, 1, 1, 0, '0000-00-00 00:00:00', 73, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(216, 10, 0, 'pict4801', 'pict4801', 'Images/youthcamp2009/pict4801.jpg', NULL, '2010-11-06 20:24:56', 21, '', '', 0, '', NULL, 0, 416551, 1, 1, 0, '0000-00-00 00:00:00', 74, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(217, 10, 0, 'pict4805', 'pict4805', 'Images/youthcamp2009/pict4805.jpg', NULL, '2010-11-06 20:24:56', 23, '', '', 0, '', NULL, 0, 513380, 1, 1, 0, '0000-00-00 00:00:00', 75, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(218, 10, 0, 'pict4808', 'pict4808', 'Images/youthcamp2009/pict4808.jpg', NULL, '2010-11-06 20:24:56', 22, '', '', 0, '', NULL, 0, 574998, 1, 1, 0, '0000-00-00 00:00:00', 76, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(219, 10, 0, 'pict4811', 'pict4811', 'Images/youthcamp2009/pict4811.jpg', NULL, '2010-11-06 20:24:56', 25, '', '', 0, '', NULL, 0, 400221, 1, 1, 0, '0000-00-00 00:00:00', 77, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(220, 10, 0, 'pict4814', 'pict4814', 'Images/youthcamp2009/pict4814.jpg', NULL, '2010-11-06 20:24:56', 19, '', '', 0, '', NULL, 0, 545955, 1, 1, 0, '0000-00-00 00:00:00', 78, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(221, 10, 0, 'pict4817', 'pict4817', 'Images/youthcamp2009/pict4817.jpg', NULL, '2010-11-06 20:24:56', 23, '', '', 0, '', NULL, 0, 553326, 1, 1, 0, '0000-00-00 00:00:00', 79, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(222, 10, 0, 'pict4821', 'pict4821', 'Images/youthcamp2009/pict4821.jpg', NULL, '2010-11-06 20:24:56', 23, '', '', 0, '', NULL, 0, 549773, 1, 1, 0, '0000-00-00 00:00:00', 80, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(223, 10, 0, 'pict4824', 'pict4824', 'Images/youthcamp2009/pict4824.jpg', NULL, '2010-11-06 20:24:56', 22, '', '', 0, '', NULL, 0, 459676, 1, 1, 0, '0000-00-00 00:00:00', 81, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(224, 10, 0, 'pict4829', 'pict4829', 'Images/youthcamp2009/pict4829.jpg', NULL, '2010-11-06 20:24:56', 21, '', '', 0, '', NULL, 0, 482037, 1, 1, 0, '0000-00-00 00:00:00', 82, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(225, 10, 0, 'pict4832', 'pict4832', 'Images/youthcamp2009/pict4832.jpg', NULL, '2010-11-06 20:24:56', 17, '', '', 0, '', NULL, 0, 612367, 1, 1, 0, '0000-00-00 00:00:00', 83, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(226, 10, 0, 'pict4835', 'pict4835', 'Images/youthcamp2009/pict4835.jpg', NULL, '2010-11-06 20:24:56', 17, '', '', 0, '', NULL, 0, 556441, 1, 1, 0, '0000-00-00 00:00:00', 84, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(227, 10, 0, 'pict4839', 'pict4839', 'Images/youthcamp2009/pict4839.jpg', NULL, '2010-11-06 20:24:56', 19, '', '', 0, '', NULL, 0, 705479, 1, 1, 0, '0000-00-00 00:00:00', 85, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(228, 10, 0, 'pict4843', 'pict4843', 'Images/youthcamp2009/pict4843.jpg', NULL, '2010-11-06 20:24:56', 23, '', '', 0, '', NULL, 0, 523870, 1, 1, 0, '0000-00-00 00:00:00', 86, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(229, 10, 0, 'pict4847', 'pict4847', 'Images/youthcamp2009/pict4847.jpg', NULL, '2010-11-06 20:24:56', 33, '', '', 0, '', NULL, 0, 875993, 1, 1, 0, '0000-00-00 00:00:00', 87, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(230, 10, 0, 'pict4849', 'pict4849', 'Images/youthcamp2009/pict4849.jpg', NULL, '2010-11-06 20:24:56', 23, '', '', 0, '', NULL, 0, 640611, 1, 1, 0, '0000-00-00 00:00:00', 88, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(231, 10, 0, 'pict4854', 'pict4854', 'Images/youthcamp2009/pict4854.jpg', NULL, '2010-11-06 20:24:56', 22, '', '', 0, '', NULL, 0, 824943, 1, 1, 0, '0000-00-00 00:00:00', 89, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(232, 10, 0, 'pict4859', 'pict4859', 'Images/youthcamp2009/pict4859.jpg', NULL, '2010-11-06 20:24:56', 17, '', '', 0, '', NULL, 0, 502374, 1, 1, 0, '0000-00-00 00:00:00', 90, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(233, 10, 0, 'pict4862', 'pict4862', 'Images/youthcamp2009/pict4862.jpg', NULL, '2010-11-06 20:24:56', 19, '', '', 0, '', NULL, 0, 657899, 1, 1, 0, '0000-00-00 00:00:00', 91, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(234, 10, 0, 'pict4866', 'pict4866', 'Images/youthcamp2009/pict4866.jpg', NULL, '2010-11-06 20:24:56', 21, '', '', 0, '', NULL, 0, 598150, 1, 1, 0, '0000-00-00 00:00:00', 92, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(235, 10, 0, 'pict4872', 'pict4872', 'Images/youthcamp2009/pict4872.jpg', NULL, '2010-11-06 20:24:56', 19, '', '', 0, '', NULL, 0, 587804, 1, 1, 0, '0000-00-00 00:00:00', 93, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(236, 10, 0, 'pict4874', 'pict4874', 'Images/youthcamp2009/pict4874.jpg', NULL, '2010-11-06 20:24:56', 19, '', '', 0, '', NULL, 0, 592174, 1, 1, 0, '0000-00-00 00:00:00', 94, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(237, 10, 0, 'pict4877', 'pict4877', 'Images/youthcamp2009/pict4877.jpg', NULL, '2010-11-06 20:24:56', 20, '', '', 0, '', NULL, 0, 553128, 1, 1, 0, '0000-00-00 00:00:00', 95, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(238, 10, 0, 'pict4878', 'pict4878', 'Images/youthcamp2009/pict4878.jpg', NULL, '2010-11-06 20:24:56', 22, '', '', 0, '', NULL, 0, 565943, 1, 1, 0, '0000-00-00 00:00:00', 96, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', ''),
(239, 10, 0, 'pict4880', 'pict4880', 'Images/youthcamp2009/pict4880.jpg', NULL, '2010-11-06 20:24:56', 19, '', '', 0, '', NULL, 0, 566454, 1, 1, 0, '0000-00-00 00:00:00', 97, NULL, NULL, NULL, NULL, NULL, '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery_categories`
--

DROP TABLE IF EXISTS `jml_phocagallery_categories`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `owner_id` int(11) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` varchar(255) NOT NULL DEFAULT '',
  `section` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `editor` varchar(50) DEFAULT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `hits` int(11) NOT NULL DEFAULT '0',
  `accessuserid` text,
  `uploaduserid` text,
  `deleteuserid` text,
  `userfolder` text,
  `latitude` varchar(20) NOT NULL DEFAULT '',
  `longitude` varchar(20) NOT NULL DEFAULT '',
  `zoom` int(3) NOT NULL DEFAULT '0',
  `geotitle` varchar(255) NOT NULL DEFAULT '',
  `extid` varchar(255) NOT NULL DEFAULT '',
  `exta` varchar(255) NOT NULL DEFAULT '',
  `extu` varchar(255) NOT NULL DEFAULT '',
  `extauth` varchar(255) NOT NULL DEFAULT '',
  `params` text,
  `metakey` text,
  `metadesc` text,
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`section`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `jml_phocagallery_categories`
--

INSERT INTO `jml_phocagallery_categories` (`id`, `parent_id`, `owner_id`, `title`, `name`, `alias`, `image`, `section`, `image_position`, `description`, `date`, `published`, `approved`, `checked_out`, `checked_out_time`, `editor`, `ordering`, `access`, `count`, `hits`, `accessuserid`, `uploaduserid`, `deleteuserid`, `userfolder`, `latitude`, `longitude`, `zoom`, `geotitle`, `extid`, `exta`, `extu`, `extauth`, `params`, `metakey`, `metadesc`) VALUES
(1, 0, 0, 'Bible Bowl Camp 2009', '', 'bible-bowl-camp-2009', '', '', 'left', '', '2009-07-29 00:00:00', 1, 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, 225, '0', '-2', '62', 'Images/BibleBowl2009', '52.463593', '-3.337851', 0, 'Cefn Lea Park, Newtown, Mid-Wales', '', '', '', '', NULL, 'bible bowl 2009, Apostolic faith UK bible bwol 2009', 'View gallery of Bible Bowl 2009'),
(2, 9, 0, '2008', '', '2008', '', '', 'left', '', '2010-08-07 09:10:04', 1, 1, 0, '0000-00-00 00:00:00', NULL, 2, 0, 0, 212, '0', '-2', '-2', 'Images/youthcamp2008', '50.640456', '-1.169792', 0, 'Winchester House, Isle of Wight', '', '', '', '', NULL, '', 'Photos of youth camp 2008 at Winchester House, Isle of Wight'),
(3, 0, 0, 'Miscellaneous', '', 'miscellaneous', '', '', 'left', '', '2010-08-07 09:16:57', 1, 1, 0, '0000-00-00 00:00:00', NULL, 3, 0, 0, 270, '0', '-2', '-2', 'Images/msc', '', '', 0, '', '', '', '', '', NULL, '', ''),
(4, 0, 0, 'Kid''s Cruise', '', 'kids-cruise', '', '', 'left', '', '2010-08-07 11:04:43', 1, 1, 0, '0000-00-00 00:00:00', NULL, 4, 0, 0, 126, '0', '-2', '-2', 'Images/kidscruise', '', '', 0, '', '', '', '', '', NULL, '', ''),
(5, 9, 0, '2007', '', 'yc2007', '', '', 'left', '', '2010-08-07 11:07:56', 1, 1, 0, '0000-00-00 00:00:00', NULL, 5, 0, 0, 247, '0', '-2', '-2', 'Images/youthcamp2007', '', '', 0, '', '', '', '', '', NULL, '', ''),
(6, 0, 64, 'Teen Beach Day', '', 'teen-beach-day', '', '', 'left', '', '2010-08-07 11:15:32', 1, 1, 0, '0000-00-00 00:00:00', NULL, 6, 0, 0, 178, '0', '-2', '-2', 'Images/teenbeachday', '', '', 0, '', '', '', '', '', NULL, '', 'Teen Beach Day Photo Gallery '),
(7, 9, 0, '2010', '', '2010', '', '', 'left', '', '2010-09-24 21:12:56', 1, 1, 0, '0000-00-00 00:00:00', NULL, 1, 0, 0, 527, '0', '-2', '-2', 'Images/2010', '', '', 0, '', '', '', '', '', NULL, '', ''),
(8, 0, 0, 'Videos', '', 'videos', '', '', 'left', '', '2010-09-24 21:22:02', 1, 1, 0, '0000-00-00 00:00:00', NULL, 7, 0, 0, 174, '0', '-2', '-2', 'Videos', '', '', 0, '', '', '', '', '', NULL, '', ''),
(9, 0, 0, 'Youth Camp', '', 'youth-camp', '', '', 'left', '', '2010-10-04 10:40:45', 1, 1, 0, '0000-00-00 00:00:00', NULL, 8, 0, 0, 384, '0', '-2', '-2', '', '', '', 0, '', '', '', '', '', NULL, '', ''),
(10, 9, 0, '2009', '', '2009', '', '', 'left', '', '2010-11-06 19:48:17', 1, 1, 0, '0000-00-00 00:00:00', NULL, 6, 0, 0, 273, '0', '64', '64', 'Images/youthcamp2009', '50.7160', '-1.9151', 14, 'Youth Camp, 2009 (Bournemouth)', '', '', '', '', NULL, '', 'Youth Camp 2009 Photos'),
(11, 0, 62, 'May 2008 Away Day', '', 'may-2008-away-day', '', '', 'left', '', '2010-12-31 15:10:37', 1, 1, 0, '0000-00-00 00:00:00', NULL, 9, 0, 0, 43, '62', '62', '62', 'Images', '51.440737417836104', '-0.06112754344940185', 18, '', '', '', '', '', NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery_comments`
--

DROP TABLE IF EXISTS `jml_phocagallery_comments`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL DEFAULT '',
  `comment` text,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery_img_comments`
--

DROP TABLE IF EXISTS `jml_phocagallery_img_comments`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery_img_comments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `title` varchar(255) NOT NULL DEFAULT '',
  `comment` text,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery_img_votes`
--

DROP TABLE IF EXISTS `jml_phocagallery_img_votes`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery_img_votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rating` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery_img_votes_statistics`
--

DROP TABLE IF EXISTS `jml_phocagallery_img_votes_statistics`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery_img_votes_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imgid` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `average` float(8,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery_user`
--

DROP TABLE IF EXISTS `jml_phocagallery_user`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL DEFAULT '0',
  `avatar` varchar(40) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`),
  KEY `userid` (`userid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `jml_phocagallery_user`
--

INSERT INTO `jml_phocagallery_user` (`id`, `userid`, `avatar`, `published`, `approved`, `checked_out`, `checked_out_time`, `ordering`, `params`) VALUES
(1, 64, '', 1, 0, 0, '0000-00-00 00:00:00', 1, NULL),
(2, 62, '', 1, 0, 0, '0000-00-00 00:00:00', 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery_votes`
--

DROP TABLE IF EXISTS `jml_phocagallery_votes`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery_votes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `userid` int(11) NOT NULL DEFAULT '0',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rating` tinyint(1) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_phocagallery_votes_statistics`
--

DROP TABLE IF EXISTS `jml_phocagallery_votes_statistics`;
CREATE TABLE IF NOT EXISTS `jml_phocagallery_votes_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `average` float(8,6) NOT NULL DEFAULT '0.000000',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_plugins`
--

DROP TABLE IF EXISTS `jml_plugins`;
CREATE TABLE IF NOT EXISTS `jml_plugins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `element` varchar(100) NOT NULL DEFAULT '',
  `folder` varchar(100) NOT NULL DEFAULT '',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(3) NOT NULL DEFAULT '0',
  `iscore` tinyint(3) NOT NULL DEFAULT '0',
  `client_id` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_folder` (`published`,`client_id`,`access`,`folder`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `jml_plugins`
--

INSERT INTO `jml_plugins` (`id`, `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES
(1, 'Authentication - Joomla', 'joomla', 'authentication', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(2, 'Authentication - LDAP', 'ldap', 'authentication', 0, 2, 0, 1, 0, 0, '0000-00-00 00:00:00', 'host=\nport=389\nuse_ldapV3=0\nnegotiate_tls=0\nno_referrals=0\nauth_method=bind\nbase_dn=\nsearch_string=\nusers_dn=\nusername=\npassword=\nldap_fullname=fullName\nldap_email=mail\nldap_uid=uid\n\n'),
(3, 'Authentication - GMail', 'gmail', 'authentication', 0, 4, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(4, 'Authentication - OpenID', 'openid', 'authentication', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(5, 'User - Joomla!', 'joomla', 'user', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', 'autoregister=1\n\n'),
(6, 'Search - Content', 'content', 'search', 0, 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\nsearch_content=1\nsearch_uncategorised=1\nsearch_archived=1\n\n'),
(7, 'Search - Contacts', 'contacts', 'search', 0, 3, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(8, 'Search - Categories', 'categories', 'search', 0, 4, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(9, 'Search - Sections', 'sections', 'search', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(10, 'Search - Newsfeeds', 'newsfeeds', 'search', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(11, 'Search - Weblinks', 'weblinks', 'search', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'search_limit=50\n\n'),
(12, 'Content - Pagebreak', 'pagebreak', 'content', 0, 10000, 1, 1, 0, 0, '0000-00-00 00:00:00', 'enabled=1\ntitle=1\nmultipage_toc=1\nshowall=1\n\n'),
(13, 'Content - Rating', 'vote', 'content', 0, 4, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(14, 'Content - Email Cloaking', 'emailcloak', 'content', 0, 5, 1, 0, 0, 0, '0000-00-00 00:00:00', 'mode=1\n\n'),
(15, 'Content - Code Hightlighter (GeSHi)', 'geshi', 'content', 0, 5, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(16, 'Content - Load Module', 'loadmodule', 'content', 0, 6, 1, 0, 0, 0, '0000-00-00 00:00:00', 'enabled=1\nstyle=0\n\n'),
(17, 'Content - Page Navigation', 'pagenavigation', 'content', 0, 2, 1, 1, 0, 0, '0000-00-00 00:00:00', 'position=1\n\n'),
(18, 'Editor - No Editor', 'none', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(19, 'Editor - TinyMCE', 'tinymce', 'editors', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', 'mode=advanced\nskin=0\ncompressed=0\ncleanup_startup=0\ncleanup_save=2\nentity_encoding=raw\nlang_mode=0\nlang_code=en\ntext_direction=ltr\ncontent_css=1\ncontent_css_custom=\nrelative_urls=1\nnewlines=0\ninvalid_elements=applet\nextended_elements=\ntoolbar=top\ntoolbar_align=left\nhtml_height=550\nhtml_width=750\nelement_path=1\nfonts=1\npaste=1\nsearchreplace=1\ninsertdate=1\nformat_date=%Y-%m-%d\ninserttime=1\nformat_time=%H:%M:%S\ncolors=1\ntable=1\nsmilies=1\nmedia=1\nhr=1\ndirectionality=1\nfullscreen=1\nstyle=1\nlayer=1\nxhtmlxtras=1\nvisualchars=1\nnonbreaking=1\ntemplate=0\nadvimage=1\nadvlink=1\nautosave=1\ncontextmenu=1\ninlinepopups=1\nsafari=1\ncustom_plugin=\ncustom_button=\n\n'),
(20, 'Editor - XStandard Lite 2.0', 'xstandard', 'editors', 0, 0, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(21, 'Editor Button - Image', 'image', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(22, 'Editor Button - Pagebreak', 'pagebreak', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(23, 'Editor Button - Readmore', 'readmore', 'editors-xtd', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(24, 'XML-RPC - Joomla', 'joomla', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(25, 'XML-RPC - Blogger API', 'blogger', 'xmlrpc', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', 'catid=1\nsectionid=0\n\n'),
(27, 'System - SEF', 'sef', 'system', 0, 1, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(28, 'System - Debug', 'debug', 'system', 0, 2, 1, 0, 0, 0, '0000-00-00 00:00:00', 'queries=1\nmemory=1\nlangauge=1\n\n'),
(29, 'System - Legacy', 'legacy', 'system', 0, 3, 0, 1, 0, 0, '0000-00-00 00:00:00', 'route=0\n\n'),
(30, 'System - Cache', 'cache', 'system', 0, 4, 0, 1, 0, 0, '0000-00-00 00:00:00', 'browsercache=0\ncachetime=15\n\n'),
(31, 'System - Log', 'log', 'system', 0, 5, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(32, 'System - Remember Me', 'remember', 'system', 0, 6, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(33, 'System - Backlink', 'backlink', 'system', 0, 7, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(34, 'System - Mootools Upgrade', 'mtupgrade', 'system', 0, 8, 0, 1, 0, 0, '0000-00-00 00:00:00', ''),
(35, 'Content - Componentbot', 'componentbot', 'content', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', 'height=500\nwidth=500\n'),
(36, 'Button - Xmap Link', 'xmaplink', 'editors-xtd', 0, 0, 0, 0, 0, 0, '0000-00-00 00:00:00', ''),
(37, 'System - RokCandy', 'rokcandy_system', 'system', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(38, 'Button - RokCandy', 'rokcandy_button', 'editors-xtd', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(39, 'RokNavMenu - Boost', 'boost', 'roknavmenu', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(40, 'RokNavMenu - Extended Link', 'extendedlink', 'roknavmenu', 0, 0, 1, 1, 0, 0, '0000-00-00 00:00:00', ''),
(41, 'Acajoom Content Bot', 'acajoombot', 'acajoom', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(42, 'Load a Module into Acajoom', 'module', 'acajoom', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(43, 'Acajoom User Synchronization', 'acasyncuser', 'user', 0, 9, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(44, 'Editor - JCE 1.5.6', 'jce', 'editors', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(45, 'System - Advanced Module Manager', 'advancedmodules', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(46, 'System - ReReplacer', 'rereplacer', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(47, 'System - NoNumber! Elements', 'nonumberelements', 'system', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', ''),
(48, 'chronocontact', 'chronocontact', 'content', 0, 0, 1, 0, 0, 0, '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_polls`
--

DROP TABLE IF EXISTS `jml_polls`;
CREATE TABLE IF NOT EXISTS `jml_polls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `voters` int(9) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '0',
  `lag` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_poll_data`
--

DROP TABLE IF EXISTS `jml_poll_data`;
CREATE TABLE IF NOT EXISTS `jml_poll_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pollid` int(11) NOT NULL DEFAULT '0',
  `text` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `pollid` (`pollid`,`text`(1))
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_poll_date`
--

DROP TABLE IF EXISTS `jml_poll_date`;
CREATE TABLE IF NOT EXISTS `jml_poll_date` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `vote_id` int(11) NOT NULL DEFAULT '0',
  `poll_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `poll_id` (`poll_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_poll_menu`
--

DROP TABLE IF EXISTS `jml_poll_menu`;
CREATE TABLE IF NOT EXISTS `jml_poll_menu` (
  `pollid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`pollid`,`menuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_rereplacer`
--

DROP TABLE IF EXISTS `jml_rereplacer`;
CREATE TABLE IF NOT EXISTS `jml_rereplacer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `description` text NOT NULL,
  `search` text NOT NULL,
  `replace` text NOT NULL,
  `area` text NOT NULL,
  `params` text NOT NULL,
  `published` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `id` (`id`,`published`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jml_rereplacer`
--

INSERT INTO `jml_rereplacer` (`id`, `name`, `description`, `search`, `replace`, `area`, `params`, `published`, `ordering`, `checked_out`, `checked_out_time`) VALUES
(1, 'Powered by Links', '\\''This is for replacing all powered by Links on the frontend\\''', 'Powered By ChronoForms - ChronoEngine.com,Powered by Phoca Gallery', 'The Apostolic Faith Mission, UK', 'body', 'view_state=2\nuse_xml=0\nxml=\nregex=1\nword_search=0\ns_modifier=1\ncasesensitive=0\nthorough=0\nenable_in_title=1\nenable_in_author=1\nenable_in_feeds=1\nenable_in_admin=0\nbetween_start=\nbetween_end=\nenable_tags=1\nlimit_tagselect=0\ntagselect=*[title,alt] meta[content]\nmatch_method=and\nassignto_menuitems=0\nassignto_menuitems_inc_children=0\nassignto_menuitems_inc_noitemid=0\nassignto_secscats=0\nassignto_secscats_inc=inc_secs,inc_cats,inc_arts\nassignto_articles=0\nassignto_articles_selection=\nassignto_k2cats=0\nassignto_k2cats_inc_children=0\nassignto_k2cats_inc=inc_cats,inc_items\nassignto_mrcats=0\nassignto_mrcats_inc_children=0\nassignto_mrcats_inc=inc_cats,inc_items\nassignto_zoocats=0\nassignto_zoocats_inc_children=0\nassignto_zoocats_inc=inc_cats,inc_items\nassignto_components=1\nassignto_components_selection=x,com_acajoom,com_chronocontact,com_eventlist,com_phocagallery\nassignto_urls=0\nassignto_urls_selection=\nassignto_urls_selection_sef=\nassignto_browsers=0\nassignto_date=0\nassignto_date_publish_up=\nassignto_date_publish_down=\nassignto_usergrouplevels=0\nassignto_usergrouplevels_selection=0\nassignto_users=0\nassignto_users_selection=\nassignto_languages=0\nassignto_templates=0\n@zip=0\nassignto_php=0\nassignto_php_selection=\nother_doreplace=0\nother_replace=\narea=body\n\n', 1, 1, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `jml_rokcandy`
--

DROP TABLE IF EXISTS `jml_rokcandy`;
CREATE TABLE IF NOT EXISTS `jml_rokcandy` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL,
  `macro` text NOT NULL,
  `html` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  `checked_out` int(11) NOT NULL,
  `checked_out_time` datetime NOT NULL,
  `ordering` int(11) NOT NULL,
  `params` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `jml_rokcandy`
--

INSERT INTO `jml_rokcandy` (`id`, `catid`, `macro`, `html`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`) VALUES
(20, 7, '[code]{text}[/code]', '<code>{text}</code>', 1, 0, '0000-00-00 00:00:00', 7, ''),
(21, 7, '[i]{text}[/i]', '<em>{text}</em>', 1, 0, '0000-00-00 00:00:00', 6, ''),
(22, 7, '[b]{text}[/b]', '<strong>{text}</strong>', 1, 0, '0000-00-00 00:00:00', 5, ''),
(23, 7, '[h4]{text}[/h4]', '<h4>{text}</h4>', 1, 0, '0000-00-00 00:00:00', 4, ''),
(24, 7, '[h1]{text}[/h1]', '<h1>{text}</h1>', 1, 0, '0000-00-00 00:00:00', 1, ''),
(25, 7, '[h2]{text}[/h2]', '<h2>{text}</h2>', 1, 0, '0000-00-00 00:00:00', 2, ''),
(26, 7, '[h3]{text}[/h3]', '<h3>{text}</h3>', 1, 0, '0000-00-00 00:00:00', 3, '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_sections`
--

DROP TABLE IF EXISTS `jml_sections`;
CREATE TABLE IF NOT EXISTS `jml_sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `image` text NOT NULL,
  `scope` varchar(50) NOT NULL DEFAULT '',
  `image_position` varchar(30) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `count` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_scope` (`scope`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jml_sections`
--

INSERT INTO `jml_sections` (`id`, `title`, `name`, `alias`, `image`, `scope`, `image_position`, `description`, `published`, `checked_out`, `checked_out_time`, `ordering`, `access`, `count`, `params`) VALUES
(1, 'Categories', '', '', '', 'content', '', '', 1, 0, '0000-00-00 00:00:00', 1, 0, 4, '');

-- --------------------------------------------------------

--
-- Table structure for table `jml_session`
--

DROP TABLE IF EXISTS `jml_session`;
CREATE TABLE IF NOT EXISTS `jml_session` (
  `username` varchar(150) DEFAULT '',
  `time` varchar(14) DEFAULT '',
  `session_id` varchar(200) NOT NULL DEFAULT '0',
  `guest` tinyint(4) DEFAULT '1',
  `userid` int(11) DEFAULT '0',
  `usertype` varchar(50) DEFAULT '',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `data` longtext,
  PRIMARY KEY (`session_id`(64)),
  KEY `whosonline` (`guest`,`usertype`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jml_session`
--

INSERT INTO `jml_session` (`username`, `time`, `session_id`, `guest`, `userid`, `usertype`, `gid`, `client_id`, `data`) VALUES
('', '1314944365', '83d0atrm2gj4a5hk72ac5svb85', 1, 0, '', 0, 0, '__default|a:8:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1314944364;s:18:"session.timer.last";i:1314944364;s:17:"session.timer.now";i:1314944364;s:22:"session.client.browser";s:83:"Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:2:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}s:10:"com_search";a:1:{s:4:"data";O:8:"stdClass":1:{s:5:"limit";i:4;}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:73:"/home1/apostol8/public_html/youth/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:23:"gantry-current-template";s:18:"rt_crystalline_j15";}'),
('', '1314946651', 'epfqr0al9tnjh1021v6q60l3k4', 1, 0, '', 0, 0, '__default|a:8:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1314946651;s:18:"session.timer.last";i:1314946651;s:17:"session.timer.now";i:1314946651;s:22:"session.client.browser";s:83:"Mozilla/5.0 (compatible; Yahoo! Slurp; http://help.yahoo.com/help/us/ysearch/slurp)";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:2:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}s:10:"com_search";a:1:{s:4:"data";O:8:"stdClass":1:{s:5:"limit";i:4;}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:73:"/home1/apostol8/public_html/youth/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:23:"gantry-current-template";s:18:"rt_crystalline_j15";}'),
('', '1314947576', '1p6occefjdut2btqdats8rpoo2', 1, 0, '', 0, 0, '__default|a:8:{s:15:"session.counter";i:1;s:19:"session.timer.start";i:1314947576;s:18:"session.timer.last";i:1314947576;s:17:"session.timer.now";i:1314947576;s:22:"session.client.browser";s:84:"Mozilla/5.0 (Windows; U; Windows NT 5.1; en-GB; rv:1.9.2) Gecko/20100115 Firefox/3.6";s:8:"registry";O:9:"JRegistry":3:{s:17:"_defaultNameSpace";s:7:"session";s:9:"_registry";a:2:{s:7:"session";a:1:{s:4:"data";O:8:"stdClass":0:{}}s:10:"com_search";a:1:{s:4:"data";O:8:"stdClass":1:{s:5:"limit";i:4;}}}s:7:"_errors";a:0:{}}s:4:"user";O:5:"JUser":19:{s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:8:"usertype";N;s:5:"block";N;s:9:"sendEmail";i:0;s:3:"gid";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:3:"aid";i:0;s:5:"guest";i:1;s:7:"_params";O:10:"JParameter":7:{s:4:"_raw";s:0:"";s:4:"_xml";N;s:9:"_elements";a:0:{}s:12:"_elementPath";a:1:{i:0;s:73:"/home1/apostol8/public_html/youth/libraries/joomla/html/parameter/element";}s:17:"_defaultNameSpace";s:8:"_default";s:9:"_registry";a:1:{s:8:"_default";a:1:{s:4:"data";O:8:"stdClass":0:{}}}s:7:"_errors";a:0:{}}s:9:"_errorMsg";N;s:7:"_errors";a:0:{}}s:23:"gantry-current-template";s:18:"rt_crystalline_j15";}');

-- --------------------------------------------------------

--
-- Table structure for table `jml_stats_agents`
--

DROP TABLE IF EXISTS `jml_stats_agents`;
CREATE TABLE IF NOT EXISTS `jml_stats_agents` (
  `agent` varchar(255) NOT NULL DEFAULT '',
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `hits` int(11) unsigned NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jml_templates_menu`
--

DROP TABLE IF EXISTS `jml_templates_menu`;
CREATE TABLE IF NOT EXISTS `jml_templates_menu` (
  `template` varchar(255) NOT NULL DEFAULT '',
  `menuid` int(11) NOT NULL DEFAULT '0',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`menuid`,`client_id`,`template`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jml_templates_menu`
--

INSERT INTO `jml_templates_menu` (`template`, `menuid`, `client_id`) VALUES
('rt_crystalline_j15', 0, 0),
('khepri', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jml_tor_power_calendar`
--

DROP TABLE IF EXISTS `jml_tor_power_calendar`;
CREATE TABLE IF NOT EXISTS `jml_tor_power_calendar` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `begin_date` varchar(25) NOT NULL,
  `end_date` varchar(25) NOT NULL,
  `recurring_limit` varchar(25) NOT NULL,
  `begin_time` varchar(25) NOT NULL,
  `end_time` varchar(25) NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `generation` varchar(25) DEFAULT NULL,
  `recurring` varchar(25) DEFAULT NULL,
  `horo_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_users`
--

DROP TABLE IF EXISTS `jml_users`;
CREATE TABLE IF NOT EXISTS `jml_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `usertype` varchar(25) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `gid` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `usertype` (`usertype`),
  KEY `idx_name` (`name`),
  KEY `gid_block` (`gid`,`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=68 ;

--
-- Dumping data for table `jml_users`
--

INSERT INTO `jml_users` (`id`, `name`, `username`, `email`, `password`, `usertype`, `block`, `sendEmail`, `gid`, `registerDate`, `lastvisitDate`, `activation`, `params`) VALUES
(62, 'Administrator', 'y_admin', 'peteredohoeket@yahoo.com', 'd706df3d666e9d723fa8f5b95f52747e:7HCGvZugZjDG7YjI47rxjw9d2wJvgyvp', 'Super Administrator', 0, 1, 25, '2010-07-27 18:50:45', '2011-02-17 09:39:14', '', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n'),
(64, 'Olos Irenoa', 'Olos.irenoa', 'bright_shiningstar@msn.com', 'a98932399af760faa0f5c5139ddfabc3:1AcSyI8c5I1a9SdufKRp2DrFWoNQckYG', 'Super Administrator', 0, 0, 25, '2010-09-06 09:13:19', '2010-09-06 12:54:01', '', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n'),
(66, 'Timothy Okusanya', 'tim.okusanya', 'timokusanya@yahoo.com', 'b2ed4e31fb2832640a6de281d395f55c:gFbuAPkoQObMnW2EaCjUL8RDicPkSj89', 'Manager', 1, 0, 23, '2010-09-06 09:14:42', '0000-00-00 00:00:00', '', 'admin_language=\nlanguage=\neditor=\nhelpsite=\ntimezone=0\n\n');

-- --------------------------------------------------------

--
-- Table structure for table `jml_weblinks`
--

DROP TABLE IF EXISTS `jml_weblinks`;
CREATE TABLE IF NOT EXISTS `jml_weblinks` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `sid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(11) NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `archived` tinyint(1) NOT NULL DEFAULT '0',
  `approved` tinyint(1) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `catid` (`catid`,`published`,`archived`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `jml_xmap`
--

DROP TABLE IF EXISTS `jml_xmap`;
CREATE TABLE IF NOT EXISTS `jml_xmap` (
  `name` varchar(30) NOT NULL,
  `value` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jml_xmap`
--

INSERT INTO `jml_xmap` (`name`, `value`) VALUES
('version', '1.1'),
('classname', 'sitemap'),
('expand_category', '1'),
('expand_section', '1'),
('show_menutitle', '1'),
('columns', '1'),
('exlinks', '1'),
('ext_image', 'img_grey.gif'),
('exclmenus', ''),
('includelink', '1'),
('sitemap_default', '1'),
('exclude_css', '0'),
('exclude_xsl', '0');

-- --------------------------------------------------------

--
-- Table structure for table `jml_xmap_ext`
--

DROP TABLE IF EXISTS `jml_xmap_ext`;
CREATE TABLE IF NOT EXISTS `jml_xmap_ext` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `extension` varchar(100) NOT NULL,
  `published` int(1) DEFAULT '0',
  `params` text,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=29 ;

--
-- Dumping data for table `jml_xmap_ext`
--

INSERT INTO `jml_xmap_ext` (`id`, `extension`, `published`, `params`) VALUES
(1, 'com_agora', 1, '-1{include_forums=1\ninclude_topics=1\nmax_topics=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nforum_priority=-1\nforum_changefreq=-1\ntopic_priority=-1\ntopic_changefreq=-1\n}'),
(2, 'com_contact', 1, '-1{include_contacts=1\nmax_contacts=\ncat_priority=-1\ncat_changefreq=-1\ncontact_priority=-1\ncontact_changefreq=-1\n}'),
(3, 'com_content', 1, '-1{expand_categories=1\nexpand_sections=1\narticles_order=menu\nadd_pagebreaks=1\nadd_images=0\nmax_images=1000\nshow_unauth=0\nmax_art=0\nmax_art_age=0\ncat_priority=-1\ncat_changefreq=-1\nart_priority=-1\nart_changefreq=-1\nkeywords=1\n}'),
(4, 'com_eventlist', 1, '-1{include_events=1\nmax_events=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nfile_priority=-1\nfile_changefreq=-1\n}'),
(5, 'com_g2bridge', 1, '-1{include_items=2\ncat_priority=-1\ncat_changefreq=-1\nitem_priority=-1\nitem_changefreq=-1\n}'),
(6, 'com_glossary', 1, ''),
(7, 'com_hotproperty', 1, '-1{include_properties=1\ninclude_companies=1\ninclude_agents=1\nproperties_text=Properties\ncompanies_text=Companies\nagents_text=Agents\nmax_properties=\ntype_priority=-1\ntype_changefreq=-1\nproperty_priority=-1\nproperty_changefreq=-1\ncompany_priority=-1\ncompany_changefreq=-1\nagent_priority=-1\nagent_changefreq=-1\n}'),
(8, 'com_jcalpro', 1, '-1{include_events=1\ncat_priority=-1\ncat_changefreq=-1\nevent_priority=-1\nevent_changefreq=-1\n}'),
(9, 'com_jdownloads', 1, '-1{include_files=1\nmax_files=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nfile_priority=-1\nfile_changefreq=-1\n}'),
(10, 'com_jevents', 1, '-1{include_events=1\nmax_events=\ncat_priority=0.5\ncat_changefreq=weekly\nevent_priority=0.5\nevent_changefreq=weekly\n}'),
(11, 'com_jmovies', 1, '-1{include_movies=1\nmax_movies=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nfile_priority=-1\nfile_changefreq=-1\n}'),
(12, 'com_jomres', 1, '-1{priority=0.5\nchangefreq=weekly\n}'),
(13, 'com_joomdoc', 1, '-1{include_docs=1\ndoc_task=\ncat_priority=0.5\ncat_changefreq=weekly\ndoc_priority=0.5\ndoc_changefreq=weekly\n}'),
(14, 'com_joomgallery', 1, '-1{include_pictures=1\nmax_pictures=\ncat_priority=-1\ncat_changefreq=-1\npictures_priority=-1\npictures_changefreq=-1\n}'),
(15, 'com_kb', 1, '-1{include_articles=1\ninclude_feeds=1\nmax_articles=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nfile_priority=-1\nfile_changefreq=-1\n}'),
(16, 'com_kunena', 1, '-1{include_topics=1\nmax_topics=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\ntopic_priority=-1\ntopic_changefreq=-1\n}'),
(17, 'com_mtree', 1, '-1{cats_order=cat_name\ninclude_links=1\nlinks_order=ordering\nmax_links=\nmax_age=\ncat_priority=0.5\ncat_changefreq=weekly\nlink_priority=0.5\nlink_changefreq=weekly\n}'),
(18, 'com_myblog', 1, '-1{include_bloggers=1\ninclude_tag_clouds=1\ninclude_feed=2\ninclude_archives=2\nnumber_of_bloggers=8\ninclude_blogger_posts=1\nnumber_of_post_per_blogger=32\ntext_bloggers=Bloggers\nblogger_priority=-1\nblogger_changefreq=-1\nfeed_priority=-1\nfeed_changefreq=-1\nentry_priority=-1\nentry_changefreq=-1\ncats_priority=-1\ncats_changefreq=-1\narc_priority=-1\narc_changefreq=-1\ntag_priority=-1\ntag_changefreq=-1\n}'),
(19, 'com_rapidrecipe', 1, '-1{cats_order=cat_name\ninclude_links=1\nlinks_order=ordering\nmax_links=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nrecipe_priority=-1\nrecipe_changefreq=-1\n}'),
(20, 'com_remository', 1, '-1{include_files=1\nmax_files=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nfile_priority=-1\nfile_changefreq=-1\n}'),
(21, 'com_resource', 1, '-1{include_articles=1\nmax_articles=\ncat_priority=-1\ncat_changefreq=-1\narticle_priority=-1\narticle_changefreq=-1\n}'),
(22, 'com_rokdownloads', 1, '-1{include_files=1\nmax_files=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nfile_priority=-1\nfile_changefreq=-1\n}'),
(23, 'com_rsgallery2', 1, '-1{include_images=1\nmax_images=\nmax_age=\nimages_order=orderding\ncat_priority=0.5\ncat_changefreq=weekly\nimage_priority=0.5\nimage_changefreq=weekly\n}'),
(24, 'com_sectionex', 1, '-1{expand_categories=1\nexpand_sections=1\nshow_unauth=0\ncat_priority=-1\ncat_changefreq=-1\nart_priority=-1\nart_changefreq=-1\n}'),
(25, 'com_cmsshopbuilder', 1, '-1{include_items=1\nmax_items=\nmax_age=\ncat_priority=-1\ncat_changefreq=-1\nitem_priority=-1\nitem_changefreq=-1\n}'),
(26, 'com_sobi2', 1, '-1{include_entries=1\nmax_entries=\nmax_age=\nentries_order=a.ordering\nentries_orderdir=DESC\ncat_priority=-1\ncat_changefreq=weekly\nentry_priority=-1\nentry_changefreq=weekly\n}'),
(27, 'com_virtuemart', 1, '-1{include_products=1\ninclude_product_images=0\nproduct_image_license_url=\ncat_priority=0.5\ncat_changefreq=weekly\nprod_priority=0.5\nprod_changefreq=weekly\n}'),
(28, 'com_weblinks', 1, '-1{include_links=1\nmax_links=\ncat_priority=-1\ncat_changefreq=-1\nlink_priority=-1\nlink_changefreq=-1\n}');

-- --------------------------------------------------------

--
-- Table structure for table `jml_xmap_items`
--

DROP TABLE IF EXISTS `jml_xmap_items`;
CREATE TABLE IF NOT EXISTS `jml_xmap_items` (
  `uid` varchar(100) NOT NULL,
  `itemid` int(11) NOT NULL,
  `view` varchar(10) NOT NULL,
  `sitemap_id` int(11) NOT NULL,
  `properties` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`uid`,`itemid`,`view`,`sitemap_id`),
  KEY `uid` (`uid`,`itemid`),
  KEY `view` (`view`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jml_xmap_sitemap`
--

DROP TABLE IF EXISTS `jml_xmap_sitemap`;
CREATE TABLE IF NOT EXISTS `jml_xmap_sitemap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `expand_category` int(11) DEFAULT NULL,
  `expand_section` int(11) DEFAULT NULL,
  `show_menutitle` int(11) DEFAULT NULL,
  `columns` int(11) DEFAULT NULL,
  `exlinks` int(11) DEFAULT NULL,
  `ext_image` varchar(255) DEFAULT NULL,
  `menus` text,
  `exclmenus` varchar(255) DEFAULT NULL,
  `includelink` int(11) DEFAULT NULL,
  `usecache` int(11) DEFAULT NULL,
  `cachelifetime` int(11) DEFAULT NULL,
  `classname` varchar(255) DEFAULT NULL,
  `count_xml` int(11) DEFAULT NULL,
  `count_html` int(11) DEFAULT NULL,
  `views_xml` int(11) DEFAULT NULL,
  `views_html` int(11) DEFAULT NULL,
  `lastvisit_xml` int(11) DEFAULT NULL,
  `lastvisit_html` int(11) DEFAULT NULL,
  `excluded_items` text,
  `compress_xml` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jml_xmap_sitemap`
--

INSERT INTO `jml_xmap_sitemap` (`id`, `name`, `expand_category`, `expand_section`, `show_menutitle`, `columns`, `exlinks`, `ext_image`, `menus`, `exclmenus`, `includelink`, `usecache`, `cachelifetime`, `classname`, `count_xml`, `count_html`, `views_xml`, `views_html`, `lastvisit_xml`, `lastvisit_html`, `excluded_items`, `compress_xml`) VALUES
(1, 'New Sitemap', 1, 1, 1, 1, 1, 'img_grey.gif', 'mainmenu,0,1,1,0.5,daily', '', 1, 0, 15, 'xmap', 0, 0, 0, 0, 0, 0, '', 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
