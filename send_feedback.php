<?php
	session_start();
	
	require_once("utils/config.php");
	require_once("utils/dbclass.php");
	require_once("utils/functions.php");
	$objDB = new MySQLCN();
	
	$comment = $_REQUEST['c'];
	$name = $_REQUEST['n'];	
	$email = $_REQUEST['e'];	
			
	if($email != ""){
		$flag = checkEmail($email);
		if(!$flag){
			echo "INVALIDEMAIL";
		}	
	}
	
	if($flag){
		if(!empty($comment) && !empty($email) && !empty($name)){		
			$SQL = "INSERT feedback SET ";
			$SQL .= "name='".addslashes($name)."', ";		
			$SQL .= 'email="'.addslashes($email).'",';	
			$SQL .= 'comment="'.addslashes($comment).'",';	
			$SQL .= "created='".date('Y-m-d H:i:s')."'";
			$objDB->sql_query($SQL);
					
			$msg = '';
			$msg .= '<table width="98%" class="maintext" border="0" align="center" cellpadding="0" cellspacing="0">
	    <tr>
	        <td colspan="2" align="left" class="maintext" style="padding-left:10px;"><strong class="lefttitle">Following are the feedback details sent by the user.</strong></td>
	    </tr>
	    <tr>
	        <td colspan="2">&nbsp;</td>
	    </tr>
		<tr>
	        <td colspan="2" align="left" class="maintext" style="padding-left:10px;"><strong class="lefttitle">Name</strong>: '.$name.'</td>
	    </tr>
	    <tr>
	        <td colspan="2">&nbsp;</td>
	    </tr>
	    <tr>
	        <td colspan="2" align="left" class="maintext" style="padding-left:10px;"><strong>Email</strong>: '.$email.'</td>
	    </tr>
	    <tr>
	        <td colspan="2">&nbsp;</td>
	    </tr>
	    <tr>
	        <td colspan="2" align="left" class="maintext" style="padding-left:10px;"><strong>Comment</strong>: '.$comment.'</td>
	    </tr>
	    <tr>
	        <td colspan="2">&nbsp;</td>
	    </tr>
	    <tr>
	        <td align="left" class="maintext" style="padding-left:20px;">&copy; The Apostolic Faith Church Limited. All Rights Reserved. </td>
	    </tr>
	    <tr>
	        <td height="10"></td>
	    </tr>
	</table>
	';
			
			$Template="mail_templates/general.html";
			
			$TemplateVars=array('Message'=>$msg);
								
			
			$To = EMAIL_ADDRESS;
			//$To = "sejal@zipprosystem.co.uk";
			$Subject = "Feedback from the ".$name;
			$From = $email;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			//echo $flag;
			//exit;
			echo "OK";	
		}
		else{
			echo "FAIL";
		}
	}
?>