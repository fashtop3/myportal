<?php
	session_start();
	
	require_once('../utils/config.php');	
	require_once('../utils/dbClass.php');
	require_once('../utils/functions.php');
	require_once('../utils/paging.php');
	$objDB = new MySQLCN;

	$userplc = DEFAULT_PLACE;
	if($_REQUEST['p']=='')
		$p = LoadVariable('p',"home");	
	else
		$p = LoadVariable('p',"");	
	$_SESSION['page'] = $_SERVER['REQUEST_URI'];

 ?>
 <?php
	$notice = FetchData('event',array(),' order by event_date');
?>
<?php 							
	$mon = '';
	
	for($i=0;$i<count($notice);$i++) {
		if($notice[$i]['to_date']!='0000-00-00') {
			$endDate =	$notice[$i]['to_date'];
		}else {
			$endDate =	$notice[$i]['event_date'];
		}
			
		$mon .= "{
					title: '<img src=\"../images/arrow_001.gif\" alt=\"\" height=5 /> <span style=\"font-size:10px;\"><a href=\"../index.php?p=event_detail&id=".$notice[$i]['id']."\" target=\"_parent\" class=\"orange_12_link\"><span class=\"wordwrap\">".substr(addslashes($notice[$i]['title']),0,10)."...</span></a></span>',
					start: '".date("D M d Y", strtotime($notice[$i]['event_date']))."'";
		$mon .= ", end: '".date("D M d Y", strtotime($endDate))."'";
//		$mon .= ",					allDay: false";
		$mon .= ",location: '".addslashes($notice[$i]['title'])."<br>".stripslashes($notice[$i]['address'])."'";
		$mon .= "},"; 
	}
	
	//Sun Oct 24 2010
	$mon = substr($mon,0,strlen($mon)-1);		
//	echo $mon;
	//var_dump($mon);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Demo - MooTools Events Calendar</title>
<meta name="description" content="Try a demo of the calendar for yourself"/>
<meta name="keywords" content="mootools, calendar, events, web-based, javascript, demo"/>
<script type="text/javascript" src="mootools-1.2.3-core-yc.js"></script>
<script type="text/javascript" src="mootools-1.2.3.1-more.js"></script>
<script type="text/javascript" src="mooECal.js"></script>
<script type="text/javascript">
	window.addEvent('domready',function(){
		new Calendar({
			calContainer:'calBody',
			newDate:new Date(),
			cEvents: [
				<?php echo $mon ?>
			]	
		});
	})
	</script>

<link rel="stylesheet" type="text/css" id="calStyle" href="mooECal.css" />
</head>
<body>
<table width="735" border="0"><tr><td align="left">
<div class="main">    
  <!--div class="styleSizes"> <a href="change style" title="Use mooECal.css" onclick="$('calStyle').set('href','mooECal.css'); return false;">Normal</a> <a href="change style" title="Use mooECalSmall.css" onclick="$('calStyle').set('href','mooECalSmall.css'); return false;">Small</a> <a href="change style" title="Use mooECalLarge.css" onclick="$('calStyle').set('href','mooECalLarge.css'); return false;">Large</a> </div-->
  <div id="calBody"></div>
</div>
</td></tr></table>
</body>
</html>
