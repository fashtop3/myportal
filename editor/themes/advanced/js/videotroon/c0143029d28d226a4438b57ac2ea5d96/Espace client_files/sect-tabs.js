var sect_tabs =
{
	tabsId: "",
	tabsElem: "",
	tabsLength: "",
	tabsCurrent: "",
	contentId: "",
	contentElem: "",
	contentLength: "",
	previousTab: "",
	
	init: function(tId,tElem,cId,cElem, cCurrent)
	{
		sect_tabs.tabsCurrent = cCurrent;
		
		document.getElementById("content-main-hd").className = "";
		
		this.tabsElem = document.getElementById(tId).getElementsByTagName(tElem);
		this.tabsLength = this.tabsElem.length;
		
		this.contentElem = document.getElementById(cId).childNodes;
		this.contentLength = this.contentElem.length;

		sect_tabs.reset();//reset all to default
		
		//set 1st elements to default
		for(var c=0; c<this.tabsLength; c++)
		{
			if(this.tabsElem[c].nodeType == 1)
			{
				this.tabsElem[c].className = sect_tabs.tabsCurrent;
				break;
			}
		}

		for(var d=0; d<this.contentLength; d++)
		{
			if(this.contentElem[d].tagName == cElem.toUpperCase())
			{
				this.contentElem[d].style.display = "block";
				break;
			}
		}
		
		//onclick behaviors
		for(var i=0; i<this.tabsLength; i++)
		{
			if(this.tabsElem[i].nodeType == 1)
			{
				this.tabsElem[i].onclick = function()
				{
					sect_tabs.show(this.id);
					
					//**
					//** Pour la page de profile on g�re les messages d'erreurs selon les tabs
					//**
					if (document.forms['f-profile'] && sect_tabs.previousTab!=this.id) {
						
//						//on efface les boites jaunes  d'erreur autour des champs 
//						var obj = document.getElementsByClassName("msg-bg-yellow-int");
//						for (i = 0 ; i<obj.length ; i++) {
//							$(obj[i]).removeClass("msg-bg-yellow-int");
//						}
//						//on efface les messages d'erreur
//						var obj = document.getElementsByClassName("msg-error");
//						for (i = 0 ; i<obj.length ; i++) {
//							$(obj[i]).empty();
//						}
						
						sect_tabs.previousTab = this.id;
						//on efface le message d'erreur g�n�ral
						if (document.getElementById("message_erreur")) {
							document.getElementById("message_erreur").style.display = "none";
						} 
					}
					
				}
			}					
		}	
	},
	
	reset: function()
	{
		var tabsCounter = 0;
		var contentCounter = 0;

		for(var m=0; m<this.tabsLength; m++)
		{
			if(this.tabsElem[m].nodeType == 1)
			{
				this.tabsElem[m].id = "tabId_"+tabsCounter;
				this.tabsElem[m].className = "";
				tabsCounter++;
			}
		}
		
		for(var n=0; n<this.contentLength; n++)
		{
			if(this.contentElem[n].nodeType == 1)
			{
				this.contentElem[n].id = "contentId_"+contentCounter;
				this.contentElem[n].style.display = "none";
				contentCounter++;
			}
		}
	},
	
	show: function(eId)
	{
		sect_tabs.reset();

		var eIndex = parseInt(eId.substring(eId.indexOf("_") + 1));

		document.getElementById("tabId_"+eIndex).className = sect_tabs.tabsCurrent;
		// set hidden field to current tab id
		if(document.getElementById("showOnglet") != null){
			document.getElementById("showOnglet").value = eIndex;
		}
		//$("#contentId_"+eIndex).fadeIn("slow");
		$("#contentId_"+eIndex).show();
	}
};

function radioClicked(clicked)
{
	// id du div a afficher
	var id = "chart-" + clicked.id;
	
	var divs = document.getElementsByTagName("div");
	for (x in divs)
	{
		if (divs[x])
		if (divs[x].className != null)
		if (divs[x].className == "chart-container")
		{
			// c'est le chart qu'on desire afficher ?
			if (divs[x].id == id)
				// on affiche
				divs[x].style.display = "block";
			else
				// on cache
				divs[x].style.display = "none";
		}
	}
}

function selectForfait(select)
{
	// select value should be id of span to display
	var id = document.getElementById(select.value);

	// hide all candidate spans
	var spans = document.getElementsByTagName("span");
	for (x in spans)
	{
		if (spans[x])
		if (spans[x].id != null)
		if (spans[x].id.indexOf("span.forfait.") == 0)
		{
			if (spans[x].style)
				spans[x].style.display = "none";
		}
	}
	
	// display selected span
	if (id)
	if (id.style){
		id.style.display = "block";		
		var selectAnnexe = select.value+'.annexe';		
		idAnnexe = document.getElementById(selectAnnexe);		
		if(idAnnexe){
			idAnnexe.style.display = "block";
		}
	}
}


function graphe_ou_tableau(id)
{
    var enableGraphics = (id == "graphes");
    
	// hide or show "graphes" div
	var divGraphes = document.getElementById("graphe-periode-facturation");
	if (divGraphes)
		divGraphes.style.display = (enableGraphics ? "block" : "none");
	
	var divGraphesTel = document.getElementById("graphes-telephone");
	if (divGraphesTel)
		divGraphesTel.style.display = (enableGraphics? "block" : "none");
	
	// hide or show "tableau-detaille" div
	var divTableau = document.getElementById("tableau-detaille");
	if (divTableau)
		divTableau.style.display = (enableGraphics ? "none" : "block");

	var divTableauTel = document.getElementById("tableau-tel-detaille");
	if (divTableauTel)
		divTableauTel.style.display = (enableGraphics ? "none" : "block");

	// update hidden field to preserve current state after posting
	var hidden = document.getElementById("show-details");
	if (hidden)
		hidden.value = (enableGraphics ? "false" : "true");
}

function changeUniteMesure( currentUnit ){	
	var fInternet = document.getElementById('f-internet');
	ratio = (currentUnit.substr(0,1) == 'M')?1024:1024*1024;
	inputs = document.getElementsByName("cable.conversionRatio");
	inputs[0].value = ratio;	
	submitTab('tabId_0');
}

function submitTab( tab ){	
	var fInternet = document.getElementById('f-internet');
	fInternet.tabId.value = tab;
	fInternet.submit();		
}


