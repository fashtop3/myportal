/* 
Title:      videotron.remoteLoad.js
Author:     nurun.com
Updated:    September 2008

Content:	- VIDEOTRON.widget.Overlay
			- VIDEOTRON.widget.remoteLoad
			- VIDEOTRON.widget.Dialog 

*/


if (typeof VIDEOTRON === "undefined" || !VIDEOTRON) {
    var VIDEOTRON = {};
}
if (typeof VIDEOTRON.widget === "undefined" || !VIDEOTRON.widget) {
    VIDEOTRON.widget = {};
}

// ------[ WIDGET OVERLAY ]------------------------------------------------- //
/**
 * Overlay cr�e un panel YUI avec un contenu li� � un objet (lien/form/etc.)
 *
 * @param {Object}  UserConfig  Un objet contenant les valeurs initials pour les
 								configs initials du panel
 * @param {String | HTMLElement} target Id | Element for the overlay panel
 * @param {Objetc remoteLoad} rl - R�f�rence � l'objet remoteLoad qui demande la construction
 */
 

VIDEOTRON.widget.Overlay = function(userConfig, hookedElement, rl) {

	/**********
		Private member
	**********/
		var that = this; //R�f�rence � l'object lui m�me

		this.submittedId = false;
		
		// Cr�e le Namespace de travail (scope global � tous les overlays)
		if(!YAHOO.custom || !YAHOO.custom.overlay) {
			this.scope = YAHOO.namespace('custom.overlay');
		} else {
			this.scope = YAHOO.custom.overlay;
		}

		// Ajoute le Overlay Manager s'il n'existe pas (scope global � tous les overlay) 
		if(!this.scope.manager) {
			this.scope.manager = new YAHOO.widget.OverlayManager();
		}
		
		// Ajoute automatiquement le script pour les objets flash
		// IE ne supporte pas bien le onload sur les script donc le script doit �tre charg� le plus t�t possible
		if(typeof(swfobject) == 'undefined') {
			var script = document.createElement("script");
			script.src = "/resources/legacy/js/swfobject.js";
			script.type = "text/javascript";
			var hd = document.getElementsByTagName("head")[0];
			hd.appendChild(script);
		}

		// Cr�e un div qui sert de wrapper au diff�rent overlay du scope
		if(!this.scope.wrapper) {
			this.scope.wrapper = document.createElement('div');
			this.scope.wrapper.id='custom-overlay-wrapper';
			this.scope.wrapper.style.position = 'absolute';
			this.scope.wrapper.style.zIndex = '102';
			this.scope.wrapper.style.left = '0px';
			this.scope.wrapper.style.top = '0px';
			document.body.appendChild(this.scope.wrapper);
		}
		
		this.hookedElement = hookedElement;
		
		this._isLoaded = false; // indique si le panel a �t� charg�
		
		this._headerContent = ''; // Contenue par d�faut du header du overlay
		this._bodyConent = ''; // Contenue par d�faut du body du overlay
		this._footerContent = ''; // Contenue par d�faut du footer du overlay
		
		// Configuration initial par d�faut
		this.cfg = {
			/**
			 * ID {String} Identifiant du container du panel
			 * Optionnel : Identifiant par d�faut du panel. Le Id est g�n�r� � partir du targetId ou du ID du hookedElement lors du rendering du panel
			 * D�faut : remoteLoad
			 **/
	        id: "remoteLoad",

			/**
			 * renderTarget {Object} Noeud du DOM qui sert de point d'origine pour le rendering du panel et la recherche des �l�ments
			 * Optionnel : Le noeud sert de point d'origine, donc d�finir sur le container parent pour les sous-panels
			 * D�faut : document.body
			 **/
	        renderTarget: document.body,
			
			/**
			 * title {String} Titre du panel
			 * Optionnel : Peut �tre d�fini avec la config meta overlay-title si le contentType est d�fini � "remote"
			 * D�faut : ""
			 **/
			title: "",
			
			/**
			 * width {Integer} largeur total du panel
			 * Optionnel : Peut �tre d�fini avec la config meta overlay-width si le contentType est d�fini � "remote"
			 * D�faut : 50em - �viter les mesures en pourcentage (%).
			 **/
			width: "50em",
			
			/**
			 * height {Integer} hauteur total du panel
			 * Optionnel : Peut �tre d�fini avec la config meta overlay-height si le contentType est d�fini � "remote"
			 * D�faut : auto - �viter les mesures en pourcentage (%).
			 **/
			height: 'auto',
			
			/**
			 * fixedCenter {Boolean} Indique si le panel doit �tre centr�
			 * Optionnel : Positionne le panel au centre de l'espace visible � l'�cran et le conserve centr� m�me si l'utilisateur scroll le contenue
			 * 			   Cette propri�t� est automatiquement d�fini � false pour les panel qui affiche des objets flash
			 * D�faut : true
			 **/
			fixedCenter: true,
			
			/**
			 * dragable {Boolean} Indique si le panel peut �tre d�plac� par l'utilisateur
			 * Optionnel : Permet � l'utilisateur de d�placer la fen�tre avec la souris
			 * D�faut : false
			 **/
			dragable: false,
			
			/**
			 * showCloseX {Boolean} Permettre la fermeture du panel
			 * Optionnel : Fait r�f�rence � la propri�t� 'close' du panel.
			 * D�faut : true
			 **/
			showCloseX: true, 
			
			/**
			 * modal {Boolean} Indique si le panel est modal ou pas
			 * Optionnel : Ajoute un mask qui couvre tout le contenue et g�re les click pour pr�venir tout action sur le contenu 
			 *             � l'ext�rieur du panel lorsqu'il est affich�
			 * D�faut : true
			 **/
			modal: true,
			
			/**
			 * addCloseLink {Boolean} Permet de g�n�rer un lien (bouton) "fermmer" dans le footer du panel
			 * Optionnel : Cette propri�t� d�pend directement de la m�thode getFooterContent().
			 * D�faut : false
			 **/
			addCloseLink: false,
			
			/**
			 * useTooltips {Boolean} Indique que le panel utilise des fonctionnalit� du YAHOO.widget.Tooltip
			 * Optionnel : Requiert la pr�sence des scripts n�cessaire au fonctionnement du YAHOO.widget.Tooltip.
			 *             Peut �tre d�fini avec la config meta overlay-useTooltips si le contentType est d�fini � "remote".
			 *			   Cette valeur sert uniquement d'indiquateur. Si le contentType est d�fini � "remote", la page charg�
			 *			   doit contenir les scripts n�cessaire � l'initialisation des Tooltips.
			 * D�faut : false
			 **/
			useTooltips: false,
			
			/**
			 * useColapsedList {Boolean} Indique que le panel utilise des fonctionnalit� de list Collapse
			 * Optionnel : Cette fonctionnalit� a �t� ajout� dans le cadre du projet refonte Vid�otron.
			 *			   Requiert la pr�sence des scripts n�cessaire au fonctionnement des liste Collapse (voir fonction collapseUncollapseDataList() du projet refonte Vid�otron)
			 *             Peut �tre d�fini avec la config meta overlay-useColapsedList si le contentType est d�fini � "remote".
			 * D�faut : false
			 **/
			useColapsedList: false,

			/**
			 * useSWFObject {Boolean} Indique que le panel utilise des fonctionnalit� du objectswf.js
			 * Optionnel : Requiert la pr�sence des scripts n�cessaire au fonctionnement du objectswf.
			 *             Peut �tre d�fini avec la config meta overlay-useSWFObject si le contentType est d�fini � "remote".
			 *			   Cette valeur sert uniquement d'indiquateur. Si le contentType est d�fini � "remote", la page charg�
			 *			   doit contenir les scripts n�cessaire � l'initialisation des swf. La m�thode _testSWFScript convertie
			 *			   automatiquement les calls embedSWF en createSWF. 
			 *			   Voir la m�thode _testSWFScript pour plus de d�tails.
			 * D�faut : false
			 **/
			useSWFObject: false,
			
			/**
			 * contentType {String | Function} Indique le mode de fonctionnement du chargement des donn�es du panel.
			 * Optionnel : {String} 'remote' - Utilise l'attribut href d'un lien ou l'attribut action d'un form pour charger le 
			 *								   contenue avec un YUI.connection (AJAX)
			 *			   {Function} - Appelle une fonction pour r�cup�rer le contenue du overlay. La fonction doit retourner un objet 
			 *							strucutur� {head: 'head content', body: 'body content', foot: 'footer content'}
			 * D�faut : 'remote'
			 **/
			contentType: 'remote',
			
			/**
			 * useMetaConfig {Boolean} Indique que des metas sont d�finie dans le document pour configurer les diff�rentes options du Panel.
			 * Optionnel : Applicable uniquement avec le contentType 'remote'.
			 * Utilisation : i.e.: <meta name="overlay-title" content="titre du overlay" />
			 *				 Configuration meta disponible :
			 *			 	 overlay-title : D�fini le titre du panel
			 *			 	 overlay-width : Largeur total du panel
			 *			 	 overlay-height : Hauteur total du panel
			 *			 	 overlay-addCloseLink : Indique qu'un lien (bouton) de fermeture doit �tre ajout� automatiqment dans le footer du panel
			 *			 	 overlay-useTooltips : Le panel utilise des fonctionnalit� de tooltips, la fonction applicable doit donc �tre ex�cut�
			 *			 	 	 				   Plus vraiment utile de l'indquer dans cette version, la d�tection et l'ex�cution des script est maintenant automatis�
			 *			 	 overlay-useColapsedList : Dans le cadre du projet Vid�otron, cette fonction permet d'ex�cuter les initialisation des liste r�tractable
			 *			 	 					       Fonctionnalit� ajout� uniquement pour le projet refonte Vid�otron.
			 *			 	 overlay-useSWFObject : Le panel contient du flash et doit s'afficher proprement en conc�quence de cette option
			 *			 	 				   		Optionnel, la d�tection est automatis� au moment de parser les scripts
			 *								   		contenue avec un YUI.connection (AJAX)
			 * D�faut : true
			 **/
			useMetaConfig: true,
			
			/**
			 * useRemotePreLoader {Boolean} Indique si une image de pr�chargement doit �tre affich�.
			 * Optionnel : Affiche une image de pr�chargement sur un mask modal.
			 *			   Applicable uniquement si le contentType est d�fini � "remote"
			 * D�faut : true
			 **/
			useRemotePreLoader: true,
			
			/**
			 * remoteSourceElements {Object} Indique les sources de donn�es pour le head, le body et le footer du panel
			 * Optionnel : Applicable uniquement si le contentType est d�fini � "remote".
			 *			   L'objet contient 3 propri�t� qui indentifi les ID des noeuds � utiliser pour les infos du panel
			 *			   Structure de l'objet :
			 *			   header {String) Id du noeud � r�cup�rer dans le DOM pour le contenu du header du panel.
			 *							   * Si le test du header retourne false, le contenu par d�faut sera utilis�
			 *							   D�faut : false
			 *			   content {String) Id du noeud � r�cup�rer dans le DOM pour le contenu du body du panel.
			 *							   * Si le test du content retourne false, le contenu par d�faut sera utilis�
			 *							   D�faut : 'content'
			 *			   footer {String) Id du noeud � r�cup�rer dans le DOM pour le contenu du footer du panel.
			 *							   * Si le test du footer retourne false, le contenu par d�faut sera utilis�
			 *							   D�faut : false
			 **/
			remoteSourceElements: {
				header: false,
				content: 'content',
				footer: false
			},

			/**
			 * imgPreloader {Object} Information sur l'image utilis� pour l'affichage du preloader.
			 * Optionnel : Applicable uniquement si le contentType est d�fini � "remote".
			 *			   L'objet contient 3 propri�t�s obligatoires qui indentifi la source, le width et le height de l'image
			 *			   Structure de l'objet :
			 *			   src {String) Url de l'image � utiliser
			 *							D�faut : '/resources/legacy/img/interface/ajax-loader4.gif'
			 *			   width {Integer) Width en PX de l'image pour l'affichage
			 *							   D�faut : 62
			 *			   height {Integer) Height en PX de l'image pour l'affichage
			 *							   D�faut : 62
			 **/
			imgPreloader: {
				src: '/resources/legacy/img/interface/ajax-loader4.gif',
				width: '62',
				height: '62'
			},
			
			/**
			 * effect {Boolean false | Object} Effet � utiliser pour l'affichage du panel
			 * Optionnel : Si un effet est d�fini, elle sera appliqu� au panel pour l'affichage et la fermetur du panel
			 *			   La structure de l'objet d�pend de l'effet demand�.
			 *			   Les code d'ex�cution de l'effet doivent �tre pr�alablement charg� pour le bon fonctionnement de l'effet
			 * D�faut : {effect: YAHOO.widget.ContainerEffect.FADE, duration: 0.5}
			 **/
			effect:{effect:YAHOO.widget.ContainerEffect.FADE,duration:0.5}
			
		};

		// R�cup�re les configs initial pass� en param�tre
	    for (var key in userConfig) {
	        if (userConfig.hasOwnProperty(key)) {
	            this.cfg[key] = userConfig[key];
	        }
	    }
	
		// initialise le target pour le rendering du panel
		// Si le hookedElement est une string, l'�l�ment doit �tre r�cup�r� dans le dom
		if(typeof hookedElement == 'string') {
			var tmpEl = document.getElementById(hookedElement);
			// Si l'�l�ment n'a pas �t� trouv� le overlay ne peut pas exister!
			if(!tmpEl || typeof tmpEl == 'undefined') {
				return false;
			} else {
				hoodedElement = tmpEl;
			}
		}
		this.cfg.renderTarget = hookedElement.targetId || hookedElement.id || this.cfg.id;
		
		// D�fini le nom du panel
		this.panelName = '_panel_' + this.cfg.renderTarget;
		
		/***** OLD TRACKING
			WORS WITH ALL BROWSER EXCEPT IE < 8 -- Not tested on IE 8
		*****
		this._timerID = null;
		
		this._startHistoryTracking = function() {
			that._historyLength = window.history.length;
			that._timerID = setInterval(that._checkHistoryHash, 5000);
		};

		this._checkHistoryHash = function() {
			alert('check history hash Loc = ' + document.location.href);
			if(!/\#overlay-/i.test(document.location.href)) {
				alert('hide panel!');
				that.scope[that.panelName].hide();
				that._stopHistoryTracking();
			}
		};

		this._stopHistoryTracking = function() {
			clearInterval(that._timerID);
		};
		*****/
		
		/**
		 * Ajoute la possibilit� d'avoir un callback apr�s que le panel soit ferm�
		 * @return void
		 */
		this._afterHide = function () {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension qui doit �tre ex�cut� apr�s la fermeture du panel
				***** Le callback est d�j� impl�ment� dans la m�thode _prepareMyPanel
			*****/
			//alert('afterHide - ' + that.scope.wrapper.style.zIndex);
			//that.scope.wrapper.style.zIndex = '102';
			that.scope[that.panelName].isVisible = false;
			YAHOO.util.Dom.removeClass(document.body, "overlay-print-layout");
			YAHOO.util.Dom.removeClass('yui-panel-' + that.cfg.renderTarget, "overlay-print-layout");
			YAHOO.util.Dom.removeClass('yui-panel-' + that.cfg.renderTarget + '_c', "overlay-print-layout");
		};
		
		/**
		 * Ajoute la possibilit� d'avoir un callback juste avant que le panel se ferme
		 * @return void
		 */
		this._beforeHide = function () {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension de cette m�hode qui doit �tre ex�cut� avant la fermeture du panel
				***** Le callback est d�j� impl�ment� dans la m�thode _prepareMyPanel
			*****/
			//if(/^\#overlay-/i.test(document.location.hash)) {
			//	history.back(1);
			//}
		};

		/**
		 * Ajoute la possibilit� d'avoir un callback lorsque le panel est affich�
		 * @return void
		 */
		this._afterShow = function () {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension et de callback de cette m�hode qui doit �tre ex�cut� apr�s l'affichage du panel
			*****/
			//var loc = document.location;
			//window.location.assign('#' + that.cfg.renderTarget);
			//document.location = '#' + that.cfg.renderTarget;
			//window.navigate('#' + that.cfg.renderTarget);
			//that._startHistoryTracking();
		};
		
		/**
		 * Ajoute la possibilit� d'avoir un callback avant l'affichage du panel
		 * @return void
		 */
		this._beforeShow = function () {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension de cette m�hode qui doit �tre ex�cut� avant l'affichage du panel
				***** Le callback est d�j� impl�ment� dans la m�thode _prepareMyPanel
			*****/
			// Assure que le wrapper des panel est toujours au dessus du mask avant de faire l'affichage
			that.scope.wrapper.style.zIndex = that.scope[that.panelName].cfg.getProperty('zIndex');
			YAHOO.util.Dom.addClass(document.body, "overlay-print-layout");
			YAHOO.util.Dom.addClass('yui-panel-' + that.cfg.renderTarget, "overlay-print-layout");
			YAHOO.util.Dom.addClass('yui-panel-' + that.cfg.renderTarget + '_c', "overlay-print-layout");
		};
		
		/**
		 * Ajoute la possibilit� d'avoir un callback apr�s le chargement des donn�es du panel
		 * @return void
		 */
		this._afterLoad = function () {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension de cette m�hode qui doit �tre ex�cut� apr�s le chargement des donn�es du panel
			*****/
			// Initialise le panel
			that._prepareMyPanel();
			if(that.submittedId){
			//	rl.manageOnReady(null ,null ,rl);
			//	rl.manageLinks(that.submittedId,rl);
			}
		};

		/**
		 * Ajoute la possibilit� d'avoir un callback avant le chargement des donn�es du panel (avant le call AJAX)
		 * @return void
		 */
		this._beforeRemoteLoad = function() {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension de cette m�hode qui doit �tre ex�cut� avant le call AJAX pour charg� les donn�es du panel.
				Incluant la possibilit� d'avoir d'autre code ex�cut� avant le code de la fonction elle m�me et la possibilit� d'overwriter la m�thode elle m�me.
			*****/

			/*****
				Un seul preloader est g�n�r� pour tous les overlay. Le scope du preloader est donc global � tous les overlay.
			*****/
			
			// Si le preloader doit �tre affich� pour indiquer un t�l�chargement
			if(that.cfg.useRemotePreLoader) {
				// Si le preloader n'a pas encore �t� cr��, cr�e le preloader
				if(!this.scope.preloader) {
					// Si un div#overlay-preloader existe d�j� dans le document, r�cup�re se DIV
					var div = document.getElementById('overlay-preloader');
					// Si aucune div#overlay-preloader, ajoute le div au DOM
					if(!div) {
						div = document.createElement('div');
						div.id = 'overlay-preloader';
						this.scope.wrapper.appendChild(div);
						div.style.position = 'relative';
					}

					// Initialise le panel pour le preloader temporaire qui sera afficher jusqu'a se que les donn�es externe soit re�us et que le panel soit pr�t � afficher
					this.scope.preloader = 
							new YAHOO.widget.Panel("preloader",  
								{ width:"50em",
								  fixedcenter:true, 
								  close:false, 
								  draggable:false, 
								  zindex:1,
								  modal:true,
								  visible:false
								} 
							);

					this.scope.preloader.isVisible = false;
					
					/**
					 * setPosition permet de centrer l'image du preloader au centre de la zone visible de la fen�tre
					 * @return void
					 */
					this.scope.preloader.setPosition = function() {

						var l, t, Dom;
						Dom = YAHOO.util.Dom;
						// Calcul la position horizontal
						l = Dom.getDocumentScrollLeft() + Math.ceil(Dom.getViewportWidth()/2) - Math.ceil(that.cfg.imgPreloader.width/2);
						// Calcul la position vertical
						t = Dom.getDocumentScrollTop() + Math.ceil(Dom.getViewportHeight()/2) - Math.ceil(that.cfg.imgPreloader.height/2);
						// Applique le positionnement sur l'image
						that.scope.preloader.loaderImage.style.left = l + 'px';
						that.scope.preloader.loaderImage.style.top = t + 'px';
						// Applique le positionnement sur le label
						t += parseInt(that.cfg.imgPreloader.height);
						that.scope.preloader.loaderLabel.style.top = t + 'px';
						that.scope.preloader.loaderLabel.style.left = '0px';
						
					};
					// Ajoute le preloader au manager des overlays
					this.scope.manager.register(this.scope.preloader);

					// Gestion du positionnement de l'image du preloader
					that.scope.preloader.showMaskEvent.subscribe(function() {
																	if(!that.scope.preloader.loaderImage) {
																		var img = new Image;
																		img.src = that.cfg.imgPreloader.src;
																		img.id = 'CPNL_overlay___ajax_img_loader___';
																		img.style.width = that.cfg.imgPreloader.width + 'px';
																		img.style.height = that.cfg.imgPreloader.height + 'px';
																		img.style.position = 'absolute';
																		img.style.zIndex = 101;
																		img.setAttribute('alt', rl.lang.lbl.titlePreloader);
																		document.body.appendChild(img);
																		that.scope.preloader.loaderImage = img;
																		var lbl = document.createElement('div');
																		lbl.className = 'overlay-loading-label';
																		lbl.id = 'CPNL_overlay___ajax_lbl_loader___';
																		lbl.style.zIndex = 102;
																		lbl.innerHTML = rl.lang.lbl.titlePreloader;
																		document.body.appendChild(lbl);
																		that.scope.preloader.loaderLabel = lbl;
																	}
																	that.scope.preloader.setPosition();
																	that.scope.preloader.loaderImage.style.display = 'block';
																	that.scope.preloader.loaderLabel.style.display = 'block';
																}, that.scope.preloader, true);					
				}
				
				// Aucun panel n'est affich� pour le preloader. L'image du preloader est affich� au dessus du mask
				// du panel et centr� dans la zone visible de l'�cran
				this.scope.preloader.buildMask();
				this.scope.preloader.sizeMask();
				this.scope.preloader.cfg.setProperty('zIndex', 100);
				this.scope.preloader.stackMask();
				this.scope.preloader.showMask();
				this.scope.preloader.isVisible = true;

				// Charge le contenue du overlay
				that._loadContent();

			}
			
		};
		

		/**
		 * searchNodes est utilis� par la m�thode getElementsByAttribute qui permet
		 * de recherche des nodes � travers un DOM XML pour IE
		 *
		 * @param {Node Object}  node  Un objet repr�sentant une node d'un DOM XML
		 * @param {Function} func Fonction utilis� pour la r�cursivit�
		 */
		this.searchNodes = function _searchNodes(node, func) {
			// Ex�cute la fonction avec le param�tre node
			func(node);
			// R�cup�re la node suivante
			node = node.firstChild;
			// Boucle r�cursive pour rechercher dans tous les nodes enfants
			while(node) {
				_searchNodes(node, func);
				node = node.nextSibling;
			}
		};
		
		/**
		 * getElementsByAttribute est une m�thode utilis�e pour rechercher des nodes d'un
		 * DOM XML ayant une valeur pr�cise dans un attribut pr�cis.
		 *
		 * @param {DOM Object}  dom  Un objet repr�sentant un DOM XML ou un noeud de DOM XML
		 * @param {String} attr Nom de l'attribut � tester
		 * @param {String} value Valeur recherch� dans l'attribut attr
		 * @param [Integer] limit Valeur optionnel permettant de limiter le nombre de noeuds retourn� par la fonction
		 */
		this.getElementsByAttribute = function (dom, attr, value, limit) {
			// S'il n'y a pas de dom, retourne false
			if(!dom) return false; // D�fini le limit � false si aucune limite (num�rique) n'est re�us
			if(typeof limit == 'undefined' || typeof limit != 'number') limit = false;
			var results = [];
			
			// Recherche r�cursive pour trouver les nodes correspondante
			this.searchNodes(dom, function (node) {
				// R�cup�re uniquement si le node type est un element et que la node contient l'attribut voulue
				var curNode = node.nodeType === 1 && node.getAttribute(attr);
				// Si curNode est une string et �guale � value ou que value n'est pas une string r�cup�re la node
				if(typeof curNode === 'string' && (curNode == value || typeof value !== 'string')) {
					// Si le DOM ne supporte pas le innerHTML sur les node (IE par exemple)
					if(!node.innerHTML) {
						// Reconstruit le r�sultat produit par le innerHTML.
						// Le innerHTML ne contient pas le XML (html) de la node courrante, mais seulement l'int�rieur
						var inHTML = '';
						if(node.hasChildNodes()) {
							for(var c=0;c<node.childNodes.length;c+=1) {
								inHTML += node.childNodes(c).xml;
							}
						}
						// Cr�e un nouvel objet avec les propri�t�s n�cessaires :: Objet �volutif, ajout� les propri�t�s utilis�s ici pour une bonne constance
						var tNode = {
						  tagName: node.tagName,
						  id: node.getAttribute('id'),
						  innerHTML: inHTML
						};
						results.push(tNode);
					} else {
						results.push(node);
					}
				}
			});
			
			// Si aucune node trouv�, retourne false
			if(results.length === 0) return false;
			else if(limit === 1 && results.length) return results[0]; // Nodes trouv� et limit de 1 demand�, retourne l'objet 0
			else if(limit && results.length > limit) return results.slice(0, limit); // Retourne le nombre d'�l�ments impos� par la limite
			else return results; // Retourne le array de nodes trouv� (aucune limite)
			
		};

		/**
		 * getHeaderContent est la m�thode utilis�e pour pr�parer le contenu HTML � envoyer dans le header du panel
		 *
		 * @return {String} Html � injecter dans le header du panel
		 */
		this.getHeaderContent = function() {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension de cette m�hode pour remplacer le code utils� par d�faut
			*****/
			//Pr�pare le header du overlay
			var txtHeader = '<div id="' + that.cfg.renderTarget + '-closePanel" class="closePanel" title="' + rl.lang.lbl.xCloseTitle + '"><span>' + rl.lang.lbl.xCloseLabel + '</span></div>';
			if(that.cfg.title) {
				txtHeader = "<span class='headPanelTitle'>" + that.cfg.title + "</span>" + txtHeader;
			}
			return txtHeader;
		};
		
		/**
		 * getBodyContent est la m�thode utilis�e pour pr�parer le contenu HTML � envoyer dans le body du panel
		 *
		 * @return {String} Html � injecter dans le body du panel
		 */
		this.getBodyContent = function() {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension de cette m�hode pour remplacer le code utils� par d�faut
			*****/

			return '<div class="main-content overlay-content">' + that._bodyContent + '</div>';
		};
		
		/**
		 * getFooterContent est la m�thode utilis�e pour pr�parer le contenu HTML � envoyer dans le footer du panel
		 *
		 * @return {String} Html � injecter dans le footer du panel
		 */
		this.getFooterContent = function() {
			/*****
				::: TO DO ::: 
				Ajouter les fonctionnalit�s d'extension de cette m�hode pour remplacer le code utils� par d�faut
			*****/
			// Par d�faut, si l'option de configuration addCloseLink est false, retourne une string vide
			return (that.cfg.addCloseLink ? '<div id="' + that.cfg.renderTarget + '-closePanelFooter" class="closePanelFooter button y-button"><button><span><span>' + rl.lang.lbl.btnCloseCaption + '</span></span></div>' : '');
		};
		

		this._handleCLose = function() {
	        try {
                YAHOO.util.History.navigate("cpnl", "none");
				that.scope[that.panelName].hide();
	        } catch (e) {
	            that.scope[that.panelName].hide();
	        }
		}

		/**
		 * _prepareMyPanel est la m�thode utilis�e pour pr�parer le panel pour l'affichage
		 *
		 * @return void
		 */
		this._prepareMyPanel = function() {
			// Si le panel n'a pas encore �t� cr��, cr�e le panel
			//if(!that.scope[that.panelName]) {
				// Si le div qui doit contenir le panel n'existe pas, il sera ajout� dans le wrapper
				//if(!document.getElementById(that.cfg.renderTarget)) {
					// Cr�� un div pour le marqueur des URL pourqu'un saut soit possible
					var div = document.createElement('div');
					div.id = that.cfg.renderTarget;
					div.name = that.cfg.renderTarget;
					that.scope.wrapper.appendChild(div);
			//	}
				
				// Configuration initial du panel
				var conf = {};
				conf['width'] = that.cfg.width;
				if(that.cfg.height != 'auto') conf['height'] = that.cfg.height;
				conf['fixedcenter'] = that.cfg.fixedCenter;
				conf['close'] = that.cfg.showCloseX;
				conf['dragable'] = that.cfg.dragable;
				conf['AutoFillHeight'] = true;
				conf['zindex'] = 10 + that.scope.manager.overlays.length;
				conf['modal'] = that.cfg.modal;
				conf['visible'] = false;
				if(that.cfg.effect) conf['effect'] = that.cfg.effect;
				
				// Cr�e un nouveau panel et l'ajoute au scope
				that.scope[that.panelName] = new YAHOO.widget.Panel('yui-panel-' + that.cfg.renderTarget, conf);
				that.scope[that.panelName].setHeader(that.getHeaderContent()); 
				that.scope[that.panelName].setBody(that.getBodyContent()); 
				that.scope[that.panelName].setFooter(that.getFooterContent()); 


				//Ajoute la gestion de la touche ESC pour fermer la fen�tre
				var kl = new YAHOO.util.KeyListener(document, { keys:27 },
													  { fn:that._handleCLose,
														scope:that.scope[that.panelName],
														correctScope:true }, "keyup" ); 
														// keyup is used here because Safari won't recognize the ESC
														// keydown event, which would normally be used by default
			
				that.scope[that.panelName].cfg.queueProperty("keylisteners", kl);
				// Pr�pare le panel
				that.scope[that.panelName].render(document.getElementById(that.cfg.renderTarget));

				// Gestion des footers multiple. Les div.ft qui sont d�finis dans le code qui est charg� sont utilis�s
				// pour l'affichage sans javascript. Ils doivent donc �tre cach� � l'exception du dernier si l'option
				// de configuration "addCloseLink" est � false 
				var fts = YAHOO.util.Dom.getElementsByClassName("ft", "div", that.cfg.renderTarget);
				if(fts) {
					for(var i=0; i<fts.length-1; i+=1) {
						fts[i].style.display = (that.cfg.addCloseLink ? 'none' : 'block');
					}
					fts[i].style.display = (!that.cfg.addCloseLink ? 'none' : 'block');
				}
				//Ajoute la gestion des click sur le "X" le bouton "Fermer" et le bouton "Annuler" pour fermer le panel
				YAHOO.util.Event.addListener([that.cfg.renderTarget + "-closePanel", that.cfg.renderTarget + "-closePanelFooter"], "click", that._handleCLose, that.scope[that.panelName], true);
				
				var btnResets = YAHOO.util.Dom.getElementsByClassName('bt-cancel', 'button', document.getElementById(that.cfg.renderTarget));
				if(btnResets) {
					for(var i=0; btnResets[i]; i++) {
						//console.dir(btnResets[i]);
						if(btnResets[i].type == 'reset') {
							YAHOO.util.Event.addListener(btnResets[i], "click", that._handleCLose, that.scope[that.panelName], true);
						}
					}
				}
				
				// Gestion des tag script est des embedswf()
				that.scope[that.panelName].showEvent.subscribe(function() {
																	if(that.cfg.useSWFObject) that._testSWFScript();
																	else that._parseForScriptTag();
																}, that.scope[that.panelName], true);
				
				// Gestion du callback avant l'affichage du panel
				that.scope[that.panelName].beforeShowEvent.subscribe(that._beforeShow, that.scope[that.panelName], true);
				// Gestion du callback apr�s l'affichage du panel
				that.scope[that.panelName].showEvent.subscribe(that._afterShow, that.scope[that.panelName], true);
				// Gestion du callback avant la fermeture du panel
				that.scope[that.panelName].beforeHideEvent.subscribe(that._beforeHide, that.scope[that.panelName], true);
				// Gestion du callback apr�s la fermeture du panel
				that.scope[that.panelName].hideEvent.subscribe(that._afterHide, that.scope[that.panelName], true);
				
				// Si le panel utilise des call embedswf() pour ajouter des flash, le fixedCenter doit �tre retir� au moment du show
				// pour laisser l'overlay scrollay avec la fen�tre pour ne pas laisser le flash sortir du overlay.
				// Le fixedCenter est r�tablie � la valeur de config une fois la fen�tre ferm�, pour qu'elle se repositionne correctement � la prochaine ouverture.
				if(that.cfg.useSWFObject) {
					//�v�nement qui doit �tre activer lorsque le panel est affich�
					that.scope[that.panelName].showEvent.subscribe(function() {arguments[2].cfg.setProperty('fixedCenter', false);}, that.scope[that.panelName], true);
					//�v�nement qui doit �tre activer lorsque le panel est ferm�
					that.scope[that.panelName].hideEvent.subscribe(function() {arguments[2].cfg.setProperty('fixedCenter', that.cfg.fixedCenter);}, that.scope[that.panelName], true);
				}

				// Suppression des ul.skiplink dans le code charg�				
				/*****
					::: TO DO ::: 
					D�placer cette fonctionnalit� dans un extend de la fonction. Cette gestion est sp�cifique au porjet Vid�otron
				*****/
				var container = document.getElementById(that.cfg.renderTarget);
				testContainer = document.getElementById(that.cfg.renderTarget);
				
				//FIX pour redraw dans safari Mac seulement, si YUI est � la version est inf�rieur a 2.7.0
				var ua = navigator.userAgent;
				var version = parseInt(YAHOO.env.getVersion('container').versions[0].replace(/\.*/g,''));

				if ( version < 270 && ua.match(/AppleWebKit\/([^\s]*)/)){
					var	forceRedraw = function (e, s,container){ 
						container.style.opacity = "0.99";
						setTimeout(function(){ 	container.style.opacity = "1" ;},100);
					}
					YAHOO.widget.Overlay.windowScrollEvent.subscribe(forceRedraw,container);
				}

				var skip = YAHOO.util.Dom.getElementsByClassName('skiplink', 'ul', container);
				if(skip) {
					for(var i=0;i<skip.lenght;i+=1) {
						container.removeChild(skip[i]);
					}
				}
				// Gestion du useColapsedList				
				/*****
					::: TO DO ::: 
					D�placer cette fonctionnalit� dans un extend de la fonction. Cette gestion est sp�cifique au porjet Vid�otron
					Il faut aussi alt�rer l'utilisation du id "collapse" cette utilisation peut cr�er un conflit si la page principal
					utilise aussi un id "callapse"
				*****/
				if(that.cfg.useColapsedList) {
					var div = document.getElementById(that.cfg.renderTarget);
					if(div) {
						//var collapsedList = document.getElementById("collapse");
						var collapsedList = YAHOO.util.Dom.getElementsByClassName("collapse-list");
						for (var i = 0; i < collapsedList.length; i++){
							collapseUncollapseDataList(collapsedList[i]);
						}
					}
				}

				
				// Highjack des liens et formulaire dans le paneau lui m�me 
				// (Cette m�hode est r�cursive lorsque les panel sont charg�. Donc un nombre X de panel peuvent souvrir en suivant
				// une s�quence de traitement par exemple. 
				// Ex.: Un formulaire � remplir dans un overlay, lors du submit, la confirmation peut s'afficher dans un overlay aussi.

				that.scope[that.panelName].subPanel = new VIDEOTRON.widget.remoteLoad({	root: container, 
																						overlayBaseName: that.panelName.substr(7) + "-", 
																						effect: false});

				panelObj = that.scope[that.panelName].subPanel.getThat();
				panelObj
				
				that.scope[that.panelName].subPanel.parentOverlay = that;
				
				// Ajoute le panel au manager
				this.scope.manager.register(that.scope[that.panelName]);
				that.scope[that.panelName].isVisible = false;
			
			//}


			// Affichage du panel
			that.displayPanel();
			
		};

		/**
		 * hideAllPanels est la m�thode utilis�e pour fermer tous les panneaux
		 *
		 * @return void
		 */
		this.hideAllPanels = function (){
			// Boucle pour fermer tous les panel qui pevent �tre ouvert. Principalement utilis� pour 
			// les sous-panel qui s'ouvre � partir d'un lien ou d'un form � l'int�rieur d'un panel
			for(var i in that.scope) {
				if(/^Panel /.test(that.scope[i].toString()) && that.scope[i].isVisible && i != that.panelName) {
					that.scope[i].hide();
					that.scope[i].isVisible = false;
				}
			}
		}
		/**
		 * emptyForm()
		 * S'assure de vider les valeurs du formulaire � l'exception des champs
		 * qui ont la classe "KeepValue"
		 **/
		this.emptyForm = function(){

				panelElement = that.scope[that.panelName].element;
				inputs  = panelElement.getElementsByTagName("input");
				selects = panelElement.getElementsByTagName("select");
				textarea = panelElement.getElementsByTagName("textarea");
				controls = Array(inputs, selects, textarea);
				
				for (var i = 0 ; i < controls.length; i++){
					for (var j = 0 ; j < controls[i].length; j++){
						if (controls[i][j].type != "hidden" && controls[i][j].className.indexOf("keepValue") == -1)
							controls[i][j].value = "";
					}
				} 
		}
		/**
		 * displayPanel est la m�thode utilis�e pour afficher le panel
		 *
		 * @return void
		 */
		this.displayPanel = function() {
			that.hideAllPanels();

			if(!that.scope[that.panelName].isVisible) {
				var cliReg = YAHOO.util.Dom.getClientRegion();
				//that.scope.wrapper.style.top = cliReg.top + 'px'; ////////// ----------> Temp, for test focus !
				
				that.scope[that.panelName].isVisible = true;
				//console.debug('zindex = ' + that.scope[that.panelName].cfg.getProperty('zIndex'));				
				that.scope[that.panelName].show();	
				that.emptyForm();
				
				that.scope.manager.focus([that.panelName]);

			}
		};
		
		/**
		 * _enumProps est une m�thode utilis� pour le d�bugage et permet d'afficher un alert avec toutes les propri�t�s d'un objet
		 *
		 * @return void
		 */
		this._enumProps = function(obj) {
			if(console && console.dir) {
				console.dir(obj);
			} else {
				var res = '';
				try {
					for(var i in obj) {
						res += i + ' : ' + typeof(obj[i]) + "\n";
					}
				} catch(e) {res += i + ' : error';}
				alert(res);
			}
		};
		
		/**
		 * _loadConent est la m�thode utilis�e pour charger le contenue du panel
		 *
		 * @return void
		 */
		this._loadContent = function() {
			if(typeof that.cfg.contentType == 'string' && that.cfg.contentType == 'remote') {
				var src = ((that.hookedElement.tagName == 'A')||(that.hookedElement.tagName == 'AREA')) ? that.hookedElement.href : that.hookedElement.action;
				if(src) {
					if((that.hookedElement.tagName == 'A')||(that.hookedElement.tagName == 'AREA')) {
						that.conn = YAHOO.util.Connect.asyncRequest("get", src, {success: that._loadCallback_success, failure: that._loadCallback_failure});
					} else {
						that.conn = YAHOO.util.Connect.asyncRequest(that.hookedElement.method, src, {success: that._loadCallback_success, failure: that._loadCallback_failure});
					}
				}
			} else {
				/*****
					::: TO DO ::: 
					Ajouter la fonctionnalit� d'un callback pour ex�cuter une fonction au lieu du call AJAX pour pr�parer le contenue
				*****/
				alert('_loadConent function callback not implemented yet!');
			}
		};
		

		/**
		 * _loadCallback_succes est la m�thode appel� suite � un call AJAX avec un status 'success'
		 * Cette m�thode priv� permet de traiter le r�sultat XML re�us par le REQUEST
		 *
		 * @param {Response Object} o Re�ois l'objet response retourn�e par le YAHOO.connection
		 *
		 * @return void
		 */
		this._loadCallback_success = function(o){
			// R�cup�re le contenue XML du response
			//console.dir(o);
			var doc = o.responseXML;
			var res = '';

			// Conserve le contenue XML original
			that.contentXML = doc.documentElement;

			// Si le panel utilise la configuration par m�ta, cherche les configuration dans le dom
			if(that.cfg.useMetaConfig) that._metasConfigFromXML(doc.documentElement);
			
			// R�cup�re les contenus du Head, Body et footer du panel
			// Si le DOM r�cup�r� supporte le getElementById, utilise cette fonctionnalit�
			if(doc.getElementById) {
				var pHead = that.cfg.remoteSourceElements.header ? doc.getElementById(that.cfg.remoteSourceElements.header) : '';
				var pBody = that.cfg.remoteSourceElements.content ? doc.getElementById(that.cfg.remoteSourceElements.content) : '';
				var pFoot = that.cfg.remoteSourceElements.footer ? doc.getElementById(that.cfg.remoteSourceElements.footer) : '';

			} else { // Hack pour le DOM IE qui ne supporte pas toutes les fonctionnalit�s des autres browser au niveau du DOM XML
				var pHead = that.cfg.remoteSourceElements.header ? that.getElementsByAttribute(doc, 'id', that.cfg.remoteSourceElements.header, 1) : '';
				var pBody = that.cfg.remoteSourceElements.content ? that.getElementsByAttribute(doc, 'id', that.cfg.remoteSourceElements.content, 1) : '';
				var pFoot = that.cfg.remoteSourceElements.footer ? that.getElementsByAttribute(doc, 'id', that.cfg.remoteSourceElements.footer, 1) : '';
			}

			// R�cup�re les contenus HTML de chaque section du overlay
			if(pHead && pHead.innerHTML) that._headerContent = pHead.innerHTML;
			if(pBody && pBody.innerHTML) that._bodyContent = pBody.innerHTML;
			if(pFoot && pFoot.innerHTML) that._footerContent = pFoot.innerHTML;
			
			// Si le panel utilise l'affichage du preloader et que le preloader existe et qu'il est visible, cache le preloader
			if(that.cfg.useRemotePreLoader && that.scope.preloader && that.scope.preloader.isVisible) {
				that.scope.preloader.hideMask();
				if(that.scope.preloader.loaderImage) that.scope.preloader.loaderImage.style.display = 'none';
				if(that.scope.preloader.loaderLabel) that.scope.preloader.loaderLabel.style.display = 'none';
				that.scope.preloader.isVisible = false;
			}
			// Indique que le panel a �t� charg�, donc au prochains affichage, aucune chargement n�cessaire, affichage direct du panel
			//that._isLoaded = true;

			// M�thode ex�cut� avant le rendering du panel / apr�s le chargement des donn�es
			that._afterLoad();

		};

		/**
		 * _loadCallback_failure est la m�thode appel� suite � un call AJAX avec un status 'failled'
		 * Cette m�thode priv� permet de g�rer les requ�te AJAX qui sont en erreur
		 *
		 * @param {Response Object} o Re�ois l'objet response retourn�e par le YAHOO.connection
		 *
		 * @return void
		 */
		this._loadCallback_failure = function(o) {
			that._isLoaded = false;
			/*****
				::: TO DO ::: 
				D�finir quoi faire lors d'un failled par d�faut. Ajouter la possibilit� d'�tendre cette fonctionnalit� pour
				une gestion personnalis� des erreur pour l'application courrante.
			*****/
			// force le bouton fermer 
			that.cfg.addCloseLink = true; 
			var error_text = (rl.lang.lang != "en") ? "Une erreur s'est produite, s.v.p. r�essayer plus tard." : "An error has occured, please try again later"; 
			that._bodyContent = "<h2>"+error_text+"</h2>"; 
			// Si le panel utilise l'affichage du preloader et que le preloader existe et qu'il est visible, cache le preloader 
			if(that.cfg.useRemotePreLoader && that.scope.preloader && that.scope.preloader.isVisible) { 
					that.scope.preloader.hideMask(); 
					if(that.scope.preloader.loaderImage) that.scope.preloader.loaderImage.style.display = 'none'; 
					if(that.scope.preloader.loaderLabel) that.scope.preloader.loaderLabel.style.display = 'none'; 
					that.scope.preloader.isVisible = false; 
			} 
			// Indique que le panel a �t� charg�, donc au prochains affichage, aucune chargement n�cessaire, affichage direct du panel 
			//that._isLoaded = true; 
			
			// M�thode ex�cut� avant le rendering du panel / apr�s le chargement des donn�es 
			that._afterLoad();
		};
				

		/**
		 * _metasConfigFromXML est la m�thode utilis� pour r�cup�rer les configuration d�fini dans les m�tas du
		 * document charg� avec un call AJAX (remote).
		 * Configuration des metas disponibles :
		 * overlay-title : Permet de d�finir le titre du overlay (default = '')
		 * overlay-width : D�fini la largeur du Panel (default = 50em)
		 * overlay-height : D�fini la hauteur du Panel (default = 'auto')
		 * overlay-addCloseLink : Indique si un lien ferm� doit �tre ajout� dans le footer du overlay (default : true)
		 * overlay-useSWFObject : Indique si le overlay utilise des objet flash - DEPRECATED pour la version 2, d�tection automatique int�gr�
		 * overlay-useTooltips: Indique l'utilisation de tooltip dans le panel (default : false) - DEPRECATED pour la version 2, ajout� un ID sur le script contenant le code
		 * overlay-useColapsedList: Indique l'utilisate de table calapse dans le panel - ajout� dans le cadre du projet Vid�otron (default : false)
		 *
		 * @param {DOMXml Object} oXML DOM Xml � utiliser pour le traitement. Normalement le documentElement du 
		 *							   responseXML de l'objet YAHOO.connection
		 *
		 * @return void
		 */
		this._metasConfigFromXML = function(oXML) {
			// R�cup�re les �lements 'meta' dans le dom
			var metas = oXML.getElementsByTagName('meta');
			var metas_conf = [];
			var attrName = new String();
			var attrContent = new String();
			
			// Boucle pour r�cup�rer les configurations
			for(var i=0; i<metas.length; i+=1) {
				// R�cup�re le name et le content du tag meta
				attrName = metas[i].getAttribute('name');
				attrContent = metas[i].getAttribute('content');
				// Si le name d�bute par overlay-
				if(/^overlay-/i.test(attrName)) {
					// R�cup�re la section pertinante du nom de la config � setter
					attrName = attrName.substring(8);
					// Si la configuration existe
					if(this.cfg.hasOwnProperty(attrName)) {
						// Assure une conversion propre en boolean pour les valeur "true" et "false"
						attrContent = (attrContent == "true") ? true : (attrContent == "false" ? false : attrContent);
						this.cfg[attrName] = attrContent;
					}
				}
			}
		};
	
		/**
		 * _testSWFScript est la m�thode utilis� pour d�termin� si le script swfobject.js est charg� et le charg� s'il ne l'est pas
		 *
		 * @return void
		 */
		this._testSWFScript = function () {
			if(typeof(swfobject) == 'undefined' || typeof(swfobject) != 'object') {
				// swfobject n'est pas d�fini ou n'est pas un objet, il faut donc charger le script
				var script = document.createElement("script");
				/*****
					::: TO DO ::: 
					Permettre la personnalisation du chemin d'acc�s du script swfobject.js
				*****/
				script.src = "/resources/legacy/js/swfobject.js";
				script.type = "text/javascript";
				script.onload = function (event) {
					that._parseForScriptTag();
				}
				// R�cup�re le header du document
				var hd = document.getElementsByTagName("head")[0];
				// Ajoute le script
				hd.appendChild(script);
			} else {
				that._parseForScriptTag();
			}
		};
		
		/**
		 * _parseForScriptTag est la m�thode utilis� rechercher des script dans le document charg� et les ajout�s 
		 * au document courrant.
		 * Pour qu'un tag script soit tenue en compte et ajout� au document courant il doit avoir un id d�butant 
		 * par extend-overlay- suivi d'un numr�o N. i.e.: extend-overlay-1, extend-overlay-2, ..., extend-overlay-N
		 * 
		 * @return void
		 */
		this._parseForScriptTag = function () {
			// Si les script ont d�j� �t� ajout�, retourne
			/*if(that._scriptIsWrited) {
				//console.log('script allready writted for this overlay!');
				//return true;
			}*/
			// R�cup�re les tag script
			var scripts = that.contentXML.getElementsByTagName('script');
			var container = document.getElementById(that.cfg.renderTarget);
			
			/**
			 * _hackEmbedSWF est une m�thode utilis� pour tester si le script courrant utilise un swfobject.embedSWF
			 * Si le script utilise un embedSWF(), la script sera modifi� pour utiliser un createSWF() puisque le
			 * embedSWF est ex�cut� uniquement sur le document.onload.
			 *
			 * ATTENTION : Pour que cette proc�dure fonctionne correctement, le call au embedSWF doit avoir tous les
			 * param�tre requis et optionnel du call. Au total 9 param�tre sont n�cessaire pour reconstruire le createSWF()
			 * 
			 * @param {String} text Script � tester et � modifier si n�cessaire
			 *
			 * @return {String} script alt�r� ou non
			 */
			var _hackEmbedSWF = function(text) {
				// Regular Expression qui permet de d�finir si un embedSWF est appell� dans le script
				var reg = /swfobject\.embedSWF\(([\s\S]*)\);/im;
				var matches = text.match(reg); // Test la RegExp et r�cup�re tous les match
				var endObject = false;
				var curScript = false;
				// S'il y un call au emebedSWF le matches devrait contenir dans le [0] le script original et dans le [1] les param�tres du call
				if(matches && matches.length == 2) {
					curScript = matches[0];
					matches = matches[1].split(','); // S�pare les param�tres dans un array
					// Gestion des param�tre fesant parti d'un objet dans les param�tres
					for(var i=0;i<matches.length;i+=1) {
						matches[i] = matches[i].trim(); //Supprime les espaces innutiles
						// Si le param�tre d�bute par un { il s'agit de la d�claration d'un objet. Il faut donc trouver la fin de cet objet dans les param�tres qui suivent
						if(matches[i].substr(0,1) == '{') {
							var curI = i, endObject = false;
							// G�re le cas ou la fin de l'objet est dans la m�me valeur. Donc un seul param�tre pass� � l'objet
							if(/\}$/.test(matches[i])) endObject = true;
							// Boucle pour trouver la fin de l'objet
							while(!endObject && i<matches.length) {
								i+=1;
								matches[i] = matches[i].trim(); // Supprime les espaces innutile
								// Si le param�tre courrant fini par }, la fin de l'objet a �t� trouv�
								if(/\}$/.test(matches[i])) endObject = true;
								// Ajoute le param�tre � la d�claration de l'objet
								matches[curI] += ',' + matches[i];
								// Marque le param�tre courrant comme �tant supprim�
								matches[i] = '[[deleted]]';
								// Si la boucle tire � sa fin, arr�t forc�
								if(i ==  matches.length-1) break;
							}
						}
					}
					// Boucle pour r�cup�rer seulement les 9 param�tres du embedswf bien structur�
					var result = [];
					for(var i=0; i<matches.length; i+=1) {
						matches[i] = matches[i].trim();
						if(matches[i] !== '[[deleted]]') result.push(matches[i]);
					}
					
					// Tous les param�tres du swfebmed() sont requis pour le bon fonctionnement de la conversion. Donc 9 au total. 
					// Si un param�tre est manquant, le traitement n'a pas lieu
					if(result.length == 9) {
						/*
							S�quence des informations pass� au embedSWF et r�cup�r� dans le result
							0 = swfUrlStr, 
							1 = replaceElemIdStr, 
							2 = widthStr, 
							3 = heightStr, 
							4 = swfVersionStr, 
							5 = xiSwfUrlStr, 
							6 = flashvarsObj, 
							7 = parObj, 
							8 = attObj
						*/
						// Construit la conversion du embedSWF en createSWF
						var replaceOutput = 'var _fvars = \'\'; for(var i in ' + result[6] + ') { _fvars += i + \'=\' + ' + result[6] + '[i] + \'&\';}';
						replaceOutput += 'if(swfobject.hasFlashPlayerVersion(' + result[4] + ')) {'; // Gestion de la version du plugin
						replaceOutput += 'var flashAttributes = ' + result[8] + ' || {};';
						replaceOutput += 'flashAttributes.data = ' + result[0] + ';';
						replaceOutput += 'flashAttributes.width = ' + result[2] + ';';
						replaceOutput += 'flashAttributes.height = ' + result[3] + ';';
						replaceOutput += 'flashAttributes.flashvars = _fvars;';
		
						replaceOutput += 'var flashParams = ' + result[7] + ';';
						replaceOutput += 'flashParams.flashvars = _fvars;';
						replaceOutput += 'var replaceContentId = ' + result[1] + ';';
						
						replaceOutput += 'var myObject = swfobject.createSWF(flashAttributes, flashParams, replaceContentId);';
						replaceOutput += '}';
						
						if(curScript) {
							that.cfg.effect = false; // Pas d'effet sur le overlay s'il contient un flash.
							// Assure que la config qui indique l'utilisation du embedswf est � true et que les �vents sont associ� au show - hide
							if(!that.cfg.useSWFObject) {
								that.cfg.useSWFObject = true;
								//�v�nement qui doit �tre activer lorsque le panel est affich�
								that.scope[that.panelName].showEvent.subscribe(function() {arguments[2].cfg.setProperty('fixedCenter', false);}, that.scope[that.panelName], true);
								//�v�nement qui doit �tre activer lorsque le panel est ferm�
								that.scope[that.panelName].hideEvent.subscribe(function() {arguments[2].cfg.setProperty('fixedCenter', that.cfg.fixedCenter);}, that.scope[that.panelName], true);
								that.scope[that.panelName].cfg.setProperty('fixedCenter', false);
							}
							// Remplace la d�claration du embedSWF par le createSWF g�n�r�
							text = text.replace(curScript, replaceOutput);
						}
					}
				}
				// Retourne le script alt�r� ou non
				return text;

			};
			
			/**
			 * _hackEventListener est une m�thode utilis� pour r�cup�rer les Listener normalement ajout� � un Event.addListener de YAHOO
			 *
			 * ATTENTION : Pour que cette proc�dure fonctionne correctement, le call au addListener doit avoir au moins les 3 premiers
			 * param�tres. 1- l'objet, 2- L'event et 3- La fonction � ex�cuter
			 * 
			 * @param {String} code Script � tester et � modifier si n�cessaire (i.e.: window)
			 * @param {String} ev Nom de l'�v�nement � r�cup�rer (i.e.: load)
			 *
			 * @return {String} script alt�r� ou non
			 */
			var _hackEventListener = function (code, ev) {
				// Regular Expression qui permet de d�finir si un embedSWF est appell� dans le script
				var reg = /YAHOO\.util\.Event\.addListener\(([^\)]*)\);/im;
				var matches = code.match(reg); // Test la RegExp et r�cup�re tous les match
				var curScript = false;
				// S'il y un call au emebedSWF le matches devrait contenir dans le [0] le script original et dans le [1] les param�tres du call
				if(matches && matches.length == 2) {
					curScript = matches[0];
					matches = matches[1].split(','); // S�pare les param�tres dans un array
					
					if(matches.length >= 3) {
						for(var i=0;i<matches.length;i+=1) {
							matches[i] = matches[i].trim(); //Supprime les espaces innutiles
						}
						var re = new RegExp("('|\")" + ev + "\\1", "i");
						if(re.test(matches[1])) {
							var replaceOutput = matches[2] + '();';
							code = code.replace(curScript, replaceOutput);
						}
					}
				}
				return code;
			};
			
			// Si des scripts ont �t� trouv� et que le container (div du overlay) est d�fini 
			if(scripts && container) {
				// Boucle dans tous les �l�ments scripts trouv�
				for(var i=0; i < scripts.length; i++) {
					var id = scripts[i].getAttribute('id');
					// Test le ID, s'il d�bute par extend-overlay- suivi d'un num�ro N, le script doit �tre ajout� au overlay
					if(id && typeof id == 'string' && /^extend-overlay-\d+$/i.test(id)) {
						// Ajoute le script au document dans le overlay
						var s = document.createElement("script");
						s.type = "text/javascript";
						s.text = _hackEmbedSWF(scripts[i].innerHTML || scripts[i].text);
						s.text = _hackEventListener(s.text, "load");
						
						container.appendChild(s);
						
					}
				}
			}
			// Indique que les scripts ont �t� trait� pour l'overlay courrant
			that._scriptIsWrited = true;
		};
		
		
	/**********
		Public member
	**********/
			
		return {
			
			// Indique si le panel a d�j� �t� charg�
			isLoaded: function() { return that._isLoaded;},
			// D�finition du scope contenant tous les overlay, le wrapper, le manager, etc.
			scope: function() { return that.scope},
			
			/**
			 * Conf - Propri�t� (get|set) pour les diff�rentes conffigurations du panel
			 *
			 * @param {String}  attr  Propri� (attribut) � configurer ou r�cup�rer
			 * @param {String} value Valeur ou fonction a assigner � l'attribut 
			 * 						 (optionel) Si le value est d�fini, la m�thode
			 *						 set sera utilis� et true sera retourn�.
			 */
			conf: function(attr, value) {
				// GET
				if(!value) {
					if (typeof that.cfg[attr] != 'undefine') {
						var m = that.cfg[attr];
						if(m.call) {
							return m.call();
						} else if (window[that.cfg[attr]] && window[that.cfg[attr]].call) {
							return window[that.cfg[attr]].call();
						} else {
							return that.cfg[attr];
						}
					} else {
						return;
					}
				// SET
				} else {
					that.cfg[attr] = value;
					return true;
				}
			},

			/**
			 * show M�thode utilis� pour afficher le panel
			 *
			 * @return void
			 */
			show: function() {
				// Si le panel n'a pas encore �t� charg�
				
				if(!this.isLoaded() || that.hookedElement.tagName == "FORM") {
					that.hideAllPanels();
					// Si le type est remote et que le preloader doit �tre affich�
					if(this.conf('contentType') == 'remote' && this.conf('useRemotePreLoader')) {
						// M�thode ex�cut� avant le chargement ext�rieur du contenue
						that._beforeRemoteLoad();
					} else {
						// M�thode ex�cut� avant l'ex�cution de la fonction de r�cup�ration du contenue (sans AJAX)
						that._beforeLoad();
					}
					
				// Le panel est d�j� charg� et en m�moire, affiche le panel
				} else {

					that.displayPanel();
				}

			},
			
			
			/**
			 * toString M�thode utilis� pour identifer l'objet
			 *
			 * @return {String} identifiant du prototype de l'objet
			 */
			toString: function() {
				return "Object widget.overlay";
			},
			
			setSubmittedId: function(id){
				that.submittedId = id;
			}
			
			
		};

};


// ------[ WIDGET REMOTE LOAD V2 ]------------------------------------------------- //
/**
 * Remote Load creates a YUI panel whose contents are drawn from an
 * anchor's target page. The target page is expected to have one form
 * within it. The code hooks into all anchors on a page
 * with a given class name and overrides their default behaviour by
 * loading the contents of their target page into a YUI panel instead.
 *
 * @param {Object}  UserConfig  An object containing values that will
 *                              override Remote Load's default 
 *                              config values
 */
VIDEOTRON.widget.remoteLoad = function (userConfig) {

	/**********
		Private member
	**********/
		// D�finie une variable global pour garder une r�f�rence � tous les liens qui sont g�r� par tous les objets remoteLoad
		if(typeof window.__remoteLoadRef__ == 'undefined') window.__remoteLoadRef__ = [];
		
		var that = this; //R�f�rence � l'object lui m�me
		// Ajoute une r�f�rence � cette objet dans les r�f�rences global
		window.__remoteLoadRef__.push(this);

		this._lnks = []; // Collection des liens/form remoteLoad

		/* D�fini les configs initial */
	    this.cfg = {
	        hookClassName: ['remoteLoad'], // Class recherch� par d�faut
	        hookNodeName: ['a','area','form'],    // TagName recherch� par d�faut
	        root: document.body,		   // Node � root pour la recherche des nodes
			defaultLang: null, 			   // Langue par d�faut � utiliser (null = d�tection auto || fr || en)
			overlayBaseName: 'overlay-panel-', // Base des id qui sont g�n�r� pour les overlays
			effect: {effect:YAHOO.widget.ContainerEffect.FADE,duration:0.5}
			
	    };
		// R�cup�re les configs initial pass� en param�tre
	    for (var key in userConfig) {
	        if (userConfig.hasOwnProperty(key)) {
	            this.cfg[key] = userConfig[key];
	        }
	    }
		
		// Si la langue par d�faut n'est pas d�finie ou diff�rent de fr et en, d�finie la langue � partir de l'attribut lang du tag <html>
		if(!this.cfg.defaultLang || this.cfg.defaultLang !== 'fr' || this.cfg.defaultLang !== 'en') this.cfg.defaultLang = document.documentElement.lang;
			
		/* R�cup�re la langue de la page courante */
		var curDocLang = document.body.className.substring(0,2);

		// D�finition des labels initiaux pour le fran�ais
		this.fr = {
			'xCloseLabel': 'Fermer',
			'xCloseTitle': 'Fermer',
			'btnCloseCaption': 'Fermer',
			'titlePreloader': 'Chargement...'
		};

		// D�finition des labels initiaux pour l'anglais
		this.en = {
			'xCloseLabel': 'Close',
			'xCloseTitle': 'Close',
			'btnCloseCaption': 'Close',
			'titlePreloader': 'Loading...'
		};

		// D�finition de l'objet dictionnaire lang
		this.lang = {
			lbl: ((that.cfg.defaultLang === 'en') ? this.en : this.fr), // Initilise les libell� en fonction de la langue par d�faut
			lang: that.cfg.defaultLang,
			/**
			 * addLabel permet d'ajouter des libell� au dictionnaire
			 *
			 * @param {String} name Nom du libell� � ajouter
			 * @param {String} value Valeur du libell�
			 *
			 * @return void
			 */
			addLabel: function(name, value) {
				this.lang[name] = value;
			}
		};

		// Collections des overlays qui sont charg�
		this.overlays = [];
		
		// Gestion du browser history
		
		if(typeof this._defaultState === 'undefined') {
		    this._defaultState = 'none'; // Le targetIs est utilis� pour le state du panel (none = toute ferm�, un id ouvre le panel qui si ratache)
		    this._bookmarkedState = YAHOO.util.History.getBookmarkedState("cpnl");
		    this._initialState = this._bookmarkedState || this._defaultState;
	
			this.manageLinks = function (state)  {
				//console.debug("in manageLinks " + state);
		        // This is called after calling YAHOO.util.History.navigate, or after the user
		        // has trigerred the back/forward button. We cannot discrminate between
		        // these two situations.
				// console.debug("StateChange to : " + state);
				var rl = arguments[1];
		        // Show the right overlay according to the "state" parameter:
				for(var i=0; i<rl._lnks.length; i+=1) {
					if(rl._lnks[i].targetId == state) {
						if(typeof rl._lnks[i].overlay == 'undefined') {
							// Cr�e un nouveau panel pour le lien/form
							rl._lnks[i].overlay = new VIDEOTRON.widget.Overlay(rl.cfg, rl._lnks[i], rl);
							// Ajoute le panal � la collection de overlay
							rl.overlays.push(rl._lnks[i].overlay);
						}
						rl._lnks[i].overlay.show();
						break;
					} else {
						//console.debug("else");
						/*if(rl._lnks[i].overlay ){
							console.dir(rl._lnks[i].overlay.scope()['_panel_' + rl._lnks[i].targetId].subPanel.getThat());
						}*/
						if(rl._lnks[i].overlay){// && rl._lnks[i].overlay.scope()['_panel_' + rl._lnks[i].targetId].isVisible) {
							//if(console) console.debug(rl._lnks[i].overlay.scope()['_panel_' + rl._lnks[i].targetId]);
							//alert(rl._lnks[i].overlay.hide);
							that.hideAllSubPanel(rl._lnks[i].overlay.scope()['_panel_' + rl._lnks[i].targetId]);
							
							if (rl._lnks[i].overlay.scope()['_panel_' + rl._lnks[i].targetId].isVisible)
								rl._lnks[i].overlay.scope()['_panel_' + rl._lnks[i].targetId].hide();
						}
					}
				}
		    };
		 
			this.hideAllSubPanel = function (overlay){
//				console.debug ("hide subpanels ");
				subPanel = overlay.subPanel.getThat();
				for (var spln = 0 ; spln < subPanel._lnks.length ; spln++){
					if (subPanel._lnks[spln].overlay && subPanel.overlays[spln] && subPanel.overlays[spln].isLoaded()){
						that.hideAllSubPanel(subPanel._lnks[spln].overlay.scope()['_panel_' + subPanel._lnks[spln].targetId])
					}
				}
				if (overlay.isVisible){
					overlay.hide();
					
				}
			}

		    // Use the Browser History Manager onReady method to initialize the application.
		    this.manageOnReady = function () {
		        var currentState;
				//alert('onReady trigered!');
		        currentState = YAHOO.util.History.getCurrentState("cpnl");
				if(currentState !== 'none') {
					var rl = arguments[2];
					for(var i=0; i<rl._lnks.length; i+=1) {
						if(rl._lnks[i].targetId == currentState) {
							if(typeof rl._lnks[i].overlay == 'undefined') {
								// Cr�e un nouveau panel pour le lien/form
								rl._lnks[i].overlay = new VIDEOTRON.widget.Overlay(rl.cfg, rl._lnks[i], rl);
								// Ajoute le panal � la collection de overlay
								rl.overlays.push(rl._lnks[i].overlay);
							}
							rl._lnks[i].overlay.show();
							break;
						}
					}
				}
		    }
		    
		    // Register le module des overlay sous le nom de cpnl
		    YAHOO.util.History.register("cpnl", this._initialState, that.manageLinks, that,true);
		    // Initialize the browser history management library.
		    YAHOO.util.History.onReady(that.manageOnReady, that);

			
		}		
	   
	/**********
		Public member
	**********/
		return {
			getThat: function (){
				return that;
			},
			// R�f�rence au dictionnaire priv�
			lang: that.lang,
			// R�f�rence � la collection de overlay
			overlays: function() { return that.overlays},

			/**
			 * init Ex�cution automatique pour initialis� les fonctionnalit� du remoteLoad.
			 *
			 * @return true
			 */
			init: (function() {
				
				/**
				 * displayOverlay event Onclick ajout� � tous les liens/form .remoteLoad
				 *
				 * @return {Function} click event
				 */
				var displayOverlay = function(rl) {
					return function(e) {
						var overlayForceNav = false;
						// Annule les fonctionnalit� par d�faut du lien ou le submit du form
			            YAHOO.util.Event.preventDefault(e);
						// Si l'overlay pour se lien/form n'a pas encore �t� charg�
						if(typeof this.overlay == 'undefined') {
							// Cherche si un autre overlay a pr�c�dement �t� d�finie pour le m�me lien que celui ci
							var curOverlay;
							//console.group('current _lnks');
							for(var o = 0; window.__remoteLoadRef__[o]; o+=1) {
								if(window.__remoteLoadRef__[o]._lnks.length) {
									for(ln = 0; window.__remoteLoadRef__[o]._lnks[ln]; ln+=1) {
										if(window.__remoteLoadRef__[o]._lnks[ln].href && window.__remoteLoadRef__[o]._lnks[ln].href == this.href) {
											//console.debug('href = ' + window.__remoteLoadRef__[o]._lnks[ln].href);
											this.targetId = window.__remoteLoadRef__[o]._lnks[ln].targetId;
											this.overlay = window.__remoteLoadRef__[o]._lnks[ln].overlay;
											//console.debug('found - break the loop');
											break;
										}
									}
								}
								if(typeof this.overlay != 'undefined') break;
								//console.debug('loop ' + o);
								//console.dir(window.__remoteLoadRef__[o]._lnks);
							}
							//console.groupEnd();
							if(typeof this.overlay == 'undefined') {
								// Cr�e un nouveau panel pour le lien/form
								this.overlay = new VIDEOTRON.widget.Overlay(that.cfg, this, rl);
								// Ajoute le panel � la collection de overlay
								rl.overlays.push(this.overlay);
							}
						}

				        var newState = this.targetId;
						
						if(e.type !== 'submit' && !newState.match(/(-[0-9]+){2,}$/)) {
					        try {
					            var currentState = YAHOO.util.History.getCurrentState("cpnl");
					            // The following test is crucial. Otherwise, we end up circling forever.
					            // Indeed, YAHOO.util.History.navigate will call the module onStateChange
					            // callback, which will call createCalendar, which will call calendar.render(),
					            // which will end up calling handleCalendarBeforeRender, and it keeps going
					            // from here...
					            if (newState !== currentState) {
					                YAHOO.util.History.navigate("cpnl", newState);
					            } else {
									this.overlay.show();
								}
					        } catch (e) {
					            this.overlay.show();
					        }
						} else {
							this.overlay.show();
							//this.overlay.setSubmittedId (newState);
						//	rl.manageLinks(newState, rl);
						}
					};
				};
				
				// Boucle dans tous les class � g�rer comme �tant remoteLoad
				for(var cn=0; that.cfg.hookClassName[cn]; cn+=1) {
					// Boucle dans tous les types de tag � appliquer un remoteLoad
					for(var nn=0; that.cfg.hookNodeName[nn]; nn+=1) {
						// R�cup�re les �l�ments avec correspondant au class et au tagname
					    var tmpLnks = YAHOO.util.Dom.getElementsByClassName(that.cfg.hookClassName[cn], that.cfg.hookNodeName[nn], that.cfg.root);
						// Boucle pour ajouter les �l�ments � la collection d'objet g�r�
						for(var l=0; tmpLnks[l]; l+=1) that._lnks.push(tmpLnks[l]);
					}
				}
				//console.dir(window.__remoteLoadRef__);
				// Boucle dans tous les objets g�r� pour ajouter les �v�nement onclick ou onsubmit
			    for (var i = 0; that._lnks[i]; i += 1) {
					that._lnks[i].targetId = that.cfg.overlayBaseName + i;	
					var ev = ((that._lnks[i].tagName == 'A')||(that._lnks[i].tagName == 'AREA')) ? 'click' : 'submit';
					// Ajoute un event listener pour l'objet
			        YAHOO.util.Event.on(that._lnks[i], ev, displayOverlay(that));
			    }
				return true;
			})(),
			
			/**
			 * toString M�thode utilis� pour identifer l'objet
			 *
			 * @return {String} identifiant du prototype de l'objet
			 */
			toString : function() {
				return '[Object widget.remoteLoad]';
			}
		};
};

// ------[ WIDGET DIALOG ]------------------------------------------------- //
/**********
	***** VIDEOTRON.widget.Dialog PAS ENCORE UTILIS� N'Y COMPL�T� *****
**********/
/**
 * Remote Load creates a YUI panel whose contents are drawn from an
 * anchor's target page. The target page is expected to have one form
 * within it. The code hooks into all anchors on a page
 * with a given class name and overrides their default behaviour by
 * loading the contents of their target page into a YUI panel instead.
 *
 * @param {Object}  UserConfig  An object containing values that will
 *                              override Remote Load's default 
 *                              config values
 */
VIDEOTRON.widget.Dialog = function (dialogConfig) {

	/**********
		Private member
	**********/
		var that = this; //R�f�rence � l'object lui m�me

		// Cr�e le Namespace de travail (scope global � tous les dialogs)
		if(!YAHOO.custom || !YAHOO.custom.dilogs) {
			this.scope = YAHOO.namespace('custom.dialogs');
		} else {
			this.scope = YAHOO.custom.dialogs;
		}

		// Cr�e un div qui sert de wrapper au diff�rent overlay du scope
		if(!this.scope.wrapper) {
			this.scope.wrapper = document.createElement('div');
			this.scope.wrapper.id='custom-dialogs-wrapper';
			document.body.appendChild(this.scope.wrapper);
		}

		// Configuration initial par d�faut
		this.cfg = {
			/**
			 * ID {String} Identifiant du container du dialog
			 * Optionnel : Identifiant par d�faut du dialog.
			 * D�faut : simple-dialog
			 **/
	        id: "simple-dialog",

			/**
			 * renderTarget {Object} Noeud du DOM qui sert de point d'origine pour le rendering du dialog et la recherche des �l�ments
			 * Optionnel : Le noeud sert de point d'origine pour l'insertion du code.
			 * D�faut : document.body
			 **/
	        renderTarget: document.body,
			
			/**
			 * title {String} Titre du dialog
			 * Optionnel
			 * D�faut : ""
			 **/
			title: "",
			
			/**
			 * Text {String} Texte � afficher dans le dialog
			 * Optionnel
			 * D�faut : ""
			 **/
			text: "",
			
			/**
			 * width {Integer} largeur total du dialog
			 * Optionnel
			 * D�faut : 40em - �viter les mesures en pourcentage (%).
			 **/
			width: "40em",
			
			/**
			 * height {Integer} hauteur total du dialog
			 * Optionnel
			 * D�faut : auto - �viter les mesures en pourcentage (%).
			 **/
			height: 'auto',
			
			/**
			 * fixedCenter {Boolean} Indique si le dialog doit �tre centr�
			 * Optionnel : Positionne le dialog au centre de l'espace visible � l'�cran et le conserve centr� m�me si l'utilisateur scroll le contenue
			 * D�faut : true
			 **/
			fixedCenter: true,
			
			/**
			 * dragable {Boolean} Indique si le dialog peut �tre d�plac� par l'utilisateur
			 * Optionnel : Permet � l'utilisateur de d�placer la fen�tre avec la souris
			 * D�faut : false
			 **/
			dragable: false,
			
			/**
			 * showCloseX {Boolean} Permettre la fermeture du panel
			 * Optionnel : Fait r�f�rence � la propri�t� 'close' du panel.
			 * D�faut : true
			 **/
			showCloseX: true, 
			
			/**
			 * modal {Boolean} Indique si le dialog est modal ou pas
			 * Optionnel : Ajoute un mask qui couvre tout le contenue et g�re les click pour pr�venir toute action sur le contenu 
			 *             � l'ext�rieur du dialog lorsqu'il est affich�.
			 *			   Lorsque d�finie � true, le code en cours d'ex�cution est interompu jusqu'a la fermeture du dialog.
			 * D�faut : true
			 **/
			modal: true,
			
			/**
			 * listener {Object} Objet contenant l'information pour l'ajout du listener pour l'affichage du dialogue
			 * Requis : Information pour l'ajout du listener sur l'�v�nement et l'objet qui affichera le dialog
			 *			L'objet contient 5 propri�t�s qui indentifi le(s) �lements affect�, le type d'�v�enement � g�rer, 
			 *			la m�thode pour le callback de l'�v�nement, l'objet � passer en param�tre � la m�thode et l'override
			 *			Structure de l'objet :
			 *			el {String | HTMLElement | Array | NodeList} Un Id, une r�f�rence � un �l�ment, une collection de Id et/ou d'�l�ment � appliquer.
			 *			   Requis
			 *			sType {String} Le type d'�v�nement � appliquer
			 *			   Requis
			 *			fn {Function} M�thode qui sera invoqu� en callback de l'�v�nement.
			 *			   Requis
			 *			obj {Object} Un objet qui sera pass� en param�tre au gestionnaire.
			 *			   Optionnel
			 *			override {Boolean | object} Si true, obj devient le scope d'ex�cution du listener. Si un objet, l'objet devient le scope d'ex�cution.
			 *			   Optionnel
			 *			   D�faut : false
			 * D�faut : N/A
			 **/
			listener: {
				 el: '',
				 sType: '',
				 fn: function(){},
				 obj: {},
				 override: false
			},
			
			/**
			 * buttons {Boolean | Array} Indique les boutons � afficher dans le 
			 * Optionnel : Ajoute un mask qui couvre tout le contenue et g�re les click pour pr�venir toute action sur le contenu 
			 *             � l'ext�rieur du dialog lorsqu'il est affich�.
			 *			   Lorsque d�finie � true, le code en cours d'ex�cution est interompu jusqu'a la fermeture du dialog.
			 * D�faut : true
			 **/
			buttons: 0
		};

		// R�cup�re les configs initial pass� en param�tre
	    for (var key in dialogConfig) {
	        if (dialogConfig.hasOwnProperty(key)) {
	            this.cfg[key] = dialogConfig[key];
	        }
	    }
		
	/**********
		Public member
	**********/
		return {
		};
};
// Mode d'affichage des dialogs
VIDEOTRON.widget.Dialog.DMODE_YES_NO = 0;
VIDEOTRON.widget.Dialog.DMODE_OK = 2;
VIDEOTRON.widget.Dialog.DMODE_OK_CANCEL = 4;
