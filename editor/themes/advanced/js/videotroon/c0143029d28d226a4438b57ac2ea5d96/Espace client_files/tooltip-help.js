window.onload = function()
{
	tooltipHelp.init();
	tooltipLink.init();
};

var tooltipHelp =
{
	btn: "btn-help-yellow",
	box: "help-msg-bg",
	bLength: "",
	btnId: "tooltipBtn_",
	boxId: "tooltipBox_",
	
	init: function()
	{
		this.bLength = $("."+this.btn).length;
		
		for(var i=0; i<this.bLength; i++)
		{
			$("."+this.btn)[i].id = this.btnId+i;
			if ($("."+this.box)[i] != undefined) {
				$("."+this.box)[i].id = this.boxId+i;
			}
			$("."+this.btn)[i].onmouseover = function()
			{
				tooltipHelp.show(tooltipHelp.find(this.id));
			};
			
			$("."+this.btn)[i].onmouseout = function()
			{
				tooltipHelp.hide(tooltipHelp.find(this.id));
			};
		}		
	},
	
	find: function(eId)
	{
		return(parseInt(eId.substring(eId.indexOf("_")+1)));
	},
	
	show: function(numIndex)
	{
		$("#"+this.boxId+numIndex).fadeIn("slow");
	},
	
	hide: function(numIndex)
	{
		$("#"+this.boxId+numIndex).hide();
	}
};

var tooltipLink =
{
	btn: "link-help",
	box: "help-msg-bg",
	bLength: "",
	btnId: "tooltipBtn_",
	boxId: "tooltipBox_",
	
	init: function()
	{
		this.bLength = $("."+this.btn).length;
		
		for(var i=0; i<this.bLength; i++)
		{
			$("."+this.btn)[i].id = this.btnId+i;
			$("."+this.box)[i].id = this.boxId+i;
			
			$("."+this.btn)[i].onmouseover = function()
			{
				tooltipLink.show(tooltipLink.find(this.id));
			};
			
			$("."+this.btn)[i].onmouseout = function()
			{
				tooltipLink.hide(tooltipLink.find(this.id));
			};
		}		
	},
	
	find: function(eId)
	{
		return(parseInt(eId.substring(eId.indexOf("_")+1)));
	},
	
	show: function(numIndex)
	{
		$("#"+this.boxId+numIndex).fadeIn("slow");
	},
	
	hide: function(numIndex)
	{
		$("#"+this.boxId+numIndex).hide();
	}
};