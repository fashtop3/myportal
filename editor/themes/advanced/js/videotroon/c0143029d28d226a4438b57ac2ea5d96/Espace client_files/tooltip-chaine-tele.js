
// toggle la class du 2ieme parent
function toggleSelected(elem, type){
	if(type == "radio"){
		$(elem).parents("ul").find("dl").removeClass("selected");
	}
	if($(elem).attr("checked")){
		$(elem).parents("dl").addClass("selected")
	}else{
		$(elem).parents("dl").removeClass("selected")							
	}
}
function updateCheckedCount(checked){
	if($("#nbServicesSelectionnes").val() != undefined){
		var nbChaines = $("#nbServicesSelectionnes").val();
		if(checked){
	    	nbChaines++;
	    }else{
	    	nbChaines--;
	    }
	    $("#nbServicesSelectionnes").val(nbChaines);	      
	    $("#compteur-chaine").text(nbChaines)
	}
}

// fait floater le bloc calculateur de prix t�l� 
function calculateur_fix_pos(){
	//position de la colonne de droite
	var offset = $("#col-small").offset()
	//distance deja scroller
	var scrollTop = window.pageYOffset;
	// buffer entre le haut de la fenetre et la boite
	var topOffset = 10;
	// if IE
	if(scrollTop == undefined){
		var iebody=(document.compatMode && document.compatMode != "BackCompat")? document.documentElement : document.body;
		scrollTop = document.all? iebody.scrollTop : pageYOffset;
	}
	// seulement quand le bloc se rend en haut de la page
	if(offset.top <= (scrollTop+topOffset)){		
		var cssObj = {position: "fixed", top: topOffset, left: offset.left, width: "245px"}
		$("#calculateur-tele").css(cssObj);
	}else{
		var cssObj = {position: "static"}
		$("#calculateur-tele").css(cssObj)
	}
	
}

// rendre les jeux "disabled" quand on change de selection 
function jeux_disabled(disable, check){
	$("#forfaits-jeux dl:not(.ignore_hover) input:checkbox").attr("disabled", disable).attr("checked", check);
	if(check){
		$("#forfaits-jeux dl:not(.ignore_hover)").addClass("selected");
	}else{
		$("#forfaits-jeux dl:not(.ignore_hover)").removeClass("selected");
	}
	count = $("#forfaits-jeux dl:not(.ignore_hover) input:checked").size()
	$("#nbServicesSelectionnes").val(count);
	$("#compteur-chaine").text(count)
}

// on DOM load
$(function(){
	// fait floater le bloc calculateur de prix t�l� 
	if($("#calculateur-tele").size() != 0 ){
		if(!$.browser.msie || ($.browser.msie && $.browser.version > 6)){
			calculateur_fix_pos()
			$(window).bind("resize", calculateur_fix_pos);
			$(window).bind("scroll", calculateur_fix_pos);
		}
    }
	

	// gestion du click/highlight pour les listes de forfaits/chaines			
	$("#col-big-int form.main-cvc .liste-chaines-forfaits dl:not(.ignore_hover)").click(function(e){
		// s'assurer qu'on a un input dans le DL
		input_click = $(this).find(":input:visible:first");
		if($(input_click).size() == 1){
			// trouve le target du click
			e = e || window.event;
			var targ = e.target || e.srcElement;
			// Safari bug - retourne le parent du text node
			if (targ.nodeType == 3) {
				targ = targ.parentNode;
			}	
			// ignorer les labels et les liens pour �viter des doubles click
			if(targ.nodeName != "LABEL" && targ.nodeName != "A" && targ.parentNode.nodeName != "LABEL"){
				// toggle le checkbox/radio
				if (targ.type !== "checkbox" && targ.type !== "radio" && !$(input_click).attr("disabled")) {
					$(input_click).attr('checked', function() {
						if($(input_click).attr("type") == "radio"){
							return true
						}else{
							return !this.checked;
						}
					});
				}
				// ajout de logique pour la page jeux
				if($(input_click).parents("#forfait-jeux-tele").size() == 1){
					if($(input_click).attr("type") == "checkbox"){
						checked_count = $(input_click).parents("li").find(":checkbox:checked").size(); 
						if(checked_count != 0){
							// ignorer si on coche/decoche le 2ieme checkbox
							if(checked_count==1 && $(input_click).parents("ul").find("dl :radio").parents("dl").hasClass("selected")){
								jeux_disabled(false, false);
							};
							// Coche le radio parent vu que plus de 1 checkbox est cocher
							$(input_click).parents("li").find(":radio:first").attr("checked", "checked");
							$(input_click).parents("ul").find("dl :radio").parents("dl").removeClass("selected");
						}else{
							// Aucun checkbox cocher, reset le form en cochant le premier radio
							$(input_click).parents("ul").find(":radio:first").trigger("click");
						}
					}else if($(input_click).attr("type") == "radio"){
						// Les checkbox ce font d�cocher
						$(input_click).parents("ul").find(":checkbox").attr("checked", "");
						if($(input_click).val()=="-1" || $(input_click).val()=="-2"){
							jeux_disabled(true, false);
						}else{
							jeux_disabled(true, true);
						}
					}
				}
				// highlight
				toggleSelected(input_click, $(input_click).attr("type"));
				// filtrage pour la page jeux
				if(!$(input_click).hasClass("no_count") && !$(input_click).attr("disabled") && $(input_click).attr("type") == "checkbox"){
					updateCheckedCount($(input_click).attr("checked"));
				}
			}
			//IE image in label bug 
			if($.browser.msie && targ.nodeName == "IMG" && targ.parentNode.nodeName == "LABEL"){
				$(targ.parentNode).parent().trigger("click");
			}
		}
	})
	
	
	// update nombre de chaine selectionner - forfait sur mesure
	if($("#nbServicesSelectionnes").val() != undefined && $("#nbServicesSelectionnes").val() != $("#compteur-chaine").text()){
		$("#compteur-chaine").text($("#nbServicesSelectionnes").val());
	}
	
	
	
	
	
	// ToolTip (tt)
	// using flowplayer tools 1.1.3 (http://flowplayer.org/tools/tooltip)
	// library is here : /client/residentiel/static/espace-client-residentiel/js/jquery-1.3.2.min.js
		

	// tooltip pour les d�tails des cha�nes et des forfaits
	$("#col-bigger .liste-chaines-forfaits label strong").each(function(){
		parent_depth = ($("#forfaits-sur-mesure").size() == 1) ? "dd" : "dl";
		tt = $(this).parents(parent_depth).find(".tt-cvc-details")
		if($(tt).length != 0){
			// prep tooltip
			tt_h = $(tt).height()/2 - 30;
			// fading of png alpha in IE is aweful
			tt_effect = ($.browser.msie) ? "toggle" : "fade";
			$(this).addClass("tooltip-trigger");
			
			// activate tooltip
			$(this).tooltip({ 
				tip:tt, 
				effect: tt_effect,
				relative: true, 
				position: 'center right', 
				offset: [tt_h,0], 
				predelay: 150, 
				delay: 500,
				onBeforeShow: function(event, position) {
					// petit Hack pour IE6 pour regler le probl�me de z-index
					// le tooltip est deplacer dans le DOM ver le #doc4
					if($.browser.msie && $.browser.version <= 7){       							
						this.getTip().appendTo($("#content-main")); 
						return true;
					}
				}
			});
		}
	})
/*
	*/
	// tooltip pour les cha�nes non modifiables
	$("#col-bigger .liste-chaines-forfaits :checkbox:disabled, #col-bigger .liste-chaines-forfaits :radio:disabled ").each(function(){
		tt_nm = $(this).parents("dl").find(".tt-cvc-warning")
		if($(tt_nm).length != 0){
			// prep tooltip
			tt_nm_offset = $(tt_nm).height()/2 - 30;
			tt_effect = ($.browser.msie) ? "toggle" : "fade";
			
			// ajoute un div cacher par dessus le checkbox pour agir comme trigger pour le tooltip
			tooltip_parent = $(this).parent()
			$(tooltip_parent).prepend("<div class='tooltip-trigger chkbox-disabled'></div>");
			tooltip_trigger = $(tooltip_parent).find("div.tooltip-trigger");
			parent_offset_top = parseFloat($(tooltip_parent).css("padding-top"));
			parent_offset_left = parseFloat($(tooltip_parent).css("margin-left"));	
			if($.browser.msie && $.browser.version <= 7){ 
				offset = $(tooltip_parent).offset();
				offset_box = $("#content-main").offset();
				offset_top = offset.top - offset_box.top + parent_offset_top;
				offset_left = offset.left - offset_box.left - 5 + parent_offset_left; 
			}else{
				offset_top = parent_offset_top - 5;
				offset_left = 5 + parent_offset_left;
				$(tooltip_parent).parent().css("position", "relative");
			}
			var cssObj = {
				position: "absolute",
				top: offset_top,
				left: offset_left
			}
			$(tooltip_trigger).css(cssObj)
			
			// activate tooltip
			$(tooltip_trigger).tooltip({ 
				tip:tt_nm, 
				effect: tt_effect,
				relative: true, 
				position: 'center right', 
				offset: [tt_nm_offset,0], 
				predelay: 5, 
				delay: 500,
				onBeforeShow: function(event, position) {
					// petit Hack pour IE6 pour regler le probl�me de z-index
					// le tooltip est deplacer dans le DOM ver le #doc4					
					if($.browser.msie && $.browser.version <= 7){ 
						this.getTip().appendTo($("#content-main"));
						return true;
					}
				}
			});
		}
	})
	
	
});