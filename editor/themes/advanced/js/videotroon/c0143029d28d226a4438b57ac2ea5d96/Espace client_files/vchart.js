function vchart_transform_tt_title(content){
// formattage du tooltip
	content = content.split(" - ");
	title =  "<div class='vchart_tt_title'>" + content[0] + "</div>";
	content.shift(); // Get rid of the title
	content.reverse(); // Since last bar value is displayed on top
	
	values = "";
	for (valueIndex=0;valueIndex<content.length;valueIndex++)
		{
			styleIndex = content.length - valueIndex;
			values +=  "<div class='vchart_tt_value'><div class='vchart_bar_v_style_" + styleIndex + "'></div><span>" + content[valueIndex] + "</span></div>";
		}
	
	return title + values;
}

// on Dom load
$(function(){
// tooltips don't work in IE6
	old_browser = $.browser.msie && $.browser.version < 7;				
	if(!old_browser){
		$(".vchart_table").each(function(){
			$(this).find(".vchart_bars td[title]").each(function(index){
				parent_td = $(this);
// add new divs for tooltip trigger
				if($(parent_td).children().size() > 0){
					$(parent_td).wrapInner("<div class='vchart_tt_parent'></div>");
				}else{
// can't wrap nothing, so append instead
					$(parent_td).append("<div class='vchart_tt_parent'></div>");
				}				
				tt_parent = $(parent_td).find(".vchart_tt_parent");
				$(tt_parent).append("<div class='vchart_tt_trigger' style='height:"+$(parent_td).height()+"px'></div>");
// attach hover to tooltip trigger
				$(tt_parent).find(".vchart_tt_trigger").hover(
				  function () { $(this).parents("td").addClass("vchart_hover"); }, 
				  function () { $(this).parents("td").removeClass("vchart_hover"); }
				);
// add new div for tooltip
				$(tt_parent).append("<div class='vchart_tt'>"+vchart_transform_tt_title($(parent_td).attr('title'))+"</div>");
// flag to know when we should flip tooltip because we're in the 2nd half of the chart
				invert_tt = index < Math.floor($(this).parents("table").find(".vchart_bars td:not(.vchart_v_scale)").size() /2);
				tt_dir = (invert_tt) ? "left" : "right";
// IE 7 has a hard time with depths and overlaps, show the tt under the table 
				IE7_glitch = ($.browser.msie && $.browser.version < 8) ? " bottom:-101px;" : "";
				$(tt_parent).find(".vchart_tt").attr("style", tt_dir + ":" + $(parent_td).width() + "px;" + IE7_glitch );
// hide titles so we don't get statrdard title tooltips
				$(parent_td).attr("title", "");						
			});				
		});
	}
	
	
// attach hover for detailed table
	$(".vchart_table_details tbody tr").hover(
	  function () { $(this).addClass("vchart_hover"); }, 
	  function () { $(this).removeClass("vchart_hover"); }
	);
});			
