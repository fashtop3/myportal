//* 
//Title:      videotron.js
//Author:     nurun.com
//Updated:    April 2008

//Content:	- SLIDING NAV
//			- sIFR
//			- COLLAPSE TABLES
//			- IMAGE SIDE NAV
//			- FAQ COLLAPSE DEFINITION LIST
//			- SET SUBNAV HEIGHT
//			- LEGAL COLLPASE DEFINITION LIST 
//			- APPAREIL IMAGE NAV
//*/


// ------[ SLIDING NAV ]------------------------------------------------- //

/**
 * Extend string object with trim whitespace functions
 * Ex.: var myString = "     TEST     TEST     ";
 *      myString = myString.trim();  // 'TEST     TEST'
 *      myString = myString.ltrim(); // 'TEST     TEST     '
 *		myString = myString.rtrim(); // '     TEST     TEST'
 */
 	// Supprime les espaces en d�but ET fin de string
	String.prototype.trim=function(){
		return this.replace(/^\s*|\s*$/g,'');
	};
	// Supprime les espaces en d�but de string
	String.prototype.ltrim=function(){
		return this.replace(/^\s*/g,'');
	};
	// Supprime les espaces en fin de string
	String.prototype.rtrim=function(){
		return this.replace(/\s*$/g,'');
	};
/********** END STRING EXTEND **********/

var VIDEOTRON = (typeof VIDEOTRON === "undefined" || !VIDEOTRON) ? {} : VIDEOTRON;
if (typeof VIDEOTRON.widget === "undefined" || !VIDEOTRON.widget) {
    VIDEOTRON.widget = {};
}
if (typeof VIDEOTRON.util === "undefined" || !VIDEOTRON.util) {
    VIDEOTRON.util = {};
}

var VIDEOTRON_CONTENT = (typeof VIDEOTRON_CONTENT === "undefined" || !VIDEOTRON_CONTENT) ? document.getElementById('content') : VIDEOTRON_CONTENT;

/* Detecting IE */
function isIE() {
	return (/msie/i.test(navigator.userAgent)) && !(/opera/i.test(navigator.userAgent));
}

if(isIE()) {
	try {
      document.execCommand('BackgroundImageCache', false, true);
    } catch(err) {}
}
/**
 * Creates a Panel Menu object
 * @param {String|HTMLElement} id The ID or reference to the parent element
 * @param {Object} userConfig The config object through which the panel menu is customized with optional parameters
 * @requires YAHOO.util.Dom
 * @requires YAHOO.util.Config
 * @return A Panel Menu object
 */
VIDEOTRON.widget.PanelMenu = function (id, userConfig) {
    var key;
    this.containerEl = YAHOO.util.Dom.get(id);
    if (this.containerEl) {
        this.id = this.containerEl.id;
        
        // Set default config values
        this.cfg = {
            CLASSNAME_PANEL: "panel",
            CLASSNAME_TITLE: "title",
            CLASSNAME_CONTENT: "content",
            CLASSNAME_TOGGLE: "toggle",
            CLASSNAME_OPEN: "open",
            CLASSNAME_CLOSED: "closed",
            baseFontSize: 13,
            panelAnimDuration: 0.75,
            contentAnimDuration: 0.75,
            openDelay: 0,
            minWidth: 100,
            marginOfError: 10
        };
        
        // Override defaults with user config values
        if (userConfig) {
            for (key in userConfig) {
                if (userConfig.hasOwnProperty(key)) {
                    this.cfg[key] = userConfig[key];
                }
            }
        }
        
        /* An array of HTMLElements each of which is designated a "panel" containing 
           a title, and contents, (all HTMLElements as well, defined by their class names).*/
        this.panelEls = [];
        
        // An array that will hold the panel objects created by the init function.
        this.panels = [];
    } else {
//        throw "No container defined! Please provide either an ID or HTMLElement reference to the Panel Menu container.";
    }
};

VIDEOTRON.widget.PanelMenu.prototype.initContainer = function () {
    // Initialize parent container
    YAHOO.util.Event.addListener(this.containerEl, "click", this.clickHandler, this, true);
    YAHOO.util.Event.addListener(this.containerEl, "mouseover", this.overHandler, this, true);
    YAHOO.util.Event.addListener(this.containerEl, "mouseout", this.outHandler, this, true);
    YAHOO.util.Event.addListener(this.containerEl, "keyup", this.clickHandler, this, true);
    
    this.containerEl.style.position = "relative";
};

/**
 * Initializes Panels
 */
VIDEOTRON.widget.PanelMenu.prototype.initPanels = function () {
    // Shortcut to the PanelMenu states object
    var state = VIDEOTRON.widget.PanelMenu.state;

    // Cache class names used for fetching
    var el_className = this.cfg.CLASSNAME_PANEL;
    var title_className = this.cfg.CLASSNAME_TITLE;
    var content_className = this.cfg.CLASSNAME_CONTENT;
    var toggle_className = this.cfg.CLASSNAME_TOGGLE;
    
    this.panelEls = YAHOO.util.Dom.getElementsByClassName(el_className, null, this.containerEl);
    var i, j;
    var key;
    var panel;
    var afterPanel;
    
    // Loop over panel HTML Elements and build panel objects using data gleaned from markup
    for (i = 0; this.panelEls[i]; i += 1) {
        // Get the panel element and give it an ID if it doesn't have one
        var el = this.panelEls[i];
        if (!el.id) {
            el.id = VIDEOTRON.util.generateId();
        }
        
        // Get the panel element's sibling panel elements
        var beforeEl = (this.panelEls[i - 1]) ? this.panelEls[i - 1] : null;
        var afterEl = (this.panelEls[i + 1]) ? this.panelEls[i + 1] : null;
        
        // Get the title element and its internal text
        var title = YAHOO.util.Dom.getElementsByClassName(title_className, null, el);
        title = (title && title[0]) || null;
        var title_txt = "";
        if (title) {
            title_txt = VIDEOTRON.util.getText(title);
        }
        
        // Get the content element
        var contents = YAHOO.util.Dom.getElementsByClassName(content_className, null, el);
        contents = contents || null;
        
        // Get the panel's minimum and maximum widths
        var margin = parseInt(YAHOO.util.Dom.getStyle(title, "margin-right"), 10);
        margin = (isNaN(margin)) ? 0 : margin;
        var minWidth = (title && title.offsetWidth + margin) || this.cfg.minWidth;
        var maxWidth = el.offsetWidth;
        
        // Is the panel open or closed by default?
        var panelState = (YAHOO.util.Dom.hasClass(el, "open")) ? state.OPEN : state.CLOSED;
        if (panelState === state.OPEN) {
            YAHOO.util.Dom.addClass(el, this.cfg.CLASSNAME_OPEN);
        } else {
            YAHOO.util.Dom.addClass(el, this.cfg.CLASSNAME_CLOSED);
        }
        
        // Create animation object
        var anim = new YAHOO.util.Anim(el);
        anim.duration = this.cfg.panelAnimDuration;
        anim.method = YAHOO.util.Easing.easeOutStrong; // using easeBothStrong plays havoc with collision detection routine
        
        // Build the panel object and store the properties
        this.panels[el.id] = new VIDEOTRON.widget.Panel(
            {
                el: el,
                beforeEl: beforeEl,
                afterEl: afterEl,
                maxWidth: maxWidth,
                totalMaxWidth: null,
                minWidth: minWidth,
                totalMinWidth: null,
                title: title,
                titleText: title_txt,
                contents: [],
                state: panelState,
                baseFontSize: this.cfg.baseFontSize,
                anim: anim
            });
        
        panel = this.panels[el.id];
        
        // Wire up events after panel is created (to be able to set it as anim scope)
        anim.onTween.subscribe(this.detectCollision, panel, this);
        anim.onStart.subscribe(this.handleContent, panel, this);
        
        // Store content element and its animation object
        for (j = 0; contents[j]; j += 1) {
            if (YAHOO.util.Dom.hasClass(contents[j], toggle_className)) {
                var contentAnim = new YAHOO.util.Anim(contents[j]);
                contentAnim.duration = this.cfg.contentAnimDuration;
                contentAnim.method = YAHOO.util.Easing.easeBothStrong;
                /*contentAnim.attributes.opacity = {
                    from: 1,
                    to: 0
                };*/
                panel.contents.push({
                    el: contents[j],
                    anim: contentAnim
                });
                /* YAHOO.util.Dom.setStyle(contents[j], "opacity", 0);*/
            }
        }
    }
    
    
    /* Go over panels again, this time to set relative widths with siblings which
     * wasn't possible in previous loop as all necessary data wasn't yet collected.
     * Note: This loop doesn't actually iterate over the panels array as it
     *       is a "hash" where the array items are indexed by element IDs rather
     *       than a counter integer. It is therefore impossible to do a reverse
     *       loop on such an array, and so I'm looping over the panel elements 
     *       array instead, which has the same count, and can use the panel
     *       element id as my pointer into the panels array.
     */
    var panelWidth;
    for (i = this.panelEls.length - 1; i >= 0; i -= 1) {
        panel = this.panels[this.panelEls[i].id];
        el = panel.el;
        afterEl = panel.afterEl;
        minWidth = panel.minWidth;
        maxWidth = panel.maxWidth;
        panel.totalMinWidth = minWidth + ((afterEl) ? this.panels[afterEl.id].totalMinWidth : 0);
        panel.totalMaxWidth = maxWidth + ((afterEl) ? this.panels[afterEl.id].totalMaxWidth : 0);
        el.style.position = "absolute";
        el.style.right = 0;
        // Don't size the first panel (serves as background panel, always visible)
        if (i !== 0) {
            if (panel.state === state.OPEN) {
                // If panel is open, give it its maximum width value
                panelWidth = maxWidth;
            } else if (afterEl) {
                /* If panel is not open and has a predecessor, give it its 
                   minimum width value plus its predecessors width value.
                   This compensates for the part of the panel hidden by its
                   predecessor panels. */
                panelWidth = minWidth + afterEl.offsetWidth;
            } else {
                /* If panel doesn't have a predecessor, i.e. is the last in the stack,
                   then simply give it its minimum width value. */
                panelWidth = minWidth;
            }
            el.style.width = panelWidth + "px";
        }
    }
};

VIDEOTRON.widget.PanelMenu.prototype.detectCollision = function (type, status, panel) {
    var marginOfError = this.cfg.marginOfError;
    var width = panel.el.offsetWidth;
    var minWidth = panel.minWidth;
    
    var before = this.panels[panel.beforeEl.id];
    var beforeWidth = null;
    var beforeMinWidth = null;
    if (before) {
        beforeWidth = before.el.offsetWidth;
        beforeMinWidth = before.minWidth;
        // Open collision detection
        if (width > (beforeWidth - beforeMinWidth + marginOfError)) {
            if (!before.anim.isAnimated()) {
                before.move(panel);
            }
        }
    }
    
    var after = this.panels[panel.afterEl.id];
    var afterWidth = null;
    if (after) {
        afterWidth = after.el.offsetWidth;
        // Close collision detection
        if (width < (afterWidth + minWidth - marginOfError)) {
            if (!after.anim.isAnimated()) {
                after.close(this);
            }
        }
    }
};

/**
 * Contains all possible states of a Panel Menu panel.
 * CLOSED: When a panel is in its most compressed form
 * OPEN: When a panel is open (exclusive)
 */
VIDEOTRON.widget.PanelMenu.state = {
    CLOSED: 0,
    OPEN: 1,
    MOVED: 2
};

/**
 * Trigger's the current panel's open method and the close method of the one
 * that comes after it.
 * @param {HTMLElement} panelEl The panel to open
 */
VIDEOTRON.widget.PanelMenu.prototype.openPanel = function (panelEl) {
    var panel = this.panels[panelEl.id];
	var state = VIDEOTRON.widget.PanelMenu.state;
	
    if (!panel.anim.isAnimated()) {
        panel.open(this, true);
    }
    
    var after = this.panels[panel.afterEl && panel.afterEl.id];
    if (after) {
        if (!after.anim.isAnimated()) {
			after.state = state.OPEN;
            after.close(this);
        }
    }
};

VIDEOTRON.widget.PanelMenu.prototype.handleContent = function (type, status, panel) {
    var state = VIDEOTRON.widget.PanelMenu.state;
    var before = this.panels[panel.beforeEl && panel.beforeEl.id];

    // Show or hide the panel's content
    if (panel.state === state.OPEN) {
        panel.openContents();
    } else if (panel.state === state.CLOSED) {
        panel.closeContents();
    }
    
     // If you're about to overlap a "before" panel which is open, then hide its toggle contents
    if (panel.state === state.OPEN && before && before.state === state.OPEN) {
        before.closeContents();
    } else if (panel.state === state.CLOSED && before && before.state === state.OPEN) {
        before.openContents();
    }
};

VIDEOTRON.widget.PanelMenu.prototype.clickHandler = function (e) {
    e = e || event;
    var target = e.target || e.srcElement;
    var delay = this.cfg.openDelay;
    var s_className = this.cfg.CLASSNAME_PANEL;
    
    window.clearTimeout(this.overTimer);
    
    while (target) {
        if (YAHOO.util.Dom.hasClass(target, s_className)) {
            this.openPanel(target);
            break;
        }
        target = target.parentNode;
    }
};

/**
 * Called by the mouseover event of the Panel Menu container element.
 * Finds the panel that's currently under the mouse and hands it off to
 * the openPanel method.
 */
VIDEOTRON.widget.PanelMenu.prototype.overHandler = function (e) {
	//alert(this.id);
    e = e || event;
    var target = e.target || e.srcElement;
    var delay = this.cfg.openDelay;
    var s_className = this.cfg.CLASSNAME_PANEL;
    
    window.clearTimeout(this.overTimer);
    
    while (target) {
        if (YAHOO.util.Dom.hasClass(target, s_className)) {
            this.overTimer = window.setTimeout(
                function (that, el) {
                    return function () {
                        that.openPanel(el);
                    };
                }(this, target),
                delay);
            break;
        }
        target = target.parentNode;
		//alert('loop to parentNode ' + target.id);
    }
};

/**
 * Clears the timer that's waiting to open a panel. This is so
 * that accidental momentary hovers over the nav don't trigger
 * an open event when the mouse hasn't stayed on the panel for
 * the prescribed period of time.
 */
VIDEOTRON.widget.PanelMenu.prototype.outHandler = function (e) {
    e = e || event;
    var target = e.relatedTarget || e.toElement;
    var outOfContainer = true;

    while (target) {
        if (target === this.containerEl) {
            outOfContainer = false;
            break;
        }
        /* Use a try/catch block here because Mozilla gets lost in anonymous 
           elements and throws permission errors when trying to access their 
           properties. */
        try {
            target = target.parentNode;
        } catch (err) {
            break;
        }
    }
    if (outOfContainer) {
        window.clearTimeout(this.overTimer);
    }
};

/**
 * Renders the Panel Menu. Sets the position, states and widths of all panels.
 */
VIDEOTRON.widget.PanelMenu.prototype.render = function () {
    var state = VIDEOTRON.widget.PanelMenu.state;
    var panel;
    
    // Initialize the panel container
    this.initContainer();
    
    // Get and initialize panels
    this.initPanels();
    
    // Make container visible
    this.containerEl.style.visibility = "visible";

    // Open whichever panel is set to open
    for (var i = 0; this.panelEls[i]; i += 1) {
        panel = this.panels[this.panelEls[i].id];
        if (panel.state === state.OPEN) {
            panel.state = state.CLOSED; // otherwise open method will see that it's already open and do nothing
            panel.open(this);
        }
    }
};

/**
 * Identify this object as being a Panel Menu object coupled with its ID
 */
VIDEOTRON.widget.PanelMenu.prototype.toString = function () {
    return "PanelMenu#" + this.id;
};

/**
 * Creates a panel object and appends to it params passed to it from the initPanel method
 * @constructor
 * @param {Object} params An object containing key value pairs
 */
VIDEOTRON.widget.Panel = function (params) {
    var key;
    for (key in params) {
        if (params.hasOwnProperty(key)) {
            this[key] = params[key];
        }
    }
    this.id = "panel-" + this.el.id;
};

/**
 * Opens a panel object by widening its HTML element.
 * @param {Object} panelMenuObj A reference to the panel menu object in order 
 *                              to access the panels collection
 */
VIDEOTRON.widget.Panel.prototype.open = function (panelMenuObj) {
    var state = VIDEOTRON.widget.PanelMenu.state;
    var after;
	var before;
    
	if(this.beforeEl) {
		before = panelMenuObj.panels[this.beforeEl.id];
	}
    // Don't do anything if the panel is already open
    if (this.state === state.OPEN) {
        return;
    } else if (!this.contents.length) {
		this.state = state.OPEN;
		if(before && before.state === state.OPEN) {
			before.close(panelMenuObj);
		}
		return;
    }
    
    this.state = state.OPEN;
    if (this.afterEl) {
        after = panelMenuObj.panels[this.afterEl.id];
    }
    var currWidth = this.el.offsetWidth;
    var newWidth = this.maxWidth;
    
    var containerX = YAHOO.util.Dom.getX(panelMenuObj.containerEl);
    var panelX = YAHOO.util.Dom.getX(this.el);

    if (containerX < panelX) {
        newWidth += (after && after.totalMinWidth) || 0;
    }
    this.anim.attributes.width = {
        from: currWidth,
        to: newWidth,
        unit: "px"
    };
    YAHOO.util.Dom.removeClass(this.el, panelMenuObj.cfg.CLASSNAME_CLOSED);
    YAHOO.util.Dom.addClass(this.el, panelMenuObj.cfg.CLASSNAME_OPEN);
    this.anim.animate();
};

/**
 * Moves a panel element out of the way of an opening sibling by 
 * increasing its width by the amount its sibling is growing.
 * @method
 * @param {Object} after A panel object referring to the panel that comes after
 *                       the one needing to be moved. Passed to gain access to 
 *                       the "after panel's" maxWidth value.
 */
VIDEOTRON.widget.Panel.prototype.move = function (after) {
    this.state = VIDEOTRON.widget.PanelMenu.state.MOVED;
    var currWidth = this.el.offsetWidth;
    var newWidth = this.minWidth + after.maxWidth;
    this.anim.attributes.width = {
        from: currWidth,
        to: newWidth,
        unit: "px"
    };
    this.anim.animate();
};

/**
 * Closes a panel object by shrinking its HTML element.
 * @param {Object} panelMenuObj A reference to the panel menu object in order 
 *                              to access the panels collection
 */
VIDEOTRON.widget.Panel.prototype.close = function (panelMenuObj) {
    var state = VIDEOTRON.widget.PanelMenu.state;
    
    // Don't do anything if panel is already closed
    if (this.state === state.CLOSED) {
        return;
    }
    
    this.state = state.CLOSED;
    var currWidth = this.el.offsetWidth;
    var newWidth = this.totalMinWidth;
    this.anim.attributes.width = {
        from: currWidth,
        to: newWidth,
        unit: "px"
    };
    YAHOO.util.Dom.removeClass(this.el, panelMenuObj.cfg.CLASSNAME_OPEN);
    YAHOO.util.Dom.addClass(this.el, panelMenuObj.cfg.CLASSNAME_CLOSED);
    this.anim.animate();
};

VIDEOTRON.widget.Panel.prototype.closeContents = function () {
    for (var i = 0; this.contents[i]; i += 1) {
        if (this.contents[i].anim) {
            this.contents[i].anim.stop();
            //this.contents[i].anim.attributes.opacity = {to: 0};
            this.contents[i].anim.animate();
        }
    }
};

VIDEOTRON.widget.Panel.prototype.openContents = function () {
    for (var i = 0; this.contents[i]; i += 1) {
        if (this.contents[i].anim) {
            this.contents[i].anim.stop();
            //this.contents[i].anim.attributes.opacity = {to: 1};
            this.contents[i].anim.animate();
        }
    }
};
/**
 * Binds a set of child anchors to a parent container by assigning each
 * anchor's id as a class name of the parent container on hover. When the
 * mouse leaves the anchor, the class name is removed from the parent 
 * container. This allows for the manipulation of a sprite assigned
 * to the parent container relative to the current anchor being hovered on.
 * @param {String|HTMLElement} containerId The ID or reference to an element
 *                                         that is the sprite container within
 *                                         which are the links
 * @param {Object} userConfig An object literal allowing the specification of
 *                            either an ID or class name that contains the 
 *                            links. Valid keys are either "linksContainerId"
 *                            or "linksContainerClassName"
 */
VIDEOTRON.util.spriteNav = function (containerId, userConfig) {
    var containerEl = YAHOO.util.Dom.get(containerId);
    var linksContainerEl;
    var i;
    var a;
    
    if (userConfig) {
        if (userConfig.linksContainerId) {
            linksContainerEl = YAHOO.util.Dom.get(userConfig.linksContainerId);
        } else if (userConfig.linksContainerClassName) {
            linksContainerEl = YAHOO.util.Dom.getElementsByClassName(userConfig.linksContainerClassName, null, containerEl)[0];
        }
    } else {
        linksContainerEl = containerEl;
    }
    var links = linksContainerEl.getElementsByTagName("a");
    for (i = 0; links[i]; i += 1) {
        a = links[i];
        a.onmouseover = function (containerEl) {
            return function () {
                if (!YAHOO.util.Dom.hasClass(containerEl, this.id)) {
                    YAHOO.util.Dom.addClass(containerEl, this.id);
                }
            };
        }(containerEl);
        a.onmouseout = function (containerEl) {
            return function () {
                YAHOO.util.Dom.removeClass(containerEl, this.id);
            };
        }(containerEl);
    }
};

/**
 * Extracts an HTML Element's inner text and returns it. This funciton's
 * purpose is to normalize cross browser methods for doing this into
 * one general purpose function.
 * @param {HTMLElement} el The HTML element whose text needs extracting
 * @return The HTML element's inner text
 * @type {String}
 */
VIDEOTRON.util.getText = function (el) {
    return el.innerText || el.textContent;
};

/**
 * Returns an incremented ID in the form of vid-gen0, vid-gen1, vid-gen(n)...
 * @return A unique string for use as an element ID
 * @type String
 */
VIDEOTRON.util.generateId = function () {
    if (typeof this.genIdCount === "undefined") { this.genIdCount = -1; }
    this.genIdCount += 1;
    return "vid-gen" + this.genIdCount;
};


// ------[ IMAGE SIDE NAV ]------------------------------------------------- //
//var YAHOO;
function showSideNavImage() {
	var sideNavButton = document.getElementById("view-phone");
	if(sideNavButton) {	
		var sideNavList = document.getElementById('side-nav');
		var sideNavImage = document.getElementById('side-nav-image');
		var href = sideNavButton.getElementsByTagName('a');	
		if(href) {	
			href.onclick = function() {
				this.sideNavImage.style.display = (this.sideNavImage.style.display == "block") ? "none" : "block";
			};
		}
	}
}

// ------[ APPAREIL IMAGE NAV ]------------------------------------------------- //
function showNavImage() {
	document.getElementById('side-nav').style.display = 'none';
	document.getElementById('side-nav-image').style.display = 'block';
}

function hideNavImage() {
	document.getElementById('side-nav-image').style.display = 'none';
	document.getElementById('side-nav').style.display = 'block';
}


// ------[ FAQ COLLAPSE DEFINITION LIST ]------------------------------------------------- //
//var YAHOO;
function collapseUncollapseDataList(root) {
	if(root) {
		var dl = root.getElementsByTagName('dl');
		var f = function(e) { 
				this.dl.className = (this.dl.className == "opened") ? "" : "opened";
				e = e || event;
				var target = e.target || e.srcElement;
				if(target.tagName == 'A') { YAHOO.util.Event.preventDefault(e); }
			};
		for (var j=0; j<dl.length; j++) {
			var dt = dl[j].getElementsByTagName('dt');
			dt[0].dl = dl[j];
			dt[0].onclick = f;
		}
	}
}

function faqCollapseDataList() {
	var faq = document.getElementById("collapse");
	if(faq) {
		collapseUncollapseDataList(faq);
	}
	/**
	* permettre d'avoir plus d'une liste collapse (suite au bug avec remoteLoad et les listes de canaux)
	**/
	var faqs = YAHOO.util.Dom.getElementsByClassName("collapse-list");
	if(!faqs) { return; }

	for(var i = 0 ; i < faqs.length; i++){
		collapseUncollapseDataList(faqs[i]);
	}
	/*******/
	if(!faq) { return; }
	//Ajoute la gestion du lien afficher ou cacher toutes les r�ponses
	var showAll = document.getElementById("show-all-answers");
	var hideAll = document.getElementById("hide-all-answers");
	var allDl = document.getElementById("collapse").getElementsByTagName('dl');
	
	if (showAll && hideAll && faq) {
		showAll.onclick = function() {
			for (var j=0; j<allDl.length; j++) {
				allDl[j].className = "opened";
			}
			
			this.className = "hidden";
			hideAll.className = "arrow";
			return false;
		};
		hideAll.onclick = function() {
			for (var j=0; j<allDl.length; j++) {
				allDl[j].className = "";
			}
			
			showAll.className = "arrow";
			this.className = "hidden";
			return false;
		};
		
	}
}		
	

// ------[ LEGAL COLLPASE DEFINITION LIST ]------------------------------------------------- //
//var YAHOO;

function legalCollapseDataList() {
	var legal = YAHOO.util.Dom.getElementsByClassName("legalopened", "div", VIDEOTRON_CONTENT);
	if(legal.length) {
		var mustOpen = (/\#note-[0-9]+/.test(document.location));
		var f = function(e) {
				this.className = (this.className == "legalopened") ? "legal" : "legalopened";
				e = e || event;
				YAHOO.util.Event.preventDefault(e);
			};
			for (var j=0; j<legal.length; j++) {
				// ajoute exception, si la classe "must_stay_open" est pr�sente, ignore la fontionalit� standard
				if(legal[j].className.indexOf("must_stay_open") == -1){
					legal[j].className= (mustOpen) ? "legalopened" : "legal";
					if (legal[j].getElementsByTagName('p')) {
						var p = legal[j].getElementsByTagName('p');
						if (p[j].getElementsByTagName('a')){
							var lien = p[j].getElementsByTagName('a');
							lien = legal[j];
							lien.onclick = f;
						}
					 }
				}
			}
	}	
	// Ajoute la gestion des clicks sur les note-X
	var doClickForAnchors = function() {
		var a = arguments[0];
		var loc=document.location.href.split('#')[0];
		var href = a.getAttribute('href');
		href = (href.indexOf(loc) === 0) ? href.substring(loc.length) : href;
		if(/^\#note-[0-9]+/.test(href)) {
			a.onclick = function(e) {
				YAHOO.util.Dom.getElementsByClassName('legal', 'div', document.body, function() {arguments[0].className = 'legalopened';});
			};
		}
	};
	YAHOO.util.Dom.getElementsByClassName('anchor', 'a', VIDEOTRON_CONTENT, doClickForAnchors);
	YAHOO.util.Dom.getElementsByClassName('anchor-0', 'a', VIDEOTRON_CONTENT, doClickForAnchors);
}		
	
// ------[ COLLAPSE TABLES ]------------------------------------------------- //
VIDEOTRON.util.tableCollapse = function () {
	var collapsingTables = YAHOO.util.Dom.getElementsByClassName("collapse", "table", VIDEOTRON_CONTENT);
	var heading;
	
	if(collapsingTables) {

		for (var i = 0; collapsingTables[i]; i++) {
			var collapsingTable = collapsingTables[i].tBodies[0], c0 = collapsingTables[i].tHead.rows[0].cells[0];
			// R�cup�re les h2
			var h23 = c0.getElementsByTagName("h2");
			// Si pas de h2, r�cup�re les h3
			if(!h23 || !h23[0]) { h23 = c0.getElementsByTagName("h3"); }
			// Si un h2 ou un h3, r�cup�re le premier �l�ment
			if(h23 && h23[0]) { heading = h23[0]; }
			// Sinon conserve la cellule
			else { heading = c0; }
			
			var headingA = document.createElement("a");
			headingA.href = "#";
			headingA.innerHTML = heading.innerHTML;
			headingA.onclick = function (el) {
				return function () {
					if(YAHOO.util.Dom.hasClass(el, "closed")) {
						YAHOO.util.Dom.removeClass(el, "closed");
					} else {
						YAHOO.util.Dom.addClass(el, "closed");
					}
					return false;
				};
			}(collapsingTables[i]);
			heading.innerHTML = "";
			heading.appendChild(headingA);
		}
	}
};


// ------[ CAROUSSEL ]------------------------------------------------- //
function initSlideShowAnim() {
	var caroussel = document.getElementById("presentation-list");
	var carousselWidth = caroussel.offsetWidth;
			
   	var elems = YAHOO.util.Dom.getChildren(caroussel);
	var dlAnims = [];
	var row = 1;
	var f = function () {
			var slideShow = new VIDEOTRON.widget.Slideshow("presentation-list", "slide-controls", {groupBy: 5, slideElNodeName: "div"});
		};
		        
   	for (var i = 0; elems[i]; i += 1) {
   		if (i < 5 && row===1) {
			var dlXY = YAHOO.util.Dom.getXY(elems[i]);
			if(!isIE()) {
				dlAnims.push(new YAHOO.util.Motion(elems[i], {
						opacity: {
							from: 0,
							to: 1
						},
						points: {
							from: [
								dlXY[0] + carousselWidth + (i*200), 
								dlXY[1]
							],
							to: [
								dlXY[0], 
								dlXY[1]
							]
						}
					}
				));
			}
			else {
				dlAnims.push(new YAHOO.util.Motion(elems[i], {
						points: {
							from: [
								dlXY[0] + carousselWidth + (i*200), 
								dlXY[1]
							],
							to: [
								dlXY[0], 
								dlXY[1]
							]
						}
					}
				));
			}
			dlAnims[dlAnims.length-1].duration = 0.4 + (0.2 * i);
			dlAnims[dlAnims.length-1].method = YAHOO.util.Easing.easeOutStrong;
			elems[i].style.visibility = 'visible';
			if(i === 4 || i === elems.length-1) {
		        dlAnims[dlAnims.length-1].onComplete.subscribe(f);
			}
			dlAnims[i].animate();		
   		} else {
			row++;
			elems[i].style.visibility = 'visible';
   		}
   	}
}

function initSlideShowAnimAccueil() {
	var caroussel = document.getElementById("presentation-list");
		
   	var elems = document.getElementsByTagName("dl");
	var dlAnims = [];
	var row = 1;
	var f = function () {
			var slideShow = new VIDEOTRON.widget.Slideshow("presentation-list", "slide-controls", {groupBy:1,slideElNodeName:"div",timer:6000,randomStart:true});
		};
	
   	for (var i = 0; elems[i]; i += 1) {
   		if (i < 5 && row===1) {
			if(!isIE()) {
				dlAnims.push(new YAHOO.util.Motion(elems[i], {
						/*opacity: {
							from: 0,
							to: 1
						}*/
					}
				));
			}
			else {
				dlAnims.push(new YAHOO.util.Motion(elems[i], {
					}
				));
			}
			dlAnims[dlAnims.length-1].duration = 0;
			if (i!=0) {
				elems[i].style.visibility = 'hidden';
			}
			else {
				elems[i].style.visibility = 'visible';
			}
			if(i === 4 || i === elems.length-1) {
		        dlAnims[dlAnims.length-1].onComplete.subscribe(f);
			}
			dlAnims[i].animate();
			elems[i].style.visibility = 'visible';
   		} else {
			row++;
			elems[i].style.visibility = 'visible';
   		}
   	}
}

/**
 * Creates a slideshow with previous/next buttons and pips in between representing
 * the current slide among the total number of slides in the show
 */
VIDEOTRON.widget.Slideshow = function (slidesId, controlbarId, userConfig) {
    this.slidesEl = YAHOO.util.Dom.get(slidesId);
    this.controlBarEl = YAHOO.util.Dom.get(controlbarId);
    var i;
	var f = { 
		over: function () {
			if (!YAHOO.util.Dom.hasClass(this, "hover")) {
				YAHOO.util.Dom.addClass(this, "hover");
			}
		},
		out: function () {
			YAHOO.util.Dom.removeClass(this, "hover");
		},
		down: function () {
			if (!YAHOO.util.Dom.hasClass(this, "active")) {
				YAHOO.util.Dom.addClass(this, "active");
			}
		},
		up: function () {
			YAHOO.util.Dom.removeClass(this, "active");
		}
	};
    if (!this.slidesEl || !this.controlBarEl) { return; }
    
    //this.refreshSlidesElDims();
    this.slidesElDims = {
        xy: YAHOO.util.Dom.getXY(this.slidesEl),
        width: this.slidesEl.offsetWidth,
        height: this.slidesEl.offsetHeight
    };
    
    this.cfg = {
    	slideClassName: "slide",
    	slideElNodeName: "li",
    	timer: 0,
    	groupBy: false,
		randomStart: false
    };
    
	// Define labels and text depending on the document language
	if(document.documentElement.lang == 'fr') {
		this.labels = {
			prevTitle: "Afficher la s�lection pr�c�dente",
			nextTitle: "Afficher la s�lection suivante"
		};
	} else {
		this.labels = {
			prevTitle: "Show previous selection",
			nextTitle: "Show next selection"
		};
	}
		
	for (var key in userConfig) {
		if (userConfig.hasOwnProperty(key)) {
			this.cfg[key] = userConfig[key];
		}
	}
	
	// Manually group sub elements of container into slides (grouped by 5 for example)
	//  This is useful if the markup doesn't have natural regroupments of content
    if (this.cfg.groupBy) {
    	var elems = YAHOO.util.Dom.getChildren(this.slidesEl);
    	for (i = 0; elems[i]; i += 1) {
    		if (i % this.cfg.groupBy === 0) {
    			var groupDiv = document.createElement("div");
    			groupDiv.className = this.cfg.slideClassName;
    			this.slidesEl.appendChild(groupDiv);
   				groupDiv.appendChild(elems[i]);
    		} else {
    			if (groupDiv) {
    				groupDiv.appendChild(elems[i]);
    			}
    		}
    	}
    }
    // Get slides and make them absolute at the same time
    this.slides = YAHOO.util.Dom.getElementsByClassName(
    		this.cfg.slideClassName,
    		this.cfg.slideElNodeName,
    		this.slidesEl,
    		function (el) {
    			el.style.position = "absolute";
		    });
    this.numSlides = this.slides.length;
    
    this.slidesPtr = 0;
    this.currentSlide = function () { 
		return this.slidesPtr+1; 
	};
	
	// Set a random slide on init
    if (this.cfg.randomStart) {
		var rndSlide = Math.floor(Math.random()*(this.numSlides));
		if (rndSlide!=0) {
			this.goTo(1, rndSlide);
		}
	}
	
    // Only create previous and next buttons if there is more than one slide
    if (this.numSlides > 1) {
		// Previous button
        this.prevBtn = document.createElement(this.cfg.buttonNodeName || "a");
        this.prevBtn.innerHTML = "&larr;";
		this.prevBtn.setAttribute ("href","#");
		this.prevBtn.setAttribute ("title", this.labels.prevTitle);
        this.prevBtn.className = this.cfg.prevBtnClass || "prev";
        this.prevBtn.onclick = function (that) {
            return function () {
                that.goTo(-1);
                that.resetTimer();
                return false;
            };
        }(this);
		this.prevBtn.onmouseover = f.over;
		this.prevBtn.onmouseout = f.out;
		this.prevBtn.onmousedown = f.down;
		this.prevBtn.onmouseup = f.up;
        this.controlBarEl.appendChild(this.prevBtn);
    
		// Indicator
        this.indicator = document.createElement(this.cfg.buttonNodeName || "span");
        this.indicator.className = this.cfg.indicatorClass || "indicator";
        this.pips = [];
        for (i=0; i<this.numSlides; i++) {
			var pip = document.createElement("a");
			if ((i+1) === this.currentSlide()) {
				pip.className = "current";
			}
			pip.onclick = function (that, num) {
				return function () {
					if(that.slidesPtr != num) {
						var dir = (that.slidesPtr > num) ? -1 : 1;
						that.goTo(dir, num);
	    	            that.resetTimer();
						return false;
					}
				};
			}(this, i);
			pip.onmouseover = f.over;
			pip.onmouseout = f.out;
			pip.onmousedown = f.down;
			pip.onmouseup = f.up;
			pip.appendChild(document.createTextNode(i + 1));
			pip.setAttribute ("href","#");
			this.indicator.appendChild(pip);
			this.pips.push(pip);
        }
        this.controlBarEl.appendChild(this.indicator);

		// Next button
        this.nextBtn = document.createElement(this.cfg.buttonNodeName || "a");
		this.nextBtn.setAttribute ("href","#");
		this.nextBtn.setAttribute ("title",this.labels.nextTitle);
        this.nextBtn.innerHTML = "&rarr;";
        this.nextBtn.className = this.cfg.nextBtnClass || "next";
        this.nextBtn.onclick = function (that) {
            return function () {
                that.goTo(1);
                that.resetTimer();
                return false;
            };
        }(this);
        this.nextBtn.onmouseover = f.over;
		this.nextBtn.onmouseout = function () {
			YAHOO.util.Dom.removeClass(this, "hover");
		};
		this.nextBtn.onmousedown = f.down;
		this.nextBtn.onmouseup = f.up;
        this.controlBarEl.appendChild(this.nextBtn);
        
        // Get slides out of the way for Safari 2.0.x because they block page links
        for (i=1; this.slides[i]; i++) {
            this.slides[i].style.top = "-3000px";
            this.slides[i].style.left = "-3000px";
        }
    }
    this.goTo(0); // to initialize the first slide if it has a popup
    this.setTimer(); // set automated forward timer
};

VIDEOTRON.widget.Slideshow.prototype.setTimer = function () {
    if (this.cfg.timer > 0) {
		this.slideShowInterval = window.setInterval(
			function (that) {
				return function () {
					that.goTo(1);
				};
			}(this),
			this.cfg.timer
		);
	}
};

VIDEOTRON.widget.Slideshow.prototype.resetTimer = function () {
	if (this.slideShowInterval) {
		window.clearInterval(this.slideShowInterval);
		this.setTimer();
	}
};

VIDEOTRON.widget.Slideshow.prototype.refreshSlidesElDims = function () {
    this.slidesElDims = {
        xy: YAHOO.util.Dom.getXY(this.slidesEl),
        width: this.slidesEl.offsetWidth,
        height: this.slidesEl.offsetHeight
    };
};

VIDEOTRON.widget.Slideshow.prototype.goTo = function (dir, skipTo) {
   this.slidesElDims = {
        xy: YAHOO.util.Dom.getXY(this.slidesEl),
        width: this.slidesEl.offsetWidth,
        height: this.slidesEl.offsetHeight
    };
    
    var outSlide = this.slides[this.slidesPtr];
    var outSlideXY = YAHOO.util.Dom.getXY(outSlide);
    var outSlideWidth = outSlide.offsetWidth;
    var outSlideAnim, inSlideAnim, inSlide, inSlideXY;
    if (dir == -1) {
		if(!isIE()) {
			outSlideAnim = new YAHOO.util.Motion(outSlide, {
					opacity: {
						from: 1,
						to: 0
					},
					points: {
						from: [
							this.slidesElDims.xy[0], 
							this.slidesElDims.xy[1]
						],
						to: [
							this.slidesElDims.xy[0] + this.slidesElDims.width, 
							this.slidesElDims.xy[1]
						]
					}
				}
			);
		}
		else {
			outSlideAnim = new YAHOO.util.Motion(outSlide, {
					points: {
						from: [
							this.slidesElDims.xy[0], 
							this.slidesElDims.xy[1]
						],
						to: [
							this.slidesElDims.xy[0] + this.slidesElDims.width, 
							this.slidesElDims.xy[1]
						]
					}
				}
			);
		}
        outSlideAnim.onComplete.subscribe(
           function (el) {
               return function () {
                   el.style.top = "-3000px";
                   el.style.left = "-3000px";
               };
           }(outSlide)
        );
        outSlideAnim.duration = 1;
        outSlideAnim.method = YAHOO.util.Easing.easeOutStrong;

        this.slidesPtr = (this.slides[skipTo]) ? skipTo : this.slides[this.slidesPtr-1] ? this.slidesPtr-1 : this.numSlides-1;
        inSlide = this.slides[this.slidesPtr];
        inSlideXY = YAHOO.util.Dom.getXY(inSlide);
		if(!isIE()) {
			inSlideAnim = new YAHOO.util.Motion(inSlide, {
					opacity: {
						from: 0,
						to: 1
					},
					points: {
						from: [
							this.slidesElDims.xy[0] - this.slidesElDims.width, 
							this.slidesElDims.xy[1]
						],
						to: [
							this.slidesElDims.xy[0], 
							this.slidesElDims.xy[1]
						]
					}
				}
			);
		}
		else {
			inSlideAnim = new YAHOO.util.Motion(inSlide, {
					points: {
						from: [
							this.slidesElDims.xy[0] - this.slidesElDims.width, 
							this.slidesElDims.xy[1]
						],
						to: [
							this.slidesElDims.xy[0], 
							this.slidesElDims.xy[1]
						]
					}
				}
			);
		}
        inSlideAnim.duration = 1;
        inSlideAnim.method = YAHOO.util.Easing.easeOutStrong;
        
        outSlideAnim.animate();
        inSlideAnim.animate();
        
        
    } else if (dir == 1) {
		if(!isIE()) {
			outSlideAnim = new YAHOO.util.Motion(outSlide, {
					opacity: {
						from: 1,
						to: 0
					},
					points: {
						from: [
							this.slidesElDims.xy[0], 
							this.slidesElDims.xy[1]
						],
						to: [
							this.slidesElDims.xy[0] - this.slidesElDims.width, 
							this.slidesElDims.xy[1]
						]
					}
				}
			);
		}
		else {
			outSlideAnim = new YAHOO.util.Motion(outSlide, {
					points: {
						from: [
							this.slidesElDims.xy[0], 
							this.slidesElDims.xy[1]
						],
						to: [
							this.slidesElDims.xy[0] - this.slidesElDims.width, 
							this.slidesElDims.xy[1]
						]
					}
				}
			);
		}
        outSlideAnim.onComplete.subscribe(
           function (el) {
               return function () {
                   el.style.top = "-3000px";
                   el.style.left = "-3000px";
               };
           }(outSlide)
        );
        outSlideAnim.duration = 1;
        outSlideAnim.method = YAHOO.util.Easing.easeOutStrong;

        this.slidesPtr = (this.slides[skipTo]) ? skipTo : this.slides[this.slidesPtr+1] ? this.slidesPtr+1 : 0;
        inSlide = this.slides[this.slidesPtr];
        inSlideXY = YAHOO.util.Dom.getXY(inSlide);
		if(!isIE()) {
			inSlideAnim = new YAHOO.util.Motion(inSlide, {
					opacity: {
						from: 0,
						to: 1
					},
					points: {
						from: [
							this.slidesElDims.xy[0] + this.slidesElDims.width, 
							this.slidesElDims.xy[1]
						],
						to: [
							this.slidesElDims.xy[0], 
							this.slidesElDims.xy[1]
						]
					}
				}
			);
		}
		else {
			inSlideAnim = new YAHOO.util.Motion(inSlide, {
					points: {
						from: [
							this.slidesElDims.xy[0] + this.slidesElDims.width, 
							this.slidesElDims.xy[1]
						],
						to: [
							this.slidesElDims.xy[0], 
							this.slidesElDims.xy[1]
						]
					}
				}
			);
		}
        inSlideAnim.duration = 1;
        inSlideAnim.method = YAHOO.util.Easing.easeOutStrong;
        
        outSlideAnim.animate();
        inSlideAnim.animate();
    }

    if (this.indicator) {
		for (var i=0; this.pips[i]; i++) {
			if ((i+1) === this.currentSlide()) {
				this.pips[i].className = "current";
			} else {
				this.pips[i].className = "";
			}
		}
    }
};


// ------[ GESTION DES CLICK SUR LES IMAGES DES PAGES DE COMPARAISON ]-------- //
function preparePresentationList() {
	var oList = document.getElementById("presentation-list");
	var ochk;
	var f = function() {
		ochk = document.getElementById(this.getAttribute('for'));
		if(ochk) { 
			ochk.checked = (ochk.checked === '') ? 'checked' : ''; 
		}
	};
	if(oList && oList.className == 'compare') {
		var dls = oList.getElementsByTagName("dl");
		for (var i=0; i < dls.length; i++) {
			// R�cup�re les dt dans les dl
			var dt = dls[i].getElementsByTagName("dt");
			if(dt) {
				//R�cup�re le label dans le premier dt
				var labels = dt[0].getElementsByTagName("label");
				if(labels.length) {
					var forId = labels[0].htmlFor;
					var dds = dls[i].getElementsByTagName("dd");
					if(dds && forId) {
						for(var j=0; j < dds.length; j++) {
							if(dds[j].className == "image") {
								dds[j].setAttribute('for', forId);
								dds[j].onclick = f;
							} // dds.className
						} // for j
					} // dds && forId
				} // labels
			} // dt
		} // for i
	} // oList
}


// ------[ TOP SEARCH INPUT ]------------------------------------------------- //
function setSearchEvent() {
	var oSearchInput = document.getElementById("search-input");
	var oLabelSearch = document.getElementById("search-input-label");
	if(oSearchInput && oLabelSearch) {
		oLabelSearch.style.display = 'block';
		oSearchInput.onfocus = function() {
			oLabelSearch.style.display = 'none';
		};
		oSearchInput.onblur = function() {
			if(this.value.length === 0) { 
				oLabelSearch.style.display = 'block';
			}
		};
	}
}


// -----[ Gestion et g�n�ration des widget tooltips pour les listes ] -----//
var elemExtend = function() {
	return {
		text: null,
		disabled: false,
		className: ''
	};
};
function generateTooltipsFromUl(ulClassName, root) {
	if(!root) {
		root = VIDEOTRON_CONTENT;
	}
	var oUls = YAHOO.util.Dom.getElementsByClassName(ulClassName, "ul", root);
	// alert(oUls.length + " :: " + root);
	var ulTooltips = [];
	var f = function(type, args) {
		var context = args[0];
		this.cfg.setProperty("text", context.tooltip.text);
		this.cfg.setProperty("disabled", context.tooltip.disabled);
		this.cfg.setProperty("width", "auto");
		if(context.tooltip.className) {
			YAHOO.util.Dom.addClass(context.container, context.tooltip.className);
		}
		this.cfg.refresh();
	};
	for(var u = 0; oUls[u]; u++) {
		var oLis = oUls[u].getElementsByTagName("li");
		var tooltipsBox = document.getElementById("tooltips-box");
		// var alltooltips = [];
		var oElems = [];
		for(var i = 0; oLis[i]; i++) {
			var curExtend = new elemExtend();
			curExtend.text = oLis[i].innerHTML;
			curExtend.className = '';
			curExtend.disabled = false;
			oLis[i].tooltip = curExtend;
		}
		ulTooltips = new YAHOO.widget.Tooltip("alltooltips", {context:oLis, container: tooltipsBox});
		ulTooltips.contextTriggerEvent.subscribe(f);
	}
}

// -----[ Gestion et g�n�ration des widget tooltips pour les listes ] -----//
function generateTooltipsForTagName(className, tagName) {
	var cTr2ul = (arguments.length > 2) ? arguments[2] : false;
	var tooltipsBox = document.getElementById("tooltips-box");
	if(tooltipsBox !== null) {
		var oElems = YAHOO.util.Dom.getElementsByClassName(className, tagName, document.body);
		generateTooltipsForElements(oElems, tooltipsBox, cTr2ul);
	}
}
function generateTooltipsWithMoreData(className, tagName, IdOfElementToGenerate ) {

		var bd = document.getElementById("bd");
		
		// YAHOO.util.Dom.addClass(bd, "hasJS"); 
	
		var oElems = YAHOO.util.Dom.getElementsByClassName(className, tagName, document.body);
		var gElems = document.getElementById(IdOfElementToGenerate);
		
		
		for(elem in oElems) {
		
			oElems[elem].onmouseover = function () {
				YAHOO.util.Dom.addClass(gElems, "tooltipsOn");
			};
		
			oElems[elem].onmouseout = function () {
				YAHOO.util.Dom.removeClass(gElems, "tooltipsOn");
			};
		}
	
		
	
}
function generateTooltipsForElements(oElems, tipsBox) {
	var cTr2ul = (arguments.length > 2) ? arguments[2] : false;
	for(var i = 0; oElems[i]; i++) {
		var curExtend = new elemExtend();
		curExtend.text = (cTr2ul === true) ? convertTR2UL(oElems[i]) : (oElems[i].title ? oElems[i].title : oElems[i].innerHTML);
		curExtend.className = (oElems.className && YAHOO.util.Dom.hasClass(oElems[i], 'tip-normal-color')) ? 'normal-color' : '';
		curExtend.disabled = (oElems.className && YAHOO.util.Dom.hasClass(oElems[i], 'disabled-tip')) ? true : false;
		oElems[i].tooltip = curExtend;
	}
	var alltooltips = new YAHOO.widget.Tooltip("alltooltips", {context:oElems, container: tipsBox});
	alltooltips.contextTriggerEvent.subscribe(
		function(type, args) {
			var context = args[0];
			this.cfg.setProperty("text", context.tooltip.text);
			this.cfg.setProperty("disabled", context.tooltip.disabled);
			if(context.tooltip.className) {
				YAHOO.util.Dom.addClass(context.container, context.tooltip.className);
			}
			this.cfg.refresh();
		}
	);
	alltooltips.contextMouseOverEvent.subscribe(
		function(type, args) {
			var context = args[0];
			return !(context.tooltip.disabled);
		}
	);
}

function convertTR2UL(elem) {
	if(elem.TooltipsText) {
		return elem.TooltipsText;
	}
	var li;
	var lis = [], i, duplicate;
	var td;
	var tds = elem.getElementsByTagName('td');
	if(tds && typeof(tds) != 'undefined' && tds.length) {
		for(td in tds) {
			if(tds[td].title) {
				duplicate = false;
				for(i = 0; i < lis.length; i+=1) {
					if(lis[i].toLowerCase() === tds[td].title.toLowerCase()) {
						duplicate = true;
						tds[td].removeAttribute('title');
						break;
					}
				}
				if(!duplicate) {
					lis.push(tds[td].title);
					tds[td].removeAttribute('title');
				}
			}
		}
	}
	if(lis.length) {
		var html = '<ul>';
		for(li in lis) {
			html += '<li>' + lis[li] + '</li>';
		}
		html += '</ul>';
		return html;
	} else {
		return elem.title;
	}
}


// ------[ Generic Popup Window ]------------------------------------------------- //
function popWin(url,w,h,scroll,tools,name,center) {
	var str = "height=" + h + ",innerHeight=" + h;
	str += ",width=" + w + ",innerWidth=" + w;
	if(!center) { center = false; }
	if(!scroll) { scroll = 0; }
	if(!tools) { tools = 0; }
	if(!name) { name = "pop"; }
	if((window.screen) && (center)) {
		var ah = screen.availHeight - 30;
		var aw = screen.availWidth - 10;
		var xc = (aw - w) / 2;
		var yc = (ah - h) / 2;
		str += ",left=" + xc + ",screenX=" + xc;
		str += ",top=" + yc + ",screenY=" + yc;
		}
		window.open(url,name,'toolbar=' + tools + ',location=0,directories=0,status=0,menubar=0,scrollbars=' + scroll + ',resizable=1,' + str).focus();
}

// ------[ Validation de formulaire ]------------------------------------------------- //
/**
 * Creates a Form Validator object
 * @param {HTMLFormElement} Form � valider
 * @param {Object} validation object to validate the form
 * @requires YAHOO.util.Dom
 * @requires YAHOO.util.Event
 * @return A validation function (see public members)
 */
var formValidator = (function(data, validateInfo) {

	/**********
		Private members
	**********/
		var that = this; //R�f�rence au constructeur

		/**
		 *
		 **/
		this.validateRequired = function(input) {
			// Pour les checkbox
			if(input.type == 'checkbox') { return this.checked; }
			// Pour les autres type de input, le value ne doit pas �tre vide
			// v�rifi si le input est vide ou contient seulement des espaces
			return !(input.value.trim() === '');
		};

		this.validateEmail = function(input) {
			var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
			return filter.test(input.value);
		};

		this.validatePhone = function(input) {
			// 555-555-5555
			return (/^\d{3,}-\d{3,}-\d{4,}$/.test(input.value));
		};

		this.validateDateFormat = function(input) {
			// AAAA/MM/JJ
			return (/^\d{4,}\/\d{2,}\/\d{2,}$/.test(input.value));
		};
	
	
	/**********
		Public members
	**********/

	/**
	 * Loop on all elements in the form and apply validation rules 
	 * @param {HTMLFormElement} Form � valider
	 * @param {Object} validation object to validate the form
	 * @requires YAHOO.util.Dom
	 * @requires YAHOO.util.Event
	 * @return Bolean value
	 */
	return (function(data, validateInfo) {
		var from, ancestor, request, el, elem, elems, isRequired, isEmpty, isValide, isError, elem_id;	

		isError = false; // Valeur initial par d�faut
		
		// Supprime tous les class error sur les div
		elems = YAHOO.util.Dom.getElementsByClassName('error', 'div', document.body);
		for(elem in elems) {
			YAHOO.util.Dom.removeClass(elems[elem], 'error');
		}
		// Suprime tous les span avec une class error
		elems = YAHOO.util.Dom.getElementsByClassName('error', 'span', document.body);
		for(elem in elems) {
			elems[elem].parentNode.removeChild(elems[elem]);
		}
		
		request = '';
		// Boucle dans les inputs pour valider leur valeur
		for(el=0; el<data.elements.length; el+=1) {
			elem = data.elements[el];
			isValide = true;
			if(elem.type != 'hidden' && typeof(elem.id) != 'undefined') {
				// Hack pour remplacer les - par des _ pour la recherche dans l'objet validator
				elem_id = elem.id.replace('-', '_');
				
				if(typeof validateInfo[elem_id] != 'undefined') {
				
					if(elem.type == 'text' || elem.type == 'textarea') { 
						elem.value = elem.value.trim();
					}

					// Est-ce que le input est Requis ?
					isRequired = YAHOO.util.Dom.hasClass(elem, 'required');
					// Est-ce qu'il est vide ?
					isEmpty = !that.validateRequired(elem);
					
					// Si il est requis et vide, il n'est pas valide !
					if(isRequired && isEmpty) {
						isValide = false;
					// Si il est requis et PAS vide
					} else if (isRequired || !isEmpty) {
						// S'il y a une fonction de test affect� � ce input, sinon, indique que le ch
						isValide = (typeof validateInfo[elem_id]['test'] != 'undefined') ? that[validateInfo[elem_id]['test']](elem) : true;
					}
					
					// S'il est valide, ajuste le request avec �a valeur
					if(isValide) {
						request += '&' + elem.id + '=' + encodeURIComponent(elem.value);

					// S'il n'est pas valide
					} else {
						// Au moins une erreur � eu lieu
						isError = true;
						// Ajoute les class error sur tous les div.field parents
						from = elem;
						while (from = YAHOO.util.Dom.getAncestorByClassName(from, 'field')) {
							YAHOO.util.Dom.addClass(from, 'error');
							ancestor = from;
						}
						// Ajoute le span d'erreur pour le input
						var sp = document.createElement('span');
						sp.className = 'error';
						sp.innerHTML = validateInfo[elem_id]['error'];
						ancestor.appendChild(sp);
					}
				}
			}
		}
		
		if(!isError) {
			var gotos = document.getElementById('goto');
			if(gotos) {
				var params = gotos.value.split('?');
				gotos.value = params[0] + '?' + request.replace(/^&/, '');
				//alert(goto.value);
			}
			return true;
		}

		return false;
		
	});
	
})();


// ------[ Ajout de select Ann�e et mois dans un formulaire ]------------------------------------------------- //
var updateDateValue = function () {
	var y = document.getElementById('addYear');
	var m = document.getElementById('addMonth');
	var d = document.getElementById('custom_Fin_Sans-fil_Res');
	if(y && m && d) { d.value = y.value + '/' + m.value; }
	//alert(d.value);
};

var makeDateSelect = function () {
	var startYear = __CurYear; // __CurYear est d�fini dans meta-css-js.jsp et provient d'une variable JAVA pour suivre l'ann�e courrante.
	var endYear = startYear + 4;
	var months, _month, _year;
	var i, hidden, addYear, addMonth, lblYear, lblMonth;
	if(document.documentElement.lang == 'fr') {
		months = ['Janvier','F�vrier','Mars','Avril','Mai','Juin','Juillet','Ao�t','Septembre','Octobre','Novembre','D�cembre'];
		_month = 'Mois';
		_year = 'Ann&eacute;e'; // MYST�RE !!! IE 6 ne veut pas d'accent dans cette variable !
	} else {
		months = ['January','February','March','April','Mai','June','July','August','September','October','November','December'];
		_month = 'Month';
		_year = 'Year';
	}
	var container = document.getElementById('add-date-dropdown');
	var opt;
	if(!YAHOO.env.ua.ie) {
		// Cr�e le hidden qui va recevoir la valeur
		hidden = document.createElement('input');
		hidden.type = 'hidden';
		hidden.name = 'custom_Fin_Sans-fil_Res';
		hidden.id = hidden.name;
		hidden.value = startYear + '/01/01';
		// Cr�e le select pour les ann�e
		addYear = document.createElement('select');
		addYear.name = 'addYear';
		addYear.id = addYear.name;
		for(i=startYear; i<=endYear; i+=1) {
			opt = document.createElement('option');
			opt.value = i;
			opt.label = i;
			opt.innerHTML = opt.label;
			addYear.add(opt, null);
		}
		// Cr�e le select pour les mois
		addMonth = document.createElement('select');
		addMonth.name = 'addMonth';
		addMonth.id = addMonth.name;
		for(i in months) {
			opt = document.createElement('option');
			opt.value = (i<9 ? '0' : '') + (parseInt(i)+1) + '/01';
			opt.label = months[i];
			opt.innerHTML = opt.label;
			addMonth.add(opt, null);
		}
		lblMonth = document.createElement('label');
		lblMonth.setAttribute('for', 'addMonth');
		lblYear = document.createElement('label');
		lblYear.setAttribute('for', 'addYear');
	} 
	else {
		// Cr�e le hidden qui va recevoir la valeur
		hidden = document.createElement('<input type="hidden" name="custom_Fin_Sans-fil_Res" id="custom_Fin_Sans-fil_Res" value="' + startYear + '/01/01" />');
		// Cr�e le select pour les ann�e
		addYear = document.createElement('<select name="addYear" id="addYear"></select>');
		for(i=startYear; i<=endYear; i+=1) {
			opt = document.createElement('<option value="' + i + '" label="' + i + '"></option>');
			opt.innerHTML = i;
			addYear.appendChild(opt);
		}
		// Cr�e le select pour les mois
		addMonth = document.createElement('<select name="addMonth" id="addMonth"></select>');
		for(i=0; i<months.length; i+=1) {
			var sValue = (i<9 ? '0' : '') + (parseInt(i)+1) + '/01'; 
			opt = document.createElement('<option value="' + sValue + '" label="' + months[i] + '"></option>');
			opt.innerHTML = months[i];
			addMonth.appendChild(opt);
		}
		lblMonth = document.createElement('<label for="addMonth"></label>');
		lblYear = document.createElement('<label for="addYear"></label>');
	}
	lblMonth.innerHTML = _month;
	lblMonth.className = 'longdesc';
	lblYear.innerHTML = _year;
	lblYear.className = 'longdesc';
	container.appendChild(lblYear);
	container.appendChild(addYear);
	container.appendChild(lblMonth);
	container.appendChild(addMonth);
	container.appendChild(hidden);
	YAHOO.util.Event.addListener('addYear', 'change', updateDateValue);
	YAHOO.util.Event.addListener('addMonth', 'change', updateDateValue);
};

// ------[ Gestion de limite de caract�res dans un TEXTAREA ]------------------------------------------------- //
VIDEOTRON.util.maxCharScout = function(e,obj) {
	var maxCharScoutStringSingular, maxCharScoutStringPlural, a, b;
	var handleFocus = function(e,obj){
		 obj.focused = true;
	};
	var handleBlur = function(e,obj){
		 obj.focused = false;
	};
	if(obj.lang == 'fr') {
		//maxCharScoutStringSingularDebut = "Limite de ";
		maxCharScoutStringSingular = "caract�re disponible";
		maxCharScoutStringPlural = "caract�res disponibles";
	}
	else {
		//maxCharScoutStringSingularDebut = "Limit of ";	
		maxCharScoutStringSingular = "character available";
		maxCharScoutStringPlural = "characters available";
	}
	var oField = document.getElementById(obj.fieldId);
	var oFieldMax = parseInt(obj.fieldMax);	
	YAHOO.util.Event.addListener(oField, 'keydown', doScout);
	YAHOO.util.Event.addListener(oField, 'keyup', doScout);
	if (obj.fieldsToConcat) {
		var oFieldsConcat = [];
		for (a = 0; a < obj.fieldsToConcat.length; a += 1) {
			oFieldsConcat[a] = document.getElementById(obj.fieldsToConcat[a]);
			YAHOO.util.Event.addListener(oFieldsConcat[a], 'keydown', doScout);
			YAHOO.util.Event.addListener(oFieldsConcat[a], 'keyup', doScout);
			YAHOO.util.Event.addListener(oFieldsConcat[a], 'focus',handleFocus,oFieldsConcat[a], true );
			YAHOO.util.Event.addListener(oFieldsConcat[a], 'blur',handleBlur,oFieldsConcat[a], true );
			oFieldsConcat[a].focused = false;
		}
	}
	function ScoutCounter(val) {
		var nbLeftField = document.getElementById(obj.fieldId + "-char-remain");
		var remainValueSpan = document.createElement("span");
		remainValueSpan.id = obj.fieldId + "-char-remain-value";
		if(val <= 1) {
			var remainValueText = document.createTextNode("(" +  val + " " + maxCharScoutStringSingular + ")");
		}
		else {
			var remainValueText = document.createTextNode("(" +  val + " " + maxCharScoutStringPlural + ")");
		}
		remainValueSpan.appendChild(remainValueText);
		nbLeftField.innerHTML = "";
		nbLeftField.appendChild(remainValueSpan);
	}
	function doScout(e) {
		var sCurrentFieldString = "";
		var oFieldString = oField.value;
		var oFieldsConcatString = "";
		var nOtherFields = 0;
		var focusedField = null;
		if(oFieldsConcat) {
			for (b = 0; b < oFieldsConcat.length; b++) {
				oFieldsConcatString += oFieldsConcat[b].value;
			}
		}
		if ((oFieldString.length + oFieldsConcatString.length) > oFieldMax) {
			if(oFieldsConcat) {
				for (b = 0; b < oFieldsConcat.length; b++) {
					if ( oFieldsConcat[b].focused === false){
						nOtherFields += oFieldsConcat[b].value.length;
					}
					else{
						focusedField = 	oFieldsConcat[b];
					}
				}
				if (focusedField !== null){
					nOtherFields += oField.value.length;
				}
			}
			if (focusedField !== null){
				//champ du array
				sCurrentFieldString = focusedField.value.substring(0, oFieldMax - nOtherFields);
				focusedField.value = sCurrentFieldString;
			}
			else{
				//champs du message
				sCurrentFieldString = oField.value.substring(0, oFieldMax - nOtherFields);
				oField.value = sCurrentFieldString;
			}
		}
		else {
			ScoutCounter(oFieldMax - (oFieldString.length + oFieldsConcatString.length));
		}
	}
	doScout();
};


// ------[ Gestion des champs phone SMS ]------------------------------------------------- //
VIDEOTRON.util.addInstField = function(e,obj) {
	var oInstFieldGroup = document.getElementById(obj.instFieldGroup);
	var errors = YAHOO.util.Dom.getElementsByClassName("error");
	var lblDeleteField, lblAddField;
	if(obj.lang == 'fr') {
		lblDeleteField = "Supprimer";
		lblAddField = "Ajouter un destinataire";
		lblTitle = "Supprimer le num�ro de t�l�phone du destinataire";
		}
	else {
		lblDeleteField = "Delete";
		lblAddField = "Add a recipient";
		lblTitle = "Delete a recipient\'s phone number";
	}
	if (oInstFieldGroup) {
		oInstFieldGroupInputs = oInstFieldGroup.getElementsByTagName("input");
		var theDeleteLink = [];
		var forceVisible = false;
		for (a = oInstFieldGroupInputs.length-1 ; a >= 0; a--) {
			oInstFieldGroupInputs[a].parentNode.id = "add-" + oInstFieldGroupInputs[a].id;
			if (errors !== null && oInstFieldGroupInputs[a].value !== "" || forceVisible){
				forceVisible = true;
				oInstFieldGroupInputs[a].parentNode.style.display = "block";
			}
			else if( errors === null){
				oInstFieldGroupInputs[a].value = "";
			}
			theDeleteLink[a] = document.createElement("a");
			theDeleteLink[a].id = "del-" + oInstFieldGroupInputs[a].id;
			theDeleteLink[a].className = "remove";
			theDeleteLink[a].title = lblTitle;
			theDeleteLink[a].style.cursor = "pointer";
			theDeleteLink[a].style.marginLeft = "1em";
			theDeleteLink[a].href = "#";
			theDeleteLink[a].innerHTML = lblDeleteField;
			YAHOO.util.Dom.insertAfter(theDeleteLink[a], oInstFieldGroupInputs[a]);
			YAHOO.util.Event.addListener(theDeleteLink[a], 'click', delInstField);
		}
		var theAddLink = document.createElement("a");
		theAddLink.id = "add-field";
		theAddLink.className = "add";
		if (oInstFieldGroupInputs[oInstFieldGroupInputs.length -1].parentNode.style.display == "block") {
			theAddLink.style.display = "none";
		}
		else {
			theAddLink.style.display = "inline-block";
		}
		theAddLink.style.margin = ".4em 0";
		theAddLink.style.cssFloat = "left";
		theAddLink.href = "#";
		theAddLink.innerHTML = lblAddField;
		oInstFieldGroup.appendChild(theAddLink);
		YAHOO.util.Event.addListener(theAddLink, 'click', addInstField);
	}
	// Fonction supprimer
	function delInstField(e) {
		index = (this.id.substring(this.id.length-1)) -2 ;
		//On remonte les valeurs des champs qui suivent celui qu'on veut supprimer
		for (var i = index; i < oInstFieldGroupInputs.length ; i++){
			if( i < oInstFieldGroupInputs.length -1) {
				fieldState =  YAHOO.util.Dom.getStyle(oInstFieldGroupInputs[i + 1].parentNode, "display");
			}
			else {
				fieldState = "";
			}
			oDivNewMessage = document.getElementById("sms-phone-error-" + (i + 2));
			if (fieldState == "block"){
				//console.debug("gestion des messages");
				oDivOldMessage = document.getElementById("sms-phone-error-" + (i + 3));
				//console.debug(oInstFieldGroupInputs[i].parentNode.id + " " + oDivNewMessage);
				//console.debug(oInstFieldGroupInputs[i +1 ].parentNode.id + " " + oDivOldMessage)
				if ( oDivNewMessage === null && oDivOldMessage !== null  ){
					//console.debug("ici test 1");
					var oMsg = oInstFieldGroupInputs[i + 1].parentNode.removeChild(oDivOldMessage);
					oMsg.id = "sms-phone-error-" + i + 2;
					oInstFieldGroupInputs[i].parentNode.appendChild(oMsg);
					YAHOO.util.Dom.addClass(oInstFieldGroupInputs[i].parentNode, "error");
					YAHOO.util.Dom.removeClass(oInstFieldGroupInputs[i +1].parentNode, "error");
				}
				else if(oDivOldMessage === null && oDivNewMessage !== null ){
					oInstFieldGroupInputs[i].parentNode.removeChild(oDivNewMessage);
					YAHOO.util.Dom.removeClass(oInstFieldGroupInputs[i].parentNode, "error");
				}
				oInstFieldGroupInputs[i].value =  oInstFieldGroupInputs[i +1].value;
			}
			else{
				oDivMessage = document.getElementById("sms-phone-error-" + (i + 2));
				if (oDivNewMessage !== null) {
					oInstFieldGroupInputs[i].parentNode.removeChild(oDivMessage);
				}
				YAHOO.util.Dom.removeClass(oInstFieldGroupInputs[i].parentNode, "error");
				break;
			}
		}
		theAddLink.style.display = "inline-block";
		//une fois les valeurs renn
		oInstFieldGroupInputs[i].value = "";
		oInstFieldGroupInputs[i].parentNode.style.display ="none";
		YAHOO.util.Event.preventDefault(e);
	}
	// Fonction ajouter
	function addInstField(e) {
		var theFieldStates = "";
		for (a = 0; a < oInstFieldGroupInputs.length; a++) {
			theFieldStates = YAHOO.util.Dom.getStyle(oInstFieldGroupInputs[a].parentNode, "display");
			if (theFieldStates == "none") {
				oInstFieldGroupInputs[a].parentNode.style.display = "block";
				if (a  == oInstFieldGroupInputs.length - 1) {
					theAddLink.style.display = "none";
				}
				break;
			}
		}
		YAHOO.util.Event.preventDefault(e);
	}
};

/********** [ Prevent enter submit ] **********/
function overideEnter(e) {
	e = e || event;
	if (YAHOO.util.Event.getCharCode(e) === 13) {
		YAHOO.util.Event.preventDefault(e);
	}
}


// ------[ Gestion des indicateurs d'usage de la consommation sans-fil ]------------------------------------------------- //
VIDEOTRON.util.sommairePourcent = function (e,container){
	container = document.getElementById(container);

	if(container==null) return;
	
	elements = container.getElementsByTagName("li");
	var graphBar, graphPercentage, legend;
	for (var i = 0 ; i < elements.length; i++){
		graphContainer = document.createElement("div");
		graphContainer.className = "graphWrap";
		graphContainer.style.position = "relative";
		graphContainer.style.marginBottom = "0.2em";
		data = YAHOO.util.Dom.getElementsByClassName("usage-data","p", elements[i]);
		if (data.length > 1){
			elements[i].insertBefore(graphContainer,elements[i].getElementsByTagName("p")[1]);
		}
		else{
			elements[i].appendChild(graphContainer);
		}
		for (var j = 0 ; j < data.length ; j++){
			legend = null;
			graphPercentage = null;
			graphBar = document.createElement("div");
			fraction = data[j].getElementsByTagName("span");
			numerator  = fraction[0].innerHTML.split(":");
			denominator = fraction[1].innerHTML;
			if (numerator.length > 1){
				numerator = parseFloat(numerator[0] + "." + (numerator[1] / 60) * 100);
			}
			else if (numerator[0].match("&nbsp;")) {
			    numerator  = (numerator[0].replace(/&nbsp;/g,''));  
			}
			else if (numerator[0].match(",")) {
			    numerator  = (numerator[0].replace(/,/g,''));  
			} 
			else if (numerator[0].match(" ")) {
			    numerator  = (numerator[0].replace(/ /g,''));  
			}
			if (denominator.match("&nbsp;")) {
				denominator  = (denominator.replace(/&nbsp;/g,''));  
			}
			else if (denominator.match(",")) {
				denominator  = (denominator.replace(/,/g,''));  
			} 
			else if (denominator.match(" ")) {
				denominator  = (denominator.replace(/ /g,''));  
			}
			graphBar.value = parseFloat(numerator / denominator) * 100;
			if ((graphBar.value > 0) && (graphBar.value < 1)){
				graphBar.value = 1;
			}
			else {
				graphBar.value = Math.round((numerator / denominator) * 100);
			}
			graphBar.style.zIndex = j;
			graphBar.className = "graph graphBar-" + j ;
			if(j > 0){
				//creer une l�gende lorsqu'il y a plus d'une barre
				
				legend = document.createElement("span");
				legend.className = "legend graphBar-" + j
				
				graphPercentage = document.createElement("span");
				graphPercentage.className = "percentage";
				graphPercentage.innerHTML = "(" + graphBar.value + "%" + ")";
				
				graphContainer.appendChild(graphBar);
				data[j].appendChild (graphPercentage);
				if ( j + 1 < data.length){
					elements[i].insertBefore(legend,data[j+1]);
				}
				else{
					elements[i].appendChild(legend);
				}
				
			}
			else{
				//construire une barre simple
				graphPercentage = document.createElement("div");
				graphPercentage.className = "graphString";
				graphPercentage.innerHTML = graphBar.value + "%";
				graphContainer.appendChild(graphBar);
				graphContainer.appendChild(graphPercentage);
			}
			if (graphBar.value > 100){
				graphBar.style.width = "100%";
				graphBar.className += " graph-plus-100";
				elements[i].className += " graph-plus-100";
				if (legend){
					legend.className += " graph-plus-100";
				}
				if(graphPercentage){
					graphPercentage.className += " graphString-plus-100"; 
				}
			}
			else{
				graphBar.style.width = graphBar.value + "%";
			}
		}
	}
};


// ------[ Gestion des Toggle des tableaux de sommaire details ]------------------------------------------------- //
VIDEOTRON.util.sommaireDetailsCollapse = function(e,obj) {
	var tables = new Array(obj.tableID.length);
	var candidates, nodes;
	var oLabel, tmpHTML, i, j;
	document.documentElement.lang == 'fr';
	if(document.documentElement.lang == 'fr') {
		oLabel = {
			show: 'Voir les d�tails de la communication',
			hide: 'Cacher les d�tails de la communication'
		};
	} else {
		oLabel = {
			show: 'See communication detail',
			hide: 'See communication detail'
		};
	}
	for(i=0; i < tables.length; i++) {
		tables[i] = document.getElementById(obj.tableID[i]);
		if(candidates!=null || tables[i]==null) return;
			else {
			candidates = tables[i].rows;
			nodes = YAHOO.util.Selector.filter(candidates, '[id]');
			for(j=0; nodes[j]; j++) {
				nodes[j].tblId = obj.tableID[i];
				tmpHTML = nodes[j].cells[0].innerHTML;
				nodes[j].cells[0].innerHTML = "<a class='add' id='show-" + nodes[j].id + "' href='#'>" + oLabel.show + "</a>" + tmpHTML;
				nodes[j].style.cursor = "pointer";
				YAHOO.util.Event.addListener(nodes[j], "click", showHide);
				var r = YAHOO.util.Dom.getElementsByClassName(nodes[j].id, "tr", nodes[j].tblId);
				YAHOO.util.Dom.addClass(r, "hidden");
				YAHOO.util.Dom.removeClass(nodes[j], "opened");
			}
		}	
	}
	function showHide(e) {
		e = e || event;
		var target = e.target || e.srcElement;
		if(target.tagName == 'A') {
			YAHOO.util.Event.preventDefault(e);
		}
		var r = YAHOO.util.Dom.getElementsByClassName(this.id, "tr", this.tblId);
		var a = document.getElementById("show-" + this.id);
		if(YAHOO.util.Dom.hasClass(this, "opened")) { 
			YAHOO.util.Dom.addClass(r, "hidden");
			YAHOO.util.Dom.removeClass(this, "opened");
			YAHOO.util.Dom.replaceClass(a, "substract", "add");
			a.innerHTML = oLabel.show;
		}
		else {
			YAHOO.util.Dom.removeClass(r, "hidden");
			YAHOO.util.Dom.addClass(this, "opened");
			YAHOO.util.Dom.replaceClass(a, "add", "substract");
			a.innerHTML = oLabel.hide;
		}
	}
};

/* Landing Box Click */
var containerClick = function(e) {
	var t = document.getElementById(e.id + '-target');
	if(typeof(t) != 'undefined' && t != null) {
		YAHOO.util.Event.preventDefault(e);
		e.style.cursor = 'pointer';
		e.title  = t.title;
		YAHOO.util.Event.addListener(e, 'click', function() {window.location = t.getAttribute('href');});
		if(isIE) { 
			e.style.position = "relative"; 
		}
	}
}

/********** [ Gestion de contenu alternatif ] **********/
function initVisibilityTimer(root) {
	if(!root) { root = document.body; }
	else if(typeof root === 'string') { root = document.getElementById(root); }
	var elems = YAHOO.util.Dom.getElementsByClassName('init-visibility-timer', null, root);
	if(elems.length) {
	    this.execTimer = window.setTimeout(
	        function (el) {
	            return function () {
	                this.showVisibilityItem(el);
	            };
	        }(elems),
	        2000);
		this.showVisibilityItem = function(elems) {
			for(var i=0; elems[i]; i+=1) {
				elems[i].style.visibility = 'inherit'; 
			}
		};
	}
}

var handleSections = function (e,sections){
	if (e === null) { //sur le pageLoad
		currentSection = document.location.hash.substring(1);
	}
	else{ //sur le click
		currentSection = this.href.substring(this.href.indexOf('#')+1);
		//YAHOO.util.Event.preventDefault(e);
	}
	var i = 0;
	if (currentSection === "") {
		i = 1;
	}
	for (i ; i < sections.length ; i++){
		section = sections[i].getElementsByTagName("h2")[0];
		if (section.id == currentSection){
			sections[i].style.display = "block";
		}
		else{
			sections[i].style.display = "none";
		}
	}
};

var handleShowAllSects = function(e, faqRoot){
	items = YAHOO.util.Dom.getElementsByClassName("sub-sect","div",faqRoot);
	showAllFromArray(items,true );
};

var handleShowAllAnswers = function(e, faqRoot){
	items = faqRoot.getElementsByTagName("dd");
	YAHOO.util.Event.preventDefault(e);
	if (faqRoot.className.indexOf("opened") != -1){
		this.innerHTML =  sShowAllAnswersClosed;
		YAHOO.util.Dom.removeClass(faqRoot, "opened");
		showAllFromArray(items, false);
	}
	else{
		this.innerHTML = sShowAllAnswersOpened;
		YAHOO.util.Dom.addClass(faqRoot, "opened");	
		showAllFromArray(items,true);
	}
};

var initializeFaq = function(){
	var aShowAllSects = document.getElementById("showAllSects");
	var aShowAllAnswers = document.getElementById("showAllAnswers");
	var sectionHandlers = YAHOO.util.Dom.getElementsByClassName("nav-sub");
	var sections = YAHOO.util.Dom.getElementsByClassName("sub-sect");
	question = window.location.hash.match(/#q[0-9]*/);
	var i;
	handleSections (null,sections);
	for (i =0; i < sectionHandlers.length; i++ ) {
		YAHOO.util.Event.addListener(sectionHandlers[i],"click", handleSections, sections);
	}
	faqRoot = document.getElementById("faq");
	YAHOO.util.Event.addListener(aShowAllSects,"click", handleShowAllSects, faqRoot);
	YAHOO.util.Event.addListener(aShowAllAnswers,"click", handleShowAllAnswers, faqRoot);
	dtQuestions = faqRoot.getElementsByTagName("dt");
	for (i =0; i < dtQuestions.length; i++ ){
		sQuestion = dtQuestions[i].innerHTML;
		dtQuestions[i].innerHTML = "";
		aExpandLink = document.createElement("a");
		aExpandLink.href="#" + dtQuestions[i].id;
		aExpandLink.innerHTML = sQuestion;
		dtQuestions[i].appendChild(aExpandLink);
		ddAnswer = YAHOO.util.Dom.getNextSibling(dtQuestions[i]);
		if (question !== null && question == "#" + dtQuestions[i].id){
			dtQuestions[i].className="opened";
			ddAnswer.style.display = "block";
		}
		else{
			ddAnswer.style.display = "none";	
		}
		YAHOO.util.Event.addListener(aExpandLink, "click",displayToggle, ddAnswer);
	}
};

var displayToggle = function(e, obj, style){
	if (style !== null) {
		toTest = style;
	}
	else {
		toTest = obj.style.display;
	}
	if (toTest == "block"){
		YAHOO.util.Dom.removeClass(YAHOO.util.Dom.getPreviousSibling(obj), "opened");
		obj.style.display = "none";
	}
	else{
		YAHOO.util.Dom.addClass(YAHOO.util.Dom.getPreviousSibling(obj), "opened");
		obj.style.display = "block";
	}
	if (e !== null) {
		YAHOO.util.Event.preventDefault(e);
	}
};

var showAllFromArray = function (items, bShow){
	if (bShow) {
		style = "none";
	}
	else {
		style = "block";
	}
	for (var i = 0 ; i < items.length ; i ++){
		displayToggle(null, items[i], style);
	}
};


/********** INITIALISATION DES FONCTIONNALIT�S G�N�RIQUE **********/
var initVideotronScript = function () {
	if(!window.OVERLAY_LATE_INIT) {
		legalCollapseDataList();
		setSearchEvent();
		preparePresentationList();
		showSideNavImage();
		faqCollapseDataList();
	}
}
YAHOO.util.Event.addListener(window,"load",initVideotronScript);	
