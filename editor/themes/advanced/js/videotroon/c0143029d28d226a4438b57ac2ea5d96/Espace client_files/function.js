function switchLangue(module, langid, lastid, strutsPage, authentifie) {
	if (document.location.href.indexOf("/user-management/") != -1) {	    
		changeModuleLanguage("/user-management/","lang", langid);
		return;
	}	   		
	var template = document.location.href;
	var	lastParam = ( lastid == "/fr/" ) ? "lang=FRENCH" : "lang=ENGLISH";
	var	langParam = ( langid == "/fr/" ) ? "lang=FRENCH" : "lang=ENGLISH";
	var secur = (template.indexOf("/secur/") != -1) ? "/secur" : "";

	if(strutsPage != null && strutsPage != "" && strutsPage.indexOf(".do") == -1 && template.indexOf(".do") != -1) {
		template = "/" + module + secur + "/Forward.do?to=" + strutsPage + "&" + langParam;
	}
	else {
		if (template.indexOf(".do") != -1 && template.indexOf("/Forward.do") == -1) {
			if(template.indexOf("ShortcutLogin.do") != -1) {
			
				if ( authentifie == "N") {
					template = "/" + module + "/Forward.do?to=" + langid + "votre_compte/index.jsp" + "&" + langParam;
				} else  {
					template = "/" + module + "/secur/Forward.do?to=/secur" + langid + "votre_compte/LoginOk.jsp" + "&" + langParam;
				}
			}
			else if (template.indexOf(lastParam) == -1) {
			   template += ((template.indexOf("?") == -1)? "?" : "&") + lastParam;
			}
		}
	}		
	template = template.replace(lastParam,langParam);
	template = template.replace(lastid,langid);
	document.location.href = template;
	return;
}

function changeModuleLanguage(moduleId, langParam, langId) {
//  alert("function.js:changeModuleLanguage");
    var langValue = (langId == "/fr/") ? "fr" : "en";
    var httpParam = "?" + langParam + "=";
	var forme = findForm(moduleId);
//  alert("forme = :" + forme);
	if (forme != null) {
	    //La page du module contient une forme.
	    //On change la langue et on soumet la forme.
        forme.elements[langParam].value = langValue;
//      alert("forme.lang = :" + forme.elements[langParam].value);
        var formAction = forme.action;
        var p = formAction.indexOf("?");
        if (p == -1) {
        	formAction = formAction + "?";
        } else {
        	formAction = formAction + "&";
        }
        if (formAction.indexOf("langSwitched") == -1) {
           formAction = formAction + "langSwitched=true";
        }
        forme.action = formAction;
//      alert("forme.action = :" + forme.action);        
        // Change la langue sans effectuer le traitement de la forme.
    	forme.submit();          
	} else {
	    //La page du module ne contient pas de forme.
	    //Si le param�tre de langue est pr�sent dans le url
	    //on le modifie. Sinon, on l'ajoute au url.
	    //Ensuite, on fait un http get.
        var result = document.location.href;
	    var p = result.indexOf(httpParam);
	    if (p == -1) {
	        httpParam = "&" + langParam + "=";
	    	p = result.indexOf(httpParam);
	    }
	    if (p != -1) {
	    //  langParam est dans le url du navigateur. On le modifie.
	    	var s = result.substring(p, (p + httpParam.length + 2));
	    	var newParam = httpParam + langValue;
	   		result = result.replace(s,newParam);
		} else {
	    //  langParam n'est pas le url du navigateur. On l'ajoute.		
		    if (result.indexOf("?") == -1) {
		    	result = result + "?";
		    } else {
	   	    	result = result + "&";
		    }
		    result = result + "lang=" + langValue;
		}
		if (result.indexOf("langSwitched") == -1) {
		    result = result + "&langSwitched=true";
		}
   		document.location.href = result;		
	}    
}

//Retourne la premi�re forme du document destin�e au module pass� en param�tre.
function findForm(moduleId) {
    var forme = null;
	var i = 0;
	var fAction = null;
	for (i=0; i < document.forms.length; i++) {
	    fAction = document.forms[i].action;
		if (fAction.indexOf(moduleId) != -1) {
			forme = document.forms[i];
			break;
		}
	}
	return forme;
}


function setFieldByName(elementName, index, newValue) {
	var x=getFieldByName(elementName, index);
	if (x != null) {
		x.value = newValue;
	}
}

function getFieldByName(elementName, index) {
	var x = document.getElementsByName(elementName);
	if (x.length > 0) {
		if ((index >= 0) && (index < x.length)) {
			alert("x[index].form = " + x[index].form);
			if (x[index].form != null) {
				return x[index];
			}
		}		
	}
	return null;
}

function getFieldValueByName(elementName, index) {
	var x = getFieldByName(elementName);
	if (x != null) {
		return x.value;
	}
	return null;
}


function setFieldById(elementId, newValue) {
	var x=getFieldById(elementId);
	if (x != null) {
		x.value = newValue;
	}
}

function getFieldValueById(elementId) {
	var x=getFieldById(elementId);
	if (x != null) {
		return x.value;
	}
	return null;
}

function getFieldById(elementId) {
	var x=document.getElementById(elementId);
	if ((x != null) && (x.form != undefined)) {
		return x;
	}
	return null;
}


/*------------ ddc 3108 ------------------*/

function swap(id,isOn,lang) {
	if (isOn == "on") return;
	state = (document["n" + id].src.indexOf("_on") == -1) ? "_on" : "_off" ;
	document["n" + id].src = "/services/static/" + lang + "/img/nav" +  state + "_" + id  +".gif"
}

function pop(url,winname,w,h,sc){
	self.name = "opener"; 
	if (sc==1){w+=16}
	var winleft = (screen.width - w) / 2;
	var wintop = (screen.height - h) / 2;
		
	if(url.indexOf("?") != -1){url = url + "&popup=true";}
	else {url = url + "?popup=true";}
			
	pfWin = window.open(url, winname, 'width='+w+',height='+h+',left='+winleft+',top='+wintop+',screenX=0,screenY=0,directories=0,resizable=1,location=0,menubar=0,scrollbars='+sc+',status=0,toolbar=0');
	pfWin.focus();
}

/* Fonction popWin. R�plique exacte du script de popup qui se trouve dans Main.js afin que la fonction soit disponible sur les anciennes pages du site (section TPC et page contact) */
function popWin(url,w,h,scroll,tools,name,center) {
	var str = "height=" + h + ",innerHeight=" + h;
	str += ",width=" + w + ",innerWidth=" + w;
	if(!center) var center = false;
	if(!scroll) scroll = 0;
	if(!tools) tools = 0;
	if(!name) name = "pop";

	if((window.screen) && (center)) {
		var ah = screen.availHeight - 30;
		var aw = screen.availWidth - 10;

		var xc = (aw - w) / 2;
		var yc = (ah - h) / 2;

		str += ",left=" + xc + ",screenX=" + xc;
		str += ",top=" + yc + ",screenY=" + yc;
		}
		
		window.open(url,name,'toolbar=' + tools + ',location=0,directories=0,status=0,menubar=0,scrollbars=' + scroll + ',resizable=1,' + str).focus();
	}

function closeWindow() {
	timer = setTimeout('window.close();', 10);
}

function printMe() {
	window.focus();
	window.print();
}

function goopage(page) {
	opener.location.href=page
	window.close();
}

function checkURL() {
	var idx = document.URL.indexOf('?');
	
	//Demande 169813
	//dans la fonction pop() il y a un &popup=true qui peut �tre ajouter on ne doit pas en prendre compte
	var idx2 = document.URL.indexOf('&');
	
	if (idx2 == -1) {
		idx2 = document.URL.length;
	}
	
	var truc = document.URL.substring(idx+1, idx2);
	
	return truc;
}

function getCookie(Name)  {
	var search = Name + "="
	if (document.cookie.length > 0) {
	offset = document.cookie.indexOf(search)
		if (offset != -1) {
		offset += search.length
		end = document.cookie.indexOf(";", offset)
			if (end == -1)
            end = document.cookie.length
    		return unescape(document.cookie.substring(offset, end))
		}
	}
}
function setCookieValue(value) {
	var today = new Date()
	var expires = new Date()
	expires.setTime(today.getTime() + 1000*60*60*24*365)
	setCookie("isFlash", value, expires);
}

function setCookie(name, value, expires) {
	document.cookie = name + "=" + escape(value) + ((expires == null) ? "" : ("; expires=" + expires.toGMTString())) + "; path=/";
}
function DetectFlash6() {
	HasFlash = false;
	if (navigator.mimeTypes) {
		if (navigator.mimeTypes.length > 0) {
			for (i = 0 ; i < navigator.plugins.length ; i++ ) {
				if (navigator.plugins[i].name.indexOf('Shockwave Flash') >= 0) {
					if (navigator.plugins[i].description.indexOf('7.') >= 0) HasFlash = true ;
				}
			}
		}
	}
	if((navigator.appVersion.indexOf("MSIE") != -1) && (navigator.appVersion.indexOf("Windows") != -1)){ // don't write vbscript tags on anything but ie win
		document.write('<SCR' + 'IPT LANGUAGE=VBScript\> \n');
		document.write('on error resume next \n');
		document.write('HasFlash = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.7"))) \n');
		document.write('</SCR' + 'IPT\> \n'); // break up end tag so it doesn't end script
	}
}
var VersionNeeded = 7;

var IsFlash = false;
var IsIEWin = (navigator.appVersion.indexOf("MSIE") != -1) && (navigator.appVersion.indexOf("Windows") != -1);
if (navigator.mimeTypes) {
	if (navigator.mimeTypes.length > 0) {
		for (i = 0 ; i < navigator.plugins.length ; i++ ) {
			var shockIndex = navigator.plugins[i].name.indexOf('Shockwave Flash');
			if (shockIndex>=0) {
				var versionString = navigator.plugins[i].description.substr(shockIndex+16,30);
				var spaceIndex = versionString.indexOf(' ');
				versionString = versionString.substr(0, spaceIndex);
				var versionNumber = Number(versionString);
				if(versionNumber >= VersionNeeded){
					IsFlash = true;
					break;
				}
			}
		}
	}
}
if(IsIEWin){
	document.write('<SCR' + 'IPT LANGUAGE=VBScript\> \n');
	document.write('on error resume next \n');
	document.write('IsFlash = (IsObject(CreateObject("ShockwaveFlash.ShockwaveFlash.'+VersionNeeded+'"))) \n');
	document.write('</SCR' + 'IPT\> \n'); // break up end tag so it doesn't end script
}
function setDate() {
	document.forms.reminder.Fin_InternetParticulier.value = document.forms.reminder.Fin_month.value + "/" + document.forms.reminder.Fin_year.value ;
	}
function setDateAff() {
	document.forms.reminder.Fin_TelephonieAffaire.value = document.forms.reminder.Fin_month.value + "/" + document.forms.reminder.Fin_year.value ;
	}
	
	
function trim(s) {
/******************************************************
 Enl�ve de s, les blancs non significatifs.
 Param�tres :
    s - la chaine � traiter.
 Retourne :
    s sans caract�res blancs au d�but et la fin.
*******************************************************/

       return LTrim(RTrim(s));
}


function LTrim(s) {
/******************************************************
 Enl�ve les blancs au d�but de s.
 Param�tres :
    s - la chaine � traiter.
 Retourne :
    s sans caract�res blancs au d�but.
*******************************************************/
   if (s == null) {
      return null;
   } else {
       return s.replace( /^\s*/, "" );
   }
}


function RTrim(s) {
/******************************************************
 Enl�ve les blancs � la fin de s.
 Param�tres :
    s - la chaine � traiter.
 Retourne :
    s sans caract�res blancs � la fin.
*******************************************************/
   if (s == null) {
      return null;
   } else {
       return s.replace( /\s*$/, "" );
   }
}
	
function openFull(p,n) {
	var r = null;
	r = window.open(p,n,'width='+screen.width+',height='+screen.height+',top='+0+',left='+0+','+'scrollbars=no,toolbar=no,location=no,status=no,menubar=no,resizable=yes,dependent=no');
	if(r!=null && window.focus) r.window.focus();	
} 







