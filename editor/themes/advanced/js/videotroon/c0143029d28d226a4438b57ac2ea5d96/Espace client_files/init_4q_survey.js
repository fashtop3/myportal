/*
 * Script pour inclure le sondage iPerception 4Q
 */
// --> true pour bloquer l'acces a l'interne
// --> false pour les tests
if(ips_ip_interne !== true){		
	var ips = {
			proto : ('https:'==location.protocol) ? "https:" : "http:",
			sdfc : 	"a0b35c4f-107102-5e2bc7f7-b037-4a58-8e10-0fe369e2a376",
			lang : 	($("body").hasClass("en")) ? "1" : "10"
	};
	ips.uri = ips.proto + "//ips-invite.iperceptions.com/webValidator.aspx?sdfc=" + ips.sdfc + "&lID=" + ips.lang + "&loc=STUDY&cD=90&rF=False&iType=1&domainname=0";
	$(function(){
			$("body").append("<script src='" + ips.uri + "' type='text/javascript' defer='defer'><\/script>");
	});
}
