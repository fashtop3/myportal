function setUserField(cookieName) {
	var cookieUser;
	if (cookieName != "loginAll") {
		cookieUser = getCookie(cookieName);
		if(cookieUser != undefined){
			document.forms[cookieName + 'Form'].user.value=cookieUser;
			document.forms[cookieName + 'Form'].memoriser.checked = true;
		}
		document.forms[cookieName + 'Form'].user.focus();
	} else {
		cookieUser = getCookie("monCompte");
		if(cookieUser != undefined){
			document.forms['monCompteForm'].user.value=cookieUser;
			document.forms['monCompteForm'].memoriser.checked = true;
		}

		cookieUser = getCookie("courrielWeb");
		if(cookieUser != undefined){
			document.forms['courrielWebForm'].user.value=cookieUser;
			document.forms['courrielWebForm'].memoriser.checked = true;
		}

		cookieUser = getCookie("softphone");
		if(cookieUser != undefined){
			document.forms['softphoneForm'].user.value=cookieUser;
			document.forms['softphoneForm'].memoriser.checked = true;
		}
		
		setUserFieldSofPhoneAffaires();
		
		
	}
}
/**---- ddc_3108 ---------------------------------------
Pour cette ddc, la fonction SSO s'applique aux applications Espace Client et Courriel Web.
Lorsque la fonction SSO sera appliqu�e aux autres applications, il suffira de modifier
cette fonction.
*/
var securHost = null;
function initForme(formId, targetUrl) {
	var disabledDiv = new Array();
	if (formId == 'EC') {
	    enabledDiv = "accesMonCompte";
		disabledDiv[0] = "accesCourrielWeb";
		disabledDiv[1] = "accesSoftphone";		
		toggleDiv(enabledDiv, disabledDiv);
	} else {
	    if (formId == 'CW') {
   		    var ssoCookie = getCookie("iPlanetDirectoryPro");
	        if (ssoCookie != null) {
	            window.location.href = targetUrl;
	        } else {
			    enabledDiv = "accesCourrielWeb";
				disabledDiv[0] = "accesMonCompte";
				disabledDiv[1] = "accesSoftphone";
				toggleDiv(enabledDiv, disabledDiv);	    
			}
	    }
	}
}

function toggleDiv(enabledDiv, disabledDiv) {
	document.getElementById(enabledDiv).style.display = 'block';
	for (var i=0; i < disabledDiv.length; i++) {
		document.getElementById(disabledDiv[i]).style.display = 'none'; 
	}
}

function removeXSiteScripting(value) {
	var tmp = value.replace(/[{}\[\];\/\\]/g, "");
	tmp = tmp.replace(/</g, "&lt;");
	return tmp.replace(/>/g, "&gt;");
}

function initPageSoftphoneFromCookie(userIdCookieName, userIdField, userIdCheckBox, userIdRadioButton, userIdPsw) {
	
	var cookieValue = getCookie(userIdCookieName);	
	var field = document.getElementById(userIdField);
	var psw = document.getElementById(userIdPsw);
	var cb = document.getElementById(userIdCheckBox);	
	var rb = document.getElementsByName(userIdRadioButton);
	
  	if (psw != null) psw.value = "";
	if (cookieValue != undefined) {
		var fieldArray  = cookieValue.split("|");
		cookieValue = fieldArray[0];
		var rbValue = fieldArray[1];
    
		if (field != null) {
	    	field.value = removeXSiteScripting(cookieValue);	
	    	if (cb != null) {
  		   		cb.checked=true; 	
	    	}
	    	if (rb != null) {
	    		var domain = document.getElementById("domain");
	    		if( domain != null ) domain.value= rbValue;
	    		for (x=0; x < rb.length; x++) {	    			 
	    			if (rb[x].value == rbValue ){
	    			  rb[x].checked = true
	    			}
	    		}
	    	}
		}		
	} else {
		if (field != null) {
	    	field.value = "";	
	    	if (cb != null)	cb.checked=false; 	
	    	if (rb != null)	rb[0].checked = true;	  
    	}			    
	}
}


function initPageFromCookie(userIdCookieName, userIdField, userIdCheckBox) {
	var cookieValue = getCookie(userIdCookieName);
	var field = document.getElementById(userIdField);
	var cb = document.getElementById(userIdCheckBox);
    if (cookieValue != undefined) {
		if (field != null) {
	    	field.value = removeXSiteScripting(cookieValue);
	    	if (cb != null) {
  		   		cb.checked=true; 	
	    	}
    	}		
	} else {
    	if (cb != null) {
	   		cb.checked=false; 	
    	}	    
	}
}

function setCookieEnabled(field) {
var elem = document.getElementsByName(field);
elem[0].value = navigator.cookieEnabled;
}

function initAccesDirect() {
	initPageFromCookie('monCodeUtilEC', 'monCodeUtilEC', 'CBsauverCodeUtilEC');
	initPageFromCookie('monCodeUtilCW', 'monCodeUtilCW', 'CBsauverCodeUtilCW');
}

function getCodeUtilNouveauClient(userIdField){
	var field = document.getElementById(userIdField);	
	return field.value;
}

function initNouveauClient(userIdField, value){
	if(value != '' ){
		var field = document.getElementById(userIdField);	
		field.value = value;	
	}
}
	
function saveUserId(userIdCookieName, userIdField, userIdCheckBox) {
	var field = document.getElementById(userIdField);
	if (field != null) {
	    var cb = document.getElementById(userIdCheckBox);
	    if (cb != null) {
  	       var expires = -1;
	       if ((cb.checked == true) && (trim(field.value) != "")) {
	       	 var now = new Date();
			 expires = now.getTime() + 365 * 24 * 60 * 60 * 1000;	       
	       }
	    }
	    if ((userIdCookieName != null) && (trim(userIdCookieName) != "")) {
	    	setCookieExpires(userIdCookieName, removeXSiteScripting(field.value), expires);
	    }	
	}
}

function httpsSubmit(formeId) {
  var forme = document.getElementById(formeId);
  if (forme.action.indexOf('/secur/') >= 0) {
     if (securHost != null) {
        var loc = window.location;
        forme.action = correctMisspelledUrl(securHost) + loc.pathname + loc.search;
     }
  }
  forme.submit();
}

function setSecurHost(host) {
  securHost = host;
}
function correctMisspelledUrl(url) {
    var cUrl = url;
	if (url != null) {
	    cUrl = trim(url);
	    if (cUrl.length > 0) {
	        if (cUrl.charAt(cUrl.length - 1) == '/') {
	        	cUrl = cUrl.substring(0, cUrl.length - 1); 
	        }
	    }	   
	}
	return cUrl;
}

function initPwIndicator(pwField) {
	var field = document.getElementById(pwField);
	if (field != null) {
		updatePwIndicator(field.value);
	}	
}

function updatePwIndicator(pwValue) {
	var pwIndicatorDiv = document.getElementById("mpFort");
	if (pwIndicatorDiv == null) {
		return;
	}
	var strong = ((pwValue != null) && (pwValue.length >= 6));
	if (strong) {
		var pattern = new RegExp("[a-zA-Z]");
		strong = strong && pattern.test(pwValue);
		if (strong) {
			pattern = new RegExp("[0-9]");
			strong = strong && pattern.test(pwValue);		
		}
	} 
	var vElem = null;
	var hElem = null;
	if (strong) {
		vElem = document.getElementById("mpFort");
		hElem = document.getElementById("mpFaible");
	} else {
		vElem = document.getElementById("mpFaible");
		hElem = document.getElementById("mpFort");	
	}
	vElem.style.display = 'block';
	hElem.style.display = 'none';	
}

  
function setSSOOverlay(displayValue) {
   document.getElementById('container-overlay').style.display = displayValue; 
   var divElem = document.getElementById('liensRapides');
   if (divElem != null) {   
	 if (displayValue == 'none') {
        divElem.style.display = 'block';
     } else {
        divElem.style.display = 'none';
     }   
   }  
}
   

// Highlights the input and select fields that have the names passed in the array
function highlightErrorFields(fieldNames) {
	for(var x = 0; x < fieldNames.length; x++) {
		var field = $('input[name=\'' + fieldNames[x] + '\'], select[name=\'' + fieldNames[x] + '\']');
		
		if(field.is('input')) field.addClass('errorlightinput');
		else if(field.is('select')) {
		
			if ($.browser.msie) {
				field.wrap('<span class="errorlightselect"></span>');
			} else {
				field.addClass('errorlightselect');
			}
		}
	}
}

//---- ddc3108 ----------------------------------


function setUserFieldSofPhoneAffaires()

{
	
	if(document.forms['softphoneAffairesForm']!=undefined && getCookie("softphoneAffaires") != undefined)
	
	{
	
		document.forms['softphoneAffairesForm'].user.value=getCookie("softphoneAffaires");
		document.forms['softphoneAffairesForm'].memoriser.checked = true;
			
	}
}


function getCookie(cookieName)
{
	var search = cookieName + "="   
	if (document.cookie.length > 0) {
		offset = document.cookie.indexOf(search)
		if (offset != -1) {
			offset += search.length
			end = document.cookie.indexOf(";", offset)
			if (end == -1) end = document.cookie.length
			return unescape(document.cookie.substring(offset, end))
		}    
	}
}

function updateExpireCookieDate(cookieName)
{
	var now = new Date();
	var expires = now.getTime() + 365 * 24 * 60 * 60 * 1000;

	if (document.forms[cookieName + 'Form'].memoriser.checked == false)
	{
		expires =  - 1;
	}
	setCookieExpires(cookieName, document.forms[cookieName + 'Form'].user.value, expires);
}

function setCookieExpires(name, value, expires) {
	document.cookie = name + "=" + escape(value) + "; expires=" + new Date(expires).toGMTString() + "; path=/";
}


/**
 Ca l`air redondant mais j`ai s�par� les m�thodes affaires de r�sidentiels
 car actuellement on utilise le m�me fichier JS mais puisque on fait la 
 distinction entre application affaires et r�sidentiel, je pr�vois aussi 
 qu`un jour on va s�parer le fichier JS, dans ce cas il suffira de copier
 le code affaires dans le nouveau fichier JS
**/



/*********************************************************
  telechargementSoftphoneAffaires
**********************************************************/
function telechargementSoftphoneAffaires(locale, softphoneUrlDomain, softphoneReturnUrl)
{

	if (isValideSoftPhoneForm(document.forms['softphoneAffairesForm'], getLangue(locale))) {

			document.forms['softphoneAffairesForm'].action = getSoftPhoneTelechargementURL(softphoneUrlDomain,document.forms['softphoneAffairesForm'],getLangue(locale),softphoneReturnUrl);
	        document.forms['softphoneAffairesForm'].userSoftphone.value = document.forms['softphoneAffairesForm'].user.value + "@" + document.forms['softphoneAffairesForm'].domain.value;	    
		    document.forms['softphoneAffairesForm'].submit();   	
	}
}	


/*********************************************************
  telechargementSoftphoneResidentiel
**********************************************************/				
function telechargementSoftphone(locale, softphoneUrlDomain,softphoneReturnUrl)
{

	if (isValideSoftPhoneForm(document.forms['connectSoftphoneForm'], getLangue(locale))) {

			document.forms['connectSoftphoneForm'].action = getSoftPhoneTelechargementURL(softphoneUrlDomain,document.forms['connectSoftphoneForm'],getLangue(locale),softphoneReturnUrl);
	        document.forms['connectSoftphoneForm'].userSoftphone.value = document.forms['connectSoftphoneForm'].user.value + "@" + document.forms['connectSoftphoneForm'].domain.value;
	        window.location.replace( document.forms['connectSoftphoneForm'].action )
	        //document.forms['connectSoftphoneForm'].submit();   
	}
	
}


/*********************************************************
  loginSoftphoneAffaires
**********************************************************/
function loginSoftphoneAffaires(locale, softphoneUrlDomain,softphoneReturnUrl) {
	if (isValideSoftPhoneForm(document.forms['softphoneAffairesForm'], getLangue(locale))) {

			document.forms['softphoneAffairesForm'].action = getSoftPhoneLoginURL(softphoneUrlDomain,document.forms['softphoneAffairesForm'],getLangue(locale),softphoneReturnUrl);
	        document.forms['softphoneAffairesForm'].userSoftphone.value = document.forms['softphoneAffairesForm'].user.value + "@" + document.forms['softphoneAffairesForm'].domain.value;		    
		    document.forms['softphoneAffairesForm'].submit();   
	}

}	

/*********************************************************
  Reinisialisation mot passe residentiel.
**********************************************************/
function reinitPassword(locale, softphoneUrlDomain,softphoneReturnUrl) {
	if (isValideSoftPhoneForm(document.forms['changePasswordSoftphoneForm'], getLangue(locale))) {

			document.forms['changePasswordSoftphoneForm'].action = getSoftPhoneChangePasswordURL(softphoneUrlDomain,document.forms['changePasswordSoftphoneForm'],getLangue(locale),softphoneReturnUrl);
	        document.forms['changePasswordSoftphoneForm'].userSoftphone.value = document.forms['changePasswordSoftphoneForm'].user.value + "@" + document.forms['changePasswordSoftphoneForm'].domain.value;		    
		    document.forms['changePasswordSoftphoneForm'].submit();   
	}
}


/*********************************************************
  Reinisialisation mot passe Affaires.
**********************************************************/
function reinitPasswordAffaires(locale, softphoneUrlDomain,softphoneReturnUrl) {
	if (isValideSoftPhoneForm(document.forms['softphoneAffairesForm'], getLangue(locale))) {

			document.forms['softphoneAffairesForm'].action = getSoftPhoneChangePasswordURL(softphoneUrlDomain,document.forms['softphoneAffairesForm'],getLangue(locale),softphoneReturnUrl);
	        document.forms['softphoneAffairesForm'].userSoftphone.value = document.forms['softphoneAffairesForm'].user.value + "@" + document.forms['softphoneAffairesForm'].domain.value;		    
		    document.forms['softphoneAffairesForm'].submit();   
	}
}





/*********************************************************
 loginSoftphoneResidentiel
**********************************************************/
function loginSoftphone(locale, softphoneUrlDomain,softphoneReturnUrl) {

	if (isValideSoftPhoneForm(document.forms['connectSoftphoneForm'], getLangue(locale))) {

			document.forms['connectSoftphoneForm'].action = getSoftPhoneLoginURL(softphoneUrlDomain,document.forms['connectSoftphoneForm'],getLangue(locale),softphoneReturnUrl);
	        document.forms['connectSoftphoneForm'].userSoftphone.value = document.forms['connectSoftphoneForm'].user.value + "@" + document.forms['connectSoftphoneForm'].domain.value;
	        window.location.replace( document.forms['connectSoftphoneForm'].action )
	        //document.forms['connectSoftphoneForm'].submit();   
	
	}
	//return loginSoftphoneLocal(locale, softphoneUrlDomain,document.forms['connectSoftphoneForm']);
}





/*********************************************************
 Return the langue based on the local value passed as a 
 parmaeter.
**********************************************************/
function getLangue(locale)
{
	return (locale != "en") ? "FRENCH" : "ENGLISH";
}


/*********************************************************
 Validate the sofPhone Form 
**********************************************************/
function isValideSoftPhoneForm(theForm,langue)
{
	
	if (theForm.domain.value == "") 
	
	{
		var domainMsg = ((langue == "FRENCH") ? "Vous devez choisir un domaine" : "Please choose a domain");
		alert(domainMsg);
		return false;
	}
			

	if (theForm.username.value == "")
	 {
		var uidMsg = ((langue == "FRENCH") ? "Vous devez inscrire un nom d'utilisateur" : "Please enter a username");
		alert(uidMsg);
		return false;
	}

	
	if (theForm.password2 == undefined ) {
		if (theForm.password1.value == "") {
			var pwdMsg = ((langue == "FRENCH") ? "Vous devez inscrire un mot de passe" : "Please enter a password");
			alert(pwdMsg);
			return false;
		}	
	}else{
		if (theForm.password1.value == "") {
			var pwdMsg = ((langue == "FRENCH") ? "Vous devez inscrire un mot de passe" : "Please enter a password");
			alert(pwdMsg);
			return false;
		}
		if (theForm.password2.value == "") {
			var pwdMsg = ((langue == "FRENCH") ? "Vous devez confirmer le mot de passe" : "Please re-enter a password");
			alert(pwdMsg);
			return false;
		}
		if (theForm.password1.value != theForm.password2.value) {
			var pwdMsg = ((langue == "FRENCH") ? "Les deux mots de passe ne sont pas identiques" : "Both password must be identical");
			alert(pwdMsg);
			return false;
		}
	}	
	
	
	return true;
}


/*********************************************************
 Construit le login url sur le serveur de Nortel
**********************************************************/
function getSoftPhoneLoginURL(softphoneUrlDomain,theForm,langue,softphoneReturnUrl) {
	var protocol = "https://";
	var domain   = theForm.domain.value;
	var username = "&username=" + theForm.user.value;
	var password = "&password=" + theForm.password1.value;
	var redirect = "&redirect=home";	
	var baseUrl  = "/pa/direct/LoginServlet?lang=" + langue + "&action=2";
	var urlDomain = domain.replace(".ca",".com");
	var returnUrl ="&returnUrl=" +softphoneReturnUrl;
	
	if (softphoneUrlDomain != "") {
		urlDomain = softphoneUrlDomain;
	}
	
	return protocol + urlDomain + baseUrl + username + "@" + domain + password + redirect +returnUrl;

}


/*****************************************************************
 Construit le telechargement url sur le serveur de Nortel
******************************************************************/
function getSoftPhoneTelechargementURL(softphoneUrlDomain,theForm,langue,softphoneReturnUrl) {
	var protocol = "https://";
	var domain   = theForm.domain.value;
	var username = "&username=" + theForm.user.value;
	var password = "&password=" + theForm.password1.value;
	var redirect = "&redirect=download";	
	var baseUrl  = "/pa/direct/LoginServlet?lang=" + langue + "&action=2";
	var urlDomain = domain.replace(".ca",".com");
	var returnUrl ="&returnUrl=" +softphoneReturnUrl;	
	
	if (softphoneUrlDomain != "") {
		urlDomain = softphoneUrlDomain;
	}

	return protocol + urlDomain + baseUrl + username + "@" + domain + password + redirect + returnUrl;

}


/*****************************************************************
 Construit l'url de changement de mot de passe sur le serveur de Nortel
******************************************************************/
function getSoftPhoneChangePasswordURL(softphoneUrlDomain,theForm,langue,softphoneReturnUrl) {

	var protocol = "https://";
	var domain   = theForm.domain.value;
	var username = "&username=" + theForm.user.value;
	var password = "&password=" + theForm.password1.value;
	var redirect = "&redirect=confirmation";	
	var baseUrl  = "/pa/direct/LoginServlet?lang=" + langue + "&action=2";
	var urlDomain = domain.replace(".ca",".com");
	var returnUrl ="&returnUrl=" +softphoneReturnUrl;	
	
	if (softphoneUrlDomain != "") {
		urlDomain = softphoneUrlDomain;
	}

	return protocol + urlDomain + baseUrl + username + "@" + domain + password + redirect +returnUrl;
	
}

 <!--
//onsubmit de la form onsubmit="return avoidDoubleClick();"
var isClicked = 0;
function avoidDoubleClick(){
	if (isClicked == 1) return false;
	isClicked = 1;
	return true;
}

/*****************************************************************
Pour la page recommandation
******************************************************************/
var state = 'none';
function showhide() {
  if (state == 'none') state = 'block';
  else state = 'none';
  document.getElementById('message-conf').style.display = state;
}
/*****************************************************************
Pour 
******************************************************************/


//------[ FAQ COLLAPSE DEFINITION LIST ]------------------------------------------------- //
//var YAHOO;
function collapseUncollapseDataList(root) {

	if(root) {
		var dl = root.getElementsByTagName('dl');
		
		for (var j=0; j<dl.length; j++) {
			var dt = dl[j].getElementsByTagName('dt');
			
			dt[0].dl = dl[j];
			dt[0].onclick = function(e) { 
				this.dl.className = (this.dl.className == "opened") ? "" : "opened";
				e = e || event;
				var target = e.target || e.srcElement;
				if(target.tagName == 'A') YAHOO.util.Event.preventDefault(e);
			};
		}
	}
	
}

function faqCollapseDataList() {
	var faq = document.getElementById("collapse");
	
	//var faq = YAHOO.util.Dom.getElementsByClassName("question-reponse");
	if(!faq) return;
	
	collapseUncollapseDataList(faq);
	
	//Ajoute la gestion du lien afficher ou cacher toutes les r�ponses
	var showAll = document.getElementById("show-all-answers");
	var hideAll = document.getElementById("hide-all-answers");
	var allDl = document.getElementById("collapse").getElementsByTagName('dl');
	
	if (showAll && hideAll && faq) {
		showAll.onclick = function() {
			for (var j=0; j<allDl.length; j++) {
				allDl[j].className = "opened";
			}
			
			this.className = "hidden";
			hideAll.className = "arrow";
			return false;
		};

		hideAll.onclick = function() {
			for (var j=0; j<allDl.length; j++) {
				allDl[j].className = "";
			}
			
			showAll.className = "arrow";
			this.className = "hidden";
			return false;
		};
		
	}
}		
/*
 * Fonction permettant de modifier l'aspect du bouton suivant
 * dans la page d'autorisation du module de PPA (paiement pr�-autoris�) 
 * selon que l'utilisateur accepte ou non les conditions d'utilisation.
 * 
 */
function changeBoutonSuivantStatus(componentName) {
	var boutonSuivant = document.getElementById('accepteBoutonSuivant');
	var component = document.getElementById(componentName);
	if (component.checked == true) {
		document.getElementById("spanPPABoutonSuivant").className = 'btn-yellow top-marge-20pct';
		boutonSuivant.disabled = false;
		boutonSuivant.style.cursor="pointer";
	} else {
		document.getElementById("spanPPABoutonSuivant").className = 'btn-gray top-marge-20pct';
		boutonSuivant.disabled = true;
		boutonSuivant.style.cursor="default";
	}
}





//-->



/*

function loginSoftphoneLocal(locale, softphoneUrlDomain,theForm)

{
	 
	var langue = ((locale != "en") ? "FRENCH" : "ENGLISH");
	
	var protocol = "https://";
	var domain   = theForm.domain.value;
	var username = "&username=" + theForm.user.value;
	var password = "&password=" + theForm.password.value;
	var redirect = "&redirect=home";	
	var baseUrl  = "/pa/direct/LoginServlet?lang=" + langue + "&action=2";

	var domainMsg = ((langue == "FRENCH") ? "Vous devez choisir un domaine" : "Please choose a domain");
	if (theForm.domain.value == "") {
		alert(domainMsg);
		return false;
	}
			
	var uidMsg = ((langue == "FRENCH") ? "Vous devez inscrire un nom d'utilisateur" : "Please enter a username");
	if (theForm.user.value == "") {
		alert(uidMsg);
		return false;
	}

	var pwdMsg = ((langue == "FRENCH") ? "Vous devez inscrire un mot de passe" : "Please enter a password");
	if (theForm.password.value == "") {
		alert(pwdMsg);
		return false;
	}
	
	var urlDomain = domain.replace(".ca",".com");
	
	if (softphoneUrlDomain != "") {
		urlDomain = softphoneUrlDomain;
	}
	
	theForm.action = protocol + urlDomain + baseUrl + username + "@" + domain + password + redirect;
	theForm.userSoftphone.value = theForm.user.value + "@" + domain;

    theForm.submit();    

}

*/

/* fancy submit buttons
 * 
 * allows a link to submit a form
 *
 **********************************************************************/
$(function(){
	
	function initializeClientAgreementBoxes(){
		var SELECTED_WHITE = 'selected-white';
		var NOT_SELECTED_GREY = 'not-selected-grey';

		function checkboxIsSelected(checkboxElement){
			return checkboxElement && checkboxElement.length === 1 && checkboxElement[0].checked;
		}

		function handleCheckBoxStateChange(boxElement,checkboxElement){
			return function(){
				if(checkboxIsSelected(checkboxElement)){
					boxElement.addClass(SELECTED_WHITE);
					boxElement.removeClass(NOT_SELECTED_GREY);	
				}else{
					boxElement.addClass(NOT_SELECTED_GREY);
					boxElement.removeClass(SELECTED_WHITE);	
				}					
			};
		}

		var boxElements = $('#agreementsForm .client-agreement .box-wrapper .box');
		boxElements.each(function(index,boxElement){
			boxElement = $(boxElement);
			var checkboxElement = boxElement.find('input[type=checkbox]');
			checkboxElement.bind('change',handleCheckBoxStateChange(boxElement,checkboxElement));
			handleCheckBoxStateChange(boxElement,checkboxElement)();
		});
	}
	
	// important to trigger click instead of submitting via JS so form actions get triggered
	$("a.fn-submit").each(function(){
		$link = $(this);
		// avoid double actions in IE for courriel web since this function is in both files, in ext_menu.js and espaceClient.js
		// so make sure we don't attach the action twice
		if(!$link.hasClass("button_enhanced")){
			$link.addClass("button_enhanced");
			//fake submit button needs to act like a real button
			$link.click(function(e){
				e.preventDefault();
				$(this).parents("form").find(":submit").trigger("click");
			});
			// make sure enter submits the form also
			$link.parents("form:first").keyup(function(e){
				if(e.keyCode === 13){
					$(this).find(":submit").trigger("click");
				}
			});
		}
	});
	
	initializeClientAgreementBoxes();
	
});
