/*
 * Modal overlay pour permettre au syst�me de valider le courriel
 * Effectue une recherche de la pr�sence du courriel, ainsi que de l'etat du compte dans le PUC
 * 
 */

// on DOM load
$(function(){
	
	// Appel � la validation au click du bouton soumettre du formulaire
	$(".validation").click(function(e) {
		e.preventDefault();
		
		var email = $(".courrielPUC").val();
		var accountNumber = null;
		
		// Le num�ro de compte est initialis� � partir de la session normalement, mais comme dans le processus d'inscription
        // � Espace-Client nous ne sommes pas autentifi�, nous devons prendre cette information directement dans la page jsp.
		if($(".accountNumberInputText").length != 0){
			// Inscription R�sidentiel: inscription.jsp
			accountNumber = $(".accountNumberInputText").val();
		} else if ($(".accountNumberSpan").length != 0){
			// Inscription Affaires: autoSubscriptionPageStep_2.jsp
			accountNumber = $(".accountNumberSpan").html();
		}
		
		callAjaxForValidation(email, accountNumber);
	});
	
	var callAjaxForValidation = function(email,accountNumber){
		$.ajax({
			url: "/client/preferences-de-communication/validation/validationCourrielPUC.action",
			type: "POST",
			contentType: "application/x-www-form-urlencoded; charset=ISO-8859-1",
			cache: false,
			data: "emailAdress="+email+"&accountNumber="+accountNumber,
			dataType: "json",
			timeout: 20000,
			success: function(response) {
				var submit = function(){
					$(".form_validation_puc").append( "<input type='hidden' name='emailState' id='processIdInputText' value='"+response.processId+"' >" );
					$(".form_validation_puc").submit();
				};
				if(response.processId == "EMAIL_EXIST_ACCOUNT_ACTIVE") {
					modalEmailExistant(submit);
				}
				else if(response.processId == "EMAIL_EXIST_ACCOUNT_INACTIVE"){
					modalEmailSync(submit);
				}
				else {
					submit();
				}
			}, error: function(){
				$(".form_validation_puc").append( "<input type='hidden' name='emailState' id='processIdInputText' value='ERROR' >" );
				$(".form_validation_puc").submit(); 
		    }
		});
	};
});

function modalEmailExistant(submit) {
	$("#message-validation-ECErrEmailExistant").modal({
		onOpen: function (dialog) {
			dialog.overlay.fadeIn('medium', function () {
				dialog.container.slideDown('slow', function () {
					dialog.data.fadeIn('fast');
				});
			});
		},
		onShow: function (dialog) {
			$("#message-validation-ECErrEmailExistant button.confirm").click(function() {
				submit();
				$.modal.close();
			});
		},
		onClose: function (dialog) {
			dialog.data.fadeOut('fast', function () {
				dialog.container.slideUp('fast', function () {
					dialog.overlay.fadeOut('fast', function () {
						$.modal.close(); // must call this!
					});
				});
			});
		},
		minHeight:$("#message-validation-ECErrEmailExistant").innerHeight() + 20,
		opacity:50,
		minWidth: 570
	});
};

function modalEmailSync(submit) {
	$("#message-validation-ECErrEmailSync").modal({
		onOpen: function (dialog) {
			dialog.overlay.fadeIn('medium', function () {
				dialog.container.slideDown('slow', function () {
					dialog.data.fadeIn('fast');
				});
			});
		},
		onShow: function (dialog) {
			$("#message-validation-ECErrEmailSync button.confirm").click(function() {
				submit();
				$.modal.close();
			});
		},
		onClose: function (dialog) {
			dialog.data.fadeOut('fast', function () {
				dialog.container.slideUp('fast', function () {
					dialog.overlay.fadeOut('fast', function () {
						$.modal.close(); // must call this!
					});
				});
			});
		},
		minHeight: $("#message-validation-ECErrEmailSync").innerHeight() + 35,
		opacity:50,
		minWidth: 570
	});
};
