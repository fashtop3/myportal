var IsIEWin = (navigator.appVersion.indexOf("MSIE") != -1) && (navigator.appVersion.indexOf("Windows") != -1);

var overlay =
{
	eIndex: "",
	eWidth: "400",
	eTop: "",
	eLeft: "",	
	eObject:"",
	
	init: function(w, t, l)
	{
		if(IsIEWin){
			eObject = "div.overlay-bg";
		} else {
			eObject= "div.overlay-bg";
		}

		$( eObject).overlay({
			expose: {
            	color: '#333', 
            	loadSpeed: 200,
				opacity: 0.4
			},
			closeOnClick: false
		})
		
		$("a.overlay-open").click(function(){
			overlay.eIndex = $("a.overlay-open").index(this);
			overlay.position(w, t, l);
		});
			
		$("a.overlay-btn-close").click(function()
		{
			overlay.eIndex = $("a.overlay-btn-close").index(this);
			overlay.close();
		});
			
		$("div.overlay-bg").click(function()
		{
			overlay.close();
		});
	},
	
	position: function(w, t, l)
	{
		if(w)
		{
			eWidth = w;
		}
		
		if(t)
		{
			eTop = t;
		}
		else
		{
			alert( $(window).height() + " - " + $('div.overlay-box').height() + " - " + $('div.overlay-box').css("height") );
			if($('div.overlay-box').height()>$(window).height()){
				eTop = ($(window).height() - 537) / 2;
			} else {
				eTop = ($(window).height() - $('div.overlay-box').eq(overlay.eIndex).height()) / 2;
			}
		}
		
		if(l)
		{
			eLeft = l;
		}
		else
		{
			eLeft = ($(window).width() - eWidth) / 2;
		}
		$('div.overlay-box').eq(overlay.eIndex).css({"width":eWidth, "top":eTop, "left":eLeft});
		overlay.open();		
	},
	
	open: function()
	{
		$( eObject).fadeIn("fast", function()
		{
			$('div.overlay-box').eq(overlay.eIndex).fadeIn("slow");
		});
	},
	
	close: function()
	{
		$('div.overlay-box').eq(overlay.eIndex).fadeOut("slow", function()
		{
			$( eObject).fadeOut("fast");
		});
	}
};
	