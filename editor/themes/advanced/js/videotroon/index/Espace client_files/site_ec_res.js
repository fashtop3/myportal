/*
site_ec_res.js
Librairie pour l'espace client residentiel
Mai 2012

Inclusion : 
 - Cookie managemant
 - script pour nav ATG
	
*/


/*
	Cookie managemant
	
	cookie management code from quirksmode.org - Not modified, just minified - see http://www.quirksmode.org/js/cookies.html#script for originals
###############################################################################################################################################*/
function createCookie(name,value,days){if(days){var date=new Date();date.setTime(date.getTime()+(days*24*60*60*1000));var expires="; expires="+date.toGMTString()}else{var expires=""}document.cookie=name+"="+value+expires+"; path=/"}
function readCookie(name){var nameEQ=name+"=";var ca=document.cookie.split(";");for(var i=0;i<ca.length;i++){var c=ca[i];while(c.charAt(0)==" "){c=c.substring(1,c.length)}if(c.indexOf(nameEQ)==0){return c.substring(nameEQ.length,c.length)}}return null}
function eraseCookie(name){createCookie(name,"",-1)}; 

/*
	script pour nav ATG
	
	Ces script sont des retrofit pour jquery 1.3.2 des scripts inclus dans vcom-fe
###############################################################################################################################################*/

if (typeof VIDEOTRON === "undefined") {
	VIDEOTRON = {
			//used for product combine ruleset
			onTheFlyRuleset:{},
			deferred:[],
			globals:{facebookOnloadCalls:[]}
	};
}
/*
voir NADROX.utils.titleText.js dans vcom-fe
**********************************************/
(function( $ ){
	$.fn.titleText = function(){
		if ($.browser.msie) {
			//if (parseInt($.browser.version) <= 9) {
			$(this).each(function(){
				form = this;
				$(form).attr("titletextenabled","true");
				$(".titleText",$(form)[0]).focus(function(){
			        if ($(this).val() == $(this).attr("placeholder"))
			        {
			            $(this).removeClass("titleTextActive",$(form)[0]);
			            $(this).val("");
			        }
			    });
			
			    $(".titleText",$(form)[0]).blur(function()
			    {
			        if ($(this).val() == "")
			        {
			            $(this).addClass("titleTextActive",$(form)[0]);
			            $(this).val($(this).attr("placeholder"));
			        }
			    });
			    
			    $($(form)[0]).submit(function() {
			    	$(".titleTextActive").each(function() {
			    		if($(this).val() == $(this).attr("placeholder")) {
			    			$(this).val("");
			    		}
			    	});
			    });
			
			    $(".titleText",$(form)[0]).blur();
			});
		};
	};
})(jQuery);


/*
voir jquery.isMobile.js dans vcom-fe
**********************************************/
(function($){
	$.fn.isMobile = function(){
		
		return( navigator.userAgent.match(/Android/i) ||
				 navigator.userAgent.match(/webOS/i) ||
				 navigator.userAgent.match(/iPhone/i) ||
				 navigator.userAgent.match(/iPod/i)||
				 navigator.userAgent.match(/iPad/i)||
				 navigator.userAgent.match(/htc_flyer/i)
				 );
	}
})(jQuery);

/*
voir jquery.hoverIntent.js dans vcom-fe
**********************************************/
(function($) {
	$.fn.hoverIntent = function(f,g) {
		// default configuration options
		var cfg = {
			sensitivity: 7,
			interval: 100,
			timeout: 0
		};
		// override configuration options with user supplied object
		cfg = $.extend(cfg, g ? { over: f, out: g } : f );

		// instantiate variables
		// cX, cY = current X and Y position of mouse, updated by mousemove event
		// pX, pY = previous X and Y position of mouse, set by mouseover and polling interval
		var cX, cY, pX, pY;

		// A private function for getting mouse position
		var track = function(ev) {
			cX = ev.pageX;
			cY = ev.pageY;
		};

		// A private function for comparing current and previous mouse position
		var compare = function(ev,ob) {
			ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
			// compare mouse positions to see if they've crossed the threshold
			if ( ( Math.abs(pX-cX) + Math.abs(pY-cY) ) < cfg.sensitivity ) {
				$(ob).unbind("mousemove",track);
				// set hoverIntent state to true (so mouseOut can be called)
				ob.hoverIntent_s = 1;
				return cfg.over.apply(ob,[ev]);
			} else {
				// set previous coordinates for next time
				pX = cX; pY = cY;
				// use self-calling timeout, guarantees intervals are spaced out properly (avoids JavaScript timer bugs)
				ob.hoverIntent_t = setTimeout( function(){compare(ev, ob);} , cfg.interval );
			}
		};

		// A private function for delaying the mouseOut function
		var delay = function(ev,ob) {
			ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t);
			ob.hoverIntent_s = 0;
			return cfg.out.apply(ob,[ev]);
		};

		// A private function for handling mouse 'hovering'
		var handleHover = function(e) {
			// copy objects to be passed into t (required for event object to be passed in IE)
			var ev = jQuery.extend({},e);
			var ob = this;

			// cancel hoverIntent timer if it exists
			if (ob.hoverIntent_t) { ob.hoverIntent_t = clearTimeout(ob.hoverIntent_t); }

			// if e.type == "mouseenter"
			if (e.type == "mouseenter") {
				// set "previous" X and Y position based on initial entry point
				pX = ev.pageX; pY = ev.pageY;
				// update "current" X and Y position based on mousemove
				$(ob).bind("mousemove",track);
				// start polling interval (self-calling timeout) to compare mouse coordinates over time
				if (ob.hoverIntent_s != 1) { ob.hoverIntent_t = setTimeout( function(){compare(ev,ob);} , cfg.interval );}

			// else e.type == "mouseleave"
			} else {
				// unbind expensive mousemove event
				$(ob).unbind("mousemove",track);
				// if hoverIntent state is true, then call the mouseOut function after the specified delay
				if (ob.hoverIntent_s == 1) { ob.hoverIntent_t = setTimeout( function(){delay(ev,ob);} , cfg.timeout );}
			}
		};

		// bind the function to the two event listeners
		return this.bind('mouseenter',handleHover).bind('mouseleave',handleHover);
	};
})(jQuery);



/*
voir initHoverIntent.js dans vcom-fe
**********************************************/
VIDEOTRON.globals.initHoverIntent = {clickedList : null};

VIDEOTRON.initHoverIntent = function (enhancementRoot){
	var clickClose = function(el){
		$(el).css("margin-left","-10000px");
		$(el).removeClass("opened");
	};
	var clickOpen = function (el){
		$(el).css("margin-left","0px");
		$(el).addClass("opened");
	};
	var isOpen = function(el){
		 return $(el).hasClass("opened");
	};
	$(enhancementRoot).each(function(){
		var instance = this;
		$("* > .fn-hover-intent",this).css("margin-left","-10000px");
		if ($(this).hasClass("select-style") || $.fn.isMobile()){
			$("* > .fn-hover-intent",this).parent().click(function(e){
				//if the same list is clicked twice, it means that we want to navigate, but if a select-style there is no navigation
				if (!isOpen($(".fn-hover-intent",this))){
					e.preventDefault();
					clickOpen($(".fn-hover-intent",this));
					VIDEOTRON.globals.initHoverIntent.clickedList = this;
				}else{
				
					clickClose($(".fn-hover-intent",this)); 
					
				}
			});
		}else{
			$("* > .fn-hover-intent",this).parent().hoverIntent(
				function(){
					$(".fn-hover-intent",this).css("margin-left","");
				},function(){
					$(".fn-hover-intent",this).css("margin-left","-10000px");
				}
			);
		}
		
	});
	
	//prevent binding events more than once if multple enhance are called
	
	if (!$("body").hasClass("hoverIntentClickMode")){
		//when escape key is pressed or when click is detected, we close all opened elements 
		$("body").bind("keydown click",function(e){
			if( (e.keyCode == 27 || e.type == "click")){
				//	we must check if the click event comes directly from the element when wanted to open.
				 //  if that is the case, we close only other opened elements/
				$(".fn-hover-intent.opened").each(function(){
					var tmp = $(VIDEOTRON.globals.initHoverIntent.clickedList);
					var clickParent = $(this , tmp).size();
					if (VIDEOTRON.globals.initHoverIntent.clickedList == null || clickParent == 0){
						clickClose(this);
					}
				});
				VIDEOTRON.globals.initHoverIntent.clickedList = null;
			}
		});
		
		$("body").addClass("hoverIntentClickMode");
	}
};


/*
voir jquery.initSubMenuA11y.js dans vcom-fe
**********************************************/
(function( $ ){
	$.fn.subMenuA11y = function (options){
		var hoverClass		= "hover",
			$menu			= $("ul:first-child",this);
		
		$menu.find("li").each(function(){
			$(this)
			.find("a:first")
			.focus(function(){
				//$(this).parentsUntil($menu, "ul").css("margin-left","0");
				$(this).parents("ul.fn-hover-intent").css("margin-left","0");
			})
			.blur(function(){
				//$(this).parentsUntil($menu, "ul").css("margin-left","-10000px");
				$(this).parents("ul.fn-hover-intent").css("margin-left","-10000px");
			});
		});
	};
})(jQuery);

/*
voir initSubMenuA11y.js dans vcom-fe
**********************************************/
VIDEOTRON.initSubMenuA11y = function (enhancementRoot){
	$(this.elems).subMenuA11y();
}

/*
custom enhancement inits
**********************************************/
$(function(){
	$("#header .enhance, #toolbar .enhance").addClass("enhanced");
	VIDEOTRON.initHoverIntent($("#toolbar .dropdown"));
	VIDEOTRON.initSubMenuA11y($("#toolbar .dropdown"));
	$("#header #menu-search-form").titleText();
});
/*
	FIN - script pour nav ATG
###############################################################################################################################################*/