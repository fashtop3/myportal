var sel_elem =
{
	btns: "",
	btns_num: "",
	btn_tag: "",
	btn_all: "btn-sel_0",//select all
	sel_all: "",
	sel_array: "",
	sel_array_length: "",
	sel_tag: "",
	
	init: function(bElem,bTag,sArray,sTag)
	{
		this.btn_tag = bTag;
		if( document.getElementById(bElem) != null){
			this.btns = document.getElementById(bElem).getElementsByTagName(this.btn_tag);
			this.btns_num = this.btns.length;
			this.sel_tag = sTag;
			this.sel_array = sArray;
			this.sel_array_length = this.sel_array.length;
			
			for(var i=0; i<this.btns_num; i++)
			{
				this.btns[i].id = "btn-sel_"+i;
				this.btns[i].onclick = function()
				{
					sel_elem.select(this.id);
				}
			}	
		}
	},
	
	select: function(bIndex)
	{
		var eTemp = "";
		var eLength = "";
		
		for(var m=0; m<this.sel_array_length; m++)
		{
			if(document.getElementById(this.sel_array[m]) != undefined){
				
				eTemp = document.getElementById(this.sel_array[m]).getElementsByTagName(this.sel_tag);
				eLength = eTemp.length;
				
				for(var r=0; r<eLength; r++)
				{
					if(bIndex == this.btn_all)
					{
						eTemp[r].checked = "checked";
					}
					else
					{
						eTemp[r].checked = "";
					}
				}
			}			
		}
	}
};