/*
window.onload = function()
{
	pwd_test.init("f-user-pwd1");
};
*/

var pwd_test =
{
	pwd_length: 6,
	pattern_char: new RegExp("[a-zA-Z]"),
	pattern_num: new RegExp("[0-9]"),
	pwd_weak_color: "#8c2923",
	pwd_weak_width: "60px",
	pwd_strong_color: "#009966",
	pwd_strong_width: "120px",
	pwd_border_width: "5px",
	pwd_border_style: "solid",
	pwd_input_id: "",
	pwd_eff_id: "",
	pwd_weak_id: "",
	pwd_strong_id: "",
		
	init: function(iPwd)
	{
		this.pwd_input_id = document.getElementById(iPwd);
		this.pwd_eff_id = document.getElementById("pwd-efficacity");
		this.pwd_graph_id = document.getElementById("pwd-test-graphic");
		this.pwd_weak_id = document.getElementById("pwd-test-weak");
		this.pwd_strong_id = document.getElementById("pwd-test-strong");
		
		this.pwd_weak_id.style.display = "none";
		this.pwd_strong_id.style.display = "none";
		
		//test on key up
		this.pwd_input_id.onkeyup = function()
		{
			pwd_test.graph(pwd_test.validate());
		}
	},
	
	validate: function()
	{
		this.pwd_eff_id.style.display = "inline-block";
		
		if(this.pwd_input_id.value.length >= this.pwd_length)
		{
			if(this.pattern_char.test(this.pwd_input_id.value))
			{
				if(this.pattern_num.test(this.pwd_input_id.value))
				{
					return true;
				}
			}
		}
		
		return false;		
	},

	graph: function(e)
	{
		if(e)
		{
			this.pwd_strong_id.style.width = this.pwd_strong_width;
			this.pwd_strong_id.style.borderBottomWidth = this.pwd_border_width;
			this.pwd_strong_id.style.borderBottomColor = this.pwd_strong_color;
			this.pwd_strong_id.style.borderBottomStyle = this.pwd_border_style;
			this.pwd_strong_id.style.display = "inline-block";
			this.pwd_weak_id.style.display = "none";	
		}	
		else
		{
			this.pwd_weak_id.style.width = this.pwd_weak_width;
			this.pwd_weak_id.style.borderBottomWidth = this.pwd_border_width;
			this.pwd_weak_id.style.borderBottomColor = this.pwd_weak_color;
			this.pwd_weak_id.style.borderBottomStyle = this.pwd_border_style;
			this.pwd_weak_id.style.display = "inline-block";
			this.pwd_strong_id.style.display = "none";	
		}	
	}
};
