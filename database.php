<table width="90%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><table width="100%" cellpadding="3" cellspacing="1"  style="border:4px solid #E2E7E9">
        <tr>
          <td width="78%" class="title">Database Backup</td>
          <td width="22%">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" align="center" class="normaltext">
						<?php
							/* Quickly and easily backup your MySQL database send to your email and ftp. */
							
							/* Database Backup */
							ini_set("memory_limit","80M");
							
							if($_SERVER['HTTP_HOST']=='localhost' || $_SERVER['HTTP_HOST']=='php'){
								$dbhost = "localhost"; 
								$dbuser = "root"; //  Database username 
								$dbpass = ""; //  Database password 
								$dbname = "church"; // Database name. Use --all-databases if you have more than one
								$use_gzip = "yes"; 
								$remove_file = "no"; 
								$use_email = "yes"; 								
								$send_from = "info@apostolicfaith.org.uk"; //  email sends
								$subject = "Database backup for ($dbname) - " . date("j F Y"); 
								$use_ftp = "yes"; //  to store on the ftp yes or not
								$ftp_server = "localhost"; //  address ftp server
								$ftp_user_name = "uk_church"; //  Login
								$ftp_user_pass = ""; //  password
								$ftp_path = "/"; //  place of storage
								$echo_status = "no"; //  resumer yes or not
								$local = 'yes';
								$path = "";
							}
							else{																
								$dbhost = "77.68.63.33"; 
								$dbuser = "apostol8_church"; //  Database username 
								$dbpass = "church123"; //  Database password 
								$dbname = "apostol8_church"; // Database name. Use --all-databases if you have more than one
								$use_gzip = "yes"; 
								$remove_file = "no"; 
								$use_email = "yes"; 								
								$send_from = "info@apostolicfaith.org.uk"; //  email sends
								$subject = "Database backup for ($dbname) - " . date("j F Y"); 
								$use_ftp = "yes"; //  to store on the ftp yes or not
								$ftp_server = "apostolicfaith.org.uk"; //  address ftp server
								$ftp_user_name = "apostol8"; //  Login
								$ftp_user_pass = "N3w?testament."; //  password
								$ftp_path = "/"; //  place of storage
								$echo_status = "no"; //  resumer yes or not
								$local = 'no';
								//$path = "../../";				
								$path = "/var/www/vhosts/apostolicfaith.org.uk/public_html/";
							}
							
							$db = mysql_connect($dbhost,$dbuser,$dbpass);
							mysql_select_db($dbname,$db);															
							
							if ($echo_status == 'yes') {
								print "Dumpfile will be written to $path<br>";
							}else{
								print "Please wait...<br><br>";
								print "while we back up the database";
							}
							
							$result = mysql_query("show tables from $dbname");
							while (list($table) = mysql_fetch_row($result)) {
								$newfile .= get_def($table);
								$newfile .= "\n\n";
								$newfile .= get_content($table);
								$newfile .= "\n\n";
								$i++;
								if ($echo_status == "yes") {
									print "Dumped table $table<br>";
								}
							}
							
							$file_name = $dbname.".sql";
							$file_path = $path. 'db/' . $file_name;
							
							if ($use_gzip == "yes") {
								$file_name .= ".gz";
								$file_path .= ".gz";
								$zp = gzopen($file_path, "wb9");
								gzwrite($zp,$newfile);
								gzclose($zp);
								
								if ($echo_status == 'yes') {
									print "<br>Gzip-file is created...<br>";
								}else{
									print "<br><br>Database backup is completed now";
								}
							} else {
								$fp = fopen($file_path, "a+");
								fwrite($fp, $newfile);
								fclose($fp);
								
								if ($echo_status == 'yes') {
									print "<br>SQL-file is created...<br>";
								}
							}
							
							if ($use_email == 'yes') {
								$fileatt_type = filetype($file_path);
								
								$headers = "From: $send_from";
								
								// Read the file to be attached ('rb' = read binary)
								$fp = fopen($file_path,'rb');
								$data = fread($fp,filesize($file_path));
								fclose($fp);
								
								// Generate a boundary string
								$semi_rand = md5(time());
								$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
								
								// Add the headers for a file attachment
								$headers .= "\nMIME-Version: 1.0\n" ."Content-Type: multipart/mixed;\n" ." boundary=\"{$mime_boundary}\"";
								
								// Add a multipart boundary above the plain message
								$message = "This is a multi-part message in MIME format.\n\n" ."--{$mime_boundary}\n" ."Content-Type: text/plain; charset=\"iso-8859-1\"\n" ."Content-Transfer-Encoding: 7bit\n\n" .
								$message . "\n\n";
								
								// Base64 encode the file data
								$data = chunk_split(base64_encode($data));
								
								// Add file attachment to the message
								$message .= "--{$mime_boundary}\n" ."Content-Type: {$fileatt_type};\n" ." name=\"{$file_name}\"\n" ."Content-Disposition: attachment;\n" ." filename=\"{$file_name}\"\n" ."Content-Transfer-Encoding: base64\n\n" .
								$data . "\n\n" ."--{$mime_boundary}--\n";
								
								// Send the message								
								
								$send_to = "sejal@zipprosystem.co.uk"; //  email recipient
								$ok = @mail($send_to, $subject, $message, $headers);
								
								if($ok){
									print "<br><br>Mail with a zip file is sent";
								}
								unlink($file_path);
								if ($echo_status == 'yes') {
									print "<br>Mail is sent...<br>";
									
								}
							}
							
							if($local=='no'){
								if ($use_ftp == 'yes') {
									if ($use_gzip == 'yes') {
										$mode = FTP_BINARY;
									} else {
										$mode = FTP_ASCII;
									}
									$ftp_id = ftp_connect($ftp_server);
									$login_result = ftp_login($ftp_id, $ftp_user_name, $ftp_user_pass);
									$upload = ftp_put($ftp_id, $ftp_path . $file_name, $file_path, $mode);
									ftp_close($ftp_id);
									
									if ($echo_status == 'yes') {
										print "<br>Backup is uploaded to $ftp_user_name@$ftp_server...<br>";
									}
								}
							}
							if ($remove_file == "yes") {
								unlink($file_path);
								if ($echo_status == 'yes') {
									print "<br>File is deleted...<br>";
								}
							}
							
							if ($echo_status == 'yes') {
								print "<br>I am done!<br>";
							}
						
						function get_def($table) {
							$def = "";
							$def .= "DROP TABLE IF EXISTS $table;\n";
							$def .= "CREATE TABLE $table (\n";
							$result = mysql_query("SHOW FIELDS FROM $table") or die("Table $table not existing in database");
							
							while($row = mysql_fetch_array($result)) {
								$def .= " $row[Field] $row[Type]";
								if ($row["Default"] != "") $def .= " DEFAULT '$row[Default]'";
								if ($row["Null"] != "YES") $def .= " NOT NULL";
								if ($row[Extra] != "") $def .= " $row[Extra]";
								$def .= ",\n";
							}
							
							$def = ereg_replace(",\n$","", $def);
							$result = mysql_query("SHOW KEYS FROM $table");
							
							while($row = mysql_fetch_array($result)) {
								$kname=$row[Key_name];
								if(($kname != "PRIMARY") && ($row[Non_unique] == 0)) $kname="UNIQUE|$kname";
								if(!isset($index[$kname])) $index[$kname] = array();
								$index[$kname][] = $row[Column_name];
							}
						
							while(list($x, $columns) = @each($index)) {
								$def .= ",\n";
								if($x == "PRIMARY") $def .= " PRIMARY KEY (" . implode($columns, ", ") . ")";
								else if (substr($x,0,6) == "UNIQUE") $def .= " UNIQUE ".substr($x,7)." (" . implode($columns, ", ") . ")";
								else $def .= " KEY $x (" . implode($columns, ", ") . ")";
							}
							$def .= "\n);";
							return (stripslashes($def));
						}
						
						function get_content($table) {
							$content="";
							$result = mysql_query("SELECT * FROM $table");
							while($row = mysql_fetch_row($result)) {
								$insert = "INSERT INTO $table VALUES (";
								for($j=0; $j<mysql_num_fields($result);$j++) {
									if(!isset($row[$j])) $insert .= "NULL,";
									else if($row[$j] != "") $insert .= "'".addslashes($row[$j])."',";
									else $insert .= "'',";
								}
								$insert = ereg_replace(",$","",$insert);
								$insert .= ");\n";
								$content .= $insert;
							}
							return $content;
						}																		
						?>
          </td>
        </tr>
      </table></td>
  </tr>
</table>
