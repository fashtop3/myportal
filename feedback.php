<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<title>FEEDBACK</title>
<link href="css/site.css" rel="stylesheet" type="text/css">
</head>
<body style="background: none; background-color: #FFFFFF;">
	<table width="450" border="0" align="center" cellpadding="0"
		cellspacing="0">
		<tr>
			<td align="left" valign="top"><h2>
					<img src="images/feedback.gif" align="absmiddle" alt=""> FEEDBACK
				</h2></td>
		</tr>
		<tr>
			<td align="left" valign="top" class="bdr_all">&nbsp;</td>
		</tr>
		<tr>
			<td align="left" valign="top"><form action="" method="post"
					name="frm_sms1" id="frm_sms1">
					<table width="94%" border="0" align="center" cellpadding="2"
						cellspacing="2">
						<tr>
							<td align="left" valign="top"><b>Name : </b></td>
							<td align="left" valign="top"><input name="Name" type="text"
								title=" *" validate="required:true" class="field_bdr" id="Name1"
								style="width: 300px; height: 17px;" /></td>
						</tr>
						<tr>
							<td align="left" valign="top"><b>Email : </b></td>
							<td align="left" valign="top"><input name="Email" title=" *"
								validate="required:true, email:true" type="text"
								class="field_bdr" id="Email1"
								style="width: 300px; height: 17px;" /></td>
						</tr>
						<tr>
							<td align="left" valign="top"><b>Comments : </b></td>
							<td align="left" valign="top"><textarea name="Comment" title=" *"
									validate="required:true" id="Comment1" class="field_bdr"
									style="width: 300px; height: 100px;"></textarea></td>
						</tr>
						<tr>
							<td align="right" valign="top"></td>
							<td align="left" valign="top"><input name="button3" type="button" onClick="checkBlank1('frm_sms1')" class="login_bg" id="button3" value="Send" />
							</td>
						</tr>
						<tr>
							<td height="35" colspan="2" align="left" valign="top"><div id="sms1"></div></td>
						</tr>
					</table>
				</form></td>
		</tr>
	</table>
</body>
</html>
