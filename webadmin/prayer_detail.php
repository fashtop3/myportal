<?php
ob_start();
session_start();
require_once("../utils/config.php");
require_once("../utils/color_config.php");
require_once("../utils/functions.php");
require_once("../utils/dbclass.php");
require_once("../utils/paging.php");
$objDB = new MySQLCN;
if($_REQUEST['SDate']=='')
	$date = date('Y-m-d');
else
	$date = $_REQUEST['SDate'];
?>
<html>
<head>
<title><?php echo COMPANY_NAME?>- Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><span style="font:Arial, Helvetica, sans-serif; font-size:24px; color:#009999">Prayers</span></td>
  </tr>
  <tr>
    <td align="left" valign="top"><?php 							
									$sql = "select * from prayer where p_id = '".$_REQUEST['id']."'";																	
									$img = $objDB->select($sql);									
					?>
      <table width="100%" border="0" cellspacing="1" cellpadding="5">
        <tr bgcolor="#993300">
          <td align="left" width="20%" valign="top" style="color:#FFFFFF; font-weight:bold;">Name</td>
          <td align="left" valign="top" style="color:#FFFFFF; font-weight:bold;">Prayer</td>
        </tr>
        <?php if(count($img)>0){
								for($i=0;$i<count($img);$i++){
							?>
        <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
          <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo stripslashes($img[$i]['p_name'])?></td>
          <td style="border-bottom:1px dashed #999999" align="left" valign="top"><?php echo wordwrap(stripslashes($img[$i]['p_prayer']),60,"\n",1)?></td>
        </tr>
        <tr>
          <td height="3"  colspan="4"></td>
        </tr>
        <?php } ?>
        <?php }else{ ?>
        <tr style="font:Arial, Helvetica, sans-serif; font-size:13px; color:#333333">
          <td align="left" valign="top" colspan="4">No record found&nbsp;</td>
        </tr>
        <?php } ?>
      </table></td>
  </tr>
  <tr>
    <td align="center" valign="top"><input type="button" value="Close" onClick="window.close()" /></td>
  </tr>
</table>
</body>
</html>
