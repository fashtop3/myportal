<?php
if (isset($_SESSION['AdminID'])) { ?>
    <body class="skin-blue sidebar-mini">
    <div class="wrapper">
    <header class="main-header">
        <a href="index.php" class="logo">
            <!-- <img src="<?php echo ADMIN_IMAGE_PATH; ?>/logo.gif" width="114" height="90" alt="Apostolic Faith Mission UK" title="Apostolic Faith Mission UK" border="0" /> -->
            <span class="logo-mini"><b>A</b></span>
            <span class="logo-lg"><b><?php echo COMPANY_NAME ?></b></span>
        </a>
        <nav class="navbar navbar-static-top" role="navigation">
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
            </a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?php echo ADMIN_IMAGE_PATH; ?>/home.jpg" class="user-image" alt="User Image">
                            <span class="hidden-xs"><?php echo $_SESSION['AdminName'] ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="user-header">
                                <img src="<?php echo ADMIN_IMAGE_PATH; ?>/home.jpg" class="img-circle" alt="User Image">
                                <p>
                                    <?php echo $_SESSION['AdminName'] ?> <?php if ($_SESSION['PlaceID'] == 0) echo "(Super Administrator)"; else {
                                        echo "(Administrator) (" . stripslashes(FetchValue('county', 'c_name', 'c_id', $_SESSION['PlaceID'])) . " Church)";
                                    } ?>
                                    <!--  <small>Member since Nov. 2016</small> -->
                                </p>
                            </li>
                            <li class="user-footer">
                                <div class="pull-right">
                                    <a href="index.php?p=admin" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-left">
                                    <a href="index.php?p=logout" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>
    <aside class="main-sidebar">
        <section class="sidebar">
            <ul class="sidebar-menu">
                <li class="header">MAIN NAVIGATION</li>
                <li class="active treeview"><a href="index.php"><i class="fa fa-dashboard"></i>
                        <span>Dashboard</span></a></li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gear"></i>
                        <span>Setup</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="index.php"><i class="fa fa-circle-o"></i> Admin Home</a></li>
                        <li><a href="index.php?p=admin"><i class="fa fa-circle-o"></i> My Account</a></li>
                        <?php if ($_SESSION['PlaceID'] == 0) { ?>
                            <li><a href="index.php?p=config"><i class="fa fa-circle-o"></i> General Configuration</a>
                            </li>
                            <li><a href="index.php?p=menu_list"><i class="fa fa-circle-o"></i> Front Menu Config</a>
                            </li>
                        <?php } ?>
                        <li><a href="index.php?p=playlist"><i class="fa fa-circle-o"></i> Playlist Songs</a></li>
                        <li><a href="index.php?p=clock"><i class="fa fa-circle-o"></i> Live Clock Setting</a></li>
                        <li><a href="index.php?p=logout"><i class="fa fa-circle-o"></i> Logout</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-music"></i>
                        <span>Music</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="index.php?p=music_category"><i class="fa fa-circle-o"></i> Music Category</a></li>
                        <li><a href="index.php?p=music_category_new"><i class="fa fa-circle-o"></i> Add Music
                                Category</a></li>
                        <li><a href="index.php?p=cd_list"><i class="fa fa-circle-o"></i> CDs/DVDs/Higherway <br>Magazine/Music
                                &amp; Tracks</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Master</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <?php if ($_SESSION['PlaceID'] == 0) { ?>
                            <li><a href="index.php?p=place_list"><i class="fa fa-circle-o"></i> Lists All Places/County</a>
                            </li>
                            <li><a href="index.php?p=place_addedit"><i class="fa fa-circle-o"></i> Add New Place/County</a>
                            </li>
                        <?php } ?>
                        <li><a href="index.php?p=schedule_list"><i class="fa fa-circle-o"></i> List Schedules</a></li>
                        <li><a href="index.php?p=schedule_addedit"><i class="fa fa-circle-o"></i> Add New Schedule</a>
                        </li>
                        <?php if ($_SESSION['PlaceID'] == 0) { ?>
                            <li><a href="index.php?p=administrator"><i class="fa fa-circle-o"></i> List All
                                    Administrators</a></li>
                            <li><a href="index.php?p=administrator_addedit"><i class="fa fa-circle-o"></i> Add New
                                    Administrator</a></li>
                        <?php } ?>
                        <?php if ($_SESSION['PlaceID'] == 0) { ?>
                            <li><a href="index.php?p=shop"><i class="fa fa-circle-o"></i> List Online Payment
                                    Category</a></li>
                            <li><a href="index.php?p=shop_addedit"><i class="fa fa-circle-o"></i> Add New Online Payment
                                    Category</a></li>
                            <li><a href="index.php?p=camp_setting"><i class="fa fa-circle-o"></i> Camp Meeting</a></li>
                        <?php } ?>
                        <li><a href="index.php?p=banners"><i class="fa fa-circle-o"></i> List Banners</a></li>
                        <li><a href="index.php?p=banner_new"><i class="fa fa-circle-o"></i> Add New Banner</a></li>
                        <!-- <li><a href="index.php?p=curriculum_category_list"><i class="fa fa-circle-o"></i> List Curriculum Category</a></li>
                        <li><a href="index.php?p=curriculum_category_addedit"><i class="fa fa-circle-o"></i> Add Curriculum Category</a></li>
                        <li><a href="index.php?p=tract_category_list"><i class="fa fa-circle-o"></i> List Tract Category</a></li>
                        <li><a href="index.php?p=tract_category_addedit"><i class="fa fa-circle-o"></i> Add Tract Category</a></li>
                        <li><a href="index.php?p=occation_list"><i class="fa fa-circle-o"></i> List Occasions</a></li>
                        <li><a href="index.php?p=occation_addedit"><i class="fa fa-circle-o"></i> Add Occasion</a></li> -->
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Content</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="index.php?p=cms_list"><i class="fa fa-circle-o"></i> List CMS Pages</a></li>
                        <li><a href="index.php?p=outreach_list"><i class="fa fa-circle-o"></i> List Outreach Pages</a>
                        </li>
                        <li><a href="index.php?p=outreach_addedit"><i class="fa fa-circle-o"></i> Add New Outreach Page</a>
                        </li>
                        <li><a href="index.php?p=reflection_list"><i class="fa fa-circle-o"></i> List Reflection
                                Pages</a></li>
                        <li><a href="index.php?p=reflection_addedit"><i class="fa fa-circle-o"></i> Add New Reflection
                                Page</a></li>
                        <li><a href="index.php?p=doctrines_list"><i class="fa fa-circle-o"></i> List Doctrines <br>(Seeking
                                Christ) Pages</a></li>
                        <li><a href="index.php?p=doctrines_addedit"><i class="fa fa-circle-o"></i> Add New Doctrines
                                <br>(Seeking Christ) Page</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>News/Event</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="index.php?p=news_list"><i class="fa fa-circle-o"></i> List News</a></li>
                        <li><a href="index.php?p=news_addedit"><i class="fa fa-circle-o"></i> Add New News</a></li>
                        <li><a href="index.php?p=event_list"><i class="fa fa-circle-o"></i> List Events</a></li>
                        <li><a href="index.php?p=event_addedit"><i class="fa fa-circle-o"></i> Add New Event</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Contacts</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="index.php?p=contact_category_list"><i class="fa fa-circle-o"></i> List Contact
                                Category</a></li>
                        <li><a href="index.php?p=contact_category_addedit"><i class="fa fa-circle-o"></i> Add New
                                Contact Category</a></li>
                        <li><a href="index.php?p=contact_list"><i class="fa fa-circle-o"></i> List Contacts</a></li>
                        <li><a href="index.php?p=contact_addedit"><i class="fa fa-circle-o"></i> Add New Contact</a>
                        </li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Resources</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="index.php?p=book_list"><i class="fa fa-circle-o"></i> List Books</a></li>
                        <li><a href="index.php?p=book_addedit"><i class="fa fa-circle-o"></i> Add Book</a></li>
                        <li><a href="index.php?p=curriculum_category_list"><i class="fa fa-circle-o"></i> List
                                Curriculum Category</a></li>
                        <li><a href="index.php?p=curriculum_category_addedit"><i class="fa fa-circle-o"></i> Add
                                Curriculum Category</a></li>
                        <li><a href="index.php?p=curriculum_list"><i class="fa fa-circle-o"></i> Curriculums (Junior and
                                <br> Senior)</a></li>
                        <li><a href="index.php?p=curriculum_primary"><i class="fa fa-circle-o"></i> Curriculums (Primary
                                Pal <br> and Answer)</a></li>
                        <li><a href="index.php?p=tract_category_list"><i class="fa fa-circle-o"></i> List Tract Category</a>
                        </li>
                        <li><a href="index.php?p=tract_category_addedit"><i class="fa fa-circle-o"></i> Add Tract
                                Category</a></li>
                        <li><a href="index.php?p=tract_list"><i class="fa fa-circle-o"></i> List Tracts</a></li>
                        <li><a href="index.php?p=tract_addedit"><i class="fa fa-circle-o"></i> Add Tract</a></li>
                        <li><a href="index.php?p=occation_list"><i class="fa fa-circle-o"></i> List Occasions</a></li>
                        <li><a href="index.php?p=occation_addedit"><i class="fa fa-circle-o"></i> Add Occasion</a></li>
                        <li><a href="index.php?p=sermon_list"><i class="fa fa-circle-o"></i> List Sermons</a></li>
                        <li><a href="index.php?p=sermon_addedit"><i class="fa fa-circle-o"></i> Add Sermon</a></li>
                        <!-- <li><a href="index.php?p=video_list"><i class="fa fa-circle-o"></i> List Sermon Video</a></li>
                        <li><a href="index.php?p=video_addedit"><i class="fa fa-circle-o"></i> Add New Video</a></li>
                        <li><a href="index.php?p=audio_list"><i class="fa fa-circle-o"></i> List Sermon Audios</a></li>
                        <li><a href="index.php?p=audio_addedit"><i class="fa fa-circle-o"></i> Add New Audio</a></li>
                        <li><a href="index.php?p=photo_category_list"><i class="fa fa-circle-o"></i> List Photo Gallery Category</a></li>
                        <li><a href="index.php?p=photo_category_addedit"><i class="fa fa-circle-o"></i> Add Photo category</a></li>
                        <li><a href="index.php?p=photo_list"><i class="fa fa-circle-o"></i> List Photo Gallery</a></li>
                        <li><a href="index.php?p=photo_addedit"><i class="fa fa-circle-o"></i> Add Photo</a></li> -->
                    </ul>
                </li>
                <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Members</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=member_list"><i class="fa fa-circle-o"></i> List Members</a></li>
                <li><a href="index.php?p=member_addedit"><i class="fa fa-circle-o"></i> Add New Members</a></li>
                <?php if ($_SESSION['PlaceID'] == 0) { ?>
                <li><a href="index.php?p=position_list"><i class="fa fa-circle-o"></i> List Member Postions</a></li>
                <li><a href="index.php?p=position_addedit"><i class="fa fa-circle-o"></i> Add New Position</a></li>
                <?php } ?>
              </ul>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-money"></i>
                <span>Videos</span>
                <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">                
                <li><a href="index.php?p=video_list"><i class="fa fa-circle-o"></i> List Webcast Videos</a></li>
                <li><a href="index.php?p=video_addedit"><i class="fa fa-circle-o"></i> Add New Video</a></li>
              </ul>
            </li> -->
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Gallery</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="index.php?p=news_gallery_list"><i class="fa fa-circle-o"></i> List Image
                                Gallery</a></li>
                        <li><a href="index.php?p=news_gallery_addedit"><i class="fa fa-circle-o"></i> Add New Image
                                Gallery</a></li>
                        <li><a href="index.php?p=gallery_images_list"><i class="fa fa-circle-o"></i> List Gallery Images</a>
                        </li>
                        <li><a href="index.php?p=gallery_images_addedit"><i class="fa fa-circle-o"></i> Add Gallery
                                Image</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-money"></i>
                        <span>Others</span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="index.php?p=prayer_list"><i class="fa fa-circle-o"></i> Prayer Requests</a></li>
                        <li><a href="index.php?p=testi_list"><i class="fa fa-circle-o"></i> Testimonials</a></li>
                        <li><a href="index.php?p=feedback_list"><i class="fa fa-circle-o"></i> Feedback</a></li>
                        <li><a href="index.php?p=comment_list"><i class="fa fa-circle-o"></i> Comments / Suggestions
                                <br> (Live Video)</a></li>
                        <li><a href="index.php?p=sms_list"><i class="fa fa-circle-o"></i> List SMS Subscribers</a></li>
                        <li><a href="index.php?p=sms_addedit"><i class="fa fa-circle-o"></i> Add New SMS Subscribers</a>
                        </li>
                        <li><a href="index.php?p=send_bulk_sms"><i class="fa fa-circle-o"></i> Send Bulk SMS</a></li>
                    </ul>
                </li>
                <li><a href="index.php?p=database"><i class="fa fa-database"></i> <span>DB Backup</span> </a></li>
            </ul>
        </section>
    </aside>
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <?php include("includes/include_external.php"); ?>
    </div><!-- /.content-wrapper -->
<?php } else {  ?>
    <?php include("includes/include_external.php"); ?>
<?php } ?>