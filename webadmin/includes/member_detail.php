<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"member"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<style type="text/css">
#pg_main {
	font-size:1.1em;
	padding:10px;
}
#pg_main .h3title {
	font-size:large;
	font-weight:bold;
	color:#003366;
	margin:5px 5px 8px 0;
}
p {
	margin:0;
	padding:0 0 10px;
	color:black;
	font-family:Arial, Helvetica, sans-serif;
	font-size:70%;
}
</style>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"> <img src="<?php echo ADMIN_IMAGE_PATH;?>/contact_detail.jpg" width="48" height="48" /> </td>
		<td class="tbl_head" height="24">CONTACT DETAIL</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" class="form">
				<tr>
					<td valign="top" width="5%" style="padding-left:5px; padding-top:5px;"> <img src="../uploads/member/big/<?php echo stripslashes($Info[0]['image'])?>" width="80" height="100" /> </td>
					<td colspan="" style="padding-left:5px; padding-right:5px; padding-top:5px;" valign="top">
						<div id="pg_main" style="vertical-align:top">
							<h3 class="h3title"><?php echo stripslashes($Info[0]['firstname']." ".$Info[0]['lastname'])?></h3>
							<p> <?php echo stripslashes($Info[0]['address'])?><br />
								<?php echo stripslashes($Info[0]['city'])?><br />
								<?php echo stripslashes($Info[0]['state'])?><br />
								<?php echo stripslashes($Info[0]['zipcode'])?><br />
								<?php echo stripslashes($Info[0]['country'])?><br />
								<br>
								<strong>Email:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="mailto:<?php echo stripslashes($Info[0]['email'])?>"><?php echo stripslashes($Info[0]['email'])?></a><br>
								<strong>Position:</strong>&nbsp;&nbsp; <?php echo stripslashes(FetchValue1('member_position','title','id',$Info[0]['position'])); ?><br>
								<strong>Phone:</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo stripslashes($Info[0]['phone'])?><br>
							</p>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2">
						<center>
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=member_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						</center>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
			</table>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
