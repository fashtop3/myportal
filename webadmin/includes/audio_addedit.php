<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATEAUDIO";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"sermon"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$path = "../uploads/sermon/video/".stripslashes($Info[0]['audio']);	
}else
	$MODE="ADDAUDIO";
?>
<script>
  $(document).ready(function(){
  	
    // validate signup form on keyup and submit
	
	$("#frmAdmin").validate({
		rules: {			
			 <?php if($MODE=='ADDAUDIO'){?>CVideo: "required",<?php } ?>
			 sermon: "required"
		},
		messages: {
			 <?php if($MODE=='ADDAUDIO'){?>CVideo: "Please enter audio",<?php } ?>
			 sermon: "Please select sermon"
		}
	});	
	
  });
</script>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addvideo.jpg" /></td>
		<td colspan=5 class="tbl_head" height="24">SERMON AUDIO</td>
	</tr>
	<tr>
		<td width="" colspan="6">
			<form method="post" action="manage_sermon.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="5%">Sermon</td>
											<td class="fieldarea">
												<select name="sermon" id="sermon" style="width:300px;">
													<option value="">--Select Sermon--</option>
													<?php echo FillCombo1('sermon','title','id',$Info[0]['id'],"");?>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel">Audio</td>
											<td class="fieldarea" valign="top">
												<input name="CVideo" id="CVideo" class="input" style="width: 300px;" type="file" accept="mp3|MP3" />
												<input type="hidden" id="OldVideo" name="OldVideo" value="<?php echo stripslashes($Info[0]['audio'])?>" />
											</td>
										</tr>
										<?php if($MODE=='UPDATEAUDIO'){ ?>
										<tr>
											<td class="fieldlabel"></td>
											<td class="fieldarea" valign="top">
												<div id="container"></div>
												<script type="text/javascript" src="../Scripts/swfobject.js"></script>
												<script type="text/javascript">
		var s1 = new SWFObject("../images/player.swf","ply","350","320","9","#FFFFFF");
		s1.addParam("allowfullscreen","true");
		s1.addParam("allowscriptaccess","always");
		s1.addParam("flashvars","file=<?php echo $path;?>");
		s1.write("container");
	          </script>
											</td>
										</tr>
										<?php } ?>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<br>
								<div align="center">
									<input value="SUBMIT" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=audio_list';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
