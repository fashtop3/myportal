<?php

	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"admin"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE UserID =".$_SESSION['AdminID'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		$Photo = "<img src='../uploads/member/big/".stripslashes($Info[0]['image'])."' width=75 height=75>";			

?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			UserName: {
				required: true
			},
			Password: {
				required: true,
				minlength: 6,
				maxlength: <?php echo PASSWORD_LIMIT?>
				},
			Email: {
				required: true,
				email: true
			},
			FirstName: "required",
			LastName: "required"		
		},
		messages: {
			UserName: "Please enter username",
			Password: {required: "Please enter password",
				minlength: "Please enter at least 6 characters",
				maxlength: "Password should not be more than <?php echo PASSWORD_LIMIT?> characters"},
			Email: {
				required: "Please enter email address",
				email: "Please enter valid email address"
			},
			FirstName: "Please enter firstname",
			LastName: "Please enter lastname"
		}
	});
  });
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/admin.png" width="35" height="35" />&nbsp;&nbsp; ADMIN MANAGER
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<?php showMessage();?>
			<form class="form-horizontal"  method="post" id="frmAdmin" action="manage_administrator.php" name="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_SESSION['AdminID']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
		
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">User Name</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="UserName" id="UserName" value="<?php echo $Info[0]['UserName']?>"/>
							<span id="msgbox" style="display:none"></span>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Password</label>
						<div class="col-sm-5">
							<input type="password" class="form-control" name="Password" id="Password" value="<?php echo $Info[0]['Password']?>"/>
							<input type="hidden" value="<?php echo $Info[0]['place_id'];?>" name="Place" id="Place" />
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">First Name</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="FirstName" id="FirstName" value="<?php echo stripslashes($Info[0]['FirstName'])?>"/>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Last Name</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="LastName" id="LastName" value="<?php echo stripslashes($Info[0]['LastName'])?>"/>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Address</label>
						<div class="col-sm-5">
							<textarea name="Address" class="form-control" id="Address"><?php echo stripslashes($Info[0]['address'])?></textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Phone/Mobile</label>
						<div class="col-sm-5">
							<input name="Phone" class="form-control" id="Phone" value="<?php echo stripslashes($Info[0]['Phone'])?>" />
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-5">
							<input type="text" class="form-control" name="Email" id="Email" value="<?php echo stripslashes($Info[0]['Email'])?>"/>
							<span id="msgbox1" style="display:none"></span>
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label">Photo/Image</label>
						<div class="col-sm-5">
							<input name="CImage" id="CImage" class="input" style="width: 300px;" type="file">
							<input type="hidden" id="OldImage" name="OldImage" value="<?php echo stripslashes($Info[0]['image'])?>" >
						</div>
					</div>
					<div class="form-group">
						<label for="inputEmail" class="col-sm-2 control-label"></label>
						<div class="col-sm-5"><?php echo $Photo;?> </div>
					</div>

			<input type="hidden" value="<?php echo $MODE?>" name="action" />
			<input type="hidden" value="admin" name="apath" />
			<div class="form-group">

                <div class="col-sm-offset-2 col-sm-10">
                	<input value="<?php echo $MODE?>"  class="btn btn-success" type="submit" name="submit1" id="RegisterSubmit">
					<input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php';">
                  

                </div>

              </div>
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
