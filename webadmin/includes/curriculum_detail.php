<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"curriculum"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder" ></script>
<script type="text/javascript" src="js/lightbox.js" ></script>
<link href="css/lightbox.css" rel="stylesheet" type="text/css">
<section class="content-header">
  <h1>
   <img src="<?php echo ADMIN_IMAGE_PATH;?>/curriculum.jpg" width="30" height="30" />  CURRICULUM DETAIL
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<table width="100%" class="form">
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Category : </b> <?php echo stripslashes(FetchValue1('curriculum_category','title','id',$Info[0]['category_id']));?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Title : </b> <?php echo stripslashes($Info[0]['title'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Lesson Number : </b> <?php echo stripslashes(GetFieldData("book","lesson_no","where id=".$Info[0]['lesson_no'])); ?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Date : </b> <?php echo date('F d',strtotime($Info[0]['cdate']));?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Memory Verse : </b> <?php echo stripslashes($Info[0]['memory_verse'])?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<?php if($Info[0]['image']){ ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"><a rel="lightbox0" href="../uploads/curriculum/image/big/<?php echo stripslashes($Info[0]['image']); ?>"> <img style="border:none" src="../uploads/curriculum/image/big/<?php echo stripslashes($Info[0]['image']); ?>" width="50" height="50" title="Click hre to view"/> </a> <br />
						Click on the Image to enlarge it. </td>
				</tr>
				<?php } ?>
				<?php if($Info[0]['file']){ ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"><a href="../uploads/curriculum/download/<?php echo stripslashes($Info[0]['file'])?>"> <img border="none" src="images/word.jpg" /> </a> <br />
						Click on the icon to open the file. </td>
				</tr>
				<?php } ?>
				<?php if($Info[0]['video']){ ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"><a href="../uploads/curriculum/video/<?php echo stripslashes($Info[0]['video'])?>"> <img border="none;" src="images/pdf.jpg" /> </a> <br />
						Click on the icon to open the file. </td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"></td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;"><?php echo str_replace('"images/','"../images/',stripslashes($Info[0]['full_description']))?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=curriculum_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
