<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/functions.php");
		CheckPermission(); 
		exit();
	}

	$PANEL_HEADING = "PRAYER REQUEST";
	$Tbl="prayer";
	$FieldsArr=array();
	if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!='')
			$Where=' where place_id="" or place_id=0 or place_id='.$_REQUEST['plc'];
		else
			$Where=' where 1';	
	}
	else
		$Where=' WHERE place_id='.$_SESSION['PlaceID'];
	
	if($_REQUEST['Keyword']!=''){
		$Where.= " and p_name like '%".$_REQUEST['Keyword']."%' ";
	}
	
	$Sort="order by p_id desc";
	$Limit="";
	
	// -------------- REQUEST A CALL -----------------
	$SQL = FetchQuery($Tbl,$FieldsArr,$Where,$Sort,$Limit);
	$objPaging = new paging($SQL, "p_id",10);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$RecordSet = $objDB->select($objPaging->get_query());
	if($RecordSet)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	
?>
<script language="JavaScript">
	function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_prayer.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmPrayer.ID.value = ID;
			document.FrmPrayer.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmPrayer.Process.value = Process;
			document.FrmPrayer.action = 'manage_prayer.php';
			document.FrmPrayer.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmPrayer;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($RecordSet)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
	
</script>
<section class="content-header">
  <h1>
   <img src="<?php echo ADMIN_IMAGE_PATH;?>/prayer.jpg" width="30" height="30" /> &nbsp;&nbsp;<?php echo $PANEL_HEADING?>
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
        	<?php showMessage();	?>
          	<form name="FrmPrayer" class="form-horizontal" method="post" action="">
				<input type="hidden" name="ID" id="ID" value="">
			    <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<div class="form-group col-md-12">
					<?php if($_SESSION['AdminID']==1){ ?>
					<div class="col-md-2">
						<select style="width:85px;" name="PlaceValue" class="InputBox" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=prayer_list&plc='+this.value; else window.location='index.php?p=prayer_list';">
							<option value="">All Places</option>
							<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
						</select>
					</div>
					
					<?php } ?>
					<div class="col-md-2">
					<label for="inputEmail" class="col-ms-2">Search by name:</label>
					</div>
					<div class="col-md-2">
						<input type="text" size="15" value="<?php echo $_REQUEST['Keyword']?>" name="Keyword" class="form-control" />
					</div>
					<div class="col-md-1">
						<input type="submit" value="Search" class="btn btn-default"/>
					</div>
					<div class="col-md-1">
						<input type="button" value="SHOW ALL" class="btn btn-default" onclick="window.location='index.php?p=prayer_list'" />
					</div>
					<div class="col-md-4" style="float:right;text-align:right">
						<?php echo $objPaging->show_paging()?>
					</div>
				</div>
				<div class="table-responsive col-md-12" >
                  	<table id="example2" class="table table-bordered table-striped">
                  		<thead>
                  			<tr height="25" class="SmallBlackHeading">
								<th width="4%" align="center" class="BottomBorder">
									<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
								</th>
								<th width="4%" align="center" class="BottomBorder">#</th>
								<th width="35%" align="left" class="BottomBorder">Name</th>
					            <th width="15%" align="left" class="BottomBorder">Date</th>
								<?php if($_SESSION['AdminID']==1){ ?>
								<th width="15%" align="center" class="BottomBorder">Place</th>
								<?php } ?>
								<th width="15%" align="center" class="BottomBorder">Options</th>
							</tr>
                  		</thead>
                  		<tbody>
                  			<?php
								$i=0;
								$r=$start_limit+1;
								if(!empty($RecordSet))
								{
									for($i=0;$i<count($RecordSet);$i++)
									{ 

										if(($i+1)%2==0)
											$bg=BGCOLOR_ODD_ROW;
										else
											$bg=BGCOLOR_EVEN_ROW;
							?>
							<!-- bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';" -->
							<tr>
								<td align="center">
									<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $RecordSet[$i]['p_id']?>">
								</td>
								<td align="center" class="Numbers"><?php echo $i+1?></td>
								<td align="left"><?php echo stripslashes($RecordSet[$i]['p_name']); ?>
									<?php if($RecordSet[$i]['type']!='') echo "(".stripslashes($RecordSet[$i]['type']).")"; ?>
								</td>
					            <td align="left"><?php echo date("d/m/Y H:i:s",strtotime($RecordSet[$i]['created']));?></td>
								<?php if($_SESSION['AdminID']==1){ ?>
								<td align="center">
									<?php if($RecordSet[$i]['place_id']!='' && $RecordSet[$i]['place_id']!='0' || $_REQUEST['plc']!=''){ if($_REQUEST['plc']!='') $pl = $_REQUEST['plc'];else $pl=$RecordSet[$i]['place_id'];  echo stripslashes(FetchValue('county','c_name','c_id',$pl)); }else{
									$pl = FetchData("county",array(),""); 
															$pl1=array(); 
															for($p=0;$p<count($pl);$p++) 
																$pl1[] = str_replace(","," -",stripslashes($pl[$p]['c_name'])); 
															echo @implode(', ',$pl1);
								}?>
								</td>
								<?php } ?>
								<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/reply.png" style="cursor:pointer" alt="Reply" title="Reply" width="23" height="23" onclick="window.open('includes/prayer_reply.php?id=<?php echo $RecordSet[$i]['p_id'];?>', '', 'scrollbars=1,width=500,height=400');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" style="cursor:pointer" alt="Detail" title="Detail" onclick="window.open('includes/prayer_detail.php?id=<?php echo $RecordSet[$i]['p_id'];?>', 'Quote', 'width=500,height=250,scrollbars=1');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onClick="javascript: singleDel('<?php echo $RecordSet[$i]['p_id']?>', 'DELETE');"></td>
							</tr>
							<?php
									$r++;
									}
								}
							?>
                  		</tbody>
                  	</table>
                </div>
				<div class="col-md-12 col-xs-12">
						<?php if(count($RecordSet)) print $page_link; else print "&nbsp;"; ?>
				</div>
                <div class="col-md-6 col-xs-6">
                	<input type="submit" name="delete" value="DELETE" class="btn btn-default" onClick="return ValidateSelection(this.form);">
                 </div>
                <div class="col-md-6 col-xs-6">
                 	<?php echo $objPaging->show_paging()?>
                 </div>
			</form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
