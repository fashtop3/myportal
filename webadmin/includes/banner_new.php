<?php
	$MODE="ADD";
	$PANEL_HEADING="CATEGORY";

	if(isset($_REQUEST['cms_id']) && !empty($_REQUEST['cms_id']))
	{
		$MODE="UPDATE";

		$TblFieldsArr = array
		(
			//table name=>feilds name
			"slider"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['cms_id'];
		$Sort="";
		$Limit="";

		$info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
	}
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#ContentDesc').val(con); // put it in the textarea
	});
	$("#frmAdmin").validate();
});
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "ContentDesc",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 400,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
           
		}
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/addmember.jpg" width="30" height="30" /> <?php echo $MODE;?> BANNER
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
            <?php showMessage(); ?>
            
            <form class="form-horizontal" action="manage_banner.php" method="post" enctype="multipart/form-data" id="frmAdmin" name="FrmCategoryAddEdit">
                <?php if($MODE=='UPDATE') { ?>
            <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label"></label>
                  <div class="col-sm-10">
                        <img src="../uploads/banner/<?php echo $info[0]['image'];?>" width="300" />
                  </div>
                </div>
            
            <?php } ?>
                <input type="hidden" name="Process" value="<?php echo $MODE?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
                <input type="hidden" name="OldImage" value="<?php echo $info[0]['image'];?>">
                <input type="hidden" name="OldPImage" value="<?php echo $info[0]['popup_image'];?>">
                <input type="hidden" name="TOldImage" value="<?php echo $info[0]['thumb_icon'];?>">
                <input type="hidden" name="cms_id" value="<?php echo $_REQUEST['cms_id']?>">
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-5">
                        <input name="Title" title=" *" validate="required:true" value="<?php echo stripslashes($info[0]['title']);?>" id="Title" class="form-control" type="text">
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Image</label>
                  <div class="col-sm-5">
                        <input type="file" name="CImage" id="CImage" <?php if($MODE=='ADD'){ ?>title=" *" validate="required:true"<?php } ?> accept="JPG|JPEG|GIF|PNG" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Popup Image</label>
                  <div class="col-sm-5">
                        <input type="file" name="PImage" id="PImage" accept="JPG|JPEG|GIF|PNG" />
                  </div>
                </div>
                <center><h6>OR</h6></center>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Link</label>
                  <div class="col-sm-5">
                        <input type="text" name="Links" id="Links" value="<?php echo $info[0]['url']?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-5">
                        <textarea title=" *" validate="required:true" name="ContentDesc" cols="65" rows="5" class="form-control" id="ContentDesc" style="width: 300px;" ><?php echo stripslashes($info[0]['description']) ?></textarea>
                  </div>
                </div>
                <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                            <input name="submit" id="submit1" value="<?php echo ucwords(strtolower($MODE));?> Now" class="btn btn-success" type="submit">
                            <a class="btn btn-default" href="index.php?p=banners&pg_no=<?php echo $_REQUEST['pg_no']?>">Back</a>
                            
                          </div>
                        </div>            
                <?php if($info[0]['popup_image'] != ''){ ?>   Popup Image<br />
                <img src="../uploads/banner/<?php echo $info[0]['popup_image']?>" width="450" alt="" />
                <?php } ?>
            </form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
