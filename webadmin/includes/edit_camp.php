<script type="text/javascript">
$(document).ready(function(){  		
	$("#frmCamp").validate();
});
</script>
<?php
if($_SESSION['Desc']=='')
	$_SESSION['Desc'] = $_REQUEST['id'];

$id = $_REQUEST['id'];
if($id==''){
	$id = $_SESSION['Desc'];
}
$uid = $id;
$user = FetchData("camp_user",array(),"where id= '".$uid."'");
$req = FetchData("camp_request",array(),"where user_id='".$uid."'");
$cid = GetFieldData("camp_meeting_user","camp_id","where user_id='".$uid."'");
$req = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
$did = GetFieldData("camp_meeting","donation_id","where id='".$cid."'");
$type = GetFieldData("camp_meeting","category","where id='".$cid."'");
$per = FetchData("camp_meeting",array(),"where donation_id='".$did."' and category='".$type."' order by price desc");
if(count($_SESSION['Name'])==0){
	for($i=0;$i<count($per);$i++){
		for($j=0;$j<count($req);$j++){
			if($per[$i]['id']==$req[$j]['camp_id']){
				$_SESSION['Name'][$i] = $req[$j]['person'];
				break;
			}
			else
				$_SESSION['Name'][$i] = 0;
		}
	}
}	
?>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="images/meeting.gif" width="48" height="48" /></td>
		<td class="tbl_head" height="24">CAMP MEETING - EDIT USER REGISTRATION</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<form action="index.php?p=edit_camp2&pg_no=<?php echo $_REQUEST['pg_no']?>" method="post" name="frmCamp" id="frmCamp">
				<input type="hidden" value="<?php echo $id;?>" name="Desc" />
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<table width="100%" border="0" cellspacing="2" cellpadding="3" class="form">
					<tr>
						<td height="28" align="left" valign="top">
							<h3><?php echo stripslashes($user[0]['description']);?></h3>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">
							<table width="100%" border="0" cellspacing="2" class="content" cellpadding="3">
								<?php for($i=0;$i<count($per);$i++){ 
						if($_SESSION['Name'][$i]!='')
							$val = $_SESSION['Name'][$i];
						else{
							if($i==0)
								$val = "1";
							else
								$val = "0";
						}
					?>
								<tr>
									<td class="fieldlabel" width="25%" align="left" valign="top"><?php echo stripslashes($per[$i]['title'])?></td>
									<td class="fieldlabel" align="left" valign="top">
										<input value="<?php echo $val;?>" type="text" title=" *" validate="required:true,number:true" id="Name[]" name="Name[]" class="InputBox" style="width:50px; height:20px;" />
										Person (&pound;<?php echo stripslashes($per[$i]['price'])?>/person) </td>
								</tr>
								<?php } ?>
							</table>
							<label class="error" for="Name[]" style="display:none;">Please enter no. of person in one box at least.</label>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top"><br />
							<input type="button" onclick="window.location='index.php?p=accmodation&pg_no=<?php echo $_REQUEST['pg_no']?>'" value="Previous" class="Btn" style="cursor:pointer;" />
							<span style="float:right">
							<input type="submit" value="Next" class="Btn" style="cursor:pointer;" />
							</span> </td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
