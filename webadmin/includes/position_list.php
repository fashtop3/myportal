<?php
	$sql = "select * from member_position";
	/*if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!='')
			$sql.=' where place_id='.$_REQUEST['plc'];
		else*/
			$sql.=' where 1';	
/*	}
	else
		$sql.=' WHERE place_id='.$_SESSION['PlaceID'];
*/
	if($_REQUEST['Search']!='')
		$sql .= " and title like '%".$_REQUEST['Search']."%'";
		
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$position = $objDB->select($objPaging->get_query());

		if($position)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_position.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_position.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($position)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}

</script>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/position.jpg" width="48" height="48" /></td>
		<td class="tbl_head" height="24">POSITION MANAGER</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<?php showMessage();	?>
			<form name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td colspan="4" class="content" height="25">
							<input type="Button" name="ADDNEW" value="ADD NEW" class="Btn" onClick="window.location.href='index.php?p=position_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
							<?php if($_SESSION['AdminID']==1){ ?>
							<!--select name="PlaceValue" class="InputBox" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=position_list&plc='+this.value; else window.location='index.php?p=position_list';">
                <option value="">All Places</option>
                <?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
              </select-->
							<?php } ?>
							<input type="button" value="SHOW ALL" class="Btn" onclick="window.location='index.php?p=position_list'" />
							Search by Title:
							<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="InputBox" />
							<input type="submit" value="Search" class="Btn" />
							<span class="column_head">Total No of Results:&nbsp;<?php echo count($position)?></span> </td>
						<td height="25" colspan="2" align="right"  class="BottomBorder"><?php echo $objPaging->show_paging()?></td>
					</tr>
					<tr>
						<td colspan="6" class="BottomBorder"></td>
					</tr>
					<tr height="25" class="SmallBlackHeading">
						<td width="5%" align="center" class="BottomBorder">
							<!--input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);"-->
						</td>
						<td width="5%" align="center" class="BottomBorder">#</td>
						<td width="60%" align="left" class="BottomBorder">Title</td>
						<td width="" align="left" class="BottomBorder">Type</td>
						<?php if($_SESSION['AdminID']==1){ ?>
						<!--td width="" align="left" class="BottomBorder">Place</td-->
						<?php } ?>
						<td align="center" class="BottomBorder">Options</td>
					</tr>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($position))
							{
								for($i=0;$i<count($position);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
						<td align="center">
							<!--input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $position[$i]['id']?>"-->
							&nbsp;</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo ucwords(stripslashes($position[$i]['title'])); ?></td>
						<td align="left"><?php echo ucwords(stripslashes($position[$i]['type'])); ?> Member</td>
						<?php if($_SESSION['AdminID']==1){ ?>
						<!--td align="left"><?php echo stripslashes(FetchValue('county','c_name','c_id',$position[$i]['place'])); ?></td-->
						<?php } ?>
						<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=position_addedit&id=<?php echo $position[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp;
							<!--img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $position[$i]['id']?>', 'DELETE');" />&nbsp; -->
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="Detail" title="Detail" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onClick="window.location.href='index.php?p=position_detail&id=<?php echo $position[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" /> </td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
					<tr>
						<td colspan="6" class="<?php echo $class?>" align="right">
							<?php if(count($position)) print $page_link; else print "&nbsp;"; ?>
						</td>
					</tr>
					<tr height="25">
						<td colspan="6" class="TopBorder">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="13%">
										<!--input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);"-->
									</td>
									<td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
