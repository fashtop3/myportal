<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"cd_category"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$Photo = "<img src='../uploads/contact/big/".stripslashes($Info[0]['image'])."' width=75 height=75>";	
		
}else
	$MODE="ADD";
	
?>
<script>
  $(document).ready(function(){
  	
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required",
			Type: "required"			
		},
		messages: {
			Title: "Please enter title",
			Type: "Please select type"
		}
	});	
  });
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/addcontactcategory.jpg" width="48" height="48" />&nbsp;&nbsp;<?php echo $MODE;?> MUSIC &amp; TRACK CATEGORY    
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
        	<?php showMessage(); ?>
			<form method="post" class="form-horizontal" action="manage_cd_category.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<div class="form-group">
					<label for="inputEmail" class="col-sm-2 control-label">Type</label>
					<div class="col-sm-5">
						<select name="Type" class="form-control" id="Type" style="width:300px;">
							<option value="">--Select--</option>
							<option <?php if($Info[0]['type']=='CD') echo "selected";?> value="CD">CDs</option>
							<option <?php if($Info[0]['type']=='DVD') echo "selected";?> value="DVD">DVDs</option>
							<option <?php if($Info[0]['type']=='HM') echo "selected";?> value="HM">Higherway Magazine</option>
							<option <?php if($Info[0]['type']=='MT') echo "selected";?> value="MT">Music &amp; Track</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-sm-2 control-label">Title</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="Title" id="Title" value="<?php echo stripslashes($Info[0]['title'])?>" style="width:300px;" />
					</div>
				</div>
				<input type="hidden" value="<?php echo $MODE?>" name="action" />
				<div class="form-group">
                	<div class="col-sm-offset-2 col-sm-10">
                		<input value="<?php echo $MODE?>" class="btn btn-success" type="submit" name="submit1" id="submit1">
                		<input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=music_category&pg_no=<?php echo $_REQUEST['pg_no']?>';">
                  	</div>
              	</div>
			</form>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
