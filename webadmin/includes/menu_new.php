<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"menu"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);			
		
}else
	$MODE="ADD";
	
?>
<script>
  $(document).ready(function(){
		$("#frmAdmin").validate();
	});
</script>
<section class="content-header">
  <h1>
  <img src="<?php echo ADMIN_IMAGE_PATH;?>/addcontactcategory.jpg" /> Front Menu
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          
			<form class="form-horizontal"  method="post" action="manage_menu.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>" />
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input type="hidden" value="<?php echo $MODE?>" name="action" />
				<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Menu Title</label>
                  <div class="col-sm-5">
                    	<input type="text" name="Title" id="Title" size="30" title=" *" validate="required: true" class="form-control" value="<?php echo stripslashes($Info[0]['title'])?>" />
                  </div>
                </div>
                <!-- <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Parent Menu</label>
                  <div class="col-sm-5">
                    	<select name="Parent" id="Parent" class="form-control" style="width:200px;">
              <option value="">--Select--</option>
              <?php echo FillCombo1("menu","title","id",$Info[0]['parent_id'],"where id != '".$Info[0]['id']."'");?>
            </select>
                  </div>
                </div>
					 <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Link</label>
                  <div class="col-sm-5">
                    	<input type="text" name="Links" id="Links" title=" *" validate="required: true,url: true" size="70" class="form-control" value="<?php echo $Info[0]['title_link']?>" />
                  </div>
                </div> -->
					 <div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">

                            <input value="<?php echo $MODE?>" class="btn btn-success" type="submit" name="submit1" id="submit1">
							<input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=menu_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
                          </div>
                        </div>
			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
