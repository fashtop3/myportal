<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"news_gallery_category"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);		
}else
	$MODE="ADD";
if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $Info[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}	
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required"/*,
			NewsId:"required"*/
		},
		messages: {
			/*NewsId: "Please select news",*/
			Title:"Please enter title"
		}
	});	
  });
  function dayvalue(id)
	{			
			$.post("gallery_chk.php",{ username:id } ,function(data)
			{				
				document.getElementById('DayName1').innerHTML = data;
			});
	}
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/addphotogallery.jpg" height="30" /> <?php echo $MODE;?> IMAGE GALLERY
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<form  class="form-horizontal" method="post" action="manage_news_gallery.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<?php if($_SESSION['AdminID']==1){ ?>
				<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Place</label>
                  <div class="col-sm-5">
                    	<select name="Place" class="form-control" id="Place" onchange="dayvalue(this.value)">
							<?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
						</select>
                  </div>
                </div>
                <?php }else{ ?>
                <input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
                <?php } ?>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-5">
                    	<input type="text" class="form-control" name="Title" id="Title" value="<?php echo stripslashes($Info[0]['title'])?>" />
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">News</label>
                  <div class="col-sm-5">
                    	<select class="form-control" name="NewsId" id="NewsId">
							<option value="">--Select News--</option>
							<?php if($_SESSION['PlaceID']!=0)
  			$where = 'where place_id='.$_SESSION['PlaceID'];
		else
			$where = $where = 'where place_id='.$pl;
		echo FillCombo1('news','title','id',$Info[0]['news_id'],$where);
	?>
						</select>
                  </div>
                </div>

		<input type="hidden" value="<?php echo $MODE?>" name="action" />
		<div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">
                          	<input value="<?php echo $MODE?> GALLERY" class="btn btn-success" type="submit" name="submit1" id="submit1">
			<input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=news_gallery_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">


                          </div>
                        </div>
			
	
			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
