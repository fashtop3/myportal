<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"camp_user"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/meeting.gif" width="48" height="48" /> </td>
		<td class="tbl_head" height="24">Internation Family Detail - <?php echo stripslashes($Info[0]['description'])?></td>
	</tr>
	<tr>
		<td colspan="2">
			<?php if($_SESSION['Msg']!=''){ echo '<div id="divmsg" class="notice" style="color:#FF0000;">'.$_SESSION['Msg'].'</div>'; unset($_SESSION['Msg']);}?>
			<table width="100%" class="form" cellpadding="5" cellspacing="5">
				<tr>
					<td colspan="2" style="padding-left:5px;font-size:16px"><b>ID : <?php echo $Info[0]['id']?> </b> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;font-size:16px"><b>Booking Reference Number : <?php echo stripslashes($Info[0]['booking_ref_no'])?> </b> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Name : </b><?php echo stripslashes($Info[0]['name'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Email :</b> <?php echo stripslashes($Info[0]['email'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Phone :</b> <?php echo stripslashes($Info[0]['phone'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Mobile :</b> <?php echo stripslashes($Info[0]['mobile'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Address :</b> <?php echo stripslashes($Info[0]['address'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Country :</b> <?php echo stripslashes($Info[0]['country'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Postcode :</b> <?php echo stripslashes($Info[0]['postcode'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Date :</b> <?php echo $Info[0]['udate']?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;border:1px solid #CCCCCC;"><b>List of persons who wants to come :</b> <br />
						<?php $rm = $Info[0]['room'];
					$rms = @explode(',',$rm);
					?>
						<?php $per = @explode(', ',$Info[0]['persons']);
					?>
						<table width="100%" class="content" cellpadding="5" cellspacing="1" border="0">
							<tr>
								<td width="5%" align="left" valign="top"><b>No</b></td>
								<td width="30%" align="left" valign="top"><b>Full Name</b></td>
								<td align="left" valign="top"><b>Room No</b></td>
							</tr>
							<?php for($j=0;$j<count($per);$j++){ ?>
							<tr>
								<td align="left" width="5%" valign="top"><?php echo $j+1?></td>
								<td align="left" width="20%" valign="top"><?php echo $per[$j]?></td>
								<td align="left" valign="top"><b><?php echo $rms[$j]?></b> </td>
							</tr>
							<?php } ?>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<center>
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=family_camp&pg_no=<?php echo $_REQUEST['pg_no']?>';">
							<input value="REPLY" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=family_camp_reply&id=<?php echo $_REQUEST['id']?>&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						</center>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
