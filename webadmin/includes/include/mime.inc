<?php
	function chkfile($name,&$icon) 
	{
		/*set $rc to SHOW_EDIT to allow editing or set to HIDE_EDIT to disable the edit icon*/
		global $ROOTURL,$dir;
		
		$rc=HIDE_EDIT;
		$icon="images/file.gif";
		
		if (strpos($name,".exe")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_exe.gif";
			return($rc);
		}
		
		if (strpos($name,".txt") || strpos($name,".ini") || strpos($name,".log")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_txt.gif";
			return($rc);
		}
		
		if (strpos($name,".php") || strpos($name,".php3")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_php.gif";
			return($rc);
		}
		
		if (strpos($name,".asp") || strpos($name,".asa")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_php.gif";
			return($rc);
		}
		
		if (strpos($name,".htm") || strpos($name,".html")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_htm.gif";
			return($rc);
		}
		
		if (strpos($name,".inc")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_inc.gif";
			return($rc);
		}
		
		if (strpos($name,".inf") || strpos($name,".css")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_inf.gif";
			return($rc);
		}
		
		if (strpos($name,".sql")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_inc.gif";
			return($rc);
		}
		
		if (strpos($name,".cls")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_cls.gif";
			return($rc);
		}
		
		if (strpos($name,".js")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_js.gif";
			return($rc);
		}
		
		if (strpos($name,".cfg")) {
			$rc=SHOW_EDIT;
			$icon="images/file.gif";
			return($rc);
		}
		
		if (strpos($name,".jpg") || strpos($name,".JPG")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_jpg.gif";
			return($rc);
		}
		
		if (strpos($name,".gif") || strpos($name,".bmp") || strpos($name,".ico")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_gif.gif";
			return($rc);
		}
		
		if (strpos($name,".wav")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_wav.gif";
			return($rc);
		}
		
		if (strpos($name,".mp3")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_mp3.gif";
			return($rc);
		}
		
		if (strpos($name,".fla")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_flashworkfile.gif";
			return($rc);
		}
		
		if (strpos($name,".swf") || strpos($name,".swi")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_flashfile.gif";
			return($rc);
		}
		
		if (strpos($name,".doc")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_office_word.gif";
			return($rc);
		}
		
		if (strpos($name,".ppt")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_office_powerpoint.gif";
			return($rc);
		}
		
		if (strpos($name,".xls")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_office_excel.gif";
			return($rc);
		}
		
		if (strpos($name,".xml") || strpos($name,".dtd") || strpos($name,".xsl")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_xml.gif";
			return($rc);
		}
		
		if (strpos($name,".cmd") || strpos($name,".x")) {
			$rc=SHOW_EDIT;
			$icon="images/icon_cmd.gif";
			return($rc);
		}
		
		if (strpos($name,".zip")|| strpos($name,".rar")){
			$rc=HIDE_EDIT;
			$icon="images/icon_zip.gif";
			return($rc);
		}
		
		if (strpos($name,".pdf")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_pdf.gif";
			return($rc);
		}
		
		if (strpos($name,".psd")) {
			$rc=HIDE_EDIT;
			$icon="images/icon_psd.gif";
			return($rc);
		}
		
		return($rc);
}
?>