<?php
	
	
		$MODE="ADD";
		if(isset($_SESSION['config']))
			$RecordSet[0] = $_SESSION['config'];
	
	
?>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <form name="frmcms" id="frmcms" method="post" action="manage_config.php">
	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td height="24">
				<table class="link" width="100%" border="0" cellpadding="0" cellspacing="0">
					<tbody>
						<tr>
							<td height="23">&nbsp;&nbsp;&nbsp;&nbsp;Welcome <?php echo $AdminRecordSet; ?></td>
							<td align="right">&nbsp;&nbsp;<a href="index.php?p=logout">Logout</a>&nbsp;&nbsp;&nbsp;&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2" bgcolor="#e3e3e3" height="1"><img src="images/spacer.gif" width="1" height="1"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
		<tr>
			<td height="66">
				<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
					<tr>
						<td width="66" height="66"><img src="images/content_icon.jpg" vspace="4" width="59" height="59" hspace="8"></td>
						<td class="head-title"> &nbsp;<?php echo ucwords(strtolower($MODE));?> Configuration</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="center">
				<table width="85%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td>
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td class="border" bgcolor="#f7f8fa">
										<table width="100%" border="0" cellpadding="0" cellspacing="0" height="100%">
											<tr>
												<td class="table-shade-bg">
													<table class="normaltext" width="100%" border="0" cellpadding="0" cellspacing="10">
														<tr>
															<td class="link" width="100%">Configuration Details</td>
														</tr>
														<tr>
															<td colspan="3" align="center">
																<?php ShowMessage(); ?>
															</td>
														</tr>
														<tr>
															<td class="link">
																<table class="normaltext" width="100%" border="0" cellpadding="0" cellspacing="5">
																	<tr>
																		<td width="17%">
																			<div align="right">Title: </div>
																		</td>
																		<td width="83%" valign="top">
																			<input name="ConfigTitle" type="text" class="input" id="cc_title"  value="<?php echo stripslashes($RecordSet[0]["config_title"]) ?>" size="100" width="400" >
																		</td>
																	</tr>
																	<tr>
																		<td width="17%">
																			<div align="right">Key: </div>
																		</td>
																		<td width="83%" valign="top">
																			<input name="ConfigKey" type="text" class="input" id="cc_key" value="<?php echo stripslashes($RecordSet[0]['config_key']) ?>" size="50">
																			<div id="err"><?php echo htmlspecialchars($_SESSION['cckey']);unset($_SESSION['config'],$_SESSION['cckey']);?></div>
																		</td>
																	</tr>
																	<tr>
																		<td width="17%">
																			<div align="right">Value: </div>
																		</td>
																		<td width="83%" valign="top">
																			<input type="text" name="ConfigValue" class="input" id="cc_value" value="<?php echo stripslashes($RecordSet[0]['config_value']) ?>" size="50"/>
																		</td>
																	</tr>
																	<tr>
																		<td width="17%">
																			<div align="right">Description: </div>
																		</td>
																		<td width="83%" valign="top">
																			<textarea name="ConfigDesc" class="input" rows="5" cols="35"  ><?php echo stripslashes($RecordSet[0]['config_description']) ?></textarea>
																		</td>
																	</tr>
																	<tr class="error" height="22">
																		<td>&nbsp;</td>
																		<td></td>
																	</tr>
																	<tr>
																		<td>&nbsp;</td>
																		<td>
																			<input name="Update" type="submit" class="button" value="<?php echo $MODE;?>">
																			&nbsp;&nbsp;
																			<input name="back" type="button" class="button" value="BACK" onclick="window.location.href='index.php?p=general_config'">
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</table>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<input type=hidden name=action value="<?php echo $MODE;?>">
	<input type=hidden name=ID value="<?php echo $_REQUEST['ID'];?>">
	<input type="hidden" name="Validation" value="Field=cc_title|Alias=Title|Validate=Blank^Field=cc_key|Alias=Key|Validate=Blank^Field=cc_value|Alias=Value|Validate=Blank">
</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
