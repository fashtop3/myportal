<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"curriculum"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder" ></script>
<script type="text/javascript" src="js/lightbox.js" ></script>
<link href="css/lightbox.css" rel="stylesheet" type="text/css">


			<section class="content-header">
  <h1>
  <img src="<?php echo ADMIN_IMAGE_PATH;?>/curriculum.jpg" width="30" height="30" /> CURRICULUM DETAIL
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="100%" class="form">
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Category : </b> <?php echo stripslashes(FetchValue1('curriculum_category','title','id',$Info[0]['category_id']));?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Title : </b> <?php echo stripslashes($Info[0]['title'])?> </td>
				</tr>				
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Date : </b> <?php echo date('F d',strtotime($Info[0]['cdate']));?> </td>
				</tr>												
				<tr>
					<td colspan="2" style="padding-left:5px;"></td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;"><?php echo str_replace('"images/','"../images/',stripslashes($Info[0]['full_description']))?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
							<input value="BACK" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=curriculum_primary&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
