<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/functions.php");
		CheckPermission(); 
		exit();
	}

	$PANEL_HEADING = "MANAGE PLAYLIST SONGS";
	$Tbl="playlist";
	$FieldsArr=array();
	if($_SESSION['AdminID']==1)
	{
		if($_REQUEST['plc']!=''){
			$Where=' where place_id='.$_REQUEST['plc'];
			$path1 = '&plc='.$_REQUEST['plc'];
		}
		else{
			$Where=' where 1=1';	
			$path1 = '';
		}
	}
	else
		$Where='where place_id='.$_SESSION['PlaceID'];
	
	if($_REQUEST['Search']!='')
		$Where .= " and title like '%".$_REQUEST['Search']."%'";
	if($_REQUEST['admins']!='')
		$Where .= ' and createdby = '.$_REQUEST['admins'];		
	if($_REQUEST['view']=='my_view'){	
		$Where .= ' and createdby = '.$_SESSION['AdminID'];
		$view = strtoupper("All Songs");
		$viewpath = 'index.php?p=playlist'.$path1;
		$path2 = '&view=my_view';
	}
	else{
		$view = strtoupper("My Added Songs");	
		$viewpath = 'index.php?p=playlist&view=my_view'.$path1;
		$path2 = '';
	}
	$Sort="order by id desc";
	$Limit="";
	
	// -------------- REQUEST A CALL -----------------
	$SQL = FetchQuery($Tbl,$FieldsArr,$Where,$Sort,$Limit);
	$objPaging = new paging($SQL, "id");
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$RecordSet = $objDB->select($SQL);
	if($RecordSet)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	
?>
<script language="JavaScript">
	function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_playlist.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_playlist.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($RecordSet)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
	
	function ValidateSelection1(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($RecordSet)?>;i++)
		{
			if(document.getElementById('ad'+i).checked)
			{
				x=false;
				frm.Process.value='ADDTOPLAYLIST';
				frm.action='manage_playlist.php';	
				frm.submit();
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}	
</script>
<section class="content-header">
  <h1><img src="<?php echo ADMIN_IMAGE_PATH;?>/playlist.jpg" width="30" height="30" /> <?php echo $PANEL_HEADING?></h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
        	<?php showMessage();	?>
          	<form name="FrmSMS" method="post" action="">
				<input type="hidden" name="ID" id="ID" value="">
			    <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<div class="form-group col-md-12">
					<?php if($_SESSION['AdminID']==1){ ?>
					<div class="col-md-2">
						<select style="width:125px" name="PlaceValue" class="form-control" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=playlist&plc='+this.value+'<?php echo $path2; ?>'; else window.location='index.php?p=playlist'+'<?php echo $path2; ?>';">
							<option value="">All Places</option>
							<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
						</select>
					</div>
					<div class="col-md-2">
					<select style="width:165px" name="admins" class="form-control" id="admins" onchange="if(this.value!='') window.location='index.php?p=playlist&admins='+this.value; else window.location='index.php?p=playlist';">
						<option value="">--Select Administrator--</option>
						<?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
					</select>
					</div>
					<?php } ?>
					<div class="col-md-2">
					<label for="inputEmail" class="col-ms-2">Search by Title:</label>
					</div>
					<div class="col-md-2">
						<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="form-control" />
					</div>
					<div class="col-md-1">
						<input type="submit" value="Search" class="btn btn-default"/>
					</div>
				</div>
				<div class="form-group">
					<div class="col-md-6">
						<input type="button" name="ADDNEW" value="ADD NEW SONG" class="btn btn-default" onClick="window.location.href='index.php?p=new_song&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
						&nbsp;&nbsp;
						<input type="button" name="songs" value="<?php echo $view?>" class="btn btn-default" onClick="window.location.href='<?php echo $viewpath; ?>'" style="width:130px;"/>&nbsp;&nbsp;
						<input type="button" value="SHOW ALL" class="btn btn-default" onclick="window.location='index.php?p=playlist'" />
					</div>
					<div class="col-md-6">
						<span class="column_head" style="float:right;padding-top:5px;">Total No of Results:&nbsp;<?php echo count($RecordSet)?></span>
					</div>
				</div>
				<div class="table-responsive col-md-12" >
                  	<table id="example2" class="table table-bordered table-striped">
                  		<thead>
                  			<tr height="25" class="SmallBlackHeading">
								<th width="4%" align="center" class="BottomBorder">
									<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
								</th>
								<th width="4%" align="center" class="BottomBorder">#</th>
								<th width="34%" align="left" class="BottomBorder">Title</th>
								<?php if($_SESSION['AdminID']==1){ ?>
								<th width="23%" align="center" class="BottomBorder">Place</th>
								<?php } ?>
								<th width="23%" align="center" class="BottomBorder">Add To Playlist</th>
								<th width="12%" align="center" class="BottomBorder">Options</th>
							</tr>
                  		</thead>
                  		<tbody>
                  		<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($RecordSet))
							{
								for($i=0;$i<count($RecordSet);$i++)
								{ 
									/*if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;

										 bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';"*/
						?>
						<tr>
							<td align="center">
								<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $RecordSet[$i]['id']?>">
							</td>
							<td align="center" class="Numbers"><?php echo $i+1?></td>
							<td align="left"><?php echo ucwords(stripslashes($RecordSet[$i]['title'])); ?></td>
							<?php if($_SESSION['AdminID']==1){ ?>
							<td align="center"><?php echo stripslashes(FetchValue('county','c_name','c_id',$RecordSet[$i]['place_id']));?> </td>
							<?php } ?>
							<td align="center">
								<input type="hidden" name="place[]" id="place<?php echo $i?>" value="<?php echo $RecordSet[$i]['place_id']?>" />
								<input type="checkbox" name="ad[]" id="ad<?php echo $i?>" value="<?php echo $RecordSet[$i]['id']?>" <?php if($RecordSet[$i]['play']==1){?> checked="checked" <?php } ?> />
							</td>
							<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $RecordSet[$i]['id']?>', 'DELETE');" /></td>
						</tr>
						<?php
								$r++;
								}
							}
						?>
						</tbody>
                  	</table>
                 </div>
                 <div class="col-md-6 col-xs-6">
                 	<input type="submit" name="delete" value="DELETE" class="btn btn-default" onClick="return ValidateSelection(this.form);">&nbsp;&nbsp;&nbsp;
					<input type="submit" name="play1" value="UPDATE PLAYLIST" class="btn btn-default" onClick="return ValidateSelection1(this.form);">
                 </div>
                 <div class="col-md-6 col-xs-6">
                 	<?php if(count($RecordSet)) print $page_link; else print "&nbsp;"; ?>
                 	<?php //echo $objPaging->show_paging()?>
                 </div>
			</form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
