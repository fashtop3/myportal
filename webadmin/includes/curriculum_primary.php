<?php
	$sql = "select * from curriculum";
	if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!=''){
			$sql.=' where category_id in(1,2) and place_id='.$_REQUEST['plc'];
			$path1 = '&plc='.$_REQUEST['plc'];
		}
		else{
			$sql.=' where category_id in(1,2)';	
			$path1 = '';
		}
		$sql .= " and cdate!='0000-00-00' and full_description!=''";
	}
	else
		$sql.=' WHERE place_id='.$_SESSION['PlaceID'];
	
	if($_REQUEST['category']!=''){
		if($_REQUEST['category']>0)
			$sql .= " and category_id = '".$_REQUEST['category']."'";		
		$catid = $_REQUEST['category'];
	}
	else{
		$sql .= " and category_id = '1'";
		$catid = 1;
	}
		
	if($_REQUEST['Search']!='')
		$sql .= " and title like '%".$_REQUEST['Search']."%'";
				
	if($_REQUEST['admins']!='')
		$sql .= ' and createdby = '.$_REQUEST['admins'];
	if($_REQUEST['view']=='my_view'){	
		$sql .= ' and createdby = '.$_SESSION['AdminID'];
		$view = strtoupper("All Curriculums");
		$viewpath = 'index.php?p=curriculum_primary'.$path1;
		$path2 = '&view=my_view';
	}
	else{
		$view = strtoupper("My Added Curriculums");	
		$viewpath = 'index.php?p=curriculum_primary&view=my_view'.$path1;
		$path2 = '';
	}
	
	//$sql .= " order by cdate desc";
	$sql .= " order by id desc";
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_curriculum.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.action = 'manage_curriculum.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/curriculum.jpg" width="30" height="30" /> CURRICULUM (Primary Pal and Answer) MANAGER
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<?php showMessage();	?>
			<form class="form-horizontal" name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<div class="form-group col-md-12">
					<div class="col-md-9">
						<?php if($_SESSION['AdminID']==1){ ?>
						<div class="col-md-3">
							<select name="PlaceValue" class="form-control" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=curriculum_primary&plc='+this.value+'<?php echo $path2; ?>'; else window.location='index.php?p=curriculum_primary'+'<?php echo $path2; ?>';">
								<option value="">All Places</option>
								<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
							</select>
						</div>
						<div class="col-md-3">
							<select name="admins" class="form-control" id="admins" onchange="if(this.value!='') window.location='index.php?p=curriculum_primary&admins='+this.value; else window.location='index.php?p=curriculum_primary';">
								<option value="">--Select Administrator--</option>
								<?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
							</select>
						</div>
						<?php } ?>
						<div class="col-md-2">
							Search by Title:
						</div>
						<div class="col-md-3">
							<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="form-control" />
						</div>
						<div class="col-md-1">
							<input type="submit" value="Search" class="btn btn-default" />
						</div>
					</div>
					<div class="col-md-3" style="text-align: right;float: right;">
						<span class="column_head" style="float:right; padding-top:2px;">Total No of Results:&nbsp;<?php echo count($cms)?></span>
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="col-md-9">
						
							<input type="Button" name="ADDNEW" value="ADD NEW" class="btn btn-default" onClick="window.location.href='index.php?p=curriculum_primary_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
						
							<input type="Button" name="sermons" value="<?php echo $view?>" class="btn btn-default" onClick="window.location.href='<?php echo $viewpath; ?>'" />
						
							<input type="button" value="SHOW ALL" class="btn btn-default" onclick="window.location='index.php?p=curriculum_primary&category=0'" />
						
							<input type="button" value="PRIMARY PAL" class="btn btn-default" onclick="window.location='index.php?p=curriculum_primary&category=1'" />
						
							<input type="button" value="ANSWER" class="btn btn-default" onclick="window.location='index.php?p=curriculum_primary&category=2'" />
						
					</div>
					<div class="col-md-3" style="text-align: right;float: right;">
						<?php echo $objPaging->show_paging()?>
					</div>
				</div>
				<h4><?php echo stripslashes(GetFieldData("curriculum_category","title","where id=".$catid)); ?></h4>
					<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
					<tr height="25" class="SmallBlackHeading">
						<th width="4%" align="center" class="BottomBorder">
							<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
						</th>
						<th width="4%" align="center" class="BottomBorder">#</th>
						<th width="" align="left" class="BottomBorder">Title</th>
                        <th width="15%" align="left" class="BottomBorder">Date</th>
						<th width="" align="left" class="BottomBorder">Category</th>
						<?php if($_SESSION['AdminID']==1){ ?>
						<tdh width="10%" align="center" class="BottomBorder">Place</th>
						<?php } ?>
						<th width="12%" align="center" class="BottomBorder">Options</th>
					</tr></thead>
<tbody>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr>
						<td align="center">
							<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i]['id']?>">
						</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo ucwords(stripslashes($cms[$i]['title'])); ?></td>						
                        <td align="left">
                        	<?php if($cms[$i]['category_id']>2){ ?>
							<?php echo date('d-M-Y',strtotime(GetFieldData("book","lesson_date","where id=".$cms[$i]['lesson_no']))); ?>
                            <?php } else { ?>
                            <?php echo date('d-M-Y',strtotime($cms[$i]['cdate'])); ?>
                            <?php } ?>
                        </td>
						<td align="left"><?php echo stripslashes(GetFieldData("curriculum_category","title","where id=".$cms[$i]['category_id'])); ?></td>
						<?php if($_SESSION['AdminID']==1){ ?>
						<td align="center"><?php echo stripslashes(FetchValue('county','c_name','c_id',$cms[$i]['place_id']));?> </td>
						<?php } ?>
						<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=curriculum_primary_addedit&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=curriculum_primary_detail&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>';" />&nbsp; </td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?></tbody>
	</table>
</div>

<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
					</div>
				</div>


				<div class="form-group col-md-12">
					<div class="col-md-6"><input type="submit" name="delete" value="DELETE" class="btn btn-default" onClick="return ValidateSelection(this.form);">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php echo $objPaging->show_paging()?>
					</div>
				</div>

				
			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
