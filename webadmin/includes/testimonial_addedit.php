<?php
	$ID = loadVariable("id","");
	
	if(isset($ID) && !empty($ID)){
		$MODE = "UPDATE";
		
		$TestiInfo = FetchData("testimonial",array(),"where id=".$ID,'','');
		
		
	}else{
		$MODE = "ADD";		
	}
	if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $Info[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}	
?>
<style type="text/css">
* {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 96%;
}
label {
	width: 10em;
	float: left;
}
label.error {
	float: none;
	color: red;
	padding-left: .5em;
	vertical-align: top;
}
.submit {
	margin-left: 12em;
}
em {
	font-weight: bold;
	padding-right: 1em;
	vertical-align: top;
}
</style>
<script>
  $(document).ready(function(){
  	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#message').val(con); // put it in the textarea
	});
    // validate signup form on keyup and submit
	$("#AddTesti").validate({
		rules: {
			MemberId: "required",
			message: "required",
			Title: "required"
		},
		messages: {
			MemberId: "Please select member",
			message: "Please enter message",
			Title: "Please enter title"
		}
	});
  });
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "message",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
/*            return false;			
			var fileBrowserWindow = new Array();
			fileBrowserWindow["file"] = ajaxfilemanagerurl;
			fileBrowserWindow["title"] = "Ajax File Manager";
			fileBrowserWindow["width"] = "782";
			fileBrowserWindow["height"] = "440";
			fileBrowserWindow["close_previous"] = "no";
			tinyMCE.openWindow(fileBrowserWindow, {
			  window : win,
			  input : field_name,
			  resizable : "yes",
			  inline : "yes",
			  editor_id : tinyMCE.getWindowArg("editor_id")
			});
			
			return false;*/
		}
</script>
<script language="javascript" type="text/javascript" src="js/jquery.simplemodal.js"></script>
<script type="text/javascript">
function showAddPagePopUp(id){
	$('#basic-modal-content').modal();
	$('#basic-modal-content').html('<p><img src="images/busy.gif" width="15" height="15" /></p>');
	$('#basic-modal-content').load("includes/add_member.php");	
}
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/addtestimonial.jpg" width="30" height="30" /> <?php echo $MODE;?> TESTIMONIAL
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<STYLE TYPE="text/css">
.form-horizontal .control-label {
    padding-top: 7px;
    margin-bottom: 0;
    text-align: right;
    font-size: 13px;
}
</STYLE>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          	<div id="basic-modal-content"></div>
			<form  class="form-horizontal" name="AddTesti" id="AddTesti" method="post" action="manage_testi.php" enctype="multipart/form-data">
				<input type="hidden" name="action" value="<?php echo $MODE;?>" />
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input type="hidden" name="ID" value="<?php echo $ID;?>" />

								<?php 
									if($_SESSION['AdminID']==1){ 
										$where = "";
								?>
								<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Place</label>
                  <div class="col-sm-5">
                    <select name="Place" class="form-control" id="Place">
						<?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
					</select>
                  </div>
                </div>
								
								<?php }else{ $where = " where place_id = ".$_SESSION['PlaceID'];?>
								<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
								
								
								<?php } ?>
								<?php /*?><tr>
									<td class="fieldlabel" width="5%">Member</td>
									<td class="fieldarea">
										<select class="InputBox" name="Member" id="Member" style="height:22px;">
											<option value="0">--Select--</option>
											<?php
                        $mem = FetchData("member",array(),$where." order by firstname");
                        for($i=0;$i<count($mem);$i++){
                          if($TestiInfo[0]['createdby']==$mem[$i]['id'])
                            $select = "selected";
                          else
                            $select = "";
                          echo "<option ".$select." value='".$mem[$i]['id']."'>".stripslashes($mem[$i]['firstname']." ".$mem[$i]['lastname'])."</option>";
                        }
                      ?>
										</select>
										<img src="images/add.png" align="absmiddle" style="cursor:pointer" title="Add New Member" onclick="showAddPagePopUp()" /> </td>
								</tr><?php */?>
								<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Testimonial By</label>
                  <div class="col-sm-5">
					<input type="text" class="form-control" name="NonMember" id="NonMember" value="<?php echo stripslashes($TestiInfo[0]['name']);?>" />
										Photo
										<input type="file" name="NonImage" />
										<input type="hidden" name="OldNonImage" value="<?php echo stripslashes($TestiInfo[0]['image'])?>" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-5">
					<input type="text" class="form-control" name="Title" id="Title" style="width:300px;" value="<?php echo stripslashes($TestiInfo[0]['title']);?>" />
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Message</label>
                  <div class="col-sm-5">
                  	<textarea name="message" cols="65" rows="15" id="message" class="form-control"><?php echo stripslashes($TestiInfo[0]['description'])?></textarea>
					
                  </div>
                </div>
                <div class="form-group">

                <div class="col-sm-offset-2 col-sm-10">
                	<input value="<?php echo $MODE?> TESTIMONIAL"  class="btn btn-success" type="submit" id="submit1" name="submit1">
					<input value="CANCEL" class="btn btn-default"  type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=testi_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
                	
                  

                </div>

              </div>
					
			
			</form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>

