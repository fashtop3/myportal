<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/functions.php");
		CheckPermission(); 
		exit();
	}

	//$Tbl="member_product";
	//$FieldsArr=array();
	if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!=''){
			$sql.=' where place_id='.$_REQUEST['plc'];
			$path1 = '&plc='.$_REQUEST['plc'];
		}
		else{
			$sql.=' where 1=1';	
			$path1 = '';
		}
	}	
	else
		$sql.=' WHERE place_id='.$_SESSION['PlaceID'];
		
	if($_REQUEST['Users']!=''){
		if($_REQUEST['Users']=='Unregisterd'){
			if($_REQUEST['Unregister']!='')
			{
				$user = $_REQUEST['Unregister'];
				$where = " where member_id in (".$user.")";		
			}
		}else if($_REQUEST['Users']=='Registered'){
			if($_REQUEST['Register']!='')
			{
				$user = $_REQUEST['Register'];
				$where = " where member_id in (".$user.")";		
			}
		}
	}
	else{		
		$where = "";
	}

	// -------------- REQUEST A CALL -----------------
	$SQL = "select count(member_id) as product, sum(price) as price, sum(qty) as qty, member_id from member_product".$where." group by member_id order by id desc";
	//echo $SQL;
	//$SQL = FetchQuery($Tbl,$FieldsArr," group by member_id order by id desc","","");
	$objPaging = new paging($SQL, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$prod = $objDB->select($objPaging->get_query());
	/*if($prod)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	*/
	
?>
<?php 
$prod1 = FetchData("member_product",array(),"");
$total1 = 0;
for($i=0;$i<count($prod1);$i++){
	$total1 += $prod1[$i]['total_price'];
}
?>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/payment.jpg" width="48" height="48" /></td>
		<td class="tbl_head" height="24">PAYMENT/DONATION HISTORY <span style="float:right">Grand Total: &nbsp;<?php echo htmlentities(CURRENCY_SIGN).number_format($total1,2)?></span> </td>
	</tr>
	<tr>
		<td colspan="2" align="right" class="tbl_head"><span class="column_head">Total
			No of Results:&nbsp;<?php echo count($cms)?></span></td>
	</tr>
	<tr>
		<td colspan="2" align="left">
			<form action="" method="post">
				<table width="100%" border="0" cellpadding="5" cellspacing="1">
					<tr>
						<td width="38%" align="left">
							<select class="InputBox" name="PlaceValue" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=payment_history&plc='+this.value; else window.location='index.php?p=payment_history;">
								<option value="">All Places</option>
								<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
							</select>
							<select name="Users" id="Users" onchange="showUser(this.value)" class="InputBox">
								<option value="">All Users</option>
								<option <?php if($_REQUEST['Users']=='Unregisterd') echo "selected";?> value="Unregisterd">Unregistered Users</option>
								<option <?php if($_REQUEST['Users']=='Registered') echo "selected";?> value="Registered">Registered Users</option>
							</select>
							<script type="text/javascript">
							function showUser(value){
								if(value=='Unregisterd'){
									document.getElementById('Unregisterd').style.display='';
									document.getElementById('Registered').style.display='none';
								}else if(value=='Registered'){
									document.getElementById('Unregisterd').style.display='none';
									document.getElementById('Registered').style.display='';
								}else{
									document.getElementById('Unregisterd').style.display='none';
									document.getElementById('Registered').style.display='none';
								}								
							}
						</script>
						</td>
						<td align="left">
							<?php if($_REQUEST['Users']=='Unregisterd') $style = ""; else $style = "display:none;"; ?>
							<span id="Unregisterd" style=" <?php echo $style?>">
							<?php 
                $sql = "select * from member_product where member_id not in (select id from member) group by member_id";
                $mem = $objDB->select($sql);		
								for($i=0;$i<count($mem);$i++)
									$id[] = $mem[$i]['member_id'];
								$ids = @implode(",",$id);								
              ?>
							<select name="Unregister" class="InputBox">
								<option value="<?php echo $ids?>">All</option>
								<?php 
                for($i=0;$i<count($mem);$i++){
                  if($mem[$i]['member_id']==$_REQUEST['Unregister'])
                    $select = "selected";
                  else
                    $select = "";
                  echo "<option value='".$mem[$i]['member_id']."' ".$select.">".$mem[$i]['member_id']."</option>";
                }
                ?>
							</select>
							</span>
							<?php if($_REQUEST['Users']=='Registered') $style = ""; else $style = "display:none;"; ?>
							<span id="Registered" style=" <?php echo $style?>">
							<?php 
                $sql = "select * from member_product where member_id in (select id from member) group by member_id";
                $mem = $objDB->select($sql);	
								for($i=0;$i<count($mem);$i++)
									$id1[] = $mem[$i]['member_id'];
								$ids = @implode(",",$id1);					
              ?>
							<select name="Register" class="InputBox">
								<option value="<?php echo $ids?>">All</option>
								<?php 
                for($i=0;$i<count($mem);$i++){
                  if($mem[$i]['member_id']==$_REQUEST['Register'])
                    $select = "selected";
                  else
                    $select = "";
                  echo "<option value='".$mem[$i]['member_id']."' ".$select.">".stripslashes(FetchValue1("member","firstname,lastname","id",$mem[$i]['member_id']))."</option>";
                }
                ?>
							</select>
							</span>
							<input type="submit" value="Search" class="Btn" />
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<table class="content" width="100%" cellpadding="5" cellspacing="1">
				<tr class="SmallBlackHeading">
					<td align="left"><b>Member Name</b></td>
					<td align="left"><b>Place</b></td>
					<td align="left"><b>Total Product</b></td>
					<td align="left"><b>Total Quantity</b></td>
					<td align="left"><b>Total Amount</b></td>
				</tr>
				<?php $total = 0;		
								
								for($i=0;$i<count($prod);$i++){
									$total += $prod[$i]['price'];
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
							?>
				<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
					<td align="left"> <a href="index.php?p=member_payment&id=<?php echo $prod[$i]['member_id']?>&pg_no=<?php echo $_REQUEST['pg_no']?>" class="LinkGray">
						<?php $mem = FetchData("member",array(),"where id = '".$prod[$i]['member_id']."'");
									if(count($mem)>0)
										echo stripslashes($mem[0]['firstname']." ".$mem[0]['lastname']);
									else
										echo "Unregistered Member (".$prod[$i]['member_id'].")";
						?>
						</a> </td>
					<td align="left">
						<?php if($prod[$i]['place_id']!='' && $prod[$i]['place_id']!='0') echo stripslashes(FetchValue("county","c_name","c_id",$prod[$i]['place_id']));else echo stripslashes(FetchValue("county","c_name","c_id",DEFAULT_PLACE));?>
					</td>
					<td align="left">
						<?php if(strtolower($prod[$i]['product'])=='tithes' || strtolower($prod[$i]['product'])=='offering'){?>
						<?php echo $prod[$i]['product']?>
						<?php }else{ ?>
						<?php echo stripslashes(FetchValue("cd","title","id",$prod[$i]['product_id']));?>
						<?php } ?>
						<?php echo $prod[$i]['product']?></td>
					<td align="left">
						<?php if($prod[$i]['qty']!='' && $prod[$i]['qty']!='0') echo $prod[$i]['qty']; else echo "1";?>
					</td>
					<td align="left"><?php echo htmlentities(CURRENCY_SIGN).number_format($prod[$i]['price'],2);?></td>
				</tr>
				<?php } ?>
				<tr height="25">
					<td colspan="4" align="right" class="TopBorder"><b>Sub Total: </b></td>
					<td align="left"><b><?php echo htmlentities(CURRENCY_SIGN).number_format($total,2);?></b></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr height="25">
		<td colspan="2" align="right" class="TopBorder"><?php echo $objPaging->show_paging()?></td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
