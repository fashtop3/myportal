<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"donation"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);		
}else
	$MODE="ADD";


?>
<script>
  $(document).ready(function(){
  	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#Content').val(con); // put it in the textarea
	});
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required",
			Amount: "required",
			Content:"required"
		},
		messages: {
			Title: "Please enter title",
			Amount: "Please enter amount field description",
			Content:"Please enter description"
		}
	});	
  });
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "Content",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 500,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
           
		}

function dayvalue(id)
		{			
			$.post("place_chk.php",{ username:id } ,function(data)
			{				
				document.getElementById('DayName1').innerHTML = data;
			});
	}
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/shop.jpg" width="30" height="30" /> <?php echo $MODE;?> SHOP CATEGORY
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          
			<form  class="form-horizontal" method="post" action="manage_shop.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Type</label>
                  <div class="col-sm-5">
                    	<label><input type="radio" name="Type" value="General" class="InputBox" checked="checked" />
						General</label>
						<label><input type="radio" name="Type" value="Camp Meeting" <?php if($Info[0]['type']=='Camp Meeting') echo "checked='checked'";?> class="InputBox" />
						Camp Meeting </label></td>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Title</label>
                  <div class="col-sm-5">
                    	<input type="text" name="Title" id="Title" value="<?php echo stripslashes($Info[0]['title'])?>" class="form-control"/>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Amount Field Text</label>
                  <div class="col-sm-5">
                    	<textarea name="Amount" id="Amount" class="form-control" cols="50" rows="3" ><?php echo stripslashes($Info[0]['amount_text'])?></textarea>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputEmail" class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-5">
                    	<textarea name="Content" cols="65" rows="15" id="Content" style="width: 500px;" ><?php echo stripslashes($Info[0]['description'])?></textarea>
                  </div>
                </div>
											
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<div class="form-group">
                          <div class="col-sm-offset-2 col-sm-10">

                            <input value="<?php echo $MODE?>" class="btn btn-success" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=shop&pg_no=<?php echo $_REQUEST['pg_no']?>';">
                          </div>
                        </div>
								
			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
