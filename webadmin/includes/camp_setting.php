<?php
	$sql = "select * from camp_meeting where donation_id in (select id from donation where type='Camp Meeting' and status=1)";	
	
	if($_REQUEST['Search']!='')
		$sql .= " and title like '%".$_REQUEST['Search']."%'";		
	$sql .= " order by id desc";
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_camp_meeting.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_camp_meeting.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
</script>
<section class="content-header">
  <h1>
    <img src="images/meeting.gif" width="48" height="48" /> CAMP MEETING
    <!-- <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol> -->
  
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<?php showMessage();	?>
			<div class="form-group col-md-12">
				<form name="FrmSMS1" method="post" action="" id="FrmSMS1">
					<input type="hidden" name="ID" id="ID" value="">
							<input type="hidden" name="Process" id="Process" value="">
					<div class="col-md-9">
						<div class="col-md-3">Search by Title:
						</div>
						<div class="col-md-3"><input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="form-control" />
						</div>
						<div class="col-md-2"><input type="submit" value="Search" class="btn btn-default" />
						</div>
						</form>
					</div>
				</div>
				<div class="form-group col-md-12">
					<div class="col-md-9">
						<input type="Button" name="ADDNEW" value="ADD NEW" class="btn btn-default" onClick="window.location.href='index.php?p=meeting_addedit&pg_no=<?php echo $_REQUEST[pg_no]?>'"/>
						<?php
					$uk = FetchData("camp_user",array(),"where type='UK DELEGATES'");
					$cuk = count($uk);
					$uk = FetchData("camp_user",array(),"where type='OVERSEAS DELEGATES'");
					$ouk = count($uk);
					$uk = FetchData("camp_user",array(),"where type='Overseas Family'");
					$fuk = count($uk);
					?>
						<input type="Button" name="UK" value="UK DELEGATE     (<?php echo $cuk;?>)" class="btn btn-default" onClick="window.location.href='index.php?p=uk_camp'"/>
						<input type="Button" name="OVER" value="OVERSEAS DELEGATE     (<?php echo $ouk;?>)" class="btn btn-default" onClick="window.location.href='index.php?p=over_camp'"/>
						<input type="Button" name="Family" value="OVERSEAS FAMILY     (<?php echo $fuk;?>)" class="btn btn-default" onClick="window.location.href='index.php?p=family_camp'"/>
						<input type="Button" name="accomod" value="ACCOMODATION     (ALL USERS)" class="btn btn-default" onClick="window.location.href='index.php?p=accmodation'"/>
					</div>
					<div class="col-md-3" style="text-align: right;float: right;"><?php echo $objPaging->show_paging()?>
					</div>
				</div>
						
				<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
				
						<form name="FrmSMS" method="post" action="" id="FrmSMS">
							<input type="hidden" name="ID" id="ID" value="">
                            <input type="hidden" name="pg_no" id="pg_no" value="">
							<input type="hidden" name="Process" id="Process" value="">
							<table width="100%" cellspacing="0" cellpadding="5">
								<tr height="25" class="SmallBlackHeading">
									<th width="4%" align="center" class="BottomBorder">
										<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
									</th>
									<th width="5%" align="center" class="BottomBorder">#</th>
									<th width="" align="left" class="BottomBorder">Title</th>
									<th width="" align="left" class="BottomBorder">Price</th>
									<th width="" align="left" class="BottomBorder">Category</th>
									<th width="" align="left" class="BottomBorder">Type</th>
									<th align="center" class="BottomBorder">Options</th>
								</tr></thead>
<tbody>
								<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
								<tr>
									<td align="center">
										<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i]['id']?>">
									</td>
									<td align="center" class="Numbers"><?php echo $i+1?></td>
									<td align="left"><?php echo ucwords(stripslashes($cms[$i]['title'])); ?></td>
									<td align="left"><?php echo CURRENCY_SIGN.ucwords($cms[$i]['price']); ?></td>
									<td align="left"><?php echo ucwords(stripslashes($cms[$i]['category'])); ?></td>
									<td align="left"><?php echo stripslashes(GetFieldData("donation","title","where id = ".$cms[$i]['donation_id'])); ?></td>
									<td align="center" class="BlackMediumNormal"><img src="images/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=meeting_addedit&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="images/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" /></td>
								</tr>
								<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?></tbody>
	</table>
</div>

<div class="form-group col-md-12">
					<div class="col-md-6">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
					</div>
				</div>

				<div class="form-group col-md-12">
					<div class="col-md-6"><input type="submit" name="delete" value="DELETE" class="btn btn-default" onClick="return ValidateSelection(this.form);">
					</div>
					<div class="col-md-6" style="text-align: right;float: right;"><?php echo $objPaging->show_paging()?>
					</div>
				</div>

								
						</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
