<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"playlist"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$path = "../prayer/mp3/".stripslashes($Info[0]['file']);	
}else
	$MODE="ADD";
?>
<script>
  $(document).ready(function(){
  	
    // validate signup form on keyup and submit
	
	$("#frmAdmin").validate({
		rules: {			
			 <?php if($MODE=='ADD'){?>CImage: "required",<?php } ?>
			 Title: "required"
		},
		messages: {
			 <?php if($MODE=='ADD'){?>CImage: "Please enter song path",<?php } ?>
			 Title: "Please enter title"
		}
	});	
	
  });
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/song.jpg" width="30" height="30" /> &nbsp;&nbsp;<?php echo $MODE;?> SONG
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
			<?php showMessage(); ?>
			<form class="form-horizontal" method="post" action="manage_playlist.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<?php if($_SESSION['AdminID']==1){ 
					if($MODE=='ADD')
						$pl = DEFAULT_PLACE;
					else
						$pl = $Info[0]['place_id'];
				?>
				<div class="form-group">
					<label for="inputEmail" class="col-sm-2 control-label">Place</label>
					<div class="col-sm-5">
						<select name="Place" class="form-control" id="Place" style="width:300px;">
							<?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
						</select>
					</div>
				</div>
				<?php }else{ ?>				
				<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
				<?php } ?>
				<div class="form-group">
					<label for="inputEmail" class="col-sm-2 control-label">Title</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" name="Title" id="Title" value="<?php echo stripslashes($Info[0]['title'])?>" style="width:300px;" />
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-sm-2 control-label">Song</label>
					<div class="col-sm-5">
						<input name="CImage"  id="CImage" style="width: 300px;" type="file" />
						<input type="hidden" id="OldImage" name="OldImage" value="<?php echo stripslashes($Info[0]['file'])?>" />
					</div>
				</div>
				<input type="hidden" value="<?php echo $MODE?>" name="action" />
				<div class="form-group">
                	<div class="col-sm-offset-2 col-sm-10">
                		<input value="<?php echo $MODE?> SONG" class="btn btn-success" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="btn btn-default" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=playlist&pg_no=<?php echo $_REQUEST['pg_no']?>';">
                  	</div>
              	</div>
			</form>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
