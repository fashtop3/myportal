<?php
if($_REQUEST['action']=='ApproveStatus'){
	$sql = "UPDATE testimonial SET status = 'approved' where id = ".$_REQUEST['id'];
	$objDB->sql_query($sql);
	header("Location:index.php?p=testi_list");
}
	$sql = "select * from testimonial";
	if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!='')
			$sql.=' where place_id='.$_REQUEST['plc'];
		else
			$sql.=' where 1=1';	
	}
	else
		$sql.=' WHERE place_id='.$_SESSION['PlaceID'];
		
	if($_REQUEST['Keyword']!=''){
		$sql .= " and title like '%".$_REQUEST['Keyword']."%'";
	}
	
	if($_REQUEST['admins']!='')
		$sql .= ' and admin_createdby = '.$_REQUEST['admins'];
	
	if($_REQUEST['user']=='admin')
		$sql .= ' and createdby = 0 and admin_createdby = '.$_SESSION['AdminID'];
	//$sql.=" group by createdby,admin_createdby";
	$sql .= " order by status, id desc";
	
	//=================================================
	
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());
	if($cms)
		$_SESSION['FoundMsg'] = '';
	else
		$_SESSION['SuccessMsg'] = 'No Record Found';	
	
?>
<script type="text/javascript" language="javascript">

function confirmMail(frm)
{
		if(confirm("Do you really want to Mail?"))
		{		
			frm.Process.value='MULTIPLEDELETE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.QueryString.value='<?php echo $query_var?>';
			frm.action='manage_testi.php';	
			frm.submit();
		}	
}
function singleDel(ID,Process)
{
	if(confirm("Do you really want to delete?"))
	{		
		document.FrmSMS.ID.value = ID;
		document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
		document.FrmSMS.Process.value = Process;
		document.FrmSMS.action = 'manage_testi.php';
		document.FrmSMS.submit();
	}	
}

function ValidateSelection(frm)
{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('testi'+i).checked)
			{
				x=false;
				confirmMail(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
}

	$(document).ready(function(){   
		$(".tabbox").css("display","none");
		var selectedTab;
		$(".tab").click(function(){
			var elid = $(this).attr("id");
			$(".tab").removeClass("tabselected");
			$("#"+elid).addClass("tabselected");
			$(".tabbox").slideUp();
			if (elid != selectedTab) {
				selectedTab = elid;
				$("#"+elid+"box").slideDown();
			} else {
				selectedTab = null;
				$(".tab").removeClass("tabselected");
			}
			$("#tab").val(elid.substr(3));
		});
	});
	
</script>
<section class="content-header">
  <h1>
    <img src="<?php echo ADMIN_IMAGE_PATH;?>/testimonial.jpg" width="30" height="30" />&nbsp;&nbsp; TESTIMONIAL MANAGER
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
        	<?php showMessage();	?>
			<form name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
					<div class="form-group col-md-12">
						<div class="col-md-12">
						<div class="col-md-9">
							<?php if($_SESSION['AdminID']==1){ ?><div class="col-md-3">
							<select name="PlaceValue" class="form-control" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=testi_list&plc='+this.value; else window.location='index.php?p=testi_list';">
								<option value="">All Places</option>
								<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
							</select>
						</div>
						<div class="col-md-3">
							<select  name="admins" class="form-control" id="admins" onchange="if(this.value!='') window.location='index.php?p=testi_list&admins='+this.value; else window.location='index.php?p=testi_list';">
								<option value="">--Select Administrator--</option>
								<?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
							</select>
						</div>
							<?php } ?>
							<div class="col-md-2">Search by title:</div>
							<div class="col-md-3"><input type="text" name="Keyword" id="Keyword" value="<?php echo $_REQUEST['Keyword']?>" class="form-control"/></div>
							<div class="col-md-1"><input type="submit" value="Search" class="btn btn-default" /></div>
						</div>
							<div class="col-md-3" style="text-align: right;float: right;"><span class="column_head" style="float:right; padding-top:2px;">TotalNo of Results:&nbsp;<?php echo count($cms)?></span> 
								</div>
						</div>
					</div>
					<div class="form-group col-md-12">
						<div class="col-md-6">
						
							<input type="Button" name="ADDNEW" value="ADD NEW" class="btn btn-default" onClick="window.location.href='index.php?p=testimonial_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
							<input type="Button" name="sermons" value="MY TESTIMONIALS" class="btn btn-default" onClick="window.location.href='index.php?p=testi_list&user=admin'"  style="width:140px;"/>
							<input type="button" value="SHOW ALL" class="btn btn-default" onclick="window.location='index.php?p=testi_list'" />
						</div>
						<div class="col-md-2">
							<?php echo $objPaging->show_paging()?>
						</div>
				</div>
					<div class="table-responsive col-md-12" >
                  		<table id="example2" class="table table-bordered table-striped">
                  			<thead>
	                  			<tr>
									<th width="4%" align="center" class="BottomBorder">&nbsp;</th>
									<th width="4%" align="center" class="BottomBorder">#</th>
									<th width="" align="left" class="BottomBorder">Title</th>
									<th width="" align="left" class="BottomBorder">Status</th>
									<?php if($_SESSION['AdminID']==1){ ?>
									<th width="20%" align="center" class="BottomBorder">Place</th>
									<?php } ?>
									<th align="center" width="15%" class="BottomBorder">Options</th>
								</tr>
							</thead>
							<tbody>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr>
						<td align="center">&nbsp;</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left">
							<?php 
				  	echo stripslashes($cms[$i]['title']);
						if($cms[$i]['createdby']!=0 && $cms[$i]['createdby']!=''){
					?>
							(
							<?php 
				  	if($cms[$i]['createdby']==0){
						echo stripslashes(FetchValue1('admin','FirstName,LastName','UserID',$cms[$i]['admin_createdby']));
						$user = 'admin';
						$uid = $cms[$i]['admin_createdby'];
					}
					/*else {
						echo stripslashes(FetchValue1('member','firstname,lastname','id',$cms[$i]['createdby']));
						$user = 'member';
						$uid = $cms[$i]['createdby'];
					}*/
					?>
							)
							<?php } ?>
						</td>
						<td align="left">
							<?php if($cms[$i]['status']=='pending'){ ?>
							<a href="index.php?p=testi_list&action=ApproveStatus&id=<?php echo $cms[$i]['id']?>&pg_no=<?php echo $_REQUEST['pg_no']?>" class="LinkGray"><?php echo ucwords($cms[$i]['status']); ?></a>
							<?php }else echo ucwords($cms[$i]['status']);  ?>
						</td>
						<?php if($_SESSION['AdminID']==1){ ?>
						<td align="center"><?php echo stripslashes(FetchValue('county','c_name','c_id',$cms[$i]['place_id']));?> </td>
						<?php } ?>
						<td align="center" class="BlackMediumNormal"> <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=testimonial_detail&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>';" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=testimonial_addedit&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" /> </td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
			</tbody>
		</table>
		<div class="form-group col-md-12">
			<div class="col-md-6">
				<?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
			</div>
			<div class="col-md-6">
				<?php echo $objPaging->show_paging()?>
			</div>
		</div>
			</form>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
