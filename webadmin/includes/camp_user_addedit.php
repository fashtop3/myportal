<?php

	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"camp_user"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);

?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {			
			Email: {
				required: true,
				email: true
			},
			Name: "required",
			Phone: "required"		
		},
		messages: {			
			Email: {
				required: "Please enter email address",
				email: "Please enter valid email address"
			},
			Name: "Please enter name",
			Phone: "Please enter phone"
		}
	});
  });
</script>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF" align="center">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/meeting.gif" width="48" height="48" /></td>
		<td class="tbl_head" height="24">CAMP MEETING USER</td>
	</tr>
	<tr>
		<td width="" colspan="2">
			<?php
							showMessage();			
						?>
			<form method="post" id="frmAdmin" action="manage_camp_user.php" name="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="5%">Name</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Name" id="Name" value="<?php echo stripslashes($Info[0]['name'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Address</td>
											<td class="fieldarea">
												<textarea name="Address" class="TextArea" id="Address" style="width:300px; height:50px;"><?php echo stripslashes($Info[0]['address'])?></textarea>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Phone</td>
											<td class="fieldarea">
												<input name="Phone" class="InputBox" id="Phone" style="width:300px;" value="<?php echo stripslashes($Info[0]['phone'])?>" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Mobile</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Mobile" id="Mobile" value="<?php echo stripslashes($Info[0]['mobile'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Email</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Email" id="Email" value="<?php echo stripslashes($Info[0]['email'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Country</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Country" id="Country" value="<?php echo stripslashes($Info[0]['country'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Postcode</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Postcode" id="Postcode" value="<?php echo stripslashes($Info[0]['postcode'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%"></td>
											<td class="fieldarea">
												<input type="hidden" value="EDITUSER" name="action" />
												<input value="<?php echo $MODE?>" class="Btn" type="submit" name="submit1" id="RegisterSubmit">
												<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=accmodation';">
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>

