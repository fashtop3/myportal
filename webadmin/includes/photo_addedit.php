<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"photo_gallery"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$Photo = "<img src=../uploads/photo_gallery/big/".stripslashes($Info[0]['image'])." width=75 height=75>";	
}else
	$MODE="ADD";
?>
<script>
  $(document).ready(function(){
  	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#Content').val(con); // put it in the textarea
	});
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			GalleryId: "required"
			<?php if($MODE=='ADD'){?>
			,CImage: "required"
			<?php } ?>			
		},
		messages: {
			GalleryId: "Please select category"
			<?php if($MODE=='ADD'){?>
			,CImage: "Please enter image path"
			<?php } ?>
		}
	});	
  });
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "Content",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
/*            return false;			
			var fileBrowserWindow = new Array();
			fileBrowserWindow["file"] = ajaxfilemanagerurl;
			fileBrowserWindow["title"] = "Ajax File Manager";
			fileBrowserWindow["width"] = "782";
			fileBrowserWindow["height"] = "440";
			fileBrowserWindow["close_previous"] = "no";
			tinyMCE.openWindow(fileBrowserWindow, {
			  window : win,
			  input : field_name,
			  resizable : "yes",
			  inline : "yes",
			  editor_id : tinyMCE.getWindowArg("editor_id")
			});
			
			return false;*/
		}
	</script>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addphoto.jpg" /></td>
		<td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> PHOTO GALLERY</td>
	</tr>
	<tr>
		<td width="">&nbsp;</td>
		<td width="">
			<form method="post" action="manage_photo.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="5%">
												<label for="ContentHeading">Title</label>
											</td>
											<td class="fieldarea">
												<input type="text" name="Title" id="Title" value="<?php echo stripslashes($Info[0]['title'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel">Image</td>
											<td class="fieldarea" valign="top">
												<input name="CImage" id="CImage" class="input" style="width: 300px;" type="file">
												<input type="hidden" id="OldImage" name="OldImage" value="<?php echo stripslashes($Info[0]['image'])?>" >
											</td>
										</tr>
										<tr>
											<td class="fieldlabel"></td>
											<td class="fieldarea" valign="top"> <?php echo $Photo;?> </td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">
												<label for="Place">Category</label>
											</td>
											<td class="fieldarea">
												<select name="GalleryId" id="GalleryId" style="width:300px;">
													<option value="">--Select Category--</option>
													<?php
								echo FillCombo1('photo_gallery_category','title','id',explode(',',$Info[0]['gallery_id']),'');
							?>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">
												<label for="ContentHeading">Photo Date</label>
											</td>
											<td class="fieldarea">
												<input type="text" readonly="readonly" onfocus="scwShow(this, this);" onclick="scwShow(this, this);" name="Date" id="Date" value="<?php echo $Info[0]['photo_date']?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Description</td>
											<td class="fieldarea" valign="top">
												<textarea name="Content" cols="45" rows="10" id="Content"><?php echo stripslashes($Info[0]['description'])?></textarea>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td> <br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<br>
								<div align="center">
									<input value="<?php echo $MODE?> PHOTO" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=photo_list';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
