<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"camp_user"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<?php $rm = $Info[0]['room'];
$rms = @explode(',',$rm);
?>
<section class="content-header">
  <h1>
    <!-- Page Header
    <small>Optional description</small> -->
  </h1>
<!--   <ol class="breadcrumb">
    <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
    <li class="active">Here</li>
  </ol>
   -->
</section>
<section class="content">
  <div class="row">
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header"></div>
        <div class="box-body">
          <table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/meeting.gif" width="48" height="48" /> </td>
		<td class="tbl_head" height="24">OVERSEAS DELEGATES DETAIL - <?php echo $Info[0]['description']?></td>
	</tr>
	<tr>
		<td colspan="2">
			<?php if($_SESSION['Msg']!=''){ echo '<div id="divmsg" class="notice" style="color:#FF0000;">'.$_SESSION['Msg'].'</div>'; unset($_SESSION['Msg']);}?>
			<table width="100%" class="form" cellpadding="5" cellspacing="5">
				<tr>
					<td colspan="2" style="padding-left:5px;font-size:16px"><b>ID : <?php echo $Info[0]['id']?> </b> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;font-size:16px"><b>Booking Reference Number : <?php echo stripslashes($Info[0]['booking_ref_no'])?> </b> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Name : </b><?php echo stripslashes($Info[0]['name'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Email :</b> <?php echo stripslashes($Info[0]['email'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Phone :</b> <?php echo stripslashes($Info[0]['phone'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Mobile :</b> <?php echo stripslashes($Info[0]['mobile'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Address :</b> <?php echo stripslashes($Info[0]['address'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Country :</b> <?php echo stripslashes($Info[0]['country'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Postcode :</b> <?php echo stripslashes($Info[0]['postcode'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Date :</b> <?php echo $Info[0]['udate']?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;border:1px solid #CCCCCC;"><b>Payment Detail :</b> <br />
						<hr />
						<?php
					$payment = FetchData("camp_payment",array(),"where user_id='".$_REQUEST['id']."'");
					?>
						<table class="content" width="100%" cellpadding="5" cellspacing="0" border="0">
							<tr>
								<td align="left" valign="top" width="20%"><b>Payment Type:</b></td>
								<td align="left" valign="top"><?php echo stripslashes($payment[0]['payment_type']);?></td>
							</tr>
							<?php if($payment[0]['payment_type']=='Paypal'){ ?>
							<tr>
								<td align="left" valign="top" width="20%"><b>Payment Mode:</b></td>
								<td align="left" valign="top"><?php echo stripslashes($payment[0]['payment_mode']);?></td>
							</tr>
							<?php } ?>
							<tr>
								<td align="left" valign="top" width="20%"><b>Total Payment:</b></td>
								<td align="left" valign="top"><?php echo CURRENCY_SIGN.number_format($payment[0]['total_price'],2);?></td>
							</tr>
							<tr>
								<td align="left" valign="top" width="20%"><b>Pending Payment:</b></td>
								<td align="left" valign="top"><?php echo CURRENCY_SIGN.number_format($payment[0]['pending_payment'],2);?></td>
							</tr>
							<tr>
								<td align="left" valign="top" width="20%"><b>Submitted Payment:</b></td>
								<td align="left" valign="top"><?php echo CURRENCY_SIGN.number_format($payment[0]['payment_submit'],2);?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;border:1px solid #CCCCCC;"><b>List of persons who are coming :</b> <br />
						<?php
						$uid = $_REQUEST['id'];
						$user = FetchData("camp_user",array(),"where id=".$uid);
						$person = '<table class="content" width="100%" cellpadding="5" cellspacing="0" border="0">';
						$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
						$k=0;
						for($i=0;$i<count($age);$i++){
							$person .= '<tr>';
							$person .= '<td colspan="7" align="left" valign="top"><hr><b>';
							$person .= stripslashes(GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'"));
							$person .= '</b><hr></td>';
							$person .= '</tr>';
							$person .= '<tr>';
							$person .= '<td width="5%" align="left" valign="top"><b>Room No</b></td>';
							$person .= '<td align="left" valign="top"><b>Name</b></td>';
							$person .= '<td align="left" valign="top"><b>Country</b></td>';
							$person .= '<td align="left" valign="top"><b>Require Visa?</b></td>';
							$person .= '<td align="left" valign="top"><b>Coming Date</b></td>';
							$person .= '<td align="left" valign="top"><b>Passport No.</b></td>';
							$person .= '<td align="left" valign="top"><b>Price</b></td>';
							$person .= '</tr>';
							$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
							for($j=0;$j<count($per);$j++){
								$person .= '<tr>';
								$person .= '<td align="left" valign="top"><b>'.$rms[$k].'</b></td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['name']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['station']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['visa']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['coming_date']).'</td>';
								$person .= '<td align="left" valign="top">'.stripslashes($per[$j]['passport_no']).'</td>';
								$person .= '<td align="left" valign="top">&pound;';
								if($age[$i]['price']>0)
									$person .= number_format($age[$i]['price']/$age[$i]['person'],2);
								else
									$person .= number_format($age[$i]['price'],2);
								$person .= '</td>';
								$person .= '</tr>';
								$k++;
							}
						}
						$person .= "</table>";
						echo $person;
					?>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<center>
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=over_camp&pg_no=<?php echo $_REQUEST['pg_no']?>';">
							<input value="REPLY" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=over_camp_reply&id=<?php echo $_REQUEST['id']?>&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						</center>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section>
