<?php
	require_once("../utils/dbclass.php");
	require_once("../utils/functions.php");	
	require_once("../utils/config.php");
	$objDB = new MySQLCN;

	$Tbl = "sms";


//========================  ADD FUNCTION  ========================
if(isset($_REQUEST["Process"]) && $_REQUEST["Process"] == 'ADD')
{
	$sql = "select * from sms where sms_mobile_no='".addslashes($_REQUEST['MobileNo'])."'";
		$cu = $objDB->select($sql);
		if($cu[0]['sms_id']!=''){
			$_SESSION['ErrorMsg'] = 'Subscriber Already Exists!';
			header("Location: index.php?p=sms_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
	$SQL = "INSERT sms SET ";
	$SQL .= "sms_mobile_no = '".addslashes($_REQUEST["MobileNo"])."',";
	$SQL .= 'place_id='.addslashes($_REQUEST['Place']).',';	
	$SQL .= "sms_name = '".addslashes($_REQUEST["Name"])."',";	
	$SQL .= "created='".date('Y-m-d H:i:s')."',";
	$SQL .= "createdby='".$_SESSION['AdminID']."'";
	
	//echo $SQL;exit;
	$objDB->sql_query($SQL);
	
	$_SESSION['SuccessMsg'] = "Subscriber Saved Successfully!";
	header("location: index.php?p=sms_list&pg_no=".$_REQUEST['pg_no']);
	exit();
}

//========================  UPDATE FUNCTION  ========================
if(isset($_REQUEST["Process"]) && $_REQUEST["Process"] == 'UPDATE')
{
	$sql = "select * from sms where sms_mobile_no='".addslashes($_REQUEST['MobileNo'])."' and sms_id !=".$_REQUEST["ID"];
		$cu = $objDB->select($sql);
		if($cu[0]['sms_id']!=''){
			$_SESSION['ErrorMsg'] = 'Subscriber Already Exists!';
			header("Location: index.php?p=sms_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
	$SQL = "UPDATE sms SET ";
	$SQL .= "sms_mobile_no = '".addslashes($_REQUEST["MobileNo"])."',";
	$SQL .= "sms_name = '".addslashes($_REQUEST["Name"])."',";
	$SQL .= 'place_id='.addslashes($_REQUEST['Place']).',';	
	$SQL .= "modified='".date('Y-m-d H:i:s')."',";
	$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
	$SQL .= " WHERE sms_id = ".$_REQUEST["ID"];	
	
	//echo $SQL;exit;
	$objDB->sql_query($SQL);
	
	$_SESSION['SuccessMsg'] = "Subscriber Updated Successfully!";
	header("location: index.php?p=sms_list&pg_no=".$_REQUEST['pg_no']);
	exit();
}


//==================================  SINGLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
		DeleteData($Tbl,"sms_id","=",$_REQUEST['ID']);
		$_SESSION['SuccessMsg'] = "Subscriber Deleted Successfully";
		header("Location: index.php?p=sms_list&pg_no=".$_REQUEST['pg_no']);
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
		//print_r($_REQUEST['del']);exit;
		for($i=0;$i<count($_REQUEST['del']);$i++)
		{
			DeleteData($Tbl,'sms_id','=',$_REQUEST['del'][$i]);
		}
		$_SESSION['SuccessMsg'] = "Subscribers Deleted Successfully";
		header("Location: index.php?p=sms_list");
}
header("location: index.php?p=sms_list&pg_no=".$_REQUEST['pg_no']);
exit();
?>
