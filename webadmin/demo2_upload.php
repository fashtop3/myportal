<?php 
require_once "phpuploader/include_phpuploader.php";

require_once("../utils/config.php");
require_once("../utils/functions.php");
require_once("../utils/dbClass.php");

$objDB = new MySQLCN;


$uploader=new PhpUploader();

$mvcfile=$uploader->GetValidatingFile();

if($mvcfile->FileName=="accord.bmp")
{
	$uploader->WriteValidationError("My custom error : Invalid file name. ");
	exit(200);
}

//USER CODE:

$filename = $_REQUEST['GalleryId'].'_'.date("ymdhis")."_".$mvcfile->FileName;
$path = "../uploads/news/big/";

$targetfilepath= $path.$filename;
if( is_file ($targetfilepath) ){
	unlink($targetfilepath);
}
$mvcfile->MoveTo( $targetfilepath );

$SQL = "INSERT news_gallery_images SET ";		
$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
$SQL .= "image='".$filename."',";
$SQL .= "news_id='".$_REQUEST['NewsId']."',";
$SQL .= "gallery_id='".$_REQUEST['GalleryId']."',";
$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
$SQL .= "created='".date('Y-m-d H:i:s')."',";
$SQL .= 'place_id='.$_REQUEST['Place'].',';	
$SQL .= "admin_createdby='".$_SESSION['AdminID']."',";
$SQL .= "createdby='0'";

$objDB->sql_query($SQL);

$uploader->WriteValidationOK("");

?>