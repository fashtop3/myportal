<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$SQL = "UPDATE news_gallery_category SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "news_id='".$_REQUEST['NewsId']."',";
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "admin_modifiedby='".$_SESSION['AdminID']."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modifiedby='0'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Gallery Updated Successfully!';
		header("Location: index.php?p=news_gallery_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$SQL = "INSERT news_gallery_category SET ";		
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "news_id='".$_REQUEST['NewsId']."',";
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
//		$SQL .= 'place_id='.$_SESSION['PlaceID'].',';	
		$SQL .= "admin_createdby='".$_SESSION['AdminID']."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "createdby='0'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$sql = "select * from news_gallery_category order by id desc";
		$res = $objDB->select($sql);
		if($res)
			$id = $res[0]['id'];
		else
			$id=1;
		//$name = 'news_gallery_'.$id;
		//mkdir('../uploads/news/'.$name);
		//mkdir('../uploads/news/'.$name.'/big');
		//mkdir('../uploads/news/'.$name.'/small');
		$_SESSION['SuccessMsg'] = 'Gallery Added Successfully!';
		header("Location: index.php?p=news_gallery_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$sql = "select * from news_gallery_images where gallery_id = ".$_REQUEST['ID'];
	$cu = $objDB->select($sql);
	if(count($cu)>0){
		$_SESSION['ErrorMsg'] = 'You can not delete this Image Gallery. There are few images available in it.';
		header("Location: index.php?p=news_gallery_list&pg_no=".$_REQUEST['pg_no']);	
		exit;
	}
	//$name = 'news_gallery_'.$_REQUEST['ID'];	
	//rmdir('../uploads/news/'.$name.'/big');
	//rmdir('../uploads/news/'.$name.'/small');
	//rmdir('../uploads/news/'.$name);
	
	$SQL = "DELETE FROM news_gallery_category ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Gallery Deleted Successfully!';
	header("Location: index.php?p=news_gallery_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$sql = "select * from news_gallery_images where gallery_id = ".$_REQUEST['del'][$i];
		$cu = $objDB->select($sql);
		if(count($cu)>0){
			$_SESSION['ErrorMsg'] = 'You can not delete this Image Gallery. There are few images available in it.';
			header("Location: index.php?p=news_gallery_list&pg_no=".$_REQUEST['pg_no']);	
			exit;
		}
		//$name = 'news_gallery_'.$_REQUEST['del'][$i];		
		//rmdir('../uploads/news/'.$name.'/big');
		//rmdir('../uploads/news/'.$name.'/small');
		//rmdir('../uploads/news/'.$name);
		
		$SQL = "DELETE FROM news_gallery_category ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Gallery Deleted Successfully!';
	
	header("Location: index.php?p=news_gallery_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
