<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$sql = "select * from cd_category where title='".addslashes($_REQUEST['Title'])."' and id != ".$_REQUEST['id']." and type = '".$_REQUEST['Type']."'";
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Category Already Exists!';
			header("Location: index.php?p=music_category&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		
		$SQL = "UPDATE cd_category SET ";
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";
		$SQL .= 'type="'.addslashes($_REQUEST['Type']).'",';
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	  
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Category Updated Successfully!';
		header("Location: index.php?p=music_category&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$sql = "select * from cd_category where title='".addslashes($_REQUEST['Title'])."' and type = '".$_REQUEST['Type']."'";
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Category Already Exists!';
			header("Location: index.php?p=music_category&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		
		$SQL = "INSERT cd_category SET ";		
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";
		$SQL .= 'type="'.addslashes($_REQUEST['Type']).'",';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Category Added Successfully!';
		header("Location: index.php?p=music_category&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$SQL = "DELETE FROM cd_category ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Category Deleted Successfully!';
	header("Location: index.php?p=music_category&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{		
		$SQL = "DELETE FROM cd_category ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
	}
	$_SESSION['SuccessMsg'] = 'Category Deleted Successfully!';
	
	header("Location: index.php?p=music_category&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
