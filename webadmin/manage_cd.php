<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$sql = "select * from cd_category where title='".addslashes($_REQUEST['Title'])."' and id != ".$_REQUEST['id']." and type = '".$_REQUEST['Type']."' and category_id = ".$_REQUEST['Category']."";
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Music &amp; Track Already Exists!';
			header("Location: index.php?p=cd_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$total = FetchValue("cd","total","id",$_REQUEST['id']);
		$avil = FetchValue("cd","available","id",$_REQUEST['id']);
		if($total == $_REQUEST['Quantity'])
			$avail = $avil;
		else{
			$diff = $total - $_REQUEST['Quantity'];
			$avail = $avil - $diff;
		}
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{					
					$FileName = $_FILES['CImage']['name'];					
						$img_extension1=explode(".",$_FILES['CImage']['name']);					
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('cd_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					$path = "../uploads/contact";
					$image = new SimpleImage();
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);
					
					$SQL = "SELECT * FROM cd ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['image']!=''){
						unlink($path."/big/".stripslashes($img[0]['image']));
						unlink($path."/small/".stripslashes($img[0]['image']));
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=cd_new&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		$SQL = "UPDATE cd SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= 'type="'.addslashes($_REQUEST['Type']).'",';
		$SQL .= 'image="'.$CImage.'",';
		$SQL .= 'available="'.$avail.'",';
		$SQL .= 'price="'.addslashes($_REQUEST['Price']).'",';
		$SQL .= 'total="'.addslashes($_REQUEST['Quantity']).'",';
		$SQL .= 'category_id="'.addslashes($_REQUEST['Category']).'",';
		$SQL .= 'item_no="'.addslashes($_REQUEST['Item']).'",';
		$SQL .= 'author="'.addslashes($_REQUEST['Author']).'",';
		$SQL .= 'publisher="'.addslashes($_REQUEST['Publisher']).'",';		
		$SQL .= "description='".addslashes($_REQUEST['Description'])."',";
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	  
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Music &amp; Track Updated Successfully!';
		header("Location: index.php?p=cd_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$sql = "select * from cd_category where title='".addslashes($_REQUEST['Title'])."' and type = '".$_REQUEST['Type']."' and category_id = ".$_REQUEST['Category']."";
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Music &amp; Track Already Exists!';
			header("Location: index.php?p=cd_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$FileName = $_FILES['CImage']['name'];					
						$img_extension1=explode(".",$_FILES['CImage']['name']);					
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('cd_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					$path = "../uploads/contact";
					$image = new SimpleImage();
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);			
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Flag Image Not Uploaded";
				header("Location: index.php?p=cd_new&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage="";
		}
		$SQL = "INSERT cd SET ";		
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= 'type="'.addslashes($_REQUEST['Type']).'",';
		$SQL .= 'image="'.$CImage.'",';
		$SQL .= 'available="'.$avail.'",';
		$SQL .= 'price="'.addslashes($_REQUEST['Price']).'",';
		$SQL .= 'total="'.addslashes($_REQUEST['Quantity']).'",';
		$SQL .= 'category_id="'.addslashes($_REQUEST['Category']).'",';
		$SQL .= 'item_no="'.addslashes($_REQUEST['Item']).'",';
		$SQL .= 'author="'.addslashes($_REQUEST['Author']).'",';
		$SQL .= 'publisher="'.addslashes($_REQUEST['Publisher']).'",';		
		$SQL .= "description='".addslashes($_REQUEST['Description'])."',";
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";
//echo $SQL;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Music &amp; Track Added Successfully!';
		header("Location: index.php?p=cd_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$SQL = "SELECT * FROM cd ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);
	$path = '../uploads/contact';
	if($img[0]['image']!=''){
		unlink($path."/big/".stripslashes($img[0]['image']));
		unlink($path."/small/".stripslashes($img[0]['image']));
	}
	$SQL = "DELETE FROM cd ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Music &amp; Track Deleted Successfully!';
	header("Location: index.php?p=cd_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{		
		$SQL = "DELETE FROM cd ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
	}
	$_SESSION['SuccessMsg'] = 'Music &amp; Track Deleted Successfully!';
	
	header("Location: index.php?p=cd_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
