<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = str_replace('"',"'",$_FILES['CImage']['name']);
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('tract_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/tract/image";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);
					
					$SQL = "SELECT * FROM tract ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['image']!=''){
						unlink($path."/big/".$img[0]['image']);
						unlink($path."/small/".$img[0]['image']);
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		
		if(!empty($_FILES['CDown']['name']))
		{  
			if($_FILES['CDown']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CDown']['name']);			
					
					$image_type = strtoupper(FILE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "File can not be uploaded...!";
						header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CDown']['name'];					
										
					$upload_file = $_FILES['CDown']['tmp_name'];
					$CDown = addslashes('tract_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/tract/download/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CDown;					
					copy($_FILES['CDown']['tmp_name'], $path.$CDown);
					
					$SQL = "SELECT * FROM tract ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['file']!=''){
						unlink($path.$img[0]['file']);
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "File Not Uploaded";
				header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CDown = addslashes($_REQUEST['OldDown']);
		}
		
		if(!empty($_FILES['CVideo']['name']))
		{  
			if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CVideo']['name']);			
					
					$image_type = strtoupper(VIDEO_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Video can not be uploaded...!";
						header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CVideo']['name'];					
										
					$upload_file = $_FILES['CVideo']['tmp_name'];
					$CVideo = addslashes('tract_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/tract/video/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CVideo;					
					copy($_FILES['CVideo']['tmp_name'], $path.$CVideo);
					$SQL = "SELECT * FROM tract ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['video']!=''){
						unlink($path.$img[0]['video']);
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Video Not Uploaded";
				header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CVideo = addslashes($_REQUEST['OldVideo']);
		}
		
		$SQL = "UPDATE tract SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "image='".$CImage."',";
		$SQL .= "file='".$CDown."',";
		$SQL .= "video='".$CVideo."',";		
		$SQL .= "category_id='".$_REQUEST['CategoryId']."',";
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "admin_modifiedby='".$_SESSION['AdminID']."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modifiedby='0'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Tract Updated Successfully!';
		header("Location: index.php?p=tract_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('tract_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/tract/image";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);									
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = "";
		}
		
		if(!empty($_FILES['CDown']['name']))
		{  
			if($_FILES['CDown']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CDown']['name']);			
					
					$image_type = strtoupper(FILE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "File can not be uploaded...!";
						header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CDown']['name'];					
										
					$upload_file = $_FILES['CDown']['tmp_name'];
					$CDown = addslashes('tract_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/tract/download/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CDown;					
					copy($_FILES['CDown']['tmp_name'], $path.$CDown);															
			}
			else
			{
				$_SESSION['ErrorMsg'] = "File Not Uploaded";
				header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CDown = "";
		}
		
		if(!empty($_FILES['CVideo']['name']))
		{  
			if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CVideo']['name']);			
					
					$image_type = strtoupper(VIDEO_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Video can not be uploaded...!";
						header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CVideo']['name'];					
										
					$upload_file = $_FILES['CVideo']['tmp_name'];
					$CVideo = addslashes('tract_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/tract/video/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CVideo;					
					copy($_FILES['CVideo']['tmp_name'], $path.$CVideo);									
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Video Not Uploaded";
				header("Location: index.php?p=tract_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CVideo = "";
		}
		
		$SQL = "INSERT tract SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "image='".$CImage."',";
		$SQL .= "file='".$CDown."',";
		$SQL .= "video='".$CVideo."',";		
		$SQL .= "category_id='".$_REQUEST['CategoryId']."',";
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "admin_createdby='".$_SESSION['AdminID']."',";
		$SQL .= "createdby='0'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Tract Added Successfully!';
		header("Location: index.php?p=tract_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
//	unlink()
	$SQL = "SELECT * FROM tract ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);
	$path = '../uploads/tract';
	if($img[0]['image']!=''){
		unlink($path."/image/big/".$img[0]['image']);
		unlink($path."/image/small/".$img[0]['image']);
	}
	if($img[0]['file']!=''){
		unlink($path."/download/".$img[0]['file']);
	}
	if($img[0]['video']!=''){
		unlink($path."/video/".$img[0]['video']);
	}

	$SQL = "DELETE FROM tract ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Tract Deleted Successfully!';
	header("Location: index.php?p=tract_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "SELECT * FROM tract ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$img = $objDB->sql_query($SQL);
		$path = '../uploads/tract';
		if($img[0]['image']!=''){
			unlink($path."/image//big/".$img[0]['image']);
			unlink($path."/image//small/".$img[0]['image']);
		}
		if($img[0]['file']!=''){
		unlink($path."/download/".$img[0]['file']);
	}
	if($img[0]['video']!=''){
		unlink($path."/video/".$img[0]['video']);
	}
		$SQL = "DELETE FROM tract ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Tract Deleted Successfully!';
	
	header("Location: index.php?p=tract_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
