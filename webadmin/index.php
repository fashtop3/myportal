<?php
ob_start();
session_start();
require_once("../utils/config.php");
require_once("../utils/color_config.php");
require_once("../utils/functions.php");
require_once("../utils/dbclass.php");
require_once("../utils/paging.php");
$objDB = new MySQLCN;
/* Echoes from past */
$sql = "select * from sermon where type='echoes from past' order by id desc limit 1";
$trt = $objDB->select($sql);

if (count($trt) > 0) {
    $today = strtotime(date("Y-m-d"));
    $past = strtotime($trt[0]['display_date']);
    $diff = $today - $past;

    if ($diff > 30) {
        $flag = 0;
        if ($trt[0]['notify'] == '') {
            $sql = "UPDATE sermon SET notify='" . date('Y-m-d') . "' where id = " . $trt[0]['id'];
            $objDB->sql_query($sql);
            $flag = 0;
        } else {
            $date = @explode(', ', $trt[0]['notify']);
            $today = strtotime(date("Y-m-d"));
            $past = strtotime($date[count($date) - 1]);
            $diff = $today - $past;

            if ($diff >= 2) {
                $not = $trt[0]['notify'] . ", " . date('Y-m-d');
                $sql = "UPDATE sermon SET notify='" . $notify . "' where id = " . $trt[0]['id'];
                $objDB->sql_query($sql);
                $flag = 0;
            } else
                $flag = 1;
            if ($flag == 0) {
                $from = EMAIL_ADDRESS;
                $to = GetFieldData("admin", "Email", "where UserID=1");
                $cc = GetFieldData("admin", "Email", "where place_id=" . DEFAULT_PLACE);
                $subject = "Echoes From The Past content notification";
                $body = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
		<tr>
			<td>Dear user,</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Please add new content for  &quot;Echoes From The Past&quot;.</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><p>Thank &amp; Regards.</p>
			</td>
		</tr>
	</table>
	';
                $Headers = "From: <" . $from . ">\r\nCc: " . $cc . "\r\n" .
                    'X-Mailer: PHP/' . phpversion() . "\r\n" .
                    "MIME-Version: 1.0\r\n" .
                    "Content-Type: text/html; charset=utf-8\r\n" .
                    "Content-Transfer-Encoding: 8bit\r\n\r\n";
                $flag = @mail($to, $subject, $body, $Headers);

            }
        }
    }
} else {
    $from = EMAIL_ADDRESS;
    $to = GetFieldData("admin", "Email", "where UserID=1");
    $cc = GetFieldData("admin", "Email", "where place_id=" . DEFAULT_PLACE);
    $subject = "Echoes From The Past content notification";
    $body = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
		<tr>
			<td>Dear user,</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>There are no &quot;Echoes From The Past&quot;. <br>Please add content for  &quot;Echoes From The Past&quot;.</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><p>Thank &amp; Regards.</p>
			</td>
		</tr>
	</table>
	';
    $Headers = "From: <" . $from . ">\r\nCc: " . $cc . "\r\n" .
        'X-Mailer: PHP/' . phpversion() . "\r\n" .
        "MIME-Version: 1.0\r\n" .
        "Content-Type: text/html; charset=utf-8\r\n" .
        "Content-Transfer-Encoding: 8bit\r\n\r\n";
    $flag = @mail($to, $subject, $body, $Headers);
}
/* */

if (isset($_SESSION['AdminID']) && !empty($_SESSION['AdminID'])) {
    if (isset($_REQUEST['p']) && !empty($_REQUEST['p']))
        $page = $_REQUEST['p'] . ".php";
    else
        $page = "home.php";
} else {
    if (isset($_REQUEST['p']) && $_REQUEST['p'] == "forgot_password")
        $page = $_REQUEST['p'] . ".php";
    else
        $page = "login.php";
}
@$PagePrefix = split("_", $_REQUEST['p']);

?>
<html>
<head>
    <title><?php echo COMPANY_NAME ?>- Admin Panel</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <?php /* ?>
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<script language="javascript" type="text/javascript" src="js/adminmenu.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqueryui.js"></script>
<script type="text/javascript" src="js/jquery.metadata.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.validate.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<script language="javascript" src="js/calender.js"></script>
<?php */ ?>
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../images/favicon.ico">
    <link rel="apple-touch-icon-precomposed" href="../images/favicon.ico">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script type="text/javascript" src="js/jqueryui.js"></script>
    <script type="text/javascript" src="js/jquery.metadata.js"></script>
    <script language="javascript" type="text/javascript" src="js/jquery.validate.js"></script>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="assets/dist/css/AdminLTE.min.css">

    <link rel="stylesheet" href="assets/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="assets/dist/css/skins/_all-skins.min.css">
    <link rel="stylesheet" href="assets/plugins/morris/morris.css">
    <link rel="stylesheet" href="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <link rel="stylesheet" href="assets/plugins/datepicker/datepicker3.css">
    <link rel="stylesheet" href="assets/plugins/daterangepicker/daterangepicker-bs3.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <link rel="stylesheet" href="assets/plugins/datatables/dataTables.bootstrap.css">
    <script src="assets/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script src="assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script src="assets/plugins/knob/jquery.knob.js"></script>
    <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="assets/plugins/daterangepicker/daterangepicker.js"></script>
    <script src="assets/plugins/datepicker/bootstrap-datepicker.js"></script>
    <script src="assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <script src="assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <script src="assets/plugins/fastclick/fastclick.min.js"></script>
    <script src="assets/dist/js/app.min.js"></script>
    <script src="assets/dist/js/demo.js"></script>
    <script src="assets/plugins/iCheck/icheck.min.js"></script>
    <style type="text/css">
        th.BottomBorder {
            text-align: center;
        }
    </style>
    <?php if (!empty($_SESSION['ErrorMsg']) || !empty($_SESSION['SuccessMsg'])) { ?>
        <script type="text/javascript">
            $(document).ready(function () {
                setTimeout(function () {
                    $('#divmsg').hide('slow');
                }, 1000);
            });
        </script>
    <?php } ?>
</head>
<?php  include('includes/header.php'); ?>
<?php if (isset($_SESSION['AdminID'])) { ?>
    <footer class="main-footer">
        <div class="pull-left hidden-xs">
            <b>Admin</b>
        </div>
        <strong>Copyright &copy; <?php echo date('Y'); ?>
            <!-- <a href="<?php echo base_url(); ?>">Almsaeed Studio</a> -->.</strong> All rights reserved.
    </footer>

    <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->
<?php } ?>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
<script type="text/javascript">
    $.metadata.setType("attr", "validate");

    function nonWorkingDates(date) {
        var day = date.getDay(), Sunday = 0, Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6;
        var closedDays = [[Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday]];
        for (var i = 0; i < closedDays.length; i++) {
            if (day == closedDays[i][0]) {
                return [false];
            }

        }
        return [true];
    }
</script>
<script>
    function MM_showHideLayers() { //v9.0
        var i, p, v, obj, args = MM_showHideLayers.arguments;
        for (i = 0; i < (args.length - 2); i += 3)
            with (document) if (getElementById && ((obj = getElementById(args[i])) != null)) {
                v = args[i + 2];
                if (obj.style) {
                    obj = obj.style;
                    v = (v == 'show') ? 'visible' : (v == 'hide') ? 'hidden' : v;
                }
                obj.visibility = v;
            }
    }
    function MM_swapImgRestore() { //v3.0
        var i, x, a = document.MM_sr;
        for (i = 0; a && i < a.length && (x = a[i]) && x.oSrc; i++) x.src = x.oSrc;
    }
</script>

</body>
</html>

<?php
$objDB->close();
?>