<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$sql = "select * from contact_category where name='".addslashes($_REQUEST['Title'])."' and id != ".$_REQUEST['id']." and place_id = ".$_REQUEST['Place'];
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Contact Category Already Exists!';
			header("Location: index.php?p=contact_category_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=contact_category_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('contact_category_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/contact";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);
					
					$SQL = "SELECT * FROM contact_category ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['image']!=''){
						unlink($path."/big/".stripslashes($img[0]['image']));
						unlink($path."/small/".stripslashes($img[0]['image']));
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=contact_category_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		
		$SQL = "UPDATE contact_category SET ";
		$SQL .= "name='".addslashes($_REQUEST['Title'])."',";
		$SQL .= "image='".$CImage."',";
		$SQL .= "schedule_id='".@implode(',',addslashes($_REQUEST['ScheduleId']))."',";
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Contact Category Updated Successfully!';
		header("Location: index.php?p=contact_category_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$sql = "select * from contact_category where name='".addslashes($_REQUEST['Title'])."' and place_id = ".$_REQUEST['Place'];
		$cu = $objDB->select($sql);
		if($cu[0]['id']!=''){
			$_SESSION['ErrorMsg'] = 'Contact Category Already Exists!';
			header("Location: index.php?p=contact_category_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=contact_category_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];
					
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('contact_category_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/contact";
					$image = new SimpleImage();
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);				
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Flag Image Not Uploaded";
				header("Location: index.php?p=contact_category_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage="notavailable.jpg";
		}
		$SQL = "INSERT contact_category SET ";		
		$SQL .= "name='".addslashes($_REQUEST['Title'])."',";
		$SQL .= "image='".$CImage."',";
		$SQL .= "schedule_id='".@implode(',',addslashes($_REQUEST['ScheduleId']))."',";
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Contact Category Added Successfully!';
		header("Location: index.php?p=contact_category_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
//	unlink()
	$sql = "select * from contact where category_id = ".$_REQUEST['ID'];
	$cu = $objDB->select($sql);
	if(count($cu)>0){
		$_SESSION['ErrorMsg'] = 'You can not delete this Contact Category. It is already assigned to few Contacts.';
		header("Location: index.php?p=contact_category_list&pg_no=".$_REQUEST['pg_no']);	
		exit;
	}
	$SQL = "SELECT * FROM contact_category ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);
	$path = '../uploads/contact';
	if($img[0]['image']!=''){
		unlink($path."/big/".stripslashes($img[0]['image']));
		unlink($path."/small/".stripslashes($img[0]['image']));
	}
	$SQL = "DELETE FROM contact_category ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Contact Category Deleted Successfully!';
	header("Location: index.php?p=contact_category_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$sql = "select * from contact where category_id = ".$_REQUEST['del'][$i]." and id != 1";
		$cu = $objDB->select($sql);
		if(count($cu)>0){
			$_SESSION['ErrorMsg'] = 'You can not delete this Contact Category. It is already assigned to few Contacts.';
			header("Location: index.php?p=contact_category_list&pg_no=".$_REQUEST['pg_no']);	
			exit;
		}
		$SQL = "SELECT * FROM contact_category ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'"." and id != 1";
		$img = $objDB->sql_query($SQL);
		$path = '../uploads/contact/';
		if($img[0]['image']!=''){
			unlink($path."/big/".stripslashes($img[0]['image']));
			unlink($path."/small/".stripslashes($img[0]['image']));
		}
		$SQL = "DELETE FROM contact_category ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'"." and id != 1";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Contact Category Deleted Successfully!';
	
	header("Location: index.php?p=contact_category_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
