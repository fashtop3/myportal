<?php
	class paging{	
		var $RECORD_PER_PAGE;
		var $NUMBER_OF_RECORDS;
		var $CURRENT_PAGE;
		var $CURRENT_SEGMENT;
		var $SEGMENT_LENGTH;
		var $CURRENT_QUERY_STRING;
		var $RECORD_FROM;
		var $RECORD_TO;
		var $SQL;
		
		var $CLASS_SELECTED;
		var $CLASS_LINK;
		var $CLASS_NOLINK;
		
		function paging($SQL, $AnyField, $RecordPerPage=0, $SegmentLength=5){	
			global $objDB;	
			if(!empty($_REQUEST['pg_no'])){
				if(is_numeric($_REQUEST['pg_no'])){
					$this->CURRENT_PAGE = $_REQUEST['pg_no'];
				}else{
					$this->CURRENT_PAGE = 0;
				}
			}else{
				$this->CURRENT_PAGE = 0;
			}
			
			/*preg_match_all("/(select|SELECT)(.*?)(from|FROM)/i", $SQL, $found);
//			echo $SQL;
			$tmpSQL = str_replace($found[2]," COUNT(".$AnyField.") AS CNT ", $SQL);
//			echo $tmpSQL;
			$result = mysql_query($tmpSQL, $objDB->CONN) or die(mysql_error($objDB->CONN));
			$row = mysql_fetch_array($result);
			$this->NUMBER_OF_RECORDS = $row['CNT'];
			
			
			if($RecordPerPage!=0){
				$this->RECORD_PER_PAGE=$RecordPerPage;
				$this->SEGMENT_LENGTH=$SegmentLength;
			}else{
				if(defined(RECORD_PER_PAGE)){
					$this->RECORD_PER_PAGE=RECORD_PER_PAGE;
					$this->SEGMENT_LENGTH=SEGMENT_LENGTH;
				}else{
					$this->RECORD_PER_PAGE=10;
					$this->SEGMENT_LENGTH=5;
				}
			}*/
			
			$total = $objDB->select($SQL);
			$this->NUMBER_OF_RECORDS = count($total);
			if($RecordPerPage!=0){
				$this->RECORD_PER_PAGE=$RecordPerPage;
				$this->SEGMENT_LENGTH=$SegmentLength;
			}else{
				if(defined(RECORD_PER_PAGE)){
					$this->RECORD_PER_PAGE=RECORD_PER_PAGE;
					$this->SEGMENT_LENGTH=SEGMENT_LENGTH;
				}else{
					$this->RECORD_PER_PAGE=10;
					$this->SEGMENT_LENGTH=5;
				}
			}
			$this->SQL = $SQL." LIMIT ".($this->CURRENT_PAGE * $this->RECORD_PER_PAGE).", ".$this->RECORD_PER_PAGE;
			
			$this->RECORD_FROM = ($this->CURRENT_PAGE * $this->RECORD_PER_PAGE)+1;
			$this->RECORD_TO = ($this->RECORD_FROM + $this->RECORD_PER_PAGE - 1);
			$this->RECORD_TO = $this->RECORD_TO <= $this->NUMBER_OF_RECORDS ? $this->RECORD_TO : $this->NUMBER_OF_RECORDS;
			
			
		}
		
		function total_records(){
			return $this->NUMBER_OF_RECORDS;
		}
		
		function start_record(){
			if($this->NUMBER_OF_RECORDS==0)
				return 0;
			else
				return $this->RECORD_FROM<=0?0:$this->RECORD_FROM;
		}
		
		function end_record(){
			return $this->RECORD_TO<=0?0:$this->RECORD_TO;
		}
		
		function get_query(){
			return $this->SQL;
		}
		
		function get_serial_start(){
			return (($this->CURRENT_PAGE * $this->RECORD_PER_PAGE) + 1);
		}
		
		function set_paging_style($classname_link="", $classname_nolink="", $classname_selected=""){			
			$this->CLASS_LINK = $classname_link;
			$this->CLASS_NOLINK = $classname_nolink;
			if($classname_selected==""){
				$this->CLASS_SELECTED = $classname_nolink;
			}else{
				$this->CLASS_SELECTED = $classname_selected;
			}
		}
	
		function show_paging(){				
			$Prev_Link = "";
			$Next_Link = "";
			$Page_Link = "";
			$Query_String = "";
			$Post_Vars = "";
			
			$TotalPages = ceil($this->NUMBER_OF_RECORDS / $this->RECORD_PER_PAGE);
			
			$Current_Page = $this->getCurrentPageName();
			
			$Segments = ceil($TotalPages / $this->SEGMENT_LENGTH);
			$this->CURRENT_SEGMENT = ceil(($this->CURRENT_PAGE + 1) / $this->SEGMENT_LENGTH);								
						
			$FromCounter = (($this->CURRENT_SEGMENT-1)* $this->SEGMENT_LENGTH);
			
			$PageRemained = ($TotalPages - $FromCounter);
					
			if($PageRemained>=$this->SEGMENT_LENGTH){
				$ToCounter = $FromCounter + $this->SEGMENT_LENGTH;
			}else{
				$ToCounter = $FromCounter + $PageRemained;
			}			
			
			$ToCounter = $ToCounter<0?0:$ToCounter;
							
			$Query_String = $this->getCurrentQuerystring();
			$Query_String = $this->filterQueryString($Query_String);
			$Post_Vars = $Query_String!=""?"&".$this->getPostVars():$this->getPostVars();
			$Post_Vars = $Post_Vars=="&"?"":$Post_Vars;
			$Query_String = $Query_String.$Post_Vars;
			
			$Query_String = $Query_String!=""?"&".$Query_String:"";
			
			if($this->CURRENT_SEGMENT>=2){
				$Prev_Link = "<a href='".$Current_Page."?pg_no=".($FromCounter-1).$Query_String."' class='LinkGray1'>&lt;&lt;</a>";
			}else{
				//$Prev_Link = "<span class='".$this->CLASS_NOLINK."'>&laquo; Prev</span>";
			}
							
			for($i=$FromCounter; $i<$ToCounter; $i++){
				$Page_Link.="&nbsp;";
				if($i==$this->CURRENT_PAGE){
					$Page_Link.="<span class='LinkGray1'>".($i+1)."</span>";
					$this->CURRENT_QUERY_STRING = "pg_no=".$i."&".$Query_String;
				}else{
					$Page_Link.="<a href='".$Current_Page."?pg_no=".$i.$Query_String."' class='LinkRed1'>".($i+1)."</a>";
				}
			}
			
			$Next_Link.= "&nbsp;";

			if($this->CURRENT_SEGMENT<$Segments){
				$Next_Link.= "<a href='".$Current_Page."?pg_no=".$ToCounter.$Query_String."' class='LinkGray1'>&gt;&gt;</a>";
			}else{
				//$Next_Link.= "<span class='".$this->CLASS_NOLINK."'>Next &raquo;</span>";
			}
			
			echo $Prev_Link.$Page_Link.$Next_Link;							
		}
		
		function getCurrentPageName(){
			$tmpArr = explode("/",$_SERVER['PHP_SELF']);
			return $tmpArr[count($tmpArr)-1];
		}
		
		function getCurrentQuerystring(){
			if($_SERVER['QUERY_STRING']=="")
				return "";
			else
				return $_SERVER['QUERY_STRING'];
		}
		
		function filterQueryString($querystring){
			if($querystring==""){
				return "";
			}else{
				$tmpStr = "";
				$system_vars = array('pg_no', 'pg_seg');
				foreach($_GET as $key=>$val){
					if(!in_array($key, $system_vars)){
						$tmpStr.="&".$key."=".$val;
					}
				}
				if($tmpStr!=""){
					$tmpStr = substr($tmpStr,1);
				}
				return $tmpStr;
			}
		}
		
		function getPostVars(){
			$tmpStr = "";
			$system_vars = array('pg_no', 'pg_seg');
			foreach($_POST as $key=>$val){
				if(!in_array($key, $system_vars)){
					$tmpStr.="&".$key."=".$val;
				}
			}
			if($tmpStr!=""){
				$tmpStr = substr($tmpStr,1);
			}
			return $tmpStr;
		}
	}
?>