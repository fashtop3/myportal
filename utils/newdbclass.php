<?
class dbclass{
	/*============= DB Related Vars ================*/
	var $CONNECTION;
	var $HOST;
	var $DATABASE;
	var $USERNAME;
	var $PASSWORD;
	var $AFFECTEDROWS;
	var $SQL;

	var $RESULTS;
	/*==============================================*/
	
	/*============== Paging Related Vars ===========*/
	var $CURRENT_PAGE;
	var $CURRENT_QUERY_STRING;
	
	var $CLASS_SELECTED;
	var $CLASS_LINK;
	var $CLASS_NOLINK;
	/*==============================================*/

	function dbclass($NewConnection=false, $UseConfigConstant=true, $Host="", $Database="", $UserName="", $Password=""){			
		if($UseConfigConstant){
			$this->HOST = DB_HOST;
			$this->DATABASE = DB_DATABASE;
			$this->USERNAME = DB_USERNAME;
			$this->PASSWORD = DB_PASSWORD;
		}else{
			$this->HOST = $Host;
			$this->DATABASE = $Database;
			$this->USERNAME = $UserName;
			$this->PASSWORD = $Password;
		}
		
		$this->CONNECTION = mysql_connect($this->HOST, $this->USERNAME, $this->PASSWORD, $NewConnection) or die($this->error());
		if(!$this->CONNECTION){
			$this->error();
		}
		mysql_select_db($this->DATABASE, $this->CONNECTION) or die($this->error());
		$this->RECORD_SET = array();
		$this->RESULTS = array();			
	}
	
	function close(){
		if($this->CONNECTION){
			mysql_close($this->CONNECTION);
		}
	}
	
	function error(){
		$str = "<h2>MySQL returned error</h2><hr><br><br>";
		$str.= "<strong>Error Description :</strong><hr>Error No : ".mysql_errno($this->CONNECTION)."<br>Description : ".mysql_error($this->CONNECTION)."<br><br>";
		if($this->SQL!=""){
			$str.="<strong>SQL Query<strong><hr>".$this->SQL;
		}
		echo $str;
		$this->close();
		exit();
	}
	
	function setPagingStyle($classname_link="", $classname_nolink="", $classname_selected=""){			
		$this->CLASS_LINK = $classname_link;
		$this->CLASS_NOLINK = $classname_nolink;
		if($classname_selected==""){
			$this->CLASS_SELECTED = $classname_nolink;
		}else{
			$this->CLASS_SELECTED = $classname_selected;
		}
	}
	
	function setQuery($Query){
		$this->SQL = $Query;
	}
	
	function buildQuery(){
		
	}
	
	function getLastQuery($Query){
		return $this->SQL;
	}
	
	function getQuery($recordset_name){
		if(!array_key_exists($recordset_name, $this->RESULTS)){
			echo "<h2>Function Error (getQuery)</h2><hr><br>The provded <strong>RECORDSET</strong> name is invalid.";
			$this->close();
			exit;
		}else{
			return $this->RESULTS[$recordset_name]['query'];
		}
	}
	
	function getRecordsetDetail($recordset_name){
		if(!array_key_exists($recordset_name, $this->RESULTS)){
			echo "<h2>Function Error (getRecordsetDetail)</h2><hr><br>The provded <strong>RECORDSET</strong> name is invalid.";
			$this->close();
			exit;
		}else{
			return $this->RESULTS[$recordset_name];
		}
	}
	
	function getRecordset($recordset_name){
		if(!array_key_exists($recordset_name, $this->RESULTS)){
			echo "<h2>Function Error (getRecordset)</h2><hr><br>The provded <strong>RECORDSET</strong> name is invalid.";
			$this->close();
			exit;
		}else{
			return $this->RESULTS[$recordset_name]['records'];
		}
	}
	
	function getAffectedRows(){
		return $this->AFFECTEDROWS;
	}
	
	function remove($RecordSetName){
		if($RecordSetName!=""){
			if(isset($this->RESULTS[$RecordSetName])){
				unset($this->RESULTS[$RecordSetName]);
			}
		}
	}
	
	function getTotalRecords($recordset_name){
		if(!array_key_exists($recordset_name, $this->RESULTS)){
			echo "<h2>Function Error (getTotalRecords)</h2><hr><br>The provded <strong>RECORDSET</strong> name is invalid.";
			$this->close();
			exit;
		}else{
			return $this->RESULTS[$recordset_name]['total_records'];
		}
	}
	
	function getStartRecord($recordset_name){
		if(!array_key_exists($recordset_name, $this->RESULTS)){
			echo "<h2>Function Error (getStartRecord)</h2><hr><br>The provded <strong>RECORDSET</strong> name is invalid.";
			$this->close();
			exit;
		}else{
			return $this->RESULTS[$recordset_name]['record_from'];
		}
	}
	
	function getEndRecord($recordset_name){
		if(!array_key_exists($recordset_name, $this->RESULTS)){
			echo "<h2>Function Error (getEndRecord)</h2><hr><br>The provded <strong>RECORDSET</strong> name is invalid.";
			$this->close();
			exit;
		}else{
			return $this->RESULTS[$recordset_name]['record_to'];
		}
	}
	
	function getSerialStart($recordset_name){
		if(!array_key_exists($recordset_name, $this->RESULTS)){
			echo "<h2>Function Error (getSerialStart)</h2><hr><br>The provded <strong>RECORDSET</strong> name is invalid.";
			$this->close();
			exit;
		}else{
			return (($this->CURRENT_PAGE * $this->RESULTS[$recordset_name]['record_per_page']) + 1);
		}
	}
	
	function execute($recordset_name="", $auto_paging=false, $field_to_count="", $record_per_page=10, $segment_length=5){
		if(eregi("^SELECT", $this->SQL) && $recordset_name==""){
			echo "<h2>Function Error (execute)</h2><hr><br>For <strong>SELECT Query</strong> you must provide the name for the <strong>RECORDSET</strong>.";
			$this->close();
			exit;
		}
		if(eregi("^SELECT", $this->SQL) && $auto_paging===true){
			if($field_to_count==""){
				echo "<h2>Function Error (execute)</h2><hr><br>If you set <strong>auto_paging = true</strong> then you must set the value of <strong>filed_to_count</strong>.";
				$this->close();
				exit;
			}else{
				if(!empty($_REQUEST['pg_no'])){
				if(is_numeric($_REQUEST['pg_no'])){
					$this->CURRENT_PAGE = $_REQUEST['pg_no'];
				}else{
					$this->CURRENT_PAGE = 0;
				}
				}else{
					$this->CURRENT_PAGE = 0;
				}				
				
				if($record_per_page!=0){
					$RECORD_PER_PAGE=$record_per_page;
					$SEGMENT_LENGTH=$segment_length;
				}else{
					if(defined(RECORD_PER_PAGE)){
						$RECORD_PER_PAGE=RECORD_PER_PAGE;
						$SEGMENT_LENGTH=SEGMENT_LENGTH;
					}else{
						$RECORD_PER_PAGE=10;
						$SEGMENT_LENGTH=5;
					}
				}	
				preg_match_all("/(select)(.*?)(from)/i", $this->SQL, $found);
				$tmpSQL = str_replace($found[2]," COUNT(".$field_to_count.") AS CNT ", $this->SQL);
				$result = mysql_query($tmpSQL, $this->CONNECTION) or die($this->error());
				$row = mysql_fetch_array($result);
				$total_records = $row['CNT'];		
				$total_pages = ceil($total_records / $record_per_page);
				
				$fields = array();
				while($field = mysql_fetch_field($result)){
					array_push($fields, $field->name);
				}
				if($total_records>0){					
					$this->SQL = $this->SQL." LIMIT ".($this->CURRENT_PAGE * $record_per_page).", ".$record_per_page;
					$result = mysql_query($this->SQL, $this->CONNECTION) or die($this->error());
					$record_set = array();						
					while($row=mysql_fetch_array($result)){
						array_push($record_set, $row);
					}
										
					$this->RESULTS[$recordset_name]['paging'] = true;
					$this->RESULTS[$recordset_name]['query'] = $this->SQL;
					$this->RESULTS[$recordset_name]['record_from'] = ($this->CURRENT_PAGE * $record_per_page)+1;
					$this->RESULTS[$recordset_name]['record_to'] = ($this->CURRENT_PAGE * $record_per_page)+1+count($record_set);
					$this->RESULTS[$recordset_name]['total_pages'] = $total_pages;
					$this->RESULTS[$recordset_name]['record_per_page'] = $RECORD_PER_PAGE;
					$this->RESULTS[$recordset_name]['segment_length'] = $SEGMENT_LENGTH;
					$this->RESULTS[$recordset_name]['current_segment'] = ceil(($this->CURRENT_PAGE + 1) / $SEGMENT_LENGTH);
					$this->RESULTS[$recordset_name]['fields'] = $fields;
					$this->RESULTS[$recordset_name]['total_records'] = $total_records;
					$this->RESULTS[$recordset_name]['records'] = $record_set;
				}else{
					$this->RESULTS[$recordset_name]['paging'] = true;
					$this->RESULTS[$recordset_name]['query'] = $this->SQL;
					$this->RESULTS[$recordset_name]['record_from'] = 0;
					$this->RESULTS[$recordset_name]['record_to'] = 0;
					$this->RESULTS[$recordset_name]['total_pages'] = 0;
					$this->RESULTS[$recordset_name]['current_segment'] = 0;
					$this->RESULTS[$recordset_name]['record_per_page'] = $RECORD_PER_PAGE;
					$this->RESULTS[$recordset_name]['segment_length'] = $SEGMENT_LENGTH;
					$this->RESULTS[$recordset_name]['current_segment'] = ceil(($this->CURRENT_PAGE + 1) / $SEGMENT_LENGTH);
					$this->RESULTS[$recordset_name]['fields'] = $fields;
					$this->RESULTS[$recordset_name]['total_records'] = 0;
					$this->RESULTS[$recordset_name]['records'] = array();
				}
				return $record_set;
			}
		}else if(eregi("^SELECT", $this->SQL) && $auto_paging===false){
			$result = mysql_query($this->SQL, $this->CONNECTION) or die($this->error());
			$fields = array();
			$record_set = array();
			while($field = mysql_fetch_field($result)){
				array_push($fields, $field->name);
			}
			while($row=mysql_fetch_array($result)){
				array_push($record_set, $row);
			}
			$this->RESULTS[$recordset_name]['paging'] = false;
			$this->RESULTS[$recordset_name]['query'] = $this->SQL;
			$this->RESULTS[$recordset_name]['record_from'] = 1;
			$this->RESULTS[$recordset_name]['record_to'] = count($record_set);
			$this->RESULTS[$recordset_name]['current_segment'] = 0;
			$this->RESULTS[$recordset_name]['segment_length'] = 0;
			$this->RESULTS[$recordset_name]['total_pages'] = 0;
			$this->RESULTS[$recordset_name]['record_per_page'] = 0;
			$this->RESULTS[$recordset_name]['fields'] = $fields;
			$this->RESULTS[$recordset_name]['total_records'] = count($record_set);
			$this->RESULTS[$recordset_name]['records'] = $record_set;
			return $record_set;
		}
		else{
			$result = mysql_query($this->SQL, $this->CONNECTION) or die($this->error());
			if($result===false){
				$this->error();
			}else if($result===true){
				$this->AFFECTEDROWS = mysql_affected_rows($this->CONNECTION);
				return true;
			}else{
				$this->AFFECTEDROWS = mysql_affected_rows($this->CONNECTION);
				if(eregi("^INSERT", $this->SQL)){
					return mysql_insert_id($this->CONNECTION);
				}else{
					return $result;
				}
			}
		}
	}
	
	function show_paging($recordset_name=""){			
		if($recordset_name==""){
			echo "<h2>Function Error (show_paging)</h2><hr><br>You must provide the name of the <strong>RECORDSET</strong> for which the paging will be shown.";
			$this->close();
			exit;
		}else if(!array_key_exists($recordset_name, $this->RESULTS)){
			echo "<h2>Function Error (show_paging)</h2><hr><br>The provided <strong>RECORDSET</strong> name is invalid.";
			$this->close();
			exit;
		}else if($this->RESULTS[$recordset_name]['paging']==false){
			echo "<h2>Function Error (show_paging)</h2><hr><br>Paging is not supported on the the provded <strong>RECORDSET</strong>";
			$this->close();
			exit;
		}else{	
			$Prev_Link = "";
			$Next_Link = "";
			$Page_Link = "";
			$Query_String = "";
			$Post_Vars = "";
			
			$TotalPages = $this->RESULTS[$recordset_name]['total_pages'];
			
			$Current_Page = $this->getCurrentPageName();
			
			$Segments = ceil($TotalPages / $this->RESULTS[$recordset_name]['segment_length']);							
						
			$FromCounter = (($this->RESULTS[$recordset_name]['current_segment']-1)* $this->RESULTS[$recordset_name]['segment_length']);
			
			$PageRemained = ($TotalPages - $FromCounter);
					
			if($PageRemained>=$this->RESULTS[$recordset_name]['segment_length']){
				$ToCounter = $FromCounter + $this->RESULTS[$recordset_name]['segment_length'];
			}else{
				$ToCounter = $FromCounter + $PageRemained;
			}			
			
			$ToCounter = $ToCounter<0?0:$ToCounter;
							
			$Query_String = $this->getCurrentQuerystring();
			$Query_String = $this->filterQueryString($Query_String);
			$Post_Vars = $Query_String!=""?"&".$this->getPostVars():$this->getPostVars();
			$Post_Vars = $Post_Vars=="&"?"":$Post_Vars;
			$Query_String = $Query_String.$Post_Vars;
			
			$Query_String = $Query_String!=""?"&".$Query_String:"";
			
			if($this->RESULTS[$recordset_name]['current_segment']>=2){
				$Prev_Link = "<a href='".$Current_Page."?pg_no=".($FromCounter-1).$Query_String."' class='".$this->CLASS_LINK."'>&lt;&lt;Prev</a>";
			}else{
				$Prev_Link = "<span class='".$this->CLASS_NOLINK."'>&lt;&lt;Prev</span>";
			}
							
			for($i=$FromCounter; $i<$ToCounter; $i++){
				$Page_Link.="&nbsp;";
				if($i==$this->CURRENT_PAGE){
					$Page_Link.="<span class='".$this->CLASS_SELECTED."'>".($i+1)."</span>";
					$this->CURRENT_QUERY_STRING = "pg_no=".$i."&".$Query_String;
				}else{
					$Page_Link.="<a href='".$Current_Page."?pg_no=".$i.$Query_String."' class='".$this->CLASS_LINK."'>".($i+1)."</a>";
				}
			}
			
			$Next_Link.= "&nbsp;";

			if($this->RESULTS[$recordset_name]['current_segment']<$Segments){
				$Next_Link.= "<a href='".$Current_Page."?pg_no=".$ToCounter.$Query_String."' class='".$this->CLASS_LINK."'>Next&gt;&gt;</a>";
			}else{
				$Next_Link.= "<span class='".$this->CLASS_NOLINK."'>Next&gt;&gt;</span>";
			}
			
			echo $Prev_Link.$Page_Link.$Next_Link;		
		}					
	}
	
	function getCurrentPageName(){
		$tmpArr = explode("/",$_SERVER['PHP_SELF']);
		return $tmpArr[count($tmpArr)-1];
	}
	
	function getCurrentQuerystring(){
		if($_SERVER['QUERY_STRING']=="")
			return "";
		else
			return $_SERVER['QUERY_STRING'];
	}
	
	function filterQueryString($querystring){
		if($querystring==""){
			return "";
		}else{
			$tmpStr = "";
			$system_vars = array('pg_no', 'pg_seg');
			foreach($_GET as $key=>$val){
				if(!in_array($key, $system_vars)){
					$tmpStr.="&".$key."=".$val;
				}
			}
			if($tmpStr!=""){
				$tmpStr = substr($tmpStr,1);
			}
			return $tmpStr;
		}
	}
	
	function getPostVars(){
		$tmpStr = "";
		$system_vars = array('pg_no', 'pg_seg');
		foreach($_POST as $key=>$val){
			if(!in_array($key, $system_vars)){
				$tmpStr.="&".$key."=".$val;
			}
		}
		if($tmpStr!=""){
			$tmpStr = substr($tmpStr,1);
		}
		return $tmpStr;
	}
}
?>