<?php
	function ShowPageCounter($Current,$NoOfRecords)
	{
		if($NoOfRecords<=$_SESSION['RecordPerPage'])
			$Pages = 1;
		else
			$Pages = (($NoOfRecords%$_SESSION['RecordPerPage'])==0)?($NoOfRecords/$_SESSION['RecordPerPage']):(floor($NoOfRecords/$_SESSION['RecordPerPage']) + 1);

		$MiddleNos = 4; 	/* No of pages between Previous and Next */
		if($Current==1)
			echo("&lt;&lt;Prev&nbsp;");
		else
			echo("<a href=".GetPageName().($Current-1)." class='LinkRed'>&lt;&lt;Prev&nbsp;</a>");
		if($Pages<=3)
		{
			for($i=1;$i<=$Pages;$i++)
			{
				if($i==$Current)
					echo $i."&nbsp;";
				else
					echo "<a href=".GetPageName().$i." class='LinkRed'>".$i."&nbsp;</a>";
			}
			if($Current==$Pages)
				echo "Next&gt;&gt;";
			else
				echo "<a href=".GetPageName().($Current+1)." class='LinkRed'>Next&gt;&gt;</a>";

		}
		else
		{
			if($Pages>$Current+($MiddleNos-1))
			{
				for($i=$Current;$i<=($Current+($MiddleNos-1));$i++)
				{
					if($i==$Current)
						echo $i."&nbsp;";
					else
						echo "<a href=".GetPageName().$i." class='LinkRed'>".$i."&nbsp;</a>";
				}
				echo "<a href=".GetPageName().($Current+1)." class='LinkRed'>Next&gt;&gt;</a>";
			}
			else
			{
				for($i=$Pages-($MiddleNos-1);$i<=$Pages;$i++)
				{
					if($i==$Current)
						echo $i."&nbsp;";
					else
						echo "<a href=".GetPageName().$i." class='LinkRed'>".$i."&nbsp;</a>";
				}
				echo "Next&gt;&gt;";
			}
		}
		return;
	}

	function GetPageName()
	{
		return "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']."?".findPostFields().findQueryString()."CurrentPage=";
		//return $_SERVER['SCRIPT_FILENAME']."?".findQueryString()."CurrentPage=";

	}

	function findQueryString()
	{
		$queryString = $_SERVER['QUERY_STRING'];
		$x = strpos($queryString,"CurrentPage");
		if($x===false)
			$queryString = $queryString;
		elseif($x==0)
		{
			$queryString = "";
		}
		else
		{
			$queryString = substr($queryString,0,$x-1);
		}

		if($queryString=="")
		{
			return "";
		}
		else
		{
			return $queryString."&";
		}
	}

	function findPostFields()
	{
		$queryString = "";

		foreach($_POST as $var => $val)
		{
			if(trim($val)!="" && trim($val)!=9 && trim($val)!="Search")
				$queryString.=$var."=".$val."&";
		}
		if($queryString=="")
			return "";
		else
			return $queryString;
	}

?>