<?php
function loadVariable($Variable, $Default){
	if(isset($_GET[$Variable]))
	return $_GET[$Variable];
	elseif(isset($_POST[$Variable]))
	return $_POST[$Variable];
	else
	return $Default;
}

function loadSessionVariable($Variable, $Default){
	if(!empty($_SESSION[$Variable]))
	return $_SESSION[$Variable];
}


function showMessage(){
	if(!empty($_SESSION['ErrorMsg'])){
		echo '<div id="divmsg" class="alert alert-warning">'.$_SESSION['ErrorMsg'].'</div>';
		//echo "<table cellspacing=0 cellpadding=0 border=0><tr><td align='left' width='24' valign='top'><img src='".ERROR_ICON."'></td><td style='font-family: Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color:#FF0000; font-weight:normal'>".$_SESSION['ErrorMsg']."</td></tr></table>";
		unset($_SESSION['ErrorMsg']);
	}elseif(!empty($_SESSION['SuccessMsg'])){
		echo '<div id="divmsg" class="alert alert-success">'.$_SESSION['SuccessMsg'].'</div>';
		//		echo "<table cellspacing=0 cellpadding=0 border=0><tr><td align='left' width='24' valign='top'><img src='".SUCCESS_ICON."'></td><td style='font-family: Lucida Sans Unicode, Verdana, Arial, Helvetica, sans-serif; font-size: 11px; color:#006600; font-weight:normal'>".$_SESSION['SuccessMsg']."</td></tr></table>";
		unset($_SESSION['SuccessMsg']);
	}


}

function checkLogout(){
	if(!empty($_REQUEST['a']) && $_REQUEST['a']=="logout"){
		$x=strpos($_SERVER['PHP_SELF'],"webadmin");
		if($x===false)
		{
			$_SESSION[FE_SESSION_VAR]="";
			session_unregister(FE_SESSION_VAR);
				
			if(isset($_SESSION["LogCust"]))
			{
				$_SESSION["LogCust"]="";
				session_unregister("LogCust");
			}
			session_unset();
			session_destroy();
			session_regenerate_id(true);
			$_SESSION['SuccessMsg']="";
		}
		else
		{
			$_SESSION[ADMIN_SESSION_VAR]="";
			session_unregister(ADMIN_SESSION_VAR);
			session_unset();
			session_destroy();
			$_SESSION['SuccessMsg']="You have successfully logged out";
		}

	}
}

function SecurityCheck($p)
{
	$x=strpos($_SERVER['PHP_SELF'],"webadmin");
	if($x===false){
		$ArrSecuredPages = explode(",",FE_SECURED_PAGES);
	}else{
		$ArrSecuredPages = explode(",",ADMIN_SECURED_PAGES);
	}

	if(in_array($p,$ArrSecuredPages))
	{
		if($x===false){
			if((!empty($_SESSION[FE_SESSION_VAR])) || (!empty($_SESSION[FE_SESSION_RESELLER_VAR])) || (!empty($_SESSION[FE_SESSION_ADVERTISER_VAR])) ){
				if($p!="")
				$page = $p.".php";
				else
				$page = "home.php";
			}else{
				$page="login.php";
			}
		}else{
			if(!empty($_SESSION[ADMIN_SESSION_VAR])){
				if($p!="")
				$page = $p.".php";
				else
				$page = "home.php";
			}else{
				$page="login.php";
			}
		}
	}else{
		$page=$p.".php";
	}
	return $page;
}


function checkPermission()
{
	$LIVE = false;
	$SERVER_ROOT="";
	$FE_PERMITTED_PAGES="index.php";
	$ADMIN_PERMITTED_PAGES="index.php";

	$x=strpos($_SERVER['PHP_SELF'],"webadmin");

	if($x===false)
	{
		$ArrUnsecuredPages = explode(",",$FE_PERMITTED_PAGES);
	}
	else
	{
		$ArrUnsecuredPages = explode(",",$ADMIN_PERMITTED_PAGES);
	}

	$RequestedPage = $_SERVER['PHP_SELF'];
	$ArrTemp = explode("/",$RequestedPage );
	$pos=count($ArrTemp)-1;
	if(!in_array($ArrTemp[$pos],$ArrUnsecuredPages))
	{
		header("location:".SERVER_ROOT."/index.php");
	}

}


function escapeString($string){
	$gpc = ini_get("magic_quotes_gpc");
	$runtime = ini_get("magic_quotes_runtime");

	if($gpc=="1" || strtoupper($gpc)=="ON"){
		$gpc=1;
	}else{
		$gpc=0;
	}

	if($runtime=="1" || strtoupper($runtime)=="ON"){
		$runtime=1;
	}else{
		$runtime=0;
	}

	if($runtime==1){
		return $string;
	}else if($gpc==1){
		return $string;
	}else{
		return preg_replace("'/","\\'",$string);
	}
}
function get_state_city()
{
	global $objDB;
	$StateCity = "";
		
	$qry="SELECT * FROM tblstate sbc,city sc
				  WHERE sc.StateID = sbc.StateID order by sbc.StateName,sc.CityName";
	$res = $objDB->select($qry);
		
	for($i=0;$i<count($res);$i++)
	{
		$StateCity.= $res[$i]['StateID']."^".$res[$i]['CityID']."^".$res[$i]['CityName']."|";
	}
	return $StateCity;
}

// --------------------------  get a single field data --------------------------------------
function GetFieldData($Tbl,$Field,$WhereOrLimit=""){
	global $objDB;
	$SQL="SELECT ".$Field." FROM ".$Tbl;
	if(!empty($WhereOrLimit))
	$SQL.=" ".$WhereOrLimit;
	//echo "|".$SQL."|";
	$res = $objDB->sql_query($SQL);
	return stripslashes($res[0][$Field]);
}
// ================================= Fill combo box from table =============================
function FillCombo($objDB,$Table, $Text, $Value, $Selected,$WhereText,$WhereValue)
{
	$SQL = "SELECT ".$Text.",".$Value." FROM ".$Table;
	if(isset($WhereText) && $WhereText!="")
	$SQL.= " WHERE ".$WhereText."='".$WhereValue."'";
	$SQL.= " ORDER BY ".$Text;
	//echo $SQL;exit;
	$res = $objDB->select($SQL);
	for($i=0;$i<sizeof($res);$i++)
	{
		echo "<Option Value='".$res[$i][$Value]."'";
		if(is_array($Selected)){
			for($j=0;$j<count($Selected);$j++){
				if($res[$i][$Value]==$Selected[$j])
				echo "selected";
			}
		}
		else if($res[$i][$Value]==$Selected){
			echo "selected";
		}
		echo ">".stripslashes($res[$i][$Text])."</Option>";
	}
}
// ================================= Fill combo box from table =============================
function FillCombo1($Table,$Text,$Value,$Selected,$Where){
	global $objDB;
	$SQL = "SELECT ".$Text.",".$Value." FROM ".$Table;
	if($Where!="")
	$SQL.= " ".$Where;
	$SQL.= " ORDER BY ".$Text;
	//echo $SQL;
	$res = $objDB->select($SQL);

	for($i=0;$i<sizeof($res);$i++)	{
		echo "<Option Value='".$res[$i][$Value]."'";
		if(is_array($Selected)){
			for($j=0;$j<count($Selected);$j++){
				if($res[$i][$Value]==$Selected[$j])
				echo "selected";
			}
		}
		else if($res[$i][$Value]==$Selected){
			echo "selected";
		}
		echo ">".stripslashes($res[$i][$Text])."</Option>\n";
	}
}
function FillCombo2($Table,$Text,$Value,$Selected,$Where){
	global $objDB;
	$SQL = "SELECT ".$Text.",".$Value." FROM ".$Table;
	if($Where!="")
	$SQL.= " ".$Where;
	$SQL.= " ORDER BY ".$Text;
	//echo $SQL;
	$res = $objDB->select($SQL);
	$txt = explode(',',$Text);
	for($i=0;$i<sizeof($res);$i++)	{
		echo "<Option Value='".$res[$i][$Value]."'";
		if(is_array($Selected)){
			for($j=0;$j<count($Selected);$j++){
				if($res[$i][$Value]==$Selected[$j])
				echo "selected";
			}
		}
		else if($res[$i][$Value]==$Selected){
			echo "selected";
		}
		echo ">";
		for($t=0;$t<count($txt);$t++)
		echo stripslashes($res[$i][$txt[$t]])." ";
		echo "</Option>\n";
	}
}


function FillNumericCombo($Start, $End, $select)
{
	for($i=$Start;$i<=$End;$i++)
	{
		echo("<Option Value='".$i."'");
		if (is_numeric($select))
		{
			if($i==$select){
				echo (" selected");
			}
		}
		echo (">".$i."</Option>");
	}
}

function GetPageName(){
	$tmpArray = explode("/",$_SERVER['SCRIPT_FILENAME']);
	$pagename = $tmpArray[sizeof($tmpArray)-1];
	return $pagename;
}

function GetPhysicalRootPath(){
	$tmpStr = str_replace($_SERVER['PHP_SELF'],"",$_SERVER['SCRIPT_FILENAME']);
	return $tmpStr."/".VIR_DIR;
}

function GetPageURL(){
	$pageURL = 'http';
	$pageURL .= "://";
	if ($_SERVER["SERVER_PORT"] != "80") {
		$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	} else {
		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	}
	return $pageURL;
}

function GetContentType($file_extension){
	switch(strtolower($file_extension)){
		case "pdf": $ctype="application/pdf"; break;
		case "exe": $ctype="application/octet-stream"; break;
		case "zip": $ctype="application/zip"; break;
		case "doc": $ctype="application/msword"; break;
		case "xls": $ctype="application/vnd.ms-excel"; break;
		case "ppt": $ctype="application/vnd.ms-powerpoint"; break;
		case "gif": $ctype="image/gif"; break;
		case "png": $ctype="image/png"; break;
		case "jpeg":
		case "jpg": $ctype="image/jpg"; break;
		case "mp3": $ctype="audio/mpeg"; break;
		case "wav": $ctype="audio/x-wav"; break;
		case "mpeg":
		case "mpg":
		case "mpe": $ctype="video/mpeg"; break;
		case "mov": $ctype="video/quicktime"; break;
		case "avi": $ctype="video/x-msvideo"; break;
		case "php":
		case "htm":
		case "html":
		case "txt": die("<b>Cannot be used for ". $file_extension ." files!</b>"); break;
		default: $ctype="application/x-download";
	}
	return $ctype;
}


function GenerateRandomKey($TableName, $FieldName, $Length){
	global $objDB;

	$Key = "";
	$found = false;
	while(!$found){
		while(strlen($Key)<$Length){
			srand((double)microtime()*1000000);
			$number = rand(50,150);
			if($number>=65 && $number<=90)
			$Key = $Key.chr($number);
			elseif($number>=48 && $number<=57)
			$Key = $Key.chr($number);
		}

		$SQL = "Select * from ".$TableName." where ".$FieldName." = '".$Key."'";
		$result = $objDB->select($SQL);

		if(count($result)>0){
			$found = false;
			$Key = "";
		}
		else
		$found = true;
	}
	return trim($Key);
}
function generateSecurityImage($SecurityCode)
{
	$imageEditor = new ImageEditor("security_code_display.jpg", "security_images/", 15);
	$imageEditor->addText($SecurityCode, 5, 5, Array(0, 0, 0));
	$imageEditor->outputFile($SecurityCode.".jpg", "security_images/");
}

function UploadFile($Files, $DestPath, $ValidExtensionArr){
	$FileName = '';
	$Lastpos = strrpos($Files['name'], '.');
	$FileExtension = strtoupper(substr($Files['name'], $Lastpos , strlen($Files['name'])));

	if(in_array($FileExtension, $ValidExtensionArr)){
		$FileName = time().$FileExtension;
		move_uploaded_file($Files['tmp_name'], $DestPath.$FileName);
	}

	return $FileName;
}

// -----------------------  function for password recovery -------------------------------

function PasswordReminder($UserID)
{
	global $objDB;
	$SQL = "Select * from admin where UserID=".$UserID;
	$RecordTmp = $objDB->select($SQL);
	$subject = "Password Reminder";
	$HTMLBody = "Dear ".$RecordTmp[0]["UserName"].",<BR>";
	$HTMLBody = $HTMLBody."This is an automatically generated mail from ".SERVICE_EMAIL." in response to your request for password reminder.<BR>";
	$HTMLBody = $HTMLBody."Your Login information at ".SERVER_ROOT." is as follows<BR><BR>";
	$HTMLBody = $HTMLBody."User Name : ".$RecordTmp[0]["UserName"]."<BR>";
	$HTMLBody = $HTMLBody."Password  : ".$RecordTmp[0]["Password"]."<BR><BR>";

	$flag=  @mail(stripslashes($RecordTmp[0]["Email"]),$subject,$HTMLBody,MAIL_HEADER);

	return $flag;
}

function userforgotPassword($UserID)
{
	global $objDB;
	$SQL = "Select * from member where id=".$UserID;
	$RecordTmp = $objDB->select($SQL);
	$subject = "Password Reminder";
	$HTMLBody = "Dear ".stripslashes($RecordTmp[0]["firstname"]." ".$RecordTmp[0]["lastname"]).",<BR>";
	$HTMLBody = $HTMLBody."This is an automatically generated mail from ".SERVICE_EMAIL." in response to your request for password reminder.<BR>";
	$HTMLBody = $HTMLBody."Your Login information at ".SERVER_ROOT." is as follows<BR><BR>";
	$HTMLBody = $HTMLBody."Username : ".$RecordTmp[0]["username"]."<BR>";
	$HTMLBody = $HTMLBody."Password  : ".$RecordTmp[0]["password"]."<BR><BR>";
	$flag=@mail(stripslashes($RecordTmp[0]["email"]),$subject,$HTMLBody,MAIL_HEADER);

	return $flag;
}

// ---------------------------- send mail template -------------------------------------------

function SendEmail($From, $To, $Subject, $Template, $TemplateVars){
	
	$Headers = "From: <".$From.">\r\n" .
				'X-Mailer: PHP/' . phpversion() . "\r\n" .
				"MIME-Version: 1.0\r\n" .
				"Content-Type: text/html; charset=utf-8\r\n" .
				"Content-Transfer-Encoding: 8bit\r\n\r\n";	
	
	$HTMLBody = "";

	$f = fopen($Template,"r") or die("File Not Found...?");
	$HTMLBody = fread($f,filesize($Template));
	fclose($f);

	foreach($TemplateVars as $Find=>$ReplaceWith){
		$HTMLBody = str_replace("{".$Find."}",$ReplaceWith,$HTMLBody);
	}
	/*
	echo $To. "<br>";
	 echo $Subject. "<br>";
	echo $HTMLBody. "<br>";
	echo $Headers;
	exit;
	*/
	
	$flag = @mail($To, $Subject, $HTMLBody, $Headers);
	return $flag;
}

function SendTemplateMail($Subject,$MultiRecepient,$Template,$DataTableName,$RecipientTableName,$ArrDataFields,$ArrRecipientFields,$MailTo,$Custom,$EmailField="",$RecipientWhere="", $DataWhere="")
{
	$HTMLBody="";
	$ArrVars = array();
	$ArrDataVars=array();
	$ArrRecipientVars=array();
	$MailCounter = 0;

	if($DataTableName!=''){
		$rsData = Fetch_Data($DataTableName,$ArrDataFields,$DataWhere);
		for($i=0;$i<sizeof($ArrDataFields);$i++){
			$ArrDataVars[$ArrDataFields[$i]]=$rsData[0][$ArrDataFields[$i]];
		}
	}

	if($RecipientTableName!=''){
		$rsRecipient = Fetch_Data($RecipientTableName,$ArrRecipientFields,$RecipientWhere);
		for($j=0;$j<count($rsRecipient);$j++){
			for($i=0;$i<sizeof($ArrRecipientFields);$i++){
				$ArrRecipientVars[$j][$ArrRecipientFields[$i]]=$rsRecipient[$j][$ArrRecipientFields[$i]];
			}
		}
	}

	$f = fopen(MAIL_TEMPLATE_PATH."/".$Template,"r") or die("File Not Found...?");
	$TemplateBody = fread($f,filesize(MAIL_TEMPLATE_PATH."/".$Template));
	fclose($f);

	$TemplateBody = str_replace("{MAIL_IMAGE_PATH}",MAIL_IMAGE_PATH,$TemplateBody);
	$TemplateBody = str_replace("{MAIL_CSS_PATH}",MAIL_CSS_PATH,$TemplateBody);


	if($MultiRecepient){
		for($i=0;$i<sizeof($rsRecipient);$i++){
			$HTMLBody=$TemplateBody;
			if(count($ArrDataVars)>0){
				foreach($ArrDataVars as $Find=>$ReplaceWith)
				$TemplateBody = str_replace("{".$Find."}",$ReplaceWith,$TemplateBody);
			}
			if(!empty($ArrRecipientVars[$i])){
				foreach($ArrRecipientVars[$i] as $Find=>$ReplaceWith)
				$TemplateBody = str_replace("{".$Find."}",$ReplaceWith,$TemplateBody);
			}
			if(is_array($Custom)){
				foreach($Custom as $Find=>$ReplaceWith)
				$TemplateBody = str_replace("{".$Find."}",$ReplaceWith,$TemplateBody);
			}
			$HTMLBody=$TemplateBody;
			if(mail($rsRecipient[$i][$EmailField],$Subject,$HTMLBody,MAIL_HEADER))
			$MailCounter++;
		}
	}else{
		$HTMLBody=$TemplateBody;
		if(count($ArrDataVars)>0){
			foreach($ArrDataVars as $Find=>$ReplaceWith)
			$TemplateBody = str_replace("{".$Find."}",$ReplaceWith,$TemplateBody);
		}
		if(is_array($Custom)){
			foreach($Custom as $Find=>$ReplaceWith)
			$TemplateBody = str_replace("{".$Find."}",$ReplaceWith,$TemplateBody);
		}
		$HTMLBody=$TemplateBody;
		if(mail($MailTo,$Subject,$HTMLBody,MAIL_HEADER))
		$MailCounter++;
	}
	return $MailCounter;
}

function DateDiff($dformat, $endDate, $beginDate){
	$date_parts1=explode($dformat, $beginDate);
	$date_parts2=explode($dformat, $endDate);
	$start_date=gregoriantojd($date_parts1[0], $date_parts1[1], $date_parts1[2]);
	$end_date=gregoriantojd($date_parts2[0], $date_parts2[1], $date_parts2[2]);
	return $end_date - $start_date;
}


function ChangeDateFormat($Date, $FromFormat, $ToFormat){
	$KnownFormat = array("012"=>"ddmmyyyy","102"=>"mmddyyyy","210"=>"yyyymmdd");
	if(!in_array($FromFormat,$KnownFormat) || !in_array($ToFormat,$KnownFormat)){
		echo "<h3>Error in function \"ConvertDateFormat\" : Unknown Date Format";
		exit;
	}
	$Seperator="";
	if(strpos($Date,"/")===false){
	}else{
		$Seperator="/";
	}
	if(strpos($Date,"\\")===false){
	}else{
		$Seperator="\\";
	}
	if(strpos($Date,"-")===false){
	}else{
		$Seperator="-";
	}
	if($Seperator==""){
		echo "<h3>Error in function \"ConvertDateFormat\" : Unknown Date Seperator";
		exit;
	}
	$DateArr = explode($Seperator,$Date);
	$FromDateSequence = array_search($FromFormat, $KnownFormat);

	$Day = $DateArr[strpos($FromDateSequence,"0")];
	$Month = $DateArr[strpos($FromDateSequence,"1")];
	$Year = $DateArr[strpos($FromDateSequence,"2")];

	$ToDateSequence = array_search($ToFormat, $KnownFormat);

	$NewDate = $DateArr[substr($ToDateSequence,0,1)].$Seperator.$DateArr[substr($ToDateSequence,1,1)].$Seperator.$DateArr[substr($ToDateSequence,2,1)];
	return $NewDate;
}

function NromalizeURL($URL){
	$URL = str_replace(" ","-",$URL);
	$URL = str_replace(".","",$URL);
	$URL = str_replace("--","-",$URL);
	$URL = str_replace("\"","",$URL);
	$URL = str_replace("'","",$URL);
	return strtolower($URL);
}


function array_search_r($needle, $haystack){
	foreach($haystack as $value){
		if(is_array($value))
		$match=array_search_r($needle, $value);
		if($value==$needle)
		$match=1;
		if($match)
		return 1;
	}
	return 0;
}

function Remove_Key_QueryString($QueryString,$Key)
{

	$result=split("&",$QueryString);
	$returnstring="";
	for($i=0;$i<count($result);$i++)
	{
		$add=true;
		foreach ($Key as $key)
		{
			if(substr($result[$i],0,strlen($key))==$key)
			$add=false;
		}
		if($add)
		{
			if($returnstring=="")
			$returnstring.=$result[$i];
			else
			$returnstring.="&".$result[$i];
		}
	}
	return $returnstring;
}
// ======================  Fetch Data from table  ===============================
function FetchData($Tbl,$FieldsArr,$Where="",$Sort="",$Limit=""){
	global $objDB;
	$FetchStr="";

	if(!empty($FieldsArr)){
		foreach($FieldsArr as $Feild)
		$FetchStr.=" ".$Feild.",";
	}else{
		$FetchStr=" * ";
	}
	$FetchStr=rtrim($FetchStr,",");
	$SQL="SELECT ".$FetchStr." FROM ".$Tbl;
	if(!empty($Where))
	$SQL.=" ".$Where;
	if(!empty($Sort))
	$SQL.=" ".$Sort;
	if(!empty($Limit))
	$SQL.=" ".$Limit;

	//echo $SQL.'<br>';exit;
	$res = $objDB->sql_query($SQL);
	return($res);
}

// ============================== Delete Data from table  =========================
function DeleteData($Tbl,$WhereField="",$Operater="",$WhereValue=""){
	global $objDB;
	$SQL="DELETE FROM ".$Tbl;
	if(!empty($WhereField) && !empty($WhereValue) && !empty($Operater))
	$SQL.=" WHERE ".$WhereField.$Operater.$WhereValue;

	//echo $SQL;exit;
	$objDB->sql_query($SQL);
}

// ================================ Fetchdata using join ============================
function FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where="",$Sort="",$Limit=""){
	global $objDB;
	$FetchStr="";
	$TableStr="";
	$JoinOrNot=0;
	$JoinStr="";
	$JoinTempArr="";
	$FromStr="";
	if(!empty($TblFieldsArr)){
		foreach($TblFieldsArr as $Table=>$Fields){
			if($JoinOrNot==0)
			$FromStr=" ".$Table;
			if(!empty($Fields)){
				$TempArr=explode(',',$Fields);
				foreach($TempArr as $Feild)
				$FetchStr.=" `".$Table."`.".$Feild.",";
			}else{
				$FetchStr.=" `".$Table."`.*,";
			}
			$JoinOrNot++;
		}
	}
	if($JoinOrNot>1){
		for($i=0;$i<count($JoinTblToArr);$i++){
			$JoinTempArrTo=array_keys($JoinTblToArr);
			$JoinTempArrToVal=array_values($JoinTblToArr);
			$JoinTempArrOn=array_keys($JoinTblOnArr);
			$JoinTempArrOnVal=array_values($JoinTblOnArr);
			$JoinStr.=" ".$JoinType." ".$JoinTempArrToVal[$i]." on ".$JoinTempArrOnVal[$i].".".$JoinTempArrOn[$i]." = ".$JoinTempArrToVal[$i].".".$JoinTempArrTo[$i]." ";
			if(!empty($JoinTblConditionArr))
			$JoinStr.="and ".$JoinTblConditionArr[$i];
		}
	}
	$FetchStr=rtrim($FetchStr,',');
	$SQL="SELECT ".$FetchStr." FROM ".$FromStr;
	if(!empty($JoinStr))
	$SQL.=" ".$JoinStr;
	if(!empty($Where))
	$SQL.=" ".$Where;
	if(!empty($Sort))
	$SQL.=" ".$Sort;
	if(!empty($Limit))
	$SQL.=" ".$Limit;

	//echo $SQL."  |  ";
	$res = $objDB->sql_query($SQL);
	return($res);
}

function HandleMagicQuotes($Value)
{
	if(ini_get('magic_quotes_gpc') == 1)
	return $Value;
	else
	return addslashes($Value);
}
function resampimagejpg($forcedwidth, $forcedheight, $sourcefile, $destfile, $imgcomp)
{
	$g_imgcomp=100-$imgcomp;
	$g_srcfile=$sourcefile;
	$g_dstfile=$destfile;
	$g_fw=$forcedwidth;
	$g_fh=$forcedheight;

	if(file_exists($g_srcfile))
	{
		$g_is=getimagesize($g_srcfile);
		if(($g_is[0]-$g_fw)>=($g_is[1]-$g_fh))
		{
			$g_iw=$g_fw;
			$g_ih=($g_fw/$g_is[0])*$g_is[1];
		}
		else
		{
			$g_ih=$g_fh;
			$g_iw=($g_ih/$g_is[1])*$g_is[0];
		}
		$src=explode(".",$g_srcfile);
		$var=count($src);

		if($src[$var-1]=='gif' || $src[$var-1]=='GIF')
		{
			$img_src=ImageCreateFromGIF($g_srcfile);
			//$img_src= ImageColorAllocate($img_src, 250, 250, 250);
			$img_dst=imagecreate($g_iw,$g_ih);
			$img_dst1 = ImageColorAllocate($img_dst, 255, 255, 255);
		}
		if($src[$var-1]=='png')
		{
			$img_src=ImageCreateFromPNG($g_srcfile);
			$img_dst=imagecreate($g_iw,$g_ih);
			$img_dst1 = ImageColorAllocate($img_dst,255, 255, 255);
		}
		if($src[$var-1]=='jpg' || $src[$var-1]=='JPG')
		{
			$img_src=imagecreatefromjpeg($g_srcfile);
			$img_dst=imagecreate($g_iw,$g_ih);
			$img_dst = &imageCreateTrueColor( $g_iw, $g_ih);
		}
		if($src[$var-1]=='jpeg' || $src[$var-1]=='JPEG')
		{
			$img_src=imagecreatefromjpeg($g_srcfile);
			$img_dst=imagecreate($g_iw,$g_ih);
			$img_dst = &imageCreateTrueColor( $g_iw, $g_ih);
		}

		imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, $g_iw, $g_ih, $g_is[0], $g_is[1]);
		if($src[$var-1]=='jpg' || $src[$var-1]=='JPG')
		{
			imagejpeg($img_dst, $g_dstfile, $g_imgcomp);
		}
		if($src[$var-1]=='jpeg' || $src[$var-1]=='JPEG')
		{
			imagejpeg($img_dst, $g_dstfile, $g_imgcomp);
		}
		if($src[$var-1]=='png')
		{
			imagepng($img_dst, $g_dstfile, $g_imgcomp);
		}
		if($src[$var-1]='gif' || $src[$var-1]=='GIF')
		{
			imagegif($img_dst, $g_dstfile, $g_imgcomp);
		}
		imagedestroy($img_dst);
		return true;
	}
	else
	return false;
}

function FetchQuery($Tbl,$FieldsArr,$Where="",$Sort="",$Limit=""){
	global $objDB;
	$FetchStr="";

	if(!empty($FieldsArr)){
		foreach($FieldsArr as $Feild)
		$FetchStr.=" ".$Feild.",";
	}else{
		$FetchStr=" * ";
	}
	$FetchStr=rtrim($FetchStr,",");
	$SQL="SELECT ".$FetchStr." FROM ".$Tbl;
	if(!empty($Where))
	$SQL.=" ".$Where;
	if(!empty($Sort))
	$SQL.=" ".$Sort;
	if(!empty($Limit))
	$SQL.=" ".$Limit;

	//echo $SQL.'<br>';exit;
	//	$res = $objDB->sql_query($SQL);
	return($SQL);
}

// ========= Fetch Value ==========
function FetchValue($Tbl,$FieldName,$WhereField,$WhereCondition){
	global $objDB;

	$SQL="SELECT * FROM ".$Tbl;
	$SQL.=" WHERE ".$WhereField." = '".$WhereCondition."'";

	//echo $SQL.'<br>';exit;
	$res = $objDB->sql_query($SQL);
	return stripslashes($res[0][$FieldName]);
}
function FetchValue1($Tbl,$DisplayField,$WhereField,$WhereCondition){
	global $objDB;

	$field = explode(',',$DisplayField);
	$SQL="SELECT * FROM ".$Tbl;
	$SQL.=" WHERE ".$WhereField." = '".$WhereCondition."'";

	//echo $SQL.'<br>';exit;
	$res = $objDB->sql_query($SQL);
	$result = '';
	for($i=0;$i<count($field);$i++){
		$result .= $res[0][$field[$i]]." ";
	}
	//var_Dump($result);
	return stripslashes($result);
}
function getCurrencySign($currency){

	if($GLOBALS['LIVE'] == true){
		return $currency;
	}else{
		return htmlentities($currency);
	}
}

function checkEmail($email){
	$pattern = "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$";
	if (eregi($pattern, $email)){
		return true;
	}else {
		return false;
	}
}

?>
