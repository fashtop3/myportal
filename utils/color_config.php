<?php
	//Set this Color constant to have different coloured view of your admin home
	define("BGCOLOR_TOP_TBL","#C1E4F8");
	define("BGCOLOR_HEADER_TBL","#FFFFFF");
	define("BGCOLOR_TOP_TBL_LeftBottomTopRight","#246494");
	//--Navigation
	define("BORDERCOLOR_NAVIGATION","#246494");
	define("BGCOLOR_NAVIGATION_TD","#cccccc");
	define("FONTCOLOR_NAVIGATION_MENU","#003399");
////	define("FONTCOLOR_NAVIGATION_MENU","#800001");
	define("FONTCOLOR_NAVIGATION_MENU_HOVER","#F2EFE6");
	define("FONTCOLOR_NAVIGATION_LINK","#246494");
	define("FONTCOLOR_NAVIGATION_LINK_HOVER","#ffffff");//#6A3626
	define("BGCOLOR_NAVIGATION_LINK","#ffffff");
	define("BGCOLOR_NAVIGATION_LINK_HOVER","#246494");//#F3D9D1
	//--Listing Table
	define("BGCOLOR_ROW","BGCOL");
	define("BGCOLOR_ROW_HOVER","#FFF5F0");
	define("BGCOLOR_ODD_ROW","#EBF2FE");	
	define("BGCOLOR_EVEN_ROW","#D9E7FE");
	define("BGTABLECOLOR_ROW","#F1F1F1");
	//--Add Edit Table
	define("BGCOLOR_MESSAGE_ROW","#FFFFFF");	
	
?>
