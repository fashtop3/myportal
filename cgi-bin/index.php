<?php
$html = $_SERVER['REQUEST_URI']; // "/school-college-management-system.html"
if($html=="/index.html" || $html=="/index.php")
	header("Location: /");
?>
<?php
session_start();

require_once('utils/config.php');	
require_once('utils/dbClass.php');
require_once('utils/functions.php');
require_once('utils/paging.php');
$objDB = new MySQLCN;

$userplc = DEFAULT_PLACE;

if($_REQUEST['p']=='')
	$p = LoadVariable('p',"home");	
else
	$p = LoadVariable('p',"");	
$_SESSION['page'] = $_SERVER['REQUEST_URI'];

if($_REQUEST['p']!='print' && $_REQUEST['p']!='gallery_view'){
	$ou = array('outreach_detail','outreach');
	
	$cm = array("request_prayer","contact");
	$rs = array("higherway","quarter_index","higherway_email","higherway_archive","resource","school","curriculum_list","sermon_list","tract_list","tract_detail","sermon_detail","curriculum_detail","past_sermon_list");	
	$nw = array("news","news_archive","news_detail","news_video","calendar","event","gallery","event_archive","event_detail");
	$lb = array("library","devotional","cd_list","cd","devotional_archive","devotional_detail","view_order","cart","offering","tithes","online_giving");
	$cm2 = array('camp_tips','camp_meeting','volunteer');
	
	if($_REQUEST['p']=='camp_meeting'){
		$hdrimg = 'images/camp_img.jpg';
		$imgcls = '';
		$imgheight = '260';
		$imgwidth = '728';
		$tdheight = '270';
	}
	else{
		$imgcls = 'inner_bnr_bg';
		$hdrimg = "images/banner_1.jpg";
		$imgheight = '163';
		$imgwidth = '728';
		$tdheight = '175';
	}
	
	if($_REQUEST['p']=='camp_tips')
		$hdrimg = 'images/tip.jpg';
	if($_REQUEST['p']=='volunteer')
		$hdrimg = 'images/volinteer.jpg';
	if(in_array($p,$ou))
		$hdrimg = 'images/outreach_ban.jpg';
	if(in_array($p,$cm))
			$hdrimg = "images/contact_ban.jpg";
	if(in_array($p,$lb))
			$hdrimg = "images/banner_2.jpg";
	if(in_array($p,$nw))
			$hdrimg = "images/new_ban.jpg";		
	if(in_array($p,$rs))
			$hdrimg = "images/school_ban.jpg";
	if($_REQUEST['p']=='tithes' || $_REQUEST['p']=='offering' || $_REQUEST['p']=='online_giving' || $_REQUEST['p']=='cart')			
		$hdrimg = 'images/shop_banner.jpg';
	$pa = array("cms");	
		if(in_array($p,$pa)) 
			$hdrimg = "images/about_hdr.jpg";
		
	if($p=='devotional' || $p=='devotional_archive' || $p=='devotional_detail')
		$hdrimg = 'images/devotional_ban.jpg';
	if($p=='calendar')
		$hdrimg = 'images/sun_school_ban.jpg';
			
	if($p=='request_prayer')
		$hdrimg = 'images/prayer_ban.jpg';
		
	if($p=='curriculum_list' || $p == 'curriculum_detail')
		$hdrimg = 'images/lession_banner.jpg';
		
	if($p=='event_detail' && $_REQUEST['id'] == '20')
		$hdrimg = 'images/dedicated.jpg';
	
	if($_REQUEST['p']=='archive_video' || $_REQUEST['p']=='sermon_list' || $_REQUEST['p']=='past_sermon_list' || $_REQUEST['p']=='gallery')
		$hdrimg = 'images/media_banner.jpg';	
	$pi = array("cd_list","cd","offering","tithes","online_giving","cart");		
	if(in_array($p,$pi))
		$multi = 1;
	else
		$multi = 0;
	
	if($userplc=='' || $userplc=='0')
		$userplc = GetFieldData("configuration","config_value","where config_key = 'DEFAULT_PLACE'");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="pragma" content="no-cache">
<title>Welcome to <?php echo COMPANY_NAME?></title>
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<link rel="canonical" href="http://www.apostolicfaith.org.uk/" />

<link href="css/site.css" rel="stylesheet" type="text/css">
<link href="css/nav.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/highslide.css" />
<link href="SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css">
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src="js/jquery.js"></script>
<script src="SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
<script type="text/javascript" src="js/highslide-with-html.js"></script>
<script type="text/javascript" src="js/xyz.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.2.js"></script>
<script src="js/jquery.anythingslider.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript" src="js/jquery.metadata.js"></script>
<?php /*?><script type="text/javascript" src="js/jqueryui.js"></script><?php */?>
<script language="javascript" type="text/javascript" src="js/jquery.validate.js"></script>
<script language="javascript" src="js/calender.js"></script>
<script language="javascript" src="js/general.js"></script>

<?php if($_REQUEST['p']=='live'){?>
<?php /*?><script type="text/javascript" src="http://centrepoint.christfaithmedia.co.uk/js/swfobject/swfobject.js"></script>
<script type="text/javascript">
var flashvars = {};
flashvars.config = "http://centrepoint.christfaithmedia.co.uk/index.php/config/live/29.xml";
var params = {};
params.play = "true";
params.loop = "true";
params.menu = "true";
params.quality = "high";
params.wmode = "transparent";
params.devicefont = "false";
params.swliveconnect = "true";
params.allowscriptaccess = "always";
params.allowfullscreen = "true";
params.allownetworking = "all";
var attributes = {};
attributes.id = "live375";
swfobject.embedSWF("http://centrepoint.christfaithmedia.co.uk/players/CfmPlayer.swf","div375", "400", "300", "10.0.0","http://centrepoint.christfaithmedia.co.uk/js/swfobject/expressInstall.swf",flashvars, params, attributes);
</script>
<script type="text/javascript" src="http://centrepoint.christfaith media.co.uk/js/piwik-callback.js"></script><?php */?>
<!-- player setup new code begin -->
<script type="text/javascript" src="http://centrepoint.christfaithmedia.co.uk/js/swfobject/swfobject.js"></script>
<script type="text/javascript">
                var flashvars = {};
                flashvars.config = "http://centrepoint.christfaithmedia.co.uk/index.php/config/live/29.xml";
                var params = {};
                params.play = "true";
                params.loop = "true";
                params.menu = "true";
                params.quality = "high";
                params.wmode = "transparent";
                params.devicefont = "false";
                params.swliveconnect = "true";
                params.allowscriptaccess = "always";
                params.allowfullscreen = "true";
                params.allownetworking = "all";
                var attributes = {};
                attributes.id = "live842";
                swfobject.embedSWF("http://centrepoint.christfaithmedia.co.uk/players/CfmPlayer.swf",
                                "div842", "400", "300", "10.0.0",
                                "http://centrepoint.christfaithmedia.co.uk/js/swfobject/expressInstall.swf",
                                flashvars, params, attributes);
</script>
<script type="text/javascript" src="http://centrepoint.christfaithmedia.co.uk/js/piwik-callback.js"></script>
<!-- Compatibility code --> 
<script type="text/javascript">
sfHover = function() {

                var sfEls = document.getElementById("nav").getElementsByTagName("LI");

                for (var i=0; i<sfEls.length; i++) {

                    sfEls[i].onmouseover=function() {

                        this.className+=" sfhover";

                    }

                    sfEls[i].onmouseout=function() {
                        this.className=this.className.replace(new RegExp(" sfhover\\b"), "");

                    }

                }
                            }

            if (window.attachEvent) window.attachEvent("onload", sfHover);

        //--><!]]></script> 
        
<!-- placeholder for player -->
<div id="div842">
    <a href="http://www.adobe.com/go/getflashplayer">
        <img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" />
    </a>
</div>
<div id="div842">
    <a href="http://www.apostolicfaith.org.uk/indexHTML"> Cannot see player?
     </a>
</div>
<!-- tracking tag begin -->
<script type="text/javascript">
var pkBaseURL = (("https:" == document.location.protocol) ? "https://centrepoint.christfaithmedia.co.uk/piwik/" : "http://centrepoint.christfaithmedia.co.uk/piwik/");
document.write(unescape("%3Cscript src='" + pkBaseURL + "piwik.js' type='text/javascript'%3E%3C/script%3E"));
</script><script type="text/javascript">
try {
  var piwikTracker = Piwik.getTracker(pkBaseURL + "piwik.php", 26);
  piwikTracker.trackPageView();
  piwikTracker.enableLinkTracking();
} catch( err ) {}
</script><noscript><p><img src="http://centrepoint.christfaithmedia.co.uk/piwik/piwik.php?idsite=26" style="border:0" alt="" /></p></noscript>
<!-- tracking tag end -->
<!--<iframe src="http://centrepoint.christfaithmedia.co.uk/index.php/live/29/YWRlQGFwb3N0b2xpY2ZhaXRoLm9yZy51aw==/26/400/300" width="400" height="300" frameborder="0" marginheight="0" marginwidth="0" scrolling="no"></iframe> -->
<!-- player setup code end -->
<?php } ?>

<?php if($_REQUEST['p']=='live_audio'){?>
<script type="text/javascript" src="http://centrepoint.christfaithmedia.co.uk/js/swfobject/swfobject.js"></script>
<script type="text/javascript">
var flashvars = {};
flashvars.config = "http://centrepoint.christfaithmedia.co.uk/index.php/config/audio/19.xml";
var params = {};
params.play = "true";
params.loop = "true";
params.menu = "true";
params.quality = "high";
params.scale = "exactfit";
params.wmode = "transparent";
params.devicefont = "false";
params.swliveconnect = "true";
params.allowscriptaccess = "always";
params.allownetworking = "all";
var attributes = {};
attributes.id = "ply521";
swfobject.embedSWF("http://centrepoint.christfaithmedia.co.uk/players/audio/chromed-player.swf", "div521","309", "150", "10.0.0","http://centrepoint.christfaithmedia.co.uk/js/swfobject/expressInstall.swf",flashvars, params, attributes);
</script>
<?php } ?>
<?php 
$slide_images = FetchData("slider",array(),"where status = 'active' and popup_image != '' order by set_order");
$pop = array();
$pop1 = array();
foreach($slide_images as $slid){
	$pop[] = "'uploads/banner/".$slid['popup_image']."'";
	$pop1[] = "uploads/banner/".$slid['popup_image'];
}
$popimg = @implode(",", $pop);
$popimg1 = @implode(",", $pop1);
if(count($pop) > 0){
	$popimg .= ',';	
}
?>

<script type="text/javascript">
function preload(images) {
    if (document.images) {
        var i = 0;
        var imageArray = new Array();
        imageArray = images.split(',');
        var imageObj = new Image();
        for(i=0; i<=imageArray.length-1; i++) {
            //document.write('<img src="' + imageArray[i] + '" />');// Write to page (uncomment to check images)
            imageObj.src=images[i];
        }
    }
}
preload('<?php echo $popimg1?>');
</script>

</head>
<body onload="MM_preloadImages(<?php echo $popimg?> 'images/about_hdr.jpg','images/school_ban.jpg','images/new_ban.jpg','images/banner_2.jpg','images/contact_ban.jpg','images/outreach_ban.jpg','images/devotional_ban.jpg','images/sun_school_ban.jpg','images/banner_1.jpg','images/prayer_ban.jpg','images/lession_banner.jpg','images/pink1.png','images/orange1.png','images/blue1.png','images/green1.png','images/pink11.png','images/orange11.png','images/blue11.png','images/green11.png','images/sl1.jpg','images/sl2.jpg','images/sl3.jpg','images/sl4.jpg','images/sl5.jpg','images/menu_bg2.jpg','images/menu_hover.jpg','clock/0.gif','clock/1.gif','clock/2.gif','clock/3.gif','clock/4.gif','clock/5.gif','clock/6.gif','clock/7.gif','clock/8.gif','clock/9.gif','clock/am.gif','clock/pm.gif')">
<?php /*?><body><?php */?>
<table width="976" cellpadding="0" cellspacing="0" border="0" class="" align="center">
	<tr>
  	<td align="left" valign="top"><table width="970" <?php if($p=='curriculum_detail') { ?> onclick="xstooltip_hide('tooltip_123')" <?php } ?> border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top" class="top_rpt">
          <?php include('includes/header.php');?></td>
        </tr>
        <tr>
          <td align="left" valign="top" bgcolor="#FFFFFF"><table width="955" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td width="735" align="left" valign="top"><?php 
                  if($p=='home'){
                    include('includes/include_external.php'); ?>
                    <script type="text/javascript">
                      
                      var TabbedPanels1 = new Spry.Widget.TabbedPanels("TabbedPanels1");
                      
                    </script>
                  <?php
                  }
                  else
                    include('includes/inner_head.php');
                ?>
                </td>
                <td width="220" align="left" valign="top"><?php include('includes/left.php');?></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="left" valign="top" bgcolor="#FFFFFF" class="btm_img"><?php include('includes/footer.php');?></td>
        </tr>
      </table></td>
  </tr>
</table>

</body>
</html>
<?php
$class = "open opencareers";
?>
<!-- Resource Menu Start -->
<?php 
	
	if(in_array($p,$rs) || in_array($p,$lb)){ 
		$class = "open opencomplaints";
		$hid= 'resource';
		
		if($p=='school')
			$pid = 'school';
			
		if($p=='higherway' || $p=='higherway_email' || $p=='higherway_archive')
			$pid = 'higherway';
			
		$pi = array("curriculum_list","curriculum_detail");		
		if(in_array($p,$pi))
			$pid = 'curriculum';
		
		$pi1 = array("sermon_list");		
		$pi2 = array("past_sermon_list");		
		$pi3 = array("sermon_detail");		
		if(in_array($p,$pi1) || in_array($p,$pi2) || in_array($p,$pi3)){
			$pid = 'sermon';
			if(in_array($p,$pi1))
				$pid1 = "sermonlist";
			if(in_array($p,$pi2))
				$pid1 = "pastsermon";
		?>
		<script type="text/javascript">
        document.getElementById('resource_menu').style.display='block';
        document.getElementById('sermon_submenu').style.display='block';
      </script>
		<?php 
		}else{ ?>
		<script type="text/javascript">
			document.getElementById('resource_menu').style.display='none';
			document.getElementById('sermon_submenu').style.display='none';
			</script>
		<?php }
		
		$pi = array("tract_list","tract_detail");		
		if(in_array($p,$pi))
			$pid = 'tract';
			
		$pi = array("devotional","devotional_detail","devotional_archive");		
		if(in_array($p,$pi))
			$pid = 'devotional';
		$pi = array("online_giving","offering","tithes");		
		if(in_array($p,$pi))
			$pid = 'give';
		/* multimedia start */
		$pi = array("cd_list","cd","cart");		
		if(in_array($p,$pi)){
			$pid = 'multimedia';
		?>
		<script type="text/javascript">
      document.getElementById('resource_menu').style.display='block';
      document.getElementById('multimedia_submenu').style.display='block';
      </script>
		<?php 
      $pi = $_REQUEST['type'];		
      if($pi!=''){
        $pid1 = $pi;
				if($_REQUEST['p']=='cd'){
        	$si = FetchValue("cd","category_id","id",$_REQUEST['id']);
				}else
					$si = $_REQUEST['id'];
        if($si!='')
          $sid = $si;
      }
		}else{
	?>
		<script type="text/javascript">
    document.getElementById('resource_menu').style.display='none';
    document.getElementById('multimedia_submenu').style.display='none';
    </script>
	<?php } 
	/* multimedia end */
	?>
	<script type="text/javascript">
  document.getElementById('resource_menu').style.display='block';
  </script>
	<?php }else{ ?>
	<script type="text/javascript">
  document.getElementById('resource_menu').style.display='none';	
  </script>
	<?php } ?>
<!-- Resource Menu End -->
<!-- News Menu Start -->
	<?php 
	if(in_array($p,$nw)){ 
		//$hdrimg = "images/inner_ban3.jpg";
		$hid= 'news';
		$pid = 'newslink';
		$class = "open openwhatwecover";
		if($p=='calendar')
			$pid = 'calendars';
			
		$pi = array("event","event_archive","event_detail");		
		if(in_array($p,$pi))
			$pid = 'event';
		
		$pi = array("gallery");		
		if(in_array($p,$pi))
			$pid = 'gallery';
	?>
<script type="text/javascript">
  document.getElementById('news_menu').style.display='block';
  </script>
<?php }else{ ?>
<script type="text/javascript">
  document.getElementById('news_menu').style.display='none';
  </script>
<?php } ?>
<!-- News Menu End -->

<!-- Camp Meeting -->
<?php
if(in_array($p,$cm2)){ 
	$hid= 'camp';
	$class = "open opencampaign";
	if($_REQUEST['p']=='camp_tips' || $_REQUEST['p']=='volunteer')
		$pid = $_REQUEST['p'];
?>
<!--<script type="text/javascript">
  document.getElementById('camp_menu').style.display='block';
  </script>-->
<?php }else{ ?>
<!--<script type="text/javascript">
  document.getElementById('camp_menu').style.display='none';
  </script>-->
<?php } ?>
<!-- End -->


<!-- Contact Menu Start -->
<?php 
	if(in_array($p,$cm)){ 
		//$hdrimg = "images/inner_ban6.jpg";
		$hid= 'contact';
		$class = "open opentools";
		$pi = array("request_prayer");		
		if(in_array($p,$pi))
			$pid = 'prayer';
		
		$pi = array("register");		
		if(in_array($p,$pi))
			$pid = 'register';
		
		$pi = array("login");		
		if(in_array($p,$pi))
			$pid = 'login';
				
	?>
<script type="text/javascript">
  document.getElementById('contact_menu').style.display='block';
  </script>
<?php }else{ ?>
<script type="text/javascript">
  document.getElementById('contact_menu').style.display='none';
  </script>
<?php } ?>
<!-- Contact Menu End -->
<script type="text/javascript" language="javascript">
	<?php 
	if($p == 'home'){
		$hid = 'home';
		
	}
	if($p=='cms' && $_REQUEST['id']=='about_us'){
		$hid = 'aboutus';		
		$class = "open openpublic";
	}
	if($p=='resource'){
		$hid= 'resource';
		
	}
	if($p=='news'){
		$hid = 'news';
		
	}
	if($p=='outreach' || $p=='outreach_detail'){
		$hid = 'outreach';
		$class = "open openmedia";
	}
	if($p=='contact'){
		$hid = 'contact';
		
	}
	if($hid!=''){
	?>
	document.getElementById('<?php echo $hid ?>').className = '<?php echo $class?>';
	//'nav_link_act';
	<?php } 
	if($pid!=''){
	?>
	document.getElementById('<?php echo $pid ?>').className = 'active';
	//'sub1nav_link_act';
	<?php } if($sid!=''){
	?>
	document.getElementById('<?php echo $sid ?>').className = 'active';
	//'sub3nav_link_act';
	<?php } ?>
	<?php  if($pid1!=''){
	?>
	document.getElementById('<?php echo $pid1 ?>').className = 'active';
	//'sub2nav_link_act';
	<?php } ?>
</script>
<?php }else include('includes/include_external.php'); ?>
