<?php
	session_start();
	
	require_once("utils/config.php");
	require_once("utils/dbclass.php");
	require_once("utils/functions.php");
	$objDB = new MySQLCN();
	
	$userplc = $_REQUEST['Place'];
	
require_once('paypal.class.php');  // include the class file
$p = new paypal_class;             // initiate an instance of the class

if(strtoupper(PAYPAL_TEST)=='YES')
	$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';//'https://www.paypal.com/cgi-bin/webscr';     // paypal url
else
	$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';//'https://www.paypal.com/cgi-bin/webscr';     // paypal url
            
$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

if (empty($_GET['action'])) $_GET['action'] = 'process';  

switch ($_GET['action']) {
   case 'process':      // Process and order...
      $p->add_field('business', PAYPAL_EMAIL);
      $p->add_field('return', $this_script.'?action=success');
      $p->add_field('cancel_return', $this_script.'?action=cancel');	  
      $p->add_field('notify_url', $this_script.'?action=ipn');
			$p->add_field('no_shipping',1);	
			
			$sqlc = "SELECT * FROM cart WHERE member_id='".$_SESSION['USER_ID']."'";
			
			$cart = $objDB->select($sqlc);
	  	$subtotal = 0;
			$qt = 0;
			for($i=0;$i<count($cart);$i++)
			{
				$qty = $cart[$i]['qty'];
				if($cart[$i]['product_id']=='')
					$product_name = $cart[$i]['product'];				
				else
					$product_name = FetchValue("cd","title","id",$cart[$i]['product_id']);				
				$price = $cart[$i]['price'];
				
				$p->add_field('item_name_'.($i+1), $product_name);
				$p->add_field('quantity_'.($i+1),$qty);
				$p->add_field('amount_'.($i+1),number_format($price,2));					
			}
			$p->add_field('image_url',"http://apostolicfaith.org.uk/images/logo.png");
		  $p->add_field('currency_code',CURRENCY_CODE);
			$p->add_field('upload', '1');
			$p->add_field('cmd', '_cart');
						
      $p->submit_paypal_post(); // submit the fields to paypal
      break;
   case 'success':      // Order was successful...
				$sql = "INSERT INTO member_product (member_id,price,total_price,qty,product,product_id,place_id) select member_id,price,total_price,qty,product,product_id,place_id from cart WHERE member_id='".$_SESSION['USER_ID']."'";
				$objDB->sql_query($sql);	
				$sql = "UPDATE member_product SET created = '".date('Y-m-d H:i:s')."' where member_id = '".$_SESSION['USER_ID']."'";
				$objDB->sql_query($sql);	
				$sql = "delete from cart where member_id = '".$_SESSION['USER_ID']."'";	
				$objDB->sql_query($sql);	
						
				header("Location: index.php?p=success&action=success&member=".$_SESSION['USER_ID']);				
				exit;
	      break;
   case 'cancel':       // Order was canceled...
	 			$sql = "delete from cart where member_id = '".$_SESSION['USER_ID']."'";	
				$objDB->sql_query($sql);	
	      header("Location: index.php?p=success&action=cancel&member=".$_SESSION['USER_ID']);exit;
	      break;
   case 'ipn':          // Paypal is calling page for IPN validation...
      if ($p->validate_ipn()) {
      }
      break;
 }     

?>