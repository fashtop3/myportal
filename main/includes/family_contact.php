<SCRIPT language="javascript">								
$(document).ready(function(){  		
	$("#frmCamp").validate();
});
function addRow(tableID) {	 
	var table = document.getElementById(tableID);			 
	var rowCount = table.rows.length;
	var row = table.insertRow(rowCount);			 
	var cell1 = row.insertCell(0);	
	cell1.align = "left";
	
	var norow = rowCount+1;
	cell1.innerHTML = '<input type="checkbox" name="check" align="absmiddle">'+norow+":";
	var cell2 = row.insertCell(1);									
	var sel = '<input ';
	sel += 'name="Name'+norow;
	sel += '" id="Name'+norow+'"';
	sel += ' class="field_bdr" title="*" validate="required:true" style="height:20px;width:250px;" /> <img src="images/plus.gif" align="absmiddle" style="cursor:pointer;" onclick="addRow(\'dataTable\')" alt="" title="Add Row" />';
	cell2.align='left';											
	cell2.innerHTML = sel;
										
	$(document).ready(function(){												 
		document.getElementById('Hide').innerHTML = '<input type="hidden" value="'+norow+'" name="TotalRow" id="TotalRow" />';
	});
}										 
function deleteRow(tableID) {
	try {		
		var table = document.getElementById(tableID);
		var rowCount = table.rows.length;
		var norow = rowCount;
		for(var i=0; i<rowCount; i++) {
				var row = table.rows[i];
				var chkbox = row.cells[0].childNodes[0];
				if(null != chkbox && true == chkbox.checked) {
						table.deleteRow(i);
						rowCount--;
						i--;
				}				
		}										
	}catch(e) {
			alert(e);
	}
}
</SCRIPT>

<form action="manage_campmetting.php" method="post" name="frmCamp" id="frmCamp">
  <input value="OverseasFamily" type="hidden" name="Process" id="Process" />
  <input type="hidden" value="<?php echo stripslashes(GetFieldData("donation","title","where id='".$_REQUEST['id']."'"));?>" name="Desc" />
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td height="28" align="left" valign="top"><h3><?php echo stripslashes(GetFieldData("donation","title","where id='".$_REQUEST['id']."'"));?></h3></td>
    </tr>
    <tr>
      <td align="left" valign="top"><span style="float:right"><a href="index.php?p=online_giving">Return to Camp Meeting</a></span><table width="100%" border="0" cellspacing="0" cellpadding="5">
          <tr>
            <td width="25%" align="left" valign="top">Your name</td>
            <td align="left" valign="top"><input type="text" title="*" validate="required:true" name="Name" id="Name" class="field_bdr" style="width:250px; height:20px;" />            
            </td>
          </tr>
          <tr>
            <td width="25%" align="left" valign="top">Your Email</td>
            <td align="left" valign="top"><input type="text" title="*" validate="required:true,email:true" name="Email" id="Email" class="field_bdr" style="width:250px; height:20px;" />            
            </td>
          </tr>
          <tr>
            <td width="25%" align="left" valign="top">Your Phone No.</td>
            <td align="left" valign="top"><input type="text" title="*" validate="required:true" name="Phone" id="Phone" class="field_bdr" style="width:250px; height:20px;" />            
            </td>
          </tr>
          <tr>
            <td width="25%" align="left" valign="top">Your Mobile No.</td>
            <td align="left" valign="top"><input type="text" name="Mobile" id="Mobile" class="field_bdr" style="width:250px; height:20px;" />            
            </td>
          </tr>
          <tr>
            <td width="25%" align="left" valign="top">Your Address</td>
            <td align="left" valign="top"><textarea name="Address" id="Address" class="field_bdr" style="width:250px; height:75px;"></textarea>
            </td>
          </tr>
          <tr>
            <td width="25%" align="left" valign="top">Your Country</td>
            <td align="left" valign="top"><input type="text" name="Country" id="Country" class="field_bdr" style="width:250px; height:20px;" />
            </td>
          </tr>
          <tr>
            <td width="25%" align="left" valign="top">Your Postcode</td>
            <td align="left" valign="top"><input type="text" name="Postcode" id="Postcode" class="field_bdr" style="width:250px; height:20px;" />
            </td>
          </tr>
        </table></td>
    </tr>
    <tr>
      <td align="left" valign="top" colspan="2"><br />
      Please enter fullname of the persons who want to come</td>
    </tr>
    <tr>
      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="5" id="dataTable">
          <tr>
            <td width="10%" align="left" style="font-size:14px;" valign="top">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1:</td>
            <td align="left" valign="top"><input type="text" title="*" validate="required:true" name="Name1" id="Name1" class="field_bdr" style="width:250px; height:20px;" />
              <img src="images/plus.gif" align="absmiddle" style="cursor:pointer;" onclick="addRow('dataTable')" alt="" title="Add Row" /> <span id="Hide">
              <input type="hidden" value="1" name="TotalRow" id="TotalRow" />
              </span> </td>
          </tr>
        </table></td>
    </tr>
    <tr>
    	<td align="left" valign="top">&nbsp;</td>
    </tr>
    <tr>
      <td align="left" valign="top"><input type="button" value="Delete" class="search_bg" style="cursor:pointer;" onclick="deleteRow('dataTable')" alt="" title="Delete Row" />
      	<input type="submit" value="Submit" class="search_bg" style="cursor:pointer;" />
      </td>
    </tr>
  </table>
</form>
