<script>
$(document).ready(function(){
	$("#frmAdmin").validate();
});
</script>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>TITHES</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top">To pay tithes online, simply type in the box your gross earnings.</td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><br />
            <form action="manage_cart.php" method="post" id="frmAdmin" name="frmAdmin">
              <input type="hidden" name="Product" value="Tithes" />
              <input type="hidden" name="url" value="" />
              <input type="hidden" name="action" value="add" />
              <input type="hidden" name="Amt" id="Amt" />
              <input type="hidden" value="<?php echo $_SESSION['PlaceID'.$userplc];?>" name="Place" id="Place" />
              Your Gross Earning: <?php echo htmlentities(CURRENCY_SIGN)?>
              <input type="text" name="Amt1" id="Amt1" size="10" class="gray_bdr" title=" *" validate="required:true,number:true" />
              <input type="button" style="padding-bottom:3px;height:25px;" value="Click to Calculate" onclick="showButton(document.getElementById('Amt1').value)" class="search_bg" onmouseover="this.style.cursor='pointer'" />
               <br />
              <br />
              <span id="pay"></span>
              <input type="submit" disabled="disabled" name="submit1" id="submit1" value="Pay Now" class="search_bg" style="height:25px;" onmouseover="this.style.cursor='pointer'" />
              
            </form>
            <script type="text/javascript">
				function showButton(value){
					if(value>0){
						var amt = (value*10)/100;						
						document.getElementById('pay').innerHTML = "Pay <?php echo htmlentities(CURRENCY_SIGN)?>"+amt;					
						document.getElementById('Amt').value = amt;
						document.frmAdmin.submit1.disabled = false;
					}
				}
			</script>
          </td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><a href="index.php?p=online_giving" style="font-size:12px;" class="orange_link">Product List</a></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
