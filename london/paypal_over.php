<?php
	session_start();
	
	require_once("utils/config.php");
	require_once("utils/dbclass.php");
	require_once("utils/functions.php");
	$objDB = new MySQLCN();
	
	$userplc = $_REQUEST['Place'];
	
require_once('paypal.class.php');  // include the class file
$p = new paypal_class;             // initiate an instance of the class

if(strtoupper(PAYPAL_TEST)=='YES')
	$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';//'https://www.paypal.com/cgi-bin/webscr';     // paypal url
else
	$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';//'https://www.paypal.com/cgi-bin/webscr';     // paypal url
            
$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

if (empty($_GET['action'])) $_GET['action'] = 'process';  

switch ($_GET['action']) {
   case 'process':      // Process and order...
      $p->add_field('business', PAYPAL_EMAIL);
      $p->add_field('return', $this_script.'?action=success&price='.$_REQUEST['price'].'&id='.$_REQUEST['id']);
      $p->add_field('cancel_return', $this_script.'?action=cancel&id='.$_REQUEST['id']);	  
      $p->add_field('notify_url', $this_script.'?action=ipn&id='.$_REQUEST['id']);
			$p->add_field('no_shipping',1);	
			
			$uid = GetFieldData("camp_payment","user_id","where id=".$_REQUEST['id']);
			$prod = GetFieldData("camp_user","description","where id=".$uid);
			
			$p->add_field('item_name', $prod);
			$p->add_field('quantity', "1");
			$p->add_field('amount',number_format($_REQUEST['price'],2));		
			$p->add_field('image_url',"http://apostolicfaith.org.uk/images/logo.png");
		  $p->add_field('currency_code',CURRENCY_CODE);
			//$p->add_field('upload', '1');
			//$p->add_field('cmd', '_cart');
						
      $p->submit_paypal_post(); // submit the fields to paypal
      break;
   case 'success':      // Order was successful...
	 		$uid = GetFieldData("camp_payment","user_id","where id=".$_REQUEST['id']);
			$prc = GetFieldData("camp_payment","total_price","where id=".$_REQUEST['id']);
			$user = FetchData("camp_user",array(),"where id=".$uid);
			$person = '<table width="100%" cellpadding="5" cellspacing="0" border="0">';
			$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
			for($i=0;$i<count($age);$i++){
				$person .= '<tr>';
				$person .= '<td colspan="6" align="left" valign="top"><hr><b>';
				$person .= GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'");
				$person .= '</b><hr></td>';
				$person .= '</tr>';
				$person .= '<tr>';
				$person .= '<td align="left" valign="top"><b>Name</b></td>';
				$person .= '<td align="left" valign="top"><b>Country</b></td>';
				$person .= '<td align="left" valign="top"><b>Required Visa?</b></td>';
				$person .= '<td align="left" valign="top"><b>Coming Date</b></td>';
				$person .= '<td align="left" valign="top"><b>Passport No.</b></td>';
				$person .= '<td align="left" valign="top"><b>Price</b></td>';
				$person .= '</tr>';
				$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
				for($j=0;$j<count($per);$j++){
					$person .= '<tr>';
					$person .= '<td align="left" valign="top">'.$per[$j]['name'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['station'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['visa'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['coming_date'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['passport_no'].'</td>';
					$person .= '<td align="left" valign="top">&pound;';
					if($age[$i]['price']>0)
						$person .= $age[$i]['price']/$age[$i]['person'];
					else
						$person .= $age[$i]['price'];
					$person .= '</td>';
					$person .= '</tr>';
				}
			}
			$person .= "</table>";
			if($_REQUEST['price']==$prc){
				$mode = 'Full Payment';
				$pending = '0';
				$submit = $prc;
			}
			else{
				$mode = 'Any Payment';
				$pending = $prc-$_REQUEST['price'];
				$submit = $_REQUEST['price'];
			}
			$Template="mail_templates/paypla_over.html";
			$TemplateVars=array(
								'RefNo'=>$user[0]['booking_ref_no'],
								'Name'=>$user[0]['name'],
								'Email'=>$user[0]['email'],
								'Phone'=>$user[0]['phone'],
								'Mobile'=>$user[0]['mobile'],
								'Address'=>$user[0]['address'],
								'Country'=>$user[0]['country'],
								'Postcode'=>$user[0]['postcode'],
								'Desc'=>$user[0]['description'],
								'Payment'=>'Paypal',
								'Mode'=>$mode,
								'Total'=>"&pound;".$prc,
								'Pending'=>"&pound;".$pending,
								'Submit'=>"&pound;".$submit,
								'Person'=>$person
								);
								
			if(count($result[0]['c_email'])>0){
				$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
			}else{
				$To = $Adminresult[0]['Email'];
			}
			$Subject = $user[0]['description']." request of Overseas Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
//			echo $flag;
			$To = $user[0]['email'];
			$Subject = $user[0]['description']." request of Overseas Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			
			$sql = "update camp_payment set ";
			$sql .= "payment_type = 'Paypal',";
			$sql .= "pending_payment = '".addslashes($pending)."',";
			$sql .= "payment_mode = '".addslashes($mode)."',";
			$sql .= "payment_submit = '".addslashes($submit)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$_REQUEST['id']."'";
			$objDB->sql_query($sql);
						
			header("Location: index.php?p=over_success&action=success&id=".$_REQUEST['id']);				
			exit;
			break;
   case 'cancel':       // Order was canceled...
	 			$uid = GetFieldData("camp_payment","user_id","where id=".$_REQUEST['id']);				
	 			$sql = "delete from camp_meeting_user where user_id = '".$uid."'";	
				$objDB->sql_query($sql);	
				$sql = "delete from camp_payment where user_id = '".$uid."'";	
				$objDB->sql_query($sql);	
				$sql = "delete from camp_request where user_id = '".$uid."'";	
				$objDB->sql_query($sql);	
				$sql = "delete from camp_user where id = '".$uid."'";	
				$objDB->sql_query($sql);	
	      header("Location: index.php?p=over_success&action=cancel");exit;
	      break;
   case 'ipn':          // Paypal is calling page for IPN validation...
      if ($p->validate_ipn()) {
      }
      break;
 }     

?>