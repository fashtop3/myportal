<?php
session_start();
	
	require_once("utils/config.php");
	require_once("utils/dbclass.php");
	require_once("utils/functions.php");
	$objDB = new MySQLCN();
	
	
	
	$name = loadVariable("Name", "");
	$email = loadVariable("Email", "");
	$place = loadVariable("Place", "");
	$placeid = loadVariable("Place", "");
	$prayer = loadVariable("Prayer", "");
	$Other = loadVariable("Other", "");
	
	// Insert Info
	$SQL = "INSERT prayer SET ";
	$SQL .= "p_name='".addslashes($name)."', ";		
	$SQL .= "p_email='".addslashes($email)."', ";
	//$SQL .= "p_place='".addslashes($place)."', ";
	$SQL .= "place_id='".addslashes($place)."', ";
	$SQL .= "type='".$_REQUEST['Type']."', ";
	$SQL .= "created='".date('Y-m-d H:i:s')."', ";
	//$SQL .= "p_place_other='".addslashes($Other)."', ";
	$SQL .= "p_prayer='".addslashes($prayer)."'";
	
	$PID = $objDB->adder($SQL);
	
	if(!empty($PID)){
		if($place=='')
			$result = $objDB->sql_query("select c_email from county");
		else
			$result = $objDB->sql_query("select c_email from county where c_id='".$place."'");
		//var_dump($result);
		$Adminresult = $objDB->sql_query("select Email from admin where UserID=1");
		
		/*if(count($result[0]['c_email'])>0){
			$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
		}else{
			$To = $Adminresult[0]['Email'];
		}*/
		$To = 'prayer@apostolicfaith.org.uk';
		$Subject = "Request a Prayer from Apostolic Faith Mission UK";
		$From = COMPANY_NAME;
		$prayerdesc = "Prayer for ".$_REQUEST['Type']."<br><br>Message: ".$_REQUEST['Prayer'];
		$Template="mail_templates/request_prayer.html";
		$places = GetFieldData("county","c_name","where c_id=".$place);
		$TemplateVars=array(
							'Name'=>$name,
							'Email'=>$email,
							'Place'=>$places,
							'Desc'=>$prayerdesc);
			
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
		if($flag){
			$_SESSION['SuccessMsg'] = 'Mail Sent Successfully!';
		}else{
			$_SESSION['ErrorMsg'] = 'Error while sending mail. Please try again.';
		}
		header('location: index.php?p=request_prayer');exit;
	}else{
		$_SESSION['ErrorMsg'] = 'Error while sending mail. Please try again.';
		header('location: index.php?p=request_prayer');exit;
	}
?>