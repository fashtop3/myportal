<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
</head>

<body><?php
$menu = FetchData("menu",array(),"");
?>
<table width="100%">
<tr>
    <td align="right" valign="top"><table width="220" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td height="10" align="left" valign="top"><img src="images/menu_c1.jpg" alt="" width="220" height="10"></td>
        </tr>
        <tr>
          <td align="left" valign="top" class="menu_rpt"><table width="206" border="0" align="center" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="top"><a id="home" href="index.php" class="nav_link"><?php echo $menu[0]['title']?></a></td>
              </tr>
              <tr>
                <td align="left" valign="top"><a id="aboutus" href="index.php?p=cms&id=about_us" class="nav_link"><?php echo $menu[1]['title']?></a></td>
              </tr>
              <tr>
                <td align="left" valign="top"><a href="index.php?p=camp_meeting" id="camp" class="nav_link">Camp Meeting 2010</a></td>
              </tr>              
              <tr>
                <td align="left" valign="top"><a id="resource" href="index.php?p=school" class="nav_link"><?php echo $menu[2]['title']?></a></td>
              </tr>
              <tr>
                <td align="left" id="resource_menu" valign="top"><table width="190" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=school" id="school" class="sub1nav_link">Sunday School</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=curriculum_list" id="curriculum" class="sub1nav_link">Curriculum</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=sermon_list" id="sermon" class="sub1nav_link">Sermon</a></td>
                    </tr>
                    <tr>
                      <td id="sermon_submenu" align="left" valign="top"><table width="175" border="0" align="right" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="left" valign="top"><a href="index.php?p=sermon_list" id="sermonlist" class="sub2nav_link">General Sermons</a></td>
                          </tr>
                          <tr>
                            <td align="left" valign="top"><a href="index.php?p=past_sermon_list" id="pastsermon" class="sub2nav_link">Echoes from the past</a></td>
                          </tr>
                      </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=tract_list" id="tract" class="sub1nav_link">Featured Tract</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=devotional" id="devotional" class="sub1nav_link">Devotional</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=higherway" id="higherway" class="sub1nav_link">Higher Way</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=offering" id="give" class="sub1nav_link">Online Payment</a></td>
                    </tr>
                    <?php $cat = FetchData("cd_category",array(),"group by type"); ?>
                    <tr>
                      <td align="left" valign="top"><a id="multimedia" href="index.php?p=cd_list&type=<?php echo $cat[0]['type']?>" class="sub1nav_link">Multimedia</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top" id="multimedia_submenu"><table width="175" border="0" align="right" cellpadding="0" cellspacing="0">
                          <?php
														for($i=0;$i<count($cat);$i++){ 
															$sub = FetchData("cd_category",array(),"where type = '".$cat[$i]['type']."'");
													?>
                          <tr>
                            <td align="left" valign="top"><a href="index.php?p=cd_list&type=<?php echo $cat[$i]['type']?>" id="<?php echo $cat[$i]['type']?>" class="sub2nav_link"><?php echo str_replace('MT','Music &amp; Tracks',str_replace('HM','Higher Way Magazine',$cat[$i]['type']))?></a></td>
                          </tr>
                          <?php if(count($sub)>0){ ?>
                          <tr>
                            <td align="left" valign="top" id="library_submenu"><table width="160" border="0" align="right" cellpadding="0" cellspacing="0">
                                <?php for($j=0;$j<count($sub);$j++){ ?>
                                <tr>
                                  <td align="left" valign="top"><a id="<?php echo $sub[$j]['id']?>" href="index.php?p=cd_list&type=<?php echo $cat[$i]['type']?>&id=<?php echo $sub[$j]['id']?>" class="sub3nav_link"><?php echo stripslashes($sub[$j]['title'])?></a></td>
                                </tr> 
                                <?php } ?>                               
                              </table></td>
                          </tr>
                          <?php } ?>
                           <?php } ?>
                        </table></td>
                    </tr>                    
                  </table></td>
              </tr>
              <tr>
                <td align="left" valign="top"><a href="index.php?p=news" id="news" class="nav_link"><?php echo $menu[3]['title']?></a></td>
              </tr>
              <tr>
                <td align="left" id="news_menu" valign="top"><table width="190" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=news" id="newslink" class="sub1nav_link">News</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a id="calendars" href="index.php?p=calendar" class="sub1nav_link">Calendar</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=event" id="event" class="sub1nav_link">Events</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="#" onclick="window.open('index.php?p=gallery','','width=1200,height=750,scrollbars=1')" id="gallery" class="sub1nav_link">Photo Gallery</a></td>
                    </tr>
              	</table></td>
              </tr>
              <tr>
                <td align="left" valign="top"><a href="<?php echo YOUTH_LINK?>" target="_blank" class="nav_link">Youth</a></td>
              </tr>
              <tr>
                <td align="left" valign="top"><a href="index.php?p=outreach" id="outreach" class="nav_link"><?php echo $menu[4]['title']?></a></td>
              </tr>
              <tr>
                <td align="left" valign="top"><a href="index.php?p=contact" id="contact" class="nav_link"><?php echo $menu[5]['title']?></a></td>
              </tr>
              <tr>
                <td align="left" id="contact_menu" valign="top"><table width="190" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="left" valign="top"><a href="index.php?p=request_prayer" id="prayer" class="sub1nav_link">Request Prayer</a></td>
                    </tr>
                    <?php /*?><tr>
                      <td align="left" valign="top"><a href="member/index.php?p=register" id="register" class="sub1nav_link">Register</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="member/index.php" id="login" class="sub1nav_link">Login</a></td>
                    </tr> <?php */?>                   
              	</table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="left" valign="top"><img src="images/menu_c2.jpg" alt="" width="220" height="10"></td>
        </tr>
      </table>
    </td></tr>
</table>
</body>
</html>
