<style type="text/css">
.alink{
	font-family:"Times New Roman", Times, serif;
	font-size:20px;	
	font-style:italic;
}
.alink:hover{
	color:#FF9900;
	text-decoration:none;
}
</style>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Devotional Archive</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top"><?php
						$sql = 'select * from devotional_archive group by ddate order by ddate desc';
						$objPaging = new paging($sql, "id",10);
						$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
						$arc = $objDB->select($objPaging->get_query());
						?>
            <table width="100%" cellpadding="5" cellspacing="1">
              <?php for($i=0;$i<count($arc);$i++){?>
              <tr>
                <td colspan="2" style="font-size:16px; color:#339999"><a href="index.php?p=devotional_detail&id=<?php echo $arc[$i]['id']?>" class="alink"><?php echo str_replace("`","'",stripslashes($arc[$i]['title']))?></a></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo $arc[$i]['ddate'];?></td>
              </tr>
              <tr>
                <td colspan="2"><?php echo str_replace('`','"',str_replace('<h3','<h2',str_replace('</h3>',"</h2>",str_replace("<br />",'"<br />',stripslashes($arc[$i]['description'])))));?></td>
              </tr>
              <tr>
                <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
              </tr>
              <?php } ?>
              <tr>
                <td colspan="2" align="right">Page: <?php $objPaging->show_paging() ?></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
