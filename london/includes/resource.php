<?php
$sql = "select * from curriculum_category where place_id = ".$userplc." order by id desc";
$cu = $objDB->select($sql);
$sql = "select * from news_gallery_category where place_id = ".$userplc." order by id desc limit 5";
$pg = $objDB->select($sql);

?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Resources</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top" style="padding-left:5px;">We have made the following resources available for your perusal and use</td>
        </tr>
        <tr>
          <td align="left" valign="top"><table width="100%">
              <tr>
                <td width="100%" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="100%" align="left" valign="top" class="blue_box_rpt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="25%" align="center" valign="top"><br />
                              <img src="images/curriculum.jpg" height="127" width="85" alt="Sunday School Curriculum" title="Sunday School Curriculum" /></td>
                            <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top"><br />
                                    <b>Sunday School Curriculum</b> </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><br />
                                    <?php
															for($i=0;$i<count($cu);$i++)
																echo "<div style='height:5px'><a class='blue_link' href='index.php?p=curriculum_list&id=".$cu[$i]['id']."'>".stripslashes($cu[$i]['title'])."</a></div><br>";
															?>
                                  </td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
              </tr>
              <tr>
                <td width="100%" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="100%" align="left" valign="top" class="blue_box_rpt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="25%" align="center" valign="top"><br />
                              <img src="images/sermon.jpg" height="127" width="85" alt="Sermons and Tracts" title="Sermons and Tracts" /></td>
                            <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top"><br />
                                    <b>Sermons and Tracts</b> </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><br />
                                    <div style='height:5px'><a class='blue_link' href='index.php?p=sermon_list'>General Sermons</a></div>
                                    <br>
                                    <div style='height:5px'><a class='blue_link' href='index.php?p=past_sermon_list'>Echoes from the past</a></div>
                                    <br>
                                    <div style='height:5px'><a class='blue_link' href='index.php?p=tract_list'>Tracts</a></div>
                                    <br>
                                  </td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
              </tr>
              <tr>
                <td width="100%" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="100%" align="left" valign="top" class="blue_box_rpt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="25%" align="center" valign="top"><br />
                              <img src="images/church_gallery.jpg" height="101" width="102" alt="Church Photo Gallery" title="Church Photo Gallery" /></td>
                            <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td align="left" valign="top"><br />
                                    <b>Photo Gallery</b> </td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><br />
                                    <?php
															for($i=0;$i<count($pg);$i++)
																echo "<div style='height:5px'><a class='blue_link' href='index.php?p=gallery&id=".$pg[$i]['id']."'>".stripslashes($pg[$i]['title'])."</a></div><br>";
															?>
                                  </td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>        
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
