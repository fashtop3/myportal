<script>
  $(document).ready(function(){
		$("#send").validate();	
  });
		
</script>
<?php
if($_REQUEST['Email']!=''){

	$to = $_REQUEST['to'];
	$from = $_REQUEST['Email'];
	$subject = "Higher Way Subscription";
	$message = "Please send my two-year subscription for the Higher Way magazine to the following address:\n\n";
	$message .= "Name: ".$_REQUEST['Name'];
	$message .= "\n\nAddress: ".$_REQUEST['Address'];	
	$message .= "\n\nCity: ".$_REQUEST['City'];
	$message .= "\n\nState: ".$_REQUEST['State'];
	$message .= "\n\nCountry: ".$_REQUEST['Country'];
	$message .= "\n\nZip Code: ".$_REQUEST['Zipcode'];

	$flag = @mail($to,$subject,$message,$from);
	if($flag)
		$_SESSION['Msg'] = "Mail send successfully!";
	else
		$_SESSION['Msg'] = "Error while sending mail. Please try again!";
		
	header("Location:index.php?p=higherway_email");
	exit;
}
?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Higher Way Subscription</h3></td>
  </tr>
  <tr>
  	<td>&nbsp;</td>
  </tr>
  <tr>
  	<td style="color:#FF0000; padding-left:5px;"><?php echo $_SESSION['Msg']; $_SESSION['Msg']=''?>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top">
    	<form action="" method="post" name="send" id="send">
      <input type="hidden" value="<?php echo EMAIL_ADDRESS?>" name="to" />
      <input type="hidden" value="" id="Msg" name="Msg" />
    	<table width="100%" border="0" align="left" cellpadding="5" cellspacing="1">
        <tr>
          <td width="15%">Name:</td>
          <td><input type="text" title=" *" validate="required: true" size="30" name="Name" class="gray_bdr" /></td>
        </tr>
        <tr>
          <td>Email:</td>
          <td><input type="text" title=" *" validate="required: true,email: true" size="30" name="Email" class="gray_bdr" /></td>
        </tr>
        <tr>
          <td>Address:</td>
          <td><input type="text" title=" *" validate="required: true" size="30" name="Address" class="gray_bdr" /></td>
        </tr>
        <tr>
          <td>City:</td>
          <td><input type="text" title=" *" validate="required: true" size="30" name="City" class="gray_bdr" /></td>
        </tr>
        <tr>
          <td>State:</td>
          <td><input type="text" title=" *" validate="required: true" size="30" name="State" class="gray_bdr" /></td>
        </tr>
        <tr>
          <td>Zip Code:</td>
          <td><input type="text" title=" *" validate="required: true" size="30" name="Zipcode" class="gray_bdr" /></td>
        </tr>
        <tr>
          <td>Country:</td>
          <td><input type="text" title=" *" validate="required: true" size="30" name="Country" class="gray_bdr" /></td>
        </tr>        
        <tr>
        	<td>&nbsp;</td>
          <td align="left"><input type="submit" value="Send" class="search_bg" style="width:75px; height:25px;" /></td>
        </tr>
      </table>
    </form>
    </td>
  </tr>
</table>