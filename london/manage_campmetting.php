<?php
	session_start();
	
	require_once("utils/config.php");
	require_once("utils/dbclass.php");
	require_once("utils/functions.php");
	$objDB = new MySQLCN();
		
	/* International Family */
	if($_REQUEST['Process']=='OverseasFamily'){
		
		$result = $objDB->sql_query("select c_email from county where c_id=".DEFAULT_PLACE);
		$Adminresult = $objDB->sql_query("select Email from admin where UserID=1");
									
		$prayerdesc = "";
		$per = array();
		for($i=1;$i<=$_REQUEST['TotalRow'];$i++)
		{
			if($_REQUEST['Name'.$i]!='')
				$per[] = $_REQUEST['Name'.$i];
		}				
		
		$person = @implode(", ",$per);
				
		$sql = "INSERT camp_user SET ";
		$sql .= "type = 'Overseas Family',";		
		$sql .= "name = '".addslashes($_REQUEST['Name'])."',";		
		$sql .= "user_id = '".$_SESSION['USERID']."',";
		$sql .= "email = '".addslashes($_REQUEST['Email'])."',";
		$sql .= "phone = '".addslashes($_REQUEST['Phone'])."',";
		$sql .= "mobile = '".addslashes($_REQUEST['Mobile'])."',";
		$sql .= "address = '".addslashes($_REQUEST['Address'])."',";
		$sql .= "country = '".addslashes($_REQUEST['Country'])."',";
		$sql .= "postcode = '".addslashes($_REQUEST['Postcode'])."',";
		$sql .= "description = '".addslashes($_REQUEST['Desc'])."',";
		$sql .= "udate = '".date('Y-m-d H:i:s')."',";
		$sql .= "persons = '".$person."'";
		$id = $objDB->adder($sql);
		
		$book_ref = $id.date('ymdHis');
		$sql = "update camp_user set booking_ref_no = '".$book_ref."' where id = ".$id;
		$objDB->sql_query($sql);
		
		$Template="mail_templates/camp.html";
		
		$TemplateVars=array(
							'RefNo'=>$book_ref,
							'Name'=>$_REQUEST['Name'],
							'Email'=>$_REQUEST['Email'],
							'Phone'=>$_REQUEST['Phone'],
							'Mobile'=>$_REQUEST['Mobile'],
							'Address'=>$_REQUEST['Address'],
							'Country'=>$_REQUEST['Country'],
							'Postcode'=>$_REQUEST['Postcode'],
							'Desc'=>$_REQUEST['Desc'],
							'Person'=>$person);
							
		/*if(count($result[0]['c_email'])>0){
			$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
		}else{
			$To = $Adminresult[0]['Email'];
		}*/
		$To = "camp@apostolicfaith.org.uk";
		$Subject = $_REQUEST['Desc']." request of International Family";
		$From = COMPANY_NAME;
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
		
		$To = $_REQUEST['Email'];
		$Subject = $_REQUEST['Desc']." request of International Family";
		$From = COMPANY_NAME;
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
		
		if($flag){
			$_SESSION['SuccessMsg'] = 'Your Booking referebce number sent to your mail id. Please check your mail.';
		}else{
			$_SESSION['ErrorMsg'] = 'Error while sending mail. Please try again.';
		}
		header('location: index.php?p=online_giving');exit;
	}
	
	/* UK Delegate */
	if($_REQUEST['Process']=='UK'){
		
		$result = $objDB->sql_query("select c_email from county where c_id=".DEFAULT_PLACE);
		$Adminresult = $objDB->sql_query("select Email from admin where UserID=1");
									
		$prayerdesc = "";
		if($_SESSION['id1']==''){	
			$sql = "INSERT camp_user SET ";
			$sql .= "type = 'UK DELEGATES',";
			$sql .= "name = '".addslashes($_REQUEST['Name'])."',";		
			$sql .= "user_id = '".$_SESSION['USERID']."',";
			$sql .= "email = '".addslashes($_REQUEST['Email'])."',";
			$sql .= "phone = '".addslashes($_REQUEST['Phone'])."',";
			$sql .= "mobile = '".addslashes($_REQUEST['Mobile'])."',";
			$sql .= "address = '".addslashes($_REQUEST['Address'])."',";
			$sql .= "country = '".addslashes($_REQUEST['Country'])."',";
			$sql .= "postcode = '".addslashes($_REQUEST['Postcode'])."',";
			$sql .= "description = '".addslashes(GetFieldData("donation","title","where id='".$_REQUEST['Desc']."'"))."',";
			$sql .= "udate = '".date('Y-m-d H:i:s')."'";
			
			$id = $objDB->adder($sql);
			
			$book_ref = $id.date('ymdHis');
			$sql = "update camp_user set booking_ref_no = '".$book_ref."' where id = ".$id;
			$objDB->sql_query($sql);
			
			$prc = 0;
			for($i=0;$i<count($_REQUEST['id']);$i++){
				$prc1 = 0;
				$prc1 = ($_REQUEST['Price'][$_REQUEST['id'][$i]]*$_REQUEST['Person'][$_REQUEST['id'][$i]]);
				$prc += $prc1;
				
				$sql = "insert camp_meeting_user set ";
				$sql .= "user_id = '".$id."',";
				$sql .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
				$sql .= "person = '".addslashes($_REQUEST['Person'][$_REQUEST['id'][$i]])."',";
				$sql .= "price = '".addslashes($prc1)."',";
				$sql .= "mudate = '".date('Y-m-d H:i:s')."'";
				$mid = $objDB->adder($sql);
				for($j=0;$j<$_REQUEST['Person'][$_REQUEST['id'][$i]];$j++){
					$sql1 = "insert camp_request set ";
					$sql1 .= "user_id = '".addslashes($id)."',";
					$sql1 .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
					$sql1 .= "meeting_id = '".addslashes($mid)."',";
					$sql1 .= "name = '".addslashes($_REQUEST['Name_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "station = '".addslashes($_REQUEST['Station_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "rdate = '".date('Y-m-d H:i:s')."'";
					$rid = $objDB->adder($sql1);
				}
			}
		
			$sql = "insert camp_payment set ";
			$sql .= "user_id = '".addslashes($id)."',";
			$sql .= "total_price = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$pid = $objDB->adder($sql);
			header("Location:index.php?p=camp_payment&id=".$pid);
			exit;
		}
		else
		{
			$uid = GetFieldData("camp_payment","user_id","where id=".$_SESSION['id1']);
			$sql = "UPDATE camp_user SET ";
			$sql .= "type = 'UK DELEGATES',";
			$sql .= "name = '".addslashes($_REQUEST['Name'])."',";		
			$sql .= "email = '".addslashes($_REQUEST['Email'])."',";
			$sql .= "phone = '".addslashes($_REQUEST['Phone'])."',";
			$sql .= "mobile = '".addslashes($_REQUEST['Mobile'])."',";
			$sql .= "address = '".addslashes($_REQUEST['Address'])."',";
			$sql .= "country = '".addslashes($_REQUEST['Country'])."',";
			$sql .= "postcode = '".addslashes($_REQUEST['Postcode'])."',";
			$sql .= "description = '".addslashes(GetFieldData("donation","title","where id='".$_REQUEST['Desc']."'"))."',";
			$sql .= "udate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$uid."'";
			$objDB->sql_query($sql);
			
			$id = $uid;
				
			$sql = "delete from camp_meeting_user where user_id = '".$uid."'";
			$objDB->sql_query($sql);
			$sql = "delete from camp_request where user_id = '".$uid."'";
			$objDB->sql_query($sql);			
				
			$prc = 0;
			for($i=0;$i<count($_REQUEST['id']);$i++){
				$prc1 = 0;
				$prc1 = ($_REQUEST['Price'][$_REQUEST['id'][$i]]*$_REQUEST['Person'][$_REQUEST['id'][$i]]);
				$prc += $prc1;
				
				$sql = "insert camp_meeting_user set ";
				$sql .= "user_id = '".addslashes($id)."',";
				$sql .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
				$sql .= "person = '".addslashes($_REQUEST['Person'][$_REQUEST['id'][$i]])."',";
				$sql .= "price = '".addslashes($prc1)."',";
				$sql .= "mudate = '".date('Y-m-d H:i:s')."'";
				$mid = $objDB->adder($sql);
				for($j=0;$j<$_REQUEST['Person'][$_REQUEST['id'][$i]];$j++){
					$sql1 = "insert camp_request set ";
					$sql1 .= "user_id = '".addslashes($id)."',";
					$sql1 .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
					$sql1 .= "meeting_id = '".addslashes($mid)."',";
					$sql1 .= "name = '".addslashes($_REQUEST['Name_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "station = '".addslashes($_REQUEST['Station_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "rdate = '".date('Y-m-d H:i:s')."'";
					$rid = $objDB->adder($sql1);
				}
			}			
			
			$sql = "update camp_payment set ";
			$sql .= "user_id = '".addslashes($id)."',";
			$sql .= "total_price = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";	
			$sql .= " where id = '".$_SESSION['id1']."'";		
			$objDB->sql_query($sql);
			header("Location:index.php?p=camp_payment&id=".$_SESSION['id1']);
			exit;
		}
	}
	
	/* UK Delegate Payment */
	if($_REQUEST['Process']=='Payment'){
		var_dump($_REQUEST);
		$prc = GetFieldData("camp_payment","total_price","where id=".$_REQUEST['id']);
		$uid = GetFieldData("camp_payment","user_id","where id=".$_REQUEST['id']);
		if($_REQUEST['Payment']=='Paypal'){
			if($_REQUEST['Mode']=='Full Payment')
				$price = $prc;
			else
				$price = $_REQUEST['Amt'];
			header("Location:paypal_uk.php?price=".$price."&id=".$_REQUEST['id']);
		}else if($_REQUEST['Payment']=='Later Payment'){
			
			$user = FetchData("camp_user",array(),"where id=".$uid);
			$person = '<table width="100%" cellpadding="5" cellspacing="0" border="0">';
			$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
			for($i=0;$i<count($age);$i++){
				$person .= '<tr>';
				$person .= '<td colspan="3" align="left" valign="top"><hr><b>';
				$person .= GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'");
				$person .= '</b><hr></td>';
				$person .= '</tr>';
				$person .= '<tr>';
				$person .= '<td align="left" valign="top"><b>Name</b></td>';
				$person .= '<td align="left" valign="top"><b>Station</b></td>';
				$person .= '<td align="left" valign="top"><b>Price</b></td>';
				$person .= '</tr>';
				$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
				for($j=0;$j<count($per);$j++){
					$person .= '<tr>';
					$person .= '<td align="left" valign="top">'.$per[$j]['name'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['station'].'</td>';
					$person .= '<td align="left" valign="top">&pound;';
					if($age[$i]['price']>0)
						$person .= $age[$i]['price']/$age[$i]['person'];
					else
						$person .= $age[$i]['price'];
					$person .= '</td>';
					$person .= '</tr>';
				}
			}
			$person .= "</table>";
			$Template="mail_templates/uk.html";
			$TemplateVars=array(
								'RefNo'=>$user[0]['booking_ref_no'],
								'Name'=>$user[0]['name'],
								'Email'=>$user[0]['email'],
								'Phone'=>$user[0]['phone'],
								'Mobile'=>$user[0]['mobile'],
								'Address'=>$user[0]['address'],
								'Country'=>$user[0]['country'],
								'Postcode'=>$user[0]['postcode'],
								'Desc'=>$user[0]['description'],
								'Payment'=>$_REQUEST['Payment'],
								'Total'=>"&pound;".$prc,
								'Person'=>$person
								);
								
/*			if(count($result[0]['c_email'])>0){
				$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
			}else{
				$To = $Adminresult[0]['Email'];
			}*/
			$To = "camp@apostolicfaith.org.uk";
			$Subject = $user[0]['description']." request of UK Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
			echo $flag;
			$To = $user[0]['email'];
			$Subject = $user[0]['description']." request of UK Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			
			$sql = "update camp_payment set ";
			$sql .= "payment_type = '".addslashes($_REQUEST['Payment'])."',";
			$sql .= "pending_payment = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$_REQUEST['id']."'";
			$objDB->sql_query($sql);
			header("Location: index.php?p=uk_success&payment=1&action=success&id=".$_REQUEST['id']);
		}
		else if($_REQUEST['Payment']=='Face to Face'){
			
			$user = FetchData("camp_user",array(),"where id=".$uid);
			$person = '<table width="100%" cellpadding="5" cellspacing="0" border="0">';
			$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
			for($i=0;$i<count($age);$i++){
				$person .= '<tr>';
				$person .= '<td colspan="3" align="left" valign="top"><hr><b>';
				$person .= GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'");
				$person .= '</b><hr></td>';
				$person .= '</tr>';
				$person .= '<tr>';
				$person .= '<td align="left" valign="top"><b>Name</b></td>';
				$person .= '<td align="left" valign="top"><b>Station</b></td>';
				$person .= '<td align="left" valign="top"><b>Price</b></td>';
				$person .= '</tr>';
				$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
				for($j=0;$j<count($per);$j++){
					$person .= '<tr>';
					$person .= '<td align="left" valign="top">'.$per[$j]['name'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['station'].'</td>';
					$person .= '<td align="left" valign="top">&pound;';
					if($age[$i]['price']>0)
						$person .= $age[$i]['price']/$age[$i]['person'];
					else
						$person .= $age[$i]['price'];
					$person .= '</td>';
					$person .= '</tr>';
				}
			}
			$person .= "</table>";
			$Template="mail_templates/uk.html";
			$TemplateVars=array(
								'RefNo'=>$user[0]['booking_ref_no'],
								'Name'=>$user[0]['name'],
								'Email'=>$user[0]['email'],
								'Phone'=>$user[0]['phone'],
								'Mobile'=>$user[0]['mobile'],
								'Address'=>$user[0]['address'],
								'Country'=>$user[0]['country'],
								'Postcode'=>$user[0]['postcode'],
								'Desc'=>$user[0]['description'],
								'Payment'=>$_REQUEST['Payment'],
								'Total'=>"&pound;".$prc,
								'Person'=>$person
								);
								
/*			if(count($result[0]['c_email'])>0){
				$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
			}else{
				$To = $Adminresult[0]['Email'];
			}*/
			$To = "camp@apostolicfaith.org.uk";
			$Subject = $user[0]['description']." request of UK Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
			echo $flag;
			$To = $user[0]['email'];
			$Subject = $user[0]['description']." request of UK Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			
			$sql = "update camp_payment set ";
			$sql .= "payment_type = '".addslashes($_REQUEST['Payment'])."',";
			$sql .= "pending_payment = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$_REQUEST['id']."'";
			$objDB->sql_query($sql);
			header("Location: index.php?p=uk_success&payment=1&action=success&id=".$_REQUEST['id']);
		}
	}
	
	/* International Delegate */
	if($_REQUEST['Process']=='OVER'){
		
		$result = $objDB->sql_query("select c_email from county where c_id=".DEFAULT_PLACE);
		$Adminresult = $objDB->sql_query("select Email from admin where UserID=1");
									
		$prayerdesc = "";
		if($_SESSION['id1']==''){	
			$sql = "INSERT camp_user SET ";
			$sql .= "type = 'OVERSEAS DELEGATES',";
			$sql .= "name = '".addslashes($_REQUEST['Name'])."',";	
			$sql .= "user_id = '".$_SESSION['USERID']."',";	
			$sql .= "email = '".addslashes($_REQUEST['Email'])."',";
			$sql .= "phone = '".addslashes($_REQUEST['Phone'])."',";
			$sql .= "mobile = '".addslashes($_REQUEST['Mobile'])."',";
			$sql .= "address = '".addslashes($_REQUEST['Address'])."',";
			$sql .= "country = '".addslashes($_REQUEST['Country'])."',";
			$sql .= "postcode = '".addslashes($_REQUEST['Postcode'])."',";
			$sql .= "description = '".addslashes(GetFieldData("donation","title","where id='".$_REQUEST['Desc']."'"))."',";
			$sql .= "udate = '".date('Y-m-d H:i:s')."'";
			$id = $objDB->adder($sql);
					
			$book_ref = $id.date('ymdHis');
			$sql = "update camp_user set booking_ref_no = '".$book_ref."' where id = ".$id;
			$objDB->sql_query($sql);
			
			$prc = 0;
			for($i=0;$i<count($_REQUEST['id']);$i++){
				$prc1 = 0;
				$prc1 = ($_REQUEST['Price'][$_REQUEST['id'][$i]]*$_REQUEST['Person'][$_REQUEST['id'][$i]]);
				$prc += $prc1;
				
				$sql = "insert camp_meeting_user set ";
				$sql .= "user_id = '".addslashes($id)."',";
				$sql .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
				$sql .= "person = '".addslashes($_REQUEST['Person'][$_REQUEST['id'][$i]])."',";
				$sql .= "price = '".addslashes($prc1)."',";
				$sql .= "mudate = '".date('Y-m-d H:i:s')."'";
				$mid = $objDB->adder($sql);
				for($j=0;$j<$_REQUEST['Person'][$_REQUEST['id'][$i]];$j++){
					$sql1 = "insert camp_request set ";
					$sql1 .= "user_id = '".addslashes($id)."',";
					$sql1 .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
					$sql1 .= "meeting_id = '".addslashes($mid)."',";
					$sql1 .= "name = '".addslashes($_REQUEST['Name_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "station = '".addslashes($_REQUEST['Country_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "visa = '".addslashes($_REQUEST['Visa_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "coming_date = '".addslashes($_REQUEST['Date_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "passport_no = '".addslashes($_REQUEST['Passport_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "rdate = '".date('Y-m-d H:i:s')."'";
					$rid = $objDB->adder($sql1);
				}
			}
			
			$sql = "insert camp_payment set ";
			$sql .= "user_id = '".addslashes($id)."',";
			$sql .= "total_price = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$pid = $objDB->adder($sql);
			header("Location:index.php?p=camp_over_payment&id=".$pid);
			exit;
		}
		else{
			$uid = GetFieldData("camp_payment","user_id","where id=".$_SESSION['id1']);
			$sql = "UPDATE camp_user SET ";
			$sql .= "type = 'OVERSEAS DELEGATES',";
			$sql .= "name = '".addslashes($_REQUEST['Name'])."',";		
			$sql .= "email = '".addslashes($_REQUEST['Email'])."',";
			$sql .= "phone = '".addslashes($_REQUEST['Phone'])."',";
			$sql .= "mobile = '".addslashes($_REQUEST['Mobile'])."',";
			$sql .= "address = '".addslashes($_REQUEST['Address'])."',";
			$sql .= "country = '".addslashes($_REQUEST['Country'])."',";
			$sql .= "postcode = '".addslashes($_REQUEST['Postcode'])."',";
			$sql .= "description = '".addslashes(GetFieldData("donation","title","where id='".$_REQUEST['Desc']."'"))."',";
			$sql .= "udate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$uid."'";
			$objDB->sql_query($sql);
			
			$id = $uid;
				
			$sql = "delete from camp_meeting_user where user_id = '".$uid."'";
			$objDB->sql_query($sql);
			$sql = "delete from camp_request where user_id = '".$uid."'";
			$objDB->sql_query($sql);			
				
			$prc = 0;
			for($i=0;$i<count($_REQUEST['id']);$i++){
				$prc1 = 0;
				$prc1 = ($_REQUEST['Price'][$_REQUEST['id'][$i]]*$_REQUEST['Person'][$_REQUEST['id'][$i]]);
				$prc += $prc1;
				
				$sql = "insert camp_meeting_user set ";
				$sql .= "user_id = '".addslashes($id)."',";
				$sql .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
				$sql .= "person = '".addslashes($_REQUEST['Person'][$_REQUEST['id'][$i]])."',";
				$sql .= "price = '".addslashes($prc1)."',";
				$sql .= "mudate = '".date('Y-m-d H:i:s')."'";
				$mid = $objDB->adder($sql);
				for($j=0;$j<$_REQUEST['Person'][$_REQUEST['id'][$i]];$j++){
					$sql1 = "insert camp_request set ";
					$sql1 .= "user_id = '".addslashes($id)."',";
					$sql1 .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
					$sql1 .= "meeting_id = '".addslashes($mid)."',";
					$sql1 .= "name = '".addslashes($_REQUEST['Name_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "station = '".addslashes($_REQUEST['Country_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "visa = '".addslashes($_REQUEST['Visa_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "coming_date = '".addslashes($_REQUEST['Date_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "passport_no = '".addslashes($_REQUEST['Passport_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "rdate = '".date('Y-m-d H:i:s')."'";
					$rid = $objDB->adder($sql1);
				}
			}			
			
			$sql = "update camp_payment set ";
			$sql .= "user_id = '".addslashes($id)."',";
			$sql .= "total_price = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";	
			$sql .= " where id = '".$_SESSION['id1']."'";		
			$objDB->sql_query($sql);
			header("Location:index.php?p=camp_over_payment&id=".$_SESSION['id1']);
			exit;
		}
	}
		
	/* Overseas Delegate Payment */
	if($_REQUEST['Process']=='OverPayment'){
		var_dump($_REQUEST);
		$prc = GetFieldData("camp_payment","total_price","where id=".$_REQUEST['id']);
		$uid = GetFieldData("camp_payment","user_id","where id=".$_REQUEST['id']);
		if($_REQUEST['Payment']=='Paypal'){
			if($_REQUEST['Mode']=='Full Payment')
				$price = $prc;
			else
				$price = $_REQUEST['Amt'];
			header("Location:paypal_over.php?price=".$price."&id=".$_REQUEST['id']);
		}else if($_REQUEST['Payment']=='Later Payment'){
			
			$user = FetchData("camp_user",array(),"where id=".$uid);
			$person = '<table width="100%" cellpadding="5" cellspacing="0" border="1">';
			$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
			for($i=0;$i<count($age);$i++){
				$person .= '<tr>';
				$person .= '<td colspan="6" align="left" valign="top"><hr><b>';
				$person .= GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'");
				$person .= '</b><hr></td>';
				$person .= '</tr>';
				$person .= '<tr>';
				$person .= '<td align="left" valign="top"><b>Name</b></td>';
				$person .= '<td align="left" valign="top"><b>Country</b></td>';
				$person .= '<td align="left" valign="top"><b>Required Visa?</b></td>';
				$person .= '<td align="left" valign="top"><b>Coming Date</b></td>';
				$person .= '<td align="left" valign="top"><b>Passport No.</b></td>';
				$person .= '<td align="left" valign="top"><b>Price</b></td>';
				$person .= '</tr>';
				$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
				for($j=0;$j<count($per);$j++){
					$person .= '<tr>';
					$person .= '<td align="left" valign="top">'.$per[$j]['name'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['station'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['visa'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['coming_date'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['passport_no'].'</td>';
					$person .= '<td align="left" valign="top">&pound;';
					if($age[$i]['price']>0)
						$person .= $age[$i]['price']/$age[$i]['person'];
					else
						$person .= $age[$i]['price'];
					$person .= '</td>';
					$person .= '</tr>';
				}
			}
			$person .= "</table>";
			$Template="mail_templates/over.html";
			$TemplateVars=array(
								'RefNo'=>$user[0]['booking_ref_no'],
								'Name'=>$user[0]['name'],
								'Email'=>$user[0]['email'],
								'Phone'=>$user[0]['phone'],
								'Mobile'=>$user[0]['mobile'],
								'Address'=>$user[0]['address'],
								'Country'=>$user[0]['country'],
								'Postcode'=>$user[0]['postcode'],
								'Desc'=>$user[0]['description'],
								'Payment'=>$_REQUEST['Payment'],
								'Total'=>"&pound;".$prc,
								'Person'=>$person
								);
								
/*			if(count($result[0]['c_email'])>0){
				$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
			}else{
				$To = $Adminresult[0]['Email'];
			}*/
			$To = "camp@apostolicfaith.org.uk";
			$Subject = $user[0]['description']." request of Overseas Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
			echo $flag;
			$To = $user[0]['email'];
			$Subject = $user[0]['description']." request of Overseas Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			
			$sql = "update camp_payment set ";
			$sql .= "payment_type = '".addslashes($_REQUEST['Payment'])."',";
			$sql .= "pending_payment = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$_REQUEST['id']."'";
			$objDB->sql_query($sql);
			header("Location: index.php?p=over_success&payment=1&action=success&id=".$_REQUEST['id']);
		}
		else if($_REQUEST['Payment']=='Face to Face'){
			
			$user = FetchData("camp_user",array(),"where id=".$uid);
			$person = '<table width="100%" cellpadding="5" cellspacing="0" border="1">';
			$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
			for($i=0;$i<count($age);$i++){
				$person .= '<tr>';
				$person .= '<td colspan="6" align="left" valign="top"><hr><b>';
				$person .= GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'");
				$person .= '</b><hr></td>';
				$person .= '</tr>';
				$person .= '<tr>';
				$person .= '<td align="left" valign="top"><b>Name</b></td>';
				$person .= '<td align="left" valign="top"><b>Country</b></td>';
				$person .= '<td align="left" valign="top"><b>Required Visa?</b></td>';
				$person .= '<td align="left" valign="top"><b>Coming Date</b></td>';
				$person .= '<td align="left" valign="top"><b>Passport No.</b></td>';
				$person .= '<td align="left" valign="top"><b>Price</b></td>';
				$person .= '</tr>';
				$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
				for($j=0;$j<count($per);$j++){
					$person .= '<tr>';
					$person .= '<td align="left" valign="top">'.$per[$j]['name'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['station'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['visa'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['coming_date'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['passport_no'].'</td>';
					$person .= '<td align="left" valign="top">&pound;';
					if($age[$i]['price']>0)
						$person .= $age[$i]['price']/$age[$i]['person'];
					else
						$person .= $age[$i]['price'];
					$person .= '</td>';
					$person .= '</tr>';
				}
			}
			$person .= "</table>";
			$Template="mail_templates/over.html";
			$TemplateVars=array(
								'RefNo'=>$user[0]['booking_ref_no'],
								'Name'=>$user[0]['name'],
								'Email'=>$user[0]['email'],
								'Phone'=>$user[0]['phone'],
								'Mobile'=>$user[0]['mobile'],
								'Address'=>$user[0]['address'],
								'Country'=>$user[0]['country'],
								'Postcode'=>$user[0]['postcode'],
								'Desc'=>$user[0]['description'],
								'Payment'=>$_REQUEST['Payment'],
								'Total'=>"&pound;".$prc,
								'Person'=>$person
								);
								
			/*if(count($result[0]['c_email'])>0){
				$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
			}else{
				$To = $Adminresult[0]['Email'];
			}*/
			$To = "camp@apostolicfaith.org.uk";
			$Subject = $user[0]['description']." request of Overseas Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
			echo $flag;
			$To = $user[0]['email'];
			$Subject = $user[0]['description']." request of Overseas Delegates";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			
			$sql = "update camp_payment set ";
			$sql .= "payment_type = '".addslashes($_REQUEST['Payment'])."',";
			$sql .= "pending_payment = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$_REQUEST['id']."'";
			$objDB->sql_query($sql);
			header("Location: index.php?p=over_success&payment=1&action=success&id=".$_REQUEST['id']);
		}
	}
	
	/* Further Payment */
	if($_REQUEST['Process']=='FinalPayment'){
		var_dump($_REQUEST);
		$prc = GetFieldData("camp_payment","pending_payment","where user_id=".$_REQUEST['id']);
		$uid = $_REQUEST['id'];
		if($_REQUEST['Payment']=='Paypal'){
			if($_REQUEST['Mode']=='Full Payment')
				$price = $prc;
			else
				$price = $_REQUEST['Amt'];
			header("Location:paypal_camp.php?price=".$price."&id=".$_REQUEST['id']);
		}else if($_REQUEST['Payment']=='Later Payment'){
			
			$user = FetchData("camp_user",array(),"where id=".$uid);
			
			$Template="mail_templates/final_payment.html";
			$TemplateVars=array(
								'RefNo'=>$user[0]['booking_ref_no'],
								'Name'=>$user[0]['name'],
								'Email'=>$user[0]['email'],
								'Phone'=>$user[0]['phone'],
								'Mobile'=>$user[0]['mobile'],
								'Address'=>$user[0]['address'],
								'Country'=>$user[0]['country'],
								'Postcode'=>$user[0]['postcode'],
								'Desc'=>$user[0]['description'],
								'Payment'=>$_REQUEST['Payment'],
								'Total'=>"&pound;".$prc,
								'Person'=>$person
								);
								
			/*if(count($result[0]['c_email'])>0){
				$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
			}else{
				$To = $Adminresult[0]['Email'];
			}*/
			$To = "camp@apostolicfaith.org.uk";
			$Subject = $user[0]['description']." Further Payment";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
			echo $flag;
			$To = $user[0]['email'];
			$Subject = $user[0]['description']." Further Payment";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			
			$pay = FetchData("camp_payment",array(),"where user_id = '".$_REQUEST['id']."'");
			$past = $pay[0]['past_payment'];
			$per = '';
			$pers = FetchData("camp_meeting_user",array(),"where user_id='".$_REQUEST['id']."'");
			for($i=0;$i<count($pers);$i++)
				$per .= GetFieldData('camp_meeting','title',"where id = '".$pers[$i]['camp_id']."'")." : <b>".$pers[$i]['person']." person</b><br>";
			if($pay[0]['payment_submit']=='')
				$subm = 0;
			else
				$subm = $pay[0]['payment_submit'];
			if($pay[0]['past_payment']==''){
				
				$past = '<table width="100%" cellpadding="5" cellspacing="0" border="0">
            	<tr>
              	<td width="30%" align="left" valign="top"><b>Total Person:</b></td>
                <td align="left" valign="top">'.$per.'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment type:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_type'].'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment mode:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_mode'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Total price:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['total_price'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Payment done till now:</b></td>
                <td align="left" valign="top">&pound;'.$subm.'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Pending payment:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['pending_payment'].'</td>
              </tr> 
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment date:</b></td>
                <td align="left" valign="top">'.$pay[0]['pdate'].'</td>
              </tr>              
            </table>';
			}
			else{
				$past .= ",".'<table width="100%" cellpadding="5" cellspacing="0" border="0">
            	<tr>
              	<td width="30%" align="left" valign="top"><b>Total Person:</b></td>
                <td align="left" valign="top">'.$per.'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment type:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_type'].'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment mode:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_mode'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Total price:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['total_price'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Payment done till now:</b></td>
                <td align="left" valign="top">&pound;'.$subm.'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Pending payment:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['pending_payment'].'</td>
              </tr> 
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment date:</b></td>
                <td align="left" valign="top">'.$pay[0]['pdate'].'</td>
              </tr>              
            </table>';
			}
			
			$sql = "update camp_payment set ";
			$sql .= "payment_type = '".addslashes($_REQUEST['Payment'])."',";
			$sql .= "payment_mode = '',";
			$sql .= "past_payment = '".addslashes($past)."',";
			$sql .= "pending_payment = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$pay[0]['id']."'";
			$objDB->sql_query($sql);
			header("Location: index.php?p=final_success&payment=1&action=success&id=".$_REQUEST['id']);
		}
		else if($_REQUEST['Payment']=='Face to Face'){
			
			$user = FetchData("camp_user",array(),"where id=".$uid);
						
			$Template="mail_templates/final_payment.html";
			$TemplateVars=array(
								'RefNo'=>$user[0]['booking_ref_no'],
								'Name'=>$user[0]['name'],
								'Email'=>$user[0]['email'],
								'Phone'=>$user[0]['phone'],
								'Mobile'=>$user[0]['mobile'],
								'Address'=>$user[0]['address'],
								'Country'=>$user[0]['country'],
								'Postcode'=>$user[0]['postcode'],
								'Desc'=>$user[0]['description'],
								'Payment'=>$_REQUEST['Payment'],
								'Total'=>"&pound;".$prc,
								'Person'=>$person
								);
								
			/*if(count($result[0]['c_email'])>0){
				$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
			}else{
				$To = $Adminresult[0]['Email'];
			}*/
			$To = "camp@apostolicfaith.org.uk";
			$Subject = $user[0]['description']." Further Payment";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
			echo $flag;
			$To = $user[0]['email'];
			$Subject = $user[0]['description']." Further Payment";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			
			$pay = FetchData("camp_payment",array(),"where user_id = '".$_REQUEST['id']."'");
			$past = $pay[0]['past_payment'];
			$per = '';
			$pers = FetchData("camp_meeting_user",array(),"where user_id='".$_REQUEST['id']."'");
			for($i=0;$i<count($pers);$i++)
				$per .= GetFieldData('camp_meeting','title',"where id = '".$pers[$i]['camp_id']."'")." : <b>".$pers[$i]['person']." person</b><br>";
			if($pay[0]['payment_submit']=='')
				$subm = 0;
			else
				$subm = $pay[0]['payment_submit'];
			if($pay[0]['past_payment']==''){
				$past = '<table width="100%" cellpadding="5" cellspacing="0" border="0">
            	<tr>
              	<td width="30%" align="left" valign="top"><b>Total Person:</b></td>
                <td align="left" valign="top">'.$per.'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment type:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_type'].'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment mode:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_mode'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Total price:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['total_price'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Payment done till now:</b></td>
                <td align="left" valign="top">&pound;'.$subm.'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Pending payment:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['pending_payment'].'</td>
              </tr> 
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment date:</b></td>
                <td align="left" valign="top">'.$pay[0]['pdate'].'</td>
              </tr>              
            </table>';
			}
			else{
				$past .= ",".'<table width="100%" cellpadding="5" cellspacing="0" border="0">
            	<tr>
              	<td width="30%" align="left" valign="top"><b>Total Person:</b></td>
                <td align="left" valign="top">'.$per.'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment type:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_type'].'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment mode:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_mode'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Total price:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['total_price'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Payment done till now:</b></td>
                <td align="left" valign="top">&pound;'.$subm.'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Pending payment:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['pending_payment'].'</td>
              </tr> 
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment date:</b></td>
                <td align="left" valign="top">'.$pay[0]['pdate'].'</td>
              </tr>              
            </table>';
			}
			
			$sql = "update camp_payment set ";
			$sql .= "payment_type = '".addslashes($_REQUEST['Payment'])."',";
			$sql .= "payment_mode = '',";
			$sql .= "past_payment = '".addslashes($past)."',";
			$sql .= "pending_payment = '".addslashes($prc)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where id = '".$pay[0]['id']."'";
			$objDB->sql_query($sql);
			header("Location: index.php?p=final_success&payment=1&action=success&id=".$_REQUEST['id']);
		}
	}
	
	/* Edit */
	if($_REQUEST['Process']=='EDITCAMP'){
		$uid = $_SESSION['Desc'];
		$cid = GetFieldData("camp_meeting_user","camp_id","where user_id=".$uid);
		$did = GetFieldData("camp_meeting","donation_id","where id=".$cid);
		$type = GetFieldData("camp_meeting","category","where id=".$cid);
		$result = $objDB->sql_query("select c_email from county where c_id=".DEFAULT_PLACE);
		$Adminresult = $objDB->sql_query("select Email from admin where UserID=1");
									
		$prayerdesc = "";		
		$uid = GetFieldData("camp_payment","user_id","where id=".$_SESSION['id1']);
		$sql = "UPDATE camp_user SET ";
		$sql .= "name = '".addslashes($_REQUEST['Name'])."',";		
		$sql .= "phone = '".addslashes($_REQUEST['Phone'])."',";
		$sql .= "mobile = '".addslashes($_REQUEST['Mobile'])."',";
		$sql .= "address = '".addslashes($_REQUEST['Address'])."',";
		$sql .= "country = '".addslashes($_REQUEST['Country'])."',";
		$sql .= "postcode = '".addslashes($_REQUEST['Postcode'])."',";
		$sql .= "modified = '".date('Y-m-d H:i:s')."'";
		$sql .= " where id = '".$uid."'";
		$objDB->sql_query($sql);
		
		$id = $uid;
			
		$sql = "delete from camp_meeting_user where user_id = '".$uid."'";
		$objDB->sql_query($sql);
		$sql = "delete from camp_request where user_id = '".$uid."'";
		$objDB->sql_query($sql);			
			
		if($type=='UK DELEGATES'){
			$prc = 0;
			for($i=0;$i<count($_REQUEST['id']);$i++){
				$prc1 = 0;
				$prc1 = ($_REQUEST['Price'][$_REQUEST['id'][$i]]*$_REQUEST['Person'][$_REQUEST['id'][$i]]);
				$prc += $prc1;
				
				$sql = "insert camp_meeting_user set ";
				$sql .= "user_id = '".addslashes($id)."',";
				$sql .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
				$sql .= "person = '".addslashes($_REQUEST['Person'][$_REQUEST['id'][$i]])."',";
				$sql .= "price = '".addslashes($prc1)."',";
				$sql .= "mudate = '".date('Y-m-d H:i:s')."'";
				$mid = $objDB->adder($sql);
				for($j=0;$j<$_REQUEST['Person'][$_REQUEST['id'][$i]];$j++){
					$sql1 = "insert camp_request set ";
					$sql1 .= "user_id = '".addslashes($id)."',";
					$sql1 .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
					$sql1 .= "meeting_id = '".addslashes($mid)."',";
					$sql1 .= "name = '".addslashes($_REQUEST['Name_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "station = '".addslashes($_REQUEST['Station_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "rdate = '".date('Y-m-d H:i:s')."'";
					$rid = $objDB->adder($sql1);
				}
			}		
		}else{
			$prc = 0;
			for($i=0;$i<count($_REQUEST['id']);$i++){
				$prc1 = 0;
				$prc1 = ($_REQUEST['Price'][$_REQUEST['id'][$i]]*$_REQUEST['Person'][$_REQUEST['id'][$i]]);
				$prc += $prc1;
				
				$sql = "insert camp_meeting_user set ";
				$sql .= "user_id = '".addslashes($id)."',";
				$sql .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
				$sql .= "person = '".addslashes($_REQUEST['Person'][$_REQUEST['id'][$i]])."',";
				$sql .= "price = '".addslashes($prc1)."',";
				$sql .= "mudate = '".date('Y-m-d H:i:s')."'";
				$mid = $objDB->adder($sql);
				for($j=0;$j<$_REQUEST['Person'][$_REQUEST['id'][$i]];$j++){
					$sql1 = "insert camp_request set ";
					$sql1 .= "user_id = '".addslashes($id)."',";
					$sql1 .= "camp_id = '".addslashes($_REQUEST['id'][$i])."',";
					$sql1 .= "meeting_id = '".addslashes($mid)."',";
					$sql1 .= "name = '".addslashes($_REQUEST['Name_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "station = '".addslashes($_REQUEST['Country_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "visa = '".addslashes($_REQUEST['Visa_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "coming_date = '".addslashes($_REQUEST['Date_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "passport_no = '".addslashes($_REQUEST['Passport_'.$_REQUEST['id'][$i].'_'.$j])."',";
					$sql1 .= "rdate = '".date('Y-m-d H:i:s')."'";
					$rid = $objDB->adder($sql1);
				}
			}
		}	
		
		$subm = GetFieldData("camp_payment","payment_submit","where id=".$_SESSION['id1']);
		$tpen = $prc - $subm;
		
		$sql = "update camp_payment set ";
		$sql .= "total_price = '".addslashes($prc)."',";
		$sql .= "pending_payment = '".addslashes($tpen)."'";
		$sql .= " where id = '".$_SESSION['id1']."'";		
		$objDB->sql_query($sql);
		
		if($type=='UK DELEGATES'){
			$person = '<table width="100%" cellpadding="5" cellspacing="0" border="1">';
			$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
			for($i=0;$i<count($age);$i++){
				$person .= '<tr>';
				$person .= '<td colspan="3" align="left" valign="top"><hr><b>';
				$person .= GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'");
				$person .= '</b><hr></td>';
				$person .= '</tr>';
				$person .= '<tr>';
				$person .= '<td align="left" valign="top"><b>Name</b></td>';
				$person .= '<td align="left" valign="top"><b>Station</b></td>';
				$person .= '<td align="left" valign="top"><b>Price</b></td>';
				$person .= '</tr>';
				$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
				for($j=0;$j<count($per);$j++){
					$person .= '<tr>';
					$person .= '<td align="left" valign="top">'.$per[$j]['name'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['station'].'</td>';
					$person .= '<td align="left" valign="top">&pound;';
					if($age[$i]['price']>0)
						$person .= $age[$i]['price']/$age[$i]['person'];
					else
						$person .= $age[$i]['price'];
					$person .= '</td>';
					$person .= '</tr>';
				}
			}
			$person .= "</table>";
		}else{
			$person = '<table width="100%" cellpadding="5" cellspacing="0" border="1">';
			$age = FetchData("camp_meeting_user",array(),"where user_id='".$uid."'");
			for($i=0;$i<count($age);$i++){
				$person .= '<tr>';
				$person .= '<td colspan="6" align="left" valign="top"><hr><b>';
				$person .= GetFieldData("camp_meeting","title","where id='".$age[$i]['camp_id']."'");
				$person .= '</b><hr></td>';
				$person .= '</tr>';
				$person .= '<tr>';
				$person .= '<td align="left" valign="top"><b>Name</b></td>';
				$person .= '<td align="left" valign="top"><b>Country</b></td>';
				$person .= '<td align="left" valign="top"><b>Required Visa?</b></td>';
				$person .= '<td align="left" valign="top"><b>Coming Date</b></td>';
				$person .= '<td align="left" valign="top"><b>Passport No.</b></td>';
				$person .= '<td align="left" valign="top"><b>Price</b></td>';
				$person .= '</tr>';
				$per = FetchData("camp_request",array(),"where user_id='".$uid."' and meeting_id = '".$age[$i]['id']."'");				
				for($j=0;$j<count($per);$j++){
					$person .= '<tr>';
					$person .= '<td align="left" valign="top">'.$per[$j]['name'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['station'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['visa'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['coming_date'].'</td>';
					$person .= '<td align="left" valign="top">'.$per[$j]['passport_no'].'</td>';
					$person .= '<td align="left" valign="top">&pound;';
					if($age[$i]['price']>0)
						$person .= $age[$i]['price']/$age[$i]['person'];
					else
						$person .= $age[$i]['price'];
					$person .= '</td>';
					$person .= '</tr>';
				}
			}
			$person .= "</table>";
		}
		$Template="mail_templates/final.html";
		$TemplateVars=array(
							'RefNo'=>$user[0]['booking_ref_no'],
							'Name'=>$user[0]['name'],
							'Email'=>$user[0]['email'],
							'Phone'=>$user[0]['phone'],
							'Mobile'=>$user[0]['mobile'],
							'Address'=>$user[0]['address'],
							'Country'=>$user[0]['country'],
							'Postcode'=>$user[0]['postcode'],
							'Desc'=>$user[0]['description'],							
							'Total'=>"&pound;".$prc,
							'Person'=>$person
							);
							
		/*if(count($result[0]['c_email'])>0){
			$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
		}else{
			$To = $Adminresult[0]['Email'];
		}*/
		$To = "camp@apostolicfaith.org.uk";
		$Subject = $user[0]['description']." request updated";
		$From = COMPANY_NAME;
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
		echo $flag;
		$To = $user[0]['email'];
		$Subject = $user[0]['description']." request updated";
		$From = COMPANY_NAME;
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
				
		header("Location:index.php?p=final_payment&id=".$_SESSION['Desc']);
		exit;
	}
?>