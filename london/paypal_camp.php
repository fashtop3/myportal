<?php
	session_start();
	
	require_once("utils/config.php");
	require_once("utils/dbclass.php");
	require_once("utils/functions.php");
	$objDB = new MySQLCN();
	
	$userplc = $_REQUEST['Place'];
	
require_once('paypal.class.php');  // include the class file
$p = new paypal_class;             // initiate an instance of the class

if(strtoupper(PAYPAL_TEST)=='YES')
	$p->paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr';//'https://www.paypal.com/cgi-bin/webscr';     // paypal url
else
	$p->paypal_url = 'https://www.paypal.com/cgi-bin/webscr';//'https://www.paypal.com/cgi-bin/webscr';     // paypal url
            
$this_script = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];

if (empty($_GET['action'])) $_GET['action'] = 'process';  

switch ($_GET['action']) {
   case 'process':      // Process and order...
      $p->add_field('business', PAYPAL_EMAIL);
      $p->add_field('return', $this_script.'?action=success&price='.$_REQUEST['price'].'&id='.$_REQUEST['id']);
      $p->add_field('cancel_return', $this_script.'?action=cancel&id='.$_REQUEST['id']);	  
      $p->add_field('notify_url', $this_script.'?action=ipn&id='.$_REQUEST['id']);
			$p->add_field('no_shipping',1);	
			
			$uid = $_REQUEST['id'];
			$prod = GetFieldData("camp_user","description","where id=".$uid);
			
			$p->add_field('item_name', $prod);
			$p->add_field('quantity', "1");
			$p->add_field('amount',number_format($_REQUEST['price'],2));		
			$p->add_field('image_url',"http://apostolicfaith.org.uk/images/logo.png");
		  $p->add_field('currency_code',CURRENCY_CODE);
			//$p->add_field('upload', '1');
			//$p->add_field('cmd', '_cart');
						
      $p->submit_paypal_post(); // submit the fields to paypal
      break;
   case 'success':      // Order was successful...
	 		$uid = $_REQUEST['id'];
			$prc = GetFieldData("camp_payment","pending_payment","where user_id=".$_REQUEST['id']);
			$subm = GetFieldData("camp_payment","payment_submit","where user_id=".$_REQUEST['id']);
			$totp = GetFieldData("camp_payment","total_price","where user_id=".$_REQUEST['id']);
			$user = FetchData("camp_user",array(),"where id=".$uid);			
			if($_REQUEST['price']==$prc){
				$mode = 'Full Payment';
				$pending = '0';
				if($totp==$prc)
					$submit = $prc;
				else
					$submit = $subm + $prc;
			}
			else{
				$mode = 'Any Payment';
				$pending = $prc-$_REQUEST['price'];
				$submit = $subm + $_REQUEST['price'];				
			}
			$Template="mail_templates/final_payment.html";
			$TemplateVars=array(
								'RefNo'=>$user[0]['booking_ref_no'],
								'Name'=>$user[0]['name'],
								'Email'=>$user[0]['email'],
								'Phone'=>$user[0]['phone'],
								'Mobile'=>$user[0]['mobile'],
								'Address'=>$user[0]['address'],
								'Country'=>$user[0]['country'],
								'Postcode'=>$user[0]['postcode'],
								'Desc'=>$user[0]['description'],
								'Payment'=>'Paypal',
								'Mode'=>$mode,
								'Total'=>"&pound;".$prc,
								'Pending'=>"&pound;".$pending,
								'Submit'=>"&pound;".$submit,
								'Person'=>$person
								);
								
			if(count($result[0]['c_email'])>0){
				$To = $result[0]['c_email'].", ".$Adminresult[0]['Email'];
			}else{
				$To = $Adminresult[0]['Email'];
			}
			$Subject = $user[0]['description']." Further Payment";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);			
			
			$To = $user[0]['email'];
			$Subject = $user[0]['description']." Further Payment";
			$From = COMPANY_NAME;
			$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
			
			$pay = FetchData("camp_payment",array(),"where user_id = '".$_REQUEST['id']."'");
			$past = $pay[0]['past_payment'];
			$per = '';
			$pers = FetchData("camp_meeting_user",array(),"where user_id='".$_REQUEST['id']."'");
			for($i=0;$i<count($pers);$i++)
				$per .= GetFieldData('camp_meeting','title',"where id = '".$pers[$i]['camp_id']."'")." : <b>".$pers[$i]['person']." person</b><br>";
			if($pay[0]['payment_submit']=='')
				$subm = 0;
			else
				$subm = $pay[0]['payment_submit'];
			if($pay[0]['past_payment']==''){
				$past = '<table width="100%" cellpadding="5" cellspacing="0" border="0">
            	<tr>
              	<td width="30%" align="left" valign="top"><b>Total Person:</b></td>
                <td align="left" valign="top">'.$per.'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment type:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_type'].'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment mode:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_mode'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Total price:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['total_price'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Payment done till now:</b></td>
                <td align="left" valign="top">&pound;'.$subm.'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Pending payment:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['pending_payment'].'</td>
              </tr> 
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment date:</b></td>
                <td align="left" valign="top">'.$pay[0]['pdate'].'</td>
              </tr>              
            </table>';
			}
			else{
				$past .= ",".'<table width="100%" cellpadding="5" cellspacing="0" border="0">
            	<tr>
              	<td width="30%" align="left" valign="top"><b>Total Person:</b></td>
                <td align="left" valign="top">'.$per.'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment type:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_type'].'</td>
              </tr>
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment mode:</b></td>
                <td align="left" valign="top">'.$pay[0]['payment_mode'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Total price:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['total_price'].'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Payment done till now:</b></td>
                <td align="left" valign="top">&pound;'.$subm.'</td>
              </tr>
              <tr>
              	<td width="30%" align="left" valign="top"><b>Pending payment:</b></td>
                <td align="left" valign="top">&pound;'.$pay[0]['pending_payment'].'</td>
              </tr> 
							<tr>
              	<td width="30%" align="left" valign="top"><b>Payment date:</b></td>
                <td align="left" valign="top">'.$pay[0]['pdate'].'</td>
              </tr>              
            </table>';
			}
			
			$sql = "update camp_payment set ";
			$sql .= "payment_type = 'Paypal',";
			$sql .= "past_payment = '".addslashes($past)."',";
			$sql .= "pending_payment = '".addslashes($pending)."',";
			$sql .= "payment_mode = '".addslashes($mode)."',";
			$sql .= "payment_submit = '".addslashes($submit)."',";
			$sql .= "pdate = '".date('Y-m-d H:i:s')."'";
			$sql .= " where user_id = '".$_REQUEST['id']."'";
			//echo $sql;
			$objDB->sql_query($sql);
						
			header("Location: index.php?p=final_success&action=success&id=".$_REQUEST['id']);				
			exit;
			break;
   case 'cancel':       // Order was canceled...
	 			$uid = $_REQUEST['id'];
	 			$sql = "delete from camp_meeting_user where user_id = '".$uid."'";	
				$objDB->sql_query($sql);	
				$sql = "delete from camp_payment where user_id = '".$uid."'";	
				$objDB->sql_query($sql);	
				$sql = "delete from camp_request where user_id = '".$uid."'";	
				$objDB->sql_query($sql);	
				$sql = "delete from camp_user where id = '".$uid."'";	
				$objDB->sql_query($sql);	
	      header("Location: index.php?p=final_success&action=cancel");exit;
	      break;
   case 'ipn':          // Paypal is calling page for IPN validation...
      if ($p->validate_ipn()) {
      }
      break;
 }     

?>