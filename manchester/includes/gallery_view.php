<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<title>Welcome to Apostolic Faith Mission</title>
<?php
$sql = "select * from news_gallery_category where place_id = ".$userplc." and id = ".$_REQUEST['id'];
$cat = $objDB->select($sql);
?>
<link rel="stylesheet" href="galleriffic/css/basic.css" type="text/css" />
<link rel="stylesheet" href="galleriffic/css/galleriffic-5.css" type="text/css" />
<link rel="stylesheet" href="galleriffic/css/black.css" type="text/css" />
<script type="text/javascript" src="galleriffic/js/jquery-1.3.2.js"></script>
<!--<script type="text/javascript" src="galleriffic/js/jquery.history.js"></script>-->
<script type="text/javascript" src="galleriffic/js/jquery.galleriffic.js"></script>
<script type="text/javascript" src="galleriffic/js/jquery.opacityrollover.js"></script>
<!-- We only want the thunbnails to display when javascript is disabled -->
<script type="text/javascript">
  document.write('<style>.noscript { display: none; }</style>');
</script>
<style type="text/css">
	.img{
		border:0;		
	}
</style>
</head>
<body>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top" style="padding:5px;"><h3>Photo Gallery</h3></td>
  </tr>
  <tr>
    <td style="font-size:16px; color:#CB9B13; font-weight:bold;"><?php echo htmlentities($cat[0]['title']);?></td>
  </tr>
  <tr>
    <td align="left" valign="top"><?php 
				$id = $_REQUEST['id'];
				if($id!=''){	
					$sql = "select * from news_gallery_images where gallery_id = '".$id."' and place_id = '".$userplc."'";
					$objPaging = new paging($sql, "id",16);
					$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
			
					$img = $objDB->select($sql);
				}
			?>
      <?php if(count($img)>0){ ?>
      <div id="page">
        <div id="container">
          <div class="navigation-container">
            <div id="thumbs" class="navigation"> <a class="pageLink prev" style="visibility: hidden;" href="#" title="Previous Page"></a>
              <ul class="thumbs noscript">
                <?php 
								for($i=0;$i<count($img);$i++){
									$imgPath = 'uploads/news/big/'.stripslashes($img[$i]['image']);
									$gwidth = 75;
									$gheight = 75;	
									$img1 = "resize.php?img={$imgPath}&width={$gwidth}&height={$gheight}&fit=inside&scale=any";		
										
								?>
                <li> <a class="thumb" name="leaf" href="<?php echo $imgPath?>" title="<?php echo stripslashes($img[$i]['title'])?>"> <img src="<?php echo $img1?>" alt="<?php echo stripslashes($img[$i]['title'])?>" /> </a>
                  <div class="caption">
                    <div class="image-title"><?php echo stripslashes($img[$i]['title'])?></div>
                    <div class="image-desc"><?php echo stripslashes($img[$i]['description'])?></div>                   
                  </div>
                </li>
                <?php } ?>
              </ul>
              <a class="pageLink next" style="visibility: hidden;" href="#" title="Next Page"></a> </div>
          </div>
          <div class="content">
            <div class="slideshow-container">
              <div id="controls" class="controls"></div>
              <div id="loading" class="loader"></div>
              <div id="slideshow" class="slideshow"></div>
            </div>
            <div id="caption" class="caption-container">
              <div class="photo-index"></div>
            </div>
          </div>
          <div style="clear: both;"></div>
        </div>
      </div>
      <script type="text/javascript">
			jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li, div.navigation a.pageLink').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     2500,
					numThumbs:                 10,
					preloadAhead:              10,
					enableTopPager:            false,
					enableBottomPager:         false,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              '<strong>Play Slideshow</strong>',
					pauseLinkText:             '<strong>Pause Slideshow</strong>',
					prevLinkText:              '<img src=\'images/fancy_nav_left.png\' class=\'img\'>',
					nextLinkText:              '<img src=\'images/fancy_nav_right.png\' class=\'img\'>',
					nextPageLinkText:          'Next &rsaquo;',
					prevPageLinkText:          '&lsaquo; Prev',
					enableHistory:             false,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);

						// Update the photo index display
						this.$captionContainer.find('div.photo-index')
							.html('Photo '+ (nextIndex+1) +' of '+ this.data.length);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						var prevPageLink = this.find('a.prev').css('visibility', 'hidden');
						var nextPageLink = this.find('a.next').css('visibility', 'hidden');
						
						// Show appropriate next / prev page links
						if (this.displayedPage > 0)
							prevPageLink.css('visibility', 'visible');

						var lastPage = this.getNumPages() - 1;
						if (this.displayedPage < lastPage)
							nextPageLink.css('visibility', 'visible');

						this.fadeTo('fast', 1.0);
					}
				});

				/**************** Event handlers for custom next / prev page links **********************/

				gallery.find('a.prev').click(function(e) {
					gallery.previousPage();
					e.preventDefault();
				});

				gallery.find('a.next').click(function(e) {
					gallery.nextPage();
					e.preventDefault();
				});

				/****************************************************************************************/

				/**** Functions to support integration of galleriffic with the jquery.history plugin ****/

				// PageLoad function
				// This function is called when:
				// 1. after calling $.historyInit();
				// 2. after calling $.historyLoad();
				// 3. after pushing "Go Back" button of a browser
				function pageload(hash) {
					// alert("pageload: " + hash);
					// hash doesn't contain the first # character.
					if(hash) {
						$.galleriffic.gotoImage(hash);
					} else {
						gallery.gotoIndex(0);
					}
				}

				// Initialize history plugin.
				// The callback is called at once by present location.hash. 
				//$.historyInit(pageload, "advanced.html");

				// set onlick event for buttons using the jQuery 1.3 live method
				$("a[rel='history']").live('click', function(e) {
					if (e.button != 0) return true;

					var hash = this.href;
					hash = hash.replace(/^.*#/, '');

					// moves to a new page. 
					// pageload is called at once. 
					// hash don't contain "#", "?"
					//$.historyLoad(hash);

					return false;
				});

				/****************************************************************************************/
			});
		</script>
      <?php }else{ ?>
      No image available&nbsp;
      <?php } ?>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" class="dot_line">&nbsp;</td>
  </tr>
</table>
</body>
</html>
