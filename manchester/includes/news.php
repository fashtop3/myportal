<?php
$sql = "select * from news where status='approved' and place_id = ".$userplc." and YEAR(created)=".date('Y')." order by id desc";
$objPaging = new paging($sql, "id",10);
$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
$news = $objDB->select($objPaging->get_query());
?>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>News</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top"><table width="100%">
              <tr>
                <td width="100%" valign="top"><?php if(count($news)>0){ ?>
                  <table width="100%">
                    <?php
										for($i=0;$i<count($news);$i++){
										?>
                    <tr>
                      <td width="100%"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="left" valign="top"><b><?php echo stripslashes($news[$i]['title']);?></b><br />
                              <?php echo stripslashes($news[$i]['short_description']);?>
                              <a href="index.php?p=news_detail&id=<?php echo $news[$i]['id'];?>" class="orange_12_link">Read More</a> </td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top" class="dot_line">&nbsp;</td>
                    </tr>
                    <?php } ?>
                  </table>
                  <span style="float:right"> Page: <?php echo $objPaging->show_paging() ?></span>
                  <?php }else echo "No News Available";?>
                </td>
              </tr>
              <tr>
                <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
              </tr>
              <tr>
                <td width="100%" align="left" valign="top" class="blue_box_rpt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td height="33" align="left" valign="middle"><h2>Related Links</h2></td>
                    </tr>
                    <tr>
                      <td style="padding:5px;" align="left"><img src="images/arrow_001.gif" alt="" /> <a href="index.php?p=news_archive" class="blue_link">Archive News</a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
