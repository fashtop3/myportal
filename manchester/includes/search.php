<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Search results for <br />"<?php echo wordwrap($_REQUEST['keyword'],50,"\n",1);?>"</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top"><h2 class="black_16">News</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from news where status='approved' and (title like '%".$k."%' or short_description like '%".$k."%' or full_description like '%".$k."%') and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=news_detail&id=".$news[$i]['id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Pages</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from cms where (title like '%".$k."%' or description like '%".$k."%') and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=cms&id=".$news[$i]['cms_key']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Contacts</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from contact where (title like '%".$k."%' or description like '%".$k."%' or address like '%".$k."%' or city like '%".$k."%' or state like '%".$k."%' or country like '%".$k."%' or pastor_name like '%".$k."%' or daytime_desc like '%".$k."%' or meeting like '%".$k."%' or category_id in(select id from contact_category where name like '%".$k."%' or description like '%".$k."%')) and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=contact&id=".$news[$i]['category_id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Sunday School</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from curriculum where (title like '%".$k."%' or full_description like '%".$k."%' or memory_verse like '%".$k."%' or lesson_no like '%".$k."%' or category_id in(select id from curriculum_category where title like '%".$k."%' or age_limit like '%".$k."%' or description like '%".$k."%')) and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=curriculum_detail&id=".$news[$i]['id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Outreach</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from outreach where (title like '%".$k."%' or description like '%".$k."%') and place_id = ".$userplc;				
				$news = $objDB->select($sql);		
				if(count($news)>0){		
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=outreach_detail&id=".$news[$i]['id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Photo Gallery</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from news_gallery_images where (title like '%".$k."%' or description like '%".$k."%' or gallery_id in (select id from news_gallery_category where title like '%".$k."%')) and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='#' onclick='window.open(\"index.php?p=gallery_view&id=".$news[$i]['gallery_id']."\",\"\",\"width=1200,height=750,scrollbars=1\")' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Reflection</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from reflection where status='approved' and (title like '%".$k."%' or description like '%".$k."%') and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=reflection_detail&id=".$news[$i]['id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Sermon</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from sermon where (title like '%".$k."%' or description like '%".$k."%' or occation in(select id from occation where title like '%".$k."%')) and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=sermon_detail&id=".$news[$i]['id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Testimonial</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from testimonial where status='approved' and (title like '%".$k."%' or description like '%".$k."%') and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=testimonial_detail&id=".$news[$i]['id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Tract</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from tract where (title like '%".$k."%' or description like '%".$k."%' or category_id in(select id from tract_category where title like '%".$k."%')) and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=tract_detail&id=".$news[$i]['id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
        <tr>
          <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
        </tr>
        <tr>
          <td align="left" valign="top"><h2 class="black_16">Doctrines</h2>
            
            <?php
				$k = $_REQUEST['keyword'];
				$sql = "select * from doctrines where (title like '%".$k."%' or description like '%".$k."%') and place_id = ".$userplc;
				$news = $objDB->select($sql);
				if(count($news)>0){
				for($i=0;$i<count($news);$i++){
					echo "<br><img src='images/arrow_001.gif' /> <a href='index.php?p=doctrines_detail&id=".$news[$i]['id']."' class='blue_link'>".stripslashes($news[$i]['title'])."</a><br>";
				}
				}else echo "No record found";
			?>
            <br />
          </td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
</table>
