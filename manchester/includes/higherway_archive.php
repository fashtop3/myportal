<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td height="28" align="left" valign="top"><h3>Higher Way Archive</h3></td>
  </tr>
  <tr>
    <td align="left" valign="top"><table width="100%" border="0" align="left" cellpadding="0" cellspacing="0">
        <tr>
          <td>The <em>Higher Way</em> has been the title of our flagship publication since 1995. It is a magazine designed to encourage Christian growth, with extra emphasis on the foundational Christian experiences of salvation, sanctification, and the baptism of the Holy Ghost. Articles include Biblical applications for Christian living, sermon excerpts, testimonies from members of our churches, and more. You can subscribe&nbsp;to the <em>Higher Way</em> <a href="index.php?p=higherway_email">electronically</a> or by <a href="http://www.apostolicfaith.org/Library/Order/tabid/115/CategoryID/37/Category2ID/37/List/0/Level/2/productid/334/Default.aspx">mail</a> free of charge, or <a href="index.php?p=contact">contact us</a> if you would like to receive the&nbsp;Braille edition.&nbsp;Cilck&nbsp;<a htmlelement="true" href="index.php?p=higherway">here</a>&nbsp;to view the current edition online.&nbsp;
            </p>
            <?php 
						/*$hg = file_get_contents("http://apostolicfaith.org/Library/Index/HigherWayArchive.aspx");
						$s1 = substr($hg,strpos($hg,"<!-- Start_Module_1464 -->"));
						$s2 = substr($s1,0,strpos($s1,"<!-- End_Module_1464 -->"));
						$s2 = substr($s2,strpos($s2,"<p>"));
						$s2 = str_replace('src="/','src="http://apostolicfaith.org/',$s2);
						echo $s2;*/
						$content = FetchData("cms",array(),"where id = 23");
						echo str_replace("`",'"',stripslashes($content[0]['description']));
						?>
            <!--<p><br>
              Click&nbsp;below to read an issue of the&nbsp;<em>Higher Way</em>.</p>
            <br />
            <p sizset="39" sizcache="0" style="text-align: center;">
            	<a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_103_1/hw_103_1">
              <img alt="" src="uploads/higherway/Web_HW 103-1_cover.jpg" border="0" width="120" height="155"></a>&nbsp;&nbsp;&nbsp;&nbsp;
              <a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_102_1/hw_102_1">
              <img alt="" src="uploads/higherway/Web_HW 102-1_cover.jpg" border="0" width="120" height="155"></a>&nbsp;&nbsp;&nbsp; 
              <a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_101_4/hw_101_4">
              <img alt="" src="uploads/higherway/Web_HW 101-4_cover.jpg" border="0" width="120" height="155"></a></p>
            <br />
            <p style="text-align: center;">
            <a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_101_3/hw_101_3">
            <img alt="" src="uploads/higherway/Web_HW 101-3_cover.jpg" border="0" width="120" height="154"></a>&nbsp;&nbsp;&nbsp;&nbsp;
            <a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_101_2/hw_101_2">
            <img alt="" src="uploads/higherway/Web_HW 101-2_cover.jpg" border="0" width="120" height="155"></a>&nbsp;&nbsp; &nbsp;
            <a onclick="window.open(this.href,'','resizable=yes,location=no,menubar=no,scrollbars=yes,status=no,toolbar=no,fullscreen=no,dependent=no,width=800,height=650,status'); return false" href="http://apostolicfaith.org/Portals/0/Website%20Assets/Library/Index/Higher%20Way%20%28Archive%29/HW_101_1/hw_101_1">
            <img alt="" src="uploads/higherway/Web_HW 101-1_cover.jpg" border="0" width="120" height="155"></a>-->
          </td>
        </tr>
      </table></td>
  </tr>
</table>
