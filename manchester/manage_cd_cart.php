<?php
	session_start();
	
	require_once("utils/config.php");
	require_once("utils/dbclass.php");
	require_once("utils/functions.php");
	$objDB = new MySQLCN();
	
	$userplc = $_REQUEST['Place'];
	if($userplc==0 || $userplc=='')
		$userplc = DEFAULT_PLACE;
	if($_SESSION['USERID'.$userplc]==''){
		if($_SESSION['USER_ID']=='')
			$_SESSION['USER_ID'] = date('dmyhis');
	}
	else
		$_SESSION['USER_ID'] = $_SESSION['USERID'.$userplc];
	
	var_dump($_REQUEST);	
	if($_REQUEST['action']=='add'){
		for($i=0;$i<$_REQUEST['total'];$i++){
			if($_REQUEST['id'.$i]!=''){
				$sql = "INSERT cart SET ";
				$sql .= "product = '".addslashes($_REQUEST['Product'])."',";
				$sql .= "product_id = '".addslashes($_REQUEST['id'.$i])."',";
				$sql .= "price = '".addslashes($_REQUEST['price'.$i])."',";		
				$sql .= "qty = '".addslashes($_REQUEST['Qty'.$i])."',";		
				$sql .= "total_price = '".addslashes(($_REQUEST['price'.$i]*$_REQUEST['Qty'.$i]))."',";
				$sql .= "member_id = '".$_SESSION['USER_ID']."',";
				$sql .= "created = '".date('Y-m-d H:i:s')."',";
				$sql .= "createdby = '".$_SESSION['USER_ID']."'";
				echo $sql;
				$objDB->sql_query($sql);
			}
		}
		$_SESSION['SuccessMsg'] = $_REQUEST['Product'].' added to cart';
		header("Location:index.php?p=cart");
		exit;
	}
	if($_REQUEST['action']=='update'){
		for($i=0;$i<$_REQUEST['Total'];$i++){
			if($_REQUEST['Quantity'.$i]!='')
				$prc = ($_REQUEST['Price'.$i]*$_REQUEST['Quantity'.$i]);
			else
				$prc = $_REQUEST['Price'.$i];
			$sql = "UPDATE cart SET ";
			$sql .= "qty = '".addslashes($_REQUEST['Quantity'.$i])."',";
			$sql .= "price = '".addslashes($_REQUEST['Price'.$i])."',";
			$sql .= "member_id = '".addslashes($_SESSION['USER_ID'])."',";
			$sql .= "description = '".addslashes($_REQUEST['Desc'])."',";
			$sql .= "total_price = '".addslashes($prc)."',";
			$sql .= "modified = '".date('Y-m-d H:i:s')."',";
			$sql .= "modifiedby = '".$_SESSION['USER_ID']."'";
			$sql .= " where id = ".$_REQUEST['ID'.$i];
			$objDB->sql_query($sql);
		}
		$_SESSION['SuccessMsg'] = 'Cart updated successfully';
		header("Location:index.php?p=cart");
		exit;
	}
	if($_REQUEST['action']=='delete'){
		$sql = "delete from cart where id = ".$_REQUEST['id'];
		$objDB->sql_query($sql);
		$_SESSION['SuccessMsg'] = 'Product deleted from cart';
		header("Location:index.php?p=cart");
		exit;
	}
?>