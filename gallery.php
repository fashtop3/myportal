<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon" />
<title>Welcome to Apostolic Faith Mission</title>
<link href="css/site.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="css/highslide.css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/highslide-with-html.js"></script>
<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function(){

	$(".btn-slide").click(function(){
		$("#panel").slideToggle("slow");
		$(this).toggleClass("active"); return false;
	});
	
	 
});
</script>	
	<script type="text/javascript" src="js/jquery-1.4.1.js"></script>
	<script type="text/javascript" src="js/jquery.mousewheel-3.0.2.pack.js"></script>
	<script type="text/javascript" src="js/jquery.fancybox-1.3.0.pack.js"></script>
<link rel="stylesheet" type="text/css" href="css/jquery.fancybox-1.3.0.css" media="screen" />
<script type="text/javascript">
		$(document).ready(function() {
			$(".example2").fancybox({
				'titleShow'     : true,
				'transitionIn'	: 'elastic',
				'transitionOut'	: 'elastic'
			});
	
     });
	</script>
<style type="text/css">
#panel {
	width:728px;
	background: #024770;
	background-image:url(images/c2.jpg);
	background-position:left bottom;
	background-repeat:no-repeat;
	color:#0033CC;
	display: none;
}
</style>
<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">

</head>
<body>
<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td align="left" valign="top" class="top_rpt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="189" align="left" valign="top"><a href="index.html"><img src="images/logo.jpg" alt="Apostolic Faith  Mission " width="189" height="122"></a></td>
          <td width="774" align="left" valign="top"><table width="100%" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td align="left" valign="top"><table width="225" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="74%" height="8" align="right" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                      <td width="26%" height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                    </tr>
                    <tr>
                      <td height="25" align="right" valign="top"><input name="textfield" type="text" class="field_bdr" id="textfield" style=" width:154px; height:12px;">                      </td>
                      <td align="right" valign="top"><input name="button" type="submit" class="login_bg" id="button" value="Search">                      </td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td align="left" valign="top"><table width="368" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="102" height="25" align="left" valign="top"><input name="textfield2" type="text" class="field_bdr" id="textfield2" style=" width:90px; height:12px;" value="User Name :"></td>
                      <td width="102" align="left" valign="top"><input name="textfield3" type="text" class="field_bdr" id="textfield3" style=" width:90px; height:12px;" value="Password :"></td>
                      <td width="49" align="left" valign="top"><input name="button2" type="submit" class="login_bg" id="button2" value="Login"></td>
                      <td width="115" align="right" valign="top" class="white_12"><a href="#" class="white_12_link">Register</a> | <a href="#" class="white_12_link">Forgotton?</a></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td align="left" valign="top"><table width="368" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="33" align="left" valign="top"><img src="images/visitor_ic.gif" alt="" width="24" height="21"></td>
                      <td width="147" align="left" valign="middle"><a href="#" class="white_12_link">Information for visitors</a></td>
                      <td width="25" align="left" valign="top"><img src="images/youth_ic.gif" alt="" width="15" height="21"></td>
                      <td width="51" align="left" valign="middle"><a href="<?php echo YOUTH_LINK?>" class="white_12_link">Youth</a></td>
                      <td width="33" align="right" valign="top"><img src="images/bible_ic.gif" alt="" width="27" height="19"></td>
                      <td width="79" align="right" valign="middle"><a href="#" class="white_12_link">Bible Forum</a></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td align="left" valign="top"><table width="245" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td height="19" align="left" valign="top">&nbsp;</td>
                      <td align="left" valign="top">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="left" valign="top" class="blue_12">Songs and Praise</td>
                      <td align="right" valign="top"><iframe src="prayer/mp3.html" width="120" height="16" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" name="n"></iframe></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
          <td width="7" align="right" valign="top"><img src="images/top_c1.jpg" alt="" width="7" height="121"></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#FFFFFF"><table width="955" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td width="735" align="left" valign="top"><table width="735" border="0" align="left" cellpadding="0" cellspacing="0">
              <tr>
                <td height="175" align="left" valign="top" class="inner_bnr_bg"><img src="images/about_hdr.jpg" alt="" width="728" height="163"></td>
              </tr>
              
              <tr>
                <td align="left" valign="top"><table width="728" border="0" align="left" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="left" valign="top"><table width="728" border="0" align="left" cellpadding="0" cellspacing="0">
                          <tr>
                            <td align="left" valign="bottom"><img src="images/c1.jpg" alt="" width="728" height="9"></td>
                          </tr>
                          <tr>
                            <td align="left" valign="bottom" bgcolor="#024770"><table width="700" border="0" align="center" cellpadding="0" cellspacing="0">
                                <tr>
                                  <td width="185" height="25" align="left" valign="top"><img src="images/life_ex.jpg" alt="" width="119" height="17"></td>
                                  <td width="182" align="left" valign="top"><img src="images/refle.jpg" alt="" width="74" height="17"></td>
                                  <td width="180" align="left" valign="top"><img src="images/feature.jpg" alt="" width="108" height="17"></td>
                                  <td width="169" align="left" valign="top"><img src="images/higher.jpg" alt="" width="85" height="17"></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><div id="panel">
                          <table width="728" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td align="left" valign="top" bgcolor="#024770"><table width="700" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#024770">
                                  <tr>
                                    <td width="179" align="left" valign="top"><table width="165" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td height="77" align="left" valign="top"><a href="#"><img src="images/life_img.jpg" alt="" width="161" height="63" class="white_bdr"></a></td>
                                        </tr>
                                        <tr>
                                          <td height="65" align="left" valign="top" class="white_12"><span class="orange_12"><strong>Lost But Found</strong></span><br>
                                            I am sincerely thankful to God for Jesus cares deeply about us and...</td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="top"><a href="#" class="orange_12_link">Read More ››</a></td>
                                        </tr>
                                      </table></td>
                                    <td width="179" align="left" valign="top"><table width="165" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td height="77" align="left" valign="top"><a href="#"><img src="images/refle_img.jpg" alt="" width="161" height="63" class="white_bdr"></a></td>
                                        </tr>
                                        <tr>
                                          <td height="65" align="left" valign="top" class="white_12"><span class="orange_12"><strong>The Solution to All
                                            Problems Pt (1)</strong></span><br>
                                            “These all died in faith,”</td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="top"><a href="#" class="orange_12_link">Read More ››</a></td>
                                        </tr>
                                      </table></td>
                                    <td width="167" align="left" valign="top"><table width="165" border="0" align="left" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td height="77" align="left" valign="top"><a href="#"><img src="images/feature_img.jpg" alt="" width="161" height="63" class="white_bdr"></a></td>
                                        </tr>
                                        <tr>
                                          <td height="65" align="left" valign="top" class="white_12"><span class="orange_12"><strong>Communicati on with God</strong></span><br>
                                            An extraordinary source of strength and power is </td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="top"><a href="#" class="orange_12_link">Read More ››</a></td>
                                        </tr>
                                      </table></td>
                                    <td width="175" align="left" valign="top"><table width="165" border="0" align="right" cellpadding="0" cellspacing="0">
                                        <tr>
                                          <td height="77" align="left" valign="top"><a href="#"><img src="images/higher_img.jpg" alt="" width="161" height="63" class="white_bdr"></a></td>
                                        </tr>
                                        <tr>
                                          <td height="65" align="left" valign="top" class="white_12"><span class="orange_12"><strong>Higher Way </strong></span><br>
                                            Looking for some inspiration? Read the latest issue of the Higher Way Magazine</td>
                                        </tr>
                                        <tr>
                                          <td align="right" valign="top"><a href="#" class="orange_12_link">Read More ››</a></td>
                                        </tr>
                                      </table></td>
                                  </tr>
                                </table></td>
                            </tr>
                            <tr>
                              <td align="left" valign="top">&nbsp;</td>
                            </tr>
                          </table>
                        </div>
                        <p class="slide"><a href="#" class="btn-slide">Click</a></p></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
              </tr>
              <tr>
                <td align="left" valign="top"><table width="717" border="0" cellspacing="0" cellpadding="0">
                  <tr>
                    <td height="28" align="left" valign="top"><h3>Photo Gallery</h3></td>
                    </tr>
                  <tr>
                    <td align="left" valign="top">&nbsp;</td>
                    </tr>
                  <tr>
                    <td align="left" valign="top"><div id="Accordion1" class="Accordion" tabindex="0">
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">2010 Easter Concert Gallery</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="gallery_pic/g_t1.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="gallery_pic/g_t2.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="gallery_pic/g_t3.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="gallery_pic/g_t4.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="gallery_pic/g_t5.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="gallery_pic/g_t6.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="gallery_pic/g_t7.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table>
</div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 1</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b9.jpg" title=""><img src="gallery_pic/g_t9.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b10.jpg" title=""><img src="gallery_pic/g_t10.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b11.jpg" title=""><img src="gallery_pic/g_t11.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b12.jpg" title=""><img src="gallery_pic/g_t12.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b9.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b10.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b11.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b12.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b13.jpg" title=""><img src="gallery_pic/g_t13.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b14.jpg" title=""><img src="gallery_pic/g_t14.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b15.jpg" title=""><img src="gallery_pic/g_t15.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 2</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="gallery_pic/g_t1.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="gallery_pic/g_t2.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="gallery_pic/g_t3.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="gallery_pic/g_t4.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="gallery_pic/g_t5.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="gallery_pic/g_t6.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="gallery_pic/g_t7.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 3</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b9.jpg" title=""><img src="gallery_pic/g_t9.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b10.jpg" title=""><img src="gallery_pic/g_t10.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b11.jpg" title=""><img src="gallery_pic/g_t11.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b12.jpg" title=""><img src="gallery_pic/g_t12.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b9.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b10.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b11.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b12.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b13.jpg" title=""><img src="gallery_pic/g_t13.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b14.jpg" title=""><img src="gallery_pic/g_t14.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b15.jpg" title=""><img src="gallery_pic/g_t15.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 4</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="gallery_pic/g_t1.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="gallery_pic/g_t2.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="gallery_pic/g_t3.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="gallery_pic/g_t4.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="gallery_pic/g_t5.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="gallery_pic/g_t6.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="gallery_pic/g_t7.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                      <div class="AccordionPanel">
                        <div class="AccordionPanelTab"><img src="images/orange_arrow.gif" alt="" hspace="5">Gallery Title 5</div>
                        <div class="AccordionPanelContent"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
    <td align="left" valign="top">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="gallery_pic/g_t1.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="gallery_pic/g_t2.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="gallery_pic/g_t3.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="gallery_pic/g_t4.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b1.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b2.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b3.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b4.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
  <tr>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="gallery_pic/g_t5.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="gallery_pic/g_t6.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="gallery_pic/g_t7.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="gallery_pic/g_t8.jpg" alt="" width="135" height="94" class="blue_bdr"></a></td>
  </tr>
  <tr>
    <td height="32" align="left" valign="top"><a class="example2" href="gallery_pic/g_b5.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b6.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b7.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
    <td align="left" valign="top"><a class="example2" href="gallery_pic/g_b8.jpg" title=""><img src="images/zoom.jpg" width="53" height="20" vspace="3"></a></td>
  </tr>
</table></div>
                      </div>
                    </div></td>
                  </tr>
                  <tr>
                    <td align="left" valign="top">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
          </table></td>
          <td width="220" align="left" valign="top"><table width="220" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td height="245" align="right" valign="top" class="menu_bg"><table width="96%" border="0" align="center" cellpadding="0" cellspacing="1">
                    <tr>
                      <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="index.html" class="menu_link">Home</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="about.html" class="menu_link">About Us</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="#" class="menu_link">Resources</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="#" class="menu_link">News and events</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="#" class="menu_link">Outreach</a></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><a href="#" class="menu_link">Contact us</a></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td height="28" align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" valign="top"><table width="97%" border="0" align="right" cellpadding="0" cellspacing="0">
                    <tr>
                      <td align="left" valign="top"><span class="times_italic" style="padding-left:10px;">“. . . the word of the LORD hath come unto me, and I have spoken unto you, rising early and speaking; but ye have not hearkened.&quot;</span></td>
                    </tr>
                    <tr>
                      <td height="50" align="right" valign="top" class="times_orange">- Jeremiah 25:3</td>
                    </tr>
                    <tr>
                      <td align="right" valign="top"><a href="#" class="times_blue_link">Devotional</a></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td height="32" align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
              </tr>
              <tr>
                <td align="left" valign="top"><a href="memory_verse.html" onClick="return hs.htmlExpand(this, { objectType: 'ajax'} )"><img src="images/get_verse.jpg" alt="" width="220" height="51"></a></td>
              </tr>
              <tr>
                <td align="left" valign="top">&nbsp;</td>
              </tr>
              <tr>
                <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="left" valign="top" class="btm_gray_bdr"><span class="gray_bg">News</span></td>
                    </tr>
                    <tr>
                      <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><span class="blue_12"><strong>Roseburg Special Meetings</strong></span><br>
                        <span class="orange_light">03/15/2010</span><br>
                        Gary Bertram, from Tacoma, Washington, was the guest speaker for the special meetings at the Apostolic Faith Church in Roseburg, Oregon, and thetheme for the weekend wa&quot;Surreder.&quot;</td>
                    </tr>
                    <tr>
                      <td height="20" align="right" valign="bottom"><a href="#" class="orange_12_link">Read More ››</a></td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td align="left" valign="top" class="btm_gray_bdr"><span class="gray_bg">Events</span></td>
                    </tr>
                    <tr>
                      <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top" class="black_11">View our upcoming events below or view our entire Calendar of events <a href="#" class="blue_11_link">here</a>.</td>
                    </tr>
                    <tr>
                      <td height="8" align="left" valign="top"><img src="images/spacer.gif" alt="" width="1" height="1"></td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="53" align="left" valign="middle" class="date_bg">24<br>
                              April</td>
                            <td width="167" align="left" valign="top"><table width="97%" border="0" align="right" cellpadding="0" cellspacing="2">
                                <tr>
                                  <td align="left" valign="top" class="orange_11">4/21/2010 - 4/25/2010</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><a href="#" class="blue_11_link"><strong>Roddickton/Bide Arm &amp; Englee Special Meetings</strong></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="53" align="left" valign="middle" class="date_bg">24<br>
                              April</td>
                            <td width="167" align="left" valign="top"><table width="97%" border="0" align="right" cellpadding="0" cellspacing="2">
                                <tr>
                                  <td align="left" valign="top" class="orange_11">4/21/2010 - 4/25/2010</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><a href="#" class="blue_11_link"><strong>Roddickton/Bide Arm &amp; Englee Special Meetings</strong></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td height="20" align="left" valign="top" class="dot_line">&nbsp;</td>
                    </tr>
                    <tr>
                      <td align="left" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="53" align="left" valign="middle" class="date_bg">24<br>
                              April</td>
                            <td width="167" align="left" valign="top"><table width="97%" border="0" align="right" cellpadding="0" cellspacing="2">
                                <tr>
                                  <td align="left" valign="top" class="orange_11">4/21/2010 - 4/25/2010</td>
                                </tr>
                                <tr>
                                  <td align="left" valign="top"><a href="#" class="blue_11_link"><strong>Roddickton/Bide Arm &amp; Englee Special Meetings</strong></a></td>
                                </tr>
                              </table></td>
                          </tr>
                        </table></td>
                    </tr>
                    <tr>
                      <td height="20" align="right" valign="bottom"><a href="#" class="orange_12_link">View More ››</a></td>
                    </tr>
                  </table></td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td align="left" valign="top" bgcolor="#FFFFFF" class="btm_img"><table width="955" border="0" align="center" cellpadding="0" cellspacing="0">
        <tr>
          <td align="left" valign="top">&nbsp;</td>
        </tr>
        <tr>
          <td height="33" align="center" valign="middle" class="bdr_t_b"><a href="index.html" class="footer_12_link">Home</a> | <a href="about.html" class="footer_12_link">About Us</a> | <a href="#" class="footer_12_link">Resources</a> | <a href="#" class="footer_12_link">News &amp; Events</a> | <a href="#" class="footer_12_link">Outreach</a> | <a href="#" class="footer_12_link">Contact Us</a></td>
        </tr>
        <tr>
          <td height="22" align="left" valign="bottom"><table width="97%" border="0" align="right" cellpadding="0" cellspacing="0">
              <tr>
                <td height="35" align="left" valign="bottom">Copyright ©
                  <script type="text/javascript">
<!--
var d = new Date();
var curr_year = d.getFullYear();
document.write(curr_year);
//-->
                  </script>
                  <span class="blue_12"><strong>Apostolic Faith Church</strong></span></td>
                <td align="right" valign="bottom"><a href="#" target="_blank"><img src="images/zippro_img.png" alt="" width="165" height="16"></a></td>
              </tr>
              <tr>
                <td width="26%" align="left" valign="top">&nbsp;</td>
                <td width="74%" align="right" valign="top"><a href="#" target="_blank"></a></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="right" valign="top">&nbsp;</td>
        </tr>
      </table></td>
  </tr>
</table>
<script type="text/javascript">
<!--
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
//-->
</script>
</body>
</html>
