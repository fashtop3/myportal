<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		if(!empty($_FILES['CVideo']['name']))
		{  			
			if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CVideo']['name']);			
					
					$image_type = strtoupper(VIDEO_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Video can not be uploaded...!";
						header("Location: index.php?p=news_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CVideo']['name'];					
										
					$upload_file = $_FILES['CVideo']['tmp_name'];
					$CVideo = addslashes('news_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/video/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CVideo;					
					copy($_FILES['CVideo']['tmp_name'], $path.$CVideo);
					
					$SQL = "SELECT * FROM news ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['video']!=''){						
						unlink($path.stripslashes($img[0]['video']));
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Video Not Uploaded";
				header("Location: index.php?p=news_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CVideo = addslashes($_REQUEST['OldVideo']);
		}
		$SQL = "UPDATE news SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "video='".$CVideo."',";		
		$SQL .= 'short_description="'.addslashes($_REQUEST['ShortContent']).'",';
		$SQL .= 'full_description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= "modified='".$_REQUEST['CDate']."',";
		$SQL .= "admin_modifiedby='".$_SESSION['AdminID']."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modifiedby='0'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'News Updated Successfully!';
		header("Location: index.php?p=news_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		if(!empty($_FILES['CVideo']['name']))
		{  
			if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CVideo']['name']);			
					
					$image_type = strtoupper(VIDEO_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Video can not be uploaded...!";
						header("Location: index.php?p=news_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CVideo']['name'];					
										
					$upload_file = $_FILES['CVideo']['tmp_name'];
					$CVideo = addslashes('news_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/video/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CVideo;					
					copy($_FILES['CVideo']['tmp_name'], $path.$CVideo);									
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Video Not Uploaded";
				header("Location: index.php?p=news_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CVideo = '';
		}
		$SQL = "INSERT news SET ";		
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "video='".$CVideo."',";		
		$SQL .= 'short_description="'.addslashes($_REQUEST['ShortContent']).'",';
		$SQL .= 'full_description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= "created='".$_REQUEST['CDate']."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "admin_createdby='".$_SESSION['AdminID']."',";
		$SQL .= "createdby='0'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		
		if($_REQUEST['GalleryTitle']!='')
		{
			$sql = "select * from news order by id desc";
			$news = $objDB->select($sql);
			$newsid = $news[0]['id'];
			$sql = "insert news_gallery_category set title = '".addslashes($_REQUEST['GalleryTitle'])."', news_id = ".$newsid;
			$sql .= ",created='".date('Y-m-d H:i:s')."',";
			$sql .= 'place_id='.$_REQUEST['Place'].',';
			$sql .= "admin_createdby='".$_SESSION['AdminID']."',";
			$sql .= "createdby='0'";
			$objDB->sql_query($sql);
		}
		$_SESSION['SuccessMsg'] = 'News Added Successfully!';
		header("Location: index.php?p=news_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$sql = "select * from news_gallery_category where news_id = ".$_REQUEST['ID'];
	$cu = $objDB->select($sql);
	if(count($cu)>0){
		$_SESSION['ErrorMsg'] = 'You can not delete this News. It is already assigned to few Image Gallery.';
		header("Location: index.php?p=news_list&pg_no=".$_REQUEST['pg_no']);	
		exit;
	}
	$video = FetchValue('news','video','id',$_REQUEST['ID']);
	if($video!='')
		unlink('../uploads/video/'.stripslashes($video));
	$SQL = "DELETE FROM news ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'News Deleted Successfully!';
	header("Location: index.php?p=news_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$sql = "select * from news_gallery_category where news_id = ".$_REQUEST['del'][$i];
		$cu = $objDB->select($sql);
		if(count($cu)>0){
			$_SESSION['ErrorMsg'] = 'You can not delete this News. It is already assigned to few Image Gallery.';
			header("Location: index.php?p=news_list&pg_no=".$_REQUEST['pg_no']);	
			exit;
		}
		$video = FetchValue('news','video','id',$_REQUEST['del'][$i]);
		if($video!='')
			unlink('../uploads/video/'.stripslashes($video));
		$SQL = "DELETE FROM news ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'News Deleted Successfully!';
	
	header("Location: index.php?p=news_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
