<?php
	require_once("../utils/dbclass.php");
	require_once("../utils/functions.php");	
	require_once("../utils/config.php");
	$objDB = new MySQLCN;

	$Tbl = "prayer";

//==================================  SINGLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
		DeleteData($Tbl,"p_id","=",$_REQUEST['ID']);
		$_SESSION['SuccessMsg'] = "Record Deleted Successfully";
		header("Location: index.php?p=prayer_list&pg_no=".$_REQUEST['pg_no']);
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
		//print_r($_REQUEST['del']);exit;
		for($i=0;$i<count($_REQUEST['del']);$i++)
		{
			DeleteData($Tbl,'p_id','=',$_REQUEST['del'][$i]);
		}
		$_SESSION['SuccessMsg'] = "Records Deleted Successfully";
		header("Location: index.php?p=prayer_list&pg_no=".$_REQUEST['pg_no']);
}
exit();
?>
