<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$flag = true;
					
						if(strtoupper($img_extension1[count($img_extension1)-1])=='MP3')		{
							$flag= true;
							break;
						}
						else
							$flag=false;
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Song can not be uploaded...! Only MP3 Songs allowed to upload.";
						header("Location: index.php?p=new_song&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = str_replace('-','',str_replace(' ','',str_replace('.','',$_FILES['CImage']['name']))).".".$img_extension1[count($img_extension1)-1];
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('song_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);//'_'.$FileName;	
					//$PImage = $FileName;	
					
					copy($_FILES['CImage']['tmp_name'], "../prayer/mp3/".$CImage);
					
					$SQL = "SELECT * FROM playlist ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['file']!=''){						
						unlink("../prayer/mp3/".$img[0]['file']);						
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Song Not Uploaded";
				header("Location: index.php?p=new_song&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		
		$SQL = "UPDATE playlist SET ";
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";	
		$SQL .= 'place_id='.$_REQUEST['Place'].',';		
		$SQL .= "file='".$CImage."',";		
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Song Updated Successfully!';
		header("Location: index.php?p=playlist&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{		
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$flag = true;
					
						if(strtoupper($img_extension1[count($img_extension1)-1])=='MP3')		{
							$flag= true;
							
						}
						else
							$flag=false;					
					
					if($flag==false)
					{
						//echo $img_extension1;
						$_SESSION['ErrorMsg'] = "Song can not be uploaded...! Only MP3 Songs allowed to upload.";
						header("Location: index.php?p=new_song&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = str_replace('-','',str_replace(' ','',str_replace('.','',$_FILES['CImage']['name']))).".".$img_extension1[count($img_extension1)-1];
					
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('song_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);//'_'.$FileName;	
					//$PImage = $FileName;	
					
					copy($_FILES['CImage']['tmp_name'], "../prayer/mp3/".$CImage);					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Song Not Uploaded";
				header("Location: index.php?p=new_song&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Song does not exists";
			header("Location: index.php?p=new_song&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$SQL = "INSERT playlist SET ";		
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";	
		$SQL .= 'place_id='.$_REQUEST['Place'].',';	
		$SQL .= "file='".$CImage."',";		
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Song Added Successfully!';
		header("Location: index.php?p=playlist&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
//	unlink()
	$SQL = "SELECT * FROM playlist ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);
	$path = '../prayer/mp3';
	if($img[0]['file']!=''){
		unlink($path."/".$img[0]['file']);
	}
	$SQL = "DELETE FROM playlist ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Song Deleted Successfully!';
	header("Location: index.php?p=playlist&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "SELECT * FROM playlist ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$img = $objDB->sql_query($SQL);
		$path = '../prayer/mp3';
		if($img[0]['file']!=''){
			unlink($path."/".$img[0]['file']);
		}
		$SQL = "DELETE FROM playlist ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Songs Deleted Successfully!';
	
	header("Location: index.php?p=playlist&pg_no=".$_REQUEST['pg_no']);	
	exit;
}

//==================================  ASSIGN MULTIPLE SONGS TO PLAYLIST  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "ADDTOPLAYLIST")
{
	$sql = "select * from county";
	$pl = $objDB->select($sql);
	for($p=0;$p<count($pl);$p++){
		$SQL = "UPDATE playlist SET play = '' where place_id=".$pl[$p]['c_id'];
		$objDB->sql_query($SQL);
	
		//var_dump($_REQUEST);
		
		$path = array();
		for($i=0;$i<count($_REQUEST['ad']);$i++)	
		{
			$SQL = "SELECT * FROM playlist ";
			$SQL .= "WHERE id = '".$_REQUEST['ad'][$i]."' and place_id=".$pl[$p]['c_id'];
			$img = $objDB->sql_query($SQL);
			if($img[0]['file']!='')
				$path[] = 'mp3/'.$img[0]['file'];
	
			$SQL = "UPDATE playlist SET play = 1 ";
			$SQL .= "WHERE id = '".$_REQUEST['ad'][$i]."' and place_id=".$pl[$p]['c_id'];
			$objDB->sql_query($SQL);			
		}
		$file = 'playlist'.$pl[$p]['c_id'].'.xml';
		//echo $file;
		unlink('../prayer/'.$file);
		
		
		// Create XML File 
		
		// Write XML File
		$str = '<?xml version="1.0" encoding="UTF-8"?><playlist version="1"><trackList>';
		for($i=0;$i<count($path);$i++)
		{
			$str .= '<track><location>'.$path[$i].'</location></track>';
		}
		
		$str .= '</trackList></playlist>';
	
		$fp = @fopen('../prayer/'.$file,'w');
		if(!$fp) {
			 die('Error cannot create XML file');
		}
		fwrite($fp,$str);
		fclose($fp);
	}
	$_SESSION['SuccessMsg'] = 'Playlist Update Successfully!';
	
	header("Location: index.php?p=playlist&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
