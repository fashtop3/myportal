<?php
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/functions.php");
	$objDB = new MySQLCN;	

//============================= Update ============================= 
if(isset($_REQUEST["action"]) && $_REQUEST["action"] == 'UPDATE')
{
	$SQL = "UPDATE donation SET ";
	$SQL .= "title  = '".addslashes($_REQUEST["Title"])."',";
	$SQL .= "type  = '".addslashes($_REQUEST["Type"])."',";
	$SQL .= "amount_text = '".addslashes($_REQUEST["Amount"])."',";
	$SQL .= "description = '".addslashes($_REQUEST["Content"])."'";
	$SQL .= " WHERE id = '".$_REQUEST['id']."'";
	/*var_dump($_REQUEST);
	echo $SQL;exit;*/
	$objDB->sql_query($SQL);	
	$_SESSION['SuccessMsg'] = "Record Updated Successfully!";
	header("location: index.php?p=shop&pg_no=".$_REQUEST['pg_no']);
	exit();
}
if(isset($_REQUEST["action"]) && $_REQUEST["action"] == 'ADD')
{
	$SQL = "INSERT donation SET ";
	$SQL .= "title  = '".addslashes($_REQUEST["Title"])."',";
	$SQL .= "type  = '".addslashes($_REQUEST["Type"])."',";
	$SQL .= "amount_text = '".addslashes($_REQUEST["Amount"])."',";
	$SQL .= "description = '".addslashes($_REQUEST["Content"])."'";
	
	//echo $SQL;exit;
	$id = $objDB->adder($SQL);	
	if($_REQUEST['Type']=='Camp Meeting'){
		$cm = FetchData("camp_meeting",array(),"where donation_id=''");
		for($i=0;$i<count($cm);$i++){
			$sql = "insert camp_meeting set ";
			$sql .= "donation_id = '".$id."',";
			$sql .= "title = '".$cm[$i]['title']."',";
			$sql .= "price = '".$cm[$i]['price']."',";
			$sql .= "category = '".$cm[$i]['category']."'";
			$objDB->sql_query($sql);
		}		
	}
	$_SESSION['SuccessMsg'] = "Record Added Successfully!";
	header("location: index.php?p=shop&pg_no=".$_REQUEST['pg_no']);
	exit();
}
//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$camp = FetchData("camp_meeting",array(),"where donation_id='".$_REQUEST['ID']."'");
	$cid = array();
	for($i=0;$i<count($camp);$i++)
		$cid[] = $camp[$i]['id'];
	
	$ids = @implode(',',$cid);
	$user = FetchData("camp_meeting_user",array(),"where camp_in in(".$ids.")");
	if(count($user)>0){
		$uid = $user[0]['user_id'];
		$sql = "update camp_user set status = 0 where user_id = '".$uid."'";
		$objDB->sql_query($sql);
	}
	
	$sql = "delete from camp_meeting where donation_id='".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$SQL = "DELETE FROM donation ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Record Deleted Successfully!';
	header("Location: index.php?p=shop&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{	
		$camp = FetchData("camp_meeting",array(),"where donation_id='".$_REQUEST['del'][$i]."'");
		$cid = array();
		for($i=0;$i<count($camp);$i++)
			$cid[] = $camp[$i]['id'];
		
		$ids = @implode(',',$cid);
		$user = FetchData("camp_meeting_user",array(),"where camp_in in(".$ids.")");
		if(count($user)>0){
			$uid = $user[0]['user_id'];
			$sql = "update camp_user set status = 0 where user_id = '".$uid."'";
			$objDB->sql_query($sql);
		}
		
		$sql = "delete from camp_meeting where donation_id='".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		$SQL = "DELETE FROM donation ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
	}
	$_SESSION['SuccessMsg'] = 'Record Deleted Successfully!';
	
	header("Location: index.php?p=shop&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
//=================================== Approve ===================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "1")
{
		$camp = FetchData("camp_meeting",array(),"where donation_id='".$_REQUEST['ID']."'");
		$cid = array();
		for($i=0;$i<count($camp);$i++)
			$cid[] = $camp[$i]['id'];
		
		if(count($cid)>0)
			$ids = @implode(',',$cid);
		else
			$ids = 0;
		$user = FetchData("camp_meeting_user",array(),"where camp_id in(".$ids.")");
		if(count($user)>0){
			$uid = $user[0]['user_id'];
			$sql = "update camp_user set status = 1 where user_id = '".$uid."'";
			$objDB->sql_query($sql);
		}
		
		$SQL = "UPDATE donation SET 
				status = 1
				WHERE id = ".$_REQUEST['ID'];
				
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
					$_SESSION['SuccessMsg'] = "Status changed successfully";	
		header("Location: index.php?p=shop&pg_no=".$_REQUEST['pg_no']);
}

//=================================== DisApprove ===================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "0")
{
		$camp = FetchData("camp_meeting",array(),"where donation_id='".$_REQUEST['ID']."'");
		$cid = array();
		for($i=0;$i<count($camp);$i++)
			$cid[] = $camp[$i]['id'];
		
		if(count($cid)>0)
			$ids = @implode(',',$cid);
		else
			$ids = 0;
		$user = FetchData("camp_meeting_user",array(),"where camp_id in(".$ids.")");
		if(count($user)>0){
			$uid = $user[0]['user_id'];
			$sql = "update camp_user set status = 0 where user_id = '".$uid."'";
			$objDB->sql_query($sql);
		}
		
		$SQL = "UPDATE donation SET 
				status = 0
				WHERE id = '".$_REQUEST['ID']."'";
				
		//echo $SQL;exit;	
		$objDB->sql_query($SQL);$_SESSION['SuccessMsg'] = "Status changed successfully";	
		header("Location: index.php?p=shop&pg_no=".$_REQUEST['pg_no']);
}
exit();

?>
