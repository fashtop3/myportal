<?php  
ob_start();
session_start();
require_once("../utils/config.php");
require_once("../utils/functions.php");
require_once("../utils/dbclass.php");
$objDB = new MySQLCN;

//======================== Update ========================
if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
{
	$SQL = "UPDATE book SET ";
	$SQL .= "book_no='".addslashes($_REQUEST['No'])."',";
	$SQL .= "lesson_no='".addslashes($_REQUEST['Lesson'])."',";
	$SQL .= 'lesson_date="'.$_REQUEST['LDate'].'"';	
	$SQL .= " WHERE id=".$_REQUEST['id'];	   
	$objDB->sql_query($SQL);
	$sql = "update curriculum set cdate = '".$_REQUEST['LDate']."' where lesson_no = '".$_REQUEST['id']."'";
	$objDB->sql_query($sql);
	$_SESSION['SuccessMsg'] = 'Book Updated Successfully!';
	header("Location: index.php?p=book_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}
else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
{
	$SQL = "INSERT book SET ";		
	$SQL .= "book_no='".addslashes($_REQUEST['No'])."',";
	$SQL .= "lesson_no='".addslashes($_REQUEST['Lesson'])."',";
	$SQL .= 'lesson_date="'.$_REQUEST['LDate'].'"';	
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Book Added Successfully!';
	header("Location: index.php?p=book_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$SQL = "DELETE FROM book ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Book Deleted Successfully!';
	header("Location: index.php?p=book_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "DELETE FROM book ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Book Deleted Successfully!';
	
	header("Location: index.php?p=book_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}

// Update Multiple Lesson date of a book
if($_REQUEST['action'] == 'UpdateMultipleDate'){
	if($_REQUEST['LessonFrom'] > $_REQUEST['LessonTo'])
	{
		$_SESSION['ErrorMsg'] = "Lesson No. From must be greater than or equal to Lesson No. To";
		header("Location:index.php?p=book_multiple_date");
		exit;
	}else{
		$start_date = $_REQUEST['SDate'];
		$day = date('l',strtotime($start_date));
		if($day == 'Sunday')
			$start_date = $start_date;
		else
			$start_date = date('Y-m-d',strtotime($start_date." next sunday"));
				
		$book = FetchData("book",array(),"where book_no = '".$_REQUEST['Book']."' and lesson_no >= ".$_REQUEST['LessonFrom']." and lesson_no <= ".$_REQUEST['LessonTo']." order by lesson_no");
		for($i=0;$i<count($book);$i++){
			if((int)$book[$i]['lesson_no'] >= $_REQUEST['LessonFrom'] && (int)$book[$i]['lesson_no'] <= $_REQUEST['LessonTo']){				
				$sql = "UPDATE book SET ";
				$sql .= "lesson_date = '".$start_date."' ";
				$sql .= "where id = '".$book[$i]['id']."'";
				$objDB->sql_query($sql);
				
				$start_date = date('Y-m-d',strtotime($start_date." next sunday"));
			}
		}
		
		$_SESSION['SuccessMsg'] = "Lesson Date updated";
		header("Location:index.php?p=book_list");
		exit;
	}
}
?>
