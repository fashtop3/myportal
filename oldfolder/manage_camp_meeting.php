<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$SQL = "UPDATE camp_meeting SET ";
		$SQL .= "category='".addslashes($_REQUEST['Category'])."',";
		$SQL .= "donation_id='".addslashes($_REQUEST['Donation'])."',";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= 'description="'.addslashes(CAMP_MEETING).'",';
		$SQL .= "price='".addslashes($_REQUEST['Price'])."'";
		$SQL .= " where id='".$_REQUEST['id']."'";
	  
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Meeting Updated Successfully!';
		header("Location: index.php?p=camp_setting&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$SQL = "INSERT camp_meeting SET ";		
		$SQL .= "category='".addslashes($_REQUEST['Category'])."',";
		$SQL .= "donation_id='".addslashes($_REQUEST['Donation'])."',";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= 'description="'.addslashes(CAMP_MEETING).'",';
		$SQL .= "price='".addslashes($_REQUEST['Price'])."'";

		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Meeting Added Successfully!';
		header("Location: index.php?p=camp_setting&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$SQL = "DELETE FROM camp_meeting ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Meeting Deleted Successfully!';
	header("Location: index.php?p=camp_setting&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "DELETE FROM camp_meeting ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Meeting Deleted Successfully!';
	
	header("Location: index.php?p=camp_setting&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>