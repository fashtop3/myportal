<?php

$Tbl="admin";
$FieldsArr=array();
$Where="where UserID=".$_SESSION['AdminID'];
$Sort="";
$Limit="";
$AdminRecordSet=FetchData($Tbl,$FieldsArr,$Where,$Sort,$Limit);

if($_SESSION['AdminID']!=1){
	$where = 'where place_id='.$_SESSION['PlaceID'];
	$prod = FetchData("member_product",array(),$where." order by id desc limit 10");
}
else{
	$where = 'where 1';
	$prod = FetchData("member_product",array(),"order by id desc limit 10");
}

// -------------- MEMBER -----------------
$SQL = FetchQuery("member",'', $where." and DATE(created) = '".date('Y-m-d')."'", '', '');
$RecordSet = $objDB->select($SQL);
$today_member = count($RecordSet);

$SQL = FetchQuery("member",'', $where." and DATE(created) = '".date('Y-m-d',strtotime("-1 day"))."'", '', '');
$RecordSet = $objDB->select($SQL);
$yesterday_member = count($RecordSet);

$SQL = FetchQuery("member",'', $where." and DATE(created) >= '".date('Y-m')."-1'", '', '');
$RecordSet = $objDB->select($SQL);
$month_member = count($RecordSet);

$SQL = FetchQuery("member",'', $where." and DATE(created) >= '".date('Y')."-1-1'", '', '');
$RecordSet = $objDB->select($SQL);
$year_member = count($RecordSet);

$SQL = FetchQuery("member",'', $where." and DATE(created) >= '".date('Y',strtotime("- 1 year"))."-1-1' and DATE(created) <= '".date('Y',strtotime("- 1 year"))."-12-31'", '', '');
$RecordSet = $objDB->select($SQL);
$total_prev = count($RecordSet);

$SQL = FetchQuery("member",'', $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_member = count($RecordSet);

// -------------- EVENT -----------------
$SQL = FetchQuery("event",array(), $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_event = count($RecordSet);

// -------------- NEWS -----------------
$SQL = FetchQuery("news",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_news = count($RecordSet);

// -------------- SMS SUBSCRIBER -----------------
$SQL = FetchQuery("sms",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_sms = count($RecordSet);

// -------------- VIDEO -----------------
$SQL = FetchQuery("webcast_video",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_video = count($RecordSet);

// -------------- PLACE -----------------
$SQL = FetchQuery("county ",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_place = count($RecordSet);

// -------------- CONTACT US -----------------
$SQL = FetchQuery("contact",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_contact = count($RecordSet);		

// -------------- TRACT -----------------
$SQL = FetchQuery("tract",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_tract = count($RecordSet);		

// -------------- SERMON -----------------
$SQL = FetchQuery("sermon",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_sermon = count($RecordSet);		

// -------------- Gallery -----------------
$SQL = FetchQuery("news_gallery_category",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_gallery = count($RecordSet);		

// -------------- Testimonial -----------------
$SQL = FetchQuery("testimonial",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_testi = count($RecordSet);		

// -------------- Prayer -----------------
$SQL = FetchQuery("prayer",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_prayer = count($RecordSet);		

// -------------- curriculum -----------------
$SQL = FetchQuery("curriculum",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_cur = count($RecordSet);		

// -------------- outreach -----------------
$SQL = FetchQuery("outreach",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_out = count($RecordSet);		

// -------------- reflection -----------------
$SQL = FetchQuery("reflection",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_ref = count($RecordSet);		

// -------------- doctrines -----------------
$SQL = FetchQuery("doctrines",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_doc = count($RecordSet);		

// -------------- schedule -----------------
$SQL = FetchQuery("schedule",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_sch = count($RecordSet);		

// -------------- cms -----------------
$SQL = FetchQuery("cms",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_cms = count($RecordSet);		

// -------------- song -----------------
$SQL = FetchQuery("playlist",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_song = count($RecordSet);	

$SQL = FetchQuery("feedback",array(),  $where, '', '');
$RecordSet = $objDB->select($SQL);
$total_feedback = count($RecordSet);		
?>

<h1 style="color:#246494;padding-left:25px;padding-right:25px;"><a href="prayers.php" target="_blank"><img align="absmiddle" style="float:left;" src="images/prayer.gif" alt="" border="0" /></a> Admin Summary ...! <a href="index.php?p=camp_setting"><img align="absmiddle" style="float:right;" src="images/camp.gif" alt="" border="0" /></a></h1>
<h1 style="color:#246494;padding-left:20px;padding-right:25px;"><a href="index.php?p=quarter_index" class="LinkGray"><img src="images/quarter_index.jpg" alt="" border="0" /></a></h1>
<table width="100%" cellspacing="0" cellpadding="0">
	<tr>
		<td width="7" align="center" class="Heading14">&nbsp;</td>
		<td align="center" height="200" class="Heading14">
			<table width="100%" align="center" cellspacing="2" cellpadding="2" bgcolor="#EEEEEE" class="tbl_info">
				<tr>
					<td colspan="5">
						<table align="center" height="50" width="98%" class="content" style="border:1px solid #CCCCCC; padding:5px; font-size:16px; color:#666666">
							<tr>
								<td width="35%" align="right" class="">Your Last Login</td>
								<td width="3%" class="">:</td>
								<td align="left"><?php echo date('l dS F,Y h:i:s',strtotime($AdminRecordSet[0]['LastLogin']));?> &nbsp; <?php echo $AdminRecordSet[0]['TimeAttrib'];?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="5" height="10"></td>
				</tr>
				<tr>
					<td colspan="5" align="center">
						<table width="100%" class="content" cellpadding="0" cellspacing="0">
							<tbody>
								<tr>
									<?php /*?><td valign="top" width="49%">
										<h3 align="center">Recent Member Activity</h3>
										<table width="100%" style='font-size:13px;font-family:Arial, Helvetica, sans-serif;' bgcolor="#cccccc" cellspacing="1" cellpadding="5">
											<tbody>
												<tr style="text-align: center; font-weight: bold;" bgcolor="#DDDDDD">
													<td>Member</td>
													<td>Place</td>
													<td>Last Access</td>
												</tr>
												<?php
							//echo date('Y-m-d',strtotime("-1 day"));
							$sqlm = "select * from member ".$where." order by last_visit desc LIMIT 5";	
							$user = $objDB->select($sqlm);
							//echo count($total);
							for($i=0;$i<count($user);$i++){
								
						?>
												<tr style="text-align: center;" bgcolor="#f1f1f1">
													<td><a class="LinkGray" href="index.php?p=member_detail&id=<?php echo $user[$i]['id'];?>"><?php echo stripslashes($user[$i]['firstname']." ".$user[$i]['lastname']);?></a></td>
													<td><?php echo stripslashes(FetchValue("county","c_name","c_id",$user[$i]['place_id']));?></td>
													<td><?php echo $user[$i]['last_visit'];?></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</td>
									<td width="2%"></td><?php */?>
									<td valign="top" width="49%">
										<h3 align="center">Recent Admin
											Activity</h3>
										<table width="100%" style='font-size:13px;font-family:Arial, Helvetica, sans-serif;' bgcolor="#cccccc" cellpadding="5" cellspacing="1">
											<tbody>
												<tr style="text-align: center; font-weight: bold;" bgcolor="#DDDDDD">
													<td>Admin</td>
													<td>Place</td>
													<td>Last Access</td>
												</tr>
												<?php
							//echo date('Y-m-d',strtotime("-1 day"));
							$sqlm = "select * from admin ".$where." order by LastLogin desc LIMIT 5";	
							$user = $objDB->select($sqlm);
							//echo count($total);
							for($i=0;$i<count($user);$i++){
								
						?>
												<tr style="text-align: center;" bgcolor="#f1f1f1">
													<td><a class="LinkGray" href="index.php?p=administrator_detail&id=<?php echo $user[$i]['UserID'];?>"><?php echo stripslashes($user[$i]['FirstName']." ".$user[$i]['LastName']);?></a></td>
													<td><?php echo stripslashes(FetchValue("county","c_name","c_id",$user[$i]['place_id']));?></td>
													<td><?php echo $user[$i]['LastLogin'];?></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="5" height="10"></td>
				</tr>
				<tr>
					<td colspan="5">
						<table height="50" align="center" class="content" bgcolor="#FFE8FF" style="border:1px dashed #FF0000" width="100%">
							<tr>
								<td align="center" style='font-size:14px; font-family:Arial, Helvetica, sans-serif;'>
									<?php if($_SESSION['AdminID']==1){ ?>
									Total Places: <a href="index.php?p=place_list" class="LinkGray"><?php echo $total_place?></a> &nbsp;&nbsp;|&nbsp;&nbsp;
									<?php } ?>
									Total CMS Pages: <a href="index.php?p=cms_list" class="LinkGray"><?php echo $total_cms?></a> &nbsp;&nbsp;|&nbsp;&nbsp; 
									Total Prayer Songs: <a href="index.php?p=playlist" class="LinkGray"><?php echo $total_song?></a> &nbsp;&nbsp;|&nbsp;&nbsp; 
									Comments / Suggestions (Live Video and Other Videos): <a href="index.php?p=comment_list" class="LinkGray">
									<?php 
										$com = FetchData("comment",array(),"");
										echo count($com);
									?>
									</a> </td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="5" height="10"></td>
				</tr>
				<tr>
					<td colspan="5" height="10">
						<table width="100%" cellpadding="5" cellspacing="1">
							<tr>
								<?php /*?><td valign="top" width="24%" class="content">
									<h3 align="center">Members</h3>
									<table width="100%" style='font-size:13px;font-family:Arial, Helvetica, sans-serif;' bgcolor="#cccccc" cellpadding="5" cellspacing="1">
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Today's Member</td>
											<td><?php echo $today_member?></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Yesterday's Member</td>
											<td><?php echo $yesterday_member?></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Month to Date Total</td>
											<td><?php echo $month_member?></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Year to Date Total</td>
											<td><?php echo $year_member?></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Last Year's Member</td>
											<td><?php echo $total_prev?></td>
										</tr>
									</table>
								</td>
								<td valign="top" width="1%"></td><?php */?>
								<td valign="top" width="24%" class="content">
									<h3 align="center">Statistics - Part 1</h3>
									<table width="100%" style='font-size:13px;font-family:Arial, Helvetica, sans-serif;' bgcolor="#cccccc" cellpadding="5" cellspacing="1">
										<?php /*?><tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Member</td>
											<td><a href="index.php?p=member_list" class="LinkGray"><?php echo $total_member?></a></td>
										</tr><?php */?>
                                                  <tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Feedback</td>
											<td><a href="index.php?p=feedback_list" class="LinkGray"><?php echo $total_feedback?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>SMS Subscribers</td>
											<td><a href="index.php?p=sms_list" class="LinkGray"><?php echo $total_sms?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Contacts</td>
											<td><a href="index.php?p=contact_list" class="LinkGray"><?php echo $total_contact?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Events</td>
											<td><a href="index.php?p=event_list" class="LinkGray"><?php echo $total_event?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total News</td>
											<td><a href="index.php?p=news_list" class="LinkGray"><?php echo $total_news?></a></td>
										</tr>
									</table>
								</td>
								<td valign="top" width="1%"></td>
								<td valign="top" width="24%" class="content">
									<h3 align="center">Statistics - Part 2</h3>
									<table width="100%" style='font-size:13px;font-family:Arial, Helvetica, sans-serif;' bgcolor="#cccccc" cellpadding="5" cellspacing="1">
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Prayer Requests</td>
											<td><a href="index.php?p=prayer_list" class="LinkGray"><?php echo $total_prayer?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Testimonials</td>
											<td><a href="index.php?p=testi_list" class="LinkGray"><?php echo $total_testi?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Galleries</td>
											<td><a href="index.php?p=news_gallery_list" class="LinkGray"><?php echo $total_gallery?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Tracts</td>
											<td><a href="index.php?p=tract_list" class="LinkGray"><?php echo $total_tract?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Sermons</td>
											<td><a href="index.php?p=sermon_list" class="LinkGray"><?php echo $total_sermon?></a></td>
										</tr>
									</table>
								</td>
								<td valign="top" width="1%"></td>
								<td valign="top" width="24%" class="content">
									<h3 align="center">Statistics - Part 3</h3>
									<table width="100%" style='font-size:13px;font-family:Arial, Helvetica, sans-serif;' bgcolor="#cccccc" cellpadding="5" cellspacing="1">
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Curriculums</td>
											<td><a href="index.php?p=curriculum_list" class="LinkGray"><?php echo $total_cur?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Outreach Pages</td>
											<td><a href="index.php?p=outreach_list" class="LinkGray"><?php echo $total_out?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Reflection Pages</td>
											<td><a href="index.php?p=reflection_list" class="LinkGray"><?php echo $total_ref?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Doctrines Pages</td>
											<td><a href="index.php?p=doctrines_list" class="LinkGray"><?php echo $total_doc?></a></td>
										</tr>
										<tr style="color:#666666;" bgcolor="#efefef">
											<td>Total Schedules</td>
											<td><a href="index.php?p=schedule_list" class="LinkGray"><?php echo $total_sch?></a></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td colspan="5" height="10"></td>
				</tr>
				<?php /*?><?php if($_SESSION['AdminID']==1){?>
				<!--<tr>
          <td colspan="5" class="content" valign="top"><h3 align="center">Payment History &nbsp;<a href="index.php?p=payment_history" class="LinkGray">(View All)</a></h3>
          	<table width="100%" style='font-size:13px;font-family:Arial, Helvetica, sans-serif;' bgcolor="#cccccc" cellpadding="5" cellspacing="1">
            	<tr bgcolor="#DDDDDD">
              	<td align="left"><b>Member Name</b></td>
                <td align="left"><b>Place</b></td>
                <td align="left"><b>Product</b></td>
                <td align="left"><b>Quantity</b></td>
                <td align="left"><b>Price</b></td>
                <td align="left"><b>Amount</b></td>
              </tr>
              <?php 
							if(count($prod)>0){
								for($i=0;$i<count($prod);$i++){
							?>
              <tr bgcolor="#F1F1F1">
              	<td align="left">
                	<?php $mem = FetchData("member",array(),"where id = '".$prod[$i]['member_id']."'");
									if(count($mem)>0)
										echo $mem[0]['firstname']." ".$mem[0]['lastname'];
									else
										echo "Unregistered Member";
									?>
                </td>
                <td align="left"><?php if($prod[$i]['place_id']!='' && $prod[$i]['place_id']!='0') echo FetchValue("county","c_name","c_id",$prod[$i]['place_id']);else echo FetchValue("county","c_name","c_id",DEFAULT_PLACE);?></td>
                <td align="left"><?php if(strtolower($prod[$i]['product'])=='tithes' || strtolower($prod[$i]['product'])=='offering'){?>
	                  <?php echo $prod[$i]['product']?>
                  <?php }else{ ?>
                  	<?php echo FetchValue("cd","title","id",$prod[$i]['product_id']);?>
                  <?php } ?><?php //echo $prod[$i]['product']?></td>
                <td align="left"><?php if($prod[$i]['qty']!='' && $prod[$i]['qty']!='0') echo $prod[$i]['qty']; else echo "1";?></td>
                <td align="left"><?php echo htmlentities(CURRENCY_SIGN).number_format($prod[$i]['price'],2);?></td>
                <td align="left"><?php echo htmlentities(CURRENCY_SIGN).number_format($prod[$i]['total_price'],2);?></td>
              </tr>
              <?php }
							}else{ ?>
              <tr bgcolor="#F1F1F1">
              	<td colspan="6" align="center" valign="top">No record found</td>
              </tr>
              <?php } ?>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="5" height="10"></td>
        </tr>-->
				<?php } ?><?php */?>
			</table>
		</td>
		<td width="8" align="center" class="Heading14">&nbsp;</td>
	</tr>
</table>
