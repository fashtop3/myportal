<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$title = "Update";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"cd"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$Photo = "<img src='../uploads/contact/big/".stripslashes($Info[0]['image'])."' width=75 height=75>";	
		
}else{
	$MODE="ADD";
	$title = "Add";
}
?>
<script>
  $(document).ready(function(){
  $("#Type").change(function()
	{
		$.post("get_cd.php",{ id:$(this).val() } ,function(data)
		{
			$("#Category").html(data);
		});
		
		if($(this).val()=='HM'){			
			var data1 = "Image";
			var data2 = "<input type='file' name='CImage' id='CImage' accept='JPG|GIF|JPEG|PNG' title=' *' validate='required: true' />";
			
			$("#Img1").html(data1);		
			$("#Img2").html(data2);	
			document.getElementById("Img1").style.display='block';
			document.getElementById("Img2").style.display='block';	
		}	else {
			$("#Img1").html('');		
			$("#Img2").html('');	
			document.getElementById("Img1").style.display='none';
			document.getElementById("Img2").style.display='none';	
		}	
	});
	
	$("#Category").change(function()
	{
		$("#Category").html(data);		
	});
		
	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#Description').val(con); // put it in the textarea
	});
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required",
			Type: "required"			
		},
		messages: {
			Title: " *",
			Type: " *"
		}
	});	
  });
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "Description",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 400,
                height: 640,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
           
		}
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td width="5%" height="24" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addcontactcategory.jpg" width="48" height="48" /></td>
		<td colspan=5 class="tbl_head"><?php echo $title?> CD / DVD / Higherway Magazine / Music &amp; Track</td>
	</tr>
	<tr>
		<td colspan="6" width="">
			<form method="post" action="manage_cd.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>" />
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input type="hidden" name="OldImage" id="OldImage" value="<?php echo stripslashes($Info[0]['image'])?>" />
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden" />
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="20%">Type</td>
											<td class="fieldarea">
												<select name="Type" class="InputBox" id="Type" style="width:300px;">
													<option value="">--Select--</option>
													<option <?php if($Info[0]['type']=='CD') echo "selected";?> value="CD">CDs</option>
													<option <?php if($Info[0]['type']=='DVD') echo "selected";?> value="DVD">DVDs</option>
													<option <?php if($Info[0]['type']=='HM') echo "selected";?> value="HM">Higherway Magazine</option>
													<option <?php if($Info[0]['type']=='MT') echo "selected";?> value="MT">Music &amp; Track</option>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Category</td>
											<td class="fieldarea">
												<select title="*" validate="required: true" name="Category" class="InputBox" id="Category" style="width:300px;">
													<option value="">--Select--</option>
													<?php echo FillCombo1("cd_category","title","id",$Info[0]['category_id'],"where type = '".$Info[0]['type']."'");?>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Title</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Title" id="Title" value="<?php echo stripslashes($Info[0]['title'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Item Number</td>
											<td class="fieldarea">
												<input title=" *" validate="required: true" type="text" class="InputBox" name="Item" id="Item" value="<?php echo stripslashes($Info[0]['item_no'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Author</td>
											<td class="fieldarea">
												<input title=" *" validate="required: true" type="text" class="InputBox" name="Author" id="Author" value="<?php echo stripslashes($Info[0]['author'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Publisher</td>
											<td class="fieldarea">
												<input title=" *" validate="required: true" type="text" class="InputBox" name="Publisher" id="Publisher" value="<?php echo stripslashes($Info[0]['publisher'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Price (per unit)</td>
											<td class="fieldarea"><?php echo htmlentities(CURRENCY_SIGN)?>
												<input title=" *" validate="required: true" type="text" class="InputBox" name="Price" id="Price" value="<?php echo stripslashes($Info[0]['price'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Total Quantity</td>
											<td class="fieldarea">
												<input title=" *" validate="required: true,number:true" type="text" class="InputBox" name="Quantity" id="Quantity" value="<?php echo stripslashes($Info[0]['total'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td id="Img1" class="fieldlabel" style="display:none;"> </td>
											<td class="fieldarea">
												<div id="Img2" style="display:none;"></div>
											</td>
										</tr>
										<?php if($Info[0]['type']=='HM'){?>
										<tr>
											<td class="fieldlabel" width="5%">Image</td>
											<td class="fieldarea">
												<input type="file" class="InputBox" name="CImage" id="CImage" style="width:300px;" />
												<img src="../uploads/contact/big/<?php echo stripslashes($Info[0]['image'])?>" width="100" /> </td>
										</tr>
										<?php } ?>
										<tr>
											<td class="fieldlabel" width="5%">Description</td>
											<td class="fieldarea">
												<textarea title="*" validate="required: true" cols="55" rows="25" name="Description" id="Description"><?php echo stripslashes($Info[0]['description'])?></textarea>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<div align="center">
									<input value="<?php echo $MODE?>" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=cd_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
								<br>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
