<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/common_functions.php");
		CheckPermission(); 
		exit();
	}

	$CategoryName = LoadVariable("CategoryName","");

	$MODE="ADD";
	$PANEL_HEADING="COUNTY";

	if(isset($_REQUEST['ID']))
	{
		$MODE="UPDATE";

		$TblFieldsArr = array
		(
			//table name=>feilds name
			"county"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE c_id =".$_REQUEST['ID'];
		$Sort="";
		$Limit="";

		$RecordSet=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
		$Name = stripslashes($RecordSet[0]['c_name']);
		$MobileNo = stripslashes($RecordSet[0]['c_email']);
		$CategoryID = $RecordSet[0]['c_id'];
}
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#FrmCategoryAddEdit").validate({
		rules: {
			Name: "required",
			Email: "required"			
		},
		messages: {
			Name: "Please enter name",
			Email: "Please enter email address"
		}
	});
  });
</script>

<form action="manage_place.php" method="post" enctype="multipart/form-data" id="FrmCategoryAddEdit" name="FrmCategoryAddEdit">
	<input type="hidden" name="Process" value="<?php echo $MODE?>">
	<input type="hidden" name="OldImage" value="<?php echo stripslashes($RecordSet[0]['image'])?>" >
	<input type="hidden" name="ID" value="<?php echo $_REQUEST['ID']?>">
    <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
	<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
		<tr>
			<td colspan="1" class="tbl_head" height="25" width="5%" align="right"><img src="<?php echo ADMIN_IMAGE_PATH;?>/globe.gif" width="48" height="48" /></td>
			<td colspan="1" height="25" class="tbl_head"><?php echo $MODE?>&nbsp;<?php echo $PANEL_HEADING?></td>
			<td height="25" colspan="2" align="right" class="tbl_head">
				<input type="button" name="BACK" id="BACK" class="Btn" value="BACK" onclick="window.location.href='index.php?p=place_list'">
			</td>
		</tr>
		<tr>
			<td colspan="4">
				<?php ShowMessage(); ?>
				<table width="100%" class="form" align="center" cellpadding="2" cellspacing="0">
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<tr class="BlackMediumNormal">
						<td width="25%" align="left" class="fieldlabel">Name</td>
						<td width="15%">
							<input type="text" name="Name" id="Name" class="InputBox" value="<?php echo stripslashes($Name)?>" maxlength="255" style="width:300px;">
						</td>
						<td valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<tr class="BlackMediumNormal">
						<td width="25%" align="left" class="fieldlabel">Church Image (Only Flash File like .swf)</td>
						<td width="15%" class="fieldlabel">
							<input class="InputBox" name="CImage" accept="SWF" id="CImage" style="width: 400px;" type="file">
						</td>
						<td valign="top">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<tr class="BlackMediumNormal">
						<td align="" valign="top" class="fieldlabel" width="25%">Email</td>
						<td valign="top" width="15%" class="fieldlabel">
							<textarea name="Email" class="TextArea" cols="" id="Email" style="width:300px; height:50px;" rows="" ><?php echo stripslashes($MobileNo)?></textarea>
						</td>
						<td valign="top" width="40%" class="fieldlabel">(Enter Email ids with comma separate)</td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<?php if($_REQUEST['ID']!=DEFAULT_PLACE){ ?>
					<tr class="BlackMediumNormal">
						<td align="" valign="top" class="fieldlabel" colspan="2">
							<input type="checkbox" value="1" <?php if($RecordSet[0]['site']) echo "checked='checked'"; ?> name="Site" id="Site" />
							Do you want to create site for this place? </td>
						<td valign="top" width="40%">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="3" height="10"></td>
					</tr>
					<?php } ?>
					<tr>
						<td colspan="3" align="center"><br />
							<input type="submit" name="<?php echo $MODE?>" id="<?php echo $MODE?>" class="Btn" value="<?php echo $MODE?>">
							<input type="button" name="cancel" id="cancel" class="Btn" value="CANCEL" onclick="window.location='index.php?p=place_list&pg_no=<?php echo $_REQUEST['pg_no']?>'">
							<br />
							<br />
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4" height="10"></td>
		</tr>
	</table>
</form>
