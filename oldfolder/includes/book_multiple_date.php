<script type="text/javascript">
$(document).ready(function(){  	
	// validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			No: "required",
			Lesson:"required",
			LDate:"required",
			SDate:"required"
		},
		messages: {
			No: "Please enter book number",
			Lesson:"Please enter lesson number",
			LDate: "Please enter to date",
			SDate: "Please enter from date"
		}
	});	
	
	$("#Book").change(function()
	{
		$.post("get_lesson1.php",{ id:$(this).val() } ,function(data)
		{														
			document.getElementById('LessonFrom').innerHTML = data;		
			document.getElementById('LessonTo').innerHTML = data;		
		});	
	});
});
$(function() {
	/*$('#LDate').datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: '1975:<?php echo date('Y')+20?>',
		dateFormat: 'yy-mm-dd'
	});*/
	$('#SDate').datepicker({
		beforeShowDay: nonWorkingDates,
		changeMonth: true,
		changeYear: true,
		yearRange: '1975:<?php echo date('Y')+20?>',
		dateFormat: 'yy-mm-dd'
	});
});
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/book.gif" width="48" height="48" /></td>
		<td colspan=5 class="tbl_head" height="24">UPDATE BOOK LESSON DATES</td>
	</tr>
	<tr>
		<td colspan="6" width="">
			<form method="post" action="manage_book.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" value="UpdateMultipleDate" name="action" />
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="15%">Book </td>
											<td class="fieldarea">
												<select name="Book" id="Book" class="InputBox" style="width: 250px;" title=" Please select book" validate="required:true">
													<option value="">-- Select Book --</option>
													<?php
													$book = FetchData("book",array(),"group by book_no order by book_no");
													for($i=0;$i<count($book);$i++){
														echo '<option value="'.$book[$i]['book_no'].'">'.$book[$i]['book_no'].'</option>';
													}
													?>
												</select>
											</td>
										</tr>										
										<tr>
											<td class="fieldlabel" width="15%">Lesson No. From</td>
											<td class="fieldarea">
												<select name="LessonFrom" id="LessonFrom" class="InputBox" style="width: 250px;" title=" Please select lesson no. from" validate="required:true">
													<option value="">-- Select Lesson No. From --</option>
													<?php
													$book = FetchData("book",array(),"where book_no = '".$_REQUEST['Book']."' order by lesson_no");
													for($i=0;$i<count($book);$i++){
														echo '<option value="'.$book[$i]['lesson_no'].'">'.$book[$i]['lesson_no'].'</option>';
													}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%">Lesson No. To</td>
											<td class="fieldarea">
												<select name="LessonTo" id="LessonTo" class="InputBox" style="width: 250px;" title=" Please select lesson no. to" validate="required:true">
													<option value="">-- Select Lesson No. To --</option>
													<?php
													$book = FetchData("book",array(),"where book_no = '".$_REQUEST['Book']."' order by lesson_no");
													for($i=0;$i<count($book);$i++){
														echo '<option value="'.$book[$i]['lesson_no'].'">'.$book[$i]['lesson_no'].'</option>';
													}
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%">From Date</td>
											<td class="fieldarea">
												<input name="SDate" id="SDate" style="width: 250px;" class="InputBox" />
											</td>
										</tr>										
										<tr>
											<td class="fieldlabel" width="15%"></td>
											<td class="fieldarea">
												<em>Note: From Date will automatically take the sunday date upto the From Lesson No. to Lesson No. To</em>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%"></td>
											<td class="fieldarea">												
												<input value="UPDATE" class="Btn" type="submit" name="submit1" id="submit1">
												<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=book_list';">
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
