<?php
	$sql = "select * from cms";
	if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!='')
			$sql.=' where place_id='.$_REQUEST['plc'];
		else
			$sql.=' where 1=1';	
	}
	else
		$sql.=' WHERE place_id='.$_SESSION['PlaceID'];
	if($_REQUEST['admins']!='')
		$sql .= ' and modifiedby = '.$_REQUEST['admins'];
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmMail(frm)
	{
		if(confirm("Do you really want to delete?"))
		{	
			
			frm.Process.value='DELETE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_cms.php';	
			frm.submit();
		}	
	}
function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmMail(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
		
  $(document).ready(function(){   
    $(".tabbox").css("display","none");
var selectedTab;
$(".tab").click(function(){
    var elid = $(this).attr("id");
    $(".tab").removeClass("tabselected");
    $("#"+elid).addClass("tabselected");
    $(".tabbox").slideUp();
    if (elid != selectedTab) {
        selectedTab = elid;
        $("#"+elid+"box").slideDown();
    } else {
        selectedTab = null;
        $(".tab").removeClass("tabselected");
    }
    $("#tab").val(elid.substr(3));
});

  });


</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/cms.jpg" width="48" height="48" /></td>
		<td class="tbl_head" height="24">CMS CONTENT MANAGER</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<?php showMessage();	?>
			<form name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td colspan="3" class="BottomBorder" height="25">
							<?php if($_SESSION['AdminID']==1){ ?>
							<select name="PlaceValue" class="InputBox" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=cms_list&plc='+this.value; else window.location='index.php?p=cms_list';">
								<option value="">All Places</option>
								<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
							</select>
							<select name="admins" class="InputBox" id="admins" onchange="if(this.value!='') window.location='index.php?p=cms_list&admins='+this.value; else window.location='index.php?p=cms_list';">
								<option value="">--Select Administrator--</option>
								<?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
							</select>
							<?php } ?>
							<span class="column_head">Total
							No of Results:&nbsp;<?php echo count($cms)?></span> </td>
						<td height="25" colspan="2" align="right"  class="BottomBorder"><?php echo $objPaging->show_paging()?></td>
					</tr>
					<tr>
						<td colspan="5" class="BottomBorder"></td>
					</tr>
					<tr height="25" class="SmallBlackHeading">
						<td width="5%" align="center" class="BottomBorder">&nbsp;
							<!--input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);"-->
						</td>
						<td width="5%" align="center" class="BottomBorder">#</td>
						<td width="60%" align="left" class="BottomBorder">Title</td>
						<?php if($_SESSION['AdminID']==1){ ?>
						<td width="20%" align="center" class="BottomBorder">Place</td>
						<?php } ?>
						<td align="center" class="BottomBorder">Options</td>
					</tr>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
						<td align="center">
							<!--input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i]['id']?>"-->
						</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo ucwords(stripslashes($cms[$i]['title'])); ?></td>
						<?php if($_SESSION['AdminID']==1){ ?>
						<td align="center"><?php echo stripslashes(FetchValue('county','c_name','c_id',$cms[$i]['place_id']));?> </td>
						<?php } ?>
						<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=cms_addedit&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp;
							<!--img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp;-->
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="Detail" title="Detail" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onClick="window.location.href='index.php?p=cms_detail&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" /> </td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
					<tr>
						<td colspan="5" class="<?php echo $class?>" align="right">
							<?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
						</td>
					</tr>
					<tr height="25">
						<td colspan="5" class="TopBorder">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="13%">
										<!--input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);"-->
									</td>
									<td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
