<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/functions.php");
		CheckPermission(); 
		exit();
	}

	$PANEL_HEADING = "COUNTY MANAGER";
	$Tbl="county";
	$FieldsArr=array();
	$Where="";
	$Sort="order by c_id ";
	$Limit="";
	
	// -------------- REQUEST A CALL -----------------
	$SQL = FetchQuery($Tbl,$FieldsArr,$Where,$Sort,$Limit);
	$objPaging = new paging($SQL, "c_id");
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$RecordSet = $objDB->select($objPaging->get_query());
	if($RecordSet)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script language="JavaScript">
	function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.action='manage_place.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmPlace.ID.value = ID;
			document.FrmPlace.Process.value = Process;
			document.FrmPlace.action = 'manage_place.php';
			document.FrmPlace.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmPlace;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($RecordSet)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
	
</script>

<form name="FrmPlace" method="post" action="">
	<input type="hidden" name="ID" id="ID" value="">
    <input type="hidden" name="pg_no" id="pg_no" value="">
	<input type="hidden" name="Process" id="Process" value="">
	<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="white">
		<tr>
			<td colspan="1" class="tbl_head" height="25"><img src="<?php echo ADMIN_IMAGE_PATH;?>/globe.gif" width="48" height="48" /></td>
			<td colspan="4" class="tbl_head" height="25"><?php echo $PANEL_HEADING?></td>
		</tr>
		<tr>
			<td colspan="5" align="" bgcolor="#FFFFFF">
				<?php showMessage(); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3" class="BottomBorder" height="45">
				<input type="Button" name="ADDNEW" value="ADD NEW" class="Btn" onClick="window.location.href='index.php?p=place_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
				<span class="column_head">Total No of Results:&nbsp;<?php echo count($RecordSet)?></span></td>
			<td height="25" colspan="2" align="right"  class="BottomBorder"><?php echo $objPaging->show_paging()?></td>
		</tr>
		<tr>
			<td colspan="5" class="BottomBorder"></td>
		</tr>
		<tr height="25" class="SmallBlackHeading">
			<!--td width="4%" align="center" class="BottomBorder"><input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);"></td-->
			<td colspan="2" width="4%" align="" class="BottomBorder">&nbsp;&nbsp;#</td>
			<td width="37%" align="left" class="BottomBorder">Place Name</td>
			<td width="46%" align="left" class="BottomBorder">Email</td>
			<td width="13%" align="center" class="BottomBorder">Options</td>
		</tr>
		<tr>
			<td colspan="5">
				<table width="100%" class="" cellpadding="3" cellspacing="2">
					<?php
                  $i=0;
                  $r=$start_limit+1;
                  if(!empty($RecordSet))
                  {
                     for($i=0;$i<count($RecordSet);$i++)
                     { 
                        if(($i+1)%2==0)
                           $bg=BGCOLOR_ODD_ROW;
                        else
                           $bg=BGCOLOR_EVEN_ROW;
													// if($RecordSet[$i]['place_key']!='portland_oragon'){
               ?>
					<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
						<!--td width="4%" align="center"><input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $RecordSet[$i]['c_id']?>"></td-->
						<td width="4%" align="center" class="Numbers"><?php echo $i+1?></td>
						<td width="33%" align="left"><?php echo ucwords(stripslashes($RecordSet[$i]['c_name'])); if(DEFAULT_PLACE==$RecordSet[$i]['c_id']) echo " (Headquarter Place)"?></td>
						<td width="46%" align="left"><?php echo substr(stripslashes($RecordSet[$i]['c_email']),0,50); ?></td>
						<td width="13%" align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/reply.png" style="cursor:pointer" alt="Reply" title="Reply" width="23" height="23" onclick="window.open('includes/send_prayer.php?id=<?php echo $RecordSet[$i]['c_id'];?>&pg_no=<?php echo $_REQUEST['pg_no']?>', 'Quote', 'width=500,height=300,scrollbar=1');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=place_addedit&ID=<?php echo $RecordSet[$i]['c_id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" width="22" height="22" />&nbsp;
							<?php if(DEFAULT_PLACE!=$RecordSet[$i]['c_id']){ ?>
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $RecordSet[$i]['c_id']?>', 'DELETE');" />
							<?php } ?>
						</td>
					</tr>
					<?php //}
                     $r++;
                     }
                  }
               ?>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="TopBorder" align="right">
				<?php if(count($RecordSet)) print $page_link; else print "&nbsp;"; ?>
			</td>
		</tr>
		<tr height="25">
			<td colspan="5" class="TopBorder" height="45">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="13%">
							<!--input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);"-->
						</td>
						<td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
