<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"admin"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE UserID =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<style type="text/css">
#pg_main {
	font-size:1.1em;
	padding:10px;
}
#pg_main .h3title {
	font-size:large;
	font-weight:bold;
	color:#003366;
	margin:5px 5px 8px 0;
}
p {
	margin:0;
	padding:0 0 10px;
	color:black;
	font-family:Arial, Helvetica, sans-serif;
	font-size:70%;
}
</style>
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"> <img src="<?php echo ADMIN_IMAGE_PATH;?>/contact_detail.jpg" width="48" height="48" /> </td>
		<td class="tbl_head" height="24">ADMINISTRATOR CONTACT DETAIL</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" class="form">
				<tr>
					<td valign="top" width="5%" style="padding-left:5px; padding-top:5px;"> <img src="../uploads/member/big/<?php echo $Info[0]['image']?>" width="80" height="100" /> </td>
					<td colspan="" style="padding-left:5px; padding-right:5px; padding-top:5px;" valign="top">
						<div id="pg_main" style="vertical-align:top">
							<h3 class="h3title"><?php echo stripslashes($Info[0]['FirstName']." ".$Info[0]['LastName'])?></h3>
							<p> <?php echo stripslashes($Info[0]['address'])?><br />
								<?php echo stripslashes($Info[0]['phone'])?><br />
								<br>
								<strong>Email:</strong> <a href="mailto:<?php echo stripslashes($Info[0]['Email'])?>"><?php echo stripslashes($Info[0]['Email'])?></a><br>
								<strong>Place:</strong>&nbsp;&nbsp; <?php echo stripslashes(FetchValue('county','c_name','c_id',$Info[0]['place_id'])); ?><br>
							</p>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2">
						<center>
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=administrator&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						</center>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
