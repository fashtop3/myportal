<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"tract_category"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);		
}else
	$MODE="ADD";
	
if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $Info[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}	
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required"
		},
		messages: {
			Title: "Please enter title"
		}
	});	
  });
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addposition.jpg" /></td>
		<td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> TRACT CATEGORY</td>
	</tr>
	<tr>
		<td colspan="6" width="">
			<form method="post" action="manage_tract_category.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<?php if($_SESSION['AdminID']==1){ 
											
										?>
										<tr>
											<td class="fieldlabel" width="5%">Place</td>
											<td class="fieldarea">
												<select name="Place" class="InputBox" id="Place" style="width:300px;">
													<?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
												</select>
											</td>
										</tr>
										<?php }else{ ?>
										<tr>
											<td colspan="2">
												<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
											</td>
										</tr>
										<?php } ?>
										<tr>
											<td class="fieldlabel" width="5%">Title</td>
											<td class="fieldarea">
												<input type="text" name="Title" class="InputBox" id="Title" style="width:300px;" value="<?php echo stripslashes($Info[0]['title'])?>" />
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<br>
								<div align="center">
									<input value="<?php echo $MODE?> TRACT CATEGORY" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=tract_category_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
