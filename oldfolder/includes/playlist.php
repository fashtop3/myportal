<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/functions.php");
		CheckPermission(); 
		exit();
	}

	$PANEL_HEADING = "MANAGE PLAYLIST SONGS";
	$Tbl="playlist";
	$FieldsArr=array();
	if($_SESSION['AdminID']==1)
	{
		if($_REQUEST['plc']!=''){
			$Where=' where place_id='.$_REQUEST['plc'];
			$path1 = '&plc='.$_REQUEST['plc'];
		}
		else{
			$Where=' where 1=1';	
			$path1 = '';
		}
	}
	else
		$Where='where place_id='.$_SESSION['PlaceID'];
	
	if($_REQUEST['Search']!='')
		$Where .= " and title like '%".$_REQUEST['Search']."%'";
	if($_REQUEST['admins']!='')
		$Where .= ' and createdby = '.$_REQUEST['admins'];		
	if($_REQUEST['view']=='my_view'){	
		$Where .= ' and createdby = '.$_SESSION['AdminID'];
		$view = strtoupper("All Songs");
		$viewpath = 'index.php?p=playlist'.$path1;
		$path2 = '&view=my_view';
	}
	else{
		$view = strtoupper("My Added Songs");	
		$viewpath = 'index.php?p=playlist&view=my_view'.$path1;
		$path2 = '';
	}
	$Sort="order by id desc";
	$Limit="";
	
	// -------------- REQUEST A CALL -----------------
	$SQL = FetchQuery($Tbl,$FieldsArr,$Where,$Sort,$Limit);
	$objPaging = new paging($SQL, "id");
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$RecordSet = $objDB->select($SQL);
	if($RecordSet)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	
?>
<script language="JavaScript">
	function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_playlist.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_playlist.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($RecordSet)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
	
	function ValidateSelection1(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($RecordSet)?>;i++)
		{
			if(document.getElementById('ad'+i).checked)
			{
				x=false;
				frm.Process.value='ADDTOPLAYLIST';
				frm.action='manage_playlist.php';	
				frm.submit();
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}	
</script>

<form name="FrmSMS" method="post" action="">
	<input type="hidden" name="ID" id="ID" value="">
    <input type="hidden" name="pg_no" id="pg_no" value="">
	<input type="hidden" name="Process" id="Process" value="">
	<table width="90%" border="0" cellpadding="5" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
		<tr>
			<td height="24" width="5%" colspan="1" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/playlist.jpg" width="48" height="48" /></td>
			<td class="tbl_head" colspan="4" height="24"><?php echo $PANEL_HEADING?></td>
		</tr>
		<tr>
			<td colspan="6" align="" bgcolor="#FFFFFF">
				<?php showMessage();	?>
			</td>
		</tr>
		<tr>
			<td colspan="5" class="content">
				<?php if($_SESSION['AdminID']==1){ ?>
				<select style="width:125px" name="PlaceValue" class="InputBox" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=playlist&plc='+this.value+'<?php echo $path2; ?>'; else window.location='index.php?p=playlist'+'<?php echo $path2; ?>';">
					<option value="">All Places</option>
					<?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
				</select>
				<select style="width:165px" name="admins" class="InputBox" id="admins" onchange="if(this.value!='') window.location='index.php?p=playlist&admins='+this.value; else window.location='index.php?p=playlist';">
					<option value="">--Select Administrator--</option>
					<?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
				</select>
				&nbsp;
				<?php } ?>
				Search by Title:
				<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="InputBox" />
				<input type="submit" value="Search" class="Btn" />
			</td>
		</tr>
		<tr>
			<td colspan="<?php if($_SESSION['AdminID']==1){ ?>6<?php } else { ?>5<?php } ?>" class="BottomBorder" valign="bottom" height="25">
				<input type="Button" name="ADDNEW" value="ADD NEW SONG" class="Btn" onClick="window.location.href='index.php?p=new_song&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
				<input type="Button" name="songs" value="<?php echo $view?>" class="Btn" onClick="window.location.href='<?php echo $viewpath; ?>'" style="width:130px;"/>
				<input type="button" value="SHOW ALL" class="Btn" onclick="window.location='index.php?p=playlist'" />
				<span class="column_head" style="float:right;padding-top:5px;">Total No of Results:&nbsp;<?php echo count($RecordSet)?></span> </td>
		</tr>
		<tr>
			<td colspan="5" class="BottomBorder"></td>
		</tr>
		<tr height="25" class="SmallBlackHeading">
			<td width="4%" align="center" class="BottomBorder">
				<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
			</td>
			<td width="4%" align="center" class="BottomBorder">#</td>
			<td width="34%" align="left" class="BottomBorder">Title</td>
			<?php if($_SESSION['AdminID']==1){ ?>
			<td width="23%" align="center" class="BottomBorder">Place</td>
			<?php } ?>
			<td width="23%" align="center" class="BottomBorder">Add To Playlist</td>
			<td width="12%" align="center" class="BottomBorder">Options</td>
		</tr>
		<?php
			$i=0;
			$r=$start_limit+1;
			if(!empty($RecordSet))
			{
				for($i=0;$i<count($RecordSet);$i++)
				{ 
					if(($i+1)%2==0)
						$bg=BGCOLOR_ODD_ROW;
					else
						$bg=BGCOLOR_EVEN_ROW;
		?>
		<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
			<td align="center">
				<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $RecordSet[$i]['id']?>">
			</td>
			<td align="center" class="Numbers"><?php echo $i+1?></td>
			<td align="left"><?php echo ucwords(stripslashes($RecordSet[$i]['title'])); ?></td>
			<?php if($_SESSION['AdminID']==1){ ?>
			<td align="center"><?php echo stripslashes(FetchValue('county','c_name','c_id',$RecordSet[$i]['place_id']));?> </td>
			<?php } ?>
			<td align="center">
				<input type="hidden" name="place[]" id="place<?php echo $i?>" value="<?php echo $RecordSet[$i]['place_id']?>" />
				<input type="checkbox" name="ad[]" id="ad<?php echo $i?>" value="<?php echo $RecordSet[$i]['id']?>" <?php if($RecordSet[$i]['play']==1){?> checked="checked" <?php } ?> />
			</td>
			<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $RecordSet[$i]['id']?>', 'DELETE');" /></td>
		</tr>
		<?php
				$r++;
				}
			}
		?>
		<tr>
			<td colspan="5" class="TopBorder" align="right">
				<?php if(count($RecordSet)) print $page_link; else print "&nbsp;"; ?>
			</td>
		</tr>
		<tr height="25">
			<td colspan="5" class="TopBorder">
				<table width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td width="43%">
							<input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);">
							<input type="submit" name="play1" value="UPDATE PLAYLIST" class="Btn" onClick="return ValidateSelection1(this.form);">
						</td>
						<td width="57%" align="right">
							<?php //echo $objPaging->show_paging()?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
