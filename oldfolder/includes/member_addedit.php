<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"member"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		$Photo = "<img src=../uploads/member/big/".stripslashes($Info[0]['image'])." width=75 height=75>";			
}else
	$MODE="ADD";

if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $Info[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}
?>
<style type="text/css">
.top {
	margin-bottom: 15px;
}
.messagebox {
	position:absolute;
	width:100px;
	margin-left:30px;
}
.messageboxok {
	position:absolute;
	width:auto;
	margin-left:5px;
	color:#008000;
}
.messageboxerror {
	position:absolute;
	width:auto;
	margin-left:5px;
	color:#CC0000;
}
</style>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	<?php if($MODE=='ADD'){?>
  	$("#Username").blur(function()
		{
			
			//remove all the class add the messagebox classes and start fading
			

		
			//check the username exists or not from ajax
			$.post("membername_chk.php",{ username:$(this).val() } ,function(data)
			{
				
				if(data=='yes') //if username not avaiable
				{
					$("#msgbox").removeClass().addClass('messagebox').text('Checking....').fadeIn("slow");
					$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						$(this).html('Username already exists').addClass('messageboxerror').fadeTo(900,1);
							if(document.frmAdmin.RegisterSubmit.disabled==false)
								document.frmAdmin.RegisterSubmit.disabled=true; 
						
					});
						
				}				
				else if(data=='no') 
				{
					$("#msgbox").removeClass().addClass('messagebox').text('').fadeIn("slow");					
					$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						//$(this).html('Email address is available').addClass('messageboxok').fadeTo(900,1);	
						if(document.frmAdmin.RegisterSubmit.disabled==true){
							document.frmAdmin.RegisterSubmit.disabled=false; 
						}
					});
				}
			});
	});
	
	$("#Email").blur(function()
		{
			
			//remove all the class add the messagebox classes and start fading
			

		
			//check the username exists or not from ajax
			$.post("memberemail_chk.php",{ email:$(this).val() } ,function(data)
			{
				if(data=='yes') //if username not avaiable
				{					
					$("#msgbox1").removeClass().addClass('messagebox').text('Checking....').fadeIn("slow");
					$("#msgbox1").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						$(this).html('Email already exists').addClass('messageboxerror').fadeTo(900,1);
							if(document.frmAdmin.RegisterSubmit.disabled==false)
								document.frmAdmin.RegisterSubmit.disabled=true; 
						
					});				;		
				}				
						
				else if(data=='no') 
				{					
					$("#msgbox1").removeClass().addClass('messagebox').text('').fadeIn("slow");					
					$("#msgbox1").fadeTo(200,0.1,function()  //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						//$(this).html('Email address is available').addClass('messageboxok').fadeTo(900,1);	
						if(document.frmAdmin.RegisterSubmit.disabled==true){
							document.frmAdmin.RegisterSubmit.disabled=false; 
						}
					});
				}
			});
	});
	<?php } ?>
	$("#frmAdmin").validate({
		rules: {			
			Position: "required",
			Email: {
				required: true,
				email: true
			},
			FirstName: "required",
			LastName: "required"	,
			Username: "required",
			Phone: "required",
			Password: {
				required: true,
				minlength: 6,
				maxlength: <?php echo PASSWORD_LIMIT?>
			}		
		},
		messages: {
			Username: "Please enter username",
			Password: {required: "Please enter password",
				minlength: "Please enter at least 6 characters",
				maxlength: "Password should not be more than <?php echo PASSWORD_LIMIT?> characters"},
			Position: "Please select position",
			Email: {
				required: "Please enter email address",
				email: "Please enter valid email address"
			},
			FirstName: "Please enter firstname",
			LastName: "Please enter lastname",
			Phone: "Please enter phone number"
		}
	});	
  });
</script>
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addmember.jpg" width="48" height="48" /></td>
		<td colspan="5" class="tbl_head" height="24"><?php echo $MODE;?> MEMBER</td>
	</tr>
	<tr>
		<td width="" colspan="6">
			<?php showMessage();?>
			<form method="post" action="manage_member.php" id="frmAdmin" name="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<?php if($_SESSION['AdminID']==1){ 
											
										?>
										<tr>
											<td class="fieldlabel" width="5%">Place</td>
											<td class="fieldarea">
												<select name="Place" class="InputBox" id="Place" style="width:300px;">
													<option value="">No Place</option>
													<?php echo FillCombo1('county','c_name','c_id',$pl,'');?>
												</select>
											</td>
										</tr>
										<?php }else{ ?>
										<tr>
											<td colspan="2">
												<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
											</td>
										</tr>
										<?php } ?>
										<tr>
											<td class="fieldlabel" width="5%">
												<label for="Place">Position</label>
											</td>
											<td class="fieldarea">
												<select name="Position" class="InputBox" id="Position" style="width:300px;">
													<option value="">--Select Position--</option>
													<?php
								echo FillCombo1('member_position','title','id',$Info[0]['position'],'');
							?>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">User Name</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="Username" id="Username" value="<?php echo $Info[0]['username']?>" style="width:300px;" />
												<span id="msgbox" style="display:none"></span> </td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Password</td>
											<td class="fieldarea">
												<input class="InputBox" type="password" name="Password" id="Password" value="<?php echo $Info[0]['password']?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">First Name</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="FirstName" id="FirstName" value="<?php echo stripslashes($Info[0]['firstname'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Last Name</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="LastName" id="LastName" value="<?php echo stripslashes($Info[0]['lastname'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Address</td>
											<td class="fieldarea">
												<textarea class="InputBox" name="Address" id="Address" style="width:300px;"><?php echo stripslashes($Info[0]['address'])?></textarea>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Address 2</td>
											<td class="fieldarea">
												<textarea class="InputBox" name="Address2" id="Address2" style="width:300px;"><?php echo stripslashes($Info[0]['address2'])?></textarea>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">City</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="City" id="City" value="<?php echo stripslashes($Info[0]['city'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">State</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="State" id="State" value="<?php echo stripslashes($Info[0]['state'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Country</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="Country" id="Country" value="<?php echo stripslashes($Info[0]['country'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Postal/Zip Code</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="Zipcode" id="Zipcode" value="<?php echo stripslashes($Info[0]['zipcode'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Email</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="Email" id="Email" value="<?php echo stripslashes($Info[0]['email'])?>" style="width:300px;" />
												<span id="msgbox1" style="display:none"></span> </td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Phone</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="Phone" id="Phone" value="<?php echo stripslashes($Info[0]['phone'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel">Photo/Image</td>
											<td class="fieldarea" valign="top">
												<input class="InputBox" name="CImage" id="CImage" style="width: 300px;" type="file">
												<input type="hidden" name="OldImage" id="OldImage" value="<?php echo stripslashes($Info[0]['image'])?>" />
											</td>
										</tr>
										<?php if($MODE=='UPDATE'){ ?>
										<tr>
											<td class="fieldlabel" colspan="2"><img src="../uploads/member/big/<?php echo stripslashes($Info[0]['image'])?>" width="75" height="100" /> </td>
										</tr>
										<?php } ?>
										<tr>
											<td class="fieldlabel"></td>
											<td class="fieldarea" valign="top">
												<?php
																if($Info[0]['attend_church']=='yes')
																	$check = "checked='checked'";
																else
																	$check = '';
															?>
												<input type="checkbox" name="attend" id="attend" <?php echo $check;?> />
												Do you want to attend the church ? </td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<div align="center">
									<input value="<?php echo $MODE?> MEMBER" class="Btn" type="submit" name="submit1" id="RegisterSubmit">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=member_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
