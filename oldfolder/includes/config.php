<?php
$sql = "select * from configuration";
$c = $objDB->select($sql);
$i=0;
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#configfrm").validate();
  });
</script>
<script type="text/javascript" language="javascript">
$(document).ready(function(){
$(".tabbox").css("display","none");
var selectedTab;
$(".tab").click(function(){
    var elid = $(this).attr("id");
    $(".tab").removeClass("tabselected");
    $("#"+elid).addClass("tabselected");
    if (elid != selectedTab) {
        $(".tabbox").slideUp();
        $("#"+elid+"box").slideDown();
        selectedTab = elid;
    }
    $("#tab").val(elid.substr(3));
});
selectedTab = "tab0";
$("#tab0").addClass("tabselected");
$("#tab0box").css("display","");
  });
</script>
<link href="css/style1.css" rel="stylesheet" type="text/css">
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/config.jpg" width="48" height="48" /></td>
		<td class="tbl_head" height="24">GENERAL SETTINGS</td>
	</tr>
	<tr>
		<td colspan="2" bgcolor="#F5F5F5">
			<div id="content_padded">
				<?php
					if(isset($_REQUEST['done']))
						showMessage();			
				?>
				<div id="tabs">
					<ul>
						<li id="tab0" class="tab tabselected" style="width:76px;"><a href="javascript:;" class="tabvisit">General</a></li>
						<li id="tab4" class="tab" style="width:76px;"><a href="javascript:;" class="tabvisit">Mail</a></li>
						<li id="tab5" class="tab" style="width:76px;"><a href="javascript:;" class="tabvisit">Uploads</a></li>
						<li id="tab9" class="tab" style="width:76px;"><a href="javascript:;" class="tabvisit">Security</a></li>
						<li id="tab10" class="tab" style="width:76px;"><a href="javascript:;" class="tabvisit">Others</a></li>
					</ul>
				</div>
				<form method="post" action="manage_config.php" name="configfrm" id="configfrm">
					<input name="action" value="UPDATE" type="hidden">
					<!-- General -->
					<div style="" id="tab0box" class="tabbox">
						<div id="tab_content">
							<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
								<tbody>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" title="*" validate="required:true" size="35" type="text" class="InputBox">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true,email:true" class="InputBox" type="text" id="ConfigValue<?php echo $i?>">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true,digits:true,min:1" class="InputBox" type="text" id="ConfigValue<?php echo $i?>">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- Mail -->
					<div style="display: none;" id="tab4box" class="tabbox">
						<div id="tab_content">
							<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
								<tbody>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<textarea class="TextArea" name="ConfigValue<?php echo $i?>" rows="4" title="*" validate="required:true" cols="60"><?php echo htmlspecialchars(stripslashes($c[$i]['config_value']))?></textarea>
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo $c[$i]['config_key']?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo $c[$i]['config_title']?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo $c[$i]['config_title']?> </td>
										<td class="fieldarea">
											<input class="InputBox" name="ConfigValue<?php echo $i?>" title="*" validate="required:true" size="60" value="<?php echo htmlspecialchars($c[$i]['config_value'])?>" />
											<?php echo $c[$i]['config_description']?>
											<input type="hidden" value="<?php echo $c[$i]['config_key']?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true,email:true" class="InputBox" type="text" id="ConfigValue<?php echo $i?>">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- Upload -->
					<div style="display: none;" id="tab5box" class="tabbox">
						<div id="tab_content">
							<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
								<tbody>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true" class="InputBox" type="text" id="ConfigValue<?php echo $i?>">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true" class="InputBox" type="text" id="ConfigValue<?php echo $i?>">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true" class="InputBox" type="text" id="ConfigValue<?php echo $i?>">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- Security -->
					<div style="display: none;" id="tab9box" class="tabbox">
						<div id="tab_content">
							<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
								<tbody>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" value="<?php echo stripslashes($c[$i]['config_value'])?>" size="35" title="*" validate="required:true,min:6" class="InputBox" type="text" id="ConfigValue<?php echo $i?>">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<!-- Others -->
					<div style="display: none;" id="tab10box" class="tabbox">
						<div id="tab_content">
							<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
								<tbody>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<textarea rows="5" cols="35" title="*" validate="required:true" class="TextArea" id="ConfigValue<?php echo $i?>" name="ConfigValue<?php echo $i?>"><?php echo stripslashes($c[$i]['config_value'])?></textarea>
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<table cellpadding="0" cellspacing="0">
												<tbody>
													<tr>
														<td>
															<select style="width:229px;" name="ConfigValue<?php echo $i?>" title="*" validate="required:true" class="InputBox">
																<?php echo FillCombo1('county','c_name','c_id',$c[$i]['config_value'],'');?>
															</select>
														</td>
														<td>&nbsp;<?php echo stripslashes($c[$i]['config_description'])?>
															<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
														</td>
													</tr>
												</tbody>
											</table>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" title="*" validate="required:true" value="<?php echo stripslashes($c[$i]['config_value'])?>" class="InputBox" size="35" type="text">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" title="*" validate="required:true" value="<?php echo stripslashes($c[$i]['config_value'])?>" class="InputBox" size="35" type="text">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" title="*" validate="required:true,email:true" value="<?php echo stripslashes($c[$i]['config_value'])?>" class="InputBox" size="35" type="text">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
									<tr>
										<td class="fieldlabel">
											<input type="hidden" value="<?php echo $c[$i]['config_id']?>" name="ConfigId<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_key'])?>" name="ConfigKey<?php echo $i?>"/>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_title'])?>" name="ConfigTitle<?php echo $i?>"/>
											<?php echo stripslashes($c[$i]['config_title'])?> </td>
										<td class="fieldarea">
											<input name="ConfigValue<?php echo $i?>" title="*" validate="required:true" value="<?php echo stripslashes($c[$i]['config_value'])?>" class="InputBox" size="35" type="text">
											<?php echo stripslashes($c[$i]['config_description'])?>
											<input type="hidden" value="<?php echo stripslashes($c[$i]['config_description'])?>" name="ConfigDesc<?php echo $i++?>"/>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<p align="center">
						<input value="Save Changes" class="button" type="submit" />
					</p>
					<input name="tab" id="tab" value="" type="hidden">
				</form>
			</div>
		</td>
	</tr>
</table>
