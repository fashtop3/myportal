<?php
if($_REQUEST['Process']=='Update'){
	$sql = "update curriculum set quarter_index = 0 where quarter_index = '".$_REQUEST['QIndex']."'";
	$objDB->sql_query($sql);
	
	$sql = "update curriculum set quarter_index = '".$_REQUEST['QIndex']."' where cdate between '".$_REQUEST['SDate']."' and '".$_REQUEST['EDate']."'";
	$objDB->sql_query($sql);
	
	$_SESSION['SuccessMsg'] = "Quarter Index Schedule (Sunday School) updated successfully!";
	header("Location:index.php?p=quarter_index");
	exit;
}
$index = FetchData("curriculum",array(),"order by quarter_index desc");
$quarter = FetchData("curriculum",array(),"where quarter_index='".$index[0]['quarter_index']."' order by cdate");
?>
<script type="text/javascript">
	$(function() {
		$('#SDate').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1910:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
		$('#EDate').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1910:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});
	$("#frmAdmin").validate();
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="images/sermon.jpg" width="48" height="48" /></td>
		<td class="tbl_head" height="24">QUARTER INDEX SCHEDULE (SUNDAY SCHOOL)</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<?php showMessage();	?>
			<form name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="Process" id="Process" value="Update" />
				<input type="hidden" name="QIndex" id="QIndex" value="<?php echo $index[0]['quarter_index']?>" />
				<table class="form" width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td align="left" valign="top" width="10%">Start Date:</td>
						<td align="left" valign="top">
							<input type="text" name="SDate" id="SDate" value="<?php echo $quarter[0]['cdate']?>" class="InputBox" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="10%">End Date:</td>
						<td align="left" valign="top">
							<input type="text" name="EDate" id="EDate" value="<?php echo $quarter[count($quarter)-1]['cdate']?>" class="InputBox" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top" width="10%">&nbsp;</td>
						<td align="left" valign="top">
							<input type="submit" value="UPDATE" class="Btn" />
							<input type="reset" value="RESET" class="Btn" />
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
