<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"menu"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);			
		
}else
	$MODE="ADD";
	
?>
<script>
  $(document).ready(function(){
		$("#frmAdmin").validate();
	});
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addcontactcategory.jpg" /></td>
		<td class="tbl_head" height="24">Front Menu</td>
	</tr>
	<tr>
		<td colspan="2">
			<form method="post" action="manage_menu.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>" />
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input type="hidden" value="<?php echo $MODE?>" name="action" />
				<table class="form" width="100%" cellpadding="5" cellspacing="1" border="0">
					<tr>
						<td class="fieldlabel" width="15%">Menu Title</td>
						<td class="fieldarea">
							<input type="text" name="Title" id="Title" size="30" title=" *" validate="required: true" class="InputBox" value="<?php echo stripslashes($Info[0]['title'])?>" />
						</td>
					</tr>
					<!--tr>
          <td class="fieldlabel">Parent Menu</td>
          <td class="fieldarea"><select name="Parent" id="Parent" class="InputBox" style="width:200px;">
              <option value="">--Select--</option>
              <?php echo FillCombo1("menu","title","id",$Info[0]['parent_id'],"where id != '".$Info[0]['id']."'");?>
            </select>
          </td>
        </tr-->
					<!--tr>
          <td class="fieldlabel" width="15%">Link</td>
          <td class="fieldarea"><input type="text" name="Links" id="Links" title=" *" validate="required: true,url: true" size="70" class="InputBox" value="<?php echo $Info[0]['title_link']?>" /></td>
        </tr-->
					<tr>
						<td align="center" colspan="2">
							<input value="<?php echo $MODE?>" class="Btn" type="submit" name="submit1" id="submit1">
							<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=menu_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
	</td>
	
</table>
