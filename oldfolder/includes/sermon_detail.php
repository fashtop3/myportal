<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"sermon"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder" ></script>
<script type="text/javascript" src="js/lightbox.js" ></script>
<link href="css/lightbox.css" rel="stylesheet" type="text/css">
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/sermon.jpg" width="48" height="48" /> </td>
		<td class="tbl_head" height="24">SERMON DETAIL</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" class="form">
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Occation : </b> <?php echo stripslashes(FetchValue1('occation','title','id',$Info[0]['occation']));?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Title : </b> <?php echo stripslashes($Info[0]['title'])?> </td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Sermon by : </b>
						<?php 
							 echo $Info[0]['sermonby'];
							 if($Info[0]['sermonby']==''){
			   	if($Info[0]['createdby']==0) 
					echo stripslashes(FetchValue1('admin','FirstName,LastName','UserID',$Info[0]['admin_createdby']));
				//else echo stripslashes(FetchValue1('member','firstname,lastname','id',$Info[0]['createdby']));
				}
				?>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px;"><b>Date : </b> <?php echo date('d-M-Y',strtotime($Info[0]['sdate']))?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<?php if($Info[0]['image']){ ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"><a rel="lightbox0" href="../uploads/sermon/image/big/<?php echo stripslashes($Info[0]['image']); ?>"> <img style="border:none" src="../uploads/sermon/image/big/<?php echo stripslashes($Info[0]['image']); ?>" width="50" height="50" title="Click hre to view"/> </a> <br />
						Click on the Image to enlarge it. </td>
				</tr>
				<?php } ?>
				<?php if($Info[0]['file']){ ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"><a href="../uploads/sermon/download/<?php echo stripslashes($Info[0]['file'])?>"> <img border="none" src="images/file.jpg" /> </a> <br />
						Click on the icon to open the file. </td>
				</tr>
				<?php } ?>
				<?php if($Info[0]['video']){ ?>
				<tr>
					<td colspan="2" style="padding-left:5px;">
						<!--<a href="../uploads/sermon/video/<?php echo stripslashes($Info[0]['video'])?>">-->
						<img border="none;" src="images/video.jpg" />
						<!--</a> <br />
                  Click on the icon to open the file.-->
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td colspan="2" style="padding-left:5px;"></td>
				</tr>
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;"><?php echo stripslashes($Info[0]['description'])?> </td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">
						<center>
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=sermon_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						</center>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
