<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"curriculum"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$Photo = "<img src=../uploads/curriculum/image/big/".stripslashes($Info[0]['image'])." width=75 height=75>";	
		$File = stripslashes($Info[0]['file']);
		$Video = stripslashes($Info[0]['video']);
}else
	$MODE="ADD";
	
if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $Info[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}	
?>
<script>
	/*$(function() {
		$('#CDate').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1975:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});*/
  $(document).ready(function(){
  	$("#BookNo").change(function()
		{
			$.post("get_lesson.php",{ id:$(this).val() } ,function(data)
			{
				$("#LessonNo").html(data);
			});
		});
		$("#LessonNo").change(function()
		{
			$.post("get_date.php",{ id:$(this).val() } ,function(data)
			{
				$("#CDate1").html(data);
			});
		});
		$('#submit1').click(function() {
			var con = tinyMCE.activeEditor.getContent(); // get the content
	
			$('#Content').val(con); // put it in the textarea
		});
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			CategoryId: "required"	,
			CDate : "required",
			Title: "required",
			LessonNo: "required"
		},
		messages: {
			CategoryId: "Please select category",
			CDate: "Please enter date",
			Title: "Please enter title",
			LessonNo: "Please enter lesson number"
		}
	});	
  });
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "Content,MemoryVerse,Description",
			theme : "advanced",
			
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 400,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
           
		}
	function dayvalue(id)
	{			
			$.post("curriculum_chk.php",{ username:id } ,function(data)
			{				
				document.getElementById('DayName1').innerHTML = data;
			});
	}
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
    <tr>
        <td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/curriculum.jpg" width="48" height="48" /></td>
        <td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> CURRICULUM</td>
    </tr>
    <tr>
        <td colspan="6" width=""><form method="post" action="manage_curriculum.php" id="frmAdmin" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
                <input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
                <table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
                    <tbody>
                        <tr>
                            <td><table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
                                    <tbody>
                                        <?php if($_SESSION['AdminID']==1){ 
											
										?>
                                        <tr>
                                            <td class="fieldlabel" width="5%">Place</td>
                                            <td class="fieldarea"><select name="Place" class="InputBox" id="Place" style="width:300px;" onchange="dayvalue(this.value)">
                                                    <?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
                                                </select>
                                            </td>
                                        </tr>
                                        <?php }else{ ?>
                                        <tr>
                                            <td colspan="2"><input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
                                            </td>
                                        </tr>
                                        <?php } ?>
                                        <tr>
                                            <td class="fieldlabel" width="15%">Book Number</td>
                                            <td class="fieldarea"><select class="InputBox" name="BookNo" id="BookNo" style="width:300px;">
                                                    <option value="">--Select Book--</option>
                                                    <?php 
												echo FillCombo1("book","book_no","id",$Info[0]['book_id'],"group by book_no");											
											?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="15%">Lesson Number</td>
                                            <td class="fieldarea"><select class="InputBox" name="LessonNo" id="LessonNo" style="width:300px;">
                                                    <option value="">--Select Lesson--</option>
                                                    <?php 
												$bid = stripslashes(GetFieldData("book","book_no","where id='".$Info[0]['book_id']."'"));
												echo FillCombo1("book","lesson_no","id",$Info[0]['lesson_no'],"where book_no='".$bid."'");
											?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="15%">Lesson Date</td>
                                            <td class="fieldarea" id="CDate1"><input type="text"  class="InputBox" name="CDate" id="CDate" value="<?php echo stripslashes($Info[0]['cdate'])?>" style="width:300px;" />
                                                (Enter only sunday date)
                                                <!-- onfocus="scwShow(this, this);" onclick="scwShow(this, this);" -->
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="15%">Category</td>
                                            <td class="fieldarea" id="DayName1"><select name="CategoryId" class="InputBox" id="CategoryId"  style="width:300px;">
                                                    <option value="">--Select Category--</option>
                                                    <?php if($_SESSION['PlaceID']!=0)
						  			$where = 'where id not in(1,2) and place_id='.$_SESSION['PlaceID'];
								else
									$where = 'where id not in(1,2) and place_id='.$pl;
						  echo FillCombo1('curriculum_category','title','id',$Info[0]['category_id'],$where);?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="15%">Title</td>
                                            <td class="fieldarea"><input type="text" class="InputBox" name="Title" id="Title" value="<?php echo stripslashes($Info[0]['title'])?>" style="width:300px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="15%">Bible Title (e.g. Joshua 10:1-27)</td>
                                            <td class="fieldarea"><input type="text" title=" Please enter bible title" validate="required: true" class="InputBox" name="FullTitle" id="FullTitle" value="<?php echo stripslashes($Info[0]['short_description'])?>" style="width:300px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="15%">Bible Author</td>
                                            <td class="fieldarea"><input type="text" class="InputBox" name="Author" id="Author" value="<?php echo stripslashes($Info[0]['bible_author'])?>" style="width:300px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="15%">Bible Description</td>
                                            <td class="fieldarea"><textarea class="TextArea" name="Description" rows="30" id="Description" style="width:300px;"><?php echo stripslashes($Info[0]['bible_description'])?></textarea>
                                            </td>
                                        </tr>
                                        <!--tr>
                      <td class="fieldlabel" width="15%">Full Title</td>
                      <td class="fieldarea"><textarea class="TextArea" name="FullTitle" id="FullTitle" style="width:300px;"><?php echo $Info[0]['short_description']?></textarea>
                      </td>
                    </tr-->
                                        <tr>
                                            <td class="fieldlabel" width="15%">Memory Verse</td>
                                            <td class="fieldarea"><textarea class="TextArea" name="MemoryVerse" id="MemoryVerse" style="width:300px;"><?php echo stripslashes($Info[0]['memory_verse'])?></textarea>
                                            </td>
                                        </tr>
                                        <!--
                <tr>
                  <td class="fieldlabel">Image</td>
                  <td class="fieldarea" valign="top"><input name="CImage" id="CImage" class="input" style="width: 300px;" type="file">
                    <input type="hidden" id="OldImage" name="OldImage" value="<?php echo $Info[0]['image']?>" >
                  </td>
                </tr>
                <?php if($Info[0]['image']!=''){ ?>
                <tr>
                  <td class="fieldlabel"></td>
                  <td class="fieldarea" valign="top"><?php echo $Photo;?> </td>
                </tr>
                <?php } ?>
                -->
                                        <tr>
                                            <td class="fieldlabel">Word/Document File</td>
                                            <td class="fieldarea" valign="top"><input title="Please enter Doc file" name="CDown" id="CDown" class="input" style="width: 300px;" accept="DOC|DOCX" type="file">
                                                <input type="hidden" id="OldDown" name="OldDown" value="<?php echo stripslashes($Info[0]['file'])?>" >
                                                <?php 
					  	if($File)
					  		echo "<img src='images/word.jpg'>";
					  ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel">PDF File</td>
                                            <td class="fieldarea" valign="top"><input title="Please enter PDF file" class="input" name="CVideo" id="CVideo" accept="PDF" style="width: 300px;" type="file">
                                                <input type="hidden" id="OldVideo" name="OldVideo" value="<?php echo stripslashes($Info[0]['video'])?>" >
                                                <?php 
					  	if($Video)
					  		echo "<img src='images/pdf.jpg'>";
					  ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel">Description</td>
                                            <td class="fieldarea" valign="top"><textarea name="Content" cols="65" rows="25" id="Content" style="width: 500px;" ><?php echo stripslashes($Info[0]['full_description'])?></textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table></td>
                        </tr>
                        <tr>
                            <td><input type="hidden" value="<?php echo $MODE?>" name="action" />
                                <div align="center">
                                    <input value="<?php echo $MODE?> CURRICULUM" class="Btn" type="submit" name="submit1" id="submit1">
                                    <input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=curriculum_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
                                </div></td>
                        </tr>
                    </tbody>
                </table>
            </form></td>
    </tr>
</table>
