<?php
	$sql = "select * from camp_meeting where donation_id in (select id from donation where type='Camp Meeting' and status=1)";	
	
	if($_REQUEST['Search']!='')
		$sql .= " and title like '%".$_REQUEST['Search']."%'";		
	$sql .= " order by id desc";
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_camp_meeting.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_camp_meeting.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="images/meeting.gif" width="48" height="48" /></td>
		<td class="tbl_head" height="24">CAMP MEETING</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<?php showMessage();	?>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
				<tr>
					<td colspan="6" class="content" height="25">
						<form name="FrmSMS1" method="post" action="" id="FrmSMS1">
							<input type="hidden" name="ID" id="ID" value="">
							<input type="hidden" name="Process" id="Process" value="">
							Search by Title:
							<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="InputBox" />
							<input type="submit" value="Search" class="Btn" />
						</form>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="content" height="25">
						<input type="Button" name="ADDNEW" value="ADD NEW" class="Btn" onClick="window.location.href='index.php?p=meeting_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
						<?php
					$uk = FetchData("camp_user",array(),"where type='UK DELEGATES'");
					$cuk = count($uk);
					$uk = FetchData("camp_user",array(),"where type='OVERSEAS DELEGATES'");
					$ouk = count($uk);
					$uk = FetchData("camp_user",array(),"where type='Overseas Family'");
					$fuk = count($uk);
					?>
						<input type="Button" name="UK" value="UK DELEGATE     (<?php echo $cuk;?>)" class="Btn" onClick="window.location.href='index.php?p=uk_camp'"/>
						<input type="Button" name="OVER" value="OVERSEAS DELEGATE     (<?php echo $ouk;?>)" class="Btn" onClick="window.location.href='index.php?p=over_camp'"/>
						<input type="Button" name="Family" value="OVERSEAS FAMILY     (<?php echo $fuk;?>)" class="Btn" onClick="window.location.href='index.php?p=family_camp'"/>
						<input type="Button" name="accomod" value="ACCOMODATION     (ALL USERS)" class="Btn" onClick="window.location.href='index.php?p=accmodation'"/>
					</td>
					<td height="25" colspan="2" align="right"  class="BottomBorder"><?php echo $objPaging->show_paging()?></td>
				</tr>
				<tr>
					<td colspan="6" class="BottomBorder"></td>
				</tr>
				<td colspan="6">
						<form name="FrmSMS" method="post" action="" id="FrmSMS">
							<input type="hidden" name="ID" id="ID" value="">
                            <input type="hidden" name="pg_no" id="pg_no" value="">
							<input type="hidden" name="Process" id="Process" value="">
							<table width="100%" cellspacing="0" cellpadding="5">
								<tr height="25" class="SmallBlackHeading">
									<td width="4%" align="center" class="BottomBorder">
										<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
									</td>
									<td width="5%" align="center" class="BottomBorder">#</td>
									<td width="" align="left" class="BottomBorder">Title</td>
									<td width="" align="left" class="BottomBorder">Price</td>
									<td width="" align="left" class="BottomBorder">Category</td>
									<td width="" align="left" class="BottomBorder">Type</td>
									<td align="center" class="BottomBorder">Options</td>
								</tr>
								<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
								<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
									<td align="center">
										<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i]['id']?>">
									</td>
									<td align="center" class="Numbers"><?php echo $i+1?></td>
									<td align="left"><?php echo ucwords(stripslashes($cms[$i]['title'])); ?></td>
									<td align="left"><?php echo CURRENCY_SIGN.ucwords($cms[$i]['price']); ?></td>
									<td align="left"><?php echo ucwords(stripslashes($cms[$i]['category'])); ?></td>
									<td align="left"><?php echo stripslashes(GetFieldData("donation","title","where id = ".$cms[$i]['donation_id'])); ?></td>
									<td align="center" class="BlackMediumNormal"><img src="images/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=meeting_addedit&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="images/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" /></td>
								</tr>
								<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
								<tr>
									<td colspan="6" class="<?php echo $class?>" align="right">
										<?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
									</td>
								</tr>
								<tr height="25">
									<td colspan="7" class="TopBorder">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td width="13%">
													<input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);">
												</td>
												<td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
