<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"cd_category"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);	
		$Photo = "<img src='../uploads/contact/big/".stripslashes($Info[0]['image'])."' width=75 height=75>";	
		
}else
	$MODE="ADD";
	
?>
<script>
  $(document).ready(function(){
  	
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required",
			Type: "required"			
		},
		messages: {
			Title: "Please enter title",
			Type: "Please select type"
		}
	});	
  });
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addcontactcategory.jpg" width="48" height="48" /></td>
		<td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> MUSIC &amp; TRACK CATEGORY</td>
	</tr>
	<tr>
		<td colspan="6" width="">
			<form method="post" action="manage_cd_category.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="5%">Type</td>
											<td class="fieldarea">
												<select name="Type" class="InputBox" id="Type" style="width:300px;">
													<option value="">--Select--</option>
													<option <?php if($Info[0]['type']=='CD') echo "selected";?> value="CD">CDs</option>
													<option <?php if($Info[0]['type']=='DVD') echo "selected";?> value="DVD">DVDs</option>
													<option <?php if($Info[0]['type']=='HM') echo "selected";?> value="HM">Higherway Magazine</option>
													<option <?php if($Info[0]['type']=='MT') echo "selected";?> value="MT">Music &amp; Track</option>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Title</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Title" id="Title" value="<?php echo stripslashes($Info[0]['title'])?>" style="width:300px;" />
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<br>
								<div align="center">
									<input value="<?php echo $MODE?>" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=music_category&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
