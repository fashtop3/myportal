<?php
	$MODE="ADD";
	$PANEL_HEADING="CATEGORY";

	if(isset($_REQUEST['cms_id']) && !empty($_REQUEST['cms_id']))
	{
		$MODE="UPDATE";

		$TblFieldsArr = array
		(
			//table name=>feilds name
			"slider"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['cms_id'];
		$Sort="";
		$Limit="";

		$info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
	}
?>
<script type="text/javascript">
$(document).ready(function(){
	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#ContentDesc').val(con); // put it in the textarea
	});
	$("#frmAdmin").validate();
});
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "ContentDesc",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 400,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
           
		}
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
    <tr>
        <td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addmember.jpg" width="48" height="48" /></td>
        <td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> BANNER</td>
    </tr>
    <tr>
        <td colspan="6" width="" align="center"><?php
							showMessage();			
						?>
            <?php if($MODE=='UPDATE') { ?>
            <span style="float:right"><img src="../uploads/banner/<?php echo $info[0]['image'];?>" width="300" /></span>
            <?php } ?>
            <form action="manage_banner.php" method="post" enctype="multipart/form-data" id="frmAdmin" name="FrmCategoryAddEdit">
                <input type="hidden" name="Process" value="<?php echo $MODE?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
                <input type="hidden" name="OldImage" value="<?php echo $info[0]['image'];?>">
                <input type="hidden" name="OldPImage" value="<?php echo $info[0]['popup_image'];?>">
                <input type="hidden" name="TOldImage" value="<?php echo $info[0]['thumb_icon'];?>">
                <input type="hidden" name="cms_id" value="<?php echo $_REQUEST['cms_id']?>">
                <table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
                    <tbody>
                        <tr>
                            <td><table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
                                    <tbody>
                                        <tr>
                                            <td width="17%" class="fieldlabel" valign="top" align="left"><div align="">Title </div></td>
                                            <td width="83%" class="fieldarea" valign="top" align="left"><input name="Title" title=" *" validate="required:true" value="<?php echo stripslashes($info[0]['title']);?>" id="Title" class="input" style="width: 300px;" type="text">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17%" valign="top" class="fieldlabel" align="left"><div align="">Image </div></td>
                                            <td width="83%" valign="top" class="fieldarea" align="left"><input type="file" name="CImage" id="CImage" <?php if($MODE=='ADD'){ ?>title=" *" validate="required:true"<?php } ?> accept="JPG|JPEG|GIF|PNG" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left" colspan="2" style="border-bottom:1px solid dashed #CCCCCC">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="17%" valign="top" class="fieldlabel" align="left"><div align="">Popup Image </div></td>
                                            <td width="83%" valign="top" class="fieldlabel" align="left"><input type="file" name="PImage" id="PImage" accept="JPG|JPEG|GIF|PNG" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17%" valign="top" align="left"><div align=""> </div></td>
                                            <td width="83%" valign="top" align="left">OR
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="17%" valign="top" class="fieldlabel" align="left"><div align="">Link </div></td>
                                            <td width="83%" valign="top" class="fieldlabel" align="left"><input type="text" name="Links" id="Links" value="<?php echo $info[0]['url']?>" class="input" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td valign="top" align="left" colspan="2" style="border-bottom:1px solid dashed #CCCCCC">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td width="17%" valign="top" class="fieldlabel" align="left"><div align="">Description </div></td>
                                            <td width="83%" valign="top" class="fieldarea" align="left"><textarea title=" *" validate="required:true" name="ContentDesc" cols="65" rows="5" class="input" id="ContentDesc" style="width: 300px;" ><?php echo stripslashes($info[0]['description']) ?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td><input name="submit" id="submit1" value="<?php echo ucwords(strtolower($MODE));?> Now" class="Btn" type="submit">
                                                &nbsp;
                                                <input name="Back" value="Back" class="Btn" type="button" onclick="window.location.href='index.php?p=banners&pg_no=<?php echo $_REQUEST['pg_no']?>'">
                                            </td>
                                        </tr>
                                    </tbody>
                                </table></td>
                        </tr>
                </table>
                <?php if($info[0]['popup_image'] != ''){ ?>
                Popup Image<br />
                <img src="../uploads/banner/<?php echo $info[0]['popup_image']?>" width="450" alt="" />
                <?php } ?>
            </form></td>
    </tr>
</table>
