<?php
	$sql = "select * from testimonial";
	if($_SESSION['AdminID']==1){
		if($_REQUEST['plc']!='')
			$sql.=' where place_id='.$_REQUEST['plc'];
		else
			$sql.=' where 1=1';	
	}
	else
		$sql.=' WHERE place_id='.$_SESSION['PlaceID'];
		
	if($_REQUEST['Keyword']!=''){
		$sql .= " and createdby in( select id from member where firstname like '%".$_REQUEST['Keyword']."%' or lastname like '%".$_REQUEST['Keyword']."%')";
	}
	
	if($_REQUEST['admins']!='')
		$sql .= ' and admin_createdby = '.$_REQUEST['admins'];
	$sql.=" group by createdby,admin_createdby";
	
	
	//=================================================
	
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());
	if($cms)
		$_SESSION['FoundMsg'] = '';
	else
		$_SESSION['SuccessMsg'] = 'No Record Found';	
	
?>
<script type="text/javascript" language="javascript">

function confirmMail(frm)
{
		if(confirm("Do you really want to Mail?"))
		{		
			frm.Process.value='MULTIPLEDELETE';
			frm.QueryString.value='<?php echo $query_var?>';
			frm.action='manage_testi.php';	
			frm.submit();
		}	
}
function ValidateSelection(frm)
{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('testi'+i).checked)
			{
				x=false;
				confirmMail(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
}

	$(document).ready(function(){   
		$(".tabbox").css("display","none");
		var selectedTab;
		$(".tab").click(function(){
			var elid = $(this).attr("id");
			$(".tab").removeClass("tabselected");
			$("#"+elid).addClass("tabselected");
			$(".tabbox").slideUp();
			if (elid != selectedTab) {
				selectedTab = elid;
				$("#"+elid+"box").slideDown();
			} else {
				selectedTab = null;
				$(".tab").removeClass("tabselected");
			}
			$("#tab").val(elid.substr(3));
		});
	});
	
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
  <tr>
    <td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/testimonial.jpg" width="48" height="48" /></td>
    <td class="tbl_head" height="24">TESTIMONIAL MANAGER</td>
  </tr>    
  <tr>
    <td colspan="2" width="100%"><?php showMessage();	?><form name="FrmSMS" method="post" action="" id="FrmSMS">
        <input type="hidden" name="ID" id="ID" value="">
        <input type="hidden" name="Process" id="Process" value="">
        <table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
          <tr>
            <td colspan="5" class="content"><?php if($_SESSION['AdminID']==1){ ?>
              <select style="width:125px;" name="PlaceValue" class="InputBox" id="PlaceValue" onchange="if(this.value!='') window.location='index.php?p=testimonials&plc='+this.value; else window.location='index.php?p=testimonials';">
                <option value="">All Places</option>
                <?php echo FillCombo1('county','c_name','c_id',$_REQUEST['plc'],'where site!=0');?>
              </select>
              <select style="width:165px;" name="admins" class="InputBox" id="admins" onchange="if(this.value!='') window.location='index.php?p=testimonials&admins='+this.value; else window.location='index.php?p=testimonials';">
                <option value="">--Select Administrator--</option>
                <?php echo FillCombo2('admin','FirstName,LastName','UserID',$_REQUEST['admins'],'where UserID!='.$_SESSION['AdminID']);?>
              </select>
              &nbsp;
              <?php } ?>Search by name: <input type="text" name="Keyword" id="Keyword" value="<?php echo $_REQUEST['Keyword']?>" class="InputBox" size="19" />
              <input type="submit" value="Search" class="Btn" />
              <span class="column_head" style="float:right; padding-top:2px;">Total
              No of Results:&nbsp;<?php echo count($cms)?></span> </td>
          </tr>
          <tr>
            <td colspan="3" class="BottomBorder" height="25"><input type="Button" name="ADDNEW" value="ADD NEW" class="Btn" onClick="window.location.href='index.php?p=testimonial_addedit'"/>
              <input type="Button" name="sermons" value="MY TESTIMONIALS" class="Btn" onClick="window.location.href='index.php?p=testimonial_list&user=admin&member_id=<?php echo $_SESSION['AdminID']; ?>'"  style="width:140px;"/>
              <input type="button" value="SHOW ALL" class="Btn" onclick="window.location='index.php?p=testimonials'" /> 
            </td>
            <td height="25" colspan="2" align="right"  class="BottomBorder"><?php echo $objPaging->show_paging()?></td>
          </tr>
          <tr>
            <td colspan="4" class="BottomBorder"></td>
          </tr>
          <tr height="25" class="SmallBlackHeading">
            <td width="4%" align="center" class="BottomBorder">&nbsp;</td>
            <td width="5%" align="center" class="BottomBorder">#</td>
            <td width="" align="left" class="BottomBorder">Member Name</td>
            <?php if($_SESSION['AdminID']==1){ ?>
            <td width="25%" align="center" class="BottomBorder">Place</td>
            <?php } ?>
            <td align="center" class="BottomBorder">Options</td>
          </tr>
          <?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
          <tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
            <td align="center">&nbsp;</td>
            <td align="center" class="Numbers"><?php echo $i+1?></td>
            <td align="left"><?php 
				  	if($cms[$i]['createdby']==0){
						echo stripslashes(FetchValue1('admin','FirstName,LastName','UserID',$cms[$i]['admin_createdby']));
						$user = 'admin';
						$uid = $cms[$i]['admin_createdby'];
					}
					else {
						echo stripslashes(FetchValue1('member','firstname,lastname','id',$cms[$i]['createdby']));
						$user = 'member';
						$uid = $cms[$i]['createdby'];
					}
					?></td>
            <?php if($_SESSION['AdminID']==1){ ?>
            <td align="center"><?php echo stripslashes(FetchValue('county','c_name','c_id',$cms[$i]['place_id']));?> </td>
            <?php } ?>
            <td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=testimonial_list&user=<?php echo $user?>&member_id=<?php echo $uid; ?>';" />&nbsp; </td>
          </tr>
          <?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
          <tr>
            <td colspan="4" class="<?php echo $class?>" align="right"><?php if(count($cms)) print $page_link; else print "&nbsp;"; ?></td>
          </tr>
          <tr height="25">
            <td colspan="5" class="TopBorder"><table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                  <td width="13%">&nbsp;</td>
                  <td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
                </tr>
              </table></td>
          </tr>
        </table>
      </form></td>
  </tr>
</table>
