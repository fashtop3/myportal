<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"news_gallery_category"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);		
}else
	$MODE="ADD";
if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $Info[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}	
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required"/*,
			NewsId:"required"*/
		},
		messages: {
			/*NewsId: "Please select news",*/
			Title:"Please enter title"
		}
	});	
  });
  function dayvalue(id)
	{			
			$.post("gallery_chk.php",{ username:id } ,function(data)
			{				
				document.getElementById('DayName1').innerHTML = data;
			});
	}
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addphotogallery.jpg" height="48" /></td>
		<td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> IMAGE GALLERY</td>
	</tr>
	<tr>
		<td colspan="6" width="">
			<form method="post" action="manage_news_gallery.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<?php if($_SESSION['AdminID']==1){ 
											
										?>
										<tr>
											<td class="fieldlabel" width="5%">Place</td>
											<td class="fieldarea">
												<select name="Place" class="InputBox" id="Place" style="width:300px;" onchange="dayvalue(this.value)">
													<?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
												</select>
											</td>
										</tr>
										<?php }else{ ?>
										<tr>
											<td colspan="2">
												<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
											</td>
										</tr>
										<?php } ?>
										<tr>
											<td class="fieldlabel" width="5%">Title</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Title" id="Title" style="width:300px;" value="<?php echo stripslashes($Info[0]['title'])?>" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">News</td>
											<td class="fieldarea" id="DayName1">
												<select class="InputBox" name="NewsId" id="NewsId" style="width:300px;">
													<option value="">--Select News--</option>
													<?php if($_SESSION['PlaceID']!=0)
						  			$where = 'where place_id='.$_SESSION['PlaceID'];
								else
									$where = $where = 'where place_id='.$pl;
								echo FillCombo1('news','title','id',$Info[0]['news_id'],$where);
							?>
												</select>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<br>
								<div align="center">
									<input value="<?php echo $MODE?> GALLERY" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=news_gallery_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
