<?php
	include('../../utils/config.php');
	include('../../utils/dbclass.php');
	include('../../utils/functions.php');
	$objDB = new MySQLCN;

	$PANEL_HEADING="Prayer Detail";
	$Tbl="prayer";
	
	// -------------- REQUEST A CALL -----------------
	$SQL = FetchQuery($Tbl,$FieldsArr,'where p_id='.$_REQUEST['id'],$Sort,$Limit);
	$RecordSet = $objDB->select($SQL);		
	
	if(isset($_REQUEST['process']) && !empty($_REQUEST['process']) && $_REQUEST['process']=="SendReply"){
		$result = $objDB->sql_query("select * from prayer where p_id=".$_REQUEST['id']);
		
		$Subject = "Request a Prayer";
		$To = $result[0]['p_email'];
		$From = "Apostolic Faith Mission UK";
		
		$Template="../../mail_templates/general.html";
		$TemplateVars=array(
							'Name'=>stripslashes($result[0]['p_name']),
							'Message'=>$_REQUEST['desc']);
			
		$flag = SendEmail($From, $To, $Subject, $Template, $TemplateVars);
		if($flag){
			echo '<script type="text/javascript">';
			echo 'alert("Mail Sent Successfully!");';
			echo 'window.close();';
			echo '</script>';
		}else{
			echo '<script type="text/javascript">';
			echo 'alert("Error! while sending Mail.");';
			echo 'window.close();';
			echo '</script>';
		}
	}
?>
<script type="text/javascript" src="../js/jquery.js"></script>
<script type="text/javascript" src="../js/jquery.metadata.js"></script>
<script language="javascript" type="text/javascript" src="../js/jquery.validate.js"></script>
<script type="text/javascript">
$.metadata.setType("attr", "validate");	
</script>
<script type="text/javascript">
$(document).ready(function(){
	$("#FrmQuoteDetail").validate();
});
</script>
<style type="text/css">
label.error {
	color:#FF0000;
}
</style>
<link href="../css/style.css" type="text/css" rel="stylesheet" />
<body bgcolor="#E6E3BD">
<form name="FrmQuoteDetail" id="FrmQuoteDetail" method="post" action="">
	<input type="hidden" name="process" value="SendReply">
	<input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" >
	<table width="100%" border="0" bgcolor="#F2EFE6" cellspacing="0" cellpadding="5" class="tbl_border">
		<tr>
			<td height="25" colspan="2" align="center" class="tbl_head"><?php echo $PANEL_HEADING?></td>
		</tr>
		<tr>
			<th width="18%" align="left" class="SmallBlackMedium" scope="row">Name</th>
			<td width="82%" class="SmallBlackNormal"><?php echo stripslashes($RecordSet[0]['p_name']); ?></td>
		</tr>
		<tr>
			<th scope="row" class="SmallBlackMedium" align="left" valign="top">Email</th>
			<td class="SmallBlackNormal"><?php echo stripslashes($RecordSet[0]['p_email'])?></td>
		</tr>
		<tr>
			<th align="left" class="SmallBlackMedium" scope="row">Place</th>
			<td class="SmallBlackNormal">
				<?php if($RecordSet[0]['p_place']!='') echo stripslashes(FetchValue("county","c_name","c_id",$RecordSet[0]['place_id'])); else{ 
										$pl = FetchData("county",array(),""); 
										$pl1=array(); 
										for($p=0;$p<count($pl);$p++) 
											$pl1[] = str_replace(","," -",stripslashes($pl[$p]['c_name'])); 
										echo @implode(', ',$pl1);
										} ?>
			</td>
		</tr>
		<tr>
			<th align="left" valign="top" class="SmallBlackMedium" scope="row">Prayer</th>
			<td class="SmallBlackNormal"><?php echo stripslashes($RecordSet[0]['p_prayer']); ?></td>
		</tr>
		<tr>
			<th align="left" valign="top" class="SmallBlackMedium" scope="row">Description</th>
			<td class="SmallBlackNormal">
				<textarea title=" *" validate="required:true" name="desc" rows="5" cols="40" ></textarea>
			</td>
		</tr>
		<tr>
			<th scope="row">&nbsp;</th>
			<td>
				<input type="submit" value="Reply" class="Btn" />
			</td>
		</tr>
	</table>
</form>
</body>
