<?php

	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"admin"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE UserID =".$_SESSION['AdminID'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		$Photo = "<img src='../uploads/member/big/".stripslashes($Info[0]['image'])."' width=75 height=75>";			

?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			UserName: {
				required: true
			},
			Password: {
				required: true,
				minlength: 6,
				maxlength: <?php echo PASSWORD_LIMIT?>
				},
			Email: {
				required: true,
				email: true
			},
			FirstName: "required",
			LastName: "required"		
		},
		messages: {
			UserName: "Please enter username",
			Password: {required: "Please enter password",
				minlength: "Please enter at least 6 characters",
				maxlength: "Password should not be more than <?php echo PASSWORD_LIMIT?> characters"},
			Email: {
				required: "Please enter email address",
				email: "Please enter valid email address"
			},
			FirstName: "Please enter firstname",
			LastName: "Please enter lastname"
		}
	});
  });
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF" align="center">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/admin.png" width="48" height="48" /></td>
		<td class="tbl_head" height="24">ADMIN MANAGER</td>
	</tr>
	<tr>
		<td width="" colspan="2">
			<?php
							showMessage();			
						?>
			<form method="post" id="frmAdmin" action="manage_administrator.php" name="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_SESSION['AdminID']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="5%">User Name</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="UserName" id="UserName" value="<?php echo $Info[0]['UserName']?>" style="width:300px;" />
												<span id="msgbox" style="display:none"></span> </td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Password</td>
											<td class="fieldarea">
												<input type="password" class="InputBox" name="Password" id="Password" value="<?php echo $Info[0]['Password']?>" style="width:300px;" />
												<input type="hidden" value="<?php echo $Info[0]['place_id'];?>" name="Place" id="Place" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">First Name</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="FirstName" id="FirstName" value="<?php echo stripslashes($Info[0]['FirstName'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Last Name</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="LastName" id="LastName" value="<?php echo stripslashes($Info[0]['LastName'])?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Address</td>
											<td class="fieldarea">
												<textarea name="Address" class="InputBox" id="Address" style="width:300px;"><?php echo stripslashes($Info[0]['address'])?></textarea>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Phone/Mobile</td>
											<td class="fieldarea">
												<input name="Phone" class="InputBox" id="Phone" style="width:300px;" value="<?php echo stripslashes($Info[0]['Phone'])?>" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Email</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Email" id="Email" value="<?php echo stripslashes($Info[0]['Email'])?>" style="width:300px;" />
												<span id="msgbox1" style="display:none"></span> </td>
										</tr>
										<tr>
											<td class="fieldlabel">Photo/Image</td>
											<td class="fieldarea" valign="top">
												<input name="CImage" class="InputBox" id="CImage" class="input" style="width: 300px;" type="file">
												<input type="hidden" id="OldImage" name="OldImage" value="<?php echo stripslashes($Info[0]['image'])?>" >
											</td>
										</tr>
										<tr>
											<td class="fieldlabel"></td>
											<td class="fieldarea" valign="top"><?php echo $Photo;?> </td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<input type="hidden" value="admin" name="apath" />
								<br>
								<div align="center">
									<input value="<?php echo $MODE?>" class="Btn" type="submit" name="submit1" id="RegisterSubmit">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
