<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"book"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);		
}else
	$MODE="ADD";
?>
<script>
  $(document).ready(function(){  	
    // validate signup form on keyup and submit
		$("#frmAdmin").validate({
			rules: {
				No: "required",
				Lesson:"required",
				LDate:"required"
			},
			messages: {
				No: "Please enter book number",
				Lesson:"Please enter lesson number",
				LDate: "Please enter lesson date"
			}
		});	
  });
	$(function() {
		$('#LDate').datepicker({
			beforeShowDay: nonWorkingDates,
			changeMonth: true,
			changeYear: true,
			yearRange: '1975:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/book.gif" width="48" height="48" /></td>
		<td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> BOOK</td>
	</tr>
	<tr>
		<td colspan="6" width="">
			<form method="post" action="manage_book.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="15%">Book No.</td>
											<td class="fieldarea" id="DayName1">
												<input name="No" id="No" style="width: 250px;" class="InputBox" value="<?php echo stripslashes($Info[0]['book_no'])?>" />
											</td>
										</tr>
										<!--<tr>
                                 <td class="fieldlabel" width="5%">Book Name</td>
                                 <td class="fieldarea"><input name="Name" id="Name" style="width: 250px;" class="InputBox" value="<?php echo $Info[0]['book_name']?>" />
                                 </td>
                              </tr>-->
										<tr>
											<td class="fieldlabel" width="15%">Lesson No.</td>
											<td class="fieldarea" id="DayName1">
												<input name="Lesson" id="Lesson" style="width: 250px;" class="InputBox" value="<?php echo stripslashes($Info[0]['lesson_no'])?>" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%">Lesson Date</td>
											<td class="fieldarea" id="DayName1">
												<input name="LDate" id="LDate" style="width: 250px;" class="InputBox" value="<?php echo $Info[0]['lesson_date']?>" />
												(Enter only sunday date) </td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<br>
								<div align="center">
									<input value="<?php echo $MODE?> BOOK" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=book_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
