<script type="text/javascript">
$(document).ready(function(){  		
	$("#frmCamp").validate();
});
</script>
<?php
if($_SESSION['id1']=='')
	$_SESSION['id1'] = GetFieldData("camp_payment","id","where user_id=".$_SESSION['Desc']);
if($_SESSION['id1']!=''){
	$uid = GetFieldData("camp_payment","user_id","where id=".$_SESSION['id1']);
	$user = FetchData("camp_user",array(),"where id=".$uid);
	$req = FetchData("camp_request",array(),"where user_id=".$uid);
	$cid = GetFieldData("camp_meeting_user","camp_id","where user_id=".$uid);
	$did = GetFieldData("camp_meeting","donation_id","where id=".$cid);
	$type = GetFieldData("camp_meeting","category","where id=".$cid);
}
if($_SESSION['Desc']=='')
	$_SESSION['Desc'] = $_REQUEST['Desc'];
if(count($_REQUEST['Name'])> 0){
	$_SESSION['Name'] = $_REQUEST['Name'];
}
?>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="images/meeting.gif" width="48" height="48" /></td>
		<td class="tbl_head" height="24">CAMP MEETING - EDIT USER REGISTRATION</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<form action="manage_camp_user.php" method="post" name="frmCamp" id="frmCamp">
				<input value="EDITCAMP" type="hidden" name="Process" id="Process" />
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input type="hidden" value="<?php echo $_SESSION['Desc'];?>" name="Desc" />
				<table width="100%" border="0" cellspacing="2" class="form" cellpadding="3">
					<tr>
						<td height="28" align="left" valign="top">
							<h3><?php echo $user[0]['description'];?></h3>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">
							<?php
				$per = FetchData("camp_meeting",array(),"where donation_id='".$did."' and category='".$type."' order by price desc");
				?>
							<table class="content" width="100%" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td width="25%" align="left" valign="top">Your name</td>
									<td align="left" valign="top">
										<input value="<?php echo stripslashes($user[0]['name'])?>" type="text" title="*" validate="required:true" name="Name" id="Name" class="InputBox" style="width:250px; height:20px;" />
									</td>
								</tr>
								<tr>
									<td width="25%" align="left" valign="top">Your Email</td>
									<td align="left" valign="top">
										<input disabled="disabled" value="<?php echo stripslashes($user[0]['email'])?>" type="text" title="*" validate="required:true,email:true" name="Email" id="Email" class="InputBox" style="width:250px; height:20px;" />
									</td>
								</tr>
								<tr>
									<td width="25%" align="left" valign="top">Your Phone No.</td>
									<td align="left" valign="top">
										<input value="<?php echo stripslashes($user[0]['phone'])?>" type="text" title="*" validate="required:true" name="Phone" id="Phone" class="InputBox" style="width:250px; height:20px;" />
									</td>
								</tr>
								<tr>
									<td width="25%" align="left" valign="top">Your Mobile No.</td>
									<td align="left" valign="top">
										<input value="<?php echo stripslashes($user[0]['mobile'])?>" type="text" name="Mobile" id="Mobile" class="InputBox" style="width:250px; height:20px;" />
									</td>
								</tr>
								<tr>
									<td width="25%" align="left" valign="top">Your Address</td>
									<td align="left" valign="top">
										<textarea name="Address" id="Address" class="TextArea" style="width:250px; height:75px;"><?php echo stripslashes($user[0]['address'])?></textarea>
									</td>
								</tr>
								<tr>
									<td width="25%" align="left" valign="top">Your Country</td>
									<td align="left" valign="top">
										<input value="<?php echo stripslashes($user[0]['country'])?>" type="text" name="Country" id="Country" class="InputBox" style="width:250px; height:20px;" />
									</td>
								</tr>
								<tr>
									<td width="25%" align="left" valign="top">Your Postcode</td>
									<td align="left" valign="top">
										<input value="<?php echo stripslashes($user[0]['postcode'])?>" type="text" name="Postcode" id="Postcode" class="InputBox" style="width:250px; height:20px;" />
									</td>
								</tr>
								<?php if($type=='UK DELEGATES'){ ?>
								<?php 
					$k=0;
					for($i=0;$i<count($_SESSION['Name']);$i++){ 
						if($_SESSION['Name'][$i]!='0' && $_SESSION['Name'][$i]!=''){
					?>
								<tr>
									<td align="left" colspan="2" valign="top"><b><?php echo stripslashes($per[$i]['title'])?> (<?php echo $_SESSION['Name'][$i]?> Person)</b>
										<input type="hidden" name="Person[<?php echo $per[$i]['id']?>]" value="<?php echo $_SESSION['Name'][$i]?>" />
										<input type="hidden" name="Price[<?php echo $per[$i]['id']?>]" value="<?php echo stripslashes($per[$i]['price'])?>" />
										<input type="hidden" name="id[]" value="<?php echo $per[$i]['id']?>" />
									</td>
								</tr>
								<tr>
									<td align="left" valign="top">Full Name</td>
									<td align="left" valign="top">Station</td>
								</tr>
								<?php for($j=0;$j<$_SESSION['Name'][$i];$j++){ ?>
								<tr>
									<td align="left" valign="top">
										<input value="<?php echo stripslashes($req[$k]['name'])?>" type="text" title=" *" validate="required:true" name="Name_<?php echo $per[$i]['id']."_".$j?>" class="InputBox" style="width:150px; height:20px;" />
									</td>
									<td align="left" valign="top">
										<input value="<?php echo stripslashes($req[$k]['station'])?>" type="text" title=" *" validate="required:true" name="Station_<?php echo $per[$i]['id']."_".$j?>" class="InputBox" style="width:150px; height:20px;" />
									</td>
								</tr>
								<?php $k++;
							}
						}
					} ?>
								<?php } else { ?>
								<tr>
									<td colspan="2">
										<table width="100%" class="content" cellpadding="3" cellspacing="0" border="0">
											<?php 
								$k=0;
								for($i=0;$i<count($_SESSION['Name']);$i++){ 
									if($_SESSION['Name'][$i]!='0' && $_SESSION['Name'][$i]!=''){
								?>
											<tr>
												<td align="left" colspan="2" valign="top"><b><?php echo stripslashes($per[$i]['title'])?> (<?php echo $_SESSION['Name'][$i]?> Person)</b>
													<input type="hidden" name="Person[<?php echo $per[$i]['id']?>]" value="<?php echo $_SESSION['Name'][$i]?>" />
													<input type="hidden" name="Price[<?php echo $per[$i]['id']?>]" value="<?php echo stripslashes($per[$i]['price'])?>" />
													<input type="hidden" name="id[]" value="<?php echo $per[$i]['id']?>" />
												</td>
											</tr>
											<tr>
												<td align="left" valign="top">Full Name</td>
												<td align="left" valign="top">Country</td>
												<td align="center" valign="top">Require Visa?</td>
												<td align="left" valign="top">Coming Date</td>
												<td align="left" valign="top">Passport no.</td>
											</tr>
											<?php for($j=0;$j<$_SESSION['Name'][$i];$j++){ 
								?>
											<script type="text/javascript">
								$(function() {
									$('#Date_<?php echo $per[$i]['id']."_".$j?>').datepicker({
										changeMonth: true,
										changeYear: true,
										yearRange: '1970:<?php echo date('Y')+10;?>',
										dateFormat: 'yy-mm-dd'
									});	
								});
								</script>
											<tr>
												<td align="left" valign="top">
													<input value="<?php echo stripslashes($req[$k]['name'])?>" type="text" title=" *" validate="required:true" name="Name_<?php echo $per[$i]['id']."_".$j?>" class="InputBox" style="width:100px; height:20px;" />
												</td>
												<td align="left" valign="top">
													<input value="<?php echo stripslashes($req[$k]['station'])?>" type="text" title=" *" validate="required:true" name="Country_<?php echo $per[$i]['id']."_".$j?>" class="InputBox" style="width:100px; height:20px;" />
												</td>
												<td align="center" valign="top">
													<input <?php if($req[$k]['visa']=='yes') echo "checked='checked'";?> value="yes" type="checkbox" name="Visa_<?php echo $per[$i]['id']."_".$j?>" class="InputBox" />
												</td>
												<td align="left" valign="top">
													<input value="<?php echo stripslashes($req[$k]['coming_date'])?>" type="text" title=" *" validate="required:true" name="Date_<?php echo $per[$i]['id']."_".$j?>" id="Date_<?php echo $per[$i]['id']."_".$j?>" class="InputBox" style="width:100px; height:20px;" />
												</td>
												<td align="left" valign="top">
													<input value="<?php echo stripslashes($req[$k]['passport_no'])?>" type="text" title=" *" validate="required:true" name="Passport_<?php echo $per[$i]['id']."_".$j?>" class="InputBox" style="width:100px; height:20px;" />
												</td>
											</tr>
											<?php $k++;
										}
									}
								} ?>
										</table>
									</td>
								</tr>
								<?php } ?>
							</table>
						</td>
					</tr>
					<tr>
						<td align="left" valign="top"><br />
							<input type="button" onclick="window.location='index.php?p=edit_camp&pg_no=<?php echo $_REQUEST['pg_no']?>'" value="Previous" class="Btn" style="cursor:pointer;" />
							<span style="float:right">
							<input type="submit" value="Next" class="Btn" style="cursor:pointer;" />
							</span> </td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
