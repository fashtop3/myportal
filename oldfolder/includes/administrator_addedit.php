<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"admin"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE UserID =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		$Photo = "<img src='../uploads/member/big/".stripslashes($Info[0]['image'])."' width=75 height=75>";			
}else
	$MODE="ADD";
?>
<script>
  $(document).ready(function(){
  	<?php if($MODE=='ADD'){ ?>
  	$("#UserName").blur(function()
		{
			
			//remove all the class add the messagebox classes and start fading
			

		
			//check the username exists or not from ajax
			$.post("name_chk.php",{ username:$(this).val() } ,function(data)
			{
				if(data=='yes') //if username not avaiable
				{
					$("#msgbox").removeClass().addClass('messagebox').text('Checking....').fadeIn("slow");
					$("#msgbox").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						$(this).html('Username already exists').addClass('messageboxerror').fadeTo(900,1);
							if(document.frmAdmin.RegisterSubmit.disabled==false)
								document.frmAdmin.RegisterSubmit.disabled=true; 
						
					});
						
				}				
				else if(data=='no') 
				{
					$("#msgbox").removeClass().addClass('messagebox').text('').fadeIn("slow");					
					$("#msgbox").fadeTo(200,0.1,function()  //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						//$(this).html('Email address is available').addClass('messageboxok').fadeTo(900,1);	
						if(document.frmAdmin.RegisterSubmit.disabled==true){
							document.frmAdmin.RegisterSubmit.disabled=false; 
						}
					});
				}
			});
	});
	
	$("#Email").blur(function()
		{
			
			//remove all the class add the messagebox classes and start fading
			

		
			//check the username exists or not from ajax
			$.post("email_chk.php",{ email:$(this).val() } ,function(data)
			{
				if(data=='yes') //if username not avaiable
				{					
					$("#msgbox1").removeClass().addClass('messagebox').text('Checking....').fadeIn("slow");
					$("#msgbox1").fadeTo(200,0.1,function() //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						$(this).html('Email already exists').addClass('messageboxerror').fadeTo(900,1);
							if(document.frmAdmin.RegisterSubmit.disabled==false)
								document.frmAdmin.RegisterSubmit.disabled=true; 
						
					});				;		
				}				
						
				else if(data=='no') 
				{					
					$("#msgbox1").removeClass().addClass('messagebox').text('').fadeIn("slow");					
					$("#msgbox1").fadeTo(200,0.1,function()  //start fading the messagebox
					{ 
						//add message and change the class of the box and start fading
						//$(this).html('Email address is available').addClass('messageboxok').fadeTo(900,1);	
						if(document.frmAdmin.RegisterSubmit.disabled==true){
							document.frmAdmin.RegisterSubmit.disabled=false; 
						}
					});
				}
			});
	});
	<?php } ?>
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {			
			UserName: {
				required: true
			},
			Password: {
				required: true,
				minlength: 6,
				maxlength: <?php echo PASSWORD_LIMIT?>
				},
			Email: {
				required: true,
				email: true
			},
			FirstName: "required",
			LastName: "required"			
		},
		messages: {
			UserName: "Please enter username",
			Password: {required: "Please enter password",
				minlength: "Please enter at least 6 characters",
				maxlength: "Password should not be more than <?php echo PASSWORD_LIMIT?> characters"},
			Email: {
				required: "Please enter email address",
				email: "Please enter valid email address"
			},
			FirstName: "Please enter firstname",
			LastName: "Please enter lastname"
		}
	});	
  });
</script>
<style type="text/css">
.top {
	margin-bottom: 15px;
}
.messagebox {
	position:absolute;
	width:100px;
	margin-left:30px;
}
.messageboxok {
	position:absolute;
	width:auto;
	margin-left:5px;
	color:#008000;
}
.messageboxerror {
	position:absolute;
	width:auto;
	margin-left:5px;
	color:#CC0000;
}
</style>
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
    <tr>
        <td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addmember.jpg" width="48" height="48" /></td>
        <td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> ADMINISTRATOR</td>
    </tr>
    <tr>
        <td colspan="6" width="" align="center"><?php
							showMessage();			
						?>
            <form method="post" id="frmAdmin" action="manage_administrator.php" name="frmAdmin" enctype="multipart/form-data">
                <input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
                <input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
                <table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
                    <tbody>
                        <tr>
                            <td><table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
                                    <tbody>
                                        <tr>
                                            <td class="fieldlabel" width="5%">User Name</td>
                                            <td class="fieldarea"><input class="InputBox" type="text" name="UserName" id="UserName" value="<?php echo $Info[0]['UserName']?>" style="width:300px;" />
                                                <span id="msgbox" style="display:none"></span> </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="5%">Password</td>
                                            <td class="fieldarea"><input class="InputBox" type="password" name="Password" id="Password" value="<?php echo $Info[0]['Password']?>" style="width:300px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="5%">Place</td>
                                            <td class="fieldarea"><select class="InputBox" name="Place" id="Place"style="width:300px;">
                                                    <option value="">--Select Place--</option>
                                                    <?php echo FillCombo1('county','c_name','c_id',$Info[0]['place_id'],'where site!=0');?>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="5%">First Name</td>
                                            <td class="fieldarea"><input class="InputBox" type="text" name="FirstName" id="FirstName" value="<?php echo stripslashes($Info[0]['FirstName'])?>" style="width:300px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="5%">Last Name</td>
                                            <td class="fieldarea"><input class="InputBox" type="text" name="LastName" id="LastName" value="<?php echo stripslashes($Info[0]['LastName'])?>" style="width:300px;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="5%">Address</td>
                                            <td class="fieldarea"><textarea class="TextArea" name="Address" id="Address" style="width:300px;"><?php echo stripslashes($Info[0]['address'])?></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="5%">Phone/Mobile</td>
                                            <td class="fieldarea"><input class="InputBox" name="Phone" id="Phone" style="width:300px;" value="<?php echo stripslashes($Info[0]['Phone'])?>" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel" width="5%">Email</td>
                                            <td class="fieldarea"><input class="InputBox" type="text" name="Email" id="Email" value="<?php echo stripslashes($Info[0]['Email'])?>" style="width:300px;" />
                                                <span id="msgbox1" style="display:none"></span> </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel">Photo/Image</td>
                                            <td class="fieldarea" valign="top"><input class="InputBox" name="CImage" id="CImage" style="width: 300px;" type="file">
                                                <input type="hidden" id="OldImage" name="OldImage" value="<?php echo stripslashes($Info[0]['image'])?>" >
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="fieldlabel"></td>
                                            <td class="fieldarea" valign="top"><?php echo $Photo;?> </td>
                                        </tr>
                                    </tbody>
                                </table></td>
                        </tr>
                        <tr>
                            <td><br>
                                <input type="hidden" value="<?php echo $MODE?>" name="action" />
                                <br>
                                <div align="center">
                                    <input value="<?php echo $MODE?> ADMINISTRATOR" class="Btn" type="submit" name="submit1" id="RegisterSubmit">
                                    <input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=administrator&pg_no=<?php echo $_REQUEST['pg_no']?>';">
                                </div></td>
                        </tr>
                    </tbody>
                </table>
            </form></td>
    </tr>
</table>
