<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../utils/functions.php");
		CheckPermission(); 
		exit();
	}
?>

<form action="manage_mail.php" method="post" enctype="multipart/form-data" name="FrmProductAddEdit">
	<input type="hidden" name="Process" value="SendMail">
	<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border">
		<tr>
			<td colspan="2" height="25" class="tbl_head">Send Mail to User</td>
			<td height="25" align="right" class="tbl_head">
				<input type="button" name="BACK" id="BACK" class="Btn" value="BACK" onclick="window.location.href='index.php?p=home'">
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center" class="BottomBorder">
				<?php ShowMessage(); ?>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="10"></td>
		</tr>
		<tr class="SmallBlackMedium">
			<td width="191" align="right" class="LinkGray">Email</td>
			<td width="11">:</td>
			<td>
				<input type="text" name="Email" id="Email" class="InputBox" maxlength="255" style="width:185px;">
				<span class="RedStar">*</span> </td>
		</tr>
		<tr>
			<td colspan="3" height="10"></td>
		</tr>
		<tr class="SmallBlackMedium">
			<td align="right" class="LinkGray">Subject</td>
			<td>:</td>
			<td>
				<input type="text" name="Subject" id="Subject" class="InputBox" maxlength="255" style="width:185px" />
				<span class="RedStar">*</span></td>
		</tr>
		<tr>
			<td colspan="3" height="10"></td>
		</tr>
		<tr class="SmallBlackMedium">
			<td align="right" class="LinkGray">Message</td>
			<td>:</td>
			<td width="612" >
				<textarea name="Message" cols="60" rows="5" id="Message"></textarea>
			</td>
		</tr>
		<tr>
			<td colspan="3" height="10"></td>
		</tr>
		<tr>
			<td colspan="2" >&nbsp;</td>
			<td>
				<input type="submit" name="Send" id="Send" class="Btn" value="Send Mail">
			</td>
		</tr>
		<tr>
			<td colspan="3" height="10"></td>
		</tr>
	</table>
</form>
<script language="javascript" src="../editor/fckeditor.js"></script>
<script language="javascript">
window.onload = function()
{
	var sBasePath ="../editor/";
	var oFCKeditor = new FCKeditor('Message','95%',300) ;
	oFCKeditor.BasePath = '../editor/' ;
	oFCKeditor.BasePath	= sBasePath ;
	oFCKeditor.ReplaceTextarea() ;
}
</script>
