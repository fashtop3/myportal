<?php
$TblFieldsArr = array
		(
			//table name=>feilds name
			"contact"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
?>
<style type="text/css">
#pg_main {
	font-size:1.1em;
	padding:10px;
}
#pg_main .h3title {
	font-size:large;
	font-weight:bold;
	color:#003366;
	margin:5px 5px 8px 0;
}
p {
	margin:0;
	padding:0 0 10px;
	color:black;
	font-family:Arial, Helvetica, sans-serif;
	font-size:70%;
}
</style>
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"> <img src="<?php echo ADMIN_IMAGE_PATH;?>/contact_detail.jpg" width="48" height="48" /> </td>
		<td class="tbl_head" height="24">CONTACT DETAIL</td>
	</tr>
	<tr>
		<td colspan="2">
			<table width="100%" class="form">
				<tr>
					<td colspan="2" style="padding-left:5px; padding-right:5px;">
						<h3 class="h3title">Category: <?php echo stripslashes(FetchValue1('contact_category','name','id',$Info[0]['category_id']));?></h3>
						<div id="pg_main">
							<h3 class="h3title"><?php echo stripslashes($Info[0]['title'])?></h3>
							<p> <?php echo stripslashes($Info[0]['address'])?><br />
								<?php echo stripslashes($Info[0]['city'])?><br />
								<?php echo stripslashes($Info[0]['state'])?><br />
								<?php echo stripslashes($Info[0]['zipcode'])?><br />
								<?php echo stripslashes($Info[0]['country'])?><br />
								<br>
								<strong>Tel:</strong> <?php echo stripslashes($Info[0]['main_phone'])?><br>
								<strong>Parsonage:</strong> <?php echo stripslashes($Info[0]['personage_phone'])?><br>
								<br>
								<strong>Email:</strong> <a href="mailto:<?php echo stripslashes($Info[0]['email'])?>"><?php echo stripslashes($Info[0]['email'])?></a><br>
								<strong>Website:</strong>&nbsp;&nbsp; <?php echo stripslashes($Info[0]['website'])?><br>
								<strong>Pastor:</strong> <?php echo stripslashes($Info[0]['pastor_name'])?><br>
								<strong>Pastor Email:</strong> <?php echo stripslashes($Info[0]['pastor_email'])?><br>
								<strong>Mobile:</strong> <?php echo stripslashes($Info[0]['mobile'])?><br />
								<strong>Day/Time:</strong> <?php echo stripslashes($Info[0]['daytime_desc'])?><br>
								<strong>Schedule:</strong> <?php echo stripslashes($Info[0]['meeting'])?><br>
								<br />
								<?php echo stripslashes($Info[0]['description'])?><br>
							</p>
						</div>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
				<tr>
					<td colspan="2">
						<center>
							<input value="BACK" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=contact_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
						</center>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp; </td>
				</tr>
			</table>
		</td>
	</tr>
</table>
