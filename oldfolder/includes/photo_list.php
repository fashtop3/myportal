<?php
	$sql = "select * from photo_gallery";
	$sql.=' WHERE 1=1';
	if($_REQUEST['category_id']!=0)
	{
		$sql.=' AND gallery_id = '.$_REQUEST['category_id'];
	}

	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLE';
			frm.action='manage_photo.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_photo.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
	function category(id)
	{
		if(id!=0)
			window.location = 'index.php?p=photo_list&category_id='+id;
		else
			window.location = 'index.php?p=photo_list';
	}
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/photogallery.jpg" width="48" height="48" /></td>
		<td class="tbl_head" height="24">PHOTO GALLERY MANAGER</td>
	</tr>
	<tr>
		<td colspan="2" align="right" class="tbl_head"><span class="column_head">Total
			No of Results:&nbsp;<?php echo count($cms)?></span></td>
	</tr>
	<?php 
	  if(isset($_SESSION['SuccessMsg']) || isset($_SESSION['ErrorMsg']))
	  {
	  ?>
	<tr>
		<td height="24" colspan="2" align="center" bgcolor="#FFFFFF">
			<table width="99%" cellpadding="2">
				<tr>
					<td>
						<?php
							showMessage();			
						?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	  }
	  ?>
	<tr>
		<td colspan="2" width="100%">
			<form name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td colspan="3" class="BottomBorder" height="25">
							<input type="Button" name="ADDNEW" value="ADD NEW" class="Btn" onClick="window.location.href='index.php?p=photo_addedit'"/>
							<select name="CategoryId" onchange="category(this.value)">
								<option value="0">--Select Category--</option>
								<?php echo FillCombo1('photo_gallery_category','title','id',$_REQUEST['category_id']); ?>
							</select>
						</td>
						<td height="25" colspan="2" align="right"  class="BottomBorder"><?php echo $objPaging->show_paging()?></td>
					</tr>
					<tr>
						<td colspan="4" height="10"></td>
					</tr>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									
						?>
					<tr class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
						<?php if($cms[$i]['image']){ 
						if($i%2!=0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
						<td align="center"  bgcolor=<?php echo $bg;?>  > <img style="border:none" src="../uploads/photo_gallery/big/<?php echo stripslashes($cms[$i]['image']); ?>" width="100" height="100"/> <br />
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=photo_addedit&id=<?php echo $cms[$i]['id']; ?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=photo_detail&id=<?php echo $cms[$i]['id']; ?>';" />&nbsp;
							<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i++]['id']?>">
						</td>
						<?php } ?>
						<?php if($cms[$i]['image']){ 
				if($i%2!=0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
				?>
						<td align="center"  bgcolor=<?php echo $bg;?>  > <img style="border:none" src="../uploads/photo_gallery/big/<?php echo stripslashes($cms[$i]['image']); ?>" width="100" height="100"/> <br />
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=photo_addedit&id=<?php echo $cms[$i]['id']; ?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=photo_detail&id=<?php echo $cms[$i]['id']; ?>';" />&nbsp;
							<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i++]['id']?>">
						</td>
						<?php } ?>
						<?php if($cms[$i]['image']){ 
				if($i%2!=0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
				?>
						<td align="center" bgcolor=<?php echo $bg;?>  > <img style="border:none" src="../uploads/photo_gallery/big/<?php echo stripslashes($cms[$i]['image']); ?>" width="100" height="100"/> <br />
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=photo_addedit&id=<?php echo $cms[$i]['id']; ?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=photo_detail&id=<?php echo $cms[$i]['id']; ?>';" />&nbsp;
							<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i++]['id']?>">
						</td>
						<?php } ?>
						<?php if($cms[$i]['image']){ 
				if($i%2!=0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
				?>
						<td align="center" bgcolor=<?php echo $bg;?>  > <img style="border:none" src="../uploads/photo_gallery/big/<?php echo stripslashes($cms[$i]['image']); ?>" width="100" height="100"/> <br />
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=photo_addedit&id=<?php echo $cms[$i]['id']; ?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=photo_detail&id=<?php echo $cms[$i]['id']; ?>';" />&nbsp;
							<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i++]['id']?>">
						</td>
						<?php } ?>
						<?php if($cms[$i]['image']){ 
				if($i%2!=0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
				?>
						<td align="center" bgcolor=<?php echo $bg;?>  > <img style="border:none" src="../uploads/photo_gallery/big/<?php echo stripslashes($cms[$i]['image']); ?>" width="100" height="100"/> <br />
							<img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=photo_addedit&id=<?php echo $cms[$i]['id']; ?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETE');" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/detail.gif" alt="View" title="View" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="window.location='index.php?p=photo_detail&id=<?php echo $cms[$i]['id']; ?>';" />&nbsp;
							<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i]['id']?>">
						</td>
						<?php } ?>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
					<tr>
						<td colspan="5" class="<?php echo $class?>" align="right">
							<?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
						</td>
					</tr>
					<tr height="25">
						<td colspan="5" class="TopBorder">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="13%">
										<input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);">
									</td>
									<td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
