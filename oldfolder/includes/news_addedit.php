<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"news"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);		
}else
	$MODE="ADD";
	
if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $Info[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}	
?>
<script>
	$(function() {
		$('#CDate').datepicker({
			changeMonth: true,
			changeYear: true,
			yearRange: '1975:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});
  $(document).ready(function(){
  	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#Content').val(con); 
		$('#ShortContent').val(con); // put it in the textarea
	});
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Title: "required",
			Content: "required",
			ShortContent: "required"
		},
		messages: {
			Title: "Please enter title",
			ShortContent: "Please enter short description",
			Content: "Please enter full description"
		}
	});	
  });
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "Content,ShortContent",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
/*            return false;			
			var fileBrowserWindow = new Array();
			fileBrowserWindow["file"] = ajaxfilemanagerurl;
			fileBrowserWindow["title"] = "Ajax File Manager";
			fileBrowserWindow["width"] = "782";
			fileBrowserWindow["height"] = "440";
			fileBrowserWindow["close_previous"] = "no";
			tinyMCE.openWindow(fileBrowserWindow, {
			  window : win,
			  input : field_name,
			  resizable : "yes",
			  inline : "yes",
			  editor_id : tinyMCE.getWindowArg("editor_id")
			});
			
			return false;*/
		}
	</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/addnews.png" width="48" height="48" /></td>
		<td colspan=5 class="tbl_head" height="24"><?php echo $MODE;?> NEWS</td>
	</tr>
	<tr>
		<td width="" colspan="6">
			<form method="post" action="manage_news.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<?php showMessage(); ?>
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<?php if($_SESSION['AdminID']==1){ 
											
										?>
										<tr>
											<td class="fieldlabel" width="5%">Place</td>
											<td class="fieldarea">
												<select name="Place" class="InputBox" id="Place" style="width:300px;">
													<?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
												</select>
											</td>
										</tr>
										<?php }else{ ?>
										<tr>
											<td colspan="2">
												<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
											</td>
										</tr>
										<?php } ?>
										<tr>
											<td class="fieldlabel" width="15%">Title</td>
											<td class="fieldarea">
												<input class="InputBox" type="text" name="Title" id="Title" style="width:300px;" value="<?php echo stripslashes($Info[0]['title'])?>" />
											</td>
										</tr>
										<?php if($MODE=='ADD'){ ?>
										<tr>
											<td class="fieldlabel" width="15%" align="right" valign="top">
												<input type="checkbox" name="AddGalleryNews" id="AddGalleryNews" onclick="if(this.checked==true) document.getElementById('GalleryAdd').style.display='block'; else document.getElementById('GalleryAdd').style.display='none';" />
											</td>
											<td class="fieldarea">Want to add news gallery? <span style="display:none;" id="GalleryAdd">
												<table width="100%">
													<tr>
														<td width="15%"> Gallery Title </td>
														<td>
															<input class="InputBox" type="text" name="GalleryTitle" style="width:200px;" />
														</td>
													</tr>
												</table>
												</span> </td>
										</tr>
										<?php } ?>
										<tr>
											<td class="fieldlabel">Video</td>
											<td class="fieldarea" valign="top">
												<input class="InputBox" name="CVideo" id="CVideo" style="width: 300px;" type="file">
												<input type="hidden" id="OldVideo" name="OldVideo" value="<?php echo stripslashes($Info[0]['video'])?>" >
												<?php 
												if($Info[0]['video']!=''){
													$u = $_SERVER['REQUEST_URI'];	
																	if(strpos($u,'webadmin')>0)
																		$uri = substr($u,0,strpos($u,'webadmin'));
																	else
																		$uri = $u;
														//			$uri1 = $uri.'index.php?p=event_detail&id='.$_REQUEST['id'];
																	$filepath = $uri."uploads/video/".stripslashes($Info[0]['video']);
											  ?>
												<div id="container"></div>
												<script type="text/javascript" src="../Scripts/swfobject.js"></script>
												<script type="text/javascript">
															var s1 = new SWFObject("../images/player.swf","ply","350","320","9","#FFFFFF");
															s1.addParam("allowfullscreen","true");
															s1.addParam("allowscriptaccess","always");
															s1.addParam("flashvars","file=<?php echo $filepath;?>");
															//s1.write('preview');
															s1.write("container");
															</script>
												<?php } ?>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%">News Date</td>
											<td class="fieldarea">
												<input type="text"  class="InputBox" name="CDate" id="CDate" value="<?php echo $Info[0]['created']?>" style="width:300px;" />
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%">Short Description</td>
											<td class="fieldarea" valign="top">
												<textarea name="ShortContent" cols="65" rows="15" id="ShortContent" style="width: 300px; height:100px;" ><?php echo stripslashes($Info[0]['short_description'])?></textarea>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%">Full Description</td>
											<td class="fieldarea" valign="top">
												<textarea name="Content" cols="65" rows="25" id="Content" style="width: 400px;" ><?php echo stripslashes($Info[0]['full_description'])?></textarea>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<br>
								<div align="center">
									<input value="<?php echo $MODE?> NEWS" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=news_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
