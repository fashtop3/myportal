<table width="100%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td height="30">
			<table width="973" border="0" cellspacing="0" cellpadding="5">
				<tr>
					<td width="113" rowspan="2" align="left" valign="middle"><a href="index.php"><img src="<?php echo ADMIN_IMAGE_PATH;?>/logo.gif" width="114" height="90" alt="Apostolic Faith Mission UK" title="Apostolic Faith Mission UK" border="0" /></a></td>
					<td width="595" rowspan="2" colspan="3" align="left" valign="middle"><span class="companytitle_red"><?php echo COMPANY_NAME?></span></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="973" border="0" align="center" cellpadding="0" cellspacing="0">
				<tr>
					<?php
               	if(isset($_SESSION['AdminID'])) {
               ?>
					<td align="left" width="80%" valign="middle" >&nbsp;<span class="column_head">Welcome, <?php echo $_SESSION['AdminName']?>
						<?php if($_SESSION['PlaceID']==0) echo "(Super Administrator)"; else{ echo "(Administrator) (".stripslashes(FetchValue('county','c_name','c_id',$_SESSION['PlaceID']))." Church)";}?>
						</span></td>
					<td align="right" width="2%"><a href="index.php" class="LinkGray"><img src="<?php echo ADMIN_IMAGE_PATH;?>/home.jpg" style="float:left;border:0px" align="top" hspace="2" vspace="2" alt=""></a></td>
					<td align="left" width="" valign="middle" ><a href="index.php" class="LinkGray"> Home</a></td>
					<td align="right" width="2%"><a href="index.php?p=logout" class="LinkGray"><img src="<?php echo ADMIN_IMAGE_PATH;?>/logout.jpg" style="float:left;border:0px" align="top" hspace="2" vspace="2" alt=""></a></td>
					<td align="left" width="" valign="middle" ><a href="index.php?p=logout" class="LinkGray"> Log Out</a></td>
					<?php
                                    }
                                    else {
                                    ?>
					<td width="131" colspan="3" align="right" style="background:url(<?php echo ADMIN_IMAGE_PATH;?>/index_34.jpg)"></td>
					<?php
                                    }
                                    ?>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td height="5">
			<?php
                                if(isset($_SESSION['AdminID'])) {
                                ?>
			<div id="dropmenudiv" style="visibility: hidden; width: 170px; background-color: rgb(231, 237, 244);" onMouseOver="clearhidemenu()" onMouseOut="dynamichide(event)"></div>
			<div id="navigation">
				<ul>
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu1, '');" onMouseOut="this.className='navbutton';delayhidemenu();" onClick="window.location='index.php'"> <a href="index.php" title="Home">Setup</a> </li>
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu2, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> Music </li>
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu3, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> Master </li>
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu4, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> Content </li>
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu5, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> News/Event </li>
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu6, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> Contacts </li>
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu7, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> Resources </li>
<?php /*?>					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu8, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> Members </li><?php */?>
					<!--li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu9, '');" onMouseOut="this.className='navbutton';delayhidemenu();">                    
				Videos
			</li-->
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu10, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> Gallery </li>
					<li class="navbutton" onMouseOver="this.className='navbuttonover';dropdownmenu(this, event, menu0, '');" onMouseOut="this.className='navbutton';delayhidemenu();"> Others </li>
                    <li class="navbutton" onMouseOver="this.className='navbuttonover';" onMouseOut="this.className='navbutton';" onclick="window.location='index.php?p=database'"> <a href="index.php?p=database" title="Home">DB Backup</a></li>
				</ul>
			</div>
			<?php
                                }
                                else {
                                ?>
			<span class='head1'>&nbsp;</span>
			<?php
                                }
                                ?>
		</td>
	</tr>
	<tr>
		<td height="20" bgcolor="#FFFFFF"></td>
	</tr>
	<tr>
		<td align="center" width="973" valign="top" height="303" bgcolor="#FFFFFF">
			<?php include("includes/include_external.php"); ?>
		</td>
	</tr>
	<tr>
		<td height="20" bgcolor="#FFFFFF"></td>
	</tr>
</table>
<!-- Header Menu -->
<script type="text/javascript" language="javascript">
var menu1=new Array()
menu1[0]='<a href="index.php">Admin Home</a>'
menu1[1]='<a href="index.php?p=logout">Logout</a>'
menu1[2]='<a href="index.php?p=admin">My Account</a>'
<?php if($_SESSION['PlaceID']==0) { ?>
menu1[3]='<a href="index.php?p=config">General Configuration</a>'
menu1[4]='<a href="index.php?p=menu_list">Front Menu Config</a>'
<?php } ?>
menu1[5]='<a href="index.php?p=playlist">Playlist Songs</a>'
menu1[6]='<a href="index.php?p=clock">Live Clock Setting</a>'

var menu2=new Array()
menu2[0]='<a href="index.php?p=music_category">Music Category</a>'
menu2[1]='<a href="index.php?p=music_category_new">Add Music Category</a>'
menu2[2]='<hr>'
menu2[3]='<a href="index.php?p=cd_list">CDs/DVDs/Higherway Magazine/Music &amp; Tracks</a>'

var menu3=new Array()
<?php if($_SESSION['PlaceID']==0) { ?>
menu3[0]='<a href="index.php?p=place_list">Lists All Places/County</a>'
menu3[1]='<a href="index.php?p=place_addedit">Add New Place/County</a>'
menu3[2]='<hr>'
<?php } ?>
menu3[3]='<a href="index.php?p=schedule_list">List Schedules</a>';
menu3[4]='<a href="index.php?p=schedule_addedit">Add New Schedule</a>'
<?php if($_SESSION['PlaceID']==0) { ?>
menu3[8]='<hr>'
menu3[9]='<a href="index.php?p=administrator">List All Administrators</a>'
menu3[10]='<a href="index.php?p=administrator_addedit">Add New Administrator</a>'
<?php } ?>
<?php if($_SESSION['PlaceID']==0) { ?>
menu3[17]='<hr>'
menu3[18]='<a href="index.php?p=shop">List Online Payment Category</a>'
menu3[19]='<a href="index.php?p=shop_addedit">Add New Online Payment Category</a>'
menu3[20]='<a href="index.php?p=camp_setting">Camp Meeting</a>'
<?php } ?>
<?php /*?>menu3[21]='<hr>'
menu3[22]='<a href="index.php?p=devotional_list">List Devotional Archive</a>'
menu3[23]='<a href="index.php?p=devotional_addedit">Add New Devotional</a>'<?php */?>
menu3[24]='<hr>'
menu3[25]='<a href="index.php?p=banners">List Banners</a>'
menu3[26]='<a href="index.php?p=banner_new">Add New Banner</a>'

<!--menu3[9]='<a href="index.php?p=curriculum_category_list">List Curriculum Category</a>'-->
<!--menu3[10]='<a href="index.php?p=curriculum_category_addedit">Add Curriculum Category</a>'->
<!--menu3[11]='<hr>'-->
<!--menu3[12]='<a href="index.php?p=tract_category_list">List Tract Category</a>'-->
<!--menu3[13]='<a href="index.php?p=tract_category_addedit">Add Tract Category</a>'-->
<!--menu3[14]='<hr>'-->
<!--menu3[15]='<a href="index.php?p=occation_list">List Occasions</a>'-->
<!--menu3[16]='<a href="index.php?p=occation_addedit">Add Occasion</a>'-->


var menu4=new Array()
menu4[0]='<a href="index.php?p=cms_list">List CMS Pages</a>'
menu4[2]='<hr>'
menu4[3]='<a href="index.php?p=outreach_list">List Outreach Pages</a>';
menu4[4]='<a href="index.php?p=outreach_addedit">Add New Outreach Page</a>'
menu4[5]='<hr>'
menu4[6]='<a href="index.php?p=reflection_list">List Reflection Pages</a>'
menu4[7]='<a href="index.php?p=reflection_addedit">Add New Reflection Page</a>'
menu4[8]='<hr>'
menu4[9]='<a href="index.php?p=doctrines_list">List Doctrines(Seeking Christ) Pages</a>'
menu4[10]='<a href="index.php?p=doctrines_addedit">Add New Doctrines(Seeking Christ) Page</a>'

var menu5=new Array()
menu5[0]='<a href="index.php?p=news_list">List News</a>'
menu5[1]='<a href="index.php?p=news_addedit">Add New News</a>';
menu5[8]='<hr>'
menu5[9]='<a href="index.php?p=event_list">List Events</a>'
menu5[10]='<a href="index.php?p=event_addedit">Add New Event</a>'

var menu6=new Array()
menu6[0]='<a href="index.php?p=contact_category_list">List Contact Category</a>'
menu6[1]='<a href="index.php?p=contact_category_addedit">Add New Contact Category</a>';
menu6[2]='<hr>'
menu6[3]='<a href="index.php?p=contact_list">List Contacts</a>';
menu6[4]='<a href="index.php?p=contact_addedit">Add New Contact</a>'

var menu7=new Array()
menu7[0]='<a href="index.php?p=book_list">List Books</a>'
menu7[1]='<a href="index.php?p=book_addedit">Add Book</a>'
menu7[2]='<a href="index.php?p=curriculum_category_list">List Curriculum Category</a>'
menu7[3]='<a href="index.php?p=curriculum_category_addedit">Add Curriculum Category</a>'
menu7[4]='<a href="index.php?p=curriculum_list">Curriculums (Junior and Senior)</a>'
menu7[5]='<a href="index.php?p=curriculum_primary">Curriculums (Primary Pal and Answer)</a>'
menu7[6]='<hr>'
menu7[7]='<a href="index.php?p=tract_category_list">List Tract Category</a>'
menu7[8]='<a href="index.php?p=tract_category_addedit">Add Tract Category</a>'
menu7[9]='<a href="index.php?p=tract_list">List Tracts</a>'
menu7[10]='<a href="index.php?p=tract_addedit">Add Tract</a>'
menu7[11]='<hr>'
menu7[12]='<a href="index.php?p=occation_list">List Occasions</a>'
menu7[13]='<a href="index.php?p=occation_addedit">Add Occasion</a>'
menu7[14]='<a href="index.php?p=sermon_list">List Sermons</a>'
menu7[15]='<a href="index.php?p=sermon_addedit">Add Sermon</a>'
<!--menu7[14]='<hr>';-->
menu7[16]='<a href="index.php?p=video_list">List Sermon Video</a>'
<!--menu7[16]='<a href="index.php?p=video_addedit">Add New Video</a>';-->
<!--menu7[18]='<a href="index.php?p=audio_list">List Sermon Audios</a>'-->
<!--menu7[19]='<a href="index.php?p=audio_addedit">Add New Audio</a>';-->

<!--menu7[8]='<hr>'-->
<!--menu7[9]='<a href="index.php?p=photo_category_list">List Photo Gallery Category</a>'-->
<!--menu7[10]='<a href="index.php?p=photo_category_addedit">Add Photo category</a>'-->
<!--menu7[11]='<hr>'-->
<!--menu7[12]='<a href="index.php?p=photo_list">List Photo Gallery</a>'-->
<!--menu7[13]='<a href="index.php?p=photo_addedit">Add Photo</a>'-->


var menu8=new Array()
menu8[0]='<a href="index.php?p=member_list">List Members</a>'
menu8[1]='<a href="index.php?p=member_addedit">Add New Members</a>'
<?php if($_SESSION['PlaceID']==0) { ?>
menu8[7]='<hr>'
menu8[8]='<a href="index.php?p=position_list">List Member Postions</a>';
menu8[9]='<a href="index.php?p=position_addedit">Add New Position</a>'
<?php } ?>

var menu9=new Array()
menu9[0]='<a href="index.php?p=video_list">List Webcast Videos</a>'
menu9[1]='<a href="index.php?p=video_addedit">Add New Video</a>';

var menu0=new Array()
menu0[0]='<a href="index.php?p=prayer_list">Prayer Requests</a>'
menu0[1]='<hr>';
menu0[2]='<a href="index.php?p=testi_list">Testimonials</a>'
menu0[3]='<hr>';
menu0[4]='<a href="index.php?p=feedback_list">Feedback</a>'
menu0[5]='<hr>';
menu0[6]='<a href="index.php?p=comment_list">Comments / Suggestions (Live Video)</a>'
menu0[7]='<hr>';
menu0[8]='<a href="index.php?p=sms_list">List SMS Subscribers</a>'
menu0[9]='<a href="index.php?p=sms_addedit">Add New SMS Subscribers</a>'
menu0[10]='<a href="index.php?p=send_bulk_sms">Send Bulk SMS</a>'

var menu10=new Array()
menu10[3]='<a href="index.php?p=news_gallery_list">List Image Gallery</a>';
menu10[4]='<a href="index.php?p=news_gallery_addedit">Add New Image Gallery</a>'
menu10[5]='<hr>'
menu10[6]='<a href="index.php?p=gallery_images_list">List Gallery Images</a>'
menu10[7]='<a href="index.php?p=gallery_images_addedit">Add Gallery Image</a>'

</script>
