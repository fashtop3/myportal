<?php
	if(!defined("__CONFIG__PHP__")){
		include_once("../../utils/common_functions.php");
		CheckPermission(); 
		exit();
	}

	$CategoryName = LoadVariable("CategoryName","");

	$MODE="ADD";
	$PANEL_HEADING="SMS SUBSCRIBER";

	if(isset($_REQUEST['ID']))
	{
		$MODE="UPDATE";

		$TblFieldsArr = array
		(
			//table name=>feilds name
			"sms"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE sms_id =".$_REQUEST['ID'];
		$Sort="";
		$Limit="";

		$RecordSet=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		
		$Name = $RecordSet[0]['sms_name'];
		$MobileNo = $RecordSet[0]['sms_mobile_no'];
		$CategoryID = $RecordSet[0]['sms_id'];
		
		if($MODE=='ADD')
	$pl = DEFAULT_PLACE;
else
	$pl = $RecordSet[0]['place_id'];
if($_SESSION['AdminID']==1)
	$where = 'WHERE place_id='.$pl;
else{	
	$where = ' WHERE place_id='.$_SESSION['PlaceID'];
}
}
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#FrmCategoryAddEdit").validate({
		rules: {
			Name: "required",
			MobileNo: {required: true,maxlength:10,minlength:10}
		},
		messages: {
			Name: "Please enter name",
			MobileNo: {required:"Please enter mobile number",maxlength:"Please enter 10 digit mobile number",minlength:"Please enter 10 digit mobile number"}
		}
	});
  });
</script>

<form action="manage_sms.php" method="post" enctype="multipart/form-data" name="FrmCategoryAddEdit" id="FrmCategoryAddEdit" >
	<input type="hidden" name="Process" value="<?php echo $MODE?>">
    <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
	<input type="hidden" name="ID" value="<?php echo $_REQUEST['ID']?>">
	<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
		<tr>
			<td colspan="" height="25" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/sms.jpg" width="48" height="48" /></td>
			<td colspan="" height="25" class="tbl_head"><?php echo $MODE?>&nbsp;<?php echo $PANEL_HEADING?></td>
			<td width="59%" height="25" align="right" class="tbl_head">
				
			</td>
		</tr>
		<tr>
			<td colspan="3" align="center">
				<?php ShowMessage(); ?>
				<table width="100%" align="center" class="form">
					<?php if($_SESSION['AdminID']==1){ 
											
										?>
					<tr class="BlackMediumNormal">
						<td class="fieldarea" width="15%">&nbsp;</td>
						<td width="10%" align="left" class="fieldarea">Place</td>
						<td class="fieldarea" width="2%">:</td>
						<td class="fieldarea">
							<select name="Place" class="InputBox" id="Place" style="width:300px;" onchange="dayvalue(this.value)">
								<?php echo FillCombo1('county','c_name','c_id',$pl,'where site!=0');?>
							</select>
							<span class="RedStar">*</span> </td>
					</tr>
					<?php }else{ ?>
					<tr>
						<td class="fieldarea" colspan="4">
							<input type="hidden" value="<?php echo $_SESSION['PlaceID'];?>" name="Place" id="Place" />
						</td>
					</tr>
					<?php } ?>
					<tr class="BlackMediumNormal">
						<td class="fieldarea" width="15%">&nbsp;</td>
						<td class="fieldarea" width="10%" align="left" > Name </td>
						<td class="fieldarea" width="2%">:</td>
						<td class="fieldarea">
							<input type="text" name="Name" id="Name" class="InputBox" value="<?php echo stripslashes($Name)?>" maxlength="255" style="width:175px;">
							<span class="RedStar">*</span> </td>
					</tr>
					<tr class="BlackMediumNormal">
						<td width="15%" class="fieldarea">&nbsp;</td>
						<td align="left" class="fieldarea">Mobile No</td>
						<td class="fieldarea">:</td>
						<td class="fieldarea">
							<input type="text" name="MobileNo" id="MobileNo" class="InputBox" value="<?php echo stripslashes($MobileNo)?>" maxlength="255" style="width:175px;" />
							<span class="RedStar">*</span> </td>
					</tr>
					<tr>
						<td colspan="3" class="fieldarea" >&nbsp;</td>
						<td class="fieldarea">
							<input type="submit" name="<?php echo $MODE?>" id="<?php echo $MODE?>" class="Btn" value="<?php echo $MODE?>">
							<input type="button" name="cancel" id="cancel" class="Btn" value="CANCEL" onclick="window.location='index.php?p=sms_list&pg_no=<?php echo $_REQUEST['pg_no']?>'">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</form>
