<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"member"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);
		$Photo = "<img src=../uploads/member/big/".stripslashes($Info[0]['image'])." width=75 height=75>";			
}else
	$MODE="ADD";
?>
<script>
  $(document).ready(function(){
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {			
			Position: "required"
		},
		messages: {
			Position: "Please select position"
		}
	});	
  });
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/position.jpg" width="48" height="48" /></td>
		<td colspan=5 class="tbl_head" height="24">UPDATE MEMBER POSITION</td>
	</tr>
	<tr>
		<td width="">&nbsp;</td>
		<td width="">
			<form method="post" action="manage_member.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<input name="token" value="fe448caf626bd507f3952e8de72acac5b89c1ad3" type="hidden">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="15%">User Name</td>
											<td class="fieldarea"><?php echo $Info[0]['username']?> </td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%">Name</td>
											<td class="fieldarea"><?php echo stripslashes($Info[0]['firstname']." ".$Info[0]['lastname'])?> </td>
										</tr>
										<tr>
											<td class="fieldlabel" width="15%">Position</td>
											<td class="fieldarea">
												<select class="InputBox" name="Position" id="Position" style="width:300px;">
													<option value="">--Select Position--</option>
													<?php
								echo FillCombo1('member_position','title','id',$Info[0]['position'],'');
							?>
												</select>
											</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="UPDATEPOSITION" name="action" />
								<br>
								<div align="center">
									<input value="UPDATE POSITION" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=member_list&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
