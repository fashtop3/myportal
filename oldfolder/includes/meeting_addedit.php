<?php
if(isset($_REQUEST['id']) && !empty($_REQUEST['id']))
{
	$MODE="UPDATE";
	$TblFieldsArr = array
		(
			//table name=>feilds name
			"camp_meeting"=>""
		);
	
		$JoinType="left outer join";
	
		$JoinTblToArr=array
		(
			//field name=>table name
		);
	
		$JoinTblOnArr=array	
		(
			//field name=>table name
		);
		
		$JoinTblConditionArr=array
		(
			//specify condition here
		);

		$Qsearch="";
		$Where="WHERE id =".$_REQUEST['id'];
		$Sort="";
		$Limit="";

		$Info=FetchDataJoined($TblFieldsArr,$JoinType,$JoinTblToArr,$JoinTblOnArr,$JoinTblConditionArr,$Where,$Sort,$Limit);		
}else
	$MODE="ADD";

?>
<script>
  $(document).ready(function(){
  	$('#submit1').click(function() {
		var con = tinyMCE.activeEditor.getContent(); // get the content

		$('#Content').val(con); // put it in the textarea
	});
    // validate signup form on keyup and submit
	$("#frmAdmin").validate({
		rules: {
			Category: "required",
			Content:"required",
			Title:"required",
			Donation: "required",
			Price: {required:true,number:true}
		},
		messages: {
			Category: "Please select category",
			Content:"Please enter description",
			Title:"Please enter title",
			Donation:"Please select camp meeting category",
			Price: "Please enter price"
		}
	});	
  });
</script>
<script type="text/javascript" src="../editor/tiny_mce.js"></script>
<script type="text/javascript">
		tinyMCE.init({
			// General options
			mode : "exact",
			elements : "Content",
			theme : "advanced",
			
			plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,wordcount",
	
			// Theme options
			theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,link,unlink,anchor,image,cleanup,code,|,insertdate,inserttime,preview",
			theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo",
			theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr",
			theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak,|,forecolor,backcolor",
			theme_advanced_buttons5 : "styleselect,formatselect,fontselect,fontsizeselect,|,print,|,ltr,rtl,|,fullscreen",
			theme_advanced_toolbar_location : "top",
			theme_advanced_toolbar_align : "left",
			theme_advanced_statusbar_location : "none",
			file_browser_callback : "ajaxfilemanager",
			theme_advanced_resizing : true,
	
			// Example content CSS (should be your site CSS)
			content_css : "css/content.css",
	
			// Drop lists for link/image/media/template dialogs
			template_external_list_url : "lists/template_list.js",
			external_link_list_url : "lists/link_list.js",
			external_image_list_url : "lists/image_list.js",
			media_external_list_url : "lists/media_list.js",
	
			// Replace values for the template plugin
			template_replace_values : {
				username : "Some User",
				staffid : "991234"
			}
		});
		function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "../editor/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 500,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
           
		}

function dayvalue(id)
		{			
			$.post("place_chk.php",{ username:id } ,function(data)
			{				
				document.getElementById('DayName1').innerHTML = data;
			});
	}
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="images/meeting.gif" width="48" height="48" /></td>
		<td colspan=5 class="tbl_head" height="24">CAMP MEETING</td>
	</tr>
	<tr>
		<td colspan="6" width="">
			<form method="post" action="manage_camp_meeting.php" id="frmAdmin" enctype="multipart/form-data">
				<input type="hidden" name="id" value="<?php echo $_REQUEST['id']?>">
                <input type="hidden" name="pg_no" id="pg_no" value="<?php echo $_REQUEST['pg_no']?>">
				<table class="form" width="100%" border="0" cellpadding="3" cellspacing="2">
					<tbody>
						<tr>
							<td>
								<table width="100%" border="0" cellpadding="3" cellspacing="2" style="vertical-align:top">
									<tbody>
										<tr>
											<td class="fieldlabel" width="5%">Delegate Category</td>
											<td class="fieldarea">
												<select  class="InputBox" name="Category" id="Category" style="width:300px;" >
													<option value="">--Select--</option>
													<?php
													$day = array("UK DELEGATES","OVERSEAS DELEGATES");
								for($i=0;$i<count($day);$i++)
								{
									if($day[$i]==$Info[0]['category'])
										$select = "selected";
									else
										$select = "";
									echo "<option ".$select." value='".$day[$i]."'>".$day[$i]."</option>";
								}
							?>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Camp Meeting Category</td>
											<td class="fieldarea">
												<select  class="InputBox" name="Donation" id="Donation" style="width:300px;" >
													<option value="">--Select--</option>
													<?php													
													echo FillCombo1("donation","title","id",$Info[0]['donation_id'],"where type='Camp Meeting' and status=1");
													?>
												</select>
											</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Title</td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Title" id="Title" style="width:300px;" value="<?php echo stripslashes($Info[0]['title'])?>" />
												(eg. Age 3-8)</td>
										</tr>
										<tr>
											<td class="fieldlabel" width="5%">Price <span style="float:right"><?php echo CURRENCY_SIGN?></span> </td>
											<td class="fieldarea">
												<input type="text" class="InputBox" name="Price" id="Price" style="width:300px;" value="<?php echo stripslashes($Info[0]['price'])?>" />
												(per person)</td>
										</tr>
										<!--<tr>
                      <td class="fieldlabel" width="5%">Description</td>
                      <td class="fieldarea"><textarea name="Content" cols="65" rows="15" id="Content" style="width: 500px;" ><?php echo $Info[0]['description']?></textarea>
                      </td>
                    </tr>-->
									</tbody>
								</table>
							</td>
						</tr>
						<tr>
							<td><br>
								<input type="hidden" value="<?php echo $MODE?>" name="action" />
								<br>
								<div align="center">
									<input value="SUBMIT" class="Btn" type="submit" name="submit1" id="submit1">
									<input value="CANCEL" class="Btn" type="button" name="cancel" id="cancel" onclick="window.location='index.php?p=camp_setting&pg_no=<?php echo $_REQUEST['pg_no']?>';">
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</form>
		</td>
	</tr>
</table>
