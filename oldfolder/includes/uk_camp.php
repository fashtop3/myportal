<?php
	$sql = "select * from camp_user where type='UK DELEGATES'";
	
	if($_REQUEST['Search']!='')
		$sql .= " and name like '%".$_REQUEST['Search']."%'";		
	
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$cms = $objDB->select($objPaging->get_query());

		if($cms)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>
<script type="text/javascript" language="javascript">

function confirmDel(frm)
	{
		if(confirm("Do you really want to delete?"))
		{		
			frm.Process.value='DELETEMULTIPLEUK';
			frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			frm.action='manage_camp_user.php';	
			frm.submit();
		}	
	}
	function singleDel(ID,Process)
	{
		if(confirm("Do you really want to delete?"))
		{		
			document.FrmSMS.ID.value = ID;
			document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
			document.FrmSMS.Process.value = Process;
			document.FrmSMS.action = 'manage_camp_user.php';
			document.FrmSMS.submit();
		}	
	}
	
	function check_all()
	{
			dml=document.FrmSMS;
			len = dml.elements.length;
			var i=0;
			if(dml.delall.checked==true)
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=true;
				}
			}
			else
			{
				for( i=0 ; i<len ; i++) 
				{
					dml.elements[i].checked=false;
				}
			}
	}
	function ValidateSelection(frm)
	{		
		var x=true;
		for(var i=0;i<<?php echo count($cms)?>;i++)
		{
			if(document.getElementById('del'+i).checked)
			{
				x=false;
				confirmDel(frm);
				break;
			}
		}
		if(x)
			alert('Please select Atleast One Record');
		return false;	
	}
</script>

<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="images/meeting.gif" width="48" height="48" /></td>
		<td class="tbl_head" height="24">CAMP MEETING UK DELEGATES</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<?php showMessage();	?>
			<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
				<tr>
					<td colspan="6" class="content" height="25">
						<form name="FrmSMS1" method="post" action="" id="FrmSMS1">
							<input type="hidden" name="ID" id="ID" value="">
							<input type="hidden" name="Process" id="Process" value="">
							Search by Name:
							<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="InputBox" />
							<input type="submit" value="Search" class="Btn" />
						</form>
					</td>
				</tr>
				<tr>
					<td colspan="4" class="content" height="25">
						<input type="Button" name="ADDNEW" value="BACK" class="Btn" onClick="window.location.href='index.php?p=camp_setting'"/>
					</td>
					<td height="25" colspan="2" align="right"  class="BottomBorder"><?php echo $objPaging->show_paging()?></td>
				</tr>
				<tr>
					<td colspan="6" class="BottomBorder"></td>
				</tr>
				<td colspan="6">
						<form name="FrmSMS" method="post" action="" id="FrmSMS">
							<input type="hidden" name="ID" id="ID" value="">
                            <input type="hidden" name="pg_no" id="pg_no" value="">
							<input type="hidden" name="Process" id="Process" value="">
							<table width="100%" cellspacing="0" cellpadding="5">
								<tr height="25" class="SmallBlackHeading">
									<td width="4%" align="center" class="BottomBorder">
										<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
									</td>
									<td width="5%" align="center" class="BottomBorder">ID</td>
									<td width="" align="left" class="BottomBorder">Booking No</td>
									<td width="" align="left" class="BottomBorder">Name</td>
									<td width="" align="left" class="BottomBorder">Email</td>
									<td width="" align="left" class="BottomBorder">Person</td>
									<td width="" align="left" class="BottomBorder">Type</td>
									<td align="center" class="BottomBorder">Options</td>
								</tr>
								<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($cms))
							{
								for($i=0;$i<count($cms);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
									if($cms[$i]['status']=='1'){
										$Status = "<img title='Active' src='images/icon_status_green.gif' align='absmiddle' width=12 height=12 />&nbsp;" ;
										$Status .= "<a title='Inactive' href='manage_camp_user.php?page=uk_camp&pg_no=".$pg_no."&Process=0&ID=".$cms[$i]['id']."&pg_no=".$_REQUEST['pg_no']."'><img src='images/icon_status_red_light.gif' width=12 height=12 border=0 align='absmiddle' style='cursor:pointer' /></a>" ;
									}else{
										$Status = "<a title='Inactive' href='manage_camp_user.php?page=uk_camp&pg_no=".$pg_no."&Process=1&ID=".$cms[$i]['id']."&pg_no=".$_REQUEST['pg_no']."'><img src='images/icon_status_green_light.gif' width=12 height=12 border=0 align='absmiddle' style='cursor:pointer' /></a>&nbsp;" ;
										$Status .= "<img title='Active' align='absmiddle' src='images/icon_status_red.gif' width=12 height=12 />" ;																				
									}	
						?>
								<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
									<td align="center">
										<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $cms[$i]['id']?>">
									</td>
									<td align="center" class="Numbers"><?php echo $cms[$i]['id']?></td>
									<td align="left" class="Numbers"><?php echo stripslashes($cms[$i]['booking_ref_no'])?></td>
									<td align="left"><a class="LinkGray" href="index.php?p=uk_camp_detail&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>"><?php echo ucwords(stripslashes($cms[$i]['name'])); ?></a></td>
									<td align="left"><?php echo stripslashes($cms[$i]['email']); ?></td>
									<td align="left">
										<?php 
									$per = FetchData("camp_request",array(),"where user_id='".$cms[$i]['id']."'");
									echo count($per); ?>
									</td>
									<td align="left"><?php echo stripslashes($cms[$i]['description']); ?></td>
									<td align="center" class="BlackMediumNormal"><img align="absmiddle" src="images/detail.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=uk_camp_detail&id=<?php echo $cms[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img align="absmiddle" src="images/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $cms[$i]['id']?>', 'DELETEUK');" />&nbsp; <?php echo $Status;?> </td>
								</tr>
								<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
								<tr>
									<td colspan="8" class="<?php echo $class?>" align="right">
										<?php if(count($cms)) print $page_link; else print "&nbsp;"; ?>
									</td>
								</tr>
								<tr height="25">
									<td colspan="8" class="TopBorder">
										<table width="100%" border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td width="13%">
													<input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);">
												</td>
												<td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</form>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
