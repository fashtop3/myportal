<?php

	$sql = "select * from book where 1";	

	if($_REQUEST['Search']!='')
		$sql .= " and book_no = ".$_REQUEST['Search'];
	
	$sql .= " order by book_no desc,lesson_no desc,lesson_date";
	$objPaging = new paging($sql, "id",RECORD_PER_PAGE);
	$objPaging->set_paging_style("LinkGray","LinkGray10","LinkRed");
	$schedule = $objDB->select($objPaging->get_query());

		if($schedule)
			$_SESSION['FoundMsg'] = '';
		else
			$_SESSION['SuccessMsg'] = 'No Record Found';	

?>

<script type="text/javascript" language="javascript">

function confirmDel(frm)
{
	if(confirm("Do you really want to delete?"))
	{		
		frm.Process.value='DELETEMULTIPLE';
		frm.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
		frm.action='manage_book.php';	
		frm.submit();
	}	
}
function singleDel(ID,Process)
{
	if(confirm("Do you really want to delete?"))
	{		
		document.FrmSMS.ID.value = ID;
		document.FrmSMS.pg_no.value='<?php echo $_REQUEST['pg_no']?>';
		document.FrmSMS.Process.value = Process;
		document.FrmSMS.action = 'manage_book.php';
		document.FrmSMS.submit();
	}	
}

function check_all()
{
	dml=document.FrmSMS;
	len = dml.elements.length;
	var i=0;
	if(dml.delall.checked==true)
	{
		for( i=0 ; i<len ; i++) 
		{
			dml.elements[i].checked=true;
		}
	}
	else
	{
		for( i=0 ; i<len ; i++) 
		{
			dml.elements[i].checked=false;
		}
	}
}
function ValidateSelection(frm)
{		
	var x=true;
	for(var i=0;i<<?php echo count($schedule)?>;i++)
	{
		if(document.getElementById('del'+i).checked)
		{
			x=false;
			confirmDel(frm);
			break;
		}
	}
	if(x)
		alert('Please select Atleast One Record');
	return false;	
}
</script>
<script type="text/javascript">
var months = new Array(12);
months[0] = "January";
months[1] = "February";
months[2] = "March";
months[3] = "April";
months[4] = "May";
months[5] = "June";
months[6] = "July";
months[7] = "August";
months[8] = "September";
months[9] = "October";
months[10] = "November";
months[11] = "December";

function showEdit(id,value){		
	hideAll();
	document.getElementById(id+'_0').style.display = 'none';
	document.getElementById(id).style.display = '';
	/*str = '<input type="text" value="'+value+'" name="SDate" id="SDate" />';
	str += '<input type="button" onclick="save()" value="Go" class="Btn" />';
	document.getElementById(id).innerHTML = str;*/
	$(function() {	
		$('#SDate'+id).datepicker({
			beforeShowDay: nonWorkingDates,
			changeMonth: true,
			changeYear: true,
			yearRange: '1975:<?php echo date('Y')+20?>',
			dateFormat: 'yy-mm-dd'
		});
	});
	
	document.getElementById('SDate'+id).focus();
}

function hideEdit(id, value){	
	var now = new Date(value);
	//alert(now);
	day = now.getDate();//toString('dd mmmm yyyy');
	month = now.getMonth();
	year = now.getFullYear();
	
	str = day+" "+months[month]+" "+year;
	document.getElementById(id).innerHTML = str;
}

function hideAll(){	
	for(id=0;id<<?php echo count($schedule)?>;id++){		
		document.getElementById(id+'_0').style.display = '';
		document.getElementById(id).style.display = 'none';
	}
}

function save(i,id){
	value = document.getElementById("SDate"+i).value;
	//alert(value);
	$.post("save_date.php",{ id:id, dt:value } ,function(data)
	{
		if(data=='No'){
			alert("Not a Sunday Date");
		}
		else{
			document.getElementById(i+'_0').innerHTML = data;
			document.getElementById(i+'_0').style.display = '';
			document.getElementById(i).style.display = 'none';
		}
	});	
}
</script>
<table width="90%" border="0" cellpadding="0" cellspacing="0" class="tbl_border" bgcolor="#FFFFFF">
	<tr>
		<td height="24" width="5%" class="tbl_head"><img src="<?php echo ADMIN_IMAGE_PATH;?>/book.gif" width="48" height="48" /></td>
		<td class="tbl_head" height="24">BOOK MANAGER</td>
	</tr>
	<tr>
		<td colspan="2" width="100%">
			<?php showMessage();	?>
			<form name="FrmSMS" method="post" action="" id="FrmSMS">
				<input type="hidden" name="ID" id="ID" value="">
                <input type="hidden" name="pg_no" id="pg_no" value="">
				<input type="hidden" name="Process" id="Process" value="">
				<table width="100%" border="0" cellpadding="5" cellspacing="0" bgcolor="#FFFFFF">
					<tr>
						<td colspan="5" class="content"> Search by Book no:
							<input type="text" size="18" value="<?php echo $_REQUEST['Search']?>" name="Search" class="InputBox" />
							<input type="submit" value="Search" class="Btn" />
						</td>
					</tr>
					<tr>
						<td colspan="4" class="BottomBorder" height="25">
							<input type="Button" name="ADDNEW" value="ADD NEW" class="Btn" onClick="window.location.href='index.php?p=book_addedit&pg_no=<?php echo $_REQUEST['pg_no']?>'"/>
							<input type="button" value="SHOW ALL" class="Btn" onclick="window.location='index.php?p=book_list'" />
							<input type="button" value="Update Multiple Lesson Date of a Book" class="Btn" onclick="window.location='index.php?p=book_multiple_date'" />
						</td>
						<td height="25" colspan="2" align="right"  class="BottomBorder"><?php echo $objPaging->show_paging()?></td>
					</tr>
					<tr>
						<td colspan="5" class="BottomBorder"></td>
					</tr>
					<tr height="25" class="SmallBlackHeading">
						<td width="4%" align="center" class="BottomBorder">
							<input type="checkbox" name="delall" id="delall" onClick="check_all(this.form,this);">
						</td>
						<td width="5%" align="center" class="BottomBorder">#</td>
						<td width="25%" align="left" class="BottomBorder">Book No</td>
						<td width="25%" align="center" class="BottomBorder">Lesson No</td>
						<td width="25%" align="center" class="BottomBorder">Lesson Date (click on date to edit)</td>
						<td align="center" class="BottomBorder">Options</td>
					</tr>
					<?php
							$i=0;
							$r=$start_limit+1;
							if(!empty($schedule))
							{
								for($i=0;$i<count($schedule);$i++)
								{ 
									if(($i+1)%2==0)
										$bg=BGCOLOR_ODD_ROW;
									else
										$bg=BGCOLOR_EVEN_ROW;
						?>
					<tr bgcolor=<?php echo $bg;?>  class="SmallBlackNormal" onMouseOver="this.style.backgroundColor='#E5E1D5';" onMouseOut="this.style.backgroundColor='<?php echo $bg?>';">
						<td align="center">
							<input type="checkbox" name="del[]" id="del<?php echo $i?>" value="<?php echo $schedule[$i]['id']?>">
						</td>
						<td align="center" class="Numbers"><?php echo $i+1?></td>
						<td align="left"><?php echo ucwords(stripslashes($schedule[$i]['book_no'])); ?></td>
						<td align="center"><?php echo stripslashes($schedule[$i]['lesson_no']);?> </td>
						<td align="center">
							<span id="<?php echo $i; ?>_0" onclick="showEdit('<?php echo $i; ?>','<?php echo $schedule[$i]['lesson_date']?>')">
							<?php echo date("d F Y",strtotime($schedule[$i]['lesson_date']));?>
							</span>
							<span id="<?php echo $i; ?>" style="display:none">
								<input type="text" value="<?php echo $schedule[$i]['lesson_date']?>" name="SDate<?php echo $i?>" id="SDate<?php echo $i?>" />
								<input type="button" onclick="save('<?php echo $i?>','<?php echo $schedule[$i]['id']?>')" value="Go" class="Btn" />
							</span>
						</td>
						<td align="center" class="BlackMediumNormal"><img src="<?php echo ADMIN_IMAGE_PATH;?>/edit.gif" style="cursor:pointer" alt="Edit" title="Edit" onClick="window.location.href='index.php?p=book_addedit&id=<?php echo $schedule[$i]['id']; ?>&pg_no=<?php echo $_REQUEST['pg_no']?>'" />&nbsp; <img src="<?php echo ADMIN_IMAGE_PATH;?>/trash.png" alt="Delete" title="Delete" border="1" width="22" height="22" style="cursor:pointer;border:0px solid #A9A9A9;" onclick="javascript: singleDel('<?php echo $schedule[$i]['id']?>', 'DELETE');" /></td>
					</tr>
					<?php
								$r++;
								}
								$class = 'TopBorder';
							}
							else
								$class = '';
			?>
					<tr>
						<td colspan="6" class="<?php echo $class?>" align="right">
							<?php if(count($schedule)) print $page_link; else print "&nbsp;"; ?>
						</td>
					</tr>
					<tr height="25">
						<td colspan="6" class="TopBorder">
							<table width="100%" border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td width="13%">
										<input type="submit" name="delete" value="DELETE" class="Btn" onClick="return ValidateSelection(this.form);">
									</td>
									<td width="87%" align="right"><?php echo $objPaging->show_paging()?></td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</form>
		</td>
	</tr>
</table>
