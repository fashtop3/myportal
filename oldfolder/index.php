<?php
ob_start();
session_start();
require_once("../utils/config.php");
require_once("../utils/color_config.php");
require_once("../utils/functions.php");
require_once("../utils/dbclass.php");
require_once("../utils/paging.php");
$objDB = new MySQLCN;

/* Echoes from past */
$sql = "select * from sermon where type='echoes from past' order by id desc limit 1";
$trt = $objDB->select($sql);

if(count($trt)>0){
	$today = strtotime(date("Y-m-d"));	
	$past = strtotime($trt[0]['display_date']);	
	$diff = $today-$past;	
	
	if($diff>30){
		$flag=0;
		if($trt[0]['notify']==''){
			$sql = "UPDATE sermon SET notify='".date('Y-m-d')."' where id = ".$trt[0]['id'];
			$objDB->sql_query($sql);
			$flag=0;
		}else{
			$date = @explode(', ',$trt[0]['notify']);
			$today = strtotime(date("Y-m-d"));	
			$past = strtotime($date[count($date)-1]);	
			$diff = $today-$past;	
	
			if($diff>=2){
				$not = $trt[0]['notify'].", ".date('Y-m-d');
				$sql = "UPDATE sermon SET notify='".$notify."' where id = ".$trt[0]['id'];
				$objDB->sql_query($sql);
				$flag=0;
			}else
				$flag=1;
			if($flag==0){
				$from = EMAIL_ADDRESS;
				$to = GetFieldData("admin","Email","where UserID=1");
				$cc = GetFieldData("admin","Email","where place_id=".DEFAULT_PLACE);
				$subject = "Echoes From The Past content notification";
				$body = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
		<tr>
			<td>Dear user,</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>Please add new content for  &quot;Echoes From The Past&quot;.</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><p>Thank &amp; Regards.</p>
			</td>
		</tr>
	</table>
	';
	$Headers = "From: <".$from.">\r\nCc: " . $cc ."\r\n".
					'X-Mailer: PHP/' . phpversion() . "\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: text/html; charset=utf-8\r\n" .
					"Content-Transfer-Encoding: 8bit\r\n\r\n";	
	$flag = @mail($to, $subject, $body, $Headers); 
	
			}
		}
	}		
}else{
	$from = EMAIL_ADDRESS;
	$to = GetFieldData("admin","Email","where UserID=1");
	$cc = GetFieldData("admin","Email","where place_id=".DEFAULT_PLACE);
	$subject = "Echoes From The Past content notification";
	$body = '<table width="100%" border="0" cellspacing="1" cellpadding="5">
		<tr>
			<td>Dear user,</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td>There are no &quot;Echoes From The Past&quot;. <br>Please add content for  &quot;Echoes From The Past&quot;.</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<td><p>Thank &amp; Regards.</p>
			</td>
		</tr>
	</table>
	';
	$Headers = "From: <".$from.">\r\nCc: " . $cc ."\r\n".
					'X-Mailer: PHP/' . phpversion() . "\r\n" .
					"MIME-Version: 1.0\r\n" .
					"Content-Type: text/html; charset=utf-8\r\n" .
					"Content-Transfer-Encoding: 8bit\r\n\r\n";	
	$flag = @mail($to, $subject, $body, $Headers); 
}
/* */

if(isset($_SESSION['AdminID']) && !empty($_SESSION['AdminID'])) {
    if(isset($_REQUEST['p']) && !empty($_REQUEST['p']))
        $page = $_REQUEST['p'].".php";
    else
        $page = "home.php";
}
else {
    if(isset($_REQUEST['p']) && $_REQUEST['p']=="forgot_password")
        $page = $_REQUEST['p'].".php";
    else
        $page = "login.php";
}
$PagePrefix=split("_",$_REQUEST['p']);

?>
<html>
<head>
<title><?php echo COMPANY_NAME?>- Admin Panel</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="shortcut icon" href="../images/favicon.ico" type="image/x-icon" />
<?php /*?><script language="JavaScript" type="text/javascript" src="../js/validator.js"></script>
<script language="javascript" type="text/javascript" src="../js/stm31.js"></script>
<script language="javascript" type="text/javascript" src="js/zippro_gen.js"></script>
<script language="javascript" type="text/javascript" src="js/ajax_class.js"></script><?php */?>
<script language="javascript" type="text/javascript" src="js/adminmenu.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jqueryui.js"></script>
<script type="text/javascript" src="js/jquery.metadata.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.validate.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css">
<?php /*?><link href="css/ui.css" rel="stylesheet" type="text/css"><?php */?>
<link href="css/jquery-ui.css" rel="stylesheet" type="text/css">

<script language="javascript" src="js/calender.js"></script>
<script type="text/javascript">
$.metadata.setType("attr", "validate");

function nonWorkingDates(date){
	var day = date.getDay(), Sunday = 0, Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6;	
	var closedDays = [[Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [Saturday]];
	for (var i = 0; i < closedDays.length; i++) {
		if (day == closedDays[i][0]) {
			return [false];
		}

	}
	return [true];
}	
</script>
<script>
function MM_showHideLayers() { //v9.0
  var i,p,v,obj,args=MM_showHideLayers.arguments;
  for (i=0; i<(args.length-2); i+=3) 
  with (document) if (getElementById && ((obj=getElementById(args[i]))!=null)) { v=args[i+2];
    if (obj.style) { obj=obj.style; v=(v=='show')?'visible':(v=='hide')?'hidden':v; }
    obj.visibility=v; }
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
</script>
<style type="text/css">
/*=========6-5-10==========*/
#map {
	width:500px;
	height:296px;
	position:absolute;
	z-index:594;
	margin-left:-415px;
	margin-top:-25px;
	visibility:hidden;
}
#map2 {
	width:445px;
	height:527px;
	position:absolute;
	z-index:594;
	margin-left:-415px;
	margin-top:-40px;
	visibility:hidden;
}
label {
	width: 10em;
	float: left;
}
label.error {
	float: none;
	color: red;
	padding-left: .5em;
	vertical-align: top;
}
.submit {
	margin-left: 12em;
}
em {
	font-weight: bold;
	padding-right: 1em;
	vertical-align: top;
}
</style>
<?php if(!empty($_SESSION['ErrorMsg']) || !empty($_SESSION['SuccessMsg'])){ ?>
<script type="text/javascript">
$(document).ready(function()
{
	setTimeout(function() {
			$('#divmsg').hide('slow');
	}, 1000);
});
</script>
<?php } ?>
</head>
<body>
<!--script language="javascript" type="text/javascript" src="js/wz_tooltip.js"></script>
        <script type="text/javascript" src="js/tip_balloon.js"></script-->
<table width="993" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="<?php if($_SESSION['AdminID']!='') echo BGCOLOR_TOP_TBL; else echo '#ffffff';?>">
  <tr>
    <td width="10" height="11" valign="top"><img src="<?php echo ADMIN_IMAGE_PATH;?>/layout_lc.gif" width="10" height="11" alt=""></td>
    <td height="11" colspan="2" bgcolor="<?php echo BGCOLOR_TOP_TBL_LeftBottomTopRight;?>"></td>
    <td width="10" height="11"><img src="<?php echo ADMIN_IMAGE_PATH;?>/layout_rc.gif" width="10" height="11" alt=""></td>
  </tr>
  <tr>
    <td bgcolor="#246494"></td>
    <td colspan="2" width="983" valign="top"><?php include('includes/header.php')?></td>
    <td bgcolor="#246494" width="10"></td>
  </tr>
  <tr>
    <td width="10" height="11" valign="bottom"><img src="<?php echo ADMIN_IMAGE_PATH;?>/layout_lcb.gif" width="10" height="11" alt=""></td>
    <td height="11" colspan="2" bgcolor="<?php echo BGCOLOR_TOP_TBL_LeftBottomTopRight;?>"></td>
    <td width="10" height="11" valign="bottom"><img src="<?php echo ADMIN_IMAGE_PATH;?>/layout_rcb.gif" width="10" height="11" alt=""></td>
  </tr>
  <tr>
    <td  height="1" bgcolor="#C0C0C0"></td>
    <td height="1" colspan="2" bgcolor="#F1F1F1" ></td>
    <td  height="1" bgcolor="#C0C0C0"></td>
  </tr>
  <tr  bgcolor="#FFFFFF">
    <td width="10" height="11" valign="top"><img src="<?php echo ADMIN_IMAGE_PATH;?>/layout_lc.gif" width="10" height="11" alt=""></td>
    <td height="11" colspan="2"  bgcolor="<?php echo BGCOLOR_TOP_TBL_LeftBottomTopRight;?>"></td>
    <td width="10" height="11" ><img src="<?php echo ADMIN_IMAGE_PATH;?>/layout_rc.gif" width="10" height="11" alt=""></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="<?php echo BGCOLOR_TOP_TBL_LeftBottomTopRight;?>" width="10"></td>
    <td align="left" class="padding_10px"><span class="backtxt">Powered By: <a href="http://www.zipprosystem.com" target="_blank"><img src="http://www.zipprosystem.co.uk/client/powered_by/powered_by_zipprosystem.jpg" alt="Powered by: Zippro System" title="Powered by: Zippro System" align="absmiddle" border="0" /></a></span></td>
	
    <td align="right" class="padding_10px"><!-- Footer -->
      <span class="footertxt"> Copyright &copy;
      <!-- Display Current Year Script -->
      <script language="JavaScript" type="text/javascript">var date = new Date();
var nextYear = parseFloat(date.getFullYear()+1);
document.write(date.getFullYear());
</script>
      .
      <!-- End Script-->
      <?php echo FOOTER?></span><br>
      <!-- ------ -->
    </td>
    <td bgcolor="<?php echo BGCOLOR_TOP_TBL_LeftBottomTopRight;?>" width="10"></td>
  </tr>
  <tr bgcolor="#FFFFFF">
    <td bgcolor="<?php echo BGCOLOR_TOP_TBL_LeftBottomTopRight;?>" height="11" width="10" valign="bottom"></td>
    <td height="11" colspan="2" bgcolor="<?php echo BGCOLOR_TOP_TBL_LeftBottomTopRight;?>"></td>
    <td  bgcolor="<?php echo BGCOLOR_TOP_TBL_LeftBottomTopRight;?>" height="11" width="10" valign="bottom"></td>
  </tr>
</table>
<div id="tooltip_123"  class="xstooltip"> </div>
<div id="window_123"   class="xstooltip"> </div>
</body>
</html>
<?php
$objDB->close();
?>
