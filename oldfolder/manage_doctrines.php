<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=doctrines_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('doctrines_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/doctrines";
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);
					
					$SQL = "SELECT * FROM doctrines ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['image']!=''){
						unlink($path."/big/".stripslashes($img[0]['image']));
						unlink($path."/small/".stripslashes($img[0]['image']));
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=doctrines_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		$SQL = "UPDATE doctrines SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['ContentHeading']).'",';
		$SQL .= "image='".$CImage."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';		
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Doctrines Content Updated Successfully!';
		header("Location: index.php?p=doctrines_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=doctrines_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('doctrines_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/doctrines";
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);							
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=doctrines_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = '';
		}
		$desc = addslashes($_REQUEST['Content']);
		
		$SQL = "INSERT doctrines SET ";		
		$SQL .= 'title="'.addslashes($_REQUEST['ContentHeading']).'",';
		$SQL .= "image='".$CImage."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';	
		$SQL .= 'description="'.$desc.'",';		
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Doctrines Content Added Successfully!';
		header("Location: index.php?p=doctrines_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	
	
//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$sql = "select * from doctrines where id = ".$_REQUEST['ID'];
	$doc = $objDB->select($sql);
	if($doc[0]['image']!=''){
		unlink('../uploads/doctrines/big/'.stripslashes($doc[0]['image']));
		unlink('../uploads/doctrines/small/'.stripslashes($doc[0]['image']));
	}
	
	$SQL = "DELETE FROM doctrines ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Doctrines Content Deleted Successfully!';
	header("Location: index.php?p=doctrines_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$sql = "select * from doctrines where id = ".$_REQUEST['del'][$i];
		$doc = $objDB->select($sql);
		if($doc[0]['image']!=''){
			unlink('../uploads/doctrines/big/'.stripslashes($doc[0]['image']));
			unlink('../uploads/doctrines/small/'.stripslashes($doc[0]['image']));
		}
		
		$SQL = "DELETE FROM doctrines ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Doctrines Content Deleted Successfully!';
	
	header("Location: index.php?p=doctrines_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
