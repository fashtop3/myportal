<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbClass.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		$sql = "select * from member_position where title = '".addslashes($_REQUEST['Title'])."' and type = '".addslashes($_REQUEST['Type'])."' and place = '".addslashes($_REQUEST['Place'])."' and id != ".$_REQUEST['id'];
		$pos = $objDB->select($sql);
		if($pos[0]['id']!='')
		{
			$_SESSION['ErrorMsg'] = 'Position Already Exists!';
			header("Location: index.php?p=position_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$SQL = "UPDATE member_position SET ";
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";
		$SQL .= "type='".addslashes($_REQUEST['Type'])."',";
		$SQL .= "place='".addslashes($_REQUEST['Place'])."',";
		$SQL .= "description='".addslashes($_REQUEST['Content'])."',";
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Position Updated Successfully!';
		header("Location: index.php?p=position_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		$sql = "select * from member_position where title = '".addslashes($_REQUEST['Title'])."' and type = '".addslashes($_REQUEST['Type'])."' and place = '".addslashes($_REQUEST['Place'])."'";
		$pos = $objDB->select($sql);
		if($pos[0]['id']!='')
		{
			$_SESSION['ErrorMsg'] = 'Position Already Exists!';
			header("Location: index.php?p=position_list&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$SQL = "INSERT member_position SET ";		
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";
		$SQL .= "type='".addslashes($_REQUEST['Type'])."',";
		$SQL .= "place='".addslashes($_REQUEST['Place'])."',";
		$SQL .= 'place_id='.addslashes($_SESSION['PlaceID']).',';	
		$SQL .= "description='".addslashes($_REQUEST['Content'])."',";
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Position Added Successfully!';
		header("Location: index.php?p=position_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$sql = "select * from member where position = ".$_REQUEST['ID'];
	$cu = $objDB->select($sql);
	if(count($cu)>0){
		$_SESSION['ErrorMsg'] = 'You can not delete this Position. It is already assigned to few members.';
		header("Location: index.php?p=position_list&pg_no=".$_REQUEST['pg_no']);	
		exit;
	}
	$SQL = "DELETE FROM member_position ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Position Deleted Successfully!';
	header("Location: index.php?p=position_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$sql = "select * from member where position = ".$_REQUEST['del'][$i];
		$cu = $objDB->select($sql);
		if(count($cu)>0){
			$_SESSION['ErrorMsg'] = 'You can not delete this Position. It is already assigned to few members.';
			header("Location: index.php?p=position_list&pg_no=".$_REQUEST['pg_no']);	
			exit;
		}
		$SQL = "DELETE FROM member_position ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Positions Deleted Successfully!';
	
	header("Location: index.php?p=position_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
