<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=event_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];
					$Lastpos = strrpos($FileName, '.');
					$Image = substr($FileName, 0 , $Lastpos);
					$FileExtension = $img_extension1[count($img_extension1)-1];
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('event_'.substr(md5($upload_file.strtotime("now")),-5).".".$FileExtension);	
					//$PImage = $FileName;	
					
					copy($_FILES['CImage']['tmp_name'], "../uploads/event/big/".$CImage);
					copy($_FILES['CImage']['tmp_name'], "../uploads/event/small/".$CImage);
					
					/*$image = new SimpleImage();
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save("../uploads/event/big/".$CImage);
					$image->scale(45);
					$image->save("../uploads/event/small/".$CImage);*/
					
					$SQL = "SELECT * FROM event ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['image']!='notavailable.jpg'){
					unlink('../uploads/event/big/'.stripslashes($img[0]['image']));
					unlink('../uploads/event/small/'.stripslashes($img[0]['image']));
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=event_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		$SQL = "UPDATE event SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['ContentHeading']).'",';
		$SQL .= "event_date='".$_REQUEST['Date']."',";
		$SQL .= "to_date='".$_REQUEST['ToDate']."',";
		$SQL .= 'address="'.addslashes($_REQUEST['Address']).'",';
		$SQL .= "image='".$CImage."',";
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "admin_modifiedby='".$_SESSION['AdminID']."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modifiedby='0'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Event Updated Successfully!';
		header("Location: index.php?p=event_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=event_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];
					$Lastpos = strrpos($FileName, '.');
					$Image = substr($FileName, 0 , $Lastpos);
					$FileExtension = $img_extension1[count($img_extension1)-1];
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('event_'.substr(md5($upload_file.strtotime("now")),-5).".".$FileExtension);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					copy($_FILES['CImage']['tmp_name'], "../uploads/event/big/".$CImage);
					copy($_FILES['CImage']['tmp_name'], "../uploads/event/small/".$CImage);
					
					/*$image = new SimpleImage();
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save("../uploads/event/big/".$CImage);
					$image->scale(45);
					$image->save("../uploads/event/small/".$CImage);	*/				
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Flag Image Not Uploaded";
				header("Location: index.php?p=event_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage="notavailable.jpg";
		}
		$SQL = "INSERT event SET ";		
		$SQL .= 'title="'.addslashes($_REQUEST['ContentHeading']).'",';
		$SQL .= "event_date='".$_REQUEST['Date']."',";
		$SQL .= "to_date='".$_REQUEST['ToDate']."',";
		$SQL .= 'address="'.addslashes($_REQUEST['Address']).'",';
		$SQL .= "image='".$CImage."',";
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "admin_createdby='".$_SESSION['AdminID']."',";
		$SQL .= "createdby='0'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Event Added Successfully!';
		header("Location: index.php?p=event_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	
	
//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
	$sql = "select * from event where id = ".$_REQUEST['ID'];
	$ser = $objDB->select($sql);
	for($i=0;$i<count($ser);$i++)
	{
		if($ser[$i]['image']!='notavailable.jpg')
		{
			unlink('../uploads/event/big/'.stripslashes($ser[$i]['image']));
			unlink('../uploads/event/small/'.stripslashes($ser[$i]['image']));
		}		
	}
	$SQL = "DELETE FROM event ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Event Deleted Successfully!';
	header("Location: index.php?p=event_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$sql = "select * from event where id = ".$_REQUEST['del'][$i];
		$ser = $objDB->select($sql);
		for($j=0;$j<count($ser);$j++)
		{
			if($ser[$j]['image']!='notavailable.jpg')
			{
				unlink('../uploads/event/big/'.stripslashes($ser[$j]['image']));
				unlink('../uploads/event/small/'.stripslashes($ser[$j]['image']));
			}		
		}
		$SQL = "DELETE FROM event ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";

		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Event Deleted Successfully!';
	
	header("Location: index.php?p=event_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
