<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;
	var_dump($_REQUEST);
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(VIDEO_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Video can not be uploaded...!";
						header("Location: index.php?p=video_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('video_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					copy($_FILES['CImage']['tmp_name'], "../uploads/video/".$CImage);
					$mp3s = explode('.',$CImage);
												//$ext = $mp3s[count($mp3s)-1] ;
												$mp3s[count($mp3s)-1] = 'mp3';
												$mp3 = implode('.',$mp3s);
											
					copy($_FILES['CImage']['tmp_name'], "../uploads/video/".$mp3);
					$SQL = "SELECT * FROM webcast_video ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['video']!=''){
						$mp3s = explode('.',$img[0]['video']);
												//$ext = $mp3s[count($mp3s)-1] ;
												$mp3s[count($mp3s)-1] = 'mp3';
												$mp3 = implode('.',$mp3s);
						unlink("../uploads/video/".$img[0]['video']);	
						unlink("../uploads/video/".$mp3);						
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Video Not Uploaded";
				header("Location: index.php?p=video_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		
		$SQL = "UPDATE webcast_video SET ";
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";
		$SQL .= "sermon_id='".addslashes($_REQUEST['sermon'])."',";
		$SQL .= "video='".$CImage."',";
		$SQL .= "video_date='".$_REQUEST['Date']."',";
		$SQL .= "description='".addslashes($_REQUEST['Content'])."',";
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "modifiedby='".$_SESSION['AdminID']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Webcast Video Updated Successfully!';
		header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		var_dump($_REQUEST);
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					$image_type = strtoupper(VIDEO_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						//echo $img_extension1;
						$_SESSION['ErrorMsg'] = "Video can not be uploaded...!";
						header("Location: index.php?p=video_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];
					
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('video_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					copy($_FILES['CImage']['tmp_name'], "../uploads/video/".$CImage);
					$mp3s = explode('.',$CImage);
												//$ext = $mp3s[count($mp3s)-1] ;
												$mp3s[count($mp3s)-1] = 'mp3';
												$mp3 = implode('.',$mp3s);
											
					copy($_FILES['CImage']['tmp_name'], "../uploads/video/".$mp3);		
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Video Not Uploaded";
				//header("Location: index.php?p=video_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Video does not exists";
			header("Location: index.php?p=video_addedit&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
		$SQL = "INSERT webcast_video SET ";		
		$SQL .= "title='".addslashes($_REQUEST['Title'])."',";
		$SQL .= "sermon_id='".addslashes($_REQUEST['sermon'])."',";
		$SQL .= "video='".$CImage."',";
		$SQL .= 'place_id='.$_SESSION['PlaceID'].',';	
		$SQL .= "video_date='".$_REQUEST['Date']."',";
		$SQL .= "description='".addslashes($_REQUEST['Content'])."',";
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "createdby='".$_SESSION['AdminID']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Video Added Successfully!';
		header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
//	unlink()
	$SQL = "SELECT * FROM webcast_video ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);
	$path = '../uploads/video';
	if($img[0]['video']!=''){
		unlink($path."/".$img[0]['video']);
	}
	$SQL = "DELETE FROM webcast_video ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Video Deleted Successfully!';
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "SELECT * FROM webcast_video ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$img = $objDB->sql_query($SQL);
		$path = '../uploads/video/';
		if($img[0]['video']!=''){
			unlink($path.$img[0]['video']);
		}
		$SQL = "DELETE FROM webcast_video ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Video Deleted Successfully!';
	
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
?>
