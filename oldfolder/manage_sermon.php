<?php  
	ob_start();
	session_start();
	require_once("../utils/config.php");
	require_once("../utils/functions.php");
	require_once("../utils/dbclass.php");
	require_once("../utils/SimpleImage.php");
	$objDB = new MySQLCN;

/*$ftp_server = "apostolicfaith.org.uk";
$ftp_user_name = "apostol8";
$ftp_user_pass = "9coh7%<oNn";
$file_path = "/public_html/phase3/uploads/sermon/video/";*/
	//======================== Update ========================
	if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATE")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=sermon_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('sermon_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/sermon/image";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);
					
					$SQL = "SELECT * FROM sermon ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['image']!=''){
						unlink($path."/big/".$img[0]['image']);
						unlink($path."/small/".$img[0]['image']);
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=sermon_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = addslashes($_REQUEST['OldImage']);
		}
		
		if(!empty($_FILES['CDown']['name']))
		{  
			if($_FILES['CDown']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CDown']['name']);			
					
					$image_type = strtoupper(FILE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "File can not be uploaded...!";
						header("Location: index.php?p=sermon_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CDown']['name'];					
										
					$upload_file = $_FILES['CDown']['tmp_name'];
					$CDown = addslashes('sermon_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/sermon/download/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CDown;					
					copy($_FILES['CDown']['tmp_name'], $path.$CDown);
					
					$SQL = "SELECT * FROM sermon ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['file']!=''){
						unlink($path.$img[0]['file']);
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "File Not Uploaded";
				header("Location: index.php?p=sermon_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CDown = addslashes($_REQUEST['OldDown']);
		}
		
		if(!empty($_FILES['CVideo']['name']))
		{  
			if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CVideo']['name']);			
					
					$image_type = strtoupper(VIDEO_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Video can not be uploaded...!";
						header("Location: index.php?p=sermon_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CVideo']['name'];					
										
					$upload_file = $_FILES['CVideo']['tmp_name'];
					$CVideo = addslashes('sermon_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/sermon/video/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CVideo;					
					copy($_FILES['CVideo']['tmp_name'], $path.$CVideo);
					$mp3s = explode('.',$CVideo);
												//$ext = $mp3s[count($mp3s)-1] ;
												$mp3s[count($mp3s)-1] = 'mp3';
												$mp3 = implode('.',$mp3s);
											
					copy($_FILES['CVideo']['tmp_name'], "../uploads/sermon/video/".$mp3);			
					$SQL = "SELECT * FROM sermon ";
					$SQL .= "WHERE id = '".$_REQUEST['id']."'";
					$img = $objDB->sql_query($SQL);
					if($img[0]['video']!=''){
						$mp3s = explode('.',$img[0]['video']);
												//$ext = $mp3s[count($mp3s)-1] ;
												$mp3s[count($mp3s)-1] = 'mp3';
												$mp3 = implode('.',$mp3s);
						unlink($path.$img[0]['video']);
						unlink("../uploads/sermon/video/".$mp3);	
					}					
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Video Not Uploaded";
				header("Location: index.php?p=sermon_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CVideo = addslashes($_REQUEST['OldVideo']);
		}
		
		$SQL = "UPDATE sermon SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "image='".$CImage."',";
		$SQL .= "file='".$CDown."',";
		/*$SQL .= "video='".$CVideo."',";		*/
		$SQL .= "occation='".addslashes($_REQUEST['Occation'])."',";
		$SQL .= "type='".addslashes($_REQUEST['Type'])."',";
		$SQL .= "sermonby='".addslashes($_REQUEST['SermonBy'])."',";
		$SQL .= "sdate='".$_REQUEST['Sdate']."',";
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= "modified='".date('Y-m-d H:i:s')."',";
		$SQL .= "admin_modifiedby='".$_SESSION['AdminID']."',";
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "modifiedby='".$_REQUEST['Preacher']."'";
		$SQL .= " WHERE id=".$_REQUEST['id'];
	   // var_dump($SQL);
		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Sermon Updated Successfully!';
		header("Location: index.php?p=sermon_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}
	else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADD")
	{
		if(!empty($_FILES['CImage']['name']))
		{  
			if($_FILES['CImage']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CImage']['name']);			
					
					$image_type = strtoupper(IMAGE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Image can not be uploaded...!";
						header("Location: index.php?p=sermon_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CImage']['name'];					
										
					$upload_file = $_FILES['CImage']['tmp_name'];
					$CImage = addslashes('sermon_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/sermon/image";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CImage;
					$image = new SimpleImage();
					//var_dump($image);
					$image->load($_FILES['CImage']['tmp_name']);
					$image->scale(100);
					$image->save($path."/big/".$CImage);
					$image->scale(45);
					$image->save($path."/small/".$CImage);									
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Image Not Uploaded";
				header("Location: index.php?p=sermon_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CImage = "";
		}
		
		if(!empty($_FILES['CDown']['name']))
		{  
			if($_FILES['CDown']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CDown']['name']);			
					
					$image_type = strtoupper(FILE_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "File can not be uploaded...!";
						header("Location: index.php?p=sermon_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CDown']['name'];					
										
					$upload_file = $_FILES['CDown']['tmp_name'];
					$CDown = addslashes('sermon_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/sermon/download/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CDown;					
					copy($_FILES['CDown']['tmp_name'], $path.$CDown);															
			}
			else
			{
				$_SESSION['ErrorMsg'] = "File Not Uploaded";
				header("Location: index.php?p=sermon_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CDown = "";
		}
		
		if(!empty($_FILES['CVideo']['name']))
		{  
			if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
			{
					$img_extension1=explode(".",$_FILES['CVideo']['name']);			
					
					$image_type = strtoupper(VIDEO_TYPE);
					$type = explode('.',$image_type);
					$type= implode('',$type);
					$type = explode(',',$type);
					$flag = true;
					for($i=0;$i<count($type);$i++)
					{
						if(strtoupper($img_extension1[count($img_extension1)-1])==strtoupper($type[$i]))		{
							$flag= true;
							break;
						}
						else
							$flag=false;					
					}
					if($flag==false)
					{
						$_SESSION['ErrorMsg'] = "Video can not be uploaded...!";
						header("Location: index.php?p=sermon_addedit&pg_no=".$_REQUEST['pg_no']);
						exit;
					}
					
					$FileName = $_FILES['CVideo']['name'];					
										
					$upload_file = $_FILES['CVideo']['tmp_name'];
					$CVideo = addslashes('sermon_'.date('ymdHis').".".$img_extension1[count($img_extension1)-1]);	
					//$PImage = $FileName;	
					
					//copy($_FILES['PImage']['tmp_name'], "../uploads/product/big/".$PImage);
					$path = "../uploads/sermon/video/";//.$_REQUEST['GalleryId'];
					//echo $path."/big/".$CVideo;					
					copy($_FILES['CVideo']['tmp_name'], $path.$CVideo);	
					$mp3s = explode('.',$CVideo);
												//$ext = $mp3s[count($mp3s)-1] ;
												$mp3s[count($mp3s)-1] = 'mp3';
												$mp3 = implode('.',$mp3s);
											
					copy($_FILES['CVideo']['tmp_name'], "../uploads/sermon/video/".$mp3);									
			}
			else
			{
				$_SESSION['ErrorMsg'] = "Video Not Uploaded";
				header("Location: index.php?p=sermon_addedit&pg_no=".$_REQUEST['pg_no']);
				exit;
			}
		}
		else
		{
			$CVideo = "";
		}
		
		$SQL = "INSERT sermon SET ";
		$SQL .= 'title="'.addslashes($_REQUEST['Title']).'",';
		$SQL .= "image='".$CImage."',";
		$SQL .= "file='".$CDown."',";
		$SQL .= "video='".$CVideo."',";		
		$SQL .= "occation='".addslashes($_REQUEST['Occation'])."',";
		$SQL .= "type='".addslashes($_REQUEST['Type'])."',";
		$SQL .= "sermonby='".addslashes($_REQUEST['SermonBy'])."',";
		$SQL .= "sdate='".addslashes($_REQUEST['Sdate'])."',";
		$SQL .= 'description="'.addslashes($_REQUEST['Content']).'",';
		$SQL .= 'place_id='.$_REQUEST['Place'].',';
		$SQL .= "created='".date('Y-m-d H:i:s')."',";
		$SQL .= "admin_createdby='".$_SESSION['AdminID']."',";		
		$SQL .= "createdby='".$_REQUEST['Preacher']."'";

		//echo $SQL;exit;
		$objDB->sql_query($SQL);
		$_SESSION['SuccessMsg'] = 'Sermon Added Successfully!';
		header("Location: index.php?p=sermon_list&pg_no=".$_REQUEST['pg_no']);
		exit;
	}

	

//------Delete------
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETE")
{
//	unlink()
	$SQL = "SELECT * FROM sermon ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);
	$path = '../uploads/sermon';
	if($img[0]['image']!=''){
		unlink($path."/image/big/".$img[0]['image']);
		unlink($path."/image/small/".$img[0]['image']);
	}
	if($img[0]['file']!=''){
		unlink($path."/download/".$img[0]['file']);
	}
	if($img[0]['video']!=''){
		unlink($path."/video/".$img[0]['video']);
	}

	$SQL = "DELETE FROM sermon ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Sermon Deleted Successfully!';
	header("Location: index.php?p=sermon_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLE")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "SELECT * FROM sermon ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$img = $objDB->sql_query($SQL);
		$path = '../uploads/sermon';
		if($img[0]['image']!=''){
			unlink($path."/image//big/".$img[0]['image']);
			unlink($path."/image//small/".$img[0]['image']);
		}
		if($img[0]['file']!=''){
		unlink($path."/download/".$img[0]['file']);
	}
	if($img[0]['video']!=''){
		unlink($path."/video/".$img[0]['video']);
	}
		$SQL = "DELETE FROM sermon ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
		//Delete_Data("bd_cms","cms_id","=",$_REQUEST['del'][$i]);
	}
	$_SESSION['SuccessMsg'] = 'Sermon Deleted Successfully!';
	
	header("Location: index.php?p=sermon_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}


/* FTP Video */

if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEVIDEO")
{
	/*$SQL = "SELECT * FROM sermon ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);	
	$path = '../uploads/sermon';
	if($img[0]['video']!=''){
		unlink($path."/video/".$img[0]['video']);
	}
	if($img[0]['flash']!=''){
		unlink($path."/video/".$img[0]['flash']);
	}
	if($img[0]['audio']!=''){
		unlink($path."/video/".$img[0]['audio']);
	}*/

	$SQL = "UPDATE sermon SET video='',flash='',audio='' ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Sermon Audio/Video Deleted Successfully!';
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLEVIDEO")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		/*$SQL = "SELECT * FROM sermon ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$img = $objDB->sql_query($SQL);		
		$path = '../uploads/sermon';
		if($img[0]['video']!=''){
			unlink($path."/video/".$img[0]['video']);
		}
		if($img[0]['flash']!=''){
		unlink($path."/video/".$img[0]['flash']);
		}
		if($img[0]['audio']!=''){
			unlink($path."/video/".$img[0]['audio']);
		}*/
	
		$SQL = "UPDATE sermon SET video='',flash='',audio='' ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
	}
	$_SESSION['SuccessMsg'] = 'Sermon Audio/Video Deleted Successfully!';
	
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATEVIDEO")
{
	/*if(!empty($_FILES['CVideo']['name']))
	{  
		if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
		{
			
			$FileName = $_FILES['CVideo']['name'];															
			$upload_file = $_FILES['CVideo']['tmp_name'];
			$CVideo = $FileName;	
				
				copy($_FILES['CVideo']['tmp_name'],"../uploads/sermon/video/".$CVideo);
				
				$SQL = "SELECT * FROM sermon ";
				$SQL .= "WHERE id = '".$_REQUEST['id']."'";
				$img = $objDB->sql_query($SQL);
				if($img[0]['video']!=''){				
					unlink("../uploads/sermon/video/".$img[0]['video']);				
				}					
				$SQL = "UPDATE sermon SET ";		
				$SQL .= "video='".$CVideo."'";		
				$SQL .= " where id = ".$_REQUEST['sermon'];
				$objDB->sql_query($SQL);
				$_SESSION['SuccessMsg'] = 'Sermon Video Updated Successfully!';
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Video Not Uploaded";
			header("Location: index.php?p=video_addedit&id=".$_REQUEST['id']);
			exit;
		}
	}
	else
	{
		$CVideo = $_REQUEST['OldVideo'];
	}*/
	
	/*$SQL = "SELECT * FROM sermon ";
	$SQL .= "WHERE id = '".$_REQUEST['id']."'";
	$img = $objDB->sql_query($SQL);
	if($img[0]['video']!=''){				
		@unlink("../uploads/sermon/video/".$img[0]['video']);				
	}
	if($img[0]['audio']!=''){				
		@unlink("../uploads/sermon/video/".$img[0]['audio']);				
	}
	if($img[0]['flash']!=''){				
		@unlink("../uploads/sermon/video/".$img[0]['flash']);				
	}				*/	
	$SQL = "UPDATE sermon SET ";		
	$SQL .= 'video="'.substr(addslashes($_REQUEST['Video']),1).'",';		
	$SQL .= 'audio="'.substr(addslashes($_REQUEST['Audio']),1).'",';		
	$SQL .= 'flash="'.substr(addslashes($_REQUEST['Flash']),1).'"';		
	$SQL .= " where id = ".$_REQUEST['sermon'];
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Sermon Audio/Video Updated Successfully!';
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}
else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADDVIDEO")
{		
	/*if(!empty($_FILES['CVideo']['name']))
	{  
		if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
		{			
			$FileName = $_FILES['CVideo']['name'];															
			$upload_file = $_FILES['CVideo']['tmp_name'];
			$CVideo = $FileName;	
				
			copy($_FILES['CVideo']['tmp_name'],"../uploads/sermon/video/".$CVideo);	
			$SQL = "UPDATE sermon SET ";		
			$SQL .= "video='".$CVideo."'";		
			$SQL .= " where id = ".$_REQUEST['sermon'];
			
			$objDB->sql_query($SQL);
			$_SESSION['SuccessMsg'] = 'Sermon Video Added Successfully!';		
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Video Not Uploaded";
			header("Location: index.php?p=video_addedit");
			exit;
		}
	}
	else
	{
		$CVideo = "";
	}	*/	
	
	$SQL = "UPDATE sermon SET ";		
	$SQL .= 'video="'.substr(addslashes($_REQUEST['Video']),1).'",';		
	$SQL .= 'audio="'.substr(addslashes($_REQUEST['Audio']),1).'",';		
	$SQL .= 'flash="'.substr(addslashes($_REQUEST['Flash']),1).'"';		
	$SQL .= " where id = ".$_REQUEST['sermon'];
	
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Sermon Audio/Video Added Successfully!';		
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}

/* FTP Audio */
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEAUDIO")
{
	$SQL = "SELECT * FROM sermon ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);	
	$path = '../uploads/sermon';
	if($img[0]['audio']!=''){
		unlink($path."/video/".$img[0]['audio']);
	}

	$SQL = "UPDATE sermon SET audio='' ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Sermon Audio Deleted Successfully!';
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEFLASH")
{
	$SQL = "SELECT * FROM sermon ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$img = $objDB->sql_query($SQL);	
	$path = '../uploads/sermon';
	if($img[0]['flash']!=''){
		unlink($path."/video/".$img[0]['flash']);
	}

	$SQL = "UPDATE sermon SET flash='' ";
	$SQL .= "WHERE id = '".$_REQUEST['ID']."'";
	$objDB->sql_query($SQL);
	$_SESSION['SuccessMsg'] = 'Sermon Flash Deleted Successfully!';
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}	

//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLEFLASH")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "SELECT * FROM sermon ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$img = $objDB->sql_query($SQL);		
		$path = '../uploads/sermon';
		if($img[0]['flash']!=''){
			unlink($path."/video/".$img[0]['flash']);
		}
		$SQL = "UPDATE sermon SET flash='' ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
	}
	$_SESSION['SuccessMsg'] = 'Sermon Flash Deleted Successfully!';
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
//==================================  MULTIPLE DELETE  ==================================
if(isset($_REQUEST['Process']) && $_REQUEST['Process'] == "DELETEMULTIPLEAUDIO")
{
	for($i=0;$i<count($_REQUEST['del']);$i++)	
	{
		$SQL = "SELECT * FROM sermon ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$img = $objDB->sql_query($SQL);		
		$path = '../uploads/sermon';
		if($img[0]['audio']!=''){
			unlink($path."/video/".$img[0]['audio']);
		}
		$SQL = "UPDATE sermon SET audio='' ";
		$SQL .= "WHERE id = '".$_REQUEST['del'][$i]."'";
		$objDB->sql_query($SQL);
	}
	$_SESSION['SuccessMsg'] = 'Sermon Audio Deleted Successfully!';
	header("Location: index.php?p=video_list&pg_no=".$_REQUEST['pg_no']);	
	exit;
}
if(isset($_REQUEST['action']) && $_REQUEST['action']=="UPDATEAUDIO")
{
	if(!empty($_FILES['CVideo']['name']))
	{  
		if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
		{
			$FileName = $_FILES['CVideo']['name'];															
			$upload_file = $_FILES['CVideo']['tmp_name'];
			$CVideo = addslashes($FileName);	
		//	$file_path = "/public_html/phase3/uploads/sermon/video/".$CVideo;	
			
			// FTP upload						
			/*$conn_id = ftp_connect($ftp_server); 				
			$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 			
			if ((!$conn_id) || (!$login_result)) { 
				$_SESSION['ErrorMsg'] = "FTP connection has failed!";
				$_SESSION['ErrorMsg'] .= "Attempted to connect to $ftp_server for user $ftp_user_name"; 
				exit; 
			} else {
				$_SESSION['SuccessMsg'] = "Connected to $ftp_server, for user $ftp_user_name";
			}
			$upload = ftp_put($conn_id, $file_path.$CVideo, $_FILES['CVideo']['tmp_name'], FTP_ASCII); 
			if (!$upload) { 
				$_SESSION['ErrorMsg'] = "FTP upload has failed!";
			} else {
				$_SESSION['SuccessMsg'] = "Audio file uploaded";
				$SQL = "SELECT * FROM sermon ";
				$SQL .= "WHERE id = '".$_REQUEST['id']."'";
				$img = $objDB->sql_query($SQL);
				if($img[0]['audio']!=''){				
					unlink("../uploads/sermon/video/".$img[0]['audio']);
				}					
				$SQL = "UPDATE sermon SET ";		
				$SQL .= "audio='".$CVideo."'";		
				$SQL .= " where id = ".$_REQUEST['sermon'];
				$objDB->sql_query($SQL);
				$_SESSION['SuccessMsg'] = 'Sermon Audio Updated Successfully!';
			}
			ftp_close($conn_id);	*/
			copy($_FILES['CVideo']['tmp_name'],"../uploads/sermon/video/".$CVideo);
			$SQL = "SELECT * FROM sermon ";
			$SQL .= "WHERE id = '".$_REQUEST['id']."'";
			$img = $objDB->sql_query($SQL);
			if($img[0]['audio']!=''){				
				unlink("../uploads/sermon/video/".$img[0]['audio']);
			}					
			$SQL = "UPDATE sermon SET ";		
			$SQL .= "audio='".$CVideo."'";		
			$SQL .= " where id = ".$_REQUEST['sermon'];
			$objDB->sql_query($SQL);
			$_SESSION['SuccessMsg'] = 'Sermon Audio Updated Successfully!';
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Audio Not Uploaded";
			header("Location: index.php?p=audio_addedit&id=".$_REQUEST['id']."&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
	}
	else
	{
		$CVideo = $_REQUEST['OldVideo'];
	}
	
	
	header("Location: index.php?p=audio_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}
else if(isset($_REQUEST['action']) && $_REQUEST['action']=="ADDAUDIO")
{		
	if(!empty($_FILES['CVideo']['name']))
	{  
		if($_FILES['CVideo']['error'] == UPLOAD_ERROR_OK )
		{			
			$FileName = $_FILES['CVideo']['name'];															
			$upload_file = $_FILES['CVideo']['tmp_name'];
			$CVideo = addslashes($FileName);	
			//$file_path = "/public_html/phase3/uploads/sermon/video/".$CVideo;	
			
			// FTP upload						
			/*$conn_id = ftp_connect($ftp_server); 				
			$login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass); 			
			if ((!$conn_id) || (!$login_result)) { 
				$_SESSION['ErrorMsg'] = "FTP connection has failed!";
				$_SESSION['ErrorMsg'] .= "Attempted to connect to $ftp_server for user $ftp_user_name"; 
				exit; 
			} else {
				$_SESSION['SuccessMsg'] = "Connected to $ftp_server, for user $ftp_user_name";
			}
			$upload = ftp_put($conn_id, $file_path.$CVideo, $_FILES['CVideo']['tmp_name'], FTP_ASCII); 
			if (!$upload) { 
				$_SESSION['ErrorMsg'] = "FTP upload has failed!";
			} else {
				$_SESSION['SuccessMsg'] = "Audio file uploaded";
				$SQL = "UPDATE sermon SET ";		
				$SQL .= "audio='".$CVideo."'";		
				$SQL .= " where id = ".$_REQUEST['sermon'];
				
				$objDB->sql_query($SQL);
				$_SESSION['SuccessMsg'] = 'Sermon Audio Added Successfully!';
			}
			ftp_close($conn_id);	*/
			copy($_FILES['CVideo']['tmp_name'],"../uploads/sermon/video/".$CVideo);			
			$SQL = "UPDATE sermon SET ";		
			$SQL .= "audio='".$CVideo."'";		
			$SQL .= " where id = ".$_REQUEST['sermon'];
			
			$objDB->sql_query($SQL);
			$_SESSION['SuccessMsg'] = 'Sermon Audio Added Successfully!';		
		}
		else
		{
			$_SESSION['ErrorMsg'] = "Audio Not Uploaded";
			header("Location: index.php?p=audio_addedit&pg_no=".$_REQUEST['pg_no']);
			exit;
		}
	}
	else
	{
		$CVideo = "";
	}		
	
	
	header("Location: index.php?p=audio_list&pg_no=".$_REQUEST['pg_no']);
	exit;
}

?>
